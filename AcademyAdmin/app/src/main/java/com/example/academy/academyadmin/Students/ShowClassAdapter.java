package com.example.academy.academyadmin.Students;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.academy.academyadmin.Class.ClassListItems;
import com.example.academy.academyadmin.MainActivity;
import com.example.academy.academyadmin.R;

import java.util.List;

public class ShowClassAdapter extends RecyclerView.Adapter<ShowClassAdapter.ViewHolder> {
    private Context context;
    private List<ClassListItems> list;

    public ShowClassAdapter(Context context, List<ClassListItems> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.home_class_listitems,viewGroup,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final ClassListItems listItems = list.get(i);
        viewHolder.classname.setText(listItems.getClassname());

        switch (i){

            case 0:

                viewHolder.classname.setBackgroundColor(Color.rgb(255, 0, 0));
                viewHolder.classname.setTextColor(Color.WHITE);

                break;
            case 1:

                viewHolder.classname.setBackgroundColor(Color.rgb(0, 153, 51));
                viewHolder.classname.setTextColor(Color.WHITE);

                break;

            case 2:

                viewHolder.classname.setBackgroundColor(Color.rgb(0, 0, 179));
                viewHolder.classname.setTextColor(Color.WHITE);

                break;
            case 3:

                viewHolder.classname.setBackgroundColor(Color.rgb(153, 102, 51));
                viewHolder.classname.setTextColor(Color.WHITE);

                break;

            case 4:

                viewHolder.classname.setBackgroundColor(Color.rgb(255, 179, 179));
                viewHolder.classname.setTextColor(Color.BLACK);

                break;
            case 5:

                viewHolder.classname.setBackgroundColor(Color.rgb(255, 255, 0));
                viewHolder.classname.setTextColor(Color.BLACK);

                break;
            case 6:

                viewHolder.classname.setBackgroundColor(Color.rgb(0, 255, 0));
                viewHolder.classname.setTextColor(Color.BLACK);

                break;
            case 7:

                viewHolder.classname.setBackgroundColor(Color.rgb(179, 179, 255));
                viewHolder.classname.setTextColor(Color.BLACK);

                break;

            case 8:

                viewHolder.classname.setBackgroundColor(Color.rgb(246, 83, 20));
                viewHolder.classname.setTextColor(Color.WHITE);

                break;
            case 9:

                viewHolder.classname.setBackgroundColor(Color.rgb(124, 187, 0));
                viewHolder.classname.setTextColor(Color.WHITE);

                break;
            case 10:

                viewHolder.classname.setBackgroundColor(Color.rgb(0, 161, 241));
                viewHolder.classname.setTextColor(Color.BLACK);

                break;
            case 11:

                viewHolder.classname.setBackgroundColor(Color.rgb(255, 187, 0));
                viewHolder.classname.setTextColor(Color.BLACK);

                break;

            case 12:

                viewHolder.classname.setBackgroundColor(Color.rgb(123, 0, 153));
                viewHolder.classname.setTextColor(Color.WHITE);

                break;
            case 13:

                viewHolder.classname.setBackgroundColor(Color.rgb(229, 9, 20));
                viewHolder.classname.setTextColor(Color.WHITE);

                break;
            case 14:

                viewHolder.classname.setBackgroundColor(Color.rgb(0, 161, 241));
                viewHolder.classname.setTextColor(Color.BLACK);

                break;
            case 15:

                viewHolder.classname.setBackgroundColor(Color.rgb(223, 223, 210));
                viewHolder.classname.setTextColor(Color.BLACK);

                break;
            case 16:

                viewHolder.classname.setBackgroundColor(Color.rgb(102, 51, 0));
                viewHolder.classname.setTextColor(Color.WHITE);

                break;
            case 17:

                viewHolder.classname.setBackgroundColor(Color.rgb(51, 77, 0));
                viewHolder.classname.setTextColor(Color.WHITE);

                break;
            case 18:

                viewHolder.classname.setBackgroundColor(Color.rgb(255, 128, 179));
                viewHolder.classname.setTextColor(Color.BLACK);

                break;
            case 19:

                viewHolder.classname.setBackgroundColor(Color.rgb(102, 255, 255));
                viewHolder.classname.setTextColor(Color.BLACK);

                break;

            case 20:

                viewHolder.classname.setBackgroundColor(Color.rgb(255, 0, 0));
                viewHolder.classname.setTextColor(Color.WHITE);

                break;
            case 21:

                viewHolder.classname.setBackgroundColor(Color.rgb(0, 153, 51));
                viewHolder.classname.setTextColor(Color.WHITE);

                break;

            case 22:

                viewHolder.classname.setBackgroundColor(Color.rgb(0, 0, 179));
                viewHolder.classname.setTextColor(Color.WHITE);

                break;
            case 23:

                viewHolder.classname.setBackgroundColor(Color.rgb(153, 102, 51));
                viewHolder.classname.setTextColor(Color.WHITE);

                break;

            case 24:

                viewHolder.classname.setBackgroundColor(Color.rgb(255, 179, 179));
                viewHolder.classname.setTextColor(Color.BLACK);

                break;
            case 25:

                viewHolder.classname.setBackgroundColor(Color.rgb(255, 255, 0));
                viewHolder.classname.setTextColor(Color.BLACK);

                break;
            case 26:

                viewHolder.classname.setBackgroundColor(Color.rgb(0, 255, 0));
                viewHolder.classname.setTextColor(Color.BLACK);

                break;
            case 27:

                viewHolder.classname.setBackgroundColor(Color.rgb(179, 179, 255));
                viewHolder.classname.setTextColor(Color.BLACK);

                break;

            case 28:

                viewHolder.classname.setBackgroundColor(Color.rgb(246, 83, 20));
                viewHolder.classname.setTextColor(Color.WHITE);

                break;
            case 29:

                viewHolder.classname.setBackgroundColor(Color.rgb(124, 187, 0));
                viewHolder.classname.setTextColor(Color.WHITE);

                break;
            case 30:

                viewHolder.classname.setBackgroundColor(Color.rgb(0, 161, 241));
                viewHolder.classname.setTextColor(Color.BLACK);

                break;
            case 31:

                viewHolder.classname.setBackgroundColor(Color.rgb(255, 187, 0));
                viewHolder.classname.setTextColor(Color.BLACK);

                break;

            case 32:

                viewHolder.classname.setBackgroundColor(Color.rgb(123, 0, 153));
                viewHolder.classname.setTextColor(Color.WHITE);

                break;
            case 33:

                viewHolder.classname.setBackgroundColor(Color.rgb(229, 9, 20));
                viewHolder.classname.setTextColor(Color.WHITE);

                break;
            case 34:

                viewHolder.classname.setBackgroundColor(Color.rgb(0, 161, 241));
                viewHolder.classname.setTextColor(Color.BLACK);

                break;
            case 35:

                viewHolder.classname.setBackgroundColor(Color.rgb(223, 223, 210));
                viewHolder.classname.setTextColor(Color.BLACK);

                break;
            case 36:

                viewHolder.classname.setBackgroundColor(Color.rgb(102, 51, 0));
                viewHolder.classname.setTextColor(Color.WHITE);

                break;
            case 37:

                viewHolder.classname.setBackgroundColor(Color.rgb(51, 77, 0));
                viewHolder.classname.setTextColor(Color.WHITE);

                break;
            case 38:

                viewHolder.classname.setBackgroundColor(Color.rgb(255, 128, 179));
                viewHolder.classname.setTextColor(Color.BLACK);

                break;
            case 39:

                viewHolder.classname.setBackgroundColor(Color.rgb(102, 255, 255));
                viewHolder.classname.setTextColor(Color.BLACK);

                break;


        }
        viewHolder.classname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.openDialog(listItems.getClassid(),listItems.getClassname());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        Button classname;
        RelativeLayout relativeLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            classname = itemView.findViewById(R.id.classname);
            relativeLayout = itemView.findViewById(R.id.relativelayout);
        }
    }
}
