package com.example.academy.academyadmin.Exams;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.academy.academyadmin.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainExam extends Fragment {
    private FloatingActionButton addexambtn;
    private RecyclerView recyclerView;
    public static TextView examtype;
    public static DatabaseReference databaseReference;
    public static ProgressBar progressBar;
    private List<MainExamsListItems> list;
    private RecyclerView.Adapter adapter;
    public static Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.main_exam, container, false);
        addexambtn =  rootView.findViewById(R.id.add_fab);
        recyclerView = rootView.findViewById(R.id.mainexamrecyclerview);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        progressBar = rootView.findViewById(R.id.progressbar);
        list = new ArrayList<>();
        activity = getActivity();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        addexambtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog(true,"1","1");
            }
        });
         getExam();
        return rootView;
    }

    public static void openDialog(final boolean check, final String examid,String examname){
        final Dialog dialog = new Dialog(activity,R.style.Theme_AppCompat_Light_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.addclass_dialog);
        examtype = dialog.findViewById(R.id.classnameedittext);

        TextView toolbartext = dialog.findViewById(R.id.toolbartext);

        examtype.setHint("Enter Exam Type");
        final ImageButton dialogButton =  dialog.findViewById(R.id.closebtn);
        Button addbtn = dialog.findViewById(R.id.addbtn);

        if (check){
            toolbartext.setText("Add Exam Type");
            addbtn.setText("ADD NOW");
        }else{
            toolbartext.setText("Update Exam Type");
            addbtn.setText("UPDATE NOW");
            examtype.setText(examname);
        }

        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (examtype.getText().length()==0){
                    examtype.setError("This field is required");
                }else{
                    if (check){
                        addExamType(dialog);
                    }else{
                        updateExamType(examid,dialog);
                    }

                }

            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private static void addExamType(final Dialog dialog){
        progressBar.setVisibility(View.VISIBLE);
        AddMainExam_Firebase addExam = new AddMainExam_Firebase();
        addExam.setExamname(examtype.getText().toString().trim());
        databaseReference.child("ExamsType").child("MainExam").push().setValue(addExam).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dialog.dismiss();
                Toast.makeText(activity, "Main Exam has been added", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void updateExamType(String examid, final Dialog dialog){
        databaseReference.child("ExamsType").child("MainExam").child(examid).child("examname").setValue(examtype.getText().toString().trim())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        dialog.dismiss();
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(activity, "Exam has been updated", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void getExam(){
        progressBar.setVisibility(View.VISIBLE);
        FirebaseDatabase.getInstance().getReference().child("ExamsType").child("MainExam")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (adapter!=null){
                            list.clear();
                        }
                        progressBar.setVisibility(View.GONE);
                        for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                            MainExamsListItems listItems = new MainExamsListItems(snapshot.getKey(),
                                    snapshot.child("examname").getValue().toString());
                            list.add(listItems);
                        }
                        adapter = new MainExamAdapter(getActivity(),list);
                        recyclerView.setAdapter(adapter);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

}
