package com.example.academy.academyadmin.Students;

public class ResultListItems {
    private String resultid,mainexam,subexam,subject,date,totalmarks,obtainedmarks,studentid,short_brief;

    public ResultListItems(String resultid, String mainexam, String subexam, String subject, String date, String totalmarks, String obtainedmarks, String studentid,String short_brief) {
        this.resultid = resultid;
        this.mainexam = mainexam;
        this.subexam = subexam;
        this.subject = subject;
        this.date = date;
        this.totalmarks = totalmarks;
        this.obtainedmarks = obtainedmarks;
        this.studentid = studentid;
        this.short_brief = short_brief;
    }

    public String getShort_brief() {
        return short_brief;
    }

    public void setShort_brief(String short_brief) {
        this.short_brief = short_brief;
    }

    public String getResultid() {
        return resultid;
    }

    public String getMainexam() {
        return mainexam;
    }

    public String getSubexam() {
        return subexam;
    }

    public String getSubject() {
        return subject;
    }

    public String getDate() {
        return date;
    }

    public String getTotalmarks() {
        return totalmarks;
    }

    public String getObtainedmarks() {
        return obtainedmarks;
    }

    public String getStudentid() {
        return studentid;
    }
}
