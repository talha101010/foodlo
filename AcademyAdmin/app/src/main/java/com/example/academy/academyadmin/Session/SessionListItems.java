package com.example.academy.academyadmin.Session;

public class SessionListItems {
    private String sessionid,sessionname;

    public SessionListItems(String sessionid, String sessionname) {
        this.sessionid = sessionid;
        this.sessionname = sessionname;
    }

    public String getSessionid() {
        return sessionid;
    }

    public String getSessionname() {
        return sessionname;
    }
}
