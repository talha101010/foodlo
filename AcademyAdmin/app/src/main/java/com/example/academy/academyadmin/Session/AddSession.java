package com.example.academy.academyadmin.Session;

import android.app.Activity;
import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.academy.academyadmin.Class.AddClass;
import com.example.academy.academyadmin.Class.AddClass_Firebase;
import com.example.academy.academyadmin.Class.ClassAdapter;
import com.example.academy.academyadmin.Class.ClassListItems;
import com.example.academy.academyadmin.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class AddSession extends AppCompatActivity {
    private FloatingActionButton addsessionbtn;
    public static EditText sessionname;
    public static Activity activity;
    public static ProgressBar progressBar;
    public static DatabaseReference databaseReference;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<SessionListItems> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_session);
        activity = AddSession.this;
        getViews();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        getSessionFromFirebase();
        addsessionbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               openDialog(true,"1","1");
            }
        });
    }
    private void getViews(){
        addsessionbtn = findViewById(R.id.add_fab);
        progressBar = findViewById(R.id.progressbar);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        list = new ArrayList<>();
        recyclerView = findViewById(R.id.sessionrecyclerview);

    }

    public static void openDialog(final boolean check, final String id,String cname){
        final Dialog dialog = new Dialog(activity,R.style.Theme_AppCompat_Light_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.addsession_dialog);
        sessionname = dialog.findViewById(R.id.sessionnameedittext);
        ImageButton dialogButton =  dialog.findViewById(R.id.closebtn);
        Button addbtn = dialog.findViewById(R.id.addbtn);
        if (check){
            addbtn.setText("Add Now");
        }else{
            sessionname.setText(cname);
            addbtn.setText("Update Now");
        }
        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionname.getText().length()==0){
                    sessionname.setError("This field is required");
                }else{
                    if (check){
                        addSessionToFirebase(dialog);
                    }else{
                        progressBar.setVisibility(View.VISIBLE);
                        updateSessionToFirebase(id,dialog);
                    }

                }

            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static void addSessionToFirebase(final Dialog dialog){
        progressBar.setVisibility(View.VISIBLE);
        AddSession_Firebase addSession = new AddSession_Firebase();
        addSession.setSessionname(sessionname.getText().toString().trim());
        databaseReference.child("Sessions").push().setValue(addSession)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        dialog.dismiss();
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(activity, "Session has been added", Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(activity, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void updateSessionToFirebase(String id, final Dialog dialog){
        databaseReference.child("Sessions").child(id).child("sessionname").setValue(sessionname.getText().toString().trim())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        dialog.dismiss();
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(activity, "Session has been updated", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void getSessionFromFirebase(){
        databaseReference.child("Sessions").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (adapter != null){
                    list.clear();
                }
                progressBar.setVisibility(View.GONE);
                for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                    SessionListItems listItems = new SessionListItems(snapshot.getKey(),snapshot.child("sessionname").getValue().toString());
                    list.add(listItems);
                }
                adapter = new SessionAdapter(AddSession.this,list);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
