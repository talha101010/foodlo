package com.example.academy.academyadmin.Students;

public class ShowStudentsListitems {
    private String studentid,studentname,fname,classid,sessionid,contactnumber,fcontactnumber,cnic,dob,gender,address,rollno,institutename;
    private String attendanceYear,absent,present,leave,nextdate;

    public ShowStudentsListitems(String studentid,String studentname, String fname, String classid, String sessionid, String contactnumber, String fcontactnumber, String cnic, String dob, String gender, String address,String rollno,String institutename) {
        this.studentid = studentid;
        this.studentname = studentname;
        this.fname = fname;
        this.classid = classid;
        this.sessionid = sessionid;
        this.contactnumber = contactnumber;
        this.fcontactnumber = fcontactnumber;
        this.cnic = cnic;
        this.dob = dob;
        this.gender = gender;
        this.address = address;
        this.rollno = rollno;
        this.institutename = institutename;
    }

    public String getNextdate() {
        return nextdate;
    }

    public void setNextdate(String nextdate) {
        this.nextdate = nextdate;
    }

    public String getRollno() {
        return rollno;
    }

    public String getInstitutename() {
        return institutename;
    }

    public String getStudentid() {
        return studentid;
    }

    public String getStudentname() {
        return studentname;
    }

    public String getFname() {
        return fname;
    }

    public String getClassid() {
        return classid;
    }

    public String getSessionid() {
        return sessionid;
    }

    public String getContactnumber() {
        return contactnumber;
    }

    public String getFcontactnumber() {
        return fcontactnumber;
    }

    public String getCnic() {
        return cnic;
    }

    public String getDob() {
        return dob;
    }

    public String getGender() {
        return gender;
    }

    public String getAddress() {
        return address;
    }

    public String getAttendanceYear() {
        return attendanceYear;
    }

    public void setAttendanceYear(String attendanceYear) {
        this.attendanceYear = attendanceYear;
    }

    public String getAbsent() {
        return absent;
    }

    public void setAbsent(String absent) {
        this.absent = absent;
    }

    public String getPresent() {
        return present;
    }

    public void setPresent(String present) {
        this.present = present;
    }

    public String getLeave() {
        return leave;
    }

    public void setLeave(String leave) {
        this.leave = leave;
    }
}
