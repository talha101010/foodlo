package com.example.academy.academyadmin.Exams;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.academy.academyadmin.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class SubExam extends Fragment {
    private FloatingActionButton addSubExambtn;
    public static Activity activity;
    public static EditText examtype;
    public static Spinner mainExamSpinner;
    public static ArrayList<String> mainName;
    public static ArrayAdapter<String> mainAdapter;
    public static DatabaseReference databaseReference;
    public static ProgressBar progressBar;
    private List<SubExamListItems> list;
    private RecyclerView.Adapter adapter;
    private RecyclerView recyclerView;
    private String examName;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sub_exam, container, false);
        addSubExambtn =  rootView.findViewById(R.id.add_fab);
        activity = getActivity();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        progressBar = rootView.findViewById(R.id.progressbar);
        recyclerView = rootView.findViewById(R.id.subexamrecyclerview);
        mainName = new ArrayList<>();
        list = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        getSubExams();
        addSubExambtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                  openDialog(true,"1","1");

            }
        });

        return rootView;
    }

    public static void openDialog(final boolean check, final String examid,String examname){
        final Dialog dialog = new Dialog(activity,R.style.Theme_AppCompat_Light_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.addsubject_dialog);
        examtype = dialog.findViewById(R.id.classnameedittext);
        mainExamSpinner = dialog.findViewById(R.id.subjectclassspinner);
        ProgressBar progressBar1 = dialog.findViewById(R.id.progressbar);
        TextView toolbartext = dialog.findViewById(R.id.toolbartext);

        examtype.setHint("Enter Sub Exam Type");
        final ImageButton dialogButton =  dialog.findViewById(R.id.closebtn);
        Button addbtn = dialog.findViewById(R.id.addbtn);
        mainAdapter = new ArrayAdapter<String>(activity,android.R.layout.simple_spinner_dropdown_item,mainName);
        getMainExams(progressBar1);
        if (check){
            toolbartext.setText("Add Sub Exam Type");
            addbtn.setText("ADD NOW");
        }else{
            toolbartext.setText("Update Sub Exam Type");
            addbtn.setText("UPDATE NOW");
            examtype.setText(examname);
        }

        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mainExamSpinner.getSelectedItemPosition()==0){
                    Toast.makeText(activity, "Please select main exam", Toast.LENGTH_SHORT).show();
                }else if (examtype.getText().length()==0){
                    examtype.setError("This field is required");
                }else{
                    if (check){
                        addExamType(dialog);
                    }else{
                       // updateExamType(examid,dialog);
                    }

                }

            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private static void addExamType(final Dialog dialog){
       progressBar.setVisibility(View.VISIBLE);
       AddSubExam_Firebase addExam = new AddSubExam_Firebase();
       addExam.setMainexamname(mainName.get(mainExamSpinner.getSelectedItemPosition()));
       addExam.setSubexamname(examtype.getText().toString().trim());
       databaseReference.child("ExamsType").child("SubExam").push().setValue(addExam).addOnSuccessListener(new OnSuccessListener<Void>() {
           @Override
           public void onSuccess(Void aVoid) {
               dialog.dismiss();
               Toast.makeText(activity, "Sub exam has been added", Toast.LENGTH_SHORT).show();
           }
       });
    }

    public static void getMainExams(final ProgressBar progressBar1){
        databaseReference.child("ExamsType").child("MainExam").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                 if (mainAdapter!=null){
                     mainName.clear();
                 }
                 progressBar1.setVisibility(View.GONE);
                 mainName.add(0,"Please Select Main Exam");
                 for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                     mainName.add(snapshot.child("examname").getValue().toString());
                 }
                 mainAdapter.notifyDataSetChanged();
                 mainExamSpinner.setAdapter(mainAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getSubExams(){
        progressBar.setVisibility(View.VISIBLE);
        FirebaseDatabase.getInstance().getReference().child("ExamsType").child("SubExam").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                  if (adapter!=null){
                      list.clear();
                  }
                progressBar.setVisibility(View.GONE);
                  for (final DataSnapshot snapshot:dataSnapshot.getChildren()){

                          SubExamListItems listItems = new SubExamListItems(snapshot.child("mainexamname").getValue().toString(),snapshot.getKey(),snapshot.child("subexamname").getValue().toString());
                          list.add(listItems);


                  }

                  adapter = new SubExamAdapter(getActivity(),list);
                  recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(activity, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
