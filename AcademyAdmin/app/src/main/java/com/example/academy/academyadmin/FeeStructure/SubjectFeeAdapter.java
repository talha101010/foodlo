package com.example.academy.academyadmin.FeeStructure;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.academy.academyadmin.R;
import com.example.academy.academyadmin.Subjects.SubjectListItems;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class SubjectFeeAdapter extends RecyclerView.Adapter<SubjectFeeAdapter.ViewHolder> {
    private Context context;
    private List<SubjectFeeListitems> list;

    public SubjectFeeAdapter(Context context, List<SubjectFeeListitems> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.feestructure_listitems,viewGroup,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final SubjectFeeListitems listItems = list.get(i);
        viewHolder.classname.setText(listItems.getClassname());
        viewHolder.subjectname.setText(listItems.getSubjectname());
        viewHolder.subjectfee.setText(listItems.getSubjectfee());

        viewHolder.deletebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseDatabase.getInstance().getReference().child("Subjects").child(listItems.getSubjectid())
                        .child("subjectfee").removeValue(new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                        Toast.makeText(context, "subject fee has been deleted", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        viewHolder.editbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             AddFeeStructure.openDialog(false,listItems.getSubjectid(),listItems.getSubjectfee(),listItems.getSubjectname(),listItems.getClassname());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView classname,subjectname,subjectfee;
        ImageButton editbtn,deletebtn;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            classname = itemView.findViewById(R.id.classname);
            subjectname = itemView.findViewById(R.id.subjectname);
            subjectfee = itemView.findViewById(R.id.subjectfee);
            editbtn = itemView.findViewById(R.id.editbtn);
            deletebtn = itemView.findViewById(R.id.deletebtn);
        }
    }
}
