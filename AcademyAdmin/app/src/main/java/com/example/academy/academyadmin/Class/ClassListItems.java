package com.example.academy.academyadmin.Class;

public class ClassListItems {
    private String classid,classname;

    public ClassListItems(String classid, String classname) {
        this.classid = classid;
        this.classname = classname;
    }

    public String getClassid() {
        return classid;
    }

    public String getClassname() {
        return classname;
    }
}
