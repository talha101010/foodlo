package com.example.academy.academyadmin.Students;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.academy.academyadmin.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Results extends Fragment {
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private FloatingActionButton addresultbtn;
    private Spinner mainExamSpinner,subExamSpinner,subjectSpinner;
    private EditText totalmarks,obtainedmarks,brief_des;
    private ArrayList<String> mainExamList,subExamList,subjectList;
    private ArrayAdapter<String> mainAdapter,subAdapter,subjectAdapter;
    private DatabaseReference databaseReference;
    private String currentDate;
    private List<ResultListItems> list;
    private RecyclerView.Adapter adapter;
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.results, container, false);
        recyclerView = rootView.findViewById(R.id.resultrecyclerview);
        progressBar = rootView.findViewById(R.id.progressbar);
        addresultbtn = rootView.findViewById(R.id.add_fab);
        list = new ArrayList<>();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        addresultbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });
        currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        getResultsFromFirebase();

        return rootView;
    }

    private void openDialog(){
        final Dialog dialog = new Dialog(getActivity(),R.style.Theme_AppCompat_Light_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.addresult_dialog);
        mainExamSpinner = dialog.findViewById(R.id.mainexamspinner);
        subExamSpinner= dialog.findViewById(R.id.subexamspinner);
        subjectSpinner = dialog.findViewById(R.id.subjectspinner);
        totalmarks = dialog.findViewById(R.id.totalmarks);
        obtainedmarks = dialog.findViewById(R.id.obtainedmarks);
        brief_des = dialog.findViewById(R.id.brif_des);
        final ProgressBar progressBar1 = dialog.findViewById(R.id.progressbar);
        final ImageButton dialogButton =  dialog.findViewById(R.id.closebtn);
        Button addbtn = dialog.findViewById(R.id.addbtn);
        mainExamList = new ArrayList<>();
        subExamList = new ArrayList<>();
        subjectList = new ArrayList<>();
        mainAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item,mainExamList);
        subAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item,subExamList);
        subjectAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item,subjectList);
        getMainExam();
        getSubjects(progressBar1);

        mainExamSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                      subExamList.add(0,"Select Main Subject First");
                      subExamSpinner.setAdapter(subAdapter);
                }else{
                    getSubExam(progressBar1,mainExamList.get(mainExamSpinner.getSelectedItemPosition()));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mainExamSpinner.getSelectedItemPosition()==0){
                    Toast.makeText(getActivity(), "Please select main exam", Toast.LENGTH_SHORT).show();
                }else if (subExamSpinner.getSelectedItemPosition()==0){
                    Toast.makeText(getActivity(), "Please select sub exam", Toast.LENGTH_SHORT).show();
                }else if (subjectSpinner.getSelectedItemPosition()==0){
                    Toast.makeText(getActivity(), "Please select subject", Toast.LENGTH_SHORT).show();
                }else if (totalmarks.getText().length()==0){
                    totalmarks.setError("This field is required");
                }else if (obtainedmarks.getText().length()==0){
                    obtainedmarks.setError("This field is required");
                }else if(brief_des.getText().length() ==0){

                    brief_des.setError("Put about exam");
                }else {
                    addResult(dialog);
                }

            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void getMainExam(){
        databaseReference.child("ExamsType").child("MainExam").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (mainAdapter!=null){
                    mainExamList.clear();
                }
                mainExamList.add(0,"Please Select Main Exam");
                for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                    mainExamList.add(snapshot.child("examname").getValue().toString());
                }
                mainAdapter.notifyDataSetChanged();
                mainExamSpinner.setAdapter(mainAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getSubExam(final ProgressBar progressBar1, String name){
        progressBar1.setVisibility(View.VISIBLE);
       Query query = databaseReference.child("ExamsType").child("SubExam")
               .orderByChild("mainexamname").equalTo(name);
       query.addValueEventListener(new ValueEventListener() {
           @Override
           public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
               if (subAdapter!=null){
                   subExamList.clear();
               }
               progressBar1.setVisibility(View.GONE);
               subExamList.add(0,"Please Select Sub Exam");
               for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                   subExamList.add(snapshot.child("subexamname").getValue().toString());
               }
               subAdapter.notifyDataSetChanged();
               subExamSpinner.setAdapter(subAdapter);
           }

           @Override
           public void onCancelled(@NonNull DatabaseError databaseError) {

           }
       });
    }

    private void getSubjects(final ProgressBar progressBar1){
        databaseReference.child("Students").child(getActivity().getIntent().getStringExtra("studentid"))
                .child("Subjects").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (subjectAdapter!=null){
                    subjectList.clear();
                }
                progressBar1.setVisibility(View.GONE);
                subjectList.add(0,"Please Select Subject");
                for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                    subjectList.add(snapshot.child("name").getValue().toString());
                }
                subjectAdapter.notifyDataSetChanged();
                subjectSpinner.setAdapter(subjectAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void addResult(final Dialog dialog){
        progressBar.setVisibility(View.VISIBLE);
        AddResult_Listitems addresult = new AddResult_Listitems();
        addresult.setMainexam(mainExamList.get(mainExamSpinner.getSelectedItemPosition()));
        addresult.setSubexam(subExamList.get(subExamSpinner.getSelectedItemPosition()));
        addresult.setSubject(subjectList.get(subjectSpinner.getSelectedItemPosition()));
        addresult.setObtainedmarks(obtainedmarks.getText().toString().trim());
        addresult.setTotalmarks(totalmarks.getText().toString().trim());
        addresult.setDate(currentDate);
        addresult.setShort_brief(brief_des.getText().toString().trim());
        addresult.setStudentid(getActivity().getIntent().getStringExtra("studentid"));
        databaseReference.child("Results").push().setValue(addresult).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
               progressBar.setVisibility(View.GONE);
               dialog.dismiss();
                Toast.makeText(getActivity(), "Result has added", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getResultsFromFirebase(){
        progressBar.setVisibility(View.VISIBLE);
        Query query = databaseReference.child("Results").orderByChild("studentid")
                .equalTo(getActivity().getIntent().getStringExtra("studentid"));
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list.clear();
                if (adapter!=null){
                    list.size();
                }
                progressBar.setVisibility(View.GONE);
                for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                    ResultListItems listItems = new ResultListItems(snapshot.getKey(),
                            snapshot.child("mainexam").getValue().toString(),snapshot.child("subexam").getValue().toString(),
                            snapshot.child("subject").getValue().toString(),snapshot.child("date").getValue().toString(),
                            snapshot.child("totalmarks").getValue().toString(),snapshot.child("obtainedmarks").getValue().toString(),
                            snapshot.child("studentid").getValue().toString(),snapshot.child("short_brief").getValue().toString());

                    list.add(listItems);
                }
                adapter = new ResultAdapter(getActivity(),list);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
