package com.example.academy.academyadmin.Exams;

public class MainExamsListItems {
    private String examid,examname;

    public MainExamsListItems(String examid, String examname) {
        this.examid = examid;
        this.examname = examname;
    }

    public String getExamid() {
        return examid;
    }

    public String getExamname() {
        return examname;
    }
}
