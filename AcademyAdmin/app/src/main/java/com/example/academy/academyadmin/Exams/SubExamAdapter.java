package com.example.academy.academyadmin.Exams;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.academy.academyadmin.R;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class SubExamAdapter extends RecyclerView.Adapter<SubExamAdapter.ViewHolder> {
    private Context context;
    private List<SubExamListItems> list;

    public SubExamAdapter(Context context, List<SubExamListItems> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.subject_listitems,viewGroup,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
           final SubExamListItems listItems = list.get(i);
           viewHolder.mainexamname.setText(listItems.getMainexamname());
           viewHolder.subexamname.setText(listItems.getSubexamname());

           viewHolder.deletebtn.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   FirebaseDatabase.getInstance().getReference().child("ExamsType").child("SubExam")
                           .child(listItems.getSubexamid())
                           .removeValue(new DatabaseReference.CompletionListener() {
                               @Override
                               public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                   Toast.makeText(context, "Exam has been deleted", Toast.LENGTH_SHORT).show();
                               }
                           });
               }
           });

           viewHolder.editbtn.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView mainexamname,subexamname;
        ImageButton editbtn,deletebtn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mainexamname = itemView.findViewById(R.id.classname);
            subexamname = itemView.findViewById(R.id.subjectname);
            editbtn = itemView.findViewById(R.id.editbtn);
            deletebtn = itemView.findViewById(R.id.deletebtn);
        }
    }
}
