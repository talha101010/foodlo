package com.example.academy.academyadmin.Students;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.academy.academyadmin.R;

import java.util.List;

public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.ViewHolder> {
    private Context context;
    private List<ResultListItems> list;

    public ResultAdapter(Context context, List<ResultListItems> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.result_listitems,viewGroup,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
          ResultListItems listItems = list.get(i);
          viewHolder.mainexam.setText(listItems.getMainexam());
        viewHolder.subexam.setText(listItems.getSubexam());
        viewHolder.subject.setText(listItems.getSubject());
        viewHolder.totalmarks.setText(listItems.getTotalmarks());
        viewHolder.obtainedmarks.setText(listItems.getObtainedmarks());
        viewHolder.date.setText(listItems.getDate());
        viewHolder.shortbrief.setText(listItems.getShort_brief());

        float totalMarksf = Float.parseFloat(listItems.getTotalmarks());
        float obtainedMarkf = Float.parseFloat(listItems.getObtainedmarks());

        float percent = (totalMarksf/obtainedMarkf)*100;

        if (percent <50.0){

            viewHolder.statusofMarks.setText("fail");
            viewHolder.card2.setCardBackgroundColor(Color.RED);
            viewHolder.statusofMarks.setTextColor(Color.WHITE);
        }else {

            viewHolder.statusofMarks.setText("pass");
            viewHolder.card2.setCardBackgroundColor(Color.GREEN);

        }



        viewHolder.percent.setText(percent+"%");


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView mainexam,subexam,subject,totalmarks,obtainedmarks,date,shortbrief,percent,statusofMarks;
        CardView card2;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mainexam = itemView.findViewById(R.id.mainexam);
            subexam = itemView.findViewById(R.id.subexam);
            subject = itemView.findViewById(R.id.subject);
            totalmarks = itemView.findViewById(R.id.totalmarks);
            obtainedmarks = itemView.findViewById(R.id.obtainedmarks);
            date = itemView.findViewById(R.id.date);
            shortbrief = itemView.findViewById(R.id.shortbrief);
            percent =itemView.findViewById(R.id.percentmarks);
            statusofMarks = itemView.findViewById(R.id.statusofresult);
            card2 = itemView.findViewById(R.id.card2);
        }
    }
}
