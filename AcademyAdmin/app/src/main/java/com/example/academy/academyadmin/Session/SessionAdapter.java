package com.example.academy.academyadmin.Session;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.academy.academyadmin.R;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class SessionAdapter extends RecyclerView.Adapter<SessionAdapter.ViewHolder> {
    private Context context;
    private List<SessionListItems> list;

    public SessionAdapter(Context context, List<SessionListItems> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.class_list_items,viewGroup,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final SessionListItems listItems = list.get(i);
        viewHolder.sessionname.setText(listItems.getSessionname());

        viewHolder.deletebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseDatabase.getInstance().getReference().child("Sessions").child(listItems.getSessionid())
                        .removeValue(new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                Toast.makeText(context, "Session has been deleted", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });

        viewHolder.editbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddSession.openDialog(false,listItems.getSessionid(),listItems.getSessionname());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView sessionname;
        ImageButton deletebtn,editbtn;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            sessionname = itemView.findViewById(R.id.classname);
            deletebtn = itemView.findViewById(R.id.deletebtn);
            editbtn = itemView.findViewById(R.id.editbtn);
        }
    }
}
