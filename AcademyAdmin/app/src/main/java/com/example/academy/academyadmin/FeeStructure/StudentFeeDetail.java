package com.example.academy.academyadmin.FeeStructure;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.example.academy.academyadmin.R;

public class StudentFeeDetail extends AppCompatActivity {
    private TextView rollno,sname,fname,classname,session,address,date,fee,fine;
    private Button connectbtn,printbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_fee_detail);
        getViews();
        rollno.setText(getIntent().getStringExtra("rollno"));
        sname.setText(getIntent().getStringExtra("studentname"));
        fname.setText(getIntent().getStringExtra("fathername"));
        classname.setText(getIntent().getStringExtra("classid"));
        session.setText(getIntent().getStringExtra("studentname"));
        sname.setText(getIntent().getStringExtra("studentname"));
    }
    private void getViews(){
        rollno = findViewById(R.id.rollno);
        sname = findViewById(R.id.sname);
        fname = findViewById(R.id.fname);
        classname = findViewById(R.id.classname);
        session = findViewById(R.id.session);
        address = findViewById(R.id.address);
        date = findViewById(R.id.date);
        fee = findViewById(R.id.fee);
        fine = findViewById(R.id.fine);
        connectbtn = findViewById(R.id.Scan);
        printbtn = findViewById(R.id.mPrint);
    }
}
