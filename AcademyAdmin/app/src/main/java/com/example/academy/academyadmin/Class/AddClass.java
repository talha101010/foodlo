package com.example.academy.academyadmin.Class;

import android.app.Activity;
import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.academy.academyadmin.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class AddClass extends AppCompatActivity {
    private FloatingActionButton addclassbtn;
    public static EditText classname;
    public static DatabaseReference databaseReference;
    private RecyclerView recyclerView;
    private List<ClassListItems> list;
    private RecyclerView.Adapter adapter;
    public static ProgressBar progressBar;
    public static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_class);
        activity = AddClass.this;
        getViews();
        addclassbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog(true,"1","1");
            }
        });
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        getClassesFromFirebase();

    }
    private void getViews(){
        addclassbtn = findViewById(R.id.add_fab);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        recyclerView = findViewById(R.id.classrecyclerview);
        list = new ArrayList<>();
        progressBar = findViewById(R.id.progressbar);
    }

    public static void openDialog(final boolean check, final String id,String cname){
        final Dialog dialog = new Dialog(activity,R.style.Theme_AppCompat_Light_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.addclass_dialog);
        classname = dialog.findViewById(R.id.classnameedittext);
        ImageButton dialogButton =  dialog.findViewById(R.id.closebtn);
        Button addbtn = dialog.findViewById(R.id.addbtn);
        if (check){
            addbtn.setText("Add Now");
        }else{
            classname.setText(cname);
            addbtn.setText("Update Now");
        }
        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (classname.getText().length()==0){
                    classname.setError("This field is required");
                }else{
                    if (check){
                        addClassToFirebase(dialog);
                    }else{
                        progressBar.setVisibility(View.VISIBLE);
                        updateClassToFirebase(id,dialog);
                    }

                }

            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static void addClassToFirebase(final Dialog dialog){
        progressBar.setVisibility(View.VISIBLE);
        AddClass_Firebase addclass = new AddClass_Firebase();
        addclass.setClassname(classname.getText().toString().trim());
        databaseReference.child("Classes").push().setValue(addclass)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        dialog.dismiss();
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(activity, "Class has been added", Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(activity, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void updateClassToFirebase(String id, final Dialog dialog){
        databaseReference.child("Classes").child(id).child("classname").setValue(classname.getText().toString().trim())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        dialog.dismiss();
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(activity, "Class has been updated", Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void getClassesFromFirebase(){
        databaseReference.child("Classes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (adapter != null){
                    list.clear();
                }
                progressBar.setVisibility(View.GONE);
                for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                    ClassListItems listItems = new ClassListItems(snapshot.getKey(),snapshot.child("classname").getValue().toString());
                    list.add(listItems);
                }
                adapter = new ClassAdapter(list,AddClass.this);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
