package com.example.academy.academyadmin.Students;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.academy.academyadmin.R;

import java.util.List;

public class FeeAdapter extends RecyclerView.Adapter<FeeAdapter.ViewHolder> {
    private Context context;
    private List<FeeListItems> list;

    public FeeAdapter(Context context, List<FeeListItems> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
     View v = LayoutInflater.from(context).inflate(R.layout.fee_listitems,viewGroup,false);
     return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        FeeListItems feeListItems = list.get(i);
        viewHolder.feedate.setText(feeListItems.getFeedate());
        viewHolder.fee.setText(feeListItems.getFee());
        viewHolder.fine.setText(feeListItems.getFine());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView feedate,fee,fine;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            feedate = itemView.findViewById(R.id.feedate);
            fee = itemView.findViewById(R.id.fee);
            fine = itemView.findViewById(R.id.fine);
        }
    }
}
