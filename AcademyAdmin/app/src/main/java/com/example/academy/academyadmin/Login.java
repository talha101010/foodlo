package com.example.academy.academyadmin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Login extends AppCompatActivity {
    private EditText email,password;
    private Button signin;
    private FirebaseAuth mAuth;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getViews();

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email.getText().length()==0){
                    email.setError("This field is required");
                }else if (password.getText().length()==0){
                    password.setError("This field is required");
                }else{
                  progressDialog.setMessage("Authenticating");
                  progressDialog.setCanceledOnTouchOutside(false);
                  progressDialog.show();
                  login();
                }
            }
        });
    }
    private void getViews(){
        email = findViewById(R.id.emailtext);
        password = findViewById(R.id.passwordtext);
        signin = findViewById(R.id.loginbtn);
        mAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);
    }

    private void login(){
         mAuth.signInWithEmailAndPassword(email.getText().toString().trim(),password.getText().toString().trim())
                 .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                     @Override
                     public void onComplete(@NonNull Task<AuthResult> task) {
                         if (task.isSuccessful()){
                             progressDialog.dismiss();
                             Toast.makeText(Login.this, "Logged in successfully", Toast.LENGTH_SHORT).show();
                             startActivity(new Intent(Login.this,MainActivity.class));
                             finish();
                         }else{
                             progressDialog.dismiss();
                             Toast.makeText(Login.this, "Authentication failed", Toast.LENGTH_SHORT).show();
                         }
                     }
                 });
    }
}
