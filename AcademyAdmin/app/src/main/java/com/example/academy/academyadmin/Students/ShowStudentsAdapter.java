package com.example.academy.academyadmin.Students;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.academy.academyadmin.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ShowStudentsAdapter extends RecyclerView.Adapter<ShowStudentsAdapter.ViewHolder> {
    private Context context;
    public static List<ShowStudentsListitems> list;
    private boolean check;
    private String monthNumber,dateyear;
    private ArrayList<String> presentid,absentid,leaveid;
    HashMap<String, String> hashMapPresent;
    HashMap<String, String> hashMapAbsent;
    HashMap<String, String> hashMapLeave;

    public ShowStudentsAdapter(Context context, List<ShowStudentsListitems> list,boolean check) {
        this.context = context;
        this.list = list;
        this.check = check;
        presentid = new ArrayList<>();
        absentid = new ArrayList<>();
        leaveid = new ArrayList<>();
         hashMapPresent = new HashMap<String, String>();
        hashMapAbsent = new HashMap<String, String>();
        hashMapLeave = new HashMap<String, String>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
      View v = LayoutInflater.from(context).inflate(R.layout.showstudents_listitems,viewGroup,false);
      return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
      final ShowStudentsListitems listitems = list.get(i);
      viewHolder.studentname.setText(listitems.getStudentname());
      viewHolder.fathername.setText(listitems.getFname());
      viewHolder.rollno.setText(listitems.getRollno());
      viewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              Intent intent = new Intent(context,StudentDetail.class);
              intent.putExtra("studentname",listitems.getStudentname());
              intent.putExtra("fathername",listitems.getFname());
              intent.putExtra("contactnumber",listitems.getContactnumber());
              intent.putExtra("fcontactnumber",listitems.getFcontactnumber());
              intent.putExtra("cnic",listitems.getCnic());
              intent.putExtra("dob",listitems.getDob());
              intent.putExtra("address",listitems.getAddress());
              intent.putExtra("gender",listitems.getGender());
              intent.putExtra("classid",listitems.getClassid());
              intent.putExtra("studentid",listitems.getStudentid());
              intent.putExtra("rollno",listitems.getRollno());
              intent.putExtra("insti",listitems.getInstitutename());
              context.startActivity(intent);
          }
      });

        if (listitems.getNextdate().equals("null")){
            viewHolder.feestatus.setText("Fee Pending");
            viewHolder.feestatus.setBackgroundColor(Color.parseColor("#8B0000"));
        }else{
            String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            try {
                Date cdate = sdf.parse(currentDate);
                Date nextdate = sdf.parse(listitems.getNextdate());
                if (cdate.compareTo(nextdate)>0){
                    viewHolder.feestatus.setText("Fee Pending");
                    viewHolder.feestatus.setBackgroundColor(Color.parseColor("#8B0000"));
                }else if (cdate.compareTo(nextdate)<0){
                    viewHolder.feestatus.setText("Fee Paid");
                    viewHolder.feestatus.setBackgroundColor(Color.parseColor("#006400"));
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


      if (check){
          viewHolder.callbtn.setVisibility(View.VISIBLE);
          viewHolder.smsbtn.setVisibility(View.VISIBLE);
          viewHolder.present.setVisibility(View.GONE);
          viewHolder.absent.setVisibility(View.GONE);
          viewHolder.leave.setVisibility(View.GONE);

      }else{
          viewHolder.present.setVisibility(View.VISIBLE);
          viewHolder.absent.setVisibility(View.VISIBLE);
          viewHolder.leave.setVisibility(View.VISIBLE);
          viewHolder.callbtn.setVisibility(View.GONE);
          viewHolder.smsbtn.setVisibility(View.GONE);
          final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Students");
          MarkAttendance.attendancebtn.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {

                      for (int i=0;i<presentid.size();i++){
                          if (hashMapPresent.get(presentid.get(i)).equals("null")){
                              databaseReference.child(presentid.get(i)).child("Attendance").child(monthNumber+"-"+dateyear).child("present").setValue("1");
                              databaseReference.child(presentid.get(i)).child("Attendance").child(monthNumber+"-"+dateyear).child("absent").setValue("0");
                              databaseReference.child(presentid.get(i)).child("Attendance").child(monthNumber+"-"+dateyear).child("leave").setValue("0");
                          }else{
                              databaseReference.child(presentid.get(i)).child("Attendance").child(monthNumber+"-"+dateyear).child("present").setValue(hashMapPresent.get(presentid.get(i)));
                          }
                      }
                      for (int j=0;j<absentid.size();j++){
                          if (hashMapAbsent.get(absentid.get(j)).equals("null")){
                              databaseReference.child(absentid.get(j)).child("Attendance").child(monthNumber+"-"+dateyear).child("present").setValue("0");
                              databaseReference.child(absentid.get(j)).child("Attendance").child(monthNumber+"-"+dateyear).child("absent").setValue("1");
                              databaseReference.child(absentid.get(j)).child("Attendance").child(monthNumber+"-"+dateyear).child("leave").setValue("0");
                          }else{
                              databaseReference.child(absentid.get(j)).child("Attendance").child(monthNumber+"-"+dateyear).child("absent").setValue(hashMapAbsent.get(absentid.get(j)));
                          }

                      }

                  for (int k=0;k<leaveid.size();k++){
                      if (hashMapLeave.get(leaveid.get(k)).equals("null")){
                          databaseReference.child(leaveid.get(k)).child("Attendance").child(monthNumber+"-"+dateyear).child("present").setValue("0");
                          databaseReference.child(leaveid.get(k)).child("Attendance").child(monthNumber+"-"+dateyear).child("absent").setValue("0");
                          databaseReference.child(leaveid.get(k)).child("Attendance").child(monthNumber+"-"+dateyear).child("leave").setValue("1");
                      }else{
                          databaseReference.child(leaveid.get(k)).child("Attendance").child(monthNumber+"-"+dateyear).child("leave").setValue(hashMapLeave.get(leaveid.get(k)));
                      }

                  }
                  Toast.makeText(context, "Attendance has been marked", Toast.LENGTH_SHORT).show();
              }
          });
      }

      viewHolder.callbtn.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + listitems.getContactnumber()));
              context.startActivity(intent);
          }
      });

      viewHolder.smsbtn.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {

              final Dialog dialog = new Dialog(context,R.style.Theme_AppCompat_Light_Dialog_Alert);
              dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
              dialog.setCancelable(false);
              dialog.setContentView(R.layout.sms_dialog);
             final EditText sms = dialog.findViewById(R.id.smsedittext);
             final String phone = listitems.getContactnumber();
              Button dialogButton =  dialog.findViewById(R.id.closebtn);
              Button addbtn = dialog.findViewById(R.id.proceddbtn);

              addbtn.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                      if (sms.getText().length()==0){
                          sms.setError("This field is required");
                      }else{
                          sendSMS(phone,sms.getText().toString().trim(),dialog);
                      }

                  }
              });
              dialogButton.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                      dialog.dismiss();
                  }
              });

              dialog.show();


          }
      });

       monthNumber = (String) DateFormat.format("MM", new Date());
       dateyear = (String) DateFormat.format("yyyy", new Date());

       viewHolder.present.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
           @Override
           public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
               if (isChecked){
                   if (listitems.getAttendanceYear().equals("null")){
                       presentid.add(listitems.getStudentid());
                       hashMapPresent.put(listitems.getStudentid(),"null");
                   }else{
                       presentid.add(listitems.getStudentid());
                       int present = Integer.parseInt(listitems.getPresent());
                       present = present+1;
                       hashMapPresent.put(listitems.getStudentid(),present+"");
                   }

               }else{
                   presentid.remove(listitems.getStudentid());
                   hashMapPresent.remove(listitems.getStudentid());
               }
           }
       });

       viewHolder.absent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
           @Override
           public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
               if (isChecked){
                   if (listitems.getAttendanceYear().equals("null")){
                       absentid.add(listitems.getStudentid());
                       hashMapAbsent.put(listitems.getStudentid(),"null");
                   }else{
                       absentid.add(listitems.getStudentid());
                       int absent = Integer.parseInt(listitems.getAbsent());
                       absent = absent+1;
                       hashMapAbsent.put(listitems.getStudentid(),absent+"");
                   }
               }else{
                     absentid.remove(listitems.getStudentid());
                     hashMapAbsent.remove(listitems.getStudentid());
               }

           }
       });

       viewHolder.leave.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
           @Override
           public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
               if (isChecked){
                   if (listitems.getAttendanceYear().equals("null")){
                       leaveid.add(listitems.getStudentid());
                       hashMapLeave.put(listitems.getStudentid(),"null");
                   }else{
                       leaveid.add(listitems.getStudentid());
                       int leave = Integer.parseInt(listitems.getLeave());
                       leave = leave+1;
                       hashMapLeave.put(listitems.getStudentid(),leave+"");
                   }
               }else{
                   leaveid.remove(listitems.getStudentid());
                   hashMapLeave.remove(listitems.getStudentid());
               }
           }
       });

    }

    private void sendSMS(final String phone, final String message, final Dialog dialog){
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        String url = "http://www.thebhakkar.com/academy/singlesms.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (!jsonObject.getBoolean("error")){
                                Toast.makeText(context, jsonObject.getString("message")+"", Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }else{
                                Toast.makeText(context, jsonObject.getString("message")+"", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(context, error.getMessage()+"", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("phone",phone);
                param.put("message",message);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView studentname,fathername,rollno,feestatus;
        ImageButton callbtn,smsbtn;
        RadioButton present,absent,leave;
        RelativeLayout relativeLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            studentname = itemView.findViewById(R.id.studentname);
            fathername = itemView.findViewById(R.id.fathername);
            callbtn = itemView.findViewById(R.id.callbtn);
            smsbtn = itemView.findViewById(R.id.smsbtn);
            relativeLayout = itemView.findViewById(R.id.relativelayout);
            present = itemView.findViewById(R.id.presentbtn);
            absent = itemView.findViewById(R.id.absentbtn);
            leave = itemView.findViewById(R.id.Leavebtn);
            rollno = itemView.findViewById(R.id.rollno);
            feestatus = itemView.findViewById(R.id.feestatus);
        }
    }

    public void setFilter(List<ShowStudentsListitems> listitems1){
          list = new ArrayList<>();
          list.addAll(listitems1);
          notifyDataSetChanged();
    }

}
