package com.example.academy.academyadmin.Exams;

public class SubExamListItems {
    private String mainexamname,subexamid,subexamname;

    public SubExamListItems(String mainexamname, String subexamid, String subexamname) {
        this.subexamid = subexamid;
        this.subexamname = subexamname;
        this.mainexamname = mainexamname;
    }

    public String getSubexamid() {
        return subexamid;
    }

    public String getSubexamname() {
        return subexamname;
    }

    public String getMainexamname() {
        return mainexamname;
    }
}
