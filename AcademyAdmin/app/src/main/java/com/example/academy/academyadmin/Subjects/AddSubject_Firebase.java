package com.example.academy.academyadmin.Subjects;

public class AddSubject_Firebase {
    private String classid,subjectname;

    public AddSubject_Firebase() {
    }

    public String getClassid() {
        return classid;
    }

    public void setClassid(String classid) {
        this.classid = classid;
    }

    public String getSubjectname() {
        return subjectname;
    }

    public void setSubjectname(String subjectname) {
        this.subjectname = subjectname;
    }
}
