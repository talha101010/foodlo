package com.example.academy.academyadmin.FeeStructure;

public class RegistrationFeeListItems {
    private String classid,classname,fee;

    public RegistrationFeeListItems(String classid, String classname, String fee) {
        this.classid = classid;
        this.classname = classname;
        this.fee = fee;
    }

    public String getClassid() {
        return classid;
    }

    public String getClassname() {
        return classname;
    }

    public String getFee() {
        return fee;
    }
}
