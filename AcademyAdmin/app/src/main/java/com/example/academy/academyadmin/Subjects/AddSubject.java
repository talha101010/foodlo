package com.example.academy.academyadmin.Subjects;

import android.app.Activity;
import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.academy.academyadmin.R;
import com.example.academy.academyadmin.Session.SessionAdapter;
import com.example.academy.academyadmin.Session.SessionListItems;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class AddSubject extends AppCompatActivity {
    private FloatingActionButton addsubjectbtn;
    public static EditText subjectname;
    public static Spinner classnamespinner;
    public static Activity activity;
    public static ArrayList<String> classnameList;
    public static ArrayAdapter<String> classAdapter;
    public static DatabaseReference databaseReference;
    public static ProgressBar progressBar;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<SubjectListItems> list;
    private Spinner classSpinner;
    private ArrayList<String> classList;
    private ArrayAdapter<String> classSpinnerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_subject);
        activity = AddSubject.this;
        getViews();
        addsubjectbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 openDialog(true,"1","1","1");
            }
        });

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        classSpinnerAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,classList);
        getClasses();
        classSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    getSubjectsFromFirebase();
                }else{
                    getSubjectsFromFirebaseSearch();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }
    private void getViews(){
        addsubjectbtn = findViewById(R.id.add_fab);
        classnameList = new ArrayList<>();
      //  classidList = new ArrayList<>();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        progressBar = findViewById(R.id.progressbar);
        list = new ArrayList<>();
        recyclerView = findViewById(R.id.subjectsrecyclerview);
        classSpinner = findViewById(R.id.classspinner);
        classList = new ArrayList<>();
    }

    public static void openDialog(final boolean check, final String id,String sname,String classname){
        final Dialog dialog = new Dialog(activity,R.style.Theme_AppCompat_Light_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.addsubject_dialog);
        subjectname = dialog.findViewById(R.id.classnameedittext);
        classnamespinner = dialog.findViewById(R.id.subjectclassspinner);
       ProgressBar classprogressbar = dialog.findViewById(R.id.progressbar);
        ImageButton dialogButton =  dialog.findViewById(R.id.closebtn);
        Button addbtn = dialog.findViewById(R.id.addbtn);
        classAdapter = new ArrayAdapter<String>(activity,android.R.layout.simple_spinner_dropdown_item,classnameList);
        getClassesFromFirebase(classprogressbar,classname,check);
        if (check){
            addbtn.setText("Add Now");
        }else{
            subjectname.setText(sname);
            addbtn.setText("Update Now");

        }
        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (classnamespinner.getSelectedItemPosition()==0){
                    Toast.makeText(activity, "please select class", Toast.LENGTH_SHORT).show();
                }
                if (subjectname.getText().length()==0){
                    subjectname.setError("This field is required");
                }else{
                    if (check){
                        addSubjectToFirebase(dialog);
                    }else{
                        progressBar.setVisibility(View.VISIBLE);
                        updateSubjectToFirebase(id,dialog);
                    }

                }

            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static void updateSubjectToFirebase(String id, final Dialog dialog){
        databaseReference.child("Subjects").child(id).child("subjectname").setValue(subjectname.getText().toString().trim());
        databaseReference.child("Subjects").child(id).child("classid").setValue(classnameList.get(classnamespinner.getSelectedItemPosition()));
        Toast.makeText(activity, "Subject has been updated", Toast.LENGTH_SHORT).show();
        dialog.dismiss();
    }

    public static void addSubjectToFirebase(final Dialog dialog){
        progressBar.setVisibility(View.VISIBLE);
        AddSubject_Firebase addsubject = new AddSubject_Firebase();
        addsubject.setClassid(classnameList.get(classnamespinner.getSelectedItemPosition()));
        addsubject.setSubjectname(subjectname.getText().toString().trim());
        databaseReference.child("Subjects").push().setValue(addsubject)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        dialog.dismiss();
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(activity, "Subject has been added", Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(activity, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void getClassesFromFirebase(final ProgressBar progressBar1, final String classname1, final boolean check){
        databaseReference.child("Classes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (classAdapter!=null){
                    classnameList.clear();
                   // classidList.clear();
                }
                progressBar1.setVisibility(View.GONE);
                classnameList.add(0,"Please select class");
               // classidList.add(0,"classid");
                for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                    classnameList.add(snapshot.child("classname").getValue().toString());
                 //   classidList.add(snapshot.getKey().toString());
                }
                classAdapter.notifyDataSetChanged();
                classnamespinner.setAdapter(classAdapter);
                if (!check){
                    classnamespinner.setSelection(classAdapter.getPosition(classname1));
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getClasses(){
        databaseReference.child("Classes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (classSpinnerAdapter!=null){
                    classList.clear();
                    // classidList.clear();
                }
                classList.add(0,"Search By Class");
                // classidList.add(0,"classid");
                for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                    classList.add(snapshot.child("classname").getValue().toString());
                    //   classidList.add(snapshot.getKey().toString());
                }
                classSpinnerAdapter.notifyDataSetChanged();
                classSpinner.setAdapter(classSpinnerAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getSubjectsFromFirebase(){
        progressBar.setVisibility(View.VISIBLE);
        databaseReference.child("Subjects").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (adapter != null){
                    list.clear();
                }
                progressBar.setVisibility(View.GONE);
                for (final DataSnapshot snapshot:dataSnapshot.getChildren()){

                  //  String value = dataSnapshot.child(snapshot.child("classid").getValue().toString()).child("classname").getValue().toString();
                    SubjectListItems listItems = new SubjectListItems(snapshot.child("classid").getValue().toString(),snapshot.getKey(),snapshot.child("subjectname").getValue().toString(),snapshot.child("classid").getValue().toString());
                    list.add(listItems);

                }
                adapter = new SubjectAdapter(AddSubject.this,list);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getSubjectsFromFirebaseSearch(){
        progressBar.setVisibility(View.VISIBLE);
        Query query = databaseReference.child("Subjects").orderByChild("classid")
                .equalTo(classList.get(classSpinner.getSelectedItemPosition()));
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (adapter != null){
                    list.clear();
                }
                progressBar.setVisibility(View.GONE);
                if (dataSnapshot.exists()){
                    for (final DataSnapshot snapshot:dataSnapshot.getChildren()){

                        //  String value = dataSnapshot.child(snapshot.child("classid").getValue().toString()).child("classname").getValue().toString();
                        SubjectListItems listItems = new SubjectListItems(snapshot.child("classid").getValue().toString(),snapshot.getKey(),snapshot.child("subjectname").getValue().toString(),snapshot.child("classid").getValue().toString());
                        list.add(listItems);

                    }
                }else{
                    Toast.makeText(AddSubject.this, "No subject available", Toast.LENGTH_SHORT).show();
                }

                adapter = new SubjectAdapter(AddSubject.this,list);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
