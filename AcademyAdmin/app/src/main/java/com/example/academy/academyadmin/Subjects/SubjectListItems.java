package com.example.academy.academyadmin.Subjects;

public class SubjectListItems {
    private String classid,subjectid,subjectname,classname;

    public SubjectListItems(String classid, String subjectid, String subjectname,String classname) {
        this.classid = classid;
        this.subjectid = subjectid;
        this.subjectname = subjectname;
        this.classname = classname;
    }

    public String getClassid() {
        return classid;
    }

    public String getSubjectid() {
        return subjectid;
    }

    public String getSubjectname() {
        return subjectname;
    }

    public String getClassname() {
        return classname;
    }
}
