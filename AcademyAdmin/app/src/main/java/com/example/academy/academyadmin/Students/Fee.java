package com.example.academy.academyadmin.Students;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.academy.academyadmin.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Fee extends Fragment {
    private RecyclerView recyclerView;
    private FloatingActionButton addfeebtn;
    private String currentDate;
    private TextView totalfee;
    private DatabaseReference databaseReference;
    private ProgressBar progressBar;
    private EditText fee,fine;
    private String monthNumber,dateyear,nextdate;
    private List<FeeListItems> list;
    private RecyclerView.Adapter adapter;
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fee, container, false);
        recyclerView = rootView.findViewById(R.id.feerecyclerview);
        addfeebtn = rootView.findViewById(R.id.add_fab);
        list = new ArrayList<>();
        progressBar = rootView.findViewById(R.id.progressbar);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
           int month,year;
        monthNumber = (String) DateFormat.format("MM", new Date());
        dateyear = (String) DateFormat.format("yyyy", new Date());
        month = Integer.parseInt(monthNumber) + 1;
        year = Integer.parseInt(dateyear) + 1;
        if (Integer.parseInt(monthNumber)==12){
            nextdate = "05-01-"+year+"";
        }else{
            nextdate = "05-"+month+"-"+dateyear;
        }
        addfeebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               openDialog();
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        getFee();

        return rootView;
    }

    private void openDialog(){
        final Dialog dialog = new Dialog(getActivity(),R.style.Theme_AppCompat_Light_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.addfee_dialog);
        totalfee = dialog.findViewById(R.id.totalfee);
        fee = dialog.findViewById(R.id.fee);
        fine = dialog.findViewById(R.id.fine);
        totalfee.setText(BasicInfo.totalfee1+"");
        ImageButton dialogButton =  dialog.findViewById(R.id.closebtn);
        Button addbtn = dialog.findViewById(R.id.addbtn);

        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fee.getText().length()==0){
                    fee.setError("This field is required");
                }else if (fine.getText().length()==0){
                    fine.setError("This field is required");
                }else{
                    addFee();
                    dialog.dismiss();
                }

            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void addFee(){
       progressBar.setVisibility(View.VISIBLE);
       addFee_Firebase addFee = new addFee_Firebase();
       addFee.setFee(fee.getText().toString().trim());
       addFee.setFine(fine.getText().toString().trim());
       addFee.setFeedate(currentDate);
       addFee.setStudentid(getActivity().getIntent().getStringExtra("studentid"));
       databaseReference.child("Fee").push().setValue(addFee).addOnSuccessListener(new OnSuccessListener<Void>() {
           @Override
           public void onSuccess(Void aVoid) {
             progressBar.setVisibility(View.GONE);
             databaseReference.child("Students").child(getActivity().getIntent().getStringExtra("studentid")).child("nextfee").setValue(nextdate);
               Toast.makeText(getActivity(), "Fee submitted successfully", Toast.LENGTH_SHORT).show();
           }
       });
    }

    private void getFee(){
        Query query = databaseReference.child("Fee").orderByChild("studentid")
                .equalTo(getActivity().getIntent().getStringExtra("studentid"));
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                  if (adapter!=null){
                      list.clear();
                  }
                  progressBar.setVisibility(View.GONE);
                  for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                      FeeListItems listItems = new FeeListItems(snapshot.getKey(),snapshot.child("studentid").getValue().toString(),
                              snapshot.child("fee").getValue().toString(),snapshot.child("fine").getValue().toString(),
                              snapshot.child("feedate").getValue().toString());
                      list.add(listItems);
                  }
                  adapter = new FeeAdapter(getActivity(),list);
                  recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
