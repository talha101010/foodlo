package com.example.academy.academyadmin.FeeStructure;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.academy.academyadmin.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class AddFeeStructure extends AppCompatActivity {
    private RecyclerView recyclerView;
    private Button subjectfeebtn,registrationfeebtn;
    public static Activity activity;
    public static Spinner subjectspinner,classspinner;
    public static EditText subjectfee;
    public static ArrayList<String> subjectList,subjectidList,classList,classIdList;
    public static ArrayAdapter<String> subjectAdapter;
    public static ArrayAdapter<String> classAdapter;
    public static DatabaseReference databaseReference;
    public static ProgressBar progressBar;
    private RecyclerView.Adapter adapter;
    private List<SubjectFeeListitems> list;
    private Spinner searchClassSpinner;
    private ArrayList<String> searchClassList;
    private ArrayAdapter<String> searchClassAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_fee_structure);
        activity = AddFeeStructure.this;
        getViews();
        subjectfeebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               openDialog(true,"1","1","1","1");
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        searchClassAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,searchClassList);
        getSearchClassFromFirebase();
        registrationfeebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AddFeeStructure.this,RegistrationFee.class));
            }
        });

        searchClassSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    getSubjectsFromFirebase();
                }else{
                    getSearchSubjectsFromFirebase();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getViews(){
        subjectfeebtn = findViewById(R.id.addsubjectfeebtn);
        registrationfeebtn = findViewById(R.id.addregistrationfee);
        recyclerView = findViewById(R.id.subjectfeerecyclerview);
        subjectList = new ArrayList<>();
        subjectidList = new ArrayList<>();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        progressBar = findViewById(R.id.progressbar);
        list = new ArrayList<>();
        classList = new ArrayList<>();
        classIdList = new ArrayList<>();
        searchClassSpinner = findViewById(R.id.classSpinner);
        searchClassList = new ArrayList<>();
    }

    public static void openDialog(final boolean check, final String id, String sfee, final String sname,String cname){
        final Dialog dialog = new Dialog(activity,R.style.Theme_AppCompat_Light_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.addsubjectfee_dialog);
        subjectfee = dialog.findViewById(R.id.subjectfee);
        subjectspinner = dialog.findViewById(R.id.subjectspinner);
        classspinner = dialog.findViewById(R.id.classspinner);
        final ProgressBar classprogressbar = dialog.findViewById(R.id.progressbar);
        ImageButton dialogButton =  dialog.findViewById(R.id.closebtn);
        Button addbtn = dialog.findViewById(R.id.addbtn);
        classAdapter = new ArrayAdapter<String>(activity,android.R.layout.simple_spinner_dropdown_item,classList);
        subjectAdapter = new ArrayAdapter<String>(activity,android.R.layout.simple_spinner_dropdown_item,subjectList);
        classspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    if (subjectAdapter!=null){
                        subjectList.clear();
                        subjectidList.clear();
                    }
                    subjectList.add(0,"Please Select Class First");
                    subjectspinner.setAdapter(subjectAdapter);
                }else{
                          getSubjectsFromFirebase(classprogressbar,sname,check);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        getClassFromFirebase(classprogressbar,cname,check);
        if (check){
            addbtn.setText("Add Now");
        }else{
            subjectfee.setText(sfee);
            addbtn.setText("Update Now");

        }
        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (subjectspinner.getSelectedItemPosition()==0){
                    Toast.makeText(activity, "please select subject", Toast.LENGTH_SHORT).show();
                }
                if (subjectfee.getText().length()==0){
                    subjectfee.setError("This field is required");
                }else{
                    if (check){
                        addSubjectFeeToFirebase(dialog);
                    }else{
                        progressBar.setVisibility(View.VISIBLE);
                        updateFeeToFirebase(id,dialog);
                    }

                }

            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static void addSubjectFeeToFirebase(final Dialog dialog){
        progressBar.setVisibility(View.VISIBLE);
        databaseReference.child("Subjects").child(subjectidList.get(subjectspinner.getSelectedItemPosition()))
                .child("subjectfee").setValue(subjectfee.getText().toString().trim())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        dialog.dismiss();
                        Toast.makeText(activity, "Subject fee has been added", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public static void updateFeeToFirebase(String id, final Dialog dialog){
        databaseReference.child("Subjects").child(id).child("subjectname").setValue(subjectList.get(subjectspinner.getSelectedItemPosition()));
        databaseReference.child("Subjects").child(id).child("subjectfee").setValue(subjectfee.getText().toString().trim());
        Toast.makeText(activity, "Subject Fee has been updated", Toast.LENGTH_SHORT).show();
        dialog.dismiss();
    }

    public static void getSubjectsFromFirebase(final ProgressBar progressBar1, final String subjectname1, final boolean check){
        Query query = FirebaseDatabase.getInstance().getReference("Subjects").orderByChild("classid")
                .equalTo(classList.get(classspinner.getSelectedItemPosition()));
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (subjectAdapter!=null){
                    subjectList.clear();
                    subjectidList.clear();
                }

                progressBar1.setVisibility(View.GONE);
                if (dataSnapshot.exists()){
                    subjectList.add(0,"Please Select Subject");
                    subjectidList.add(0,"subjectid");
                    for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                        subjectList.add(snapshot.child("subjectname").getValue().toString());
                        subjectidList.add(snapshot.getKey().toString());
                    }
                }else{
                    subjectList.add(0,"No Subject Available");
                }
                subjectAdapter.notifyDataSetChanged();
                subjectspinner.setAdapter(subjectAdapter);
                if (!check){

                    subjectspinner.setSelection(subjectAdapter.getPosition(subjectname1));
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getSubjectsFromFirebase(){
        databaseReference.child("Subjects").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (adapter != null){
                    list.clear();
                }
                progressBar.setVisibility(View.GONE);
                for (final DataSnapshot snapshot:dataSnapshot.getChildren()){
                  //  String value = dataSnapshot.child(snapshot.child("classid").getValue().toString()).child("classname").getValue().toString();
                    if (snapshot.child("subjectfee").exists()) {
                        SubjectFeeListitems listItems = new SubjectFeeListitems(snapshot.getKey(), snapshot.child("subjectname").getValue().toString(),
                                snapshot.child("subjectfee").getValue().toString(), snapshot.child("classid").getValue().toString());
                        list.add(listItems);
                    }

//                        databaseReference.child("Classes").addValueEventListener(new ValueEventListener() {
//                            @Override
//                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                            }
//
//                            @Override
//                            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                            }
//                        });
                }
                adapter = new SubjectFeeAdapter(AddFeeStructure.this,list);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getSearchSubjectsFromFirebase(){
        Query query = databaseReference.child("Subjects").orderByChild("classid")
                .equalTo(searchClassList.get(searchClassSpinner.getSelectedItemPosition()));
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (adapter != null){
                    list.clear();
                }
                progressBar.setVisibility(View.GONE);
                for (final DataSnapshot snapshot:dataSnapshot.getChildren()){
                    //  String value = dataSnapshot.child(snapshot.child("classid").getValue().toString()).child("classname").getValue().toString();
                    if (snapshot.child("subjectfee").exists()) {
                        SubjectFeeListitems listItems = new SubjectFeeListitems(snapshot.getKey(), snapshot.child("subjectname").getValue().toString(),
                                snapshot.child("subjectfee").getValue().toString(), snapshot.child("classid").getValue().toString());
                        list.add(listItems);
                    }

                }
                adapter = new SubjectFeeAdapter(AddFeeStructure.this,list);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public static void getClassFromFirebase(final ProgressBar cpbar, final String cname, final boolean check){
        databaseReference.child("Classes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (classAdapter!=null){
                    classList.clear();
                    classIdList.clear();
                }
                classList.add(0,"Please Select Class");
                classIdList.add(0,"classid");
                cpbar.setVisibility(View.GONE);
                for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                    classList.add(snapshot.child("classname").getValue().toString());
                    classIdList.add(snapshot.getKey());
                }
                classAdapter.notifyDataSetChanged();
                classspinner.setAdapter(classAdapter);
                if (!check){
                    classspinner.setSelection(classAdapter.getPosition(cname));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getSearchClassFromFirebase(){
        databaseReference.child("Classes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (searchClassAdapter!=null){
                    searchClassList.clear();
                }
                searchClassList.add(0,"Search By Class");
                for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                    searchClassList.add(snapshot.child("classname").getValue().toString());
                }
                searchClassAdapter.notifyDataSetChanged();
                searchClassSpinner.setAdapter(searchClassAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
