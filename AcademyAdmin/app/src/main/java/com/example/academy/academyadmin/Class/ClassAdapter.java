package com.example.academy.academyadmin.Class;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.academy.academyadmin.R;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class ClassAdapter extends RecyclerView.Adapter<ClassAdapter.ViewHolder> {
    private List<ClassListItems> list;
    private Context context;

    public ClassAdapter(List<ClassListItems> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
      View v = LayoutInflater.from(context).inflate(R.layout.class_list_items,viewGroup,false);
      return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
       final ClassListItems listItems = list.get(i);
       viewHolder.classname.setText(listItems.getClassname());
       viewHolder.deletebtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               FirebaseDatabase.getInstance().getReference().child("Classes").child(listItems.getClassid())
                       .removeValue(new DatabaseReference.CompletionListener() {
                           @Override
                           public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                               Toast.makeText(context, "Class has been deleted", Toast.LENGTH_SHORT).show();
                           }
                       });
           }
       });

       viewHolder.editbtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               AddClass.openDialog(false,listItems.getClassid(),listItems.getClassname());
           }
       });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView classname;
        ImageButton deletebtn,editbtn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            classname = itemView.findViewById(R.id.classname);
            deletebtn = itemView.findViewById(R.id.deletebtn);
            editbtn = itemView.findViewById(R.id.editbtn);
        }
    }
}
