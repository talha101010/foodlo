package com.example.academy.academyadmin.Exams;

public class AddSubExam_Firebase {
    private String mainexamname,subexamname;

    public AddSubExam_Firebase() {
    }

    public String getMainexamname() {
        return mainexamname;
    }

    public void setMainexamname(String mainexamid) {
        this.mainexamname = mainexamid;
    }

    public String getSubexamname() {
        return subexamname;
    }

    public void setSubexamname(String subexamname) {
        this.subexamname = subexamname;
    }
}
