package com.example.academy.academyadmin.Students;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.academy.academyadmin.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MarkAttendance extends AppCompatActivity {
    private RecyclerView recyclerView;
    private List<ShowStudentsListitems> list;
    private RecyclerView.Adapter adapter;
    private ProgressBar progressBar;
    public static Button attendancebtn;
    private String monthNumber,dateyear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mark_attendance);
        list = new ArrayList<>();
        recyclerView = findViewById(R.id.attendancerecyclerview);
        progressBar = findViewById(R.id.progressbar);
        attendancebtn = findViewById(R.id.attendancebtn);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        monthNumber = (String) DateFormat.format("MM", new Date());
        dateyear = (String) DateFormat.format("yyyy", new Date());
        getStudents();


    }

    private void getStudents(){
        Query query = FirebaseDatabase.getInstance().getReference().child("Students")
                .orderByChild("class_session").equalTo(ShowStudents.class_session_id);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (adapter!=null){
                    list.clear();
                }
                progressBar.setVisibility(View.GONE);
                if (dataSnapshot.getChildrenCount()>0){
                    for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                        ShowStudentsListitems listitems = new ShowStudentsListitems(snapshot.getKey(),snapshot.child("studentname").getValue().toString(),
                                snapshot.child("fathername").getValue().toString(),snapshot.child("classid").getValue().toString(),
                                snapshot.child("sessionid").getValue().toString(),snapshot.child("studentcontact").getValue().toString(),
                                snapshot.child("fathercontact").getValue().toString(),snapshot.child("cnic").getValue().toString(),
                                snapshot.child("dob").getValue().toString(),snapshot.child("gender").getValue().toString(),
                                snapshot.child("address").getValue().toString(),snapshot.child("rollno").getValue().toString(),
                                snapshot.child("institutename").getValue().toString());
                        if (snapshot.child("nextfee").exists()){
                            listitems.setNextdate(snapshot.child("nextfee").getValue().toString());
                        }else{
                            listitems.setNextdate("null");
                        }
                        if (snapshot.child("Attendance").exists()) {
                            if (snapshot.child("Attendance").child(monthNumber + "-" + dateyear).child("leave").exists()) {
                                listitems.setAttendanceYear(snapshot.child("Attendance").child(monthNumber + "-" + dateyear).getKey());
                                listitems.setAbsent(snapshot.child("Attendance").child(monthNumber + "-" + dateyear).child("absent").getValue().toString());
                                listitems.setPresent(snapshot.child("Attendance").child(monthNumber + "-" + dateyear).child("present").getValue().toString());
                                listitems.setLeave(snapshot.child("Attendance").child(monthNumber + "-" + dateyear).child("leave").getValue().toString());
                            } else {
                                listitems.setAttendanceYear("null");
                            }
                        }else{
                            listitems.setAttendanceYear("null");
                        }
                        list.add(listitems);
                    }
                }else{
                    Toast.makeText(MarkAttendance.this, "no data", Toast.LENGTH_SHORT).show();
                }

                adapter = new ShowStudentsAdapter(MarkAttendance.this,list,false);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
