package com.example.academy.academyadmin.Students;

public class AddResult_Listitems {
    private String mainexam,subexam,subject,totalmarks,obtainedmarks,date,studentid,short_brief;

    public AddResult_Listitems() {
    }

    public String getShort_brief() {
        return short_brief;
    }

    public void setShort_brief(String short_brief) {
        this.short_brief = short_brief;
    }

    public String getMainexam() {
        return mainexam;
    }

    public void setMainexam(String mainexam) {
        this.mainexam = mainexam;
    }

    public String getSubexam() {
        return subexam;
    }

    public void setSubexam(String subexam) {
        this.subexam = subexam;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTotalmarks() {
        return totalmarks;
    }

    public void setTotalmarks(String totalmarks) {
        this.totalmarks = totalmarks;
    }

    public String getObtainedmarks() {
        return obtainedmarks;
    }

    public void setObtainedmarks(String obtainedmarks) {
        this.obtainedmarks = obtainedmarks;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStudentid() {
        return studentid;
    }

    public void setStudentid(String studentid) {
        this.studentid = studentid;
    }
}
