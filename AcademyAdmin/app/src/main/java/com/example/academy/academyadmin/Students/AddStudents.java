package com.example.academy.academyadmin.Students;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.academy.academyadmin.MainActivity;
import com.example.academy.academyadmin.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class AddStudents extends AppCompatActivity {
    private EditText studentname,fname,scontactnumber,fcontactnumber,scnic,address,password,instiname;
    private TextView dob;
    private Spinner classSpinner,sessionSpinner;
    private Button addStudentbtn,updateStudentbtn;
    private ArrayList<String> classList,classIdList,sessionList,sessionIdList;
    private ArrayAdapter<String> classAdapter,sessionAdapter;
    private ProgressBar progressBar;
    private DatabaseReference databaseReference;
    private String fcontact,cnic,dateyear;
    private RadioButton malebtn,femalebtn;
    final Calendar myCalendar = Calendar.getInstance();
    private long totalStudents=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_students);
        getViews();
        getStudents();
        classAdapter = new ArrayAdapter<String>(AddStudents.this,android.R.layout.simple_spinner_dropdown_item,classList);
        sessionAdapter = new ArrayAdapter<String>(AddStudents.this,android.R.layout.simple_spinner_dropdown_item,sessionList);

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "MM/dd/yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                dob.setText(sdf.format(myCalendar.getTime()));
            }

        };

        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(AddStudents.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        dateyear = (String) DateFormat.format("yyyy", new Date());

        addStudentbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (studentname.getText().length()==0){
                    studentname.setError("This field is required");
                }else if (fname.getText().length()==0){
                    fname.setError("This field is required");
                }else if (classSpinner.getSelectedItemPosition()==0){
                    Toast.makeText(AddStudents.this, "Please Select Class", Toast.LENGTH_SHORT).show();
                }else if (sessionSpinner.getSelectedItemPosition()==0){
                    Toast.makeText(AddStudents.this, "Please Select Session", Toast.LENGTH_SHORT).show();
                }else if (instiname.getText().length()==0){
                    instiname.setError("This field is required");
                }else if (scontactnumber.getText().length()==0){
                    scontactnumber.setError("This field is required");
                }else if (dob.getText().length()==0){
                    dob.setError("This field is required");
                }else if (address.getText().length()==0){
                    address.setError("This field is required");
                }else if (password.getText().length()==0){
                    password.setError("This field is required");
                }else{
                     if (fcontactnumber.getText().length()==0){
                         fcontact = "null";
                     }else{
                         fcontact = fcontactnumber.getText().toString().trim();
                     }
                     if (scnic.getText().length()==0){
                         cnic = "null";
                     }else{
                         cnic = scnic.getText().toString().trim();
                     }
                     addStudentToFirebase();
                }
            }
        });

        if (getIntent().getStringExtra("edit").equals("update")){
            studentname.setText(getIntent().getStringExtra("studentname"));
            fname.setText(getIntent().getStringExtra("fathername"));
            scontactnumber.setText(getIntent().getStringExtra("contactnumber"));
            fcontactnumber.setText(getIntent().getStringExtra("fcontactnumber"));
            scnic.setText(getIntent().getStringExtra("cnic"));
            dob.setText(getIntent().getStringExtra("dob"));
            address.setText(getIntent().getStringExtra("address"));
            instiname.setText(getIntent().getStringExtra("insti"));
            if (getIntent().getStringExtra("gender").equals("male")){
                malebtn.setChecked(true);
            }else{
                femalebtn.setChecked(true);
            }
            password.setVisibility(View.GONE);
            updateStudentbtn.setVisibility(View.VISIBLE);
            addStudentbtn.setVisibility(View.GONE);
            getClassFromFirebase(true);
            getSessionFromFirebase(true);
        }else{
            updateStudentbtn.setVisibility(View.GONE);
            addStudentbtn.setVisibility(View.VISIBLE);
            password.setVisibility(View.VISIBLE);
            getClassFromFirebase(false);
            getSessionFromFirebase(false);
        }

        updateStudentbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (studentname.getText().length()==0){
                    studentname.setError("This field is required");
                }else if (fname.getText().length()==0){
                    fname.setError("This field is required");
                }else if (classSpinner.getSelectedItemPosition()==0){
                    Toast.makeText(AddStudents.this, "Please Select Class", Toast.LENGTH_SHORT).show();
                }else if (sessionSpinner.getSelectedItemPosition()==0){
                    Toast.makeText(AddStudents.this, "Please Select Session", Toast.LENGTH_SHORT).show();
                }else if (instiname.getText().length()==0){
                    instiname.setError("This field is required");
                }else if (scontactnumber.getText().length()==0){
                    scontactnumber.setError("This field is required");
                }else if (dob.getText().length()==0){
                    dob.setError("This field is required");
                }else if (address.getText().length()==0){
                    address.setError("This field is required");
                }else{
                    if (fcontactnumber.getText().length()==0){
                        fcontact = "null";
                    }else{
                        fcontact = fcontactnumber.getText().toString().trim();
                    }
                    if (scnic.getText().length()==0){
                        cnic = "null";
                    }else{
                        cnic = scnic.getText().toString().trim();
                    }
                    updateStudent();
                }
            }
        });

    }

    private void updateStudent(){
        String id = getIntent().getStringExtra("studentid");
        DatabaseReference dreference = FirebaseDatabase.getInstance().getReference().child("Students").child(id);
        dreference.child("studentname").setValue(studentname.getText().toString().trim());
        dreference.child("fathername").setValue(fname.getText().toString().trim());
        dreference.child("address").setValue(address.getText().toString().trim());
        dreference.child("classid").setValue(classIdList.get(classSpinner.getSelectedItemPosition()));
        dreference.child("sessionid").setValue(sessionIdList.get(sessionSpinner.getSelectedItemPosition()));
        dreference.child("class_session").setValue(classIdList.get(classSpinner.getSelectedItemPosition())+"_"+sessionIdList.get(sessionSpinner.getSelectedItemPosition()));
        dreference.child("cnic").setValue(scnic.getText().toString().trim());
        dreference.child("dob").setValue(dob.getText().toString().trim());
        dreference.child("fathercontact").setValue(fcontact);
        if (malebtn.isChecked()){
            dreference.child("gender").setValue("male");
        }else{
            dreference.child("gender").setValue("female");
        }
        dreference.child("institutename").setValue(instiname.getText().toString().trim());
        dreference.child("studentcontact").setValue(scontactnumber.getText().toString().trim());
        Toast.makeText(this, "Student has been updated", Toast.LENGTH_SHORT).show();
        if (StudentDetail.activity!=null){
            StudentDetail.activity.finish();
        }
        finish();
    }

    private void getViews(){
        studentname = findViewById(R.id.studentname);
        fname = findViewById(R.id.fathername);
        scontactnumber = findViewById(R.id.contactnumber);
        fcontactnumber = findViewById(R.id.fathercontactnumber);
        scnic = findViewById(R.id.studentcnic);
        dob = findViewById(R.id.studentdob);
        address = findViewById(R.id.studentaddress);
        classSpinner = findViewById(R.id.classspinner);
        sessionSpinner = findViewById(R.id.sessionspinner);
        addStudentbtn = findViewById(R.id.addstudentbtn);
        updateStudentbtn = findViewById(R.id.updatestudentbtn);
        classList = new ArrayList<>();
        classIdList = new ArrayList<>();
        sessionList = new ArrayList<>();
        sessionIdList = new ArrayList<>();
        progressBar = findViewById(R.id.progressbar);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        password = findViewById(R.id.studentpassword);
        malebtn = findViewById(R.id.maleradiobtn);
        femalebtn = findViewById(R.id.femaleradiobtn);
        instiname =findViewById(R.id.institutename);
    }

    private void getClassFromFirebase(final boolean check){
        databaseReference.child("Classes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (classAdapter!=null){
                    classList.clear();
                    classIdList.clear();
                }
                classList.add(0,"Please Select Class");
                classIdList.add(0,"classid");
                for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                    if (snapshot.child("regfee").exists()){
                        classList.add(snapshot.child("classname").getValue().toString());
                        classIdList.add(snapshot.getKey());
                    }

                }
                classAdapter.notifyDataSetChanged();
                classSpinner.setAdapter(classAdapter);
                if (check){
                   classSpinner.setSelection(classAdapter.getPosition(getIntent().getStringExtra("classname")));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getSessionFromFirebase(final boolean check){
        databaseReference.child("Sessions").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (sessionAdapter!=null){
                    sessionList.clear();
                    sessionIdList.clear();
                }
                progressBar.setVisibility(View.GONE);
                sessionList.add(0,"Please Select Session");
                sessionIdList.add(0,"id");
                for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                    sessionList.add(snapshot.child("sessionname").getValue().toString());
                    sessionIdList.add(snapshot.getKey());
                }
                sessionAdapter.notifyDataSetChanged();
                sessionSpinner.setAdapter(sessionAdapter);
                if (check){
                    sessionSpinner.setSelection(sessionAdapter.getPosition(getIntent().getStringExtra("sessionname")));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void addStudentToFirebase(){
        totalStudents++;
        progressBar.setVisibility(View.VISIBLE);
        addStudents_Firebase addStudents = new addStudents_Firebase();
        addStudents.setStudentname(studentname.getText().toString().trim());
        addStudents.setFathername(fname.getText().toString().trim());
        addStudents.setClassid(classIdList.get(classSpinner.getSelectedItemPosition()));
        addStudents.setSessionid(sessionIdList.get(sessionSpinner.getSelectedItemPosition()));
        addStudents.setInstitutename(instiname.getText().toString().trim());
        addStudents.setRollno(dateyear+"-"+classList.get(classSpinner.getSelectedItemPosition())+"-"+totalStudents+"");
        addStudents.setStudentcontact(scontactnumber.getText().toString().trim());
        addStudents.setFathercontact(fcontact);
        addStudents.setCnic(cnic);
        addStudents.setDob(dob.getText().toString().trim());
        addStudents.setAddress(address.getText().toString().trim());
        addStudents.setPassword(password.getText().toString().trim());
        addStudents.setClass_session(classIdList.get(classSpinner.getSelectedItemPosition())+"_"+sessionIdList.get(sessionSpinner.getSelectedItemPosition()));
        if (malebtn.isChecked()){
            addStudents.setGender("male");
        }else{
            addStudents.setGender("female");
        }

        databaseReference.child("Students").push().setValue(addStudents)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(AddStudents.this, "Student has been added", Toast.LENGTH_SHORT).show();
                        String phone = scontactnumber.getText().toString().trim();
                        String message = "Hello "+studentname.getText().toString().trim()+" Your username="+phone+" and " +
                                "your password="+password.getText().toString().trim();
                        sendSMS(phone,message);
                        finish();
                        MainActivity.drawerLayout.closeDrawer(Gravity.LEFT);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(AddStudents.this, "Communiction server error", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void sendSMS(final String phone, final String message){
        String url = "http://www.thebhakkar.com/academy/singlesms.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (!jsonObject.getBoolean("error")){

                            }else{
                                Toast.makeText(AddStudents.this, jsonObject.getString("message")+"", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(AddStudents.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("phone",phone);
                param.put("message",message);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(AddStudents.this);
        requestQueue.add(stringRequest);
    }

    private void getStudents(){
        databaseReference.child("Students").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                totalStudents = dataSnapshot.getChildrenCount();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

//    private void getSubjectsFromServer(){
//        Query query = databaseReference.child("Subjects").orderByChild("classid")
//                .equalTo(classIdList.get(classSpinner.getSelectedItemPosition()));
//        query.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                subjectCount = dataSnapshot.getChildrenCount();
//               linearLayout.removeAllViews();
//               int i = 0;
//               for (DataSnapshot snapshot:dataSnapshot.getChildren()){
//                   CheckBox checkBox = new CheckBox(getApplicationContext());
//                   checkBox.setId(i);
//                   checkBox.setText(snapshot.child("subjectname").getValue().toString());
//                   linearLayout.addView(checkBox);
//                   i++;
//               }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//    }


}
