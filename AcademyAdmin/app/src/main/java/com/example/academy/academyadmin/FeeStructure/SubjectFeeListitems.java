package com.example.academy.academyadmin.FeeStructure;

public class SubjectFeeListitems {
    private String subjectid,subjectname,subjectfee,classname;

    public SubjectFeeListitems(String subjectid, String subjectname, String subjectfee,String classname) {
        this.subjectid = subjectid;
        this.subjectname = subjectname;
        this.subjectfee = subjectfee;
        this.classname = classname;
    }

    public String getSubjectid() {
        return subjectid;
    }

    public String getSubjectname() {
        return subjectname;
    }

    public String getSubjectfee() {
        return subjectfee;
    }

    public String getClassname() {
        return classname;
    }
}
