package com.example.academy.academyadmin;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.example.academy.academyadmin.Class.AddClass;
import com.example.academy.academyadmin.Class.ClassListItems;
import com.example.academy.academyadmin.Exams.AddExamsType;
import com.example.academy.academyadmin.FeeStructure.AddFeeStructure;
import com.example.academy.academyadmin.Session.AddSession;
import com.example.academy.academyadmin.Students.AddStudents;
import com.example.academy.academyadmin.Students.ShowClassAdapter;
import com.example.academy.academyadmin.Students.ShowStudents;
import com.example.academy.academyadmin.Subjects.AddSubject;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private NavigationView navigationView;
    public static DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private ActionBarDrawerToggle mToggle;
    private DatabaseReference databaseReference;
    private RecyclerView recyclerView;
    private List<ClassListItems> list;
    private RecyclerView.Adapter adapter;
    private ProgressBar progressBar;
    public static Activity activity;
    public static ArrayList<String> sessionName,sessionId;
    public static ArrayAdapter<String> sessionAdapter;
    public static String cname,sname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = MainActivity.this;
        getViews();
        setSupportActionBar(toolbar);
        mToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        navigationView.inflateHeaderView(R.layout.navigation_header);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id){
                    case R.id.addclass:
                        startActivity(new Intent(MainActivity.this,AddClass.class));
                        break;
                    case R.id.addsession:
                        startActivity(new Intent(MainActivity.this,AddSession.class));
                        break;
                    case R.id.addsubject:
                        startActivity(new Intent(MainActivity.this,AddSubject.class));
                        break;
                    case R.id.addfee:
                        startActivity(new Intent(MainActivity.this,AddFeeStructure.class));
                        break;
                    case R.id.addstudent:
                      Intent intent = new Intent(MainActivity.this,AddStudents.class);
                      intent.putExtra("edit","add");
                      startActivity(intent);
                        break;
                    case R.id.examtype:
                        startActivity(new Intent(MainActivity.this,AddExamsType.class));
                    case R.id.logout:
                        FirebaseAuth.getInstance().signOut();
                        Toast.makeText(MainActivity.this, "Logged out successfully", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(MainActivity.this,Login.class));
                        finish();
                    default:
                        return true;
                }
                return true;
            }
        });

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(MainActivity.this,2));

        getClasses();
    }
    private void getViews(){
        navigationView = findViewById(R.id.nv);
        drawerLayout = findViewById(R.id.drawerlayout);
        toolbar = findViewById(R.id.toolbar);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        recyclerView = findViewById(R.id.homeclassrecyclerview);
        list = new ArrayList<>();
        progressBar = findViewById(R.id.progressbar);
        sessionName = new ArrayList<>();
        sessionId = new ArrayList<>();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getClasses(){
        databaseReference.child("Classes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                 if (adapter!=null){
                     list.clear();
                 }
                 progressBar.setVisibility(View.GONE);
                 for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                     ClassListItems listItems = new ClassListItems(snapshot.getKey(),snapshot.child("classname").getValue().toString());
                     list.add(listItems);
                 }
                 adapter = new ShowClassAdapter(MainActivity.this,list);
                 recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public static void openDialog(final String classid, final String classname){
        final Dialog dialog = new Dialog(activity,R.style.Theme_AppCompat_Light_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.showsession_dialog);
        final Spinner sessionspinner = dialog.findViewById(R.id.sessionspinner);
        ProgressBar progressBar1 = dialog.findViewById(R.id.progressbar1);
        ImageButton dialogButton =  dialog.findViewById(R.id.closebtn);
        Button proceddbtn = dialog.findViewById(R.id.proceddbtn);
        sessionAdapter = new ArrayAdapter<String>(activity,android.R.layout.simple_spinner_dropdown_item,sessionName);
        getSessionFromFirebase(progressBar1,sessionspinner);
        proceddbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionspinner.getSelectedItemPosition()==0){
                    Toast.makeText(activity, "please select session", Toast.LENGTH_SHORT).show();
                }else{
                    sname = sessionName.get(sessionspinner.getSelectedItemPosition());
                    cname = classname;
                    Intent intent = new Intent(activity,ShowStudents.class);
                    intent.putExtra("id",classid+"_"+sessionId.get(sessionspinner.getSelectedItemPosition()));
                    activity.startActivity(intent);
                }

            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static void getSessionFromFirebase(final ProgressBar progressBar1, final Spinner sessionspinner){
        FirebaseDatabase.getInstance().getReference().child("Sessions").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (sessionAdapter!=null){
                    sessionName.clear();
                    sessionId.clear();
                }
                progressBar1.setVisibility(View.GONE);
                sessionName.add(0,"Please Select Session");
                sessionId.add(0,"sessionid");
                for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                    sessionName.add(snapshot.child("sessionname").getValue().toString());
                    sessionId.add(snapshot.getKey());
                }
                sessionAdapter.notifyDataSetChanged();
                sessionspinner.setAdapter(sessionAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
