package com.example.academy.academyadmin.Students;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.academy.academyadmin.MainActivity;
import com.example.academy.academyadmin.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class BasicInfo extends Fragment {
    private TextView studentname,fname,scontactnumber,fcontactnumber,scnic,dob,address,classname,session,gender,
    assignedsubject,totalfee,rollno,institutename;
    private ImageButton editbtn,deletebtn;
    private  Button assignsubjectbtn;
    private ArrayList<String> subjectName,subjectFee,sName,sFee,doubleSName;
    private ArrayAdapter<String> subjectAdapter;
    private LinearLayout linearLayout;
    private String subjects="";
    public static int totalfee1=0;
    private long count=0;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.basic_info, container, false);
        studentname = rootView.findViewById(R.id.studentname);
        fname = rootView.findViewById(R.id.fathername);
        scontactnumber = rootView.findViewById(R.id.contactnumber);
        fcontactnumber = rootView.findViewById(R.id.fcontactnumber);
        fcontactnumber = rootView.findViewById(R.id.fcontactnumber);
        scnic = rootView.findViewById(R.id.cnic);
        dob = rootView.findViewById(R.id.dob);
        address = rootView.findViewById(R.id.address);
        classname = rootView.findViewById(R.id.classname);
        session = rootView.findViewById(R.id.session);
        gender = rootView.findViewById(R.id.gender);
        editbtn = rootView.findViewById(R.id.editbtn);
        deletebtn = rootView.findViewById(R.id.deletebtn);
        assignedsubject = rootView.findViewById(R.id.assignedsubject);
        assignsubjectbtn = rootView.findViewById(R.id.assignsubjectbtn);
        totalfee = rootView.findViewById(R.id.totalfee);
        rollno = rootView.findViewById(R.id.rollno);
        institutename = rootView.findViewById(R.id.institutename);
         subjectName = new ArrayList<>();
         subjectFee = new ArrayList<>();
        doubleSName =new ArrayList<>();
         getSubjects();

        classname.setText(MainActivity.cname);
        session.setText(MainActivity.sname);
        studentname.setText(getActivity().getIntent().getStringExtra("studentname"));
        fname.setText(getActivity().getIntent().getStringExtra("fathername"));
        scontactnumber.setText(getActivity().getIntent().getStringExtra("contactnumber"));
        fcontactnumber.setText(getActivity().getIntent().getStringExtra("fcontactnumber"));
        scnic.setText(getActivity().getIntent().getStringExtra("cnic"));
        dob.setText(getActivity().getIntent().getStringExtra("dob"));
        address.setText(getActivity().getIntent().getStringExtra("address"));
        gender.setText(getActivity().getIntent().getStringExtra("gender"));
        rollno.setText(getActivity().getIntent().getStringExtra("rollno"));
        institutename.setText(getActivity().getIntent().getStringExtra("insti"));

        assignsubjectbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               openDialog();
            }
        });

        deletebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseDatabase.getInstance().getReference().child("Students").child(getActivity().getIntent().getStringExtra("studentid"))
                        .removeValue(new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                Toast.makeText(getActivity(), "Student has been deleted", Toast.LENGTH_SHORT).show();
                            }
                        });
                StudentDetail.activity.finish();
            }
        });

        editbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),AddStudents.class);
                intent.putExtra("edit","update");
                intent.putExtra("classname",classname.getText().toString());
                intent.putExtra("sessionname",session.getText().toString());
                intent.putExtra("studentname",getActivity().getIntent().getStringExtra("studentname"));
                intent.putExtra("fathername",getActivity().getIntent().getStringExtra("fathername"));
                intent.putExtra("contactnumber",getActivity().getIntent().getStringExtra("contactnumber"));
                intent.putExtra("fcontactnumber",getActivity().getIntent().getStringExtra("fcontactnumber"));
                intent.putExtra("cnic",getActivity().getIntent().getStringExtra("cnic"));
                intent.putExtra("dob",getActivity().getIntent().getStringExtra("dob"));
                intent.putExtra("address",getActivity().getIntent().getStringExtra("address"));
                intent.putExtra("gender",getActivity().getIntent().getStringExtra("gender"));
                intent.putExtra("studentid",getActivity().getIntent().getStringExtra("studentid"));
                intent.putExtra("insti",getActivity().getIntent().getStringExtra("insti"));
                startActivity(intent);
            }
        });

        return rootView;
    }

    private void openDialog(){
        final Dialog dialog = new Dialog(getActivity(),R.style.Theme_AppCompat_Light_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.assign_subject_dialog);
        final Spinner subjectSpinner = dialog.findViewById(R.id.sessionspinner);
        ProgressBar progressBar1 = dialog.findViewById(R.id.progressbar1);
        Button dialogButton =  dialog.findViewById(R.id.closebtn);
        Button proceddbtn = dialog.findViewById(R.id.proceddbtn);
        linearLayout = dialog.findViewById(R.id.assignsubjectlayout);
        subjectAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item,subjectName);
        getSubjectFromFirebase(progressBar1,subjectSpinner);
         sName = new ArrayList<>();
         sFee = new ArrayList<>();
        subjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){

                }else{
                    if (doubleSName.size()>0){
                        int j =0;
                        for (int i =0;i<doubleSName.size();i++){
                            if (doubleSName.get(i).equals(subjectName.get(subjectSpinner.getSelectedItemPosition()))){
                                Toast.makeText(getActivity(), "Sorry this subject already assigned", Toast.LENGTH_SHORT).show();
                                j++;
                                break;
                            }
                        }
                        if (j==0){
                            TextView textView = new TextView(getActivity());
                            textView.setText(subjectName.get(subjectSpinner.getSelectedItemPosition()));
                            linearLayout.addView(textView);
                            sName.add(subjectName.get(subjectSpinner.getSelectedItemPosition()));
                            sFee.add(subjectFee.get(subjectSpinner.getSelectedItemPosition()));
                        }
                    }else{
                        TextView textView = new TextView(getActivity());
                        textView.setText(subjectName.get(subjectSpinner.getSelectedItemPosition()));
                        linearLayout.addView(textView);
                        sName.add(subjectName.get(subjectSpinner.getSelectedItemPosition()));
                        sFee.add(subjectFee.get(subjectSpinner.getSelectedItemPosition()));
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        proceddbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sName.size()>0){
                    assignSubject();
                    dialog.dismiss();
                }else{
                    dialog.dismiss();
                }

            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void getSubjectFromFirebase(final ProgressBar progressBar1, final Spinner subjectSpinner){
      Query query = FirebaseDatabase.getInstance().getReference().child("Subjects")
              .orderByChild("classid").equalTo(classname.getText().toString().trim());
      query.addValueEventListener(new ValueEventListener() {
          @Override
          public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
              if (subjectAdapter!=null){
                  subjectName.clear();
                  subjectFee.clear();
              }
              subjectName.add(0,"Please Select Subject");
              subjectFee.add(0,"subjectfee");
              progressBar1.setVisibility(View.GONE);
              for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                  if (snapshot.child("subjectfee").exists()){
                      subjectName.add(snapshot.child("subjectname").getValue().toString());
                      subjectFee.add(snapshot.child("subjectfee").getValue().toString());
                  }

              }
              subjectAdapter.notifyDataSetChanged();
              subjectSpinner.setAdapter(subjectAdapter);
          }

          @Override
          public void onCancelled(@NonNull DatabaseError databaseError) {

          }
      });
    }

    private void assignSubject(){
        DatabaseReference dbreference = FirebaseDatabase.getInstance().getReference().child("Students").
                child(getActivity().getIntent().getStringExtra("studentid"));
        for (int i=0;i<sName.size();i++){
            if (count>0){
                dbreference.child("Subjects").child("subject"+count).child("name").setValue(sName.get(i));
                dbreference.child("Subjects").child("subject"+count).child("fee").setValue(sFee.get(i));
                count++;
            }else{
                dbreference.child("Subjects").child("subject"+i).child("name").setValue(sName.get(i));
                dbreference.child("Subjects").child("subject"+i).child("fee").setValue(sFee.get(i));
            }
          subjects = "";
          totalfee1=0;
          doubleSName.clear();
          getSubjects();
        }
        Toast.makeText(getActivity(), "Subject has been assigned", Toast.LENGTH_SHORT).show();
    }

    private void getSubjects(){
        totalfee1=0;
        final DatabaseReference dbreference = FirebaseDatabase.getInstance().getReference().child("Students").
                child(getActivity().getIntent().getStringExtra("studentid")).child("Subjects");
                dbreference
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        doubleSName.clear();
                        subjects = "";
                        if (dataSnapshot.exists()){
                            count = dataSnapshot.getChildrenCount();
                           for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                           subjects +=   snapshot.child("name").getValue().toString()+" , ";
                           doubleSName.add(snapshot.child("name").getValue().toString());
                           totalfee1 +=Integer.parseInt(snapshot.child("fee").getValue().toString());
                           }
                        }

                        if (count>0){
                            assignedsubject.setText(subjects);
                            totalfee.setText(totalfee1+"");
                        }else{
                            assignedsubject.setText("No subject assigned yet to this student");
                            totalfee.setText("No subject assigned yet to this student");
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

}
