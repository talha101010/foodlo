package com.example.academy.academyadmin.Students;

public class AttendanceListItems {
    private String attendanceyear,present,absent,leave;

    public AttendanceListItems(String attendanceyear, String present, String absent,String leave) {
        this.attendanceyear = attendanceyear;
        this.present = present;
        this.absent = absent;
        this.leave = leave;
    }

    public String getAttendanceyear() {
        return attendanceyear;
    }

    public String getPresent() {
        return present;
    }

    public String getAbsent() {
        return absent;
    }

    public String getLeave() {
        return leave;
    }
}
