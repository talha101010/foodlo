package com.example.academy.academyadmin.Students;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.academy.academyadmin.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShowStudents extends AppCompatActivity implements SearchView.OnQueryTextListener {
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private List<ShowStudentsListitems> list;
    private RecyclerView.Adapter adapter;
    public static String class_session_id;
    private TextView nodata;
    private Button addAttendance,smsToAll;
    private Toolbar toolbar;
    public static Activity activity;
    private String bulkcellnumbers = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_students);
        getViews();
        activity = ShowStudents.this;
        setSupportActionBar(toolbar);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        class_session_id = getIntent().getStringExtra("id");
        getStudents();

        addAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ShowStudents.this,MarkAttendance.class));
            }
        });

        smsToAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 openDialog();
            }
        });

    }

    private void getList(){
        for (int i = 0; i<ShowStudentsAdapter.list.size(); i++){
            String checknumber = ShowStudentsAdapter.list.get(i).getContactnumber();
            if (checknumber.substring(0,1).equals("0")){
                checknumber = checknumber.substring(1);
                checknumber = "92"+checknumber;
            }
            bulkcellnumbers += checknumber+",";
        }

    }

    private void openDialog(){
        final Dialog dialog = new Dialog(ShowStudents.this,R.style.Theme_AppCompat_Light_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.sms_dialog);
        final EditText sms = dialog.findViewById(R.id.smsedittext);
        Button dialogButton =  dialog.findViewById(R.id.closebtn);
        Button addbtn = dialog.findViewById(R.id.proceddbtn);

        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sms.getText().length()==0){
                    sms.setError("This field is required");
                }else{
                    getList();
                    sendSMS(bulkcellnumbers,sms.getText().toString().trim(),dialog);
                }

            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void sendSMS(final String phone, final String message, final Dialog dialog){
        final ProgressDialog progressDialog = new ProgressDialog(ShowStudents.this);
        progressDialog.setMessage("Please wait");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        String url = "http://www.thebhakkar.com/academy/sendBulkSMS.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (!jsonObject.getBoolean("error")){
                                Toast.makeText(ShowStudents.this, jsonObject.getString("message")+"", Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }else{
                                Toast.makeText(ShowStudents.this, jsonObject.getString("message")+"", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(ShowStudents.this, error.getMessage()+"", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("phone",phone);
                param.put("message",message);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(ShowStudents.this);
        requestQueue.add(stringRequest);
    }

    private void getViews(){
        recyclerView = findViewById(R.id.studentsrecyclerview);
        progressBar = findViewById(R.id.progressbar);
        list = new ArrayList<>();
        nodata = findViewById(R.id.nodata);
        addAttendance = findViewById(R.id.addattendancebtn);
        toolbar = findViewById(R.id.toolbar);
        smsToAll = findViewById(R.id.smstoallbtn);
    }

    private void getStudents(){
        Query query = FirebaseDatabase.getInstance().getReference().child("Students")
                .orderByChild("class_session").equalTo(class_session_id);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (adapter!=null){
                    list.clear();
                }
                progressBar.setVisibility(View.GONE);
                if (dataSnapshot.getChildrenCount()>0){
                    for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                        ShowStudentsListitems listitems = new ShowStudentsListitems(snapshot.getKey(),snapshot.child("studentname").getValue().toString(),
                                snapshot.child("fathername").getValue().toString(),snapshot.child("classid").getValue().toString(),
                                snapshot.child("sessionid").getValue().toString(),snapshot.child("studentcontact").getValue().toString(),
                                snapshot.child("fathercontact").getValue().toString(),snapshot.child("cnic").getValue().toString(),
                                snapshot.child("dob").getValue().toString(),snapshot.child("gender").getValue().toString(),
                                snapshot.child("address").getValue().toString(),snapshot.child("rollno").getValue().toString(),
                                snapshot.child("institutename").getValue().toString());
                        if (snapshot.child("nextfee").exists()){
                            listitems.setNextdate(snapshot.child("nextfee").getValue().toString());
                        }else{
                            listitems.setNextdate("null");
                        }


                        list.add(listitems);
                    }
                }else{
                    nodata.setVisibility(View.VISIBLE);
                }

                adapter = new ShowStudentsAdapter(ShowStudents.this,list,true);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_items_menu,menu);
        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        searchView.setOnQueryTextListener(this);
        return true;
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        newText = newText.toLowerCase();
        List<ShowStudentsListitems> newList = new ArrayList<>();
        for (ShowStudentsListitems wordList:list){
            String name = wordList.getStudentname().toLowerCase();
            if (name.contains(newText)){
                newList.add(wordList);
            }
        }
        ((ShowStudentsAdapter) adapter).setFilter(newList);
        return true;
    }
}
