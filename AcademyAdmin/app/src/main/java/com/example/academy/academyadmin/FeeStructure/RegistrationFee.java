package com.example.academy.academyadmin.FeeStructure;

import android.app.Activity;
import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.academy.academyadmin.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class RegistrationFee extends AppCompatActivity {
    private FloatingActionButton addfeebtn;
    public static ProgressBar progressBar;
    private RecyclerView recyclerView;
    public static Activity activity;
    public static Spinner classSpinner;
    public static EditText regFee;
    public static ArrayList<String> classList,classIdList;
    public static ArrayAdapter<String> classAdapter;
    public static DatabaseReference databaseReference;
    private List<RegistrationFeeListItems> list;
    private RecyclerView.Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_fee);
        activity = RegistrationFee.this;
        getViews();
        addfeebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog(true,"1","1","1");
            }
        });

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        getClassWithFee();
    }
    private void getViews(){
        addfeebtn = findViewById(R.id.add_fab);
        progressBar = findViewById(R.id.progressbar);
        recyclerView = findViewById(R.id.registrationfeerecyclerview);
        classList = new ArrayList<>();
        classIdList = new ArrayList<>();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        list = new ArrayList<>();
    }

    public static void openDialog(final boolean check, final String id, String cname, final String rfee){
        final Dialog dialog = new Dialog(activity,R.style.Theme_AppCompat_Light_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.registrationfee_dialog);
        regFee = dialog.findViewById(R.id.feeedittext);
        classSpinner = dialog.findViewById(R.id.class_spinner);
        final ProgressBar classprogressbar = dialog.findViewById(R.id.progressbar);
        ImageButton dialogButton =  dialog.findViewById(R.id.closebtn);
        Button addbtn = dialog.findViewById(R.id.addbtn);
        classAdapter = new ArrayAdapter<String>(activity,android.R.layout.simple_spinner_dropdown_item,classList);
        if (check){
            classSpinner.setVisibility(View.VISIBLE);
            classprogressbar.setVisibility(View.VISIBLE);
            getClassFromFirebase(classprogressbar,cname,check);
            addbtn.setText("Add Now");
        }else{
            classSpinner.setVisibility(View.GONE);
            classprogressbar.setVisibility(View.GONE);
            regFee.setText(rfee);
            addbtn.setText("Update Now");

        }
        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (classSpinner.getSelectedItemPosition()==0){
                    Toast.makeText(activity, "please select subject", Toast.LENGTH_SHORT).show();
                }
                if (regFee.getText().length()==0){
                    regFee.setError("This field is required");
                }else{
                    if (check){
                        addRegFeeToFirebase(dialog);
                    }else{
                        progressBar.setVisibility(View.VISIBLE);
                        updateFeeToFirebase(id,dialog);
                    }

                }

            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static void addRegFeeToFirebase(final Dialog dialog){
        progressBar.setVisibility(View.VISIBLE);
        databaseReference.child("Classes").child(classIdList.get(classSpinner.getSelectedItemPosition()))
                .child("regfee").setValue(regFee.getText().toString().trim())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        dialog.dismiss();
                        Toast.makeText(activity, "Registration fee has been added", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public static void updateFeeToFirebase(String id, final Dialog dialog){
        databaseReference.child("Classes").child(id).child("regfee").setValue(regFee.getText().toString().trim());
        Toast.makeText(activity, "Registration Fee has been updated", Toast.LENGTH_SHORT).show();
        dialog.dismiss();
    }

    public static void getClassFromFirebase(final ProgressBar cpbar, final String cname, final boolean check){
        databaseReference.child("Classes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (classAdapter!=null){
                    classList.clear();
                    classIdList.clear();
                }
                classList.add(0,"Please Select Class");
                classIdList.add(0,"classid");
                cpbar.setVisibility(View.GONE);
                for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                    classList.add(snapshot.child("classname").getValue().toString());
                    classIdList.add(snapshot.getKey());
                }
                classAdapter.notifyDataSetChanged();
                classSpinner.setAdapter(classAdapter);
                if (!check){
                    classSpinner.setSelection(classAdapter.getPosition(cname));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getClassWithFee(){
        databaseReference.child("Classes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (adapter != null){
                    list.clear();
                }
                 progressBar.setVisibility(View.GONE);
                for (final DataSnapshot snapshot:dataSnapshot.getChildren()){

                    if (snapshot.child("regfee").exists()) {
                        RegistrationFeeListItems listItems = new RegistrationFeeListItems(snapshot.getKey(), snapshot.child("classname").getValue().toString(),
                                snapshot.child("regfee").getValue().toString());
                        list.add(listItems);
                    }

                }
                adapter = new RegistrationFeeAdapter(RegistrationFee.this,list);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
