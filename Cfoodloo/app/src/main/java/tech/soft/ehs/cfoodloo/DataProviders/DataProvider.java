package tech.soft.ehs.cfoodloo.DataProviders;

public class DataProvider {

    //   empty constructor

    public DataProvider() {
    }

    //--------------end empty constructor-----------------


    //----- add city constructor and getter and setter---

    String cityName,cityId;

    public DataProvider(String cityName,String cityId) {
        this.cityName = cityName;
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    //---------------------end of add city data provider----------


    //---------------------Start Area ----------------------------

    String areaID, areaName;



    public String getAreaID() {
        return areaID;
    }

    public String getAreaName() {
        return areaName;
    }
}
