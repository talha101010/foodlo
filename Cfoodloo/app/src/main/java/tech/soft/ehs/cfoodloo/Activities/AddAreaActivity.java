package tech.soft.ehs.cfoodloo.Activities;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import tech.soft.ehs.cfoodloo.DataProviders.DataProvider;
import tech.soft.ehs.cfoodloo.R;

public class AddAreaActivity extends AppCompatActivity {

    TextView selectCity;
    EditText writeArea;
    Button addAreaBtn;
    String strWriteArea,strSelectArea;
    DatabaseReference db;
    List<DataProvider> cityList;
    String[] cityArray;
    ArrayAdapter<String> arrayAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_area);

        selectCity = findViewById(R.id.select_city);
        writeArea = findViewById(R.id.area_edittext);
        addAreaBtn= findViewById(R.id.add_area_btn);

        db = FirebaseDatabase.getInstance().getReference("cities");
        getCities();


        addAreaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editText();

                DatabaseReference addAreaDb = FirebaseDatabase.getInstance().getReference("areas").child(strSelectArea);
                String areaId = addAreaDb.push().getKey();
                DataProvider areaDp = new DataProvider(areaId, strWriteArea);
                addAreaDb.child(areaId).setValue(areaDp);

            }
        });

        selectCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCity();
            }
        });





    }

    public void selectCity(){

        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(AddAreaActivity.this);
        final LayoutInflater inflater =getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.select_city_dialog,null);
        final ListView lv = dialogView.findViewById(R.id.select_type_listview);
        alertdialog.setView(dialogView);
        alertdialog.setTitle("Select City");

        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, cityArray);
        lv.setAdapter(arrayAdapter);

        final AlertDialog dialog = alertdialog.create();
        dialog.show();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectCity.setText(arrayAdapter.getItem(position));
                dialog.dismiss();
            }
        });

    }

    public void getCities(){

        cityList = new ArrayList<>();

        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                cityList.clear();
                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){

                    DataProvider dp = dataSnapshot1.getValue(DataProvider.class);
                    cityList.add(dp);

                }

                cityArray = new String[cityList.size()];
                for (int i=0; i<cityArray.length; i++){

                    cityArray[i] = cityList.get(i).getCityName();
                    Toast.makeText(AddAreaActivity.this, cityArray[i], Toast.LENGTH_SHORT).show();
                }





            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    public void editText(){

        if (selectCity.getText().toString().equals("Select City")){

            selectCity.setError("Please Select city");
        }else if (writeArea.getText().toString().length()<3){

            writeArea.setError("Please write area name");
        }else {

            strSelectArea = selectCity.getText().toString();

            strWriteArea = writeArea.getText().toString();
            writeArea.setText("");
            Toast.makeText(this, "Area Added", Toast.LENGTH_SHORT).show();
        }
    }
}
