package tech.soft.ehs.cfoodloo.Home;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import tech.soft.ehs.cfoodloo.Activities.AddAreaActivity;
import tech.soft.ehs.cfoodloo.Activities.AddCityActivity;
import tech.soft.ehs.cfoodloo.R;

public class HomeActivity extends AppCompatActivity {

    RecyclerView homeRecyclerView;

    int[] homeIcons = {R.drawable.icon1,R.drawable.icon2,R.drawable.gallery_icon};
    String[] homeName = {"Add City","Add Area","Photo Gallery"};
    HomeDataProvider homeDataProvider;
    HomeAdapter homeAdapter;
    ArrayList<HomeDataProvider> homeList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        homeRecyclerView = findViewById(R.id.home_recyclerview);
        homeRecyclerView.setHasFixedSize(true);
        homeRecyclerView.setLayoutManager(new GridLayoutManager(this,2));

        homeList = new ArrayList<>();

        for (int i=0; i<homeName.length; i++){

            String strHomeName = homeName[i];

            int intIcon = homeIcons[i];
            homeDataProvider= new HomeDataProvider(strHomeName,intIcon);
            homeList.add(homeDataProvider);
        }

        homeAdapter=new HomeAdapter(HomeActivity.this,homeList);
        homeRecyclerView.setAdapter(homeAdapter);

        homeAdapter.setOnItemClickListener(new HomeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                switch (position){

                    case 0:

                        startActivity(new Intent(HomeActivity.this, AddCityActivity.class));

                        break;

                    case 1:

                        startActivity(new Intent(HomeActivity.this, AddAreaActivity.class));

                        break;
                }
            }

            @Override
            public void onButtonChange(int position) {

            }
        });








    }
}
