package tech.soft.ehs.cfoodloo.Activities;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import tech.soft.ehs.cfoodloo.DataProviders.DataProvider;
import tech.soft.ehs.cfoodloo.R;

public class AddCityActivity extends AppCompatActivity {

    EditText addCityEt;
    Button addCityBtn,updateCityBtn;
    ListView cityListLv;

    DatabaseReference databaseReference;
    DataProvider dataProvider;

    List<DataProvider> cityList;
    String[] cityArray;
    ArrayAdapter arrayAdapter;
    AlertDialog dialog;
    int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_city);

        databaseReference = FirebaseDatabase.getInstance().getReference("cities");

        addCityBtn = findViewById(R.id.add_city_btn);
        addCityEt = findViewById(R.id.add_city_et);
        cityListLv= findViewById(R.id.city_list_lv);
        updateCityBtn = findViewById(R.id.update_city_btn);

        addCityBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (addCityEt.getText().toString().length()<3){

                    addCityEt.setError("Put City Name");
                }else {

                    String cityId = databaseReference.push().getKey();
                    dataProvider = new DataProvider(addCityEt.getText().toString(),cityId);
                    databaseReference.child(cityId).setValue(dataProvider);
                    Toast.makeText(AddCityActivity.this, "City Added", Toast.LENGTH_SHORT).show();
                    addCityEt.setText("");
                }

            }
        });
        cityList  = new ArrayList<>();
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                cityList.clear();

                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){

                    dataProvider = dataSnapshot1.getValue(DataProvider.class);
                    cityList.add(dataProvider);


                }

                cityArray = new String[cityList.size()];

                for (int i=0; i<cityArray.length; i++){

                    cityArray[i] = cityList.get(i).getCityName();
                }

                setList();
                cityListLv.setAdapter(arrayAdapter);






            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        cityListLv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                final AlertDialog.Builder alertdialog = new AlertDialog.Builder(AddCityActivity.this);
                final LayoutInflater inflater =getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.delete_update_dialog,null);
                alertdialog.setView(dialogView);
                alertdialog.setTitle("Delete/Update");
                final Button delete, update,showAreas;
                delete = dialogView.findViewById(R.id.delete_btn);
                update = dialogView.findViewById(R.id.update_btn);
                showAreas =dialogView.findViewById(R.id.show_area_btn);

                dialog = alertdialog.create();
                dialog.show();

                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteOperation(position);
                        dialog.dismiss();

                    }
                });

                update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addCityEt.setText(cityList.get(position).getCityName());
                        addCityBtn.setVisibility(View.GONE);
                        updateCityBtn.setVisibility(View.VISIBLE);
                        dialog.dismiss();
                        pos = position;
                    }
                });

                showAreas.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent =  new Intent(AddCityActivity.this, ShowAreasAcitivity.class);
                        intent.putExtra("city",cityList.get(position).getCityName());
                        startActivity(intent);

                    }
                });


                return false;
            }
        });

        updateCityBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (addCityEt.getText().toString().length()<3){

                    addCityEt.setError("Write City Name");
                }else {

                    updateOperation(pos);
                    Toast.makeText(AddCityActivity.this, "Updated", Toast.LENGTH_SHORT).show();
                    addCityEt.setText("");
                }

            }
        });
    }

    public void setList(){
        arrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,cityArray);


    }

    public void deleteOperation(int position){

        databaseReference.child(cityList.get(position).getCityId()).removeValue();


    }

    public void updateOperation(int position){

        dataProvider = new DataProvider(addCityEt.getText().toString(),cityList.get(position).getCityId());
        databaseReference.child(cityList.get(position).getCityId()).setValue(dataProvider);


    }
}
