package tech.soft.ehs.cfoodloo.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import tech.soft.ehs.cfoodloo.DataProviders.DataProvider;
import tech.soft.ehs.cfoodloo.R;

public class ShowAreasAcitivity extends AppCompatActivity {

    ListView areaLv;
    ArrayAdapter<String> arrayAdapter;
    List<DataProvider> areaList;
    String getCity;
    DatabaseReference databaseReference;
    String[] areaArray;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_areas_acitivity);

        Intent intent = getIntent();
        getCity = intent.getStringExtra("city");
        Toast.makeText(this, getCity, Toast.LENGTH_SHORT).show();
        databaseReference = FirebaseDatabase.getInstance().getReference("areas").child(getCity);


        areaLv= findViewById(R.id.area_lv);

        areaLv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                return false;
            }
        });
        areaList = new ArrayList<>();

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                areaList.clear();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){

                    DataProvider dataProvider = dataSnapshot1.getValue(DataProvider.class);
                    areaList.add(dataProvider);
                    areaArray = new String[areaList.size()];
                }



                for (int i=0; i<areaArray.length; i++){

                    areaArray[i] = areaList.get(i).getCityId();

                }

                getAreas(areaArray);
                areaLv.setAdapter(arrayAdapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    public void  getAreas(String[] areaArray){

        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, areaArray);

    }
}
