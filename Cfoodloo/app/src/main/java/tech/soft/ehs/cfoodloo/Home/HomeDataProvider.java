package tech.soft.ehs.cfoodloo.Home;

public class HomeDataProvider {

    String name;
    int picture;

    public HomeDataProvider(String name, int picture) {
        this.name = name;
        this.picture = picture;
    }

    public String getName() {
        return name;
    }

    public int getPicture() {
        return picture;
    }
}
