package tech.soft.ehs.cfoodloo.Home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import tech.soft.ehs.cfoodloo.R;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    private Context mcontext;
    private ArrayList<HomeDataProvider> mList;
    private OnItemClickListener mListener;

    public interface OnItemClickListener{
        void  onItemClick(int position);
        void  onButtonChange(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public HomeAdapter(Context context, ArrayList<HomeDataProvider> list){

        this.mcontext = context;
        this.mList = list;


    }


    @Override
    public HomeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mcontext);
        View view = inflater.inflate(R.layout.home_customise_recyclerview,parent,false);
        ViewHolder viewHolder = new ViewHolder(view,mListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(HomeAdapter.ViewHolder holder, int position) {

       HomeDataProvider homeDataProvider  = mList.get(position);

        TextView homeName = holder.nameEng;
         ImageView homeIcon = holder.icons;

           homeName.setText(homeDataProvider.getName());
           homeIcon.setImageResource(homeDataProvider.getPicture());






    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView nameEng;
        public ImageView icons;



        public ViewHolder(View itemView, final OnItemClickListener listener) {
            super(itemView);


            nameEng =itemView.findViewById(R.id.home_name);
            icons = itemView.findViewById(R.id.home_icon);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listener!=null){

                        int position  = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }

                }
            });
        }
    }
}
