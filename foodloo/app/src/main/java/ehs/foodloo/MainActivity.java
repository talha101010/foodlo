package ehs.foodloo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ehs.foodloo.Activities.HotelsActivity;
import ehs.foodloo.Helpers.Constants;
import ehs.foodloo.Helpers.SharedPrefrences;

public class MainActivity extends AppCompatActivity {

    Button loginactivitybtn;
    Toolbar toolbar;
    TextView textViewToOpenRegistrationActivity;
    private EditText phonenumber,password;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        phonenumber= findViewById(R.id.phonenumber);
        password=findViewById(R.id.password);

        progressDialog = new ProgressDialog(this);


        loginactivitybtn = findViewById(R.id.loginactivitybtn);
        loginactivitybtn.setBackgroundColor(Color.TRANSPARENT);

        textViewToOpenRegistrationActivity = findViewById(R.id.textviewtoopenregistrationactivity);
        textViewToOpenRegistrationActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(MainActivity.this,RegistrationActivity.class));
                finish();

            }
        });

        loginactivitybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phonenumber.getText().length()==0){
                    phonenumber.setError("Enter your phone number");
                }else if (password.getText().length()==0){
                    password.setError("Enter your password");
                }else{
                    checkLoginCredentials();
                }
            }
        });

    }
    private void checkLoginCredentials(){
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Checking Credentials.....");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.LOGIN_USER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                      progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (!jsonObject.getBoolean("message")){
                                SharedPrefrences.getInstance(getApplicationContext()).login(jsonObject.getString("id"),
                                        jsonObject.getString("name"),jsonObject.getString("phone"),jsonObject.getString("address"));
                                Toast.makeText(MainActivity.this, "Logged in successfully", Toast.LENGTH_SHORT).show();
//                                startActivity(new Intent(MainActivity.this,HotelsActivity.class));
                                finish();
                            }else{
                                Toast.makeText(MainActivity.this, jsonObject.getString("detail"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("phonenumber",phonenumber.getText().toString().trim());
                params.put("password",password.getText().toString().trim());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
