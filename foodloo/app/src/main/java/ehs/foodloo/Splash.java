package ehs.foodloo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ehs.foodloo.Activities.HotelsActivity;
import ehs.foodloo.Helpers.Constants;
import ehs.foodloo.Helpers.SharedPrefrences;

public class Splash extends AppCompatActivity {
    private static int TIME_OUT = 4000;
    private String userid,orderid,hotelname,orderstatus,feedback,hotelid;
    AlertDialog dialog;
    private ProgressDialog progressDialog;
    private boolean check1= true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        progressDialog = new ProgressDialog(this);
        SharedPreferences sharedpreferences = this.getSharedPreferences("foodloosharepreferences1", Context.MODE_PRIVATE);
        userid = sharedpreferences.getString("userid",null);
        if (userid != null){
            getLatestOrder();
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (NetworkUtil.isNetworkConnected(Splash.this)) {

                    NetworkUtil networkUtil = new NetworkUtil(Splash.this);
                    networkUtil.execute();
                    if (check1 && userid != null) {
                        if (orderstatus.equals("Pending") || orderstatus.equals("Preparing") || orderstatus.equals("Dispatched")) {
                            showDialog(orderstatus, hotelname);
                        } else if (orderstatus.equals("Delivered") && feedback.equals("null")) {
                            showDeliveredDialog(orderstatus, hotelname);
                        } else {
                            if (SharedPrefrences.getInstance(Splash.this).isAddress()) {
                                startActivity(new Intent(Splash.this, HotelsActivity.class));
                                finish();
                            } else {
                                startActivity(new Intent(Splash.this, AreaSelectionActiviy.class));
                                finish();
                            }
                        }
                    }else{
                        if (SharedPrefrences.getInstance(Splash.this).isAddress()) {
                            startActivity(new Intent(Splash.this, HotelsActivity.class));
                            finish();
                        } else {
                            startActivity(new Intent(Splash.this, AreaSelectionActiviy.class));
                            finish();
                        }
                    }
                }
                else{

                    AlertDialog alertDialog = new AlertDialog.Builder(Splash.this).create();
                    alertDialog.setTitle("Check Internet Connection");
                    alertDialog.setMessage("Yor are not connected with internet");
                    alertDialog.show();
                }


            }
        },TIME_OUT);


        }

        private void getLatestOrder(){
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.GET_LATEST_ORDER,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (!jsonObject.getBoolean("error")){
                                     orderid = jsonObject.getString("id");
                                     hotelname = jsonObject.getString("hotelname");
                                    hotelid = jsonObject.getString("hotelid");
                                     orderstatus = jsonObject.getString("orderstatus");
                                     feedback = jsonObject.getString("feedback");
                                }else{
                                    check1 = false;
                                    Toast.makeText(Splash.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(Splash.this, "Communication server error", Toast.LENGTH_SHORT).show();
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> param = new HashMap<>();
                    param.put("userid",userid);
                    return param;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }

    public void showDialog(String status,String hotelname){

        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(Splash.this);
        LayoutInflater inflater =getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.latest_order_status,null);
        alertdialog.setView(dialogView);
        TextView orderstatusview = dialogView.findViewById(R.id.orderstatus);
        Button skipbtn = dialogView.findViewById(R.id.skipbtn);
        skipbtn.setBackgroundColor(Color.TRANSPARENT);
        ImageView statusimage = dialogView.findViewById(R.id.statusimage);
        if (status.equals("Pending")){
            statusimage.setImageResource(R.drawable.pendingstatusicon);
            orderstatusview.setText("Order is pending");
        }else if (status.equals("Preparing")){
            orderstatusview.setText("Accepted & Resturant is preparing");
            statusimage.setImageResource(R.drawable.preparingicon);
        }else if (status.equals("Dispatched")){
            statusimage.setImageResource(R.drawable.dispatchedicon);
            orderstatusview.setText("Order picked up & On the Way");
        }

        alertdialog.setCancelable(false);
        skipbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Splash.this, HotelsActivity.class));
                finish();
            }
        });

        dialog = alertdialog.create();
        dialog.show();
    }

    public void showDeliveredDialog(String status,String hotelname){

        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(Splash.this);
        LayoutInflater inflater =getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.latest_delivered_order_status,null);
        alertdialog.setView(dialogView);
        TextView hotelnametextview = dialogView.findViewById(R.id.hotelname);
        Button skipbtn = dialogView.findViewById(R.id.skipbtn);
        final RatingBar ratingBar = dialogView.findViewById(R.id.ratingbar);
        final EditText feedbackedittext = dialogView.findViewById(R.id.feedbackedittext);
        Button ratebtn = dialogView.findViewById(R.id.ratebtn);
        hotelnametextview.setText(hotelname);
        alertdialog.setCancelable(false);
        skipbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Splash.this, HotelsActivity.class));
                finish();
                addRating(-1,"no feedback",false);
            }
        });

        ratebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             addRating(ratingBar.getRating(),feedbackedittext.getText().toString().trim(),true);
            }
        });

        dialog = alertdialog.create();
        dialog.show();
    }

    private void addRating(final float rating, final String userfeedback, final boolean check){
        if(check){
            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.INSERT_FEEDBACK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (check){
                            progressDialog.dismiss();
                        }

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (!jsonObject.getBoolean("error")){
                                if (check){
                                    Toast.makeText(Splash.this, "Thank you for giving us feedback", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(Splash.this, HotelsActivity.class));
                                    finish();
                                }

                            }else{
                                Toast.makeText(Splash.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (check){
                    progressDialog.dismiss();
                    Toast.makeText(Splash.this, "Communication server error", Toast.LENGTH_SHORT).show();
                }

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("orderid",orderid);
                param.put("rating",rating+"");
                param.put("feedback",userfeedback);
                param.put("hotelid",hotelid);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    }
