package ehs.foodloo.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import ehs.foodloo.Activities.HotelsDetailActivity;
import ehs.foodloo.DataProviders.BillDataProvider;
import ehs.foodloo.DataProviders.Items_Data_Provider;

import ehs.foodloo.R;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ViewHolder> {
    private ArrayList<Items_Data_Provider> list;
    private Context context;
    private SharedPreferences appSharePreference;
    private int index;

    public ItemsAdapter(ArrayList<Items_Data_Provider> list, Context context) {
        this.list = list;
        this.context = context;
        appSharePreference = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
      View v = LayoutInflater.from(context).inflate(R.layout.items_list,viewGroup,false);
      return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        Items_Data_Provider listitems = list.get(i);

        if (verifyOrder(i,viewHolder.quantity)){
            viewHolder.addtocartbtn.setVisibility(View.GONE);
            viewHolder.add.setVisibility(View.VISIBLE);
            viewHolder.minus.setVisibility(View.VISIBLE);
            viewHolder.quantity.setVisibility(View.VISIBLE);
        }else{
            viewHolder.addtocartbtn.setVisibility(View.VISIBLE);
            viewHolder.add.setVisibility(View.GONE);
            viewHolder.minus.setVisibility(View.GONE);
            viewHolder.quantity.setVisibility(View.GONE);
        }

        viewHolder.itemname.setText(listitems.getItemname());
        viewHolder.itemunit.setText(listitems.getItemunit());
        viewHolder.itemprice.setText("PKR\t"+listitems.getItemprice());

        Picasso.get().load(listitems.getImage()).fit().into(viewHolder.itemimage, new Callback() {
            @Override
            public void onSuccess() {
              viewHolder.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {

            }
        });

        viewHolder.addtocartbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (addToCart(i)){
                   viewHolder.addtocartbtn.setVisibility(View.GONE);
                   viewHolder.add.setVisibility(View.VISIBLE);
                   viewHolder.minus.setVisibility(View.VISIBLE);
                   viewHolder.quantity.setVisibility(View.VISIBLE);
                   viewHolder.quantity.setText("1");
               }
            }
        });

        viewHolder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int q = Integer.parseInt(viewHolder.quantity.getText().toString());
                q += 1;
                viewHolder.quantity.setText(q + "");
                updateCart(q+"" , i);
            }
        });

        viewHolder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int q = Integer.parseInt(viewHolder.quantity.getText().toString());
                if (q != 1 ){
                    q -= 1;
                    viewHolder.quantity.setText(q+"");
                    updateCart(q+"",i);
                }else{
                    if (RemoveOrder(list.get(i).getId())){
                        notifyDataSetChanged();
                        HotelsDetailActivity.cartNumber();
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private boolean addToCart(int position){
        ArrayList<BillDataProvider> arrayList = null;
        Gson gson = new Gson();
        Type type = new TypeToken<List<BillDataProvider>>() {
        }.getType();
        String json = appSharePreference.getString("cart","");
        if (!json.equals("")){
            arrayList = gson.fromJson(json,type);
        }else{
            arrayList = new ArrayList<>();
        }

        BillDataProvider billHolder = new BillDataProvider();
        billHolder.setItemid(list.get(position).getId());
        billHolder.setItemname(list.get(position).getItemname());
        billHolder.setItemunit(list.get(position).getItemunit());
        billHolder.setItemprice(list.get(position).getItemprice());
        billHolder.setItemimage(list.get(position).getImage());
        billHolder.setItemquantity("1");
        arrayList.add(billHolder);

        SharedPreferences.Editor prefsEditor = appSharePreference.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("cart",jsons);
        prefsEditor.commit();
        HotelsDetailActivity.cartNumber();
        return true;

    }

    public void updateCart(String quantity, int position) {
        ArrayList<BillDataProvider> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillDataProvider>>() {
        }.getType();

        String json = appSharePreference.getString("cart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            arrayList = new ArrayList<>();

        for (int i = 0; i < arrayList.size(); i++) {


            if (arrayList.get(i).getItemid().equals(list.get(position).getId())) {
                index = i;
                BillDataProvider bill = arrayList.get(i);
                bill.setItemquantity(quantity);
                break;
            }


        }


        SharedPreferences.Editor prefsEditor = appSharePreference.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("cart", jsons);
        prefsEditor.commit();
    }

    public boolean verifyOrder(int position, TextView quantity) {
        ArrayList<BillDataProvider> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillDataProvider>>() {
        }.getType();

        String json = appSharePreference.getString("cart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            return false;


        for (int i = 0; i < arrayList.size(); i++) {


            if (arrayList.get(i).getItemid().equals(list.get(position).getId())) {
                index = i;
                quantity.setText(arrayList.get(index).getItemquantity());
                return true;

            }


        }

        return false;
    }

    public boolean RemoveOrder(String id) {
        ArrayList<BillDataProvider> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillDataProvider>>() {
        }.getType();

        String json = appSharePreference.getString("cart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            return false;


        for (int i = 0; i < arrayList.size(); i++) {


            if (arrayList.get(i).getItemid().equals(id)) {
                index = i;
                arrayList.remove(index);
                break;
            }


        }

        SharedPreferences.Editor prefsEditor = appSharePreference.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("cart", jsons);
        prefsEditor.commit();
        return true;


    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView itemimage;
        TextView itemname,itemunit,itemprice,quantity;
        ProgressBar progressBar;
        Button addtocartbtn,add,minus;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemimage = itemView.findViewById(R.id.hotelimage1);
            itemname = itemView.findViewById(R.id.itemname);
            itemunit = itemView.findViewById(R.id.itemunit);
            itemprice = itemView.findViewById(R.id.itemprice);
            progressBar = itemView.findViewById(R.id.progress);
            addtocartbtn = itemView.findViewById(R.id.addtocart_fooditem);
            add = itemView.findViewById(R.id.plus);
            minus = itemView.findViewById(R.id.minus);
            quantity = itemView.findViewById(R.id.quantity);

        }
    }


}
