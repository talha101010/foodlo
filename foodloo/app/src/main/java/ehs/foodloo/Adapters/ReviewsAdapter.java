package ehs.foodloo.Adapters;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import ehs.foodloo.DataProviders.FoodMenuDataProvider;
import ehs.foodloo.DataProviders.ReviewsDataProvider;
import ehs.foodloo.R;

public class ReviewsAdapter  extends RecyclerView.Adapter<ReviewsAdapter.ViewHolder> {

    private Context mcontext;
    private ArrayList<ReviewsDataProvider> mList;
    private ReviewsAdapter.OnItemClickListener mListener;
    int index =0;

    public interface OnItemClickListener{
        void  onItemClick(int position);
        void  onButtonChange(int position);
    }

    public void setOnItemClickListener(ReviewsAdapter.OnItemClickListener listener){
        mListener = listener;
    }

    public ReviewsAdapter(Context context, ArrayList<ReviewsDataProvider> list){

        this.mcontext = context;
        this.mList = list;


    }


    @Override
    public ReviewsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mcontext);
        View view = inflater.inflate(R.layout.reviews_customise_recyclerview,parent,false);
       ReviewsAdapter.ViewHolder viewHolder = new ReviewsAdapter.ViewHolder(view,mListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ReviewsAdapter.ViewHolder holder,final int position) {

        ReviewsDataProvider reviewsDataProvider  = mList.get(position);
        final int row ;
        TextView customerName = holder.customerName;
        TextView review_statment = holder.review_statment;
        TextView reviewDate = holder.reviewDate;
        RatingBar reviewsRating = holder.ratingValue;

        customerName.setText(reviewsDataProvider.getCustomerName());
        review_statment.setText(reviewsDataProvider.getReviewStatment());
        reviewsRating.setRating(reviewsDataProvider.getRatingValue());
        reviewDate.setText(reviewsDataProvider.getReviewDate());





    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView customerName,review_statment,reviewDate;
        RatingBar ratingValue;




        public ViewHolder(View itemView, final ReviewsAdapter.OnItemClickListener listener) {
            super(itemView);


            customerName =itemView.findViewById(R.id.customer_name_textview);
            review_statment = itemView.findViewById(R.id.review_statment_textview);
            ratingValue = itemView.findViewById(R.id.reviews_rating_bar);
            reviewDate=itemView.findViewById(R.id.review_date_textview);




            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listener!=null){

                        int position  = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }

                }
            });
        }
    }
}
