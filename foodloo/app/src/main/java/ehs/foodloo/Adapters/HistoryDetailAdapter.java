package ehs.foodloo.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import ehs.foodloo.DataProviders.OrderHistoryDetailList;
import ehs.foodloo.R;

public class HistoryDetailAdapter extends RecyclerView.Adapter<HistoryDetailAdapter.ViewHolder> {
    private List<OrderHistoryDetailList> list;
    private Context context;

    public HistoryDetailAdapter(List<OrderHistoryDetailList> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
     View v = LayoutInflater.from(context).inflate(R.layout.history_detail_items,viewGroup,false);
     return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
          OrderHistoryDetailList listitems = list.get(i);
        viewHolder.itemname.setText(listitems.getItemname());
        viewHolder.quantity.setText("Quantity:"+listitems.getItemquantity());
        viewHolder.itemprice.setText("Price:"+listitems.getItemprice());
        viewHolder.itemunit.setText("Unit:"+listitems.getItemunit());
        Picasso.get().load(listitems.getImage()).fit().into(viewHolder.itemimage, new Callback() {
            @Override
            public void onSuccess() {
                viewHolder.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView itemimage;
        TextView itemname,itemunit,itemprice,quantity;
        ProgressBar progressBar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemimage = itemView.findViewById(R.id.itemimage);
            itemname = itemView.findViewById(R.id.itemname);
            itemunit = itemView.findViewById(R.id.itemunit);
            itemprice = itemView.findViewById(R.id.itemprice);
            progressBar = itemView.findViewById(R.id.progress);
            quantity = itemView.findViewById(R.id.itemquantity);
        }
    }
}
