package ehs.foodloo.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ehs.foodloo.Activities.OrderHistoryDetail;
import ehs.foodloo.DataProviders.OrderHistoryListItems;
import ehs.foodloo.R;

public class OrderHistoryDetailAdapter extends RecyclerView.Adapter<OrderHistoryDetailAdapter.ViewHolder> {
    private List<OrderHistoryListItems> list;
    private Context context;

    public OrderHistoryDetailAdapter(List<OrderHistoryListItems> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
      View v = LayoutInflater.from(context).inflate(R.layout.orderhistory_listitems,viewGroup,false);
      return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
         final OrderHistoryListItems listItems = list.get(i);
         viewHolder.orderid.setText(listItems.getOrderid());
         viewHolder.date.setText(listItems.getDate());
         viewHolder.totalbill.setText(listItems.getBill());
         viewHolder.orderstatus.setText(listItems.getStatus());
         if (!listItems.getStatus().equals("Pending")){
              if (listItems.getStatus().equals("Preparing")){
                  viewHolder.approveimage.setImageResource(R.drawable.approvecolor);
                  viewHolder.preparingimage.setImageResource(R.drawable.preparationcolor);
              }else if (listItems.getStatus().equals("Dispatched")){
                  viewHolder.approveimage.setImageResource(R.drawable.approvecolor);
                  viewHolder.preparingimage.setImageResource(R.drawable.preparationcolor);
                  viewHolder.dispatchimage.setImageResource(R.drawable.deliverycolor);
              }else if (listItems.getStatus().equals("Delivered")){
                  viewHolder.approveimage.setImageResource(R.drawable.approvecolor);
                  viewHolder.preparingimage.setImageResource(R.drawable.preparationcolor);
                  viewHolder.dispatchimage.setImageResource(R.drawable.deliverycolor);
                  viewHolder.deliveryimage.setImageResource(R.drawable.delieveredcolor);
              }
         }
         viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Intent intent = new Intent(context,OrderHistoryDetail.class);
                 intent.putExtra("orderid",listItems.getOrderid());
                 context.startActivity(intent);
             }
         });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView orderid,date,totalbill,orderstatus;
        ImageView approveimage,preparingimage,dispatchimage,deliveryimage;
        CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            orderid = itemView.findViewById(R.id.orderid);
            date = itemView.findViewById(R.id.date);
            totalbill = itemView.findViewById(R.id.totalbill);
            approveimage = itemView.findViewById(R.id.approveimge);
            preparingimage = itemView.findViewById(R.id.preparingimage);
            dispatchimage = itemView.findViewById(R.id.dispatchimage);
            deliveryimage = itemView.findViewById(R.id.deliveredimage);
            cardView = itemView.findViewById(R.id.card_view);
            orderstatus = itemView.findViewById(R.id.orderstatus);
        }
    }
}
