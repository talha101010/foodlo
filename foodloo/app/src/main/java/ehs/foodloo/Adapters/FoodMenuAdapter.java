package ehs.foodloo.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ehs.foodloo.Activities.HotelsDetailActivity;
import ehs.foodloo.DataProviders.FoodMenuDataProvider;
import ehs.foodloo.DataProviders.HotelsDataProvider;
import ehs.foodloo.DataProviders.Items_Data_Provider;
import ehs.foodloo.Helpers.Constants;
import ehs.foodloo.R;

public class FoodMenuAdapter   extends RecyclerView.Adapter<FoodMenuAdapter.ViewHolder> {

    private Context mcontext;
    private ArrayList<FoodMenuDataProvider> mList;
    private FoodMenuAdapter.OnItemClickListener mListener;
    int index =0;

    public interface OnItemClickListener{
        void  onItemClick(int position);
        void  onButtonChange(int position);
    }

    public void setOnItemClickListener(FoodMenuAdapter.OnItemClickListener listener){
        mListener = listener;
    }

    public FoodMenuAdapter(Context context, ArrayList<FoodMenuDataProvider> list){

        this.mcontext = context;
        this.mList = list;


    }


    @Override
    public FoodMenuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mcontext);
        View view = inflater.inflate(R.layout.food_menu_customise_recyclerview,parent,false);
        FoodMenuAdapter.ViewHolder viewHolder = new FoodMenuAdapter.ViewHolder(view,mListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final FoodMenuAdapter.ViewHolder holder,final int position) {

        final FoodMenuDataProvider foodDataProvider  = mList.get(position);
         final int row ;
        TextView menuName = holder.menuName;
        menuName.setText(foodDataProvider.getMenu_name());
        RelativeLayout menuItem = holder.menuItemLayout;
      if (index == position){

          holder.menuName.setTextColor(ResourcesCompat.getColor(mcontext.getResources(),R.color.colorPrimaryDark,null));
          holder.menuName.setBackgroundResource(R.color.textcolor);

      }else {

          holder.menuName.setTextColor(ResourcesCompat.getColor(mcontext.getResources(),R.color.textcolor,null));
          holder.menuName.setBackgroundResource(R.color.colorPrimaryDark);


      }

      holder.menuItemLayout.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {

              holder.menuName.setTextColor(ResourcesCompat.getColor(mcontext.getResources(),R.color.colorPrimaryDark,null));
              holder.menuName.setBackgroundResource(R.color.textcolor);
            //  new HotelsDetailActivity().getItemsFromServer(foodDataProvider.getHotelid(),foodDataProvider.getMenuid(),mcontext);
              getItemsFromServer(foodDataProvider.getHotelid(),foodDataProvider.getMenuid(),mcontext);
              index = position;

              notifyDataSetChanged();

          }
      });

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView menuName;
        public RelativeLayout menuItemLayout;



        public ViewHolder(View itemView, final FoodMenuAdapter.OnItemClickListener listener) {
            super(itemView);


             menuName =itemView.findViewById(R.id.menuName);
             menuItemLayout = itemView.findViewById(R.id.menuitemlayout);



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listener!=null){

                        int position  = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }

                }
            });
        }
    }

    public void getItemsFromServer(final String hid, final String mid, final Context context){
         HotelsDetailActivity.progressBar.setVisibility(View.VISIBLE);
        HotelsDetailActivity.nodata.setVisibility(View.GONE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.SHOW_ITEMS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        HotelsDetailActivity.progressBar.setVisibility(View.GONE);
                        try {
                            if (HotelsDetailActivity.itemsadapter != null){
                                HotelsDetailActivity.itemslist.clear();
                            }
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("items");
                            if (jsonArray.length()>0){
                                for (int i =0;i<jsonArray.length();i++){
                                    JSONObject json = jsonArray.getJSONObject(i);
                                    Items_Data_Provider list = new Items_Data_Provider(json.getString("id"),
                                            json.getString("name"),json.getString("unit"),json.getString("price"),
                                            json.getString("image"));
                                    HotelsDetailActivity.itemslist.add(list);
                                }

                                HotelsDetailActivity.itemsadapter = new ItemsAdapter(HotelsDetailActivity.itemslist,context);
                                HotelsDetailActivity.itemsrecyclerview.setAdapter(HotelsDetailActivity.itemsadapter);


                            }else{
                                HotelsDetailActivity.nodata.setVisibility(View.VISIBLE);
                                HotelsDetailActivity.itemsadapter = new ItemsAdapter(HotelsDetailActivity.itemslist,context);
                                HotelsDetailActivity.itemsrecyclerview.setAdapter(HotelsDetailActivity.itemsadapter);
                                HotelsDetailActivity.itemsadapter.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                 HotelsDetailActivity.progressBar.setVisibility(View.GONE);
                Toast.makeText(context, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("hotelid",hid);
                param.put("menuid",mid);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }
}
