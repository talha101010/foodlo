package ehs.foodloo.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ehs.foodloo.Activities.HotelsDetailActivity;
import ehs.foodloo.DataProviders.BillDataProvider;
import ehs.foodloo.DataProviders.HotelsDataProvider;
import ehs.foodloo.Helpers.SharedPrefrences;
import ehs.foodloo.R;

public class ShowHotelsAdapter extends RecyclerView.Adapter<ShowHotelsAdapter.ViewHolder> {
    private List<HotelsDataProvider> list;
    private Context context;
    private SharedPreferences appSharePreferences;

    public ShowHotelsAdapter(List<HotelsDataProvider> list, Context context) {
        this.list = list;
        this.context = context;
        appSharePreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.hotels_customise_recyclerview,viewGroup,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        final HotelsDataProvider listview = list.get(i);
        viewHolder.hotel_status.setText(listview.getHotel_status());
        viewHolder.nameEng.setText(listview.getHotel_name());
        Picasso.get().load(listview.getHotel_icon()).fit().into(viewHolder.hotelimage, new Callback() {
            @Override
            public void onSuccess() {
                viewHolder.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {

            }
        });

        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    SharedPreferences preferences = context.getSharedPreferences("foodloosharepreferences", Context.MODE_PRIVATE);
                    if (!preferences.getString("hotelid","").equals("")){
                         if (preferences.getString("hotelid","").equals(listview.getId())){

                             Intent intent = new Intent(context, HotelsDetailActivity.class);
                             intent.putExtra("hotelname", listview.getHotel_name());
                             intent.putExtra("hoteladdress", listview.getAddress());
                             intent.putExtra("minorder", listview.getMinorder());
                             intent.putExtra("phone", listview.getPhone());
                             intent.putExtra("description", listview.getDescription());
                             intent.putExtra("charges", listview.getDeliverycharges());
                             intent.putExtra("timing", listview.getTiming());
                             intent.putExtra("hotelid", listview.getId());
                             intent.putExtra("image", listview.getHotel_icon());
                             intent.putExtra("status", listview.getHotel_status());
                             intent.putExtra("preparationtime", listview.getPreparationtime());
                             context.startActivity(intent);

                         }else{

                             if (appSharePreferences.getString("cart","").equals("")){

                                 Intent intent = new Intent(context, HotelsDetailActivity.class);
                                 intent.putExtra("hotelname", listview.getHotel_name());
                                 intent.putExtra("hoteladdress", listview.getAddress());
                                 intent.putExtra("minorder", listview.getMinorder());
                                 intent.putExtra("phone", listview.getPhone());
                                 intent.putExtra("description", listview.getDescription());
                                 intent.putExtra("charges", listview.getDeliverycharges());
                                 intent.putExtra("timing", listview.getTiming());
                                 intent.putExtra("hotelid", listview.getId());
                                 intent.putExtra("image", listview.getHotel_icon());
                                 intent.putExtra("status", listview.getHotel_status());
                                 intent.putExtra("preparationtime", listview.getPreparationtime());
                                 context.startActivity(intent);

                             }else{

                                 final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                 alertDialog.setMessage("As you are going to new hotel your previous cart will be cleared.Are you sured you want to continue?");
                                 alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                     @Override
                                     public void onClick(DialogInterface dialog, int which) {
                                         if (clearCart()) {

                                             Intent intent = new Intent(context, HotelsDetailActivity.class);
                                             intent.putExtra("hotelname", listview.getHotel_name());
                                             intent.putExtra("hoteladdress", listview.getAddress());
                                             intent.putExtra("minorder", listview.getMinorder());
                                             intent.putExtra("phone", listview.getPhone());
                                             intent.putExtra("description", listview.getDescription());
                                             intent.putExtra("charges", listview.getDeliverycharges());
                                             intent.putExtra("timing", listview.getTiming());
                                             intent.putExtra("hotelid", listview.getId());
                                             intent.putExtra("image", listview.getHotel_icon());
                                             intent.putExtra("status", listview.getHotel_status());
                                             intent.putExtra("preparationtime", listview.getPreparationtime());
                                             context.startActivity(intent);

                                         }
                                     }
                                 });
                                 alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                     @Override
                                     public void onClick(DialogInterface dialog, int which) {
                                         dialog.cancel();
                                     }
                                 });

                                 alertDialog.show();
                             }
                         }
                    }else {
                        Intent intent = new Intent(context, HotelsDetailActivity.class);
                        intent.putExtra("hotelname", listview.getHotel_name());
                        intent.putExtra("hoteladdress", listview.getAddress());
                        intent.putExtra("minorder", listview.getMinorder());
                        intent.putExtra("phone", listview.getPhone());
                        intent.putExtra("description", listview.getDescription());
                        intent.putExtra("charges", listview.getDeliverycharges());
                        intent.putExtra("timing", listview.getTiming());
                        intent.putExtra("hotelid", listview.getId());
                        intent.putExtra("image", listview.getHotel_icon());
                        intent.putExtra("status", listview.getHotel_status());
                        intent.putExtra("preparationtime", listview.getPreparationtime());
                        context.startActivity(intent);
                    }

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView nameEng,hotel_status;
        public CardView cardView;
        public ImageView hotelimage;
        public ProgressBar progressBar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView =  itemView.findViewById(R.id.cardview);
            nameEng =itemView.findViewById(R.id.hotel_name_textview);
            hotelimage = itemView.findViewById(R.id.hotelimage);
            hotel_status = itemView.findViewById(R.id.hotel_status_textview);
            progressBar = itemView.findViewById(R.id.progressbar);
        }
    }

    private boolean clearCart() {
        ArrayList<BillDataProvider> arrayList = null;
        Gson gson = new Gson();
        arrayList = new ArrayList<>();
        SharedPreferences.Editor prefsEditor = appSharePreferences.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("cart", jsons);
        prefsEditor.remove("cart");
        prefsEditor.commit();
        return true;
    }
}
