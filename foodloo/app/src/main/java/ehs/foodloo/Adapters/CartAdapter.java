package ehs.foodloo.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ehs.foodloo.Activities.CartActivity;
import ehs.foodloo.Activities.HotelsDetailActivity;
import ehs.foodloo.DataProviders.BillDataProvider;
import ehs.foodloo.R;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {
    private ArrayList<BillDataProvider> list;
    private Context context;

    public CartAdapter(ArrayList<BillDataProvider> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
       View v = LayoutInflater.from(context).inflate(R.layout.row_cart,viewGroup,false);
       return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
         viewHolder.itemname.setText(list.get(i).getItemname());
         viewHolder.itemunit.setText(list.get(i).getItemunit());
         viewHolder.quantity.setText(list.get(i).getItemquantity());
         int price = Integer.parseInt(list.get(i).getItemprice()) * Integer.parseInt(list.get(i).getItemquantity());
         viewHolder.itemprice.setText(price+"");
        Picasso.get().load(list.get(i).getItemimage()).fit().into(viewHolder.itemimage, new Callback() {
            @Override
            public void onSuccess() {
                viewHolder.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {

            }
        });

        viewHolder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int pos = i;


                int price = Integer.parseInt(list.get(pos).getItemprice());
                int quantity = Integer.parseInt(list.get(pos).getItemquantity());


                quantity += 1;
                list.get(pos).setItemquantity(quantity + "");
//                price *= quantity;
//                list.get(pos).setItemprice(price + "");


                Gson gson = new Gson();
                SharedPreferences appSharedPrefs= PreferenceManager
                        .getDefaultSharedPreferences(context);
                SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
                String jsons = gson.toJson(list);
                prefsEditor.putString("cart", jsons);
                prefsEditor.commit();
                notifyDataSetChanged();
                CartActivity. calculateBill();


            }
        });

        viewHolder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int quantity = Integer.parseInt(list.get(i).getItemquantity());

                if (quantity != 1) {
                    int price = Integer.parseInt(list.get(i).getItemprice());
                    quantity -= 1;
                    list.get(i).setItemquantity(quantity + "");
//                    price *= quantity;
//                    list.get(i).setItemprice(price + "");

                    Gson gson = new Gson();
                    SharedPreferences   appSharedPrefs= PreferenceManager
                            .getDefaultSharedPreferences(context);
                    SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
                    String jsons = gson.toJson(list);
                    prefsEditor.putString("cart", jsons);
                    prefsEditor.commit();
                    notifyDataSetChanged();
                    CartActivity.  calculateBill();
                }

            }
        });

        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                list.remove(i);



                Gson gson = new Gson();
                SharedPreferences   appSharedPrefs= PreferenceManager
                        .getDefaultSharedPreferences(context);
                SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
                String jsons = gson.toJson(list);
                prefsEditor.putString("cart", jsons);
                prefsEditor.commit();
                notifyDataSetChanged();
               CartActivity. calculateBill();
                HotelsDetailActivity.cartNumber();
//                CartActivity.handlearrow();

            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView itemname,itemprice,quantity,itemunit;
        ImageView itemimage;
        Button plus,minus,delete;
        ProgressBar progressBar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemname = itemView.findViewById(R.id.itemname);
            itemprice = itemView.findViewById(R.id.itemprice);
            itemimage = itemView.findViewById(R.id.itemimage);
            quantity = itemView.findViewById(R.id.quantity);
            plus = itemView.findViewById(R.id.plus);
            minus = itemView.findViewById(R.id.minus);
            delete = itemView.findViewById(R.id.delete);
            itemunit = itemView.findViewById(R.id.itemunit);
            progressBar = itemView.findViewById(R.id.progress);
        }
    }
}
