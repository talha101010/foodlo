package ehs.foodloo.Helpers;

public class Constants {
 //   private static final String ROOT_URL = "http://192.168.8.100/foodloo/foodloowebservices/";
   private static final String ROOT_URL = "http://www.ilmistaan.com/foodloo/foodloowebservices/";

    public static int totallbill;

    public static final String SHOW_CITIES = ROOT_URL+"showCities.php";
    public static final String SHOW_AREAS = ROOT_URL+"showAreas.php";
    public static final String SHOW_HOTELS = ROOT_URL+"showHotels.php";
    public static final String SHOW_MENUS = ROOT_URL+"showMenus.php";
    public static final String SHOW_ITEMS = ROOT_URL+"showItems.php";
    public static final String SHOW_HOTEL_AREA = ROOT_URL+"showHotelAreas.php";
    public static final String SHOW_TIME = ROOT_URL+"showTime.php";
    public static final String INSERT_ORDER=ROOT_URL+"insertOrder.php";
    public static final String REGISTER_USER = ROOT_URL+"registerUser.php";
    public static final String LOGIN_USER = ROOT_URL+"login.php";
    public static final String GET_LATEST_ORDER = ROOT_URL+"showLatestOrder.php";
    public static final String INSERT_FEEDBACK =ROOT_URL+"insertFeedback.php";
    public static final String ORDER_HISTORY = ROOT_URL+"showUserOrderHistory.php";
    public static final String ORDER_HISTORY_DETAIL = ROOT_URL+"orderHistoryDetail.php";
}
