package ehs.foodloo.Helpers;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefrences {
    private static SharedPrefrences mInstance;

    public static final String SHARE_PREF_NAME = "foodloosharepreferences";
    public static final String SHARE_PREF_NAME1 = "foodloosharepreferences1";


    private static Context mCtx;

    private SharedPrefrences(Context context) {
        mCtx = context;


    }

    public static synchronized SharedPrefrences getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefrences(context);
        }
        return mInstance;
    }
    public boolean address(String city,String area,String cityid,String areaid){
        SharedPreferences pref = mCtx.getSharedPreferences(SHARE_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("city",city);
        editor.putString("area",area);
        editor.putString("cityid",cityid);
        editor.putString("areaid",areaid);
        editor.apply();

        return true;
    }
    public boolean login(String userid,String name,String phone,String address){
        SharedPreferences pref = mCtx.getSharedPreferences(SHARE_PREF_NAME1, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("userid",userid);
        editor.putString("name",name);
        editor.putString("phone",phone);
        editor.putString("address",address);
        editor.apply();

        return true;
    }

    public boolean hotelid(String id){
        SharedPreferences pref = mCtx.getSharedPreferences(SHARE_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("hotelid",id);
        editor.apply();

        return true;
    }
    public boolean isLoggedIn(){
        SharedPreferences sharedpreferences = mCtx.getSharedPreferences(SHARE_PREF_NAME1, Context.MODE_PRIVATE);
        if(sharedpreferences.getString("userid",null) != null){
            return true;
        }
        return false;
    }
    public boolean isAddress(){
        SharedPreferences sharedpreferences = mCtx.getSharedPreferences(SHARE_PREF_NAME, Context.MODE_PRIVATE);
        if(sharedpreferences.getString("city",null) != null){
            return true;
        }
        return false;
    }
    public boolean logout(){
        SharedPreferences pref = mCtx.getSharedPreferences(SHARE_PREF_NAME1, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.apply();
        return true;
    }
}
