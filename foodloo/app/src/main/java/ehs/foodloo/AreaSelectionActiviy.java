package ehs.foodloo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ehs.foodloo.Activities.HotelsActivity;
import ehs.foodloo.Helpers.Constants;
import ehs.foodloo.Helpers.SharedPrefrences;

public class AreaSelectionActiviy extends AppCompatActivity {

    TextView textViewToOpenCitySelectionPopup, textViewToOpenAreaSelectionPopup,cityidtextview,areaidtextview;
    ListView selectListview;
    ArrayAdapter<String> cityAdapter,areaAdapter;
    ArrayList<String> citiesarray,citiesidarray,areasarray,areasidarray;
    AlertDialog dialog;
    private ProgressBar cityprogress,areaprogress;
    private int cityid,areaid;
    Button goBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_selection_activiy);
        views();
        getCitiesFromServer();
        textViewToOpenAreaSelectionPopup.setEnabled(false);
        goBtn.setBackgroundColor(Color.TRANSPARENT);

        goBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkSelection();
            }
        });

        textViewToOpenCitySelectionPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                selectDialog(getResources().getStringArray(R.array.citiesarray),"Select your city");
                selectDialog(citiesarray,"Select your city");
                selectListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        textViewToOpenAreaSelectionPopup.setEnabled(true);
                        textViewToOpenAreaSelectionPopup.setText("Select Area");

                        String selectedCity =  cityAdapter.getItem(position).toString();
                         cityid = cityAdapter.getPosition(cityAdapter.getItem(position));
                        textViewToOpenCitySelectionPopup.setText(selectedCity);
                        cityidtextview.setText(citiesidarray.get(cityid).toString());
                        if (areaAdapter != null){
                            areaAdapter.clear();
                        }
                        getAreasFromServer();
                        dialog.dismiss();

                    }
                });



            }
        });


        textViewToOpenAreaSelectionPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectDialogArea(areasarray,"Select your area");
                selectListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String selectedCity =  areaAdapter.getItem(position).toString();
                        areaid = areaAdapter.getPosition(areaAdapter.getItem(position));
                        textViewToOpenAreaSelectionPopup.setText(selectedCity);
                        areaidtextview.setText(areasidarray.get(areaid));
                        dialog.dismiss();

                    }
                });

            }
        });
    }

    public void views(){

        // textviews of select city and areas

        textViewToOpenCitySelectionPopup = findViewById(R.id.textViewToOpenCitySelectionPopup);
        textViewToOpenAreaSelectionPopup=findViewById(R.id.textViewToOpenAreaSelectionPopup);
        // submit button
        goBtn = findViewById(R.id.gobtn);
        citiesarray = new ArrayList<String>();
        citiesidarray = new ArrayList<String>();
        areasarray = new ArrayList<String>();
        areasidarray = new ArrayList<String>();
        cityprogress = findViewById(R.id.cityprogressbar);
        areaprogress = findViewById(R.id.areaprogressbar);
        cityidtextview = findViewById(R.id.cityid);
        areaidtextview = findViewById(R.id.areaid);


        //   adit texts here


    }

    public void selectDialog(ArrayList<String> param,String parm1){

        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(AreaSelectionActiviy.this);
        LayoutInflater inflater =getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.listview_dialog,null);
        alertdialog.setView(dialogView);
        alertdialog.setTitle(parm1);
        selectListview = dialogView.findViewById(R.id.selectlistview_in_dialog);
        EditText editText = dialogView.findViewById(R.id.searchtext);
        cityAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,param);
        selectListview.setAdapter(cityAdapter);
        selectListview.setTextFilterEnabled(true);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                AreaSelectionActiviy.this.cityAdapter.getFilter().filter(s);
            }
        });

        dialog = alertdialog.create();
        dialog.show();
    }

    public void selectDialogArea(ArrayList<String> param,String parm1){

        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(AreaSelectionActiviy.this);
        LayoutInflater inflater =getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.listview_dialog,null);
        alertdialog.setView(dialogView);
        alertdialog.setTitle(parm1);
        selectListview = dialogView.findViewById(R.id.selectlistview_in_dialog);
        EditText editText = dialogView.findViewById(R.id.searchtext);
        areaAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,param);
        selectListview.setAdapter(areaAdapter);
        selectListview.setTextFilterEnabled(true);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                AreaSelectionActiviy.this.areaAdapter.getFilter().filter(s);
            }
        });

        dialog = alertdialog.create();
        dialog.show();
    }

    public void checkSelection(){

        if (textViewToOpenCitySelectionPopup.getText().toString().equals("Select City")){

            Toast.makeText(this, "Please select city", Toast.LENGTH_SHORT).show();
        }else if (textViewToOpenAreaSelectionPopup.getText().toString().equals("Select Area")){

            Toast.makeText(this, "Please select area", Toast.LENGTH_SHORT).show();
        }else {
            SharedPrefrences.getInstance(getApplicationContext()).address(textViewToOpenCitySelectionPopup.getText().toString(),textViewToOpenAreaSelectionPopup.getText().toString(),cityidtextview.getText().toString(),areaidtextview.getText().toString());
            Intent intent = new Intent(AreaSelectionActiviy.this, HotelsActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void getCitiesFromServer(){
         cityprogress.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.SHOW_CITIES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        cityprogress.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("cities");
                            for (int i =0;i<jsonArray.length();i++){
                                JSONObject json = jsonArray.getJSONObject(i);
                                citiesidarray.add(json.getString("id"));
                                citiesarray.add(json.getString("name"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(AreaSelectionActiviy.this, e.getMessage()+"", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cityprogress.setVisibility(View.GONE);
                Toast.makeText(AreaSelectionActiviy.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void getAreasFromServer(){
        areaprogress.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.SHOW_AREAS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        areaprogress.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("areas");
                            for (int i =0;i<jsonArray.length();i++){
                                JSONObject json = jsonArray.getJSONObject(i);
                                areasidarray.add(json.getString("id"));
                                areasarray.add(json.getString("name"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(AreaSelectionActiviy.this, e.getMessage()+"", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                areaprogress.setVisibility(View.GONE);
                Toast.makeText(AreaSelectionActiviy.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        }){;
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("cityid",citiesidarray.get(cityid));
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

}
