package ehs.foodloo.DataProviders;

public class ReviewsDataProvider {

    String CustomerName,ReviewStatment,reviewDate;
    int ratingValue;

    public String getReviewDate() {
        return reviewDate;
    }

    public ReviewsDataProvider(String customerName, String reviewStatment, String reviewDate, int ratingValue) {
        CustomerName = customerName;
        ReviewStatment = reviewStatment;
        this.ratingValue = ratingValue;
        this.reviewDate = reviewDate;

    }

    public String getCustomerName() {
        return CustomerName;
    }

    public String getReviewStatment() {
        return ReviewStatment;
    }

    public int getRatingValue() {
        return ratingValue;
    }
}
