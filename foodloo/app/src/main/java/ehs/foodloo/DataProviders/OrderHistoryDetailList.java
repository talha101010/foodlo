package ehs.foodloo.DataProviders;

public class OrderHistoryDetailList {
    private String itemname,itemunit,itemprice,itemquantity,image;

    public OrderHistoryDetailList(String itemname, String itemunit, String itemprice, String itemquantity, String image) {
        this.itemname = itemname;
        this.itemunit = itemunit;
        this.itemprice = itemprice;
        this.itemquantity = itemquantity;
        this.image = image;
    }

    public String getItemname() {
        return itemname;
    }

    public String getItemunit() {
        return itemunit;
    }

    public String getItemprice() {
        return itemprice;
    }

    public String getItemquantity() {
        return itemquantity;
    }

    public String getImage() {
        return image;
    }
}
