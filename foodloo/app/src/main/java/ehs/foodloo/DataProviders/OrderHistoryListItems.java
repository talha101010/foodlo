package ehs.foodloo.DataProviders;

public class OrderHistoryListItems {
    private String orderid,bill,status,date;

    public OrderHistoryListItems(String orderid, String bill, String status, String date) {
        this.orderid = orderid;
        this.bill = bill;
        this.status = status;
        this.date = date;
    }

    public String getOrderid() {
        return orderid;
    }

    public String getBill() {
        return bill;
    }

    public String getStatus() {
        return status;
    }

    public String getDate() {
        return date;
    }
}
