package ehs.foodloo.DataProviders;

public class FoodMenuDataProvider {

    String menu_name,menuid,hotelid;

    public String getMenu_name() {
        return menu_name;
    }

    public String getMenuid() {
        return menuid;
    }

    public String getHotelid() {
        return hotelid;
    }

    public FoodMenuDataProvider(String menu_name, String menuid, String hotelid) {

        this.menu_name = menu_name;
        this.menuid = menuid;
        this.hotelid = hotelid;
    }
}
