package ehs.foodloo.DataProviders;

public class BillDataProvider {
    String itemid,itemname,itemunit,itemimage;
    String itemprice,itemquantity;

    public void setItemid(String itemid) {
        this.itemid = itemid;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public void setItemunit(String itemunit) {
        this.itemunit = itemunit;
    }

    public void setItemimage(String itemimage) {
        this.itemimage = itemimage;
    }

    public String getItemid() {
        return itemid;
    }

    public String getItemname() {
        return itemname;
    }

    public String getItemunit() {
        return itemunit;
    }

    public String getItemimage() {
        return itemimage;
    }

    public String getItemprice() {
        return itemprice;
    }

    public String getItemquantity() {
        return itemquantity;
    }

    public void setItemprice(String itemprice) {
        this.itemprice = itemprice;
    }

    public void setItemquantity(String itemquantity) {
        this.itemquantity = itemquantity;
    }
}