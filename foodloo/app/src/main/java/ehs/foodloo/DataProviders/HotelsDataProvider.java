package ehs.foodloo.DataProviders;

public class HotelsDataProvider {

    String id,hotel_name, hotel_status,hotel_icon,address,description,timing,deliverycharges,minorder,phone,preparationtime;

    public HotelsDataProvider(String id, String hotel_name, String hotel_status, String hotel_icon, String address, String description, String timing, String deliverycharges,String minorder,String phone,String preparationtime) {
        this.id = id;
        this.hotel_name = hotel_name;
        this.hotel_status = hotel_status;
        this.hotel_icon = hotel_icon;
        this.address = address;
        this.description = description;
        this.timing = timing;
        this.deliverycharges = deliverycharges;
        this.minorder = minorder;
        this.phone = phone;
        this.preparationtime = preparationtime;
    }

    public String getHotel_name() {
        return hotel_name;
    }

    public String getHotel_status() {
        return hotel_status;
    }

    public String getHotel_icon() {
        return hotel_icon;
    }

    public String getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getDescription() {
        return description;
    }

    public String getTiming() {
        return timing;
    }

    public String getDeliverycharges() {
        return deliverycharges;
    }

    public String getMinorder() {
        return minorder;
    }

    public String getPhone() {
        return phone;
    }

    public String getPreparationtime() {
        return preparationtime;
    }
}
