package ehs.foodloo.DataProviders;

public class Items_Data_Provider {
    String id,itemname,itemunit,itemprice,image;

    public Items_Data_Provider(String id, String itemname, String itemunit, String itemprice, String image) {
        this.id = id;
        this.itemname = itemname;
        this.itemunit = itemunit;
        this.itemprice = itemprice;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public String getItemname() {
        return itemname;
    }

    public String getItemunit() {
        return itemunit;
    }

    public String getItemprice() {
        return itemprice;
    }

    public String getImage() {
        return image;
    }
}
