package ehs.foodloo.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import ehs.foodloo.Adapters.ReviewsAdapter;
import ehs.foodloo.DataProviders.ReviewsDataProvider;
import ehs.foodloo.R;

public class ReviewsActivity extends AppCompatActivity {

    RecyclerView reviews_recyclerview;
    ReviewsDataProvider reviewsDataProvider;
    ReviewsAdapter reviewsAdapter;
    ArrayList<ReviewsDataProvider> reviewsList;
    String[] customersName, reviewStatment,reviewDate;
    int[] rating_value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviews);

        reviews_recyclerview = findViewById(R.id.reviews_recyclerview);
        reviews_recyclerview.setHasFixedSize(true);
        reviews_recyclerview.setLayoutManager(new GridLayoutManager(this,1));
        reviewsList = new ArrayList<>();
        customersName = getResources().getStringArray(R.array.cusmtomersname);
        reviewStatment=getResources().getStringArray(R.array.reviewstaments);
        rating_value = getResources().getIntArray(R.array.rating_value);
        reviewDate= getResources().getStringArray(R.array.reviewDate);

        for (int i =0; i<customersName.length; i++){

            reviewsDataProvider =new  ReviewsDataProvider(customersName[i],reviewStatment[i],reviewDate[i],rating_value[i]);
            reviewsList.add(reviewsDataProvider);
        }

        reviewsAdapter= new ReviewsAdapter(ReviewsActivity.this,reviewsList);
        reviews_recyclerview.setAdapter(reviewsAdapter);

    }
}
