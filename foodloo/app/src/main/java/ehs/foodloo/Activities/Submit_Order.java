package ehs.foodloo.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import ehs.foodloo.R;

public class Submit_Order extends AppCompatActivity {
    TextView billtextview, timetextview,daytextview;
    Button done_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit__order);
        billtextview = findViewById(R.id.totalpricetextview);
        timetextview = findViewById(R.id.timeofdeliverytextview);
        daytextview  = findViewById(R.id.deliverydatetextview);
        done_btn = findViewById(R.id.doneorderbtn);

        billtextview.setText(getIntent().getStringExtra("bill"));
        timetextview.setText(getIntent().getStringExtra("time"));
        daytextview.setText(getIntent().getStringExtra("date"));

        done_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
