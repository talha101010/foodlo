package ehs.foodloo.Activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import ehs.foodloo.Adapters.CartAdapter;
import ehs.foodloo.DataProviders.BillDataProvider;
import ehs.foodloo.Helpers.Constants;
import ehs.foodloo.Helpers.SharedPrefrences;
import ehs.foodloo.MainActivity;
import ehs.foodloo.R;

public class CartActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private ArrayList<BillDataProvider> arrayList;
    private static SharedPreferences appSharedPreferences;
    private static Button checkoutbtn;
    private TextView nocart;
    public static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        getViews();
        activity = this;
        arrayList = null;
        Gson gson = new Gson();
        final Type type = new TypeToken<List<BillDataProvider>>() {
        }.getType();
        appSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String json = appSharedPreferences.getString("cart","");
        if (!json.equals("")){
            arrayList = gson.fromJson(json,type);
        }else{
           nocart.setVisibility(View.VISIBLE);
           checkoutbtn.setEnabled(false);
        }
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new CartAdapter(arrayList,CartActivity.this));



        checkoutbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (HotelsDetailActivity.status.equals("Open")){
                ArrayList<BillDataProvider> arrayList = null;

                String json = appSharedPreferences.getString("cart", "");
                Gson gson = new Gson();

                if (!json.equals(""))
                    arrayList = gson.fromJson(json, type);
                if (SharedPrefrences.getInstance(getApplicationContext()).isLoggedIn()) {
                    if (arrayList != null && !(arrayList.size() <= 0)) {
                        int minorder = Integer.parseInt(HotelsDetailActivity.minorder);
                        if (Constants.totallbill >= minorder) {

                            Intent intent = new Intent(CartActivity.this, CheckOutSecondStep.class);
                            intent.putExtra("deliverycharges", HotelsDetailActivity.charges);
                            intent.putExtra("preparationtime", HotelsDetailActivity.preparationtime);
                            intent.putExtra("bill", Constants.totallbill+"");
                            startActivity(intent);

                        } else {
                            AlertDialog.Builder builder;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                builder = new AlertDialog.Builder(CartActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                            } else {
                                builder = new AlertDialog.Builder(CartActivity.this);
                            }
                            builder.setTitle("Warning")
                                    .setMessage("You Have to do minimum Shopping of Rs " + minorder + " to Continue")
                                    .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // continue with delete


                                        }
                                    })
                                    .show();
                        }

                    } else {
                        Toast.makeText(CartActivity.this, "Please do shopping to continue", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(CartActivity.this, "Please login to place your order", Toast.LENGTH_SHORT).show();
                    Intent loginintent = new Intent(CartActivity.this, MainActivity.class);
                    startActivity(loginintent);
                }
            }else{
                    Toast.makeText(CartActivity.this, "This hotel is closed at the movement you can not place order when hotel is closed", Toast.LENGTH_LONG).show();
                }
        }
        });

    }

    private void getViews(){
        recyclerView = findViewById(R.id.cartrecyclerview);
        arrayList = new ArrayList<>();
        checkoutbtn = findViewById(R.id.checkoutbtn);
        nocart = findViewById(R.id.nocarttext);
    }


    public static void calculateBill() {
        ArrayList<BillDataProvider> arrayList=null ;


        Gson gson = new Gson();
        Type type = new TypeToken<List<BillDataProvider>>() {
        }.getType();

        String json = appSharedPreferences.getString("cart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);




        Constants.totallbill = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            Constants.totallbill += Integer.parseInt(arrayList.get(i).getItemquantity() ) * Integer.parseInt(arrayList.get(i).getItemprice() );
        }

        checkoutbtn.setText("Checkout Now " + Constants.totallbill + "");


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (HotelsDetailActivity.status.equals("Open")) {
            if (SharedPrefrences.getInstance(getApplicationContext()).isLoggedIn()) {
                calculateBill();
            } else {
                checkoutbtn.setText("Login to place order");
            }
        }else{
            checkoutbtn.setText("This hotel is closed now");
        }
    }
}
