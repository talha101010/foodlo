package ehs.foodloo.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ehs.foodloo.Adapters.FoodMenuAdapter;
import ehs.foodloo.Adapters.ItemsAdapter;
import ehs.foodloo.DataProviders.BillDataProvider;
import ehs.foodloo.DataProviders.FoodMenuDataProvider;
import ehs.foodloo.DataProviders.Items_Data_Provider;
import ehs.foodloo.Helpers.Constants;
import ehs.foodloo.Helpers.SharedPrefrences;
import ehs.foodloo.R;

public class HotelsDetailActivity extends AppCompatActivity {


    LinearLayout ratinglayout;
    TextView aboutHotelTextView,hotelNameTv, hotelAdressTv;
    RecyclerView foodmenu_recyclerview;
    ArrayList<FoodMenuDataProvider> menuList;
    ProgressBar imageprogressbar;
    private ImageView hotelimage;
    public static String minorder,charges,preparationtime,hotelid,status;
    String review_str,hotelname,hoteladdress,phone,description,timing,image;
    public static RecyclerView.Adapter itemsadapter;
    public static ArrayList<Items_Data_Provider> itemslist;
    public static RecyclerView itemsrecyclerview;
    public static ProgressBar progressBar;
    public static TextView nodata;
    private ImageView cartview;
    public static Activity activity;
    public static TextView cartnumber;
    public static LinearLayout cartlayout;
    private FoodMenuAdapter foodMenuAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotels_detail);
        activity = HotelsDetailActivity.this;

        hotelname = getIntent().getStringExtra("hotelname");
        hoteladdress = getIntent().getStringExtra("hoteladdress");
        minorder = getIntent().getStringExtra("minorder");
        phone = getIntent().getStringExtra("phone");
        description = getIntent().getStringExtra("description");
        charges = getIntent().getStringExtra("charges");
        timing = getIntent().getStringExtra("timing");
        hotelid = getIntent().getStringExtra("hotelid");
        image = getIntent().getStringExtra("image");
        status = getIntent().getStringExtra("status");
        preparationtime = getIntent().getStringExtra("preparationtime");
        progressBar = findViewById(R.id.progressbar);
        imageprogressbar = findViewById(R.id.imageprogressbar);
        cartlayout = findViewById(R.id.cartlayout);
        cartnumber = findViewById(R.id.itemnumber);
        hotelimage = findViewById(R.id.hotelimage);
        cartview = findViewById(R.id.cartimg);
        itemsrecyclerview = findViewById(R.id.itemsrecyclerview);
        itemslist = new ArrayList<>();
        nodata = findViewById(R.id.nodata);
        SharedPrefrences.getInstance(getApplicationContext()).hotelid(hotelid);
        hotelNameTv = findViewById(R.id.hotelNameTitel);
        hotelNameTv.setText(hotelname);
        hotelAdressTv = findViewById(R.id.hotelAressTitle);
        hotelAdressTv.setText(hoteladdress);
         Picasso.get().load(image).fit().into(hotelimage, new Callback() {
             @Override
             public void onSuccess() {
                 imageprogressbar.setVisibility(View.GONE);
             }

             @Override
             public void onError(Exception e) {

             }
         });

        aboutHotelTextView = findViewById(R.id.abouthoteltextview);
        aboutHotelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Intent intent = new Intent(HotelsDetailActivity.this,HotelCompleteInfoActivity.class);
              intent.putExtra("hoteladdress",hoteladdress);
              intent.putExtra("description",description);
              intent.putExtra("phone",phone);
              intent.putExtra("timing",timing);
              intent.putExtra("hotelname",hotelname);
              intent.putExtra("image",image);
              intent.putExtra("hotelid",hotelid);
              startActivity(intent);
            }
        });

        foodmenu_recyclerview = findViewById(R.id.foodmenu_recyclerview);

        menuList= new ArrayList<>();
        getMenusFromServer();

        //..Mini order textview yahan prep time ka text set karna hai........
//        miniorderTextview = findViewById(R.id.resturant_min_order);
//        miniorderTextview.setText("RS:"+minorder);
//        //.........................................
//
//        //..prep time textview yahan prep time ka time set karna hai..............
//        prepTimeTextview = findViewById(R.id.resturant_preptime);
//        prepTimeTextview.setText(preparationtime+"Min.");
//
//        //.....................................
//
//        //..................rating layout here....................
//        ratingTv = findViewById(R.id.resturant_rating_tv);
//        ratingTv.setText("RS:"+charges);
//        ratinglayout = findViewById(R.id.rating_layout);
//        ratinglayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                final AlertDialog.Builder alertdialog = new AlertDialog.Builder(HotelsDetailActivity.this);
//                LayoutInflater inflater =getLayoutInflater();
//                final View dialogView = inflater.inflate(R.layout.rating_dialog,null);
//                final Button ratingBtn= dialogView.findViewById(R.id.ratingsubmitbtn);
//                ratingBtn.setBackgroundColor(Color.TRANSPARENT);
//                final RatingBar ratingBar = dialogView.findViewById(R.id.rating_bar);
//                final EditText review_et = dialogView.findViewById(R.id.reviwes_edittext);
//
//                alertdialog.setView(dialogView);
//                dialog = alertdialog.create();
//                dialog.show();
//
//                ratingBtn.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        // yahan rating calculate kar k rating submit karnay ka function likhna hai.
//                        // agar user registered na ho to us kay samnay registration wali acitiviy open karni hai.
//
//                        if (review_et.getText().toString().equals("")){
//
//                            review_str= "null";
//
//                        }else {
//
//                            review_str = review_et.getText().toString();
//                            // send to server fuction here
//                        }
//                    }
//                });
//
//            }
//        });
        itemsrecyclerview.setLayoutManager(new LinearLayoutManager(this));
        itemsrecyclerview.setHasFixedSize(true);

        menuList= new ArrayList<>();


        getMenusFromServer();

        itemsrecyclerview.setLayoutManager(new LinearLayoutManager(this));
        itemsrecyclerview.setHasFixedSize(true);

    }

    public static void cartNumber(){


        ArrayList<BillDataProvider> arrayList = null;

        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(activity);
        Gson gson = new Gson();
        Type type = new TypeToken<List<BillDataProvider>>() {
        }.getType();
        String json = appSharedPrefs.getString("cart", "");
        if (!json.equals("")) {
            arrayList = gson.fromJson(json, type);
             if (arrayList.size()==0){
                 cartnumber.setVisibility(View.GONE);

                 cartlayout.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View v) {
                         Toast.makeText(activity, "Nothing in the cart", Toast.LENGTH_SHORT).show();
                     }
                 });
             }else{
                 cartnumber.setText(arrayList.size() + "");
                 cartnumber.setVisibility(View.VISIBLE);

                 cartlayout.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View v) {
                         activity. startActivity(new Intent(activity, CartActivity.class));
                     }
                 });
             }

        } else {
            cartnumber.setVisibility(View.GONE);
            cartlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(activity, "Nothing in the Cart Yet", Toast.LENGTH_SHORT).show();
                }
            });
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.hotels_menus, menu);
        return true;

    }

    private void getMenusFromServer(){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.SHOW_MENUS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                      progressBar.setVisibility(View.GONE);
                        try {
                            if (foodMenuAdapter != null){
                                menuList.clear();
                            }
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("menus");
                            if (jsonArray.length()>0){
                                for (int i =0;i<jsonArray.length();i++){
                                    JSONObject json = jsonArray.getJSONObject(i);
                                    FoodMenuDataProvider foodMenuDataProvider = new FoodMenuDataProvider(json.getString("name"),json.getString("id"),json.getString("hotelid"));
                                    menuList.add(foodMenuDataProvider);
                                }
                                JSONObject json1 = jsonArray.getJSONObject(0);
                                getItemsFromServer(hotelid,json1.getString("id"),HotelsDetailActivity.this);
                                 foodMenuAdapter = new FoodMenuAdapter(HotelsDetailActivity.this,menuList);
                                foodmenu_recyclerview.setAdapter(foodMenuAdapter);

                            }else{
                                Toast.makeText(HotelsDetailActivity.this, "No Menu Added yet", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(HotelsDetailActivity.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("hotelid",hotelid);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    public void getItemsFromServer(final String hid, final String mid, final Context context){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.SHOW_ITEMS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            if (itemslist != null){
                                itemslist.clear();
                            }
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("items");
                            if (jsonArray.length()>0){
                                for (int i =0;i<jsonArray.length();i++){
                                    JSONObject json = jsonArray.getJSONObject(i);
                                    Items_Data_Provider list = new Items_Data_Provider(json.getString("id"),
                                            json.getString("name"),json.getString("unit"),json.getString("price"),
                                            json.getString("image"));
                                    itemslist.add(list);
                                }

                                 itemsadapter = new ItemsAdapter(itemslist,HotelsDetailActivity.this);
                                 itemsrecyclerview.setAdapter(itemsadapter);


                            }else{
                                nodata.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(HotelsDetailActivity.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("hotelid",hid);
                param.put("menuid",mid);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        cartNumber();
        if (itemsadapter != null){
            itemsadapter.notifyDataSetChanged();
        }
    }

//    @Override
//    public void onBackPressed() {
//        if (checkCart()) {
//            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(HotelsDetailActivity.this);
//            alertDialog.setMessage("Your cart will be clear when you go to previous activity.Are you sure you want to do this?");
//            alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    if (clearCart()) {
//                        HotelsDetailActivity.super.onBackPressed();
//                    }
//                }
//            });
//            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.cancel();
//                }
//            });
//
//            alertDialog.show();
//        }else{
//            super.onBackPressed();
//        }
//    }
//
//    private boolean clearCart() {
//        ArrayList<BillDataProvider> arrayList = null;
//        Gson gson = new Gson();
//        arrayList = new ArrayList<>();
//        SharedPreferences appSharedPrefs = PreferenceManager
//                .getDefaultSharedPreferences(activity);
//        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
//        String jsons = gson.toJson(arrayList);
//        prefsEditor.putString("cart", jsons);
//        prefsEditor.remove("cart");
//        prefsEditor.commit();
//        return true;
//    }
//    private boolean checkCart(){
//        ArrayList<BillDataProvider> arrayList = null;
//        SharedPreferences appSharedPrefs = PreferenceManager
//                .getDefaultSharedPreferences(activity);
//        Gson gson = new Gson();
//        Type type = new TypeToken<List<BillDataProvider>>() {
//        }.getType();
//        String json = appSharedPrefs.getString("cart", "");
//
//        if (!json.equals("")){
//            arrayList = gson.fromJson(json, type);
//            if (arrayList.size()==0){
//                return false;
//            }else{
//                return true;
//            }
//        }else{
//            return false;
//        }
//
//    }
}
