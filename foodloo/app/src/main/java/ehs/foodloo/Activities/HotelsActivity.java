package ehs.foodloo.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ehs.foodloo.Adapters.ShowHotelsAdapter;
import ehs.foodloo.AreaSelectionActiviy;
import ehs.foodloo.DataProviders.HotelsDataProvider;
import ehs.foodloo.Helpers.Constants;
import ehs.foodloo.Helpers.SharedPrefrences;
import ehs.foodloo.MainActivity;
import ehs.foodloo.R;
import ehs.foodloo.RegistrationActivity;

public class HotelsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private RecyclerView hotels_recyclerview;
    String getCity,getArea,cityid,areaid;
    private RecyclerView.Adapter adapter;
    private ArrayList<HotelsDataProvider> list;
    private ProgressBar progressBar;
    private TextView nodata;
    private NavigationView navigationView;
    private SharedPreferences sharedpreferences1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotels);
        SharedPreferences sharedpreferences = this.getSharedPreferences("foodloosharepreferences", Context.MODE_PRIVATE);
        getCity = sharedpreferences.getString("city",null);
        getArea = sharedpreferences.getString("area",null);
        cityid = sharedpreferences.getString("cityid",null);
        areaid = sharedpreferences.getString("areaid",null);
         sharedpreferences1 = this.getSharedPreferences("foodloosharepreferences1", Context.MODE_PRIVATE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        progressBar = findViewById(R.id.progressbar);
        nodata = findViewById(R.id.nodatatext);
        list = new ArrayList<>();
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getCity);
        getSupportActionBar().setSubtitle(getArea);
        toolbar.setTitleTextColor(getResources().getColor(R.color.textcolor));
        toolbar.setSubtitleTextColor(getResources().getColor(R.color.textcolor));




        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);


        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView =  findViewById(R.id.nav_view);
//        View view = navigationView.inflateHeaderView(R.layout.nav_header_hotels);
//        TextView name = view.findViewById(R.id.customername);
//        TextView subtitle = view.findViewById(R.id.subtitle);
//        if (SharedPrefrences.getInstance(getApplicationContext()).isLoggedIn()){
//            name.setText(sharedpreferences1.getString("name",null));
//            subtitle.setText(getCity+","+getArea);
//
//        }else{
//
//            name.setText("You are not loged in");
//        }
        navigationView.setNavigationItemSelectedListener(this);

        //    recycler view code start from here

        hotels_recyclerview = findViewById(R.id.hotels_recyclerview);
        hotels_recyclerview.setHasFixedSize(true);
        hotels_recyclerview.setLayoutManager(new GridLayoutManager(this,2));
       getHotelsFromServer();
    }

    private void getHotelsFromServer(){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.SHOW_HOTELS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("hotels");
                            if (jsonArray.length()>0){
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject json = jsonArray.getJSONObject(i);
                                    HotelsDataProvider listitems = new HotelsDataProvider(json.getString("id"),
                                            json.getString("name"),json.getString("status"),json.getString("url"),
                                            json.getString("address"),json.getString("description"),json.getString("timing"),
                                            json.getString("deliverycharges"),json.getString("minorder"),json.getString("phone")
                                            ,json.getString("preparationtime"));
                                    list.add(listitems);
                                }
                                adapter = new ShowHotelsAdapter(list,HotelsActivity.this);
                                hotels_recyclerview.setAdapter(adapter);

                            }else{
                                nodata.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(HotelsActivity.this, error.getMessage()+"", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("cityid",cityid);
                param.put("areaid",areaid);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.hotels, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if (id==R.id.menu_location_id){

            startActivity(new Intent(HotelsActivity.this, AreaSelectionActiviy.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.myorders) {
            if (SharedPrefrences.getInstance(getApplicationContext()).isLoggedIn()){
                startActivity(new Intent(HotelsActivity.this,OrderHistory.class));
            }else{
                startActivity(new Intent(HotelsActivity.this,MainActivity.class));
            }
        }else  if (id == R.id.register) {
           startActivity(new Intent(HotelsActivity.this,RegistrationActivity.class));
        }else if (id == R.id.contactus) {

        } else if (id == R.id.faqs) {

        } else if (id == R.id.termsandconditions) {

        }else if (id == R.id.logout){
            if (SharedPrefrences.getInstance(getApplicationContext()).logout()){
                Toast.makeText(this, "You have been loged out successfully", Toast.LENGTH_SHORT).show();
            }
        }
        else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Menu navmenu = navigationView.getMenu();
        if (SharedPrefrences.getInstance(getApplicationContext()).isLoggedIn()){

            navmenu.findItem(R.id.register).setVisible(false);
        }else{
            navmenu.findItem(R.id.logout).setVisible(false);

        }

    }
}
