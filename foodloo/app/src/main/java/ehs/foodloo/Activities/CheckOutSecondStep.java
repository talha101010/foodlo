package ehs.foodloo.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import ehs.foodloo.AreaSelectionActiviy;
import ehs.foodloo.DataProviders.BillDataProvider;
import ehs.foodloo.Helpers.Constants;
import ehs.foodloo.R;

public class CheckOutSecondStep extends AppCompatActivity {

    private static final String TAG = "RegisterComplainAct";
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int REQUEST_CODE = 123;
    private static final int PLACE_PICKER_REQUEST = 1;
    private Boolean locationPermissionGranted = false;
    private FusedLocationProviderClient fusedLocationProviderClient;

    private String deliverycharges,preparationtime,bill;
    private TextView estimatedtextview,orderdatetextview,deliverychargestextview,billtextview,grandtotal;
    private EditText comments,deliveryaddress;
    private ProgressBar progressBar;
    private ImageView editbtn,editaddress;
    private ListView selectListview;
    private ArrayList<String> slotsarray;
    private ArrayAdapter<String> soltsadapter;
    private AlertDialog dialog;
    private Button checkout;
    private double latitude,longitude;
    private ProgressDialog progressDialog;
    private static SharedPreferences appSharedPrefs;
    private String userid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out_second_step);
        getViews();
        deliverycharges = getIntent().getStringExtra("deliverycharges");
        preparationtime = getIntent().getStringExtra("preparationtime");
        bill = getIntent().getStringExtra("bill");
        SharedPreferences sharedpreferences = this.getSharedPreferences("foodloosharepreferences1", Context.MODE_PRIVATE);
        deliverychargestextview.setText(deliverycharges+"  "+"PKR");
        billtextview.setText(bill+"  "+"PKR");
        deliveryaddress.setText(sharedpreferences.getString("address",null));
        userid = sharedpreferences.getString("userid",null);
        deliveryaddress.setTextColor(Color.parseColor("#291B2C"));
        deliveryaddress.setEnabled(false);
       // Toast.makeText(this, bill+"", Toast.LENGTH_SHORT).show();
        int total = (Integer.parseInt(deliverycharges)) + (Integer.parseInt(bill));
        grandtotal.setText(total+"  "+"PKR");
        getTime();

        editbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectDialog(slotsarray);

                selectListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String selectedCity =  soltsadapter.getItem(position).toString();
                        estimatedtextview.setText(selectedCity);
                        dialog.dismiss();
                    }
                });
            }
        });

        editaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deliveryaddress.setEnabled(true);
                deliveryaddress.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(deliveryaddress, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                    if (deliveryaddress.getText().length()==0){
                        deliveryaddress.setError("This field should not be empty");
                    }else{
                        progressDialog.setCanceledOnTouchOutside(false);
                        progressDialog.setMessage("Please Wait.....");
                        progressDialog.show();
                        getLocationPermission();
                    }

                }else{
                    showGPSDisabledAlertToUser();

                }
            }
        });

    }

    private void showGPSDisabledAlertToUser(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Goto Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                finish();
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        dialog.cancel();
                        finish();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    public void getLocationPermission(){
        Log.d(TAG,"getLocationPermission : getting location permission");
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

        if (ActivityCompat.checkSelfPermission(this.getApplicationContext(),FINE_LOCATION)== PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.checkSelfPermission(this.getApplicationContext(),COARSE_LOCATION)== PackageManager.PERMISSION_GRANTED){
                locationPermissionGranted = true;
                getDeviceLocation();

            }else{
                ActivityCompat.requestPermissions(this,permissions,REQUEST_CODE);

            }
        }else{
            ActivityCompat.requestPermissions(this,permissions,REQUEST_CODE);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG,"onRequestPermissionResult : Called");
        locationPermissionGranted = false;
        switch (requestCode){
            case REQUEST_CODE:
                if (grantResults.length >0){
                    for (int i=0;i<grantResults.length;i++){
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED){
                            Log.d(TAG,"onRequestPermissionResult : permission not granted");
                            locationPermissionGranted = false;
                            ActivityCompat.requestPermissions(this,permissions,REQUEST_CODE);
                            Toast.makeText(this, "Please grant location permission to continue", Toast.LENGTH_SHORT).show();
                            break;
                        }
                    }
                    Log.d(TAG,"onRequestPermissionResult : Permission granted");
                    locationPermissionGranted = true;
                    // initialize the map here
                    getDeviceLocation();
                }

        }

    }

    public void locationAddress(double lat,double lng) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());


        try {
            List<Address> addresses = geocoder.getFromLocation(lat,lng, 1);
            String address23 = addresses.get(0).getAddressLine(0);

        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Exception:"+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void getDeviceLocation(){
        Log.d(TAG,"getDeviceLocation : getting device location");
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        final String address;

        try {
            if (locationPermissionGranted){
                Task location = fusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful() && task.getResult() != null){
                            Log.d(TAG,"onComplete : found location ");
                            Location currentlocation = (Location) task.getResult();

                            latitude = currentlocation.getLatitude();
                            longitude = currentlocation.getLongitude();
                              checkoutNow();

                            locationAddress(currentlocation.getLatitude(),currentlocation.getLongitude());



                        }else{
                            Log.d(TAG,"onComplete : location not found");
                          //    Toast.makeText(CheckOutSecondStep.this, "unable to get current location", Toast.LENGTH_SHORT).show();
                         checkoutNow();
                        }
                    }
                });
            }
        }catch (SecurityException e){
            Toast.makeText(this, "Security Exception "+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void getViews(){
        estimatedtextview = findViewById(R.id.estimatedtextview);
        orderdatetextview = findViewById(R.id.orderdatetextview);
        deliverychargestextview = findViewById(R.id.deliverycharges);
        comments = findViewById(R.id.comments);
        progressBar = findViewById(R.id.progressbar);
        editbtn = findViewById(R.id.editbtn);
        slotsarray = new ArrayList<>();
        billtextview= findViewById(R.id.bill);
        grandtotal = findViewById(R.id.grandtotal);
        checkout = findViewById(R.id.checkout);
        progressDialog = new ProgressDialog(this);
        deliveryaddress = findViewById(R.id.deliveryaddress);
        editaddress =findViewById(R.id.editaddressimageview);
    }

    public void selectDialog(ArrayList<String> param){

        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(CheckOutSecondStep.this);
        LayoutInflater inflater =getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.slots_listview,null);
        alertdialog.setView(dialogView);
        alertdialog.setTitle("Select desired slot");
        selectListview = dialogView.findViewById(R.id.selectlistview_in_dialog);
        soltsadapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,param);
        selectListview.setAdapter(soltsadapter);
        selectListview.setTextFilterEnabled(true);

        dialog = alertdialog.create();
        dialog.show();
    }

    private void getTime(){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.SHOW_TIME,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            estimatedtextview.setText(jsonObject.getString("estimatedtime"));
                            orderdatetextview.setText(jsonObject.getString("currentdate"));
                            slotsarray.add(jsonObject.getString("slot1"));
                            slotsarray.add(jsonObject.getString("slot2"));
                            slotsarray.add(jsonObject.getString("slot3"));
                            slotsarray.add(jsonObject.getString("slot4"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(CheckOutSecondStep.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("time",preparationtime);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void checkoutNow(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.INSERT_ORDER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (!jsonObject.getBoolean("error")){
                                Toast.makeText(CheckOutSecondStep.this, "Order has been placed successfully", Toast.LENGTH_SHORT).show();
                                Intent placedintent = new Intent(CheckOutSecondStep.this,Submit_Order.class);
                                placedintent.putExtra("date",orderdatetextview.getText().toString());
                                placedintent.putExtra("time",estimatedtextview.getText().toString());
                                placedintent.putExtra("bill",grandtotal.getText().toString());
                                startActivity(placedintent);
                                clearCart();
                                if (CartActivity.activity!=null){
                                    CartActivity.activity.finish();
                                }
                                finish();
                            }else{
                                Toast.makeText(CheckOutSecondStep.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(CheckOutSecondStep.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("json_array",makeOrdersJson().toString());
                param.put("userid",userid);
                param.put("hotelid",HotelsDetailActivity.hotelid);
                param.put("time",estimatedtextview.getText().toString());
                param.put("date",orderdatetextview.getText().toString());
                param.put("charges",deliverycharges);
                param.put("bill",bill);
                param.put("grandtotal",grandtotal.getText().toString());
                param.put("latitude",latitude+"");
                param.put("longitude",longitude+"");
                param.put("comments",comments.getText().toString());
                param.put("deliveryaddress",deliveryaddress.getText().toString());
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    private JSONArray makeOrdersJson() {
        ArrayList<BillDataProvider> arrayList = null;
        appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(CheckOutSecondStep.this);
        String json = appSharedPrefs.getString("cart", "");
        Gson gson = new Gson();
        final Type type = new TypeToken<List<BillDataProvider>>() {
        }.getType();
        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < arrayList.size(); i++) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("quantity", arrayList.get(i).getItemquantity());
                obj.put("id", arrayList.get(i).getItemid());
                obj.put("price",arrayList.get(i).getItemprice());
                obj.put("productname",arrayList.get(i).getItemname());
                obj.put("unit",arrayList.get(i).getItemunit());
                jsonArray.put(obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArray;
    }

        private void clearCart() {
        ArrayList<BillDataProvider> arrayList = null;
        Gson gson = new Gson();
        arrayList = new ArrayList<>();
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(CheckOutSecondStep.this);
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("cart", jsons);
        prefsEditor.remove("cart");
        prefsEditor.commit();
    }
}
