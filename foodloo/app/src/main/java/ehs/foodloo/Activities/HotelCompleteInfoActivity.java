package ehs.foodloo.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ehs.foodloo.Adapters.ItemsAdapter;
import ehs.foodloo.AreaSelectionActiviy;
import ehs.foodloo.DataProviders.Items_Data_Provider;
import ehs.foodloo.Helpers.Constants;
import ehs.foodloo.R;

public class HotelCompleteInfoActivity extends AppCompatActivity {

    TextView delivery_areas_of_resturants;
    ArrayAdapter<String> arrayAdapter;
    AlertDialog dialog;
    ListView selectListview;
    Button read_all_reviews_btn;
    private TextView hotelname,hoteladdress,phone,description,timing;
    private ImageView hotelimage;
    private ProgressBar progressBar;
    private String hotelid;
    private ArrayList<String> areaArray;
    private TextView noarea;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_complete_info);
        hotelid = getIntent().getStringExtra("hotelid");
        delivery_areas_of_resturants = findViewById(R.id.delivery_areas_of_resturants);
        delivery_areas_of_resturants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String[] pram = getResources().getStringArray(R.array.areas);
                showAreas(areaArray);

            }
        });

        read_all_reviews_btn = findViewById(R.id.read_all_reviews_btn);
        read_all_reviews_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(HotelCompleteInfoActivity.this,ReviewsActivity.class));

            }
        });
        getViews();
        hotelname.setText(getIntent().getStringExtra("hotelname"));
        hoteladdress.setText(getIntent().getStringExtra("hoteladdress"));
        phone.setText(getIntent().getStringExtra("phone"));
        description.setText(getIntent().getStringExtra("description"));
        timing.setText(getIntent().getStringExtra("timing"));
        Picasso.get().load(getIntent().getStringExtra("image")).fit().into(hotelimage, new Callback() {
            @Override
            public void onSuccess() {
               progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {

            }
        });

    }

    private void getViews(){
        hotelname = findViewById(R.id.hotelname);
        hoteladdress = findViewById(R.id.addresstext);
        phone = findViewById(R.id.phonetext);
        description = findViewById(R.id.descriptiontext);
        timing = findViewById(R.id.timingtext);
        hotelimage = findViewById(R.id.hotelimage);
        progressBar = findViewById(R.id.progressbar);
        areaArray = new ArrayList<String>();
    }

    public void showAreas(ArrayList<String> pram){

        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(HotelCompleteInfoActivity.this);
        LayoutInflater inflater =getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.listview_dialog,null);
        alertdialog.setView(dialogView);
        alertdialog.setTitle("This resturant provide food in following areas");
        selectListview = dialogView.findViewById(R.id.selectlistview_in_dialog);
        EditText search = dialogView.findViewById(R.id.searchtext);
        ProgressBar messageprogress = dialogView.findViewById(R.id.messageprogressbar);
        noarea = dialogView.findViewById(R.id.noareatext);
        getAreasFromServer(messageprogress);
        arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,pram);
        selectListview.setAdapter(arrayAdapter);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
              HotelCompleteInfoActivity.this.arrayAdapter.getFilter().filter(s);
            }
        });
        dialog = alertdialog.create();
        dialog.show();


    }

    public void getAreasFromServer(final ProgressBar mprogress){
        mprogress.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.SHOW_HOTEL_AREA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mprogress.setVisibility(View.GONE);
                        try {
                            if (arrayAdapter != null){
                                areaArray.clear();
                            }
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("hotelareas");
                            if (jsonArray.length()>0){
                                for (int i =0;i<jsonArray.length();i++){
                                    JSONObject json = jsonArray.getJSONObject(i);
                                    areaArray.add(json.getString("name"));
                                }


                            }else{
                                noarea.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mprogress.setVisibility(View.GONE);
                Toast.makeText(HotelCompleteInfoActivity.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("hotelid",hotelid);

                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(HotelCompleteInfoActivity.this);
        requestQueue.add(stringRequest);
    }


}
