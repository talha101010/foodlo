package ehs.foodloo.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.print.PageRange;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ehs.foodloo.Adapters.OrderHistoryDetailAdapter;
import ehs.foodloo.DataProviders.OrderHistoryListItems;
import ehs.foodloo.Helpers.Constants;
import ehs.foodloo.R;

public class OrderHistory extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<OrderHistoryListItems> list;
    private String userid;
    private ProgressBar progressBar;
    private TextView notext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);
        getViews();
        SharedPreferences sharedpreferences = this.getSharedPreferences("foodloosharepreferences1", Context.MODE_PRIVATE);
        userid = sharedpreferences.getString("userid",null);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        getOrders();
    }
    private void getViews(){
        recyclerView = findViewById(R.id.orderhistoryrecyclerview);
        list = new ArrayList<>();
        progressBar = findViewById(R.id.progressbar);
        notext = findViewById(R.id.notext);
    }
    private void getOrders(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.ORDER_HISTORY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                      progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("order_tracking");
                            if (jsonArray.length()>0){
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject json = jsonArray.getJSONObject(i);
                                    OrderHistoryListItems listItems = new OrderHistoryListItems(json.getString("id"),
                                            json.getString("bill"),json.getString("status"),json.getString("date"));
                                    list.add(listItems);
                                }
                                adapter = new OrderHistoryDetailAdapter(list,OrderHistory.this);
                                recyclerView.setAdapter(adapter);
                            }else{
                                 notext.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                 progressBar.setVisibility(View.GONE);
                Toast.makeText(OrderHistory.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("userid",userid);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
