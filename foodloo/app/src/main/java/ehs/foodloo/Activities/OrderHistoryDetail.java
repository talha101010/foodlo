package ehs.foodloo.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ehs.foodloo.Adapters.HistoryDetailAdapter;
import ehs.foodloo.DataProviders.OrderHistoryDetailList;
import ehs.foodloo.Helpers.Constants;
import ehs.foodloo.R;

public class OrderHistoryDetail extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<OrderHistoryDetailList> list;
    private ProgressBar progressBar;
    private String orderid;
    private TextView items,grandtotal,bill,charges;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history_detail);
        getViews();
        orderid = getIntent().getStringExtra("orderid");
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        getOrders();
    }

    private void getViews(){
        recyclerView = findViewById(R.id.orderdetailrecyclerview);
        list = new ArrayList<>();
        progressBar = findViewById(R.id.progressbar);
        items = findViewById(R.id.totalitems);
        grandtotal = findViewById(R.id.grandtotal);
        bill = findViewById(R.id.totalbill);
        charges = findViewById(R.id.deliverycharges);
    }

    private void getOrders(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.ORDER_HISTORY_DETAIL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("detail");
                            if (jsonArray.length()>0){
                                int count=0;
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject json = jsonArray.getJSONObject(i);
                                    OrderHistoryDetailList listItems = new OrderHistoryDetailList(
                                            json.getString("itemname"),json.getString("itemunit"),json.getString("itemprice"),
                                            json.getString("itemquantity"),json.getString("image"));
                                    list.add(listItems);
                                    count++;
                                }
                                items.setText("Total Items:"+count+"");
                                grandtotal.setText("Grand Total:"+jsonObject.getString("grandtotal"));
                                bill.setText("Bill:"+jsonObject.getString("totalbill"));
                                charges.setText("Delivery Charges:"+jsonObject.getString("charges"));
                                adapter = new HistoryDetailAdapter(list,OrderHistoryDetail.this);
                                recyclerView.setAdapter(adapter);
                            }else{
                                Toast.makeText(OrderHistoryDetail.this, "no data", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(OrderHistoryDetail.this, e.getMessage()+"", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(OrderHistoryDetail.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("orderid",orderid);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
