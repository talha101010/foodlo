package ehs.foodloo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ehs.foodloo.Helpers.Constants;

public class RegistrationActivity extends AppCompatActivity {

   private EditText registrationNameEditText, registrationNumberEditText, registrationEmailAddressEditText,
            registrationHomeAddressEditText,registrationPinEditText;

//    TextView textViewToOpenCitySelectionPopup, textViewToOpenAreaSelectionPopup;
//    ListView selectListview;
//    ArrayAdapter<String> arrayAdapter;
//    AlertDialog dialog;
   private Button registrationSubmitBtn;
   TextView registerbtn;
   private String strName, strPhoneNumber, strEmailAddress, strHomeAdress, strPin,strCity,strArea;
   private ProgressDialog progressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        views();
        registrationSubmitBtn.setBackgroundColor(Color.TRANSPARENT);
        registrationSubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkEditTexts();

            }
        });

        registerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegistrationActivity.this,MainActivity.class));
                finish();
            }
        });




//        textViewToOpenCitySelectionPopup.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                selectDialog(getResources().getStringArray(R.array.citiesarray),"Select your city");
//                selectListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                      String selectedCity =  arrayAdapter.getItem(position).toString();
//                      textViewToOpenCitySelectionPopup.setText(selectedCity);
//                      dialog.dismiss();
//
//                    }
//                });
//
//
//
//            }
//        });


//        textViewToOpenAreaSelectionPopup.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                selectDialog(getResources().getStringArray(R.array.areas),"Select your area");
//                selectListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                        String selectedCity =  arrayAdapter.getItem(position).toString();
//                       textViewToOpenAreaSelectionPopup.setText(selectedCity);
//                        dialog.dismiss();
//
//                    }
//                });
//
//            }
//        });
    }

    public void views(){

        // textviews of select city and areas

//        textViewToOpenCitySelectionPopup = findViewById(R.id.textViewToOpenCitySelectionPopup);
//        textViewToOpenAreaSelectionPopup=findViewById(R.id.textViewToOpenAreaSelectionPopup);
        // submit button
        registrationSubmitBtn = findViewById(R.id.registrationsubmitbtn);

        //   adit texts here
        registrationNameEditText = findViewById(R.id.registrationNameEditText);
        registrationNumberEditText=findViewById(R.id.registrationPhoneNumberEditText);
        registrationEmailAddressEditText=findViewById(R.id.registrationEmailEditText);
        registrationHomeAddressEditText= findViewById(R.id.registrationHomeAddressEditText);
        registrationPinEditText=findViewById(R.id.registrationPinEditText);
        progressDialog = new ProgressDialog(this);
        registerbtn = findViewById(R.id.registerbtn);

    }

    // To open popup window this method is used

//    public void selectDialog(String[] pram,String parm1){
//
//        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(RegistrationActivity.this);
//        LayoutInflater inflater =getLayoutInflater();
//        final View dialogView = inflater.inflate(R.layout.listview_dialog,null);
//        alertdialog.setView(dialogView);
//        alertdialog.setTitle(parm1);
//        selectListview = dialogView.findViewById(R.id.selectlistview_in_dialog);
//        arrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,pram);
//        selectListview.setAdapter(arrayAdapter);
//
//        dialog = alertdialog.create();
//        dialog.show();
//    }

    // is method say hum ye check kar rahay hain, kay ko edit text empty hai ya nahi.

    public  void checkEditTexts(){

        if (registrationNameEditText.getText().toString().equals("")){

            registrationNameEditText.setError(" Please put your full name");

        }else if (registrationNumberEditText.getText().toString().equals("")){

            registrationNumberEditText.setError(" Please put your phone number");

        }else if (registrationHomeAddressEditText.getText().toString().equals("")){


            registrationHomeAddressEditText.setError("Please put your delivery address");
        }else  if (registrationPinEditText.getText().toString().equals("")){

             registrationPinEditText.setError("Please put 4 digits pin");

        }else  if (registrationPinEditText.getText().toString().length()!=4){

                 registrationPinEditText.setError("Pin must be 4 digits");
        }else {
            strName = registrationNameEditText.getText().toString();
            strPhoneNumber = registrationNumberEditText.getText().toString();
            strEmailAddress = registrationEmailAddressEditText.getText().toString();
            strHomeAdress= registrationHomeAddressEditText.getText().toString();

//            strCity = textViewToOpenCitySelectionPopup.getText().toString();
//            strArea = textViewToOpenAreaSelectionPopup.getText().toString();
            strPin = registrationPinEditText.getText().toString();
            sendDataToServer();
        }
    }

    private void sendDataToServer(){
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Registering.....");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.REGISTER_USER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                     progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (!jsonObject.getBoolean("error")){
                                Toast.makeText(RegistrationActivity.this, "You have been registered successfully please login to continue", Toast.LENGTH_SHORT).show();
                                 startActivity(new Intent(RegistrationActivity.this,MainActivity.class));
                                 finish();
                            }else{
                                Toast.makeText(RegistrationActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                 progressDialog.dismiss();
                Toast.makeText(RegistrationActivity.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("fullname",strName);
                param.put("phonenumber",strPhoneNumber);
                param.put("email",strEmailAddress);
                param.put("address",strHomeAdress);
                param.put("pin",strPin);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

}
