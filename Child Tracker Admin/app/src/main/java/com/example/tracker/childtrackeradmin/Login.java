package com.example.tracker.childtrackeradmin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Login extends AppCompatActivity {
    private EditText email,password;
    private Button registerbtn,loginbtn;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getViews();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user!=null){
            startActivity(new Intent(Login.this,MainActivity.class));
            finish();
            return;
        }
        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email.getText().length()==0){
                    email.setError("email is required");
                }else if (password.getText().length()==0){
                    password.setError("password is required");
                }else{
                    progressDialog.setMessage("Authenticating.....");
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.show();
                    checkCredential();
                }
            }
        });

        registerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this,AdminRegistration_Activity.class));
            }
        });
    }

    private void getViews(){
        email = findViewById(R.id.emailtext);
        password = findViewById(R.id.passwordtext);
        registerbtn = findViewById(R.id.registerbtn);
        loginbtn = findViewById(R.id.loginbtn);
        progressDialog = new ProgressDialog(this);
        firebaseAuth = FirebaseAuth.getInstance();
    }

    private void checkCredential(){
        firebaseAuth.signInWithEmailAndPassword(email.getText().toString().trim(),password.getText().toString().trim())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            progressDialog.dismiss();
                            SharedPrefrences.getInstance(getApplicationContext()).setUid(user.getUid(),password.getText().toString().trim(),email.getText().toString().trim());
                            startActivity(new Intent(Login.this,MainActivity.class));
                            Toast.makeText(Login.this, "Loged In Successfully", Toast.LENGTH_SHORT).show();
                            finish();
                        }else {
                            progressDialog.dismiss();
                            Toast.makeText(Login.this, "authentication failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
