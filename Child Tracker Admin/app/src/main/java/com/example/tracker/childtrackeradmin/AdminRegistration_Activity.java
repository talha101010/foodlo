package com.example.tracker.childtrackeradmin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AdminRegistration_Activity extends AppCompatActivity {
    private EditText name,email,password;
    private Button registerbtn,loginbtn;
    private ProgressDialog progressDialog;
    private DatabaseReference databaseReference;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_registration_);
        getViews();
        registerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().length()==0){
                    name.setError("name is required");
                }else if (email.getText().length()==0){
                    email.setError("email is required");
                }else if (password.getText().length()==0){
                    password.setError("password is required");
                }else if (password.getText().length()<5){
                    password.setError("password should be greater than 5 digits or characters");
                }else{
                   progressDialog.setMessage("Registering....");
                   progressDialog.setCanceledOnTouchOutside(false);
                   progressDialog.show();
                   registerParent();
                }
            }
        });

        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AdminRegistration_Activity.this,Login.class));
                finish();
            }
        });

    }

    private void getViews(){
        name = findViewById(R.id.nametext);
        email = findViewById(R.id.emailtext);
        password = findViewById(R.id.passwordtext);
        registerbtn = findViewById(R.id.registerbtn);
        loginbtn = findViewById(R.id.loginbtn);
        progressDialog = new ProgressDialog(this);
        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference("Parents");
    }

    private void registerParent() {
        firebaseAuth.createUserWithEmailAndPassword(email.getText().toString().trim(),password.getText().toString().trim())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            progressDialog.dismiss();
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            ParentsSetGet parentsSetGet = new ParentsSetGet();
                            parentsSetGet.setParent_name(name.getText().toString().trim());
                            databaseReference.child(user.getUid()).setValue(parentsSetGet);
//                            ParentNameSetGet parentnameSetGet = new ParentNameSetGet();
//                            parentnameSetGet.setParentname(name.getText().toString().trim());
//                            FirebaseDatabase.getInstance().getReference().child("ParentName").child(user.getUid()).setValue(parentnameSetGet);
                            SharedPrefrences.getInstance(getApplicationContext()).setUid(user.getUid(),password.getText().toString().trim(),email.getText().toString().trim());
                            startActivity(new Intent(AdminRegistration_Activity.this,MainActivity.class));
                            Toast.makeText(AdminRegistration_Activity.this, "Parent has been registered successfully", Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
                            progressDialog.dismiss();
                            try {
                                throw task.getException();
                            } catch(FirebaseAuthWeakPasswordException e) {
                                Toast.makeText(AdminRegistration_Activity.this, "Password should be 6 digit or character long.Please try again", Toast.LENGTH_LONG).show();
                                password.setError("Weak password");
                            } catch(FirebaseAuthInvalidCredentialsException e) {
                                Toast.makeText(AdminRegistration_Activity.this, "Invalid email format", Toast.LENGTH_SHORT).show();
                                email.setError("Invalid email format");
                            } catch(FirebaseAuthUserCollisionException e) {
                                Toast.makeText(AdminRegistration_Activity.this, "User already exist", Toast.LENGTH_SHORT).show();
                                email.setError("User already exist");
                            } catch(Exception e) {
                                Toast.makeText(AdminRegistration_Activity.this, "Authentication failed."+task.getException()+"",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
    }
}
