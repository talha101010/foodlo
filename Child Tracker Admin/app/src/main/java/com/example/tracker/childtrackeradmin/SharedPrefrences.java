package com.example.tracker.childtrackeradmin;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefrences {
    private static SharedPrefrences mInstance;


    private static Context mCtx;

    private SharedPrefrences(Context context) {
        mCtx = context;


    }

    public static synchronized SharedPrefrences getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefrences(context);
        }
        return mInstance;
    }

    public boolean setUid(String uid,String password,String email){
        SharedPreferences pref = mCtx.getSharedPreferences("childtrackeradmin", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("uid",uid);
        editor.putString("password",password);
        editor.putString("email",password);
        editor.apply();

        return true;
    }

    public boolean clear(){
        SharedPreferences pref = mCtx.getSharedPreferences("childtrackeradmin", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.apply();
        return true;
    }
}
