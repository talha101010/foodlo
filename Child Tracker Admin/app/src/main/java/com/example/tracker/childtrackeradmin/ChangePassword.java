package com.example.tracker.childtrackeradmin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ChangePassword extends AppCompatActivity {
    private EditText newpassword;
    private Button changebtn;
    private FirebaseAuth mAuth;
    FirebaseUser currentUser;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        newpassword = findViewById(R.id.newpassword);
        changebtn = findViewById(R.id.changepassord);
        mAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);
        currentUser = mAuth.getInstance().getCurrentUser();

        changebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (newpassword.getText().length() ==0){
                    newpassword.setError("This field is required");
                }else if (newpassword.getText().length()<5) {
                    newpassword.setError("Password length should be greater than 5 digits or characters");
                }else{
                    progressDialog.setMessage("Please Wait....");
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.show();
                    changePassword();
                }
            }
        });
    }

    private void changePassword(){
        currentUser.updatePassword(newpassword.getText().toString().trim())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            Toast.makeText(ChangePassword.this, "password has been changed", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                SharedPreferences preferences = ChangePassword.this.getSharedPreferences("childtrackeradmin", Context.MODE_PRIVATE);
                AuthCredential credential = EmailAuthProvider.getCredential(preferences.getString("email",null),preferences.getString("password",null));
                currentUser.reauthenticate(credential)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                currentUser.updatePassword(newpassword.getText().toString().trim())
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()){
                                                    Toast.makeText(ChangePassword.this, "password has been changed", Toast.LENGTH_SHORT).show();
                                                    finish();
                                                }else{
                                                    Toast.makeText(ChangePassword.this, "authentication failded", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                            }
                        });
            }
        });
    }
}
