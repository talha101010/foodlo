package com.example.tracker.childtrackeradmin;

public class ParentNameSetGet {
    private String parentname;

    public ParentNameSetGet() {
    }

    public String getParentname() {
        return parentname;
    }

    public void setParentname(String parentname) {
        this.parentname = parentname;
    }
}
