package com.example.tracker.childtrackeradmin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Registration_Activity extends AppCompatActivity {
    private EditText name,email,password;
    private Button registerbtn;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private Childs childobj;
    private ProgressDialog progressDialog;
    private SharedPreferences sharedPreferences;
    private String uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_);
        sharedPreferences = this.getSharedPreferences("childtrackeradmin",Context.MODE_PRIVATE);
        uid = sharedPreferences.getString("uid",null);
        getViews();
        registerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().length()==0){
                    name.setError("name is required");
                }else if (email.getText().length()==0){
                    email.setError("email is required");
                }else if (password.getText().length()==0){
                    password.setText("password is required");
                }else if (password.getText().length() < 6){
                    password.setError("password length should be greater than 5 digits or characters");
                }else {
                    progressDialog.setMessage("Registering....");
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.show();
                    registerChild();
                }
            }
        });


    }

    private void getViews(){
        name = findViewById(R.id.nametext);
        email = findViewById(R.id.emailtext);
        password = findViewById(R.id.passwordtext);
        registerbtn = findViewById(R.id.registerbtn);
        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("Parents").child(uid);
        childobj = new Childs();
        progressDialog = new ProgressDialog(this);
    }

    private void registerChild() {
        firebaseAuth.createUserWithEmailAndPassword(email.getText().toString().trim(),password.getText().toString().trim())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            progressDialog.dismiss();
                            FirebaseUser user1 = firebaseAuth.getCurrentUser();
                            childobj.setUid(user1.getUid());
                            childobj.setName(name.getText().toString().trim());
                            childobj.setLatitude("0.0");
                            childobj.setLongitude("0.0");
                            childobj.setStatus("Offline");
                            databaseReference.child(user1.getUid()).setValue(childobj);
                            ChildParentSetGet childParentSetGet = new ChildParentSetGet();
                            childParentSetGet.setParent_uid(uid);
                            FirebaseDatabase.getInstance().getReference().child("Childs").child(user1.getUid()).setValue(childParentSetGet);
                            Toast.makeText(Registration_Activity.this, "child has been registered successfully", Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
                          progressDialog.dismiss();
                            try {
                                throw task.getException();
                            } catch(FirebaseAuthWeakPasswordException e) {
                                Toast.makeText(Registration_Activity.this, "Password should be 6 digit or character long.Please try again", Toast.LENGTH_LONG).show();
                                password.setError("Weak password");
                            } catch(FirebaseAuthInvalidCredentialsException e) {
                                Toast.makeText(Registration_Activity.this, "Invalid email format", Toast.LENGTH_SHORT).show();
                                email.setError("Invalid email format");
                            } catch(FirebaseAuthUserCollisionException e) {
                                Toast.makeText(Registration_Activity.this, "User already exist", Toast.LENGTH_SHORT).show();
                                email.setError("User already exist");
                            } catch(Exception e) {
                                Toast.makeText(Registration_Activity.this, "Authentication failed."+task.getException()+"",
                                        Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                });
    }
}
