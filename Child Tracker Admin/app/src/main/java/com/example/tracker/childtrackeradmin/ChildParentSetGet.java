package com.example.tracker.childtrackeradmin;

public class ChildParentSetGet {
    private String parent_uid;

    public ChildParentSetGet() {
    }

    public String getParent_uid() {
        return parent_uid;
    }

    public void setParent_uid(String parent_uid) {
        this.parent_uid = parent_uid;
    }
}
