package com.example.tracker.childtrackeradmin;

public class ParentsSetGet {
    private String parent_name,parent_uid;

    public ParentsSetGet() {
    }

    public String getParent_name() {
        return parent_name;
    }

    public void setParent_name(String parent_name) {
        this.parent_name = parent_name;
    }

    public String getParent_uid() {
        return parent_uid;
    }

    public void setParent_uid(String parent_uid) {
        this.parent_uid = parent_uid;
    }
}
