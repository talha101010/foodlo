package com.example.tracker.childtrackeradmin;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    private DatabaseReference reference;
    private double latitude,longitude;
    private GoogleMap mMap;
    private final float DEFAULT_ZOOM = 15;
    private ProgressDialog progressDialog;
    private Toolbar toolbar;
    private NavigationView nv;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle mToggle;
    private SharedPreferences sharedPreferences;
    private String parentuid;
    private ArrayList<String> uidArray,nameArray;
    private TextView childstatus,locationinfo;
    private ImageView childstatusdot,locationimage;
    private Button childtrackbtn;
    private ArrayAdapter<String> childAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getViews();
        progressDialog.setMessage("Please Wait.....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        sharedPreferences = this.getSharedPreferences("childtrackeradmin",Context.MODE_PRIVATE);
        parentuid = sharedPreferences.getString("uid",null);

        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);
        setSupportActionBar(toolbar);

        mToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);

        drawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        View itemview =  nv.inflateHeaderView(R.layout.navigation_header);

        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id)
                {
                    case R.id.register:
                        startActivity(new Intent(MainActivity.this,Registration_Activity.class));
                        break;
                    case R.id.changepassword:
                        startActivity(new Intent(MainActivity.this,ChangePassword.class));
                        break;
                    case R.id.logout:
                        SharedPrefrences.getInstance(getApplicationContext()).clear();
                        FirebaseAuth.getInstance().signOut();
                        startActivity(new Intent(MainActivity.this,Login.class));
                        finish();
                    default:
                        return true;
                }
                return true;
            }
        });

        childtrackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              getNameList();
            }
        });

    }

    private void getViews(){
        toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawerlayout);
        nv = findViewById(R.id.nv);
        reference = FirebaseDatabase.getInstance().getReference().child("Childs");
        childstatus = findViewById(R.id.childstatus);
        uidArray = new ArrayList<>();
        nameArray = new ArrayList<>();
        childstatusdot = findViewById(R.id.childstatusdot);
        childtrackbtn = findViewById(R.id.childtrackbtn);
        progressDialog = new ProgressDialog(this);
        locationinfo = findViewById(R.id.locationinfo);
        locationimage = findViewById(R.id.locationimage);
    }

    @Override
    protected void onResume() {
        super.onResume();
        findCount();
       // drawerLayout.closeDrawer();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Toast.makeText(this, "map is ready", Toast.LENGTH_SHORT).show();
        mMap = googleMap;
        mMap.getUiSettings().setMyLocationButtonEnabled(false);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void findCount(){

        FirebaseDatabase.getInstance().getReference().child("Parents").child(parentuid)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (childAdapter != null){
                            nameArray.clear();
                        }
                        nameArray.add(0, "Please Select Child");
                            if (dataSnapshot.getChildrenCount()>1) {
                                for (DataSnapshot data : dataSnapshot.getChildren()) {
                                    if (!data.getKey().equals("parent_name")) {
                                        uidArray.add(data.getKey());
                                        nameArray.add(dataSnapshot.child(data.getKey()).child("name").getValue().toString());
                                    }
                                }
                                updateMap(uidArray.get(0));
                            }else{
                                progressDialog.dismiss();
                                locationinfo.setText("OOPS! Its seems like you did not add any child yet");
                                locationimage.setVisibility(View.GONE);
                            }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void updateMap(final String childuid){

        FirebaseDatabase.getInstance().getReference().child("Parents").child(parentuid).child(childuid)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        latitude = Double.parseDouble(dataSnapshot.child("latitude").getValue().toString());
                        longitude = Double.parseDouble(dataSnapshot.child("longitude").getValue().toString());
                        if (latitude!=0.0 && longitude!= 0.0){
                            locationimage.setVisibility(View.VISIBLE);
                            locationinfo.setVisibility(View.GONE);
                            mMap.addMarker(new MarkerOptions().position(new LatLng(latitude,longitude)).title(dataSnapshot.child("name").getValue().toString()));
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), DEFAULT_ZOOM));
                            childstatus.setVisibility(View.VISIBLE);
                            childstatusdot.setVisibility(View.VISIBLE);
                            childstatus.setText("Tracking "+dataSnapshot.child("name").getValue().toString());
                            if (dataSnapshot.child("status").getValue().toString().equals("Online")){
                                childstatusdot.setImageResource(R.drawable.online);
                            }else if (dataSnapshot.child("status").getValue().toString().equals("Offline")){
                                childstatusdot.setImageResource(R.drawable.offline);
                            }else if (dataSnapshot.child("status").getValue().toString().equals("Background")){
                                childstatusdot.setImageResource(R.drawable.background);
                            }
                        }else{
                            locationinfo.setVisibility(View.VISIBLE);
                            locationinfo.setText("This Child did not start its journey yet");
                            locationimage.setVisibility(View.GONE);
                            childstatus.setVisibility(View.VISIBLE);
                            childstatusdot.setVisibility(View.VISIBLE);
                            childstatus.setText("Tracking "+dataSnapshot.child("name").getValue().toString());
                            if (dataSnapshot.child("status").getValue().toString().equals("Online")){
                                childstatusdot.setImageResource(R.drawable.online);
                            }else if (dataSnapshot.child("status").getValue().toString().equals("Offline")){
                                childstatusdot.setImageResource(R.drawable.offline);
                            }else if (dataSnapshot.child("status").getValue().toString().equals("Background")){
                                childstatusdot.setImageResource(R.drawable.background);
                            }
                        }
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        progressDialog.dismiss();
                        Toast.makeText(MainActivity.this, databaseError.getMessage()+"", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void getNameList(){
        final Dialog dialog = new Dialog(MainActivity.this,R.style.Theme_AppCompat_Light_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);

        final Spinner child =  dialog.findViewById(R.id.spinner);

        Button dialogButton =  dialog.findViewById(R.id.closebtn);
        final Button changebtn = dialog.findViewById(R.id.changebtn);

        childAdapter = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_spinner_dropdown_item,nameArray);
        childAdapter.notifyDataSetChanged();
        child.setAdapter(childAdapter);
        changebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 if (child.getSelectedItemPosition()==0){
                     Toast.makeText(MainActivity.this, "Please Select Child", Toast.LENGTH_SHORT).show();
                 }else{
                     int position = child.getSelectedItemPosition();
                     position = position-1;
                     updateMap(uidArray.get(position));
                     dialog.dismiss();
                 }
            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
