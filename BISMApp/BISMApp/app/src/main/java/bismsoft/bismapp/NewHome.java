package bismsoft.bismapp;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.app.AppCompatActivity;

import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import java.util.ArrayList;


import bismsoft.bismapp.Adapters.HomeAdapter;
import bismsoft.bismapp.DataProviders.HomeDataProvider;
import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

public class NewHome extends AppCompatActivity
         {

    private RecyclerView recyclerView;
    HomeAdapter homeAdapter;
    HomeDataProvider homeDataProvider;
    String[] nameList, numberList, urduNameList, btnNameEng;
    AutoScrollViewPager msgpager;
    ProgressBar progress;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    RelativeLayout bannerplace;


    String[] btnName;
    private ArrayList<HomeDataProvider> list;
   int[] icon = {R.drawable.superstore, R.drawable.fooddeliveryicon, R.drawable.propertypicture,R.drawable.olx ,R.drawable.doctorsicon, R.drawable.phonedairyicon,R.drawable.bhakkarmarketicon,
           R.drawable.namazicon};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_home);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        preferences = getSharedPreferences("EStore", Context.MODE_PRIVATE);
//        editor = preferences.edit();
//
//        bannerplace = findViewById(R.id.banner_place);
//
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.addDrawerListener(toggle);
//        toggle.syncState();
//
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);
//
//
//        recyclerView = findViewById(R.id.honmerecyclerview);
//        recyclerView.setHasFixedSize(true);
//        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
//        btnName = getResources().getStringArray(R.array.name);
//        btnNameEng = getResources().getStringArray(R.array.name_eng);
//
//
//        list = new ArrayList<HomeDataProvider>();
//
//       //  msgpager = findViewById(R.id.msgpager);
//         progress = findViewById(R.id.progress);
//
//        try {
//        for (int i = 0; i < btnName.length; i++) {
//
//            int geticon = icon[i];
//            String namestr = btnName[i];
//            String nameEng = btnNameEng[i];
//            HomeDataProvider homeDataProvider = new HomeDataProvider(namestr, nameEng, geticon);
//            list.add(homeDataProvider);
//        }
//
//        homeAdapter = new HomeAdapter(getApplicationContext(), list);
//        recyclerView.setAdapter(homeAdapter);
//
//
//    } catch (Exception e) {
//
//        Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
//
//    }
//
//        homeAdapter.setOnItemClickListener(new HomeAdapter.OnItemClickListener() {
//        @Override
//        public void onItemClick(int position) {
//            switch (position) {
//
//                case 0:
//
//                    if (NetworkUtil.isNetworkConnected(NewHome.this)) {
//
//                        NetworkUtil networkUtil = new NetworkUtil(NewHome.this);
//                        networkUtil.execute();
//
//                        startActivity(new Intent(NewHome.this, SplashScreen.class));
//
//
//                    } else {
//
//                        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(NewHome.this);
//                        LayoutInflater inflater =getLayoutInflater();
//                        final View dialogView = inflater.inflate(R.layout.shop_close_dialog,null);
//                        final ImageButton helplineBtn= dialogView.findViewById(R.id.helplineCallBtn);
//                        final  ImageButton helplineBtn2 = dialogView.findViewById(R.id.helplineCallBtn2);
//                        helplineBtn2.setVisibility(View.GONE);
//                        final TextView note_eng = dialogView.findViewById(R.id.eng_note);
//                        final TextView note_urdu = dialogView.findViewById(R.id.urdu_note);
//                        final  TextView call_us_1 = dialogView.findViewById(R.id.call_us_1);
//                        call_us_1.setText("Call us");
//                        final  TextView call_us_2 = dialogView.findViewById(R.id.call_us_2);
//                        call_us_2.setText("Call us");
//                        call_us_2.setVisibility(View.GONE);
//                        alertdialog.setView(dialogView);
//
//                        note_eng.setText("You are not connected with internet. You can order by calling us.");
//                        note_urdu.setText("آپ انٹر نیٹ سے کنکٹ نہیں ہیں۔آپ کال کر کے آرڈر کر سکتے ہیں۔");
//
//
//                        final AlertDialog dialog = alertdialog.create();
//                        dialog.show();
//
//                        helplineBtn.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel","03338995373",null));
//                                startActivity(callIntent);
//                            }
//                        });
//
////                        helplineBtn2.setOnClickListener(new View.OnClickListener() {
////                            @Override
////                            public void onClick(View v) {
////
////                                Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel","03331670333",null));
////                                startActivity(callIntent);
////
////                            }
////                        });
//                    }
//
//                    break;
//                case 1:
//
//
//                    if (NetworkUtil.isNetworkConnected(NewHome.this)) {
//
//                        NetworkUtil networkUtil = new NetworkUtil(NewHome.this);
//                        networkUtil.execute();
//
//
//
//                        startActivity(new Intent(NewHome.this, FFSplash.class));
//
//
//                    } else {
//
//                        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(NewHome.this);
//                        LayoutInflater inflater =getLayoutInflater();
//                        final View dialogView = inflater.inflate(R.layout.shop_close_dialog,null);
//                        final ImageButton helplineBtn= dialogView.findViewById(R.id.helplineCallBtn);
//                        final  ImageButton helplineBtn2 = dialogView.findViewById(R.id.helplineCallBtn2);
//                        helplineBtn2.setVisibility(View.GONE);
//                        final TextView note_eng = dialogView.findViewById(R.id.eng_note);
//                        final TextView note_urdu = dialogView.findViewById(R.id.urdu_note);
//                        final  TextView call_us_1 = dialogView.findViewById(R.id.call_us_1);
//                        call_us_1.setText("Call us");
//                        final  TextView call_us_2 = dialogView.findViewById(R.id.call_us_2);
//                        call_us_2.setText("Call us");
//                        call_us_2.setVisibility(View.GONE);
//                        alertdialog.setView(dialogView);
//
//                        note_eng.setText("You are not connected with internet. You can order by calling us.");
//                        note_urdu.setText("آپ انٹر نیٹ سے کنکٹ نہیں ہیں۔آپ کال کر کے آرڈر کر سکتے ہیں۔");
//
//
//                        final AlertDialog dialog = alertdialog.create();
//                        dialog.show();
//
//                        helplineBtn.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel","03338995373",null));
//                               startActivity(callIntent);
//                            }
//                        });
//
////                        helplineBtn2.setOnClickListener(new View.OnClickListener() {
////                            @Override
////                            public void onClick(View v) {
////
////                                Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel","03331670333",null));
////                                startActivity(callIntent);
////
////                            }
////                        });
//                    }
//
//                    break;
//
//                case 2:
//
//                    if (NetworkUtil.isNetworkConnected(NewHome.this)) {
//
//                        NetworkUtil networkUtil = new NetworkUtil(NewHome.this);
//                        networkUtil.execute();
//
//                        startActivity(new Intent(NewHome.this, NewPropertyHome.class));
//
//
//                    } else {
//
//                        AlertDialog alertDialog = new AlertDialog.Builder(NewHome.this).create();
//                        alertDialog.setTitle("Check Internet Connection");
//                        alertDialog.setMessage("آپ انٹرنیٹ سے کنکٹ نہیں ہیں۔");
//                        alertDialog.show();
//                    }
//
//                    break;
//
//                case 3:
//
//                    if (NetworkUtil.isNetworkConnected(NewHome.this)) {
//
//                        NetworkUtil networkUtil = new NetworkUtil(NewHome.this);
//                        networkUtil.execute();
//
//                        Toast.makeText(NewHome.this, "We will add soon", Toast.LENGTH_SHORT).show();
//
//
//                    } else {
//
//                        AlertDialog alertDialog = new AlertDialog.Builder(NewHome.this).create();
//                        alertDialog.setTitle("Check Internet Connection");
//                        alertDialog.setMessage("آپ انٹرنیٹ سے کنکٹ نہیں ہیں۔");
//                        alertDialog.show();
//                    }
//
//                    break;
//
//
//
//
//
//
//
//
//                case 4:
//
//                    Intent intent = new Intent(NewHome.this, Doctors.class);
//                    startActivity(intent);
//
//                    break;
//                case 5:
//
//                    if (NetworkUtil.isNetworkConnected(NewHome.this)) {
//
//                        NetworkUtil networkUtil = new NetworkUtil(NewHome.this);
//                        networkUtil.execute();
//
//
//
//                        startActivity(new Intent(NewHome.this, BkrPhoneDirectoryHome.class));
//
//
//                    } else {
//
//                        AlertDialog alertDialog = new AlertDialog.Builder(NewHome.this).create();
//                        alertDialog.setTitle("Check Internet Connection");
//                        alertDialog.setMessage("آپ انٹرنیٹ سے کنکٹ نہیں ہیں۔");
//                        alertDialog.show();
//                    }
//
//                    break;
//
//
//
//
//                case 6:
//
//                    if (NetworkUtil.isNetworkConnected(NewHome.this)) {
//
//                        NetworkUtil networkUtil = new NetworkUtil(NewHome.this);
//                        networkUtil.execute();
//
//                        startActivity(new Intent(NewHome.this, BMSplash.class));
//
//
//                    } else {
//
//                        AlertDialog alertDialog = new AlertDialog.Builder(NewHome.this).create();
//                        alertDialog.setTitle("Check Internet Connection");
//                        alertDialog.setMessage("آپ انٹرنیٹ سے کنکٹ نہیں ہیں۔");
//                        alertDialog.show();
//                    }
//
//
//                    break;
//
//
//                case 7:
//
//                    Intent intent17 = new Intent(NewHome.this, PrayerTimesActivity.class);
//                    startActivity(intent17);
//
//
//                    break;
//
//
//
//
//            }
//        }
//
//        @Override
//        public void onButtonChange(int position) {
//
//        }
//    });
//
//        try {
//
//        if (NetworkUtil.isNetworkConnected(NewHome.this)) {
//
//            NetworkUtil networkUtil = new NetworkUtil(NewHome.this);
//            networkUtil.execute();
//
//            getmessages();
//
//        } else {
//
//            bannerplace.setVisibility(View.GONE);
//
//
//
//        }
//
//
//    } catch (Exception e) {
//
//        Toast.makeText(this, "Welcome to The Bhakkar App", Toast.LENGTH_SHORT).show();
//
//
//    }
//
//
//}
//
//    public void getmessages() {
//        try {
//            HttpAsyncRequest request = new HttpAsyncRequest(NewHome.this, Constant.getDataUrl, HttpAsyncRequest.RequestType.POST, this, listener);
//            request.execute();
//            progress.setVisibility(View.VISIBLE);
//        } catch (Exception e) {
//
//            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
//        }
//
//        // ,,,,,,,,
//
//    }
//
//    AsyncTaskListener listener = new AsyncTaskListener() {
//        @Override
//        public void onComplete(TaskResult result) {
//
//
//            if (result.isSuccess()) {
//                progress.setVisibility(View.GONE);
//
//            } else {
//
//
//            }
//        }
//
//
//    };
//
//    @Override
//
//    public TaskResult parse(int httpCode, String response) {
//        TaskResult result = new TaskResult();
//        Log.d("Response", response);
//        if (httpCode == SUCCESS) {
//            result.success(true);
//
//            try {
//                JSONObject obj = new JSONObject(response);
//
//                if (obj.optString("error").equals("false")) {
//
//                    JSONArray array = obj.getJSONArray("banner");
//                    if (array != null && array.length() != 0) {
//                        // removing previous data from sqllite
//
//                        SqlHelper sqlHelper = new SqlHelper(NewHome.this);
//                        sqlHelper.deleteAll();
//
//                        String[] path = new String[array.length()];
//
//                        for (int i = 0; i < array.length(); i++) {
//                            path[i] = array.getJSONObject(i).getString("url");
//
//                        }
//
//                        msgpager.setAdapter(new CustomPagerAdapter(NewHome.this, path));
//                        msgpager.setDirection(AutoScrollViewPager.RIGHT);
//                        msgpager.startAutoScroll();
//                        msgpager.setCycle(true);
//                        msgpager.setInterval(5000);
//                        msgpager.callOnClick();
//                        msgpager.setBorderAnimation(true);
//
//                    } else {
//                        msgpager.setVisibility(View.GONE);
//                    }
//
//
//                } else if (obj.optString("error").equals("true")) {
//
//                    Toast.makeText(NewHome.this, "Some Error in Communication", Toast.LENGTH_SHORT).show();
//
//                }
//
//                result.success(true);
//            } catch (JSONException e) {
//                e.printStackTrace();
//                result.success(false);
//            }
//        }
//        return result;
//    }
//
//    public void sendNumbers(int icon) {
//
//        Intent intent = new Intent(NewHome.this, NumbersList.class);
//        intent.putExtra("nameList", nameList);
//        intent.putExtra("numberList", numberList);
//        intent.putExtra("urduName", urduNameList);
//        intent.putExtra("icon", icon);
//        startActivity(intent);
//
//
//    }
//
//    @Override
//    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.new_home, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            editor.remove("login");
//            editor.remove("userid");
//            editor.remove("phone");
//
//
//            editor.remove("name");
//            editor.remove("area");
//            editor.remove("address");
//            editor.remove("tehsil");
//            editor.commit();
//            Toast.makeText(this, "Logout Successfully", Toast.LENGTH_SHORT).show();
//
//
//        return true;
//
//    }
//
//        if (id == R.id.wallet_id) {
//
//            if (NetworkUtil.isNetworkConnected(NewHome.this)) {
//
//                NetworkUtil networkUtil = new NetworkUtil(NewHome.this);
//                networkUtil.execute();
//
//                startActivity(new Intent(NewHome.this, WalletActivity.class));
//
//
//            } else {
//
//                AlertDialog alertDialog = new AlertDialog.Builder(NewHome.this).create();
//                alertDialog.setTitle("Check Internet Connection");
//                alertDialog.setMessage("آپ انٹرنیٹ سے کنکٹ نہیں ہیں۔");
//                alertDialog.show();
//            }
//
//            return true;
//
//
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//
//
//
//
//
//    @SuppressWarnings("StatementWithEmptyBody")
//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//
//        if (id == R.id.profile) {
//            if (NetworkUtil.isNetworkConnected(NewHome.this)) {
//
//                NetworkUtil networkUtil = new NetworkUtil(NewHome.this);
//                networkUtil.execute();
//
//                if (!preferences.getString("userid", "").equals("")) {
//                    startActivity(new Intent(getApplicationContext(), SignupActivity.class).putExtra("update", 1));
//                }
//                else{
//                    startActivity(new Intent(getApplicationContext(), LoginPanel.class));}
//
//
//            } else {
//
//                AlertDialog alertDialog = new AlertDialog.Builder(NewHome.this).create();
//                alertDialog.setTitle("Check Internet Connection");
//                alertDialog.setMessage("آپ انٹرنیٹ سے کنکٹ نہیں ہیں۔");
//                alertDialog.show();
//            }
//        } else if (id == R.id.livechat) {
//
//            try {
//                Intent sendIntents = new Intent("android.intent.action.MAIN");
//                sendIntents.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
//                sendIntents.putExtra("jid", PhoneNumberUtils.stripSeparators("923246485445") + "@s.whatsapp.net");//phone number without "+" prefix
//
//                startActivity(sendIntents);
//            } catch (Exception e) {
//                Toast.makeText(NewHome.this, "Whatsapp is not install in your mobile Please install Whatsapp to continue", Toast.LENGTH_SHORT).show();
//            }
//
//        } else if (id == R.id.faqs) {
//
//            Toast.makeText(this, "We will add soon", Toast.LENGTH_SHORT).show();
//
//
//        } else if (id == R.id.contactushome) {
//
//            final AlertDialog.Builder alertdialog = new AlertDialog.Builder(NewHome.this);
//            LayoutInflater inflater =getLayoutInflater();
//            final View dialogView = inflater.inflate(R.layout.shop_close_dialog,null);
//            final ImageButton helplineBtn= dialogView.findViewById(R.id.helplineCallBtn);
//            final  ImageButton helplineBtn2 = dialogView.findViewById(R.id.helplineCallBtn2);
//            final TextView note_eng = dialogView.findViewById(R.id.eng_note);
//            final TextView note_urdu = dialogView.findViewById(R.id.urdu_note);
//            final  TextView call_us_1 = dialogView.findViewById(R.id.call_us_1);
//            call_us_1.setText("General Manager");
//            final  TextView call_us_2 = dialogView.findViewById(R.id.call_us_2);
//            call_us_2.setText("Marketing Manager");
//            alertdialog.setView(dialogView);
//
//            note_eng.setText("You can call or text(sms) us for any query.");
//            note_urdu.setVisibility(View.GONE);
//
//
//            final AlertDialog dialog = alertdialog.create();
//            dialog.show();
//
//            helplineBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel","03338995373",null));
//                    startActivity(callIntent);
//                }
//            });
//
//            helplineBtn2.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel","03331670333",null));
//                    startActivity(callIntent);
//
//                }
//            });
//
//        } else if (id == R.id.nav_share) {
//            Intent sendIntent = new Intent();
//            sendIntent.setAction(Intent.ACTION_SEND);
//            sendIntent.putExtra(Intent.EXTRA_TEXT,
//                    "Download The Bhakkar App\n \nhttps://play.google.com/store/apps/details?id=aqdeveloper.thebhakkar\"");
//            sendIntent.setType("text/plain");
//            startActivity(sendIntent);
//
//        } else if (id == R.id.nav_send) {
//
//            Toast.makeText(this, "We will add soon", Toast.LENGTH_SHORT).show();
//
//        }
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//        return true;
    }
}
