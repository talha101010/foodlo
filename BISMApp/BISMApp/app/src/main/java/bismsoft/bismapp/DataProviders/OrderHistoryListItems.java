package bismsoft.bismapp.DataProviders;

public class OrderHistoryListItems {

    String image,name,quantity,unit,price;

    public OrderHistoryListItems(String image, String name, String quantity, String unit, String price) {
        this.image = image;
        this.name = name;
        this.quantity = quantity;
        this.unit = unit;
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getUnit() {
        return unit;
    }

    public String getPrice() {
        return price;
    }
}
