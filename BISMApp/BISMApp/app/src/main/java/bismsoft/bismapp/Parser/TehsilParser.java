package bismsoft.bismapp.Parser;

import android.location.Location;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import bismsoft.bismapp.Http.TaskResult;

public class TehsilParser  implements BaseParser {

    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {
            result.success(true);
            try {
                String [] areas= null;
                JSONObject obj = new JSONObject(response);


                if( obj.optString("error").equals("0"))
                {

                    JSONArray areaArray =  obj.optJSONArray("tehsil");
                    if(areaArray!=null)
                        areas= new String[areaArray.length()+1];
                    areas[0]  = "Select Your Tehsil";
                    for(int i  = 0 ; i <areaArray.length();i++ )
                    {
                        areas[i+1] = areaArray.optJSONObject(i).optString("tehsil");
                    }
                    result.setData(areas);






                }
                else
                if( obj.optString("response").equals("true")){

                    result.setMessage("false");

                }
                result.success(true);
            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);
            }


        }
        return  result;
    }

    @Override
    public void onLocationChanged(Location location) {

    }
}
