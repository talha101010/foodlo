package bismsoft.bismapp.SuperStore;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import bismsoft.bismapp.FastFood.ShopSecondStep;
import bismsoft.bismapp.R;

public class AutomaticLocationPicker extends AppCompatActivity {

    private static final String TAG = "RegisterComplainAct";
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int REQUEST_CODE = 123;
    private static final int PLACE_PICKER_REQUEST = 1;
    private Boolean locationPermissionGranted = false;
    private GoogleMap mMap;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private static final float ZOOM_CODE = 15f;
    private TextView currentaddress;
    private Button proceedbtn;
    private double latitude,longitude;
    Activity activity;
    String check;
    public static ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_automatic_location_picker);

        activity = this;
        check = getIntent().getStringExtra("check");
        currentaddress = findViewById(R.id.currentaddresstext);
        proceedbtn = findViewById(R.id.proceedbtn);
        if (check.equals("true")){
            CheckoutSecondStep.progressDialog.dismiss();
        }else{
            ShopSecondStep.progressDialog.dismiss();
        }
        getLocationPermission();

        proceedbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (check.equals("true")) {
//                    CheckoutSecondStep.checkoutCart(AutomaticLocationPicker.this, activity, latitude, longitude, currentaddress.getText().toString(), "true");
//                    CheckoutSecondStep.checkoutCart2(AutomaticLocationPicker.this,activity);
//                    CheckoutSecondStep.empty();
//                    finish();
                }else{
//                    ShopSecondStep.checkoutCart(AutomaticLocationPicker.this, activity, latitude, longitude, currentaddress.getText().toString(), "true");
//                    ShopSecondStep.checkoutCart1(AutomaticLocationPicker.this,activity);
//                    ShopSecondStep.empty();
//                    finish();
                }
            }
        });

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
            }
        }
    }

    public void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                Toast.makeText(AutomaticLocationPicker.this, "Map is ready", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "INITMAP : map is ready");
                mMap = googleMap;

                if (locationPermissionGranted) {
                    getDeviceLocation();
                    if (ActivityCompat.checkSelfPermission(AutomaticLocationPicker.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(AutomaticLocationPicker.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                        return;
                    }
                    mMap.setMyLocationEnabled(true);
                }
            }
        });
    }

    private void getDeviceLocation(){
        Log.d(TAG,"getDeviceLocation : getting device location");
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        final String address;

        try {
            if (locationPermissionGranted){
                Task location = fusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful() && task.getResult() != null){
                            Log.d(TAG,"onComplete : found location ");
                            Location currentlocation = (Location) task.getResult();

                            moveCamera(new LatLng(currentlocation.getLatitude(),currentlocation.getLongitude()),ZOOM_CODE
                                    ,"My Location");

                            latitude = currentlocation.getLatitude();
                            longitude = currentlocation.getLongitude();
                            locationAddress(currentlocation.getLatitude(),currentlocation.getLongitude());



                        }else{
                            Log.d(TAG,"onComplete : location not found");
                            Toast.makeText(AutomaticLocationPicker.this, "unable to get current location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }catch (SecurityException e){
            Toast.makeText(this, "Security Exception "+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void moveCamera(LatLng latLng, float zoom,String title){
        Log.d(TAG,"moveCamera : moving the camera to lat : "+latLng.latitude +" lng : "+ latLng.longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,zoom));

        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .title(title);

        mMap.addMarker(options);

    }

    public void getLocationPermission(){
        Log.d(TAG,"getLocationPermission : getting location permission");
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

        if (ActivityCompat.checkSelfPermission(this.getApplicationContext(),FINE_LOCATION)== PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.checkSelfPermission(this.getApplicationContext(),COARSE_LOCATION)== PackageManager.PERMISSION_GRANTED){
                locationPermissionGranted = true;
                initMap();
            }else{
                ActivityCompat.requestPermissions(this,permissions,REQUEST_CODE);
            }
        }else{
            ActivityCompat.requestPermissions(this,permissions,REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG,"onRequestPermissionResult : Called");
        locationPermissionGranted = false;
        switch (requestCode){
            case REQUEST_CODE:
                if (grantResults.length >0){
                    for (int i=0;i<grantResults.length;i++){
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED){
                            Log.d(TAG,"onRequestPermissionResult : permission not granted");
                            locationPermissionGranted = false;
                            break;
                        }
                    }
                    Log.d(TAG,"onRequestPermissionResult : Permission granted");
                    locationPermissionGranted = true;
                    // initialize the map here
                    initMap();
                }

        }

    }

    public void locationAddress(double lat,double lng) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());


        try {
            List<Address> addresses = geocoder.getFromLocation(lat,lng, 1);
            String address23 = addresses.get(0).getAddressLine(0);
            currentaddress.setText(address23);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Exception:"+e.getMessage(), Toast.LENGTH_SHORT).show();
        }




    }
}
