package bismsoft.bismapp.DrawerActivities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import bismsoft.bismapp.FastFood.BillModel;
import bismsoft.bismapp.R;
import bismsoft.bismapp.SuperStore.CartActivity;
import bismsoft.bismapp.SuperStore.CategoryActivity;
import bismsoft.bismapp.SuperStore.SearchActivity;
import bismsoft.bismapp.SuperStore.SubCategoryAdapter;

public class SubCategoryActivity extends AppCompatActivity {

    public  static  String categoryid;
    TextView categoryname;
    RecyclerView subgrid;
    SharedPreferences preferences;
    public  static SubCategoryActivity activity;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);

        activity = this;
        preferences = getSharedPreferences("EStore", Context.MODE_PRIVATE);

        categoryname = (TextView)findViewById(R.id.categoryname);
        subgrid = (RecyclerView) findViewById(R.id.subcat_grid);
        subgrid.setHasFixedSize(true);
        subgrid.setLayoutManager(new GridLayoutManager(this, 1));


        Intent intent = getIntent();

        if (intent.hasExtra("categoryname")) {
            categoryname.setText(intent.getStringExtra("categoryname"));

        }

        if (getIntent().hasExtra("categoryid")) {
            categoryid = getIntent().getStringExtra("categoryid");
        }



        subgrid.setAdapter(new SubCategoryAdapter(this, CategoryActivity.subcategorylist));

    }

    @Override
    protected void onResume() {
        super.onResume();
        HandleCartNumber();
    }

    public  void HandleCartNumber()
    {

        ArrayList<BillModel> arrayList = null;

        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        TextView textView  = (TextView) findViewById(R.id.itemnumber);
        LinearLayout cartlayout = (LinearLayout)findViewById(R.id.cartlayout);
        LinearLayout accountlayout = (LinearLayout)findViewById(R.id.accountlayout);
        accountlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!preferences.getString("userid","").equals(""))
                    startActivity(new Intent(getApplicationContext(), SignupActivity.class).putExtra("update", 1));
                else
                    startActivity(new Intent(getApplicationContext(), LoginPanel.class));

            }
        });

        LinearLayout searchlay= (LinearLayout)findViewById(R.id.searchlayout) ;
        searchlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),SearchActivity.class));
            }
        });

        String json = appSharedPrefs.getString("cart", "");
        if (!json.equals("")) {
            arrayList = gson.fromJson(json, type);

            textView.setText(arrayList.size()+"");
            textView.setVisibility(View.VISIBLE);
            cartlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(),CartActivity.class));
                }
            });
        }
        else{
            textView.setVisibility(View.GONE);
            cartlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(), "Nothing in the Cart Yet", Toast.LENGTH_SHORT).show();
                }
            });
        }
        LinearLayout homelayout = (LinearLayout)findViewById(R.id.homelayout);

        homelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}
