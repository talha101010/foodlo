package bismsoft.bismapp.Url;

import java.util.ArrayList;

import bismsoft.bismapp.FastFood.MenuFoodItems;

public class Constant {

    public static int totalbill,totalprofit;


    public static String ipfield = "http://ilmistaan.com/bismapp/superstorewebservices/";
    public static  String EDIT_PROFILE_URL = ipfield + "/editProfile";

    public static String GET_PREVIOUS_HISTORY_URl = ipfield + "showOrderTrackingToUser.php";
    public static String gethistorydetail = ipfield + "detailOfOrderTracking.php";
    public  static ArrayList<MenuFoodItems> categoriesArrayList;
    public  static  String get_AREAS =ipfield+ "showArea.php";
    // public static String SIGNUPURL = ipfield + "signup.php";
    public static String SIGNUPURL = ipfield + "newSignup.php";
    public static String SIGINURL = ipfield + "login.php";
    public static String get_CATEGORIES_URL = ipfield + "showMainCategory.php";


    public static String GET_SUBCATEGORY_URL=ipfield+"showSubCategory.php";
    public static String GET_MENUURL=ipfield+"showMenu.php";
    public  static  String get_deals= ipfield+"showDiscountButton.php";
    public  static String get_items_url = ipfield+"showItems.php";
    public  static String get_deal_details = ipfield+"showDiscountItems.php";
    public  static String get_time_stamp = ipfield+"showTimestamp.php";


    public static String checkout_url_location = ipfield+"orders_location.php";

    //    public static String checkout_url  = ipfield+"orders.php";
    public static String checkout_url  = ipfield+"orders1.php";
    public  static  String update_profile = ipfield+"updateprofile.php";
    public static String searchURL=ipfield+"searchdata.php";
    public static String get_Tehsil_url = ipfield+"showTehsil.php";
    public static String get_Charges_url=ipfield+"showDeliveryChargesInfo.php";
    public static String FORGETPASSWORD=ipfield+"forgotpassword.php";
    public static String PASSWORDURL= ipfield+"changepassword.php";
    public static String getstoreinfo = ipfield + "showVersionAndStatus.php";
    public static  int currentversion= 2;
    public static String getfavouriateurl=ipfield+"showFavourite.php";
    public static String getcoupon= ipfield+"checkCoupan.php";
    public static String favouriateURl=ipfield+"insertFavourite.php";
    public static String get_area_details_url= ipfield+"showAreaForCheckOut.php";
}
