package bismsoft.bismapp.Parser;

import android.location.Location;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import bismsoft.bismapp.Http.TaskResult;

/**
 * Created by Hamza on 2/1/18.
 */

public class AdShopParser implements BaseParser {
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {
            result.success(true);
            try {
                JSONObject obj = new JSONObject(response);



                if(obj.optString("error").equals("0"))
                {
                    result.setMessage("phone");
                }
                else if(obj.optString("error").equals("1")){
                    result.setMessage("name");

                }
                else{
                    result.setMessage(obj.optString("error"));

                }


                result.success(true);
            } catch (JSONException e) {
                e.printStackTrace();

                if (e.getMessage().equals("Value [] of type org.json.JSONArray cannot be converted to JSONObject")) {
                    result.setMessage("exsist");
                    result.success(true);
                } else {
                    result.success(false);
                }
            }


        }
        return result;
    }

    @Override
    public void onLocationChanged(Location location) {

    }
}
