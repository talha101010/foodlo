package bismsoft.bismapp.DrawerActivities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;

import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import bismsoft.bismapp.FastFood.MyProgressDialog;
import bismsoft.bismapp.Http.AsyncTaskListener;
import bismsoft.bismapp.Http.HttpAsyncRequest;
import bismsoft.bismapp.Http.TaskResult;
import bismsoft.bismapp.Parser.BaseParser;
import bismsoft.bismapp.R;
import bismsoft.bismapp.Url.Constant;

public class LoginPanel extends AppCompatActivity  implements BaseParser {

    Button siginbtn; TextView signup;
    EditText username, password;
    TextView forgetpassword;
    SharedPreferences preferences;

    private String TAG = "LoginPanel";
    private SharedPreferences.Editor editor;
    private MyProgressDialog myProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login_panel);

        init();
//        if(preferences.getString("login","").equals("success"))
//        {
//            startActivity(new Intent(LoginPanel.this,CategoryActivity.class));
//            finish();
//
//        }






        siginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean test = true;
                if (username.length() == 0) {
                    test = false;

                    username.setError("Mobile Number cannot be Blank");
                }
                if (username.length() < 11) {
                    test = false;

                    username.setError("Invalid Mobile Number");
                }
                if (password.length() == 0) {
                    test = false;

                    password.setError("Password cannot be Blank");


                }
                if (test)
                    postdatatoserver();
            }


        });
        forgetpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginPanel.this, ForgetPassword.class));
            }
        });

    }

    void postdatatoserver() {
        HttpAsyncRequest request = new HttpAsyncRequest(this, Constant.SIGINURL, HttpAsyncRequest.RequestType.POST, this, listener);
        request.addParam("cellnumber", username.getText().toString().trim());
        request.addParam("password", password.getText().toString().trim());

        request.execute();
        myProgressDialog = new MyProgressDialog(LoginPanel.this);
        myProgressDialog.show();
        myProgressDialog.setCanceledOnTouchOutside(false);

    }
    AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            try {
                myProgressDialog.dismiss();
            } catch (Exception e) {
            }
            if (result.isSuccess()) {

                if (result.getMessage().equals("true")) {
                    Toast.makeText(LoginPanel.this, "Successfull Login", Toast.LENGTH_SHORT).show();

                    editor.putString("login","success");
                    editor.commit();
                    //  startActivity(new Intent(LoginPanel.this,CategoryActivity.class));
                    finish();


                } else {

                    password.setError("Wrong Password or Username");


                }
            } else {

                Toast.makeText(LoginPanel.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();


            }
        }


    };

    private void init() {

        siginbtn = (Button) findViewById(R.id.sigin_siginbtn);
        username = (EditText) findViewById(R.id.sigin_username);
        password = (EditText) findViewById(R.id.sigin_password);
        forgetpassword = (TextView) findViewById(R.id.sigin_forgetpassword);
        preferences = this.getSharedPreferences("EStore", Context.MODE_PRIVATE);
        // preferences = getSharedPreferences("EMenu", Context.MODE_PRIVATE);
        editor = preferences.edit();
        signup = (TextView)

                findViewById(R.id.sigup_siginbtn);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginPanel.this, SignupActivity.class));
                finish();

            }
        });


    }
    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {
            result.success(true);
            try {
                JSONObject data = new JSONObject(response);

                String check = data.optString("message");
                if (check.equals("1")) {




                    result.setMessage("true");


                    editor.putString("userid",data.optString("id"));
                    editor.putString("name",data.optString("name"));
                    editor.putString("phone",data.optString("cell_number"));
                    editor.putString("area",data.optString("area"));
                    editor.putString("address",data.optString("address"));
                    editor.putString("tehsil",data.optString("tehsil"));
                    editor.commit();

                } else if (check.equals("2")) {
                    result.setMessage("false");


                }

            } catch (Exception e) {
                e.printStackTrace();
                result.success(false);

            }


        } else {
            result.success(false);

        }
        return result;
    }

    @Override
    public void onLocationChanged(Location location) {

    }
}
