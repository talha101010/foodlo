package bismsoft.bismapp.SuperStore;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import bismsoft.bismapp.DrawerActivities.DealsActivity;
import bismsoft.bismapp.DrawerActivities.FoodItemsActivity;
import bismsoft.bismapp.FastFood.BillModel;
import bismsoft.bismapp.FastFood.FoodItemHolder;
import bismsoft.bismapp.FastFood.MyProgressDialog;
import bismsoft.bismapp.FastFood.QuantityHolder;
import bismsoft.bismapp.R;

public class FooitemAdapter extends BaseAdapter  {
    private static Context context;
    private static LayoutInflater inflater;
    private final SharedPreferences preferences;
    public  static   ArrayList<FoodItemHolder> foodlist;
    ArrayList<QuantityHolder> quantityHolders;
    SharedPreferences.Editor editor;
    SharedPreferences appSharedPrefs;
    private MyProgressDialog myProgressDialog;
    int indexoffooditem;
    private int index;
    FooitemAdapter senario;


    public FooitemAdapter(Context mainActivity, ArrayList<FoodItemHolder> listitems) {
        // TODO Auto-generated constructor stub
        context = mainActivity;
        this.foodlist = listitems;
        preferences = context.getSharedPreferences("EStore", Context.MODE_PRIVATE);
        editor = preferences.edit();
        senario = this;
        appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);

        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return foodlist.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        ImageView foodimage;
        TextView foodname, fooddescription, price, quantity;
        Button addtocart, plus, minus;
        ;
        ProgressBar progressBar;
        RelativeLayout layout;
        TextView underlineprice,percenttextview;


        public Holder(View v) {
            foodimage = (ImageView) v.findViewById(R.id.foodimage_fooditem);
            foodname = (TextView) v.findViewById(R.id.fooditemname_fooditem);
            addtocart = (Button) v.findViewById(R.id.addtocart_fooditem);
            fooddescription = (TextView) v.findViewById(R.id.fooditemdescription_fooditem);
            price = (TextView) v.findViewById(R.id.fooditemprice_fooditem);
            progressBar = (ProgressBar) v.findViewById(R.id.progress);

            quantity = (TextView) v.findViewById(R.id.quantity);
            minus = (Button) v.findViewById(R.id.minus);
            plus = (Button) v.findViewById(R.id.plus);
            layout = (RelativeLayout)v.findViewById(R.id.lay);
            underlineprice =v.findViewById(R.id.underline_fooditem);
            percenttextview = v.findViewById(R.id.percenttextview);
        }
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final Holder holder;

        View rowView;
        rowView = inflater.inflate(R.layout.row_fooditems, null);
        holder = new Holder(rowView);



        if(foodlist.get(position).getUnderlineprice()!=null) {
            holder.underlineprice.setVisibility(View.VISIBLE);
            holder.underlineprice.setText(foodlist.get(position).getUnderlineprice());
            holder.underlineprice.setPaintFlags( holder.underlineprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            float np = Integer.parseInt(foodlist.get(position).price);
            float up = Integer.parseInt(foodlist.get(position).getUnderlineprice());
            float ans =  (np/up)*100;
            int ans1 = (int)ans;
            int ans2 = 100-ans1;
            holder.percenttextview.setVisibility(View.VISIBLE);
            holder.percenttextview.setText(ans2+"%"+"\nOFF");

        }



        if (verifyOrder(position, holder.quantity)) {
            holder.addtocart.setVisibility(View.GONE);
            holder.plus.setVisibility(View.VISIBLE);
            holder.minus.setVisibility(View.VISIBLE);
            holder.quantity.setVisibility(View.VISIBLE);

        } else {
            holder.addtocart.setVisibility(View.VISIBLE);
            holder.plus.setVisibility(View.GONE);
            holder.minus.setVisibility(View.GONE);
            holder.quantity.setVisibility(View.GONE);
        }


        Glide.with(context)
                .load(foodlist.get(position).getImagepath())
                .dontAnimate()

                .placeholder(R.drawable.logoone)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.foodimage);


        holder.foodname.setText(foodlist.get(position).getProductname().toUpperCase());
        holder.price.setText(foodlist.get(position).getProductdescription());

        holder.fooddescription.setText("Rs " + foodlist.get(position).getPrice());


        holder.addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (AddtoNormalCart(position)) {
                    holder.addtocart.setVisibility(View.GONE);
                    holder.addtocart.setVisibility(View.GONE);
                    holder.plus.setVisibility(View.VISIBLE);
                    holder.minus.setVisibility(View.VISIBLE);
                    holder.quantity.setVisibility(View.VISIBLE);
                    holder.quantity.setText("1");
                }
                if(FoodItemsActivity.activity !=null)
                    FoodItemsActivity.HandleCartNumber();

                if(DealsActivity.activity!=null)
                    DealsActivity.activity.HandleCartNumber();

                if(SearchActivity.activity!=null)
                    SearchActivity.activity.HandleCartNumber();


            }
        });


        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int q = Integer.parseInt(holder.quantity.getText().toString());
                q += 1;
                holder.quantity.setText(q + "");
                updateCart(q + "", position);


            }
        });


        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int q = Integer.parseInt(holder.quantity.getText().toString());
                if (q != 1) {
                    q -= 1;
                    holder.quantity.setText(q + "");
                    updateCart(q + "", position);

                } else {
                    if (RemoveOrder(foodlist.get(position).getDealid())) {
                        notifyDataSetChanged();
                        if(FoodItemsActivity.activity!=null)
                            FoodItemsActivity.HandleCartNumber();

                        if(DealsActivity.activity!=null)
                            DealsActivity.activity.HandleCartNumber();

                        if(SearchActivity.activity!=null)
                            SearchActivity.activity.HandleCartNumber();
                    }

                }
            }
        });


        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                context.startActivity(new Intent(context, SSDetailActivity.class)
                        .putExtra("name",foodlist.get(position).getProductname())
                        .putExtra("price",foodlist.get(position).getPrice())
                        .putExtra("unit",foodlist.get(position).getProductdescription())
                        .putExtra("path",foodlist.get(position).getImagepath())
                        .putExtra("id",foodlist.get(position).getDealid())
                        .putExtra("store",foodlist.get(position).getStorename())

                );



            }
        });


        return rowView;
    }


    private boolean AddtoNormalCart(int position) {
        ArrayList<BillModel> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        String json = appSharedPrefs.getString("cart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            arrayList = new ArrayList<>();


        BillModel billHolder = new BillModel();
        billHolder.setExsist(true);
        billHolder.setFooditem_id(foodlist.get(position).getDealid());
        billHolder.setBillprice(foodlist.get(position).getPrice());
        billHolder.setQuantity("1");
        billHolder.setProductprice(foodlist.get(position).getPrice());
        billHolder.setImagepath(foodlist.get(position).getImagepath());
        billHolder.setStorename(foodlist.get(position).getStorename());
        billHolder.setUnit(foodlist.get(position).getProductdescription());

        billHolder.setName(foodlist.get(position).getProductname());
        arrayList.add(billHolder);


        // savong cart to share preferences


        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("cart", jsons);
        prefsEditor.commit();
        return true;
        // Toast.makeText(context, "item added", Toast.LENGTH_SHORT).show();

    }


    public boolean verifyOrder(int position, TextView quantity) {
        ArrayList<BillModel> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        String json = appSharedPrefs.getString("cart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            return false;


        for (int i = 0; i < arrayList.size(); i++) {


            if (arrayList.get(i).getFooditem_id().equals(foodlist.get(position).getDealid())) {
                index = i;
                quantity.setText(arrayList.get(index).getQuantity());
                return true;

            }


        }

        return false;
    }

    public boolean RemoveOrder(String id) {
        ArrayList<BillModel> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        String json = appSharedPrefs.getString("cart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            return false;


        for (int i = 0; i < arrayList.size(); i++) {


            if (arrayList.get(i).getFooditem_id().equals(id)) {
                index = i;
                arrayList.remove(index);
                break;
            }


        }

        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("cart", jsons);
        prefsEditor.commit();
        return true;


    }

    public void updateCart(String quantity, int position) {
        ArrayList<BillModel> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        String json = appSharedPrefs.getString("cart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            arrayList = new ArrayList<>();

        for (int i = 0; i < arrayList.size(); i++) {


            if (arrayList.get(i).getFooditem_id().equals(foodlist.get(position).getDealid())) {
                index = i;
                BillModel bill = arrayList.get(i);
                bill.setQuantity(quantity);
                break;


            }


        }


        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("cart", jsons);
        prefsEditor.commit();
    }

}