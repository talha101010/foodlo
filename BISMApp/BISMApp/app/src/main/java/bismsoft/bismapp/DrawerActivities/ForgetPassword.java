package bismsoft.bismapp.DrawerActivities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import bismsoft.bismapp.FastFood.MyProgressDialog;
import bismsoft.bismapp.Http.AsyncTaskListener;
import bismsoft.bismapp.Http.HttpAsyncRequest;
import bismsoft.bismapp.Http.TaskResult;
import bismsoft.bismapp.Parser.ForgetPasswordParser;
import bismsoft.bismapp.R;
import bismsoft.bismapp.Url.Constant;

public class ForgetPassword extends AppCompatActivity {

    EditText email, verificationcode, password, re_enterpassword;
    Button submit, sendverifycode,sendpassword;
    LinearLayout emaillayout, verficationcodelayout, passwordlayout;
    private MyProgressDialog myProgressDialog;
    EditText oldpassword;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_forget_password);




        init();
        preferences = this.getSharedPreferences("EStore", Context.MODE_PRIVATE);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email.length() == 0) {
                    email.setError("Cannot be empty");
                } else {

                    postdatatoserver();

                }
            }
        });
        if(getIntent().hasExtra("change"))
        {
            emaillayout.setVisibility(View.GONE);
            passwordlayout.setVisibility(View.VISIBLE);
        }

        sendpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean check = true;
                if(oldpassword.length() ==0)
                {
                    check = false;
                    oldpassword.setError("Cannot be blank");
                }
                if(password.length() ==0)
                {
                    check = false;
                    password.setError("Cannot be blank");
                }
                if(!password.getText().toString().equals(re_enterpassword.getText().toString()))
                {
                    re_enterpassword.setError("Password not Match");
                    check = false;
                }
                if(check)
                {
                    sendpasswordtoserver();
                }
            }
        });
    }

    private void sendpasswordtoserver() {
        HttpAsyncRequest request = new HttpAsyncRequest(this, Constant.PASSWORDURL, HttpAsyncRequest.RequestType.POST, new ForgetPasswordParser(), listenerforpassword);
        request.addParam("old_password", oldpassword.getText().toString().trim());
        request.addParam("new_password", password.getText().toString().trim());
        request.addParam("phone",preferences.getString("phone",""));




        request.execute();
        myProgressDialog = new MyProgressDialog(ForgetPassword.this);
        myProgressDialog.show();
        myProgressDialog.setCanceledOnTouchOutside(false);
    }



    private void init() {

        email = (EditText) findViewById(R.id.forget_username);
        submit = (Button) findViewById(R.id.forget_send);
        emaillayout = (LinearLayout) findViewById(R.id.forget_emaillayout);
        oldpassword = (EditText)findViewById(R.id.forget_passwordold);

        passwordlayout = (LinearLayout) findViewById(R.id.forget_PASSWORDLAYOUT);
        password = (EditText) findViewById(R.id.forget_passwordfirst);
        re_enterpassword = (EditText) findViewById(R.id.forget_passwordsecond);
        sendpassword = (Button)findViewById(R.id.forget_submitpassword);


    }

    void postdatatoserver() {
        HttpAsyncRequest request = new HttpAsyncRequest(this, Constant.FORGETPASSWORD, HttpAsyncRequest.RequestType.POST, new ForgetPasswordParser(), listener);
        request.addParam("phone", email.getText().toString().trim());
        request.execute();
        myProgressDialog = new MyProgressDialog(ForgetPassword.this);
        myProgressDialog.show();
        myProgressDialog.setCanceledOnTouchOutside(false);
        email.setText("");


    }

    AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            myProgressDialog.dismiss();
            if (result.isSuccess()) {

                if (result.getMessage().equals("1")) {
                    Toast.makeText(ForgetPassword.this, "Try After Sometimes", Toast.LENGTH_SHORT).show();

                } else if (result.getMessage().equals("0")) {
                    AlertDialog.Builder builder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(ForgetPassword.this, android.R.style.Theme_Material_Dialog_Alert);
                    } else {
                        builder = new AlertDialog.Builder(ForgetPassword.this);
                    }
                    builder.setTitle("Warning")
                            .setMessage("A Password will be sent to your mobile number soon")
                            .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete



                                }
                            })
                            .show();

                } else if (result.getMessage().equals("2")) {
                    email.setError("Mobile Number not Registered ");

                }

            } else {

                Toast.makeText(getApplicationContext(), "Error Communicating with the Server", Toast.LENGTH_SHORT).show();

            }
        }


    };


    AsyncTaskListener listenerforpassword = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            try{
                myProgressDialog.dismiss();

            }catch (Exception e)
            {

            }
            if (result.isSuccess()) {

                if (result.getMessage().equals("1")) {
                    Toast.makeText(ForgetPassword.this, "Try After Sometimes", Toast.LENGTH_SHORT).show();

                } else if (result.getMessage().equals("0")) {
                    AlertDialog.Builder builder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(ForgetPassword.this, android.R.style.Theme_Material_Dialog_Alert);
                    } else {
                        builder = new AlertDialog.Builder(ForgetPassword.this);
                    }
                    builder.setTitle("Information")
                            .setMessage("Your Password is change successfully")
                            .setPositiveButton("close", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    finish();


                                }
                            })
                            .show();

                } else if (result.getMessage().equals("2")) {
                    oldpassword.setError("Wrong Password");

                }

            } else {

                Toast.makeText(getApplicationContext(), "Error Communicating with the Server", Toast.LENGTH_SHORT).show();

            }
        }


    };
}
