package bismsoft.bismapp.FastFood;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import bismsoft.bismapp.DrawerActivities.ShopDetailActivity;
import bismsoft.bismapp.DrawerActivities.ShopSearchActivity;
import bismsoft.bismapp.R;

public class ShopItemsAdaptor   extends BaseAdapter {
    private static Context context;
    private static LayoutInflater inflater;
    private final SharedPreferences preferences;
    public static ArrayList<FoodItemHolder> foodlist;
    ArrayList<QuantityHolder> quantityHolders;
    SharedPreferences.Editor editor;
    SharedPreferences appSharedPrefs;
    private MyProgressDialog myProgressDialog;
    int indexoffooditem;
    private int index;

    public ShopItemsAdaptor(Context mainActivity, ArrayList<FoodItemHolder> listitems) {
        // TODO Auto-generated constructor stub
        context = mainActivity;
        this.foodlist = listitems;
        preferences = context.getSharedPreferences("EStore", Context.MODE_PRIVATE);
        editor = preferences.edit();
        appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);

        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return foodlist.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        ImageView foodimage;
        TextView foodname, fooddescription, price, quantity;
        Button addtocart,plus, minus;

        ;
        ProgressBar progressBar;
        RelativeLayout layout;


        public Holder(View v) {
            foodimage = (ImageView) v.findViewById(R.id.foodimage_fooditem);
            foodname = (TextView) v.findViewById(R.id.fooditemname_fooditem);
            addtocart = (Button) v.findViewById(R.id.addtocart_fooditem);
            fooddescription = (TextView) v.findViewById(R.id.fooditemdescription_fooditem);
            price = (TextView) v.findViewById(R.id.fooditemprice_fooditem);
            progressBar = (ProgressBar) v.findViewById(R.id.progress);

            quantity = (TextView) v.findViewById(R.id.quantity);
            minus =  v.findViewById(R.id.minus);
            plus =  v.findViewById(R.id.plus);
            layout = (RelativeLayout) v.findViewById(R.id.lay);
        }
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final ShopItemsAdaptor.Holder holder;

        View rowView;
        rowView = inflater.inflate(R.layout.row_fooditems, null);
        holder = new ShopItemsAdaptor.Holder(rowView);


        if (verifyOrder(position, holder.quantity)) {
            holder.addtocart.setVisibility(View.GONE);
            holder.plus.setVisibility(View.VISIBLE);
            holder.minus.setVisibility(View.VISIBLE);
            holder.quantity.setVisibility(View.VISIBLE);

        } else {
            holder.addtocart.setVisibility(View.VISIBLE);
            holder.plus.setVisibility(View.GONE);
            holder.minus.setVisibility(View.GONE);
            holder.quantity.setVisibility(View.GONE);
        }


        Glide.with(context)
                .load(foodlist.get(position).getImagepath())
                .dontAnimate()

                .placeholder(R.drawable.logoone)

                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.foodimage);


        holder.foodname.setText(foodlist.get(position).getProductname());
        holder.price.setText(foodlist.get(position).getProductdescription());

        holder.fooddescription.setText("Rs " + foodlist.get(position).getPrice());


        holder.addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putString("shop", ShopMenu.shopname);
                editor.commit();

                if (AddtoNormalCart(position)) {
                    holder.addtocart.setVisibility(View.GONE);
                    holder.addtocart.setVisibility(View.GONE);
                    holder.plus.setVisibility(View.VISIBLE);
                    holder.minus.setVisibility(View.VISIBLE);
                    holder.quantity.setVisibility(View.VISIBLE);
                    holder.quantity.setText("1");
                }
                if (ShopMenu.activity != null)
                    ShopMenu.HandleCartNumber();


                if (ShopSearchActivity.activity != null)
                    ShopSearchActivity.activity.HandleCartNumber();


            }
        });


        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int q = Integer.parseInt(holder.quantity.getText().toString());
                q += 1;
                holder.quantity.setText(q + "");
                updateCart(q + "", position);


            }
        });


        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int q = Integer.parseInt(holder.quantity.getText().toString());
                if (q != 1) {
                    q -= 1;
                    holder.quantity.setText(q + "");
                    updateCart(q + "", position);

                } else {
                    if (RemoveOrder(foodlist.get(position).getDealid())) {
                        notifyDataSetChanged();
                        if (ShopMenu.activity != null)
                            ShopMenu.HandleCartNumber();


                        if (ShopMenu.activity != null)
                            ShopMenu.HandleCartNumber();


                        if (ShopSearchActivity.activity != null)
                            ShopSearchActivity.activity.HandleCartNumber();
                    }

                }
            }
        });


        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                context.startActivity(new Intent(context, ShopDetailActivity.class)
                        .putExtra("name", foodlist.get(position).getProductname())
                        .putExtra("price", foodlist.get(position).getPrice())
                        .putExtra("unit", foodlist.get(position).getProductdescription())
                        .putExtra("path", foodlist.get(position).getImagepath())
                        .putExtra("id", foodlist.get(position).getDealid())
                        .putExtra("store", foodlist.get(position).getStorename())

                );


            }
        });


        return rowView;
    }


    private boolean AddtoNormalCart(int position) {
        ArrayList<BillModel> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        String json = appSharedPrefs.getString("shopcart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            arrayList = new ArrayList<>();


        BillModel billHolder = new BillModel();
        billHolder.setExsist(true);
        billHolder.setFooditem_id(foodlist.get(position).getDealid());
        billHolder.setBillprice(foodlist.get(position).getPrice());
        billHolder.setQuantity("1");
        billHolder.setProductprice(foodlist.get(position).getPrice());
        billHolder.setImagepath(foodlist.get(position).getImagepath());
        billHolder.setStorename(foodlist.get(position).getStorename());
        billHolder.setUnit(foodlist.get(position).getProductdescription());

        billHolder.setName(foodlist.get(position).getProductname());
        arrayList.add(billHolder);


        // savong cart to share preferences


        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("shopcart", jsons);
        prefsEditor.commit();
        return true;
        // Toast.makeText(context, "item added", Toast.LENGTH_SHORT).show();

    }


    public boolean verifyOrder(int position, TextView quantity) {
        ArrayList<BillModel> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        String json = appSharedPrefs.getString("shopcart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            return false;


        for (int i = 0; i < arrayList.size(); i++) {


            if (arrayList.get(i).getFooditem_id().equals(foodlist.get(position).getDealid())) {
                index = i;
                quantity.setText(arrayList.get(index).getQuantity());
                return true;

            }


        }

        return false;
    }

    public boolean RemoveOrder(String id) {
        ArrayList<BillModel> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        String json = appSharedPrefs.getString("shopcart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            return false;


        for (int i = 0; i < arrayList.size(); i++) {


            if (arrayList.get(i).getFooditem_id().equals(id)) {
                index = i;
                arrayList.remove(index);
                break;
            }


        }

        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("shopcart", jsons);
        prefsEditor.commit();
        return true;


    }

    public void updateCart(String quantity, int position) {
        ArrayList<BillModel> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        String json = appSharedPrefs.getString("shopcart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            arrayList = new ArrayList<>();

        for (int i = 0; i < arrayList.size(); i++) {


            if (arrayList.get(i).getFooditem_id().equals(foodlist.get(position).getDealid())) {
                index = i;
                BillModel bill = arrayList.get(i);
                bill.setQuantity(quantity);
                break;


            }


        }


        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("shopcart", jsons);
        prefsEditor.commit();
    }

}