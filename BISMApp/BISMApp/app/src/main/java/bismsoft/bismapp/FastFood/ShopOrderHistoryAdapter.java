package bismsoft.bismapp.FastFood;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import bismsoft.bismapp.Helper.HistoryModel;
import bismsoft.bismapp.R;

public class ShopOrderHistoryAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    ArrayList<HistoryModel> foodlist;

    public ShopOrderHistoryAdapter(Context mainActivity, ArrayList<HistoryModel> listitems) {
        // TODO Auto-generated constructor stub
        context = mainActivity;
        this.foodlist = listitems;

        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return foodlist.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView billid,totalbill,date;
        View approveview,delieveryview,delieveredview;
        ImageView approveimage,deliveryimage,deliveredimage;
        CardView card_view;


        public Holder(View v) {
            billid = (TextView)v.findViewById(R.id.billid);
            totalbill = (TextView)v.findViewById(R.id.totalbill);
            date = (TextView)v.findViewById(R.id.date);

            approveview = (View)v.findViewById(R.id.approveview);
            delieveryview = (View)v.findViewById(R.id.deliveryview);
            delieveredview = (View)v.findViewById(R.id.delieveredview);

            approveimage = (ImageView) v.findViewById(R.id.approveimge);
            deliveryimage = (ImageView) v.findViewById(R.id.deliveryimage);
            deliveredimage = (ImageView) v.findViewById(R.id.deliveredimage);
            card_view = (CardView) v.findViewById(R.id.card_view);



        }
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final ShopOrderHistoryAdapter.Holder holder;

        View rowView;
        rowView = inflater.inflate(R.layout.row_orderhistory, null);
        holder = new ShopOrderHistoryAdapter.Holder(rowView);


        holder.billid.setText(foodlist.get(position).getId());
        holder.date.setText(foodlist.get(position).getDate());
        holder.totalbill.setText("Rs "+foodlist.get(position).getBill());


        switch (foodlist.get(position).getStatus())
        {
            case "Approve":
                holder.approveimage.setImageResource(R.drawable.approvecolor);


                if( android.os.Build.VERSION.SDK_INT <23)
                    holder. approveview.setBackgroundColor(context.getResources().getColor(R.color.main_color));
                else
                    holder.approveview.setBackgroundColor(context.getResources().getColor(R.color.main_color,null));


                holder.deliveryimage.setImageResource(R.drawable.deliverycolor);


                if( android.os.Build.VERSION.SDK_INT <23)
                    holder. delieveryview.setBackgroundColor(context.getResources().getColor(R.color.main_color));
                else
                    holder.delieveryview.setBackgroundColor(context.getResources().getColor(R.color.main_color,null));
                break;
            case "Rejected":
                holder.approveimage.setImageResource(R.drawable.reject);


                if( android.os.Build.VERSION.SDK_INT <23)
                    holder. approveview.setBackgroundColor(context.getResources().getColor(R.color.main_color));
                else
                    holder.approveview.setBackgroundColor(context.getResources().getColor(R.color.main_color,null));

                break;
            case "Delivered":
                holder.approveimage.setImageResource(R.drawable.approvecolor);


                if( android.os.Build.VERSION.SDK_INT <23)
                    holder. approveview.setBackgroundColor(context.getResources().getColor(R.color.main_color));
                else
                    holder.approveview.setBackgroundColor(context.getResources().getColor(R.color.main_color,null));


                holder.deliveryimage.setImageResource(R.drawable.deliverycolor);


                if( android.os.Build.VERSION.SDK_INT <23)
                    holder. delieveryview.setBackgroundColor(context.getResources().getColor(R.color.main_color));
                else
                    holder.delieveryview.setBackgroundColor(context.getResources().getColor(R.color.main_color,null));
                holder.deliveredimage.setImageResource(R.drawable.delieveredcolor);


                if( android.os.Build.VERSION.SDK_INT <23)
                    holder. delieveredview.setBackgroundColor(context.getResources().getColor(R.color.main_color));
                else
                    holder.delieveredview.setBackgroundColor(context.getResources().getColor(R.color.main_color,null));

                break;





        }
        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(context);
                }
                builder.setTitle("Information")
                        .setMessage("Your Order is "+foodlist.get(position).getStatus())
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete


                            }
                        })
                        .show();

            }
        });




        return rowView;
    }
}
