package bismsoft.bismapp.Parser;

import android.location.Location;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bismsoft.bismapp.FastFood.FoodItemHolder;
import bismsoft.bismapp.FastFood.SizeObject;
import bismsoft.bismapp.Http.TaskResult;

public class FoodItemParser implements BaseParser {
    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {

            try {
                result.success(true);
                JSONObject obj = new JSONObject(response);
                if (obj.optString("error").equals("false")) {
                    result.setMessage("true");

                    JSONArray array = obj.optJSONArray("data");
                    ArrayList<FoodItemHolder> list = new ArrayList<>(array.length());
                    ArrayList<SizeObject> temp = new ArrayList<>();

                    if (array.length() != 0 && array!=null) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject items = array.getJSONObject(i);
                            FoodItemHolder food = new FoodItemHolder();
                            food.setImagepath(items.optString("url"));
                            food.setProductname(items.optString("item_name"));
                            food.setDealid(items.optString("id"));
                            food.setProductdescription(items.optString("item_unit"));
                            food.setPrice(items.optString("item_price"));
                            food.setStorename(items.optString("store_name"));
                            list.add(food);
                        }
                        result.setData(list);
                    }
                    else{
                        result.setMessage("false");

                    }

                } else if (obj.optString("error").equals("true")) {
                    result.setMessage("false");
                }

            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);

            }


        }
        return result;
    }

    @Override
    public void onLocationChanged(Location location) {

    }
}
