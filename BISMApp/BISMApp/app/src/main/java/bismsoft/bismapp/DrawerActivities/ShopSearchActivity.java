package bismsoft.bismapp.DrawerActivities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import bismsoft.bismapp.FastFood.BillModel;
import bismsoft.bismapp.FastFood.FoodItemHolder;
import bismsoft.bismapp.FastFood.MyProgressDialog;
import bismsoft.bismapp.FastFood.ShopItemsAdaptor;
import bismsoft.bismapp.Http.AsyncTaskListener;
import bismsoft.bismapp.Http.HttpAsyncRequest;
import bismsoft.bismapp.Http.TaskResult;
import bismsoft.bismapp.Parser.BaseParser;
import bismsoft.bismapp.R;
import bismsoft.bismapp.Url.ConstantsforShop;

public class ShopSearchActivity extends AppCompatActivity implements BaseParser {

    ListView list;
    EditText searchtext;
    public static TextView textView;
    public static LinearLayout cartlayout;
    Button search;
    MyProgressDialog myProgressDialog;
    ShopItemsAdaptor fooditemadapter;
    TextView nomsg;
    public static ShopSearchActivity activity;
    public static ArrayList<FoodItemHolder> foodlist;
    ProgressBar progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_search);

        list = (ListView) findViewById(R.id.list);
        searchtext = (EditText) findViewById(R.id.searchtext);
        search = (Button) findViewById(R.id.search);
        nomsg = (TextView) findViewById(R.id.nomsg);
        textView = (TextView) findViewById(R.id.itemnumber);
        cartlayout = (LinearLayout) findViewById(R.id.cartlayout);
        progress = (ProgressBar)findViewById(R.id.progress);

        activity = this;

        searchtext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                sendSearch();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });




    }

    private void sendSearch() {


        HttpAsyncRequest request = new HttpAsyncRequest(this, ConstantsforShop.searchURL, HttpAsyncRequest.RequestType.POST, this, listener);
        request.addParam("name", searchtext.getText().toString());


        request.execute();
        progress.setVisibility(View.VISIBLE);

    }

    public AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            progress.setVisibility(View.GONE);

            if (result.isSuccess()) {

                if (result.getMessage().equals("true")) {
                    fooditemadapter = new ShopItemsAdaptor(ShopSearchActivity.this, foodlist = (ArrayList<FoodItemHolder>) result.getData());
                    list.setAdapter(fooditemadapter);
                    list.setVisibility(View.VISIBLE);
                    nomsg.setVisibility(View.GONE);


                } else if (result.getMessage().equals("false")) {
//                    myProgressDialog.dismiss();
                    list.setVisibility(View.GONE);
                    nomsg.setVisibility(View.VISIBLE);
                    // Toast.makeText(activity, "This Menu is not active yet", Toast.LENGTH_SHORT).show();
                }


            } else {

                Toast.makeText(ShopSearchActivity.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();

            }
        }


    };

    public static void HandleCartNumber() {

        ArrayList<BillModel> arrayList = null;

        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(activity);
        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        LinearLayout accountlayout = (LinearLayout) activity.findViewById(R.id.accountlayout);
        accountlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivity(new Intent(activity, SignupActivity.class).putExtra("update", 1));
            }
        });


        LinearLayout searchlay = (LinearLayout) activity.findViewById(R.id.searchlayout);
        searchlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivity(new Intent(activity, ShopSearchActivity.class));
            }
        });

        ImageView searchimaeg = (ImageView)activity.findViewById(R.id.searchimage);
        TextView searchtext = (TextView)activity.findViewById(R.id.searchtext);
        ImageView homeimage  =  (ImageView)activity.findViewById(R.id.homeimage);

        TextView hometext = (TextView)activity.findViewById(R.id.hometext);

        homeimage.setImageResource(R.drawable.home);
        searchimaeg.setImageResource(R.drawable.searchcolor);


        if( android.os.Build.VERSION.SDK_INT <23)
            hometext.setTextColor(activity.getResources().getColor(R.color.text_color));
        else
            hometext.setTextColor(activity.getResources().getColor(R.color.text_color,null));



        if( android.os.Build.VERSION.SDK_INT <23)
            searchtext.setTextColor(activity.getResources().getColor(R.color.main_color));
        else
            searchtext.setTextColor(activity.getResources().getColor(R.color.main_color,null));





        String json = appSharedPrefs.getString("shopcart", "");
        if (!json.equals("")) {
            arrayList = gson.fromJson(json, type);

            textView.setText(arrayList.size() + "");
            textView.setVisibility(View.VISIBLE);
            cartlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.startActivity(new Intent(activity, ShopCartActivity.class));
                }
            });
        } else {
            textView.setVisibility(View.GONE);
            cartlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(activity, "Nothing in the Cart Yet", Toast.LENGTH_SHORT).show();
                }
            });
        }
        LinearLayout homelayout = (LinearLayout)activity.findViewById(R.id.homelayout);

        homelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity. finish();
                if(SubCategoryActivity.activity!=null)
                    SubCategoryActivity.activity.finish();

                if(FoodItemsActivity.activity!=null)
                    FoodItemsActivity.activity.finish();
//                if(DetailActivity.activity!=null)
//                    DetailActivity.activity.finish();
                if(DealsActivity.activity!=null)
                    DealsActivity.activity.finish();
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        HandleCartNumber();
        if (fooditemadapter != null)
            fooditemadapter.notifyDataSetChanged();

    }
    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {

            try {
                result.success(true);
                JSONObject obj = new JSONObject(response);
                if (obj.optString("error").equals("0")) {
                    result.setMessage("true");

                    JSONArray array = obj.optJSONArray("data");
                    ArrayList<FoodItemHolder> list = new ArrayList<>(array.length());

                    if (array.length() != 0 && array!=null) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject items = array.getJSONObject(i);
                            FoodItemHolder food = new FoodItemHolder();
                            food.setImagepath(items.optString("url"));
                            food.setProductname(items.optString("item_name"));
                            food.setDealid(items.optString("id"));
                            food.setProductdescription(items.optString("item_unit"));
                            food.setPrice(items.optString("item_price"));
                            food.setStorename(items.optString("store_name"));
                            list.add(food);
                        }
                        result.setData(list);
                    }
                    else{
                        result.setMessage("false");


                    }

                } else if (obj.optString("error").equals("true")) {
                    result.setMessage("false");

                    Toast.makeText(activity, "Some error occured", Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);

            }


        }
        return result;
    }

    @Override
    public void onLocationChanged(Location location) {

    }
}
