package bismsoft.bismapp.FastFood;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import bismsoft.bismapp.DrawerActivities.ShopCartActivity;
import bismsoft.bismapp.Http.AsyncTaskListener;
import bismsoft.bismapp.Http.HttpAsyncRequest;
import bismsoft.bismapp.Http.TaskResult;
import bismsoft.bismapp.Parser.BaseParser;
import bismsoft.bismapp.Parser.CheckOutParser;
import bismsoft.bismapp.R;
import bismsoft.bismapp.SuperStore.OrderSubmitActivity;
import bismsoft.bismapp.Url.Constant;
import bismsoft.bismapp.Url.ConstantsforShop;

public class ShopSecondStep    extends AppCompatActivity implements BaseParser {

    private static final String TAG = "RegisterComplainAct";
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int REQUEST_CODE = 123;
    private static final int PLACE_PICKER_REQUEST = 1;
    private Boolean locationPermissionGranted = false;
    private FusedLocationProviderClient fusedLocationProviderClient;

    static Spinner date;
    static Spinner time;
    static EditText comments;
    Button checkout;

    static MyProgressDialog myProgressDialog;
    static ArrayList<String> timestamp;
    static String[] days = {"Today"};
    private static SharedPreferences appSharedPrefs;
    static SharedPreferences preferences;
    static ImageView check;
    TextView notes;
    String note;
    static int charges;
    static int currenttime = 0;
    public static ProgressDialog progressDialog;
    static SharedPreferences.Editor editor;
    public static String time_str, date_str, adress_str;
    private double latitude, longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_second_step);
        myProgressDialog = new MyProgressDialog(ShopSecondStep.this);

        //showGPSDisabledAlertToUser();


        progressDialog = new ProgressDialog(this);
        date = (Spinner) findViewById(R.id.date);
        time = (Spinner) findViewById(R.id.time);
        checkout = (Button) findViewById(R.id.checkout);
        comments = (EditText) findViewById(R.id.comments);
        check = (ImageView) findViewById(R.id.check);
        notes = (TextView) findViewById(R.id.instructions);

        if (getIntent().hasExtra("note")) {
            note = getIntent().getStringExtra("note");
            notes.setText(note);
            charges = Integer.parseInt(getIntent().getStringExtra("charges"));
        }


        Calendar c = Calendar.getInstance();
        System.out.println("Current time =&gt; " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        String formattedDate = df.format(c.getTime());
        String tm[] = formattedDate.split(":");
        String t = tm[0] + tm[1];
        currenttime = Integer.parseInt(t);


        preferences = getSharedPreferences("EStore", Context.MODE_PRIVATE);
        editor = preferences.edit();

        editor.putFloat("longitude", 0);
        editor.putFloat("latitude", 0);


        getTimeStamp();
        date.setAdapter(new ArrayAdapter<String>(this, R.layout.row_area, R.id.text, days));


        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean check = true;
                if (time.getSelectedItemPosition() == 0) {
                    check = false;
                    Toast.makeText(ShopSecondStep.this, "Please Select Time", Toast.LENGTH_SHORT).show();
                }
                if (date.getSelectedItemPosition() == 1) {
                    check = false;
                    Toast.makeText(ShopSecondStep.this, "Please Select Date", Toast.LENGTH_SHORT).show();
                }
                if (check) {
                    getLocationPermission();
//                    myProgressDialog.show();
//                    myProgressDialog.setCanceledOnTouchOutside(false);
//                    checkoutCart();
                     checkoutCart1();

                }

            }
        });


    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Goto Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }


    public void checkoutCart() {

        HttpAsyncRequest request = new HttpAsyncRequest(ShopSecondStep.this, ConstantsforShop.checkout_url, HttpAsyncRequest.RequestType.POST, new CheckOutParser(), listenerbill);
        request.addParam("json_array", makeOrdersJson().toString());
        request.addParam("bill", (Constant.totalbill + charges) + "");
        request.addParam("timestamp", timestamp.get(time.getSelectedItemPosition()));
        request.addParam("day", days[date.getSelectedItemPosition()]);
        request.addParam("userid", preferences.getString("userid", ""));
        request.addParam("charges", charges + "");
        request.addParam("shop_name", ShopMenu.shopname);
        request.addParam("latitude", latitude + "");
        request.addParam("longitude", longitude + "");

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c);

        request.addParam("date", formattedDate);
        request.addParam("comments", comments.getText().toString());
        time_str = timestamp.get(time.getSelectedItemPosition());
        date_str = days[date.getSelectedItemPosition()];
        adress_str = (Constant.totalbill + charges) + "";


        request.execute();

    }


    ////////////////////////////////////////////////////////////////////////
    public void checkoutCart1() {


        HttpAsyncRequest request = new HttpAsyncRequest(ShopSecondStep.this, ConstantsforShop.emailapifororder, HttpAsyncRequest.RequestType.POST, new CheckOutParser(), listenerbill);
        request.addParam("json_array", makeOrdersJson().toString());
        request.addParam("bill", (Constant.totalbill + charges) + "");
        request.addParam("timestamp", timestamp.get(time.getSelectedItemPosition()));
        request.addParam("day", days[date.getSelectedItemPosition()]);
        request.addParam("userid", preferences.getString("userid", ""));
        request.addParam("charges", charges + "");
        request.addParam("shop_name", ShopMenu.shopname);
        request.addParam("comments", comments.getText().toString());
        request.execute();

    }

    ////////////////////////////////////////////////////////////////////////

    AsyncTaskListener listenerbill = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            if (result.isSuccess()) {
                try {

                    String rs = result.getMessage();
                    if (rs.equals("0")) {
                        //     Toast.makeText(ShopSecondStep.this, "success", Toast.LENGTH_SHORT).show();
                        check.setBackgroundResource(R.drawable.checkoutthird);
                        empty();
                        myProgressDialog.dismiss();
                        Toast.makeText(ShopSecondStep.this, "Order has been submitted successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ShopSecondStep.this, OrderSubmitActivity.class);
                        intent.putExtra("bill", adress_str);
                        intent.putExtra("time", time_str);
                        intent.putExtra("day", date_str);
                        intent.putExtra("a", "f");
                        startActivity(intent);


                        finish();

                        if (ShopCartActivity.activity != null)
                            ShopCartActivity.activity.finish();

                    }


                } catch (Exception e) {
                }

            } else {
                // myProgressDialog.dismiss();
                check.setBackgroundResource(R.drawable.checkoutthird);
                empty();
                myProgressDialog.dismiss();
                Toast.makeText(ShopSecondStep.this, "Order has been submitted successfully", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ShopSecondStep.this, OrderSubmitActivity.class);
                intent.putExtra("bill", adress_str);
                intent.putExtra("time", time_str);
                intent.putExtra("day", date_str);
                intent.putExtra("a", "f");
                startActivity(intent);


                finish();

                if (ShopCartActivity.activity != null)
                    ShopCartActivity.activity.finish();

                // Toast.makeText(ShopSecondStep.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();


            }}


    }

            ;

    private void getTimeStamp() {

        HttpAsyncRequest request = new HttpAsyncRequest(this, ConstantsforShop.get_time_stamp, HttpAsyncRequest.RequestType.POST, this, listener);
        request.addParam("shop_name", ShopMenu.shopname);

        request.execute();
        myProgressDialog = new MyProgressDialog(ShopSecondStep.this);
        myProgressDialog.show();
        myProgressDialog.setCanceledOnTouchOutside(false);


    }


    AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            if (result.isSuccess()) {
                try {

                    myProgressDialog.dismiss();
                } catch (Exception e) {
                }


            } else {

                Toast.makeText(ShopSecondStep.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();
                myProgressDialog.dismiss();

            }
        }


    };

    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {

            try {
                result.success(true);
                JSONObject obj = new JSONObject(response);
                if (obj.optString("error").equals("0")) {
                    JSONArray array = obj.optJSONArray("shops_timestamp");
                    if (array != null) {

                        timestamp = new ArrayList<>();
                        timestamp.add(0, "Select Time");

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject time = array.getJSONObject(i);

                            timestamp.add(time.optString("timestamp"));

                        }


                        //timestamp[i + 1] = time.optString("start");


                        time.setAdapter(new ArrayAdapter<String>(this, R.layout.row_area, R.id.text, timestamp));
                    } else
                        Toast.makeText(this, "Time Stamp Not Availiable", Toast.LENGTH_SHORT).show();


                }
            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);

            }


        }
        return result;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    private JSONArray makeOrdersJson() {
        ArrayList<BillModel> arrayList = null;
        appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(ShopSecondStep.this);
        String json = appSharedPrefs.getString("shopcart", "");
        Gson gson = new Gson();
        final Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);


        JSONArray jsonArray = new JSONArray();

        for (int i = 0; i < arrayList.size(); i++) {
            JSONObject obj = new JSONObject();

            try {
                obj.put("price", arrayList.get(i).getProductprice());
                obj.put("quantity", arrayList.get(i).getQuantity());
                obj.put("id", arrayList.get(i).getFooditem_id());
                obj.put("url", arrayList.get(i).getImagepath());
                obj.put("name", arrayList.get(i).getName());
                obj.put("unit", arrayList.get(i).getUnit());


                jsonArray.put(obj);

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }


        return jsonArray;


    }

    public static boolean empty() {
        ArrayList<BillModel> arrayList = null;

        Gson gson = new Gson();


        arrayList = new ArrayList<>();


        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("shopcart", jsons);
        prefsEditor.remove("shopcart");
        prefsEditor.commit();
        return true;
        // Toast.makeText(context, "item added", Toast.LENGTH_SHORT).show();


    }

    public void getLocationPermission() {
        Log.d(TAG, "getLocationPermission : getting location permission");
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

        if (ActivityCompat.checkSelfPermission(this.getApplicationContext(), FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.checkSelfPermission(this.getApplicationContext(), COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationPermissionGranted = true;
                myProgressDialog.show();
                myProgressDialog.setCanceledOnTouchOutside(false);
                getDeviceLocation();
            } else {
                ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE);
            }
        } else {
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionResult : Called");
        locationPermissionGranted = false;
        switch (requestCode) {
            case REQUEST_CODE:
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            Log.d(TAG, "onRequestPermissionResult : permission not granted");
                            locationPermissionGranted = false;
//                            checkoutCart();
//                            checkoutCart1();
                            break;

                        }
                    }
                    Log.d(TAG, "onRequestPermissionResult : Permission granted");
                    locationPermissionGranted = true;
                    myProgressDialog.show();
                    myProgressDialog.setCanceledOnTouchOutside(false);
                    // initialize the map here
                    getDeviceLocation();
                }

        }

    }

    public void locationAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());


        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            String address23 = addresses.get(0).getAddressLine(0);

        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Exception:" + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void getDeviceLocation() {
        Log.d(TAG, "getDeviceLocation : getting device location");
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        final String address;

        try {
            if (locationPermissionGranted) {
                Task location = fusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            Log.d(TAG, "onComplete : found location ");
                            Location currentlocation = (Location) task.getResult();

                            latitude = currentlocation.getLatitude();
                            longitude = currentlocation.getLongitude();
                            checkoutCart();
                            // checkoutCart1();
                            locationAddress(currentlocation.getLatitude(), currentlocation.getLongitude());


                        } else {
                            Log.d(TAG, "onComplete : location not found");
                            //  Toast.makeText(ShopSecondStep.this, "unable to get current location", Toast.LENGTH_SHORT).show();
                            checkoutCart();
                            // checkoutCart1();
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Toast.makeText(this, "Security Exception " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
