package bismsoft.bismapp.Url;

public class ConstantsforShop {

    public  static  String baseUrl = "http://www.thebhakkar.com/superstore/fastfoodwebservices/";

    public  static  String  getShops = baseUrl +"showShops.php";

    public static String getShopMenu = baseUrl+"showShopsMenu.php";
    public static String getProducts  = baseUrl +"showFastFoodProducts.php";
    public static String searchURL = baseUrl+"showSearchProducts.php";
    public static String get_time_stamp = baseUrl+"showTimestamp.php";
    //    public static String checkout_url = baseUrl+"fastFoodOrders.php";
    public static String checkout_url = baseUrl+"fastFoodOrders1.php";
    public static String checkout_url_location = baseUrl+"fastFoodOrdersLocation.php";
    public static String GET_PREVIOUS_HISTORY_URl = baseUrl+"showOrderTrackingToUser.php";

    public static String emailapifororder = "https://servicemanmailapi.000webhostapp.com/emailapi.php";

    public static String emailapiforsuperstoreorder = "https://servicemanmailapi.000webhostapp.com/superstoreemailapi.php";


}
