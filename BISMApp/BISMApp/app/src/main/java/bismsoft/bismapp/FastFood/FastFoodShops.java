package bismsoft.bismapp.FastFood;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Build;
import androidx.annotation.NonNull;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bismsoft.bismapp.Adapters.FastFoodShopsAdopter;
import bismsoft.bismapp.DrawerActivities.LoginPanel;
import bismsoft.bismapp.DrawerActivities.ShopOrderHistory;
import bismsoft.bismapp.Http.AsyncTaskListener;
import bismsoft.bismapp.Http.HttpAsyncRequest;
import bismsoft.bismapp.Http.TaskResult;
import bismsoft.bismapp.Parser.BaseParser;
import bismsoft.bismapp.R;
import bismsoft.bismapp.Url.ConstantsforShop;

public class FastFoodShops extends AppCompatActivity implements BaseParser,NavigationView.OnNavigationItemSelectedListener{

   // ListView shoplist;
    RecyclerView shoplist;
    TextView nomsg;
    private MyProgressDialog myProgressDialog;
    SharedPreferences preferences;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    NavigationView nav;
    SharedPreferences.Editor editor;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fast_food_shops);

       // shoplist = (ListView)findViewById(R.id.list);

        shoplist  = findViewById(R.id.list);
        shoplist.setHasFixedSize(true);
        shoplist.setLayoutManager(new GridLayoutManager(this, 2));
        nomsg = (TextView)findViewById(R.id.nomsg);
        preferences = getSharedPreferences("EStore", Context.MODE_PRIVATE);
        editor = preferences.edit();
        nav = (NavigationView)findViewById(R.id.navview);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(drawerToggle);
        nav.setNavigationItemSelectedListener(this);

        drawerToggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        get_Shops();




    }

    private void get_Shops() {
        HttpAsyncRequest requests = new HttpAsyncRequest(this, ConstantsforShop.getShops, HttpAsyncRequest.RequestType.POST, this, listenerforcategories);



        requests.execute();
        myProgressDialog = new MyProgressDialog(FastFoodShops.this);
        myProgressDialog.show();
        myProgressDialog.setCanceledOnTouchOutside(false);
    }

    AsyncTaskListener listenerforcategories = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            try{

               myProgressDialog.dismiss();
            }catch ( Exception e){}

            if (result.isSuccess()) {





            } else {

                //  Toast.makeText(SplashScreen.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(FastFoodShops.this, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(FastFoodShops.this);
                }
                builder.setTitle("Warning")
                        .setMessage("Error Communicating with the Server")
                        .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                get_Shops();

                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();



            }
        }


    };
    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {

            try {
                result.success(true);
                JSONObject obj = new JSONObject(response);
                if (obj.optString("error").equals("0")) {
                    result.setMessage("true");

                    JSONArray array = obj.optJSONArray("shops");
                    ArrayList<ShopModel> list = new ArrayList<>(array.length());
                    if (array.length() != 0 && array != null) {
                        shoplist.setVisibility(View.VISIBLE);
                        nomsg.setVisibility(View.GONE);

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject items = array.getJSONObject(i);
                            ShopModel food = new ShopModel();
                            food.setShop_name_in_english(items.optString("shop_name_in_english"));
                            food.setShop_image(items.optString("shop_image"));
                            food.setId(items.optString("id"));
                            food.setShop_name_in_urdu(items.optString("shop_name_in_urdu"));
                            food.setShop_address(items.optString("shop_address"));
                            food.setDelivery_charges(items.optString("delivery_charges"));
                            food.setStatus(items.optString("status"));
                            food.setMin_shopping(items.optString("min_shopping"));
                            food.setTimestamp(items.optString("timestamp"));



                            list.add(food);
                        }

                        shoplist.setAdapter(new FastFoodShopsAdopter(FastFoodShops.this,list));

                        result.setData(list);
                    }
                    else
                    {

                        shoplist.setVisibility(View.GONE);
                        nomsg.setVisibility(View.VISIBLE);
                        // result.setMessage("false");

                    }

                } else if (obj.optString("error").equals("1")) {
                    result.setMessage("false");
                }

            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);

            }


        }
        return result;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {

            case R.id.history:
                if(!preferences.getString("userid","").equals(""))
                    startActivity(new Intent(FastFoodShops.this, ShopOrderHistory.class));
                else
                    startActivity(new Intent(getApplicationContext(), LoginPanel.class));
                drawerLayout.closeDrawer(GravityCompat.START);


                return true;



        }





        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(drawerToggle.onOptionsItemSelected(item))
            return  true;


        return super.onOptionsItemSelected(item);
    }


}
