package bismsoft.bismapp.FastFood;

import java.util.ArrayList;

public class FoodItemHolder {
    public String size, price;
    boolean issizeexsist;
    ArrayList<SizeObject> sizes;
    String imagepath;
    String productname;
    String productdescription;
    String productprice;
    String dealid;
    String quantity,favouriate_id;
    String orderexsistl;
    String storename;
    String underlineprice;
    int isfavouriate;
    String purchaseprice;

    public String getPurchaseprice() {

        return purchaseprice;
    }

    public void setPurchaseprice(String purchaseprice) {
        this.purchaseprice = purchaseprice;
    }

    public int getIsfavouriate() {
        return isfavouriate;
    }

    public void setIsfavouriate(int isfavouriate) {
        this.isfavouriate = isfavouriate;
    }

    public String getUnderlineprice() {
        return underlineprice;
    }

    public void setUnderlineprice(String underlineprice) {
        this.underlineprice = underlineprice;
    }

    public String getStorename() {
        return storename;
    }

    public void setStorename(String storename) {
        this.storename = storename;
    }

    public String getOrderexsistl() {
        return orderexsistl;
    }

    public void setOrderexsistl(String orderexsistl) {
        this.orderexsistl = orderexsistl;
    }

    public String getFavouriate_id() {
        return favouriate_id;
    }

    public void setFavouriate_id(String favouriate_id) {
        this.favouriate_id = favouriate_id;
    }

    public String getFavouriate() {
        return favouriate;
    }

    public void setFavouriate(String favouriate) {
        this.favouriate = favouriate;
    }

    String favouriate;
    public ArrayList<SizeObject> getSizes() {
        return sizes;
    }

    public void setSizes(ArrayList<SizeObject> sizes) {
        this.sizes = sizes;
    }

    public boolean issizeexsist() {
        return issizeexsist;
    }

    public void setIssizeexsist(boolean issizeexsist) {
        this.issizeexsist = issizeexsist;
    }


    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }



    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getProductdescription() {
        return productdescription;
    }

    public void setProductdescription(String productdescription) {
        this.productdescription = productdescription;
    }

    public String getProductprice() {
        return productprice;
    }

    public void setProductprice(String productprice) {
        this.productprice = productprice;
    }

    public String getDealid() {
        return dealid;
    }

    public void setDealid(String dealid) {
        this.dealid = dealid;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
