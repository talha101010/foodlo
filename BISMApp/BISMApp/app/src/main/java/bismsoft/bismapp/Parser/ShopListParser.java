package bismsoft.bismapp.Parser;

import android.location.Location;
import android.util.Log;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bismsoft.bismapp.DataProviders.ShopsHolder;
import bismsoft.bismapp.Http.TaskResult;

/**
 * Created by Hamza on 2/2/18.
 */

public class ShopListParser  implements BaseParser {
    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {
            result.success(true);
            try {
                JSONObject obj = new JSONObject(response);
                if(obj.optString("error").equals("false"))
                {

                    JSONArray array =  obj.optJSONArray("all_info");
                    ArrayList<ShopsHolder> list;
                    if(array!= null)
                    {
                        result.setMessage("true");
                        list = new ArrayList<>(array.length());
                        for(int i = 0; i<array.length();i++)
                        {
                            JSONObject rs =    array.getJSONObject(i);

                            ShopsHolder holder = new ShopsHolder();
                            holder.setShopid(rs.optString("id"));
                            holder.setNameinenglish(rs.optString("name_in_english"));
                            holder.setNameinurdu(rs.optString("name_in_urdu"));
                            list.add(holder);





                        }
                        result.setData(list);


                    }
                    else{
                        result.setMessage("false");
                    }


                }

                result.success(true);
            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);
            }


        }
        return result;
    }

    @Override
    public void onLocationChanged(Location location) {

    }
}
