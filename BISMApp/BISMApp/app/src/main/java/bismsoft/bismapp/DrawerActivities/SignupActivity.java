package bismsoft.bismapp.DrawerActivities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import bismsoft.bismapp.FastFood.MyProgressDialog;
import bismsoft.bismapp.Http.AsyncTaskListener;
import bismsoft.bismapp.Http.HttpAsyncRequest;
import bismsoft.bismapp.Http.TaskResult;
import bismsoft.bismapp.Parser.BaseParser;
import bismsoft.bismapp.Parser.SignupParser;
import bismsoft.bismapp.Parser.TehsilParser;
import bismsoft.bismapp.R;
import bismsoft.bismapp.SuperStore.CheckoutSecondStep;
import bismsoft.bismapp.Url.Constant;

public class SignupActivity extends AppCompatActivity implements BaseParser {

    EditText name, password, repassword, mblno, address,email;
    Button submit,update,signup_help;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    TextView signin;
    private MyProgressDialog myProgressDialog;
    private String[] areas ,tehsil;
  //  private String[] charges,min_shopping,urgent_charges,delivery_time;
    Spinner areasSpinner;
    boolean updateVersion;
    private SharedPreferences appSharedPrefs;
    Spinner tehsilSpinner;
    EditText couponcode;
    String strEmail;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        update = findViewById(R.id.update_signup);
        signup_help = findViewById(R.id.signup_help);
//        adresstextview = findViewById(R.id.adresstextview);
//        refreltextview = findViewById(R.id.refreltextview);


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        init();
        getTehsil();
        String [] area = {"Select Tehsil First"};
        areasSpinner.setAdapter(  new ArrayAdapter<String>(this, R.layout.row_area,R.id.text, area));
//        pass1 = findViewById(R.id.pass1);
//        pass2 = findViewById(R.id.pass2);


        if(getIntent().hasExtra("update")){
            password.setVisibility(View.GONE);
            repassword .setVisibility(View.GONE);
//            pass1.setVisibility(View.GONE);
//           pass2.setVisibility(View.GONE);
            address.setVisibility(View.GONE);
            mblno.setFocusable(false);
          //  adresstextview.setVisibility(View.GONE);
//            refreltextview.setVisibility(View.GONE);
            updateVersion = true;

            couponcode .setVisibility( View.GONE);
            email.setText((preferences.getString("email","")));
            name.setText(preferences.getString("name",""));


            address.setText(preferences.getString("address",""));

            String mbl = preferences.getString("phone","");


            mblno.setText(mbl);
            submit.setVisibility(View.GONE);
            update.setVisibility(View.VISIBLE);

        }

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SignupActivity.this,LoginPanel.class);
                startActivity(intent);
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean check = true;
                if (name.getText().toString().length() == 0) {
                    check = false;
                    name.setError("Name cannot be blank");

                }

                else   if(tehsilSpinner.getSelectedItemPosition()  == 0)
                {
                    check = false;
                    Toast.makeText(SignupActivity.this, "Please Select Tehsil", Toast.LENGTH_SHORT).show();
                }
                else   if(areasSpinner.getSelectedItemPosition()  == 0)
                {
                    check = false;
                    Toast.makeText(SignupActivity.this, "Please Select Area", Toast.LENGTH_SHORT).show();
                }
                else  if (address.getText().length() == 0) {
                    check = false;
                    address.setError(" Address cannot be blank");

                }


                else {

                    if (email.getText().toString().equals("")){

                        strEmail = "null";
                    }else {

                        strEmail = email.getText().toString();
                    }


                    posttoserver(strEmail);

                }


            }
        });

        signup_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder alertdialog = new AlertDialog.Builder(SignupActivity.this);
                LayoutInflater inflater =getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.chand_aham_masel_dialog,null);
                alertdialog.setView(dialogView);
                alertdialog.setTitle("Help ");

                final AlertDialog dialog = alertdialog.create();
                dialog.show();
                final TextView delete_list_item = (TextView) dialogView.findViewById(R.id.chand_aham_maslay_tv);
                final Button cancel_namaz_dialog_btn = (Button) dialogView.findViewById(R.id.cancel_namaz_dialog);
                final Button help_btn = dialogView.findViewById(R.id.contactus);
                cancel_namaz_dialog_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                help_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel","03006874423",null));
                        startActivity(callIntent);

                    }
                });
            }
        });




        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean check = true;
                if (name.getText().toString().length() == 0) {
                    check = false;
                    name.setError("Name cannot be blank");

                }
                else  if(!updateVersion)
                    if (email.getText().length() == 11) {
                        check = false;
                        email.setError("Email cannot be blank");
                    }

                else  if(!updateVersion)
                    if (password.getText().length() == 0) {
                        check = false;
                        password.setError("Password cannot be blank");
                    }
                if(!updateVersion)
                    if (!password.getText().toString().equals(repassword.getText().toString())) {
                        check = false;
                        password.setError("Password not match");
                    }
                    else if (mblno.getText().length() == 0) {
                        check = false;
                        mblno.setError("Mobile number cannot be blank");

                    }
                    else  if (mblno.getText().length() < 11) {
                        check = false;
                        mblno.setError("Invalid Mobile Number");

                    }

                    else   if(tehsilSpinner.getSelectedItemPosition()  == 0)
                    {
                        check = false;
                        Toast.makeText(SignupActivity.this, "Please Select Tehsil", Toast.LENGTH_SHORT).show();
                    }
                    else   if(areasSpinner.getSelectedItemPosition()  == 0)
                    {
                        check = false;
                        Toast.makeText(SignupActivity.this, "Please Select Area", Toast.LENGTH_SHORT).show();
                    }
                    else  if (address.getText().length() == 0) {
                        check = false;
                        address.setError(" Address cannot be blank");

                    }


                    else {
                        if (email.getText().toString().equals("")){

                           strEmail = "null";
                        }else {

                            strEmail = email.getText().toString();
                        }
                        posttoserver(strEmail);

                    }


            }
        });

    }


    public void getTehsil()

    {

        HttpAsyncRequest request = new HttpAsyncRequest(this, Constant.get_Tehsil_url, HttpAsyncRequest.RequestType.POST, new TehsilParser(), listener_tehsil);


        request.execute();
        myProgressDialog = new MyProgressDialog(SignupActivity.this);
        myProgressDialog.show();
        myProgressDialog.setCanceledOnTouchOutside(false);
    }


    AsyncTaskListener listener_tehsil = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            try {
                myProgressDialog.dismiss();
            }catch (Exception e){}

            if (result.isSuccess()) {
                tehsil = (String[]) result.getData();
                tehsilSpinner.setAdapter(  new ArrayAdapter<String>(SignupActivity.this, R.layout.row_area,R.id.text, tehsil));

                if(updateVersion && tehsil!=null) {
                    for (int i = 0; i < tehsil.length; i++) {
                        if (preferences.getString("tehsil", "").equals(tehsil[i])) {
                            tehsilSpinner.setSelection(i);
                            break;


                        }
                    }
                    getAreas(preferences.getString("tehsil", ""));

                }
                tehsilSpinner.post(new Runnable() {
                    @Override
                    public void run() {
                        tehsilSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if(position!=0)
                                    getAreas(tehsil[position]);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }
                });


            } else {

                Toast.makeText(SignupActivity.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();

            }
        }


    };

    public void posttoserver(String strEmail)

    {
        String url = null;
        if(updateVersion)
            url = Constant.update_profile;
        else
            url = Constant.SIGNUPURL;

        HttpAsyncRequest request = new HttpAsyncRequest(this, url, HttpAsyncRequest.RequestType.POST, new SignupParser(), listener);
//        name,cellnumber,password,area,address
        request.addParam("name",name.getText().toString());
        request.addParam("cellnumber",mblno.getText().toString());
        request.addParam("password",password.getText().toString());
        // request.addParam("area","Bhakkar City");
        // request.addParam("tehsil","Bhakkar");
        request.addParam("area",areas[areasSpinner.getSelectedItemPosition()]);
        request.addParam("tehsil",tehsil[tehsilSpinner.getSelectedItemPosition()]);
        request.addParam("email",strEmail);
        if(!updateVersion && couponcode.getText().length()!=0){
            request.addParam("referral_coupan",couponcode.getText().toString());}
        else{
            request.addParam("referral_coupan","null");}
        request.addParam("address",address.getText().toString());
        if(updateVersion)
            request.addParam("id",preferences.getString("userid",""));
        request.execute();
        myProgressDialog = new MyProgressDialog(SignupActivity.this);
        myProgressDialog.show();
        myProgressDialog.setCanceledOnTouchOutside(false);
    }

    private void init() {
        password = (EditText) findViewById(R.id.password_signup);
        repassword = (EditText) findViewById(R.id.repassword_signup);
        mblno = (EditText) findViewById(R.id.mobileno_signup);
        name = (EditText) findViewById(R.id.name_signup);
        areasSpinner = (Spinner)findViewById(R.id.areaspinner_signup);
        address = (EditText) findViewById(R.id.address_signup);
        submit = (Button) findViewById(R.id.submit_signup);
        signin=findViewById(R.id.signin);
        email=findViewById(R.id.email_signup);
        preferences = getSharedPreferences("EStore", Context.MODE_PRIVATE);
        editor = preferences.edit();
        tehsilSpinner = (Spinner)findViewById(R.id.tehsilspinner_signup);
        couponcode = findViewById(R.id.coupon);

    }

    AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            try {
                myProgressDialog.dismiss();
            }catch (Exception e){}

            if (result.isSuccess()) {

                if (result.getMessage().equals("true")) {

                    if (!updateVersion){

                        Toast.makeText(SignupActivity.this, "Successfully Registered", Toast.LENGTH_SHORT).show();

                        editor.putString("login", "success");
                        editor.putString("userid", (String) result.getData());
                        editor.putString("phone", mblno.getText().toString());
                        editor.putString("name", name.getText().toString());
                        editor.putString("email",email.getText().toString());
                     //   editor.putString("charges",charges[areasSpinner.getSelectedItemPosition()]);
                     //   editor.putString("min",min_shopping[areasSpinner.getSelectedItemPosition()]);
                     //   editor.putString("urgent_charges",urgent_charges[areasSpinner.getSelectedItemPosition()]);
                     //   editor.putString("time",delivery_time[areasSpinner.getSelectedItemPosition()]);
                        // editor.putString("area", "Bhakkar City");
                        editor.putString("area",areas[areasSpinner.getSelectedItemPosition()]);
                        editor.putString("tehsil", tehsil[tehsilSpinner.getSelectedItemPosition()]);
                        editor.putString("address", address.getText().toString());
                        editor .putString("referralcode",couponcode.getText().toString());
                        editor.commit();
                        finish();

                    }
                    else{
                        editor.putString("name", name.getText().toString());
                        editor.putString("area", areas[areasSpinner.getSelectedItemPosition()]);
                        editor.putString("address", address.getText().toString());
                        editor.putString("email",email.getText().toString());
                     //   editor.putString("charges",charges[areasSpinner.getSelectedItemPosition()]);
                     //   editor.putString("min",min_shopping[areasSpinner.getSelectedItemPosition()]);
                     //   editor.putString("urgent_charges",urgent_charges[areasSpinner.getSelectedItemPosition()]);
                     //   editor.putString("time",delivery_time[areasSpinner.getSelectedItemPosition()]);
                        editor.commit();
                        Toast.makeText(SignupActivity.this, "Profile Updated", Toast.LENGTH_SHORT).show();

                    }

                } else if (result.getMessage().equals("false")) {
                    if(!updateVersion) {
                        mblno.setError("Mobile Number Already Registered Please Login");
                        Toast.makeText(SignupActivity.this, "Mobile No. already registered", Toast.LENGTH_SHORT).show();
                    }else{
                        editor.putString("name", name.getText().toString());
                        editor.putString("area",areas[areasSpinner.getSelectedItemPosition()]);
                        editor.putString("address", address.getText().toString());
                        editor.putString("email",email.getText().toString());
                      //  editor.putString("charges",charges[areasSpinner.getSelectedItemPosition()]);
                      //  editor.putString("min",min_shopping[areasSpinner.getSelectedItemPosition()]);
                      //  editor.putString("urgent_charges",urgent_charges[areasSpinner.getSelectedItemPosition()]);
                      //  editor.putString("time",delivery_time[areasSpinner.getSelectedItemPosition()]);
                        editor.commit();
                        Toast.makeText(SignupActivity.this, "Profile Updated", Toast.LENGTH_SHORT).show();

                    }
                }

                else if (result.getMessage().equals("wrong"))
                {

                    couponcode.setError("Wrong Coupon code");
                }


            } else {

                Toast.makeText(SignupActivity.this, "Error", Toast.LENGTH_SHORT).show();

            }
        }


    };

    public void getAreas(String area)

    {

        HttpAsyncRequest request = new HttpAsyncRequest(this, Constant.get_AREAS, HttpAsyncRequest.RequestType.POST, this, listener_areas);
        request.addParam("tehsilname",area);


        request.execute();
        myProgressDialog = new MyProgressDialog(SignupActivity.this);
        myProgressDialog.show();
        myProgressDialog.setCanceledOnTouchOutside(false);
    }


    AsyncTaskListener listener_areas = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            try {
                myProgressDialog.dismiss();
            }catch (Exception e){}

            if (result.isSuccess()) {

            } else {

                Toast.makeText(SignupActivity.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();

            }
        }


    };
    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {
            result.success(true);
            try {
                JSONObject obj = new JSONObject(response);


                if( obj.optString("error").equals("0"))
                {

                    JSONArray areaArray =  obj.optJSONArray("area");
                    if(areaArray!=null)
                        areas= new String[areaArray.length()+1];
//                        charges= new String[areaArray.length()+1];
//                        min_shopping= new String[areaArray.length()+1];
//                        urgent_charges= new String[areaArray.length()+1];
//                        delivery_time= new String[areaArray.length()+1];
                    areas[0]  = "Select Your Area";
//                    charges[0]  = "Select Your charges";
//                    min_shopping[0]  = "Select min-shopping";
//                    urgent_charges[0] = "Select Urgent Charges";
//                    delivery_time[0] = "delivery time";
                    for(int i  = 0 ; i <areaArray.length();i++ )
                    {
                        areas[i+1] = areaArray.optJSONObject(i).optString("area");
                     //   charges[i+1] = areaArray.optJSONObject(i).optString("charges");
                     //   min_shopping[i+1] = areaArray.optJSONObject(i).optString("min");
                     //   urgent_charges[i+1] = areaArray.optJSONObject(i).optString("urgent_charges");
                     //   delivery_time[i+1] = areaArray.optJSONObject(i).optString("time");
                    }
                    areasSpinner.setAdapter(  new ArrayAdapter<String>(this, R.layout.row_area,R.id.text, areas));


                    if(updateVersion)
                    {
                        for(int i = 0 ; i <areas.length;i++)
                        {
                            if(preferences.getString("area","").equals(areas[i]))
                            {
                                areasSpinner.setSelection(i);
                                break;


                            }
                        }


                    }



                }
                else
                if( obj.optString("response").equals("1")){

                    result.setMessage("false");

                }
                result.success(true);
            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);
            }


        }
        return  result;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(updateVersion) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_layout, menu);

            // return true so that the menu pop up is opened
            return true;
        }
        else
            return false;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case R.id.logout:
                myProgressDialog = new MyProgressDialog(SignupActivity.this);
                myProgressDialog.show();
                myProgressDialog.setCanceledOnTouchOutside(false);

                editor.remove("login");
                editor.remove("userid");
                editor.remove("phone");


                editor.remove("name");
                editor.remove("area");
                editor.remove("address");
                editor.remove("tehsil");
//                editor.remove("charges");
//                editor.remove("min");
//                editor.remove("urgent_charges");
//                editor.remove("time");
                editor.commit();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            myProgressDialog.dismiss();
                            finish();




                        }catch (Exception e){}
                    }
                },2000);

                return  true;
            case R.id.changepassword:
                startActivity(new Intent(SignupActivity.this,ForgetPassword.class).putExtra("change",true));
                return true;

        }
        return  false;
    }
}
