package bismsoft.bismapp.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bismsoft.bismapp.FastFood.BillModel;
import bismsoft.bismapp.FastFood.MenuFoodItems;
import bismsoft.bismapp.FastFood.MyProgressDialog;
import bismsoft.bismapp.FastFood.ShopMenu;
import bismsoft.bismapp.FastFood.ShopModel;
import bismsoft.bismapp.Http.AsyncTaskListener;
import bismsoft.bismapp.Http.HttpAsyncRequest;
import bismsoft.bismapp.Http.TaskResult;
import bismsoft.bismapp.Parser.BaseParser;
import bismsoft.bismapp.R;
import bismsoft.bismapp.Url.ConstantsforShop;

public class FastFoodShopsAdopter extends RecyclerView.Adapter<FastFoodShopsAdopter.ViewHolder> implements BaseParser {

    private static Context context;
    private static LayoutInflater inflater;
    private final SharedPreferences preferences;
    ArrayList<ShopModel> shoplist;
    public  static  ArrayList<MenuFoodItems> menulist;
    MyProgressDialog myProgressDialog;
    String selectedname;
    String min_shopping;
    private String charges;
    private SharedPreferences appSharedPrefs;
    private FastFoodShopsAdopter.OnItemClickListener mListener;

    public interface OnItemClickListener{
        void  onItemClick(int position);
        void  onButtonChange(int position);
    }

    public void setOnItemClickListener(FastFoodShopsAdopter.OnItemClickListener listener){
        mListener = listener;
    }





    public FastFoodShopsAdopter(Context mainActivity, ArrayList<ShopModel> listitems) {
        // TODO Auto-generated constructor stub
        context = mainActivity;
        this.shoplist = listitems;
        setHasStableIds(true);
        preferences = context.getSharedPreferences("EStore", Context.MODE_PRIVATE);
        appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);

        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_shops,viewGroup,false);
        FastFoodShopsAdopter.ViewHolder viewHolder = new FastFoodShopsAdopter.ViewHolder(view,mListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder,final int position) {

        final ShopModel shopModel = shoplist.get(position);
        TextView shopnameeng = holder.shopnameeng;
        shopnameeng.setText(shopModel.getShop_name_in_english());
        TextView shopnameurdu= holder.shopnameurdu;
        shopnameurdu.setText(shopModel.getShop_name_in_urdu());
        TextView time = holder.time;

        // holder.shopnameeng.setText(shoplist.get(position).getShop_name_in_english());

        // holder.shopnameurdu.setText(shoplist.get(position).getShop_name_in_urdu());

//        if(shoplist.get(position).getStatus().toLowerCase().equals("close"))
//            holder.time.setText(shoplist.get(position).getStatus()+"d") ;

        if(shopModel.getStatus().toLowerCase().equals("close")){
            time.setVisibility(View.VISIBLE);
            time.setText(shopModel.getStatus()+"d for\nOnline order") ;
        }

        TextView address = holder.address;

        // holder.address.setText(shoplist.get(position).getShop_address());

        address.setText(shopModel.getShop_address());

//        Glide.with(context)
//                .load(shoplist.get(position).getShop_image())
//                .dontAnimate()
//                .placeholder(R.drawable.logoone)
//
//
//                .listener(new RequestListener<String, GlideDrawable>() {
//                    @Override
//                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//
//                        return false;
//                    }
//                })
//                .into(holder.shopimage);

        ImageView shopimage = holder.shopimage;
        final RelativeLayout layout =holder.layout;
        Glide.with(context)
                .load(shopModel.getShop_image())
                .dontAnimate()
                .placeholder(R.drawable.logoone)


                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

                        return false;
                    }
                })
                .fitCenter()
//                .into(shopimage);
                .into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            layout.setBackground(resource);
                        }

                    }
                });

        // RelativeLayout layout = holder.layout;

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!shopModel.getStatus().toLowerCase().equals("close")) {

                    if (!preferences.getString("shop", "").equals("")) {
                        if (preferences.getString("shop", "").equals(shopModel.getShop_name_in_english())) {
                            selectedname = shopModel.getShop_name_in_english();
                            min_shopping = shopModel.getMin_shopping();
                            charges = shopModel.getDelivery_charges();
                            get_Shops();
                        } else {


                            if(appSharedPrefs.getString("shopcart","").equals(""))
                            {
                                selectedname = shopModel.getShop_name_in_english();
                                min_shopping = shopModel.getMin_shopping();
                                charges = shopModel.getDelivery_charges();
                                get_Shops();
                            }
                            else {


                                AlertDialog.Builder builder;
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
                                } else {
                                    builder = new AlertDialog.Builder(context);
                                }
                                builder.setTitle("Warning")
                                        .setMessage("As you are going to new shop your previous cart will be cleared")
                                        .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // continue with delete

                                                empty();
                                                selectedname = shopModel.getShop_name_in_english();
                                                min_shopping = shopModel.getMin_shopping();
                                                charges = shopModel.getDelivery_charges();
                                                get_Shops();
                                            }
                                        })

                                        .setNegativeButton("Cancel", null)
                                        .setIcon(android.R.drawable.ic_dialog_alert)

                                        .show();
                            }


                        }
                    }
                    else{
                        selectedname = shopModel.getShop_name_in_english();
                        min_shopping = shopModel.getMin_shopping();
                        charges = shopModel.getDelivery_charges();
                        get_Shops();
                    }
                }
                else{


                    final AlertDialog.Builder alertdialog = new AlertDialog.Builder(context);
                    LayoutInflater inflater =LayoutInflater.from(context);
                    final View dialogView = inflater.inflate(R.layout.shop_close_dialog,null);
                    final ImageButton helplineBtn= dialogView.findViewById(R.id.helplineCallBtn);
                    final  ImageButton helplineBtn2 = dialogView.findViewById(R.id.helplineCallBtn2);
                    alertdialog.setView(dialogView);


                    final AlertDialog dialog = alertdialog.create();
                    dialog.show();

                    helplineBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel","03338995373",null));
                            context.startActivity(callIntent);
                        }
                    });

                    helplineBtn2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel","03331670333",null));
                            context.startActivity(callIntent);

                        }
                    });

                }
            }
        });


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return shoplist.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView shopnameeng,shopnameurdu,address,time;
        ImageView shopimage;
        RelativeLayout layout;




        public ViewHolder(View v, final FastFoodShopsAdopter.OnItemClickListener listener) {
            super(v);

            shopimage = (ImageView)v.findViewById(R.id.shop_image);
            shopnameeng = (TextView)v.findViewById(R.id.shopnameng);
            shopnameurdu = (TextView)v.findViewById(R.id.shopnameurdu);
            address = (TextView)v.findViewById(R.id.shopaddress);
            time = (TextView)v.findViewById(R.id.shoptime);
            layout = (RelativeLayout)v.findViewById(R.id.row);



//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    if (listener!=null){
//
//                        int position  = getAdapterPosition();
//                        if (position!=RecyclerView.NO_POSITION){
//                            listener.onItemClick(position);
//                        }
//                    }
//
//                }
//            });
        }
    }

    private boolean empty() {
        ArrayList<BillModel> arrayList = null;

        Gson gson = new Gson();




        arrayList = new ArrayList<>();


        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("shopcart", jsons);
        prefsEditor.remove("shopcart");
        prefsEditor.commit();
        return true;
        // Toast.makeText(context, "item added", Toast.LENGTH_SHORT).show();


    }

    private void get_Shops() {
        HttpAsyncRequest requests = new HttpAsyncRequest(context, ConstantsforShop.getShopMenu, HttpAsyncRequest.RequestType.POST, this, listenerforcategories);

        requests.addParam("shop_name",selectedname);

        requests.execute();
        myProgressDialog = new MyProgressDialog(context);
        myProgressDialog.show();
        myProgressDialog.setCanceledOnTouchOutside(false);
    }
    AsyncTaskListener listenerforcategories = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            try{

                myProgressDialog.dismiss();
            }catch ( Exception e){}

            if (result.isSuccess()) {





            } else {

                //  Toast.makeText(SplashScreen.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(context);
                }
                builder.setTitle("Warning")
                        .setMessage("Error Communicating with the Server")
                        .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                get_Shops();

                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();



            }
        }


    };
    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {

            try {
                result.success(true);
                JSONObject obj = new JSONObject(response);
                if (obj.optString("error").equals("0")) {
                    result.setMessage("true");

                    JSONArray array = obj.optJSONArray("shops_menu");
                    menulist = new ArrayList<>(array.length());

                    if (array.length() != 0 && array!=null) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject items = array.getJSONObject(i);
                            MenuFoodItems food = new MenuFoodItems();
                            food.setDealid(items.optString("id"));
                            food.setProductname(items.optString("menu_name"));

                            menulist.add(food);
                        }

                        context.startActivity(new Intent(context, ShopMenu.class)
                                .putExtra("shopname",selectedname)
                                .putExtra("min",min_shopping)
                                .putExtra("charges",charges)


                        );
                        //result.setData(list);
                    }
                    else{
                        result.setMessage("false");
                        Toast.makeText(context, "This Shop has No Menu yet", Toast.LENGTH_SHORT).show();

                    }

                } else if (obj.optString("error").equals("true")) {
                    result.setMessage("false");
                    Toast.makeText(context, "This Shop has No Menu yet", Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);

            }


        }
        return result;
    }

    @Override
    public void onLocationChanged(Location location) {

    }
}






