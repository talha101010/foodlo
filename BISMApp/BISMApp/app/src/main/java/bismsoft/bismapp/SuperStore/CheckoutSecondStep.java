package bismsoft.bismapp.SuperStore;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import bismsoft.bismapp.DrawerActivities.SignupActivity;
import bismsoft.bismapp.FastFood.BillModel;
import bismsoft.bismapp.FastFood.MyProgressDialog;
import bismsoft.bismapp.Http.AsyncTaskListener;
import bismsoft.bismapp.Http.HttpAsyncRequest;
import bismsoft.bismapp.Http.TaskResult;
import bismsoft.bismapp.Parser.BaseParser;
import bismsoft.bismapp.Parser.CheckOutParser;
import bismsoft.bismapp.Parser.TehsilParser;
import bismsoft.bismapp.R;
import bismsoft.bismapp.Url.Constant;
import bismsoft.bismapp.Url.ConstantsforShop;

public class CheckoutSecondStep extends AppCompatActivity implements BaseParser, LocationListener {

    private static final String TAG = "RegisterComplainAct";
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int REQUEST_CODE = 123;
    private static final int PLACE_PICKER_REQUEST = 1;
    private Boolean locationPermissionGranted = false;
    private FusedLocationProviderClient fusedLocationProviderClient;
    static Spinner date;
    static Spinner time;
    static EditText comments;
    Button checkout,changearea;
    public static ProgressDialog progressDialog;
    private String day1, time1;
    private GoogleMap mMap;

    static MyProgressDialog myProgressDialog;
    static ArrayList<String> timestamp;
    static String[] days = {"Today", "Tomorrow"};
    private static SharedPreferences appSharedPrefs;
    static SharedPreferences preferences;
    static ImageView check;
    TextView notes;
    String note, dType;
    //static int charges;
    static int currenttime = 0;
    static SharedPreferences.Editor editor;

    public static String time_str, date_str, adress_str, urgentNote;
    private double latitude, longitude;

    static Intent intent;
    RelativeLayout dayLayout, timeLayout;
    TextView urgentTv,tehsil1,area1;
    LocationManager locationManager;

    private String[] areas ,tehsil,chargesindialog,min_shopping,urgent_charges,delivery_time;
    private Spinner tehsilSpinner,areaSpinner;
    private ProgressBar progressBar1;
    private String newTime1,newTehsil,newArea,newCharges,newMin,new_urgentcharges,newTime,finalTime;
    private SimpleDateFormat df;
    private boolean checktimestamp = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_second_step);
        checktimestamp = false;
        myProgressDialog = new MyProgressDialog(CheckoutSecondStep.this);
        date = (Spinner) findViewById(R.id.date);
        time = (Spinner) findViewById(R.id.time);
        tehsil1 = findViewById(R.id.tehsil1);
        area1 = findViewById(R.id.area1);
        checkout = (Button) findViewById(R.id.checkout);
        changearea = findViewById(R.id.changearea);
        comments = (EditText) findViewById(R.id.comments);
        check = (ImageView) findViewById(R.id.check);
        notes = (TextView) findViewById(R.id.instructions);
        progressDialog = new ProgressDialog(this);
        dayLayout = findViewById(R.id.daylayout);
        timeLayout = findViewById(R.id.timeLayout);
        urgentTv = findViewById(R.id.urgentTv);

        preferences = getSharedPreferences("EStore", Context.MODE_PRIVATE);
        editor = preferences.edit();

        //getTimeStamp();
        date.setAdapter(new ArrayAdapter<String>(this, R.layout.row_area, R.id.text, days));
        newTehsil = preferences.getString("tehsil","");
        newArea = preferences.getString("area","");
        getAreaDetails();



      //showGPSDisabledAlertToUser();

        if(getIntent().hasExtra("note"))
        {
//            note = getIntent().getStringExtra("note");
//            notes.setText(note);
            //charges = Integer.parseInt(getIntent().getStringExtra("charges"));

        }

        Intent intent1 = getIntent();
        dType = intent1.getStringExtra("ctype");
        urgentNote= intent1.getStringExtra("notesdelivery");

        Calendar c = Calendar.getInstance();
        System.out.println("Current time =&gt; "+c.getTime());
        df = new SimpleDateFormat("HH:mm");
        String formattedDate = df.format(c.getTime());
        newTime1 = formattedDate;
        String tm[] = formattedDate.split(":");
        String t = tm[0]+tm[1];
        currenttime = Integer.parseInt(t);

        date.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0){
                    if (timestamp != null){
                        timestamp.clear();
                    }
                    getTimeStamp();
                    }else{
                    if (checktimestamp){
                        getTimeStamp1();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        tehsil1.setText(newTehsil);
        area1.setText(newArea);




        if (dType.equals("r")){

            timeLayout.setVisibility(View.VISIBLE);
            dayLayout.setVisibility(View.VISIBLE);
            notes.setVisibility(View.VISIBLE);
        }else if (dType.equals("u")){

            timeLayout.setVisibility(View.GONE);
            dayLayout.setVisibility(View.GONE);
            notes.setVisibility(View.GONE);
            urgentTv.setVisibility(View.VISIBLE);


        }


        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean check = true;

                if (dType.equals("r")) {
//                    if (time.getSelectedItemPosition() == 0) {
//                        check = false;
//                        Toast.makeText(CheckoutSecondStep.this, "Please Select Time", Toast.LENGTH_SHORT).show();
//                    }
//                    if (date.getSelectedItemPosition() == 0) {
//                        check = false;
//                        Toast.makeText(CheckoutSecondStep.this, "Please Select Date", Toast.LENGTH_SHORT).show();
//                    }
                    if (check) {
                        time1 = timestamp.get(time.getSelectedItemPosition());
                        day1 = days[date.getSelectedItemPosition()];
                        getLocationPermission(time1,day1 );
//                        myProgressDialog.setCanceledOnTouchOutside(false);
//                        myProgressDialog.show();
//                        checkoutCart(timestamp.get(time.getSelectedItemPosition()), days[date.getSelectedItemPosition()]);


                    }

                }else if (dType.equals("u")){

                    if (check) {
//                        myProgressDialog.setCanceledOnTouchOutside(false);
//                        myProgressDialog.show();
                        time1 = "Urgent";
                        day1 = "Today";
                        getLocationPermission(time1,day1 );
                       // checkoutCart(time1, day1);


                    }



                }

            }
        });

        changearea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 openDialog();
            }
        });


        //its for current locatino latilute and longitude

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);


    }

    private void getAreaDetails(){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wiat...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.get_area_details_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray =  jsonObject.getJSONArray("areadetails");
                            if (jsonArray != null){
                                for(int i  = 0 ; i <jsonArray.length();i++ )
                                {
                                    JSONObject json = jsonArray.getJSONObject(i);
                                    newCharges = json.getString("charges");
                                    newMin = json.getString("min");
                                    new_urgentcharges = json.getString("urgent_charges");
                                    newTime = json.getString("time");
                                }
                                notes.setText("Delivery Charges :"+newCharges+" With Shopping less than :"+newMin+" Otherwise delivery is free");
                                urgentTv.setText("Minimum time for urgent delivery is 40 to 50 minutes and Charges for urgent delivery is Rs:"+new_urgentcharges);
                                try {
                                    Date d = df.parse(newTime1);
                                    Calendar cal = Calendar.getInstance();
                                    cal.setTime(d);
                                    cal.add(Calendar.MINUTE, Integer.parseInt(newTime));
                                    finalTime = df.format(cal.getTime());
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                    Toast.makeText(CheckoutSecondStep.this, e.getLocalizedMessage()+"", Toast.LENGTH_SHORT).show();
                                }
                                  getTimeStamp1();
                                checktimestamp = true;
                            }else{
                                Toast.makeText(CheckoutSecondStep.this, "array is null", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(CheckoutSecondStep.this, e.getLocalizedMessage()+"", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(CheckoutSecondStep.this, "Communication Server Error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("tehsilname",newTehsil);
                param.put("areaname",newArea);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void openDialog(){
        final Dialog dialog = new Dialog(CheckoutSecondStep.this,R.style.Theme_AppCompat_Light_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.show_dialog);
         tehsilSpinner = dialog.findViewById(R.id.tehsilspinner);
         areaSpinner = dialog.findViewById(R.id.areaspinner);
         progressBar1 = dialog.findViewById(R.id.progressbar1);
        Button dialogButton =  dialog.findViewById(R.id.closebtn);
        Button proceddbtn = dialog.findViewById(R.id.proceddbtn);
        getTehsil();
        //sessionAdapter = new ArrayAdapter<String>(CheckoutSecondStep.this,android.R.layout.simple_spinner_dropdown_item,sessionName);
       // getSessionFromFirebase(progressBar1,sessionspinner);
        proceddbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tehsilSpinner.getSelectedItemPosition()==0 || areaSpinner.getSelectedItemPosition()==0){
                    Toast.makeText(CheckoutSecondStep.this, "please select Tehsil/Area", Toast.LENGTH_SHORT).show();
                }else{
                    newTehsil = tehsil[tehsilSpinner.getSelectedItemPosition()];
                    newArea = areas[areaSpinner.getSelectedItemPosition()];
                    newCharges = chargesindialog[areaSpinner.getSelectedItemPosition()];
                    newMin = min_shopping[areaSpinner.getSelectedItemPosition()];
                    new_urgentcharges = urgent_charges[areaSpinner.getSelectedItemPosition()];
                    newTime = delivery_time[areaSpinner.getSelectedItemPosition()];
                    finalTime = newTime;
                    tehsil1.setText(newTehsil);
                    area1.setText(newArea);
                    notes.setText("Delivery Charges :"+newCharges+" With Shopping less than :"+newMin+" Otherwise delivery is free");
                    urgentTv.setText("Minimum time for urgent delivery is 40 to 50 minutes and Charges for urgent deliver is Rs:"+new_urgentcharges);
                    try {
                        Calendar c = Calendar.getInstance();
                        System.out.println("Current time =&gt; "+c.getTime());
                        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                        String formattedDate = df.format(c.getTime());
                        String newTime1 = formattedDate;
                        Date d = df.parse(newTime1);
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(d);
                        cal.add(Calendar.MINUTE, Integer.parseInt(newTime));
                        finalTime = df.format(cal.getTime());

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (timestamp != null){
                        timestamp.clear();
                    }
                    timestamp = new ArrayList<>();
                    timestamp.add(  0,"Expected:"+ finalTime);

                    for (int i =0 ;i<=3;i++){
                        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                        try {
                            Date d = df.parse(finalTime);
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(d);
                            cal.add(Calendar.MINUTE, 60);
                            String finalTime1 = df.format(cal.getTime());
                            timestamp.add(i+1,finalTime+ " TO "+ finalTime1);
                            finalTime = finalTime1;
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }

                    time.setAdapter(new ArrayAdapter<String>(CheckoutSecondStep.this, R.layout.row_area, R.id.text, timestamp));

                    dialog.dismiss();
                }

            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public void getTehsil()

    {

        HttpAsyncRequest request = new HttpAsyncRequest(this, Constant.get_Tehsil_url, HttpAsyncRequest.RequestType.POST, new TehsilParser(), listener_tehsil);


        request.execute();
        myProgressDialog = new MyProgressDialog(CheckoutSecondStep.this);
        myProgressDialog.show();
        myProgressDialog.setCanceledOnTouchOutside(false);
    }


    AsyncTaskListener listener_tehsil = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            try {
                myProgressDialog.dismiss();
            }catch (Exception e){}

            if (result.isSuccess()) {
                tehsil = (String[]) result.getData();
                tehsilSpinner.setAdapter(  new ArrayAdapter<String>(CheckoutSecondStep.this, R.layout.row_area,R.id.text, tehsil));

                if(tehsil!=null) {
                    for (int i = 0; i < tehsil.length; i++) {
                        if (preferences.getString("tehsil", "").equals(tehsil[i])) {
                            tehsilSpinner.setSelection(i);
                            break;


                        }
                    }
                    getAreas(preferences.getString("tehsil", ""));

                }
                tehsilSpinner.post(new Runnable() {
                    @Override
                    public void run() {
                        tehsilSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if(position!=0)
                                    getAreas(tehsil[position]);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }
                });


            } else {

                Toast.makeText(CheckoutSecondStep.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();

            }
        }


    };

    public void getAreas(final String area)

    {
        progressBar1.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.get_AREAS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progressBar1.setVisibility(View.GONE);
                            JSONObject obj = new JSONObject(response);
                            JSONArray areaArray =  obj.optJSONArray("area");
                            if(areaArray!=null)
                                areas= new String[areaArray.length()+1];
                            chargesindialog= new String[areaArray.length()+1];
                            min_shopping= new String[areaArray.length()+1];
                            urgent_charges= new String[areaArray.length()+1];
                            delivery_time= new String[areaArray.length()+1];
                            areas[0]  = "Select Your Area";
                            chargesindialog[0]  = "Select Your charges";
                            min_shopping[0]  = "Select min-shopping";
                            urgent_charges[0]  = "Select urgent charges";
                            delivery_time[0]  = "Select delivery time";
                            for(int i  = 0 ; i <areaArray.length();i++ )
                            {
                                areas[i+1] = areaArray.optJSONObject(i).optString("area");
                                chargesindialog[i+1] = areaArray.optJSONObject(i).optString("charges");
                                min_shopping[i+1] = areaArray.optJSONObject(i).optString("min");
                                urgent_charges[i+1] = areaArray.optJSONObject(i).optString("urgent_charges");
                                delivery_time[i+1] = areaArray.optJSONObject(i).optString("time");
                            }
                            areaSpinner.setAdapter(  new ArrayAdapter<String>(CheckoutSecondStep.this, R.layout.row_area,R.id.text, areas));


//                    if(updateVersion)
//                    {
                            for(int i = 0 ; i <areas.length;i++)
                            {
                                if(preferences.getString("area","").equals(areas[i]))
                                {
                                    areaSpinner.setSelection(i);
                                    break;


                                }
                            }


                            //  }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar1.setVisibility(View.GONE);
                Toast.makeText(CheckoutSecondStep.this, error.getMessage()+"", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put("tehsilname",area);
                return map;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    private void showGPSDisabledAlertToUser(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Goto Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                finish();
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        dialog.cancel();
                        finish();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }



    public void checkoutCart(String day,String timeofDelivery) {

        HttpAsyncRequest request = new HttpAsyncRequest(CheckoutSecondStep.this, Constant.checkout_url, HttpAsyncRequest.RequestType.POST, new CheckOutParser(), listenerbill);
        request.addParam("json_array", makeOrdersJson().toString());
       // request.addParam("bill", (Constant.totalbill + charges)+"");
        int newCharges1 = Integer.parseInt(newCharges);
        int newUrgentCharges1 = Integer.parseInt(new_urgentcharges);
        if (dType.equals("r")){
            if (Constant.totalbill < Integer.parseInt(newMin)){
                newCharges1 = Integer.parseInt(newCharges);
            }else{
                newCharges1 = 0;
            }
        }else if (dType.equals("u")){
                newCharges1 = newUrgentCharges1;

        }

        request.addParam("bill", (Constant.totalbill + newCharges1)+"");
        request.addParam("profit",Constant.totalprofit+"");
        request.addParam("referral_coupan",preferences.getString("referralcode","null"));
        request.addParam("timestamp",day);
        request.addParam("day",timeofDelivery  );
        request.addParam("userid", preferences.getString("userid", ""));
        request.addParam("charges",newCharges1+"");
        request.addParam("latitude",latitude+"");
        request.addParam("longitude",longitude+"");

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c);

        request.addParam("date", formattedDate);
        request.addParam("comments", comments.getText().toString());
        time_str = day;
        date_str=timeofDelivery;
        adress_str = (Constant.totalbill + newCharges1)+"";


        request.execute();

    }
//////////////////////////////////////////////////////////////////////////






    /////////////////////////////////////////////////////////////////////
    AsyncTaskListener listenerbill = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            if (result.isSuccess()) {
                try {




                    String rs = result.getMessage();
                    if (rs.equals("0")) {

                        Toast.makeText(CheckoutSecondStep.this, "Order has been submitted successfully", Toast.LENGTH_SHORT).show();
                        check.setBackgroundResource(R.drawable.checkoutthird);
                        empty();
                        myProgressDialog.dismiss();
                        intent = new Intent(CheckoutSecondStep.this,OrderSubmitActivity.class);
                        intent.putExtra("bill",adress_str);
                        intent.putExtra("time",time_str);
                        intent.putExtra("day",date_str);
                        intent.putExtra("a","s");
                        startActivity(intent);
                        finish();

                        if(CartActivity.activity !=null)
                            CartActivity.activity.finish();

                    }


                } catch (Exception e) {


                }


            } else {

                //  Toast.makeText(CheckoutSecondStep.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();
                //  myProgressDialog.dismiss();

                Toast.makeText(CheckoutSecondStep.this, "Order has been submitted successfully", Toast.LENGTH_SHORT).show();
                check.setBackgroundResource(R.drawable.checkoutthird);
                empty();
                myProgressDialog.dismiss();
                intent = new Intent(CheckoutSecondStep.this,OrderSubmitActivity.class);
                intent.putExtra("bill",adress_str);
                intent.putExtra("time",time_str);
                intent.putExtra("day",date_str);
                intent.putExtra("a","s");
                startActivity(intent);
                finish();

                if(CartActivity.activity !=null)
                    CartActivity.activity.finish();

            }


        }


    };

    private void getTimeStamp1(){
        if (timestamp != null){
            timestamp.clear();
        }
        timestamp = new ArrayList<>();
        timestamp.add(  0,"Expected:"+ finalTime);

        for (int i =0 ;i<=3;i++){
            SimpleDateFormat df = new SimpleDateFormat("HH:mm");
            try {
                Date d = df.parse(finalTime);
                Calendar cal = Calendar.getInstance();
                cal.setTime(d);
                cal.add(Calendar.MINUTE, 60);
                String finalTime1 = df.format(cal.getTime());
                timestamp.add(i+1,finalTime+ " TO "+ finalTime1);
                finalTime = finalTime1;
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        time.setAdapter(new ArrayAdapter<String>(this, R.layout.row_area, R.id.text, timestamp));

    }

    private void getTimeStamp() {

        HttpAsyncRequest request = new HttpAsyncRequest(this, Constant.get_time_stamp, HttpAsyncRequest.RequestType.POST, this, listener);
        request.execute();
        myProgressDialog = new MyProgressDialog(CheckoutSecondStep.this);
        myProgressDialog.show();
        myProgressDialog.setCanceledOnTouchOutside(false);


    }


    AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            if (result.isSuccess()) {
                try {

                    myProgressDialog.dismiss();
                } catch (Exception e) {
                }


            } else {

                Toast.makeText(CheckoutSecondStep.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();
                myProgressDialog.dismiss();

            }
        }


    };

    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {

            try {
                result.success(true);
                JSONObject obj = new JSONObject(response);
                if (obj.optString("error").equals("false")) {
                    JSONArray array = obj.optJSONArray("timestamp");
                    if (array != null) {

                        timestamp = new ArrayList<>();
                        timestamp.add(  0,"Select Time");

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject time = array.getJSONObject(i);
                            if(currenttime !=0) {
                                String tm[]=  time.optString("start").split(":");
                                String t = tm[0]+tm[1];
                                int tt =  Integer.parseInt(t);

                                if(false)
                                {
                                    try {
                                        final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                                        Date dateObj = sdf.parse(time.optString("start"));
                                        System.out.println(dateObj);
                                        String st =  new SimpleDateFormat("K:mm a").format(dateObj);

                                        dateObj = sdf.parse(time.optString("end"));
                                        System.out.println(dateObj);
                                        String ed =  new SimpleDateFormat("K:mm a").format(dateObj);

                                        timestamp .add( st+" to "+ed);


                                    } catch (final ParseException e) {
                                        e.printStackTrace();
                                    }

                                }
                                else{

                                    timestamp.add( time.optString("start")+" to " +time.optString("end"));

                                }

                            }
                            else{
                                timestamp.add( time.optString("start")+" to " +time.optString("end"));

                            }






                        }


                        time.setAdapter(new ArrayAdapter<String>(this, R.layout.row_area, R.id.text, timestamp));
                    } else
                        Toast.makeText(this, "Time Stamp Not Availiable", Toast.LENGTH_SHORT).show();


                }
            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);

            }


        }
        return result;
    }

    @Override
    public void onLocationChanged(Location location) {
        if(location!=null)
        {

            latitude=location.getLatitude();
            longitude=location.getLongitude();
    //Toast.makeText(this, longitude+","+latitude, Toast.LENGTH_SHORT).show();

        }


    }

    private JSONArray makeOrdersJson() {
        ArrayList<BillModel> arrayList = null;
        appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(CheckoutSecondStep.this);
        String json = appSharedPrefs.getString("cart", "");
        Gson gson = new Gson();
        final Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);


        JSONArray jsonArray = new JSONArray();

        for (int i = 0; i < arrayList.size(); i++) {
            JSONObject obj = new JSONObject();

            try {
                obj.put("price", arrayList.get(i).getProductprice());
                obj.put("quantity", arrayList.get(i).getQuantity());
                obj.put("id", arrayList.get(i).getFooditem_id());
                obj.put("url", arrayList.get(i).getImagepath());
                obj.put("name", arrayList.get(i).getName());
                obj.put("store_name",arrayList.get(i).getStorename());
                obj.put("unit",arrayList.get(i).getUnit());


                jsonArray.put(obj);

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }


        return jsonArray;


    }

    public static boolean empty() {
        ArrayList<BillModel> arrayList = null;

        Gson gson = new Gson();




        arrayList = new ArrayList<>();


        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("cart", jsons);
        prefsEditor.remove("cart");
        prefsEditor.commit();
        return true;



    }

    public void getLocationPermission(String day,String timeofDelivery){
        Log.d(TAG,"getLocationPermission : getting location permission");
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

        if (ActivityCompat.checkSelfPermission(this.getApplicationContext(),FINE_LOCATION)== PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.checkSelfPermission(this.getApplicationContext(),COARSE_LOCATION)== PackageManager.PERMISSION_GRANTED){
                locationPermissionGranted = true;
                myProgressDialog.setCanceledOnTouchOutside(false);
                myProgressDialog.show();
                getDeviceLocation(day,timeofDelivery);
            }else{
                ActivityCompat.requestPermissions(this,permissions,REQUEST_CODE);
            }
        }else{
            ActivityCompat.requestPermissions(this,permissions,REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG,"onRequestPermissionResult : Called");
        locationPermissionGranted = false;
        switch (requestCode){
            case REQUEST_CODE:
                if (grantResults.length >0){
                    for (int i=0;i<grantResults.length;i++){
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED){
                            Log.d(TAG,"onRequestPermissionResult : permission not granted");
                            locationPermissionGranted = false;

                            break;
                        }
                    }
                    Log.d(TAG,"onRequestPermissionResult : Permission granted");
                    locationPermissionGranted = true;
                    myProgressDialog.setCanceledOnTouchOutside(false);
                    myProgressDialog.show();
                    getDeviceLocation(day1,time1);
                }

        }

    }


    public void locationAddress(double latitude,double longitude) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());


        try {
            List<Address> addresses = geocoder.getFromLocation(latitude,longitude, 1);
            String address23 = addresses.get(0).getAddressLine(0);

        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Exception:"+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void getDeviceLocation(final String day, final String timeofDelivery){
        Log.d(TAG,"getDeviceLocation : getting device location");
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        final String address;

        try {
            if (locationPermissionGranted){
                Task location = fusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful() && task.getResult() != null){
                            Log.d(TAG,"onComplete : found location ");
                            Location currentlocation = (Location) task.getResult();

                            latitude = currentlocation.getLatitude();
                            longitude = currentlocation.getLongitude();
                            checkoutCart(day,timeofDelivery);
                            //checkoutCart2();
                            locationAddress(currentlocation.getLatitude(),currentlocation.getLongitude());



                        }else{
                            Log.d(TAG,"onComplete : location not found");
                            checkoutCart(day,timeofDelivery);
                            // checkoutCart2();
                        }
                    }
                });
            }
        }catch (SecurityException e){
            Toast.makeText(this, "Security Exception "+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}