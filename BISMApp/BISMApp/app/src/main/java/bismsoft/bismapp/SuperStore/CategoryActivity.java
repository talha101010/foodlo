package bismsoft.bismapp.SuperStore;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bhargavms.podslider.PodSlider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bismsoft.bismapp.Activities.WalletActivity;
import bismsoft.bismapp.DrawerActivities.LoginPanel;
import bismsoft.bismapp.DrawerActivities.SignupActivity;
import bismsoft.bismapp.DrawerActivities.SubCategoryActivity;
import bismsoft.bismapp.FastFood.BillModel;
import bismsoft.bismapp.FastFood.MenuFoodItems;
import bismsoft.bismapp.FastFood.MyProgressDialog;
import bismsoft.bismapp.Http.AsyncTaskListener;
import bismsoft.bismapp.Http.HttpAsyncRequest;
import bismsoft.bismapp.Http.TaskResult;
import bismsoft.bismapp.Internet.NetworkUtil;
import bismsoft.bismapp.Parser.BaseParser;
import bismsoft.bismapp.Parser.CouponParser;
import bismsoft.bismapp.Parser.DealsParser;
import bismsoft.bismapp.R;
import bismsoft.bismapp.Url.Constant;

public class CategoryActivity extends AppCompatActivity  implements BaseParser,NavigationView.OnNavigationItemSelectedListener {

    public static ArrayList<MenuFoodItems> dealsArrayList;
   ViewPager dealspager;
    ProgressBar progressBar;

    PodSlider podSlider;
    GridView categoriesgridview;
    RelativeLayout categorylayout;
    public static ArrayList<MenuFoodItems>  subcategorylist;
    SharedPreferences preferences;
    MyProgressDialog myProgressDialog;
    private String categoryname, categoryid;
    Intent intent;

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    private SharedPreferences appSharedPrefs;
    SharedPreferences.Editor editor;
    Menu menu;

    NavigationView nav;
    String referalcode,note;
    String walletprice;
    DatabaseReference databaseReference;
    TextView       noteTv;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        init();

        databaseReference = FirebaseDatabase.getInstance().getReference("note");
        noteTv = findViewById(R.id.note_tv);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){


                    note = dataSnapshot1.getValue().toString();

                }

                noteTv.setText(note);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




        categoriesgridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                categoryname = Constant.categoriesArrayList.get(position).getProductname();
                sendCategoryidtoServer(position);

            }
        });
        getDeals();
        categoriesgridview.setAdapter(new CategoriesAdapter(CategoryActivity.this, Constant.categoriesArrayList));
        getCouponCode();


        //  getTotalCartCounter();
    }

    private void getCouponCode() {
        HttpAsyncRequest request = new HttpAsyncRequest(this, Constant.getcoupon, HttpAsyncRequest.RequestType.POST, new CouponParser(), couponlistner);
        request.addParam("userid",preferences.getString("userid","null"));
        request.execute();



    }

    AsyncTaskListener couponlistner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

//            if (result.isSuccess()) {
//
//                if(result.getMessage().equals("null"))
//                {
//                    menu.findItem(R.id.refferal).setVisible(false);
//                }
//                else {
//                    menu.findItem(R.id.refferal).setVisible(true);
//                    referalcode = result.getMessage();
//                }
//
//
//
//
//
//
//
//            } else {
//
//
//                try {
//                } catch (Exception e) {
//                }
//            }
      }


    };

    @Override
    protected void onResume() {
        super.onResume();
        HandleCartNumber();
    }

    public void HandleCartNumber() {

        ArrayList<BillModel> arrayList = null;

        appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        TextView textView = (TextView) findViewById(R.id.itemnumber);
        LinearLayout cartlayout = (LinearLayout) findViewById(R.id.cartlayout);
        LinearLayout accountlayout = (LinearLayout) findViewById(R.id.accountlayout);
        accountlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!preferences.getString("userid","").equals(""))
                    startActivity(new Intent(getApplicationContext(), SignupActivity.class).putExtra("update", 1));
                else
                    startActivity(new Intent(getApplicationContext(), LoginPanel.class));

            }
        });


        LinearLayout searchlay = (LinearLayout) findViewById(R.id.searchlayout);
        searchlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SearchActivity.class));
            }
        });


        String json = appSharedPrefs.getString("cart", "");
        if (!json.equals("")) {
            arrayList = gson.fromJson(json, type);

            textView.setText(arrayList.size() + "");
            textView.setVisibility(View.VISIBLE);
            cartlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), CartActivity.class));
                }
            });
        } else {
            textView.setVisibility(View.GONE);
            cartlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(), "Nothing in the Cart Yet", Toast.LENGTH_SHORT).show();
                }
            });
        }


    }

    private void getDeals() {
        HttpAsyncRequest request = new HttpAsyncRequest(this, Constant.get_deals, HttpAsyncRequest.RequestType.POST, new DealsParser(), listen_deals);


        request.execute();
        progressBar.setVisibility(View.VISIBLE);

    }

    AsyncTaskListener listen_deals = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            if (result.isSuccess()) {
                progressBar.setVisibility(View.GONE);

                if (result.getMessage().equals("true")) {
//                    myProgressDialog.dismiss();
                    dealsArrayList = (ArrayList<MenuFoodItems>) result.getData();
                    dealspager.setAdapter(new ViewPagerAdapter(CategoryActivity.this, dealsArrayList));
//                    dealspager.setDirection(AutoScrollViewPager.RIGHT);
//                    dealspager.startAutoScroll();
//                    dealspager.setCycle(true);
//                    dealspager.setInterval(3000);
//                    dealspager.setBorderAnimation(true);
//                    podSlider.setUpWithViewPager(dealspager);
//                    podSlider.setNumberOfPods(dealsArrayList.size());
//                    podSlider.setVisibility(View.VISIBLE);
                    dealspager.setVisibility(View.INVISIBLE);


                } else if (result.getMessage().equals("false")) {
                    Toast.makeText(CategoryActivity.this, "This Category is not active yet", Toast.LENGTH_SHORT).show();

                }


            } else {

                Toast.makeText(CategoryActivity.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();
                myProgressDialog.dismiss();

            }
        }


    };


    private void init() {
        dealspager =  findViewById(R.id.dealpager_category);
        categoriesgridview = (GridView) findViewById(R.id.gridview_category);
        categorylayout = (RelativeLayout) findViewById(R.id.pagerlayout);
        preferences = getSharedPreferences("EStore", Context.MODE_PRIVATE);
        editor =  preferences.edit();
        podSlider = (PodSlider) findViewById(R.id.pod_slider);
        nav = (NavigationView)findViewById(R.id.navview);
        progressBar = (ProgressBar) findViewById(R.id.progress);



        drawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(drawerToggle);
        nav.setNavigationItemSelectedListener(this);

        drawerToggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        menu= nav.getMenu();


    }



    private void sendCategoryidtoServer(int position) {

        HttpAsyncRequest request = new HttpAsyncRequest(this, Constant.GET_SUBCATEGORY_URL, HttpAsyncRequest.RequestType.POST, this, listenerfor_sub_categories);
        request.addParam("categoryid", Constant.categoriesArrayList.get(position).getDealid());
        categoryid = Constant.categoriesArrayList.get(position).getDealid();

        request.execute();
        myProgressDialog = new MyProgressDialog(CategoryActivity.this);
        myProgressDialog.show();
        myProgressDialog.setCanceledOnTouchOutside(false);


    }


    AsyncTaskListener listenerfor_sub_categories = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            if (result.isSuccess()) {

                if (result.getMessage().equals("true")) {
                    myProgressDialog.dismiss();
                    subcategorylist = (ArrayList<MenuFoodItems>) result.getData();
                    startActivity(new Intent(CategoryActivity.this, SubCategoryActivity.class)
                            .putExtra("categoryname", categoryname)
                            .putExtra("categoryid", categoryid)


                    );


                } else if (result.getMessage().equals("false")) {
                    Toast.makeText(CategoryActivity.this, "Under Process,It will come soon", Toast.LENGTH_SHORT).show();
                    myProgressDialog.dismiss();
                }


            } else {

                Toast.makeText(CategoryActivity.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();
                try {
                    myProgressDialog.dismiss();
                } catch (Exception e) {
                }
            }
        }


    };

    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {

            try {
                result.success(true);
                JSONObject obj = new JSONObject(response);
                if (obj.optString("error").equals("0")) {
                    result.setMessage("true");

                    JSONArray array = obj.optJSONArray("subcategory");
                    ArrayList<MenuFoodItems> list = new ArrayList<>(array.length());
                    if (array.length() != 0 && array != null) {
                        for (int i = 0; i < array.length(); i++) {

                            JSONObject items = array.getJSONObject(i);
                            MenuFoodItems food = new MenuFoodItems();
                            food.setProductname(items.optString("name_in_english"));
                            food.setDealid(items.optString("id"));
                            food.setNameinUrdu(items.optString("name_in_urdu"));
                            food.setImagepath(items.optString("url"));

                            list.add(food);
                        }


                        result.setData(list);
                    } else {
                        result.setMessage("false");

                    }

                } else if (obj.optString("error").equals("1")) {
                    result.setMessage("false");
                }

            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);

            }


        }
        return result;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {


            case R.id.history:
                if (!preferences.getString("userid", "").equals("")){
                     intent = new Intent(CategoryActivity.this, OrderHistory.class);
                    startActivity(intent);}

                else{
                    startActivity(new Intent(getApplicationContext(), LoginPanel.class));
                drawerLayout.closeDrawer(GravityCompat.START);}


                return true;

            case R.id.volit_:
            {
                Toast.makeText(this, "comming soon", Toast.LENGTH_SHORT).show();
                return  true;
            }
            case R.id.contact_us:
            {
                Toast.makeText(this, "comming soon", Toast.LENGTH_SHORT).show();
                return  true;
            }
            case R.id.feed_back:
            {
                Toast.makeText(this, "comming soon", Toast.LENGTH_SHORT).show();
                return  true;
            }
            case R.id.term_condition:
            {
                Toast.makeText(this, "comming soon", Toast.LENGTH_SHORT).show();
                return  true;
            }


             case R.id.log_out: {
                 myProgressDialog = new MyProgressDialog(CategoryActivity.this);
                 myProgressDialog.show();
                 myProgressDialog.setCanceledOnTouchOutside(false);

                 editor.remove("login");
                 editor.remove("userid");
                 editor.remove("phone");


                 editor.remove("name");
                 editor.remove("area");
                 editor.remove("address");
                 editor.remove("tehsil");
                 editor.commit();
                 new Handler().postDelayed(new Runnable() {
                     @Override
                     public void run() {
                         try {
                             myProgressDialog.dismiss();
                             Intent intent =new Intent(CategoryActivity.this,CategoryActivity.class);
                             startActivity(intent);
                             finish();


                         } catch (Exception e) {
                         }
                     }
                 }, 2000);
                 return false;
             }







//            case R.id.coupan:
//
//               // getCoupan();
//
//
//                return true;
//
//            case R.id.refferal:
//                Intent shareIntent = new Intent(Intent.ACTION_SEND);
//                shareIntent.setType("text/plain");
//                shareIntent.putExtra(Intent.EXTRA_TEXT,"Please install the Bhakkar App from following link"+"\n\n"+
//                        "https://play.google.com/store/apps/details?id="+CategoryActivity.this.getPackageName()
//                        +"\n\n"+"and add this code "+referalcode +" as Referral code during registration."
//                );
//                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Invitation");
//                startActivity(Intent.createChooser(shareIntent, "Share..."));
//                drawerLayout.closeDrawer(GravityCompat.START);
//
//                return  true;



    }



        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.bill, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.billid) {

            if (!preferences.getString("userid", "").equals("")) {
                if (NetworkUtil.isNetworkConnected(CategoryActivity.this)) {
//
                    NetworkUtil networkUtil = new NetworkUtil(CategoryActivity.this);
                    networkUtil.execute();

                    startActivity(new Intent(CategoryActivity.this, WalletActivity.class));


                } else {

                    AlertDialog alertDialog = new AlertDialog.Builder(CategoryActivity.this).create();
                    alertDialog.setTitle("Check Internet Connection");
                    alertDialog.setMessage("آپ انٹرنیٹ سے کنکٹ نہیں ہیں۔");
                    alertDialog.show();
                }

            }

            else{
                startActivity(new Intent(getApplicationContext(), LoginPanel.class));
                drawerLayout.closeDrawer(GravityCompat.START);}



            return true;

        }
        if(drawerToggle.onOptionsItemSelected(item))


            return  true;


        return super.onOptionsItemSelected(item);




    }

    private void getCoupan(){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait.....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.getcoupon,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            showMessage(jsonObject.getString("coupan"),jsonObject.getString("referralcoupan"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(CategoryActivity.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("userid",preferences.getString("userid",null));
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void showMessage(String coupan,String rcoupan){
        AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(this);
        dialogBuilder1.setTitle("Information");
        if (coupan.equals("null")) {
            if (rcoupan.equals("bkr0000")){
                dialogBuilder1.setMessage("You did not have any Coupon yet.Please contact to company to get your Coupon" +
                        " code and earn money by referring to your friends. Once you get coupon from coupon , Invite friends menu" +
                        " will visible in your menu list and then you can invite your friends for shop from here. Yet you " +
                        " have default Referral coupan "+rcoupan+" , you can also contact to compnay if you want to change referral coupon");
            }else {
                dialogBuilder1.setMessage("You did not have any Coupon yet.Please contact company to get your Coupon" +
                        " code and earn money by referring your freinds. Once you get coupon from coupon , Invite friends menu" +
                        " will visible in your menu list and then you can invite your friends for shop from here. And Your Referral Coupon " +
                        " is " + rcoupan);
            }
        }else{
            if (rcoupan.equals("bkr0000")) {
                dialogBuilder1.setMessage("Your Coupon is " + coupan + ". You can earn money through this Coupon." +
                        " Plese invite your friends using Invite Friends option, when your friends sign up using your coupon code as refrrel code" +
                        " Yet you have default Referral coupan "+rcoupan+" you can contact" +
                        " to company to change your referral coupon");
            }else{
                dialogBuilder1.setMessage("Your Coupon is " + coupan + ". You can earn money through this coupon." +
                        " Plese invite your friends using Invite Friends option, when your friends sign up using your coupon code as referrel" +
                        ".Now  your Referral coupan is "+rcoupan);
            }
        }
        dialogBuilder1.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        AlertDialog b1 = dialogBuilder1.create();
        b1.show();
    }





}
