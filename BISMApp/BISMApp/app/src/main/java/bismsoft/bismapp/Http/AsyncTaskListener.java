package bismsoft.bismapp.Http;

public interface AsyncTaskListener {

    public void onComplete(TaskResult result);

}