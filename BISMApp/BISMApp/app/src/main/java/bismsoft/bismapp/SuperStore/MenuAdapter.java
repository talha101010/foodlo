package bismsoft.bismapp.SuperStore;

import android.content.Context;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import bismsoft.bismapp.DrawerActivities.FoodItemsActivity;
import bismsoft.bismapp.FastFood.MenuFoodItems;
import bismsoft.bismapp.FastFood.MyProgressDialog;
import bismsoft.bismapp.R;

public class MenuAdapter    extends RecyclerView.Adapter<MenuAdapter.MyViewHolder> {

    Context c;
    ArrayList<MenuFoodItems> categorieslist;
    private MyProgressDialog myProgressDialog;
    int hold = 0;

    public MenuAdapter(Context context, ArrayList<MenuFoodItems> categorieslist) {
        c = context;
        this.categorieslist = categorieslist;


    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView categoryname;
        View view;
        // ImageView categoryimage;
        RelativeLayout lay;

        public MyViewHolder(View v) {
            super(v);

            //categoryimage = (ImageView) v.findViewById(R.id.subcategoryimage);
            categoryname = (TextView) v.findViewById(R.id.subcategoryname);
            lay = (RelativeLayout) v.findViewById(R.id.lay);
            view = (View) v.findViewById(R.id.view);

        }


    }


    @Override
    public MenuAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_menu, parent, false);
        MenuAdapter.MyViewHolder myViewHolder = new MenuAdapter.MyViewHolder(view);


        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MenuAdapter.MyViewHolder holder, final int position) {


        if (hold == position) {

            holder.categoryname.setText(categorieslist.get(position).getProductname());

            holder.view.setBackgroundColor(ResourcesCompat.getColor(c.getResources(), R.color.black, null));
            holder.categoryname.setTextColor(ResourcesCompat.getColor(c.getResources(), R.color.black, null));

            holder.view.setVisibility(View.INVISIBLE);

        } else {

          //  holder.view.setBackgroundColor(ResourcesCompat.getColor(c.getResources(), R.color.transparent, null));
            holder.categoryname.setTextColor(ResourcesCompat.getColor(c.getResources(), R.color.colorPrimaryDark, null));
            holder.categoryname.setText(categorieslist.get(position).getProductname());

        }


        holder.lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.view.setBackgroundColor(ResourcesCompat.getColor(c.getResources(), R.color.white, null));
                holder.categoryname.setTextColor(ResourcesCompat.getColor(c.getResources(), R.color.white, null));

                hold = position;
                notifyDataSetChanged();


                FoodItemsActivity.getFoodItems(categorieslist.get(position).getDealid());
            }
        });


    }

    @Override
    public int getItemCount() {
        return categorieslist.size();
    }
}
