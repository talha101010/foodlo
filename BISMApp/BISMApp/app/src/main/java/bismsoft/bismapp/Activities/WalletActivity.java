package bismsoft.bismapp.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import bismsoft.bismapp.DrawerActivities.LoginPanel;
import bismsoft.bismapp.FastFood.MyProgressDialog;
import bismsoft.bismapp.Http.AsyncTaskListener;
import bismsoft.bismapp.Http.HttpAsyncRequest;
import bismsoft.bismapp.Http.TaskResult;
import bismsoft.bismapp.Parser.CouponParser;
import bismsoft.bismapp.R;
import bismsoft.bismapp.Url.Constant;

public class WalletActivity extends AppCompatActivity {

    SharedPreferences preferences;
    MyProgressDialog myProgressDialog;
    String walletprice;
    String referalcode;
    TextView walletamount;

    LinearLayout havetologinlayout, walletamountlayout;
    Button havetologinbtn;
    ProgressDialog dowloading;
    CardView walletamountcardview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        havetologinlayout = findViewById(R.id.havetologinlayout);
        walletamountlayout =findViewById(R.id.walletamountlayout);
        havetologinbtn = findViewById(R.id.havetologinbtn);
        walletamountcardview = findViewById(R.id.walletamountcardview);



        walletamount = findViewById(R.id.walletamount);
        preferences = getSharedPreferences("EStore", Context.MODE_PRIVATE);

        getCouponCode();
    }

    private void getCouponCode() {
        HttpAsyncRequest request = new HttpAsyncRequest(this, Constant.getcoupon, HttpAsyncRequest.RequestType.POST, new CouponParser(), couponlistner);
        request.addParam("userid",preferences.getString("userid","null"));
        request.execute();
        dowloading = ProgressDialog.show(WalletActivity.this,"Data Loading...","Please Wait");



    }

    AsyncTaskListener couponlistner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            if (result.isSuccess()) {

                if(result.getMessage().equals("null"))
                {

                }
                else {

                    referalcode = result.getMessage();
                }

                dowloading.dismiss();

                walletamountlayout.setVisibility(View.VISIBLE);
                walletamountcardview.setVisibility(View.VISIBLE);

                walletamount.setText("PKR"+"\t\t"+result.getData()+"\t");
                //  menu.findItem(R.id.wallet).setTitle("Your Wallet has Rs."+(String)result.getData());





            } else {

                dowloading.dismiss();

                walletamountlayout.setVisibility(View.INVISIBLE);

                havetologinlayout.setVisibility(View.VISIBLE);
                havetologinbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(WalletActivity.this, LoginPanel.class));
                        finish();
                    }
                });
                try {
                } catch (Exception e) {


                    Toast.makeText(WalletActivity.this, "Some thing is going wrong", Toast.LENGTH_SHORT).show();


                }
            }
        }


    };
}
