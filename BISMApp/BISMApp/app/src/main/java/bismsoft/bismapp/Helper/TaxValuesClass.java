package bismsoft.bismapp.Helper;

public class TaxValuesClass {

    String taxname,taxamount,taxpercentage,tax_id;

    public String getTax_id() {
        return tax_id;
    }

    public void setTax_id(String tax_id) {
        this.tax_id = tax_id;
    }

    public String getTaxname() {
        return taxname;
    }

    public void setTaxname(String taxname) {
        this.taxname = taxname;
    }

    public String getTaxamount() {
        return taxamount;
    }

    public void setTaxamount(String taxamount) {
        this.taxamount = taxamount;
    }

    public String getTaxpercentage() {
        return taxpercentage;
    }

    public void setTaxpercentage(String taxpercentage) {
        this.taxpercentage = taxpercentage;
    }
}
