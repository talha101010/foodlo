package bismsoft.bismapp.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.InputStream;

import bismsoft.bismapp.R;


public class CustomPagerAdapter extends PagerAdapter {
    public  static    Context mContext;
    LayoutInflater mLayoutInflater;
    public  static   String[] imagepaths;
    public CustomPagerAdapter(Context context, String[] paths ) {
        mContext = context;
        imagepaths = paths;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return imagepaths.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        View v = mLayoutInflater.inflate(R.layout.row_list, container, false);

        ImageView imageView = (ImageView)v.findViewById(R.id.image);
        final ProgressBar progressBar = (ProgressBar)v.findViewById(R.id.progress);
        progressBar.setVisibility(View.VISIBLE);


        Glide.with(mContext)
                .load(imagepaths[position])

                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(imageView);


        new GetImageFromURL(imageView,position).execute();
        container.addView(v);


        return  v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }


}
class GetImageFromURL extends AsyncTask<String,Void,Bitmap> {
    ImageView imgV;
    int position;

    public GetImageFromURL(ImageView imgV,int p){
        this.imgV = imgV;
        position = p;
    }

    @Override
    protected Bitmap doInBackground(String... url) {
        String urldisplay =CustomPagerAdapter.imagepaths[position] ;
        Bitmap  bitmap = null;
        try {
            InputStream srt = new java.net.URL(urldisplay).openStream();
            bitmap = BitmapFactory.decodeStream(srt);
        } catch (Exception e){
            e.printStackTrace();
        }
        return bitmap;
    }

//    @Override
//    protected void onPostExecute(Bitmap bitmap) {
//        super.onPostExecute(bitmap);
//        // imgV.setImageBitmap(bitmap);
//        SqlHelper db = new SqlHelper(CustomPagerAdapter.mContext);
//        long v = db.insert( getImageBytes(bitmap));
//        //Toast.makeText(CustomPagerAdapter.mContext, v+"", Toast.LENGTH_SHORT).show();
//
//    }
//    public static byte[] getImageBytes(Bitmap bitmap) {
//        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
//        return stream.toByteArray();
//    }
}