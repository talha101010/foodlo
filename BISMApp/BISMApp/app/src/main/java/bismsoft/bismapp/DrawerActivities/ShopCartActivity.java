package bismsoft.bismapp.DrawerActivities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import bismsoft.bismapp.FastFood.BillModel;
import bismsoft.bismapp.FastFood.MyProgressDialog;
import bismsoft.bismapp.FastFood.ShopCartAdaptor;
import bismsoft.bismapp.FastFood.ShopMenu;
import bismsoft.bismapp.FastFood.ShopSecondStep;
import bismsoft.bismapp.Helper.TaxValuesClass;
import bismsoft.bismapp.R;
import bismsoft.bismapp.Url.Constant;

public class ShopCartActivity extends AppCompatActivity  {

    public  static SharedPreferences preferences;
    public static ListView listView;
   public static Button checkout;
    private MyProgressDialog myProgressDialog;
    RelativeLayout layout;
    public  static ArrayList<TaxValuesClass> taxaraylist;
    public  static   boolean istoCalculatetax;
    public  static Activity activity;
    private static SharedPreferences appSharedPrefs;
    String notetext,charges;
    public static ImageView arrowgobelow;
    int minimumShopping;
    ArrayList<BillModel> arrayList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_cart);

        init();
        activity = this;
        // get_cart();
        //get_Taxes();
        if(preferences.getString("userid","").equals(""))
            checkout.setText("Sign In to CheckOut");

        arrayList = null;
        minimumShopping = Integer.parseInt(ShopMenu.limit);

        Gson gson = new Gson();
        final Type type = new TypeToken<List<BillModel>>() {
        }.getType();
        appSharedPrefs= PreferenceManager
                .getDefaultSharedPreferences(this);
        String json = appSharedPrefs.getString("shopcart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);


        listView.setAdapter(new ShopCartAdaptor(this,arrayList));
        handlearrow();


        calculateBill();


        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<BillModel> arrayList = null;

                String json = appSharedPrefs.getString("shopcart", "");
                Gson gson = new Gson();

                if (!json.equals(""))
                    arrayList = gson.fromJson(json, type);
                if(!preferences.getString("userid","").equals("")) {
                    if (arrayList != null && !(arrayList.size() <= 0)) {

                        if(Constant.totalbill >= minimumShopping)
                        {
                            startActivity(new Intent(ShopCartActivity.this, ShopSecondStep.class)
                                    .putExtra("note","Delivery Charges  = "+ShopMenu.charges+"Rs")
                                    .putExtra("charges", ShopMenu.charges)
                            );

                        }
                        else{
                            AlertDialog.Builder builder;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                builder = new AlertDialog.Builder(ShopCartActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                            } else {
                                builder = new AlertDialog.Builder(ShopCartActivity.this);
                            }
                            builder.setTitle("Warning")
                                    .setMessage("Your Minimum Order should be  "+minimumShopping+" Rs")
                                    .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // continue with delete


                                        }
                                    })
                                    .show();
                        }
                    }
                    else
                        Toast.makeText(ShopCartActivity.this, "Please Do Shopping to Continue", Toast.LENGTH_SHORT).show();
                }else
                    activity. startActivity(new Intent(activity, LoginPanel.class));





            }
        });

        arrowgobelow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listView.setSelection(arrayList.size()-1);
            }
        });


    }



    private void init() {

        preferences = getSharedPreferences("EStore", Context.MODE_PRIVATE);
        listView = (ListView) findViewById(R.id.list);
        checkout = (Button) findViewById(R.id.checkout);
        layout = (RelativeLayout)findViewById(R.id.out);
        arrowgobelow = (ImageView)findViewById(R.id.arrow);

    }

    public static void calculateBill() {
        ArrayList<BillModel> arrayList=null ;


        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        String json = appSharedPrefs.getString("shopcart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);




        Constant.totalbill = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            Constant.totalbill += Integer.parseInt(arrayList.get(i).getQuantity() ) *Integer.parseInt(arrayList.get(i).getProductprice() );

        }

        if(preferences.getString("userid","").equals(""))
            checkout.setText("Log in to Order = "+" "+"Rs. "+Constant.totalbill);
        else
            checkout.setText("Order Now = "+" "+"Rs. "+Constant.totalbill);

       // totalbill.setText("Rs " + Constant.totalbill + "");


    }


    public  static void handlearrow()
    {

        try {


        Runnable fitsOnScreen = new Runnable() {
            @Override
            public void run() {
                int last = listView.getLastVisiblePosition();
                if(!( last< 0))
                    if (last == listView.getCount()  && listView.getChildAt(last).getBottom() <= listView.getHeight()) {
                        // It fits!
                        arrowgobelow.setVisibility(View.GONE);
                    } else {
                        arrowgobelow.setVisibility(View.VISIBLE);
                    }
            }
        };
        listView.post(fitsOnScreen);
    }catch (Exception e){

        Toast.makeText(activity, e.toString(), Toast.LENGTH_SHORT).show();
    }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(preferences.getString("userid","").equals(""))
            checkout.setText("Log in to Order = "+" "+"Rs. "+Constant.totalbill);
        else
            checkout.setText("Order Now = "+" "+"Rs. "+Constant.totalbill);
    }
}
