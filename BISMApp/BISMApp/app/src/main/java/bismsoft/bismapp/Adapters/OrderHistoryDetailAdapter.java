package bismsoft.bismapp.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import bismsoft.bismapp.DataProviders.OrderHistoryListItems;
import bismsoft.bismapp.R;

/**
 * Created by wwww on 10/12/2018.
 */

public class OrderHistoryDetailAdapter extends RecyclerView.Adapter<OrderHistoryDetailAdapter.ViewHolder> {
    Context context;
    List<OrderHistoryListItems> list;

    public OrderHistoryDetailAdapter(List<OrderHistoryListItems> list,Context context) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_detail_list_items,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        OrderHistoryListItems listItems = list.get(i);
        viewHolder.foodname.setText(listItems.getName());
        viewHolder.quantity.setText("Qty: "+listItems.getQuantity());
        viewHolder.price.setText("Rs. "+listItems.getPrice());
        viewHolder.size.setText(listItems.getUnit());

        Glide.with(context)

                .load(listItems.getImage())
                .dontAnimate()

                .placeholder(R.drawable.logoone)

                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        viewHolder. progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(viewHolder.foodimage);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView foodimage;
        TextView foodname, price, quantity, size;
        Button delete, minus, plus;
        ImageView edit;
        ProgressBar progressBar ;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            foodimage = (ImageView) itemView.findViewById(R.id.foodimage);
            foodname = (TextView) itemView.findViewById(R.id.ordername);
            quantity = (TextView) itemView.findViewById(R.id.quantity);
            price = (TextView) itemView.findViewById(R.id.totalbill);
            size = (TextView) itemView.findViewById(R.id.size);
            progressBar = (ProgressBar)itemView.findViewById(R.id.progress);

        }
    }
}
