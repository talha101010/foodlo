package bismsoft.bismapp.SuperStore;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;


import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bismsoft.bismapp.FastFood.MenuFoodItems;
import bismsoft.bismapp.Http.AsyncTaskListener;
import bismsoft.bismapp.Http.HttpAsyncRequest;
import bismsoft.bismapp.Http.TaskResult;
import bismsoft.bismapp.Parser.BaseParser;
import bismsoft.bismapp.Parser.CategoryParser;
import bismsoft.bismapp.R;
import bismsoft.bismapp.Url.Constant;

public class SplashScreen extends AppCompatActivity  implements BaseParser{

    private SharedPreferences preferences;
    public
    static  Activity instance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        preferences = getSharedPreferences("EStore", Context.MODE_PRIVATE);
        instance = this;
        RelativeLayout layout=findViewById(R.id.layout);
        AlphaAnimation animation=new AlphaAnimation(0,1);
        animation.setDuration(3000);
        layout.setAnimation(animation);

//        if(isNetworkAvailable())
//        {
//           new Handler().postDelayed(new Runnable() {
//               @Override
//               public void run() {
//                 //  startActivity(new Intent(SplashScreen.this, NewHome.class));
//                   finish();
//               }
//           },3000);
//        }
//        else{
//            show_Dialog();
//        }
//        compareTime("01:00","23:00");
//
//
    }

    public static void compareTime(String startTimeStr, String endTimeStr) {

        Pattern p = Pattern.compile("^([0-2][0-3]):([0-5][0-9])$"); //Regex is used to validate time format (HH:MM:SS)

        int hhS = 0;
        int mmS = 0;

        int hhE = 0;
        int mmE = 0;

        Matcher m = null;

        m = p.matcher(startTimeStr);
        if (m.matches()) {
            String hhStr = m.group(1);
            String mmStr = m.group(2);

            hhS = Integer.parseInt(hhStr);
            mmS = Integer.parseInt(mmStr);

        }

        else {
            System.out.println("Invalid start time");
            //  System.exit(0);
            Log.d("resultofcompsre","forst wrong");


        }



        m = p.matcher(endTimeStr);
        if (m.matches()) {
            String hhStr = m.group(1);
            String mmStr = m.group(2);

            hhE = Integer.parseInt(hhStr);
            mmE = Integer.parseInt(mmStr);

        }

        else {
            System.out.println("Invalid End time");
            Log.d("resultofcompsre","second wrong");
            // System.exit(0);

        }



        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hhS); // Start hour
        cal.set(Calendar.MINUTE, mmS); // Start Mintue


        Time startTime = new Time(cal.getTime().getTime());
        // System.out.println("your time: "+sqlTime3);

        cal.set(Calendar.HOUR_OF_DAY, hhE); // End hour
        cal.set(Calendar.MINUTE, mmE); // End Mintue


        Time endTime = new Time(cal.getTime().getTime());

        if (startTime.equals(endTime)) {
            System.out.println("Both Start time and End Time are equal");
            Log.d("resultofcompsre","both equal");
        } else if (startTime.before(endTime)) {
            System.out.println("Start time is less than end time");
            Log.d("resultofcompsre","2 is grreatrer");

        }

        else
            System.out.println("Start time is greater than end time");
        Log.d("resultofcompsre","1  is gretae");


    }

    @Override
    protected void onResume() {
        super.onResume();

        if(isNetworkAvailable())
        {
            getversionAndStoreStatus();
        }
        else{
            show_Dialog();
        }


    }

    private void getCategories() {
        HttpAsyncRequest requests = new HttpAsyncRequest(this, Constant.get_CATEGORIES_URL, HttpAsyncRequest.RequestType.POST, new CategoryParser(), listenerforcategories);



        requests.execute();

    }
    private void getversionAndStoreStatus() {
        HttpAsyncRequest requests = new HttpAsyncRequest(this, Constant.getstoreinfo, HttpAsyncRequest.RequestType.POST, this, listenerforstore);

        requests.execute();

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void show_Dialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Warning");
        dialogBuilder.setMessage("Your device has no internet connection. Please connect to internet to continue");

        dialogBuilder.setPositiveButton("Enable", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        });
        dialogBuilder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                finish();

            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }
    AsyncTaskListener listenerforcategories = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            if (result.isSuccess()) {

                if (result.getMessage().equals("true")) {

                    Constant.categoriesArrayList = (ArrayList<MenuFoodItems>) result.getData();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            startActivity(new Intent(SplashScreen.this, CategoryActivity.class));
                            finish();
                        }
                    },1000);


                } else if (result.getMessage().equals("false")) {

                }

//           SharedPreferences preferences = getSharedPreferences("EMenu", Context.MODE_PRIVATE);
//                SharedPreferences.Editor edot = preferences.edit();
//                edot.putString("userid",result.getMessage());
//                edot.putString("usertype","customer");


                //   edot.commit();


            } else {
                try {
                    //  Toast.makeText(SplashScreen.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();
                    AlertDialog.Builder builder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(SplashScreen.this, android.R.style.Theme_Material_Dialog_Alert);
                    } else {
                        builder = new AlertDialog.Builder(SplashScreen.this);
                    }
                    builder.setTitle("Warning")
                            .setMessage("Error Communicating with the Server")
                            .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    getCategories();

                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }catch (Exception e){}



            }
        }


    };
    AsyncTaskListener listenerforstore = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            if (result.isSuccess()) {


            } else {
                try {
                    //  Toast.makeText(SplashScreen.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();
                    AlertDialog.Builder builder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(SplashScreen.this, android.R.style.Theme_Material_Dialog_Alert);
                    } else {
                        builder = new AlertDialog.Builder(SplashScreen.this);
                    }
                    builder.setTitle("Warning")
                            .setMessage("Error Communicating with the Server")
                            .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    getversionAndStoreStatus();

                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }catch (Exception e){}



            }
        }


    };



    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {
            result.success(true);
            try {
               final JSONObject obj = new JSONObject(response);
                if( Integer.parseInt(obj.optString("version_number") )==Constant.currentversion)
                {
                    if(obj.optInt("store_status")==1)
                    {
                        getCategories();
                    }
                    else{
                        AlertDialog.Builder builder;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            builder = new AlertDialog.Builder(SplashScreen.this, android.R.style.Theme_Material_Dialog_Alert);
                        } else {
                            builder = new AlertDialog.Builder(SplashScreen.this);
                        }
                        builder.setTitle("Warning")
                                .setMessage("Store is closed Now")
                                .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                        finish();

                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                    }
                }
                else{

                    final  AlertDialog.Builder alertdialog = new AlertDialog.Builder(SplashScreen.this);
                    LayoutInflater inflater = getLayoutInflater();
                    final View dialogview = inflater.inflate(R.layout.version_alert_dialog,null);
                    alertdialog.setView(dialogview);
                    final Button laterupdatebtn = dialogview.findViewById(R.id.lateupdatebtn);
                    final Button nowupdatebtn = dialogview.findViewById(R.id.nowupdatebtn);
                    alertdialog.setTitle("New Version Update");
                    final AlertDialog dialog = alertdialog.create();
                    dialog.show();

                    laterupdatebtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if(obj.optInt("store_status")==1)
                            {
                                getCategories();
                            }
                            else{
                                AlertDialog.Builder builder;
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    builder = new AlertDialog.Builder(SplashScreen.this, android.R.style.Theme_Material_Dialog_Alert);
                                } else {
                                    builder = new AlertDialog.Builder(SplashScreen.this);
                                }
                                builder.setTitle("Warning")
                                        .setMessage("Store is closed Now")
                                        .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // continue with delete
                                                finish();

                                            }
                                        })
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();

                            }

                        }
                    });

                    nowupdatebtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            }
                        }
                    });

                }




                result.success(true);
            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);
            }


        }
        return result;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

}