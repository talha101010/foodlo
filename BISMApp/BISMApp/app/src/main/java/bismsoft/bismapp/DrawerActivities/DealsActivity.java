package bismsoft.bismapp.DrawerActivities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import bismsoft.bismapp.FastFood.BillModel;
import bismsoft.bismapp.FastFood.FoodItemHolder;
import bismsoft.bismapp.R;
import bismsoft.bismapp.SuperStore.CartActivity;
import bismsoft.bismapp.SuperStore.FooitemAdapter;
import bismsoft.bismapp.SuperStore.SearchActivity;
import bismsoft.bismapp.SuperStore.ViewPagerAdapter;

public class DealsActivity extends AppCompatActivity {

    private SharedPreferences appSharedPrefs;

    TextView topname;
    ListView listView;
    public  static  DealsActivity activity ;

    public static ArrayList<FoodItemHolder> foodlist;
    FooitemAdapter fooditemadapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deals);

        topname = (TextView)findViewById(R.id.topname);
        listView = (ListView)findViewById(R.id.list);
        activity  = this;

        if(getIntent().hasExtra("name"))
            topname.setText(getIntent().getStringExtra("name"));


        foodlist = ViewPagerAdapter.list;
        listView.setAdapter(fooditemadapter = new FooitemAdapter(this,foodlist ));



    }


    @Override
    protected void onResume() {
        super.onResume();
        HandleCartNumber();
        if (fooditemadapter != null)
            fooditemadapter.notifyDataSetChanged();
    }

    public  void HandleCartNumber()
    {

        ArrayList<BillModel> arrayList = null;

        appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        TextView textView  = (TextView) findViewById(R.id.itemnumber);
        LinearLayout cartlayout = (LinearLayout)findViewById(R.id.cartlayout);


        LinearLayout searchlay= (LinearLayout)findViewById(R.id.searchlayout) ;
        searchlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),SearchActivity.class));
            }
        });
        String json = appSharedPrefs.getString("cart", "");
        if (!json.equals("")) {
            arrayList = gson.fromJson(json, type);

            textView.setText(arrayList.size()+"");
            textView.setVisibility(View.VISIBLE);
            cartlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(),CartActivity.class));
                }
            });
        }
        else{
            textView.setVisibility(View.GONE);
            cartlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(), "Nothing in the Cart Yet", Toast.LENGTH_SHORT).show();
                }
            });
        }

        LinearLayout homelayout = (LinearLayout)activity.findViewById(R.id.homelayout);

        homelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
            }
        });


    }
}
