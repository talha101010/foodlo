package bismsoft.bismapp.DataProviders;

public class ImageModel {

    byte[] imagepath;

    public byte[] getImagepath() {
        return imagepath;
    }

    public void setImagepath(byte[] imagepath) {
        this.imagepath = imagepath;
    }
}
