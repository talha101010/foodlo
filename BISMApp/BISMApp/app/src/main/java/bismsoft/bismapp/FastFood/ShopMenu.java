package bismsoft.bismapp.FastFood;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import bismsoft.bismapp.Adapters.FastFoodShopsAdopter;
import bismsoft.bismapp.DrawerActivities.LoginPanel;
import bismsoft.bismapp.DrawerActivities.ShopCartActivity;
import bismsoft.bismapp.DrawerActivities.ShopSearchActivity;

import bismsoft.bismapp.DrawerActivities.SignupActivity;
import bismsoft.bismapp.DrawerActivities.SubCategoryActivity;
import bismsoft.bismapp.Http.AsyncTaskListener;
import bismsoft.bismapp.Http.HttpAsyncRequest;
import bismsoft.bismapp.Http.TaskResult;
import bismsoft.bismapp.Parser.BaseParser;
import bismsoft.bismapp.R;
import bismsoft.bismapp.Url.ConstantsforShop;

public class ShopMenu extends AppCompatActivity  implements BaseParser {

    public static ListView fooditemlistview;
    public static SharedPreferences preferences;
    public static TextView categoryname;

    public static  String limit,charges;

    public ArrayList<FoodItemHolder> fooditems;
    RecyclerView subcategoryrecyclerview;
    public static MyProgressDialog myProgressDialog;
    public static String  categoryid, sub_category_id;
    ShopMenuAdaptor menuAdapter;
    public static Activity activity;
    public static ProgressBar progressBar;
    public static TextView nomsg;
    public  static LinearLayout cartlayout;
    public  static TextView textView;
    public  static ShopItemsAdaptor fooditemadapter;
    public  static  String shopname;
    public  static  ArrayList<FoodItemHolder> foodlist;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_menu);

        init();
        activity = this;


        Intent intent = getIntent();

        if (intent.hasExtra("shopname")) {
            shopname =  intent.getStringExtra("shopname");
            categoryname.setText(intent.getStringExtra("shopname"));
            limit = intent.getStringExtra("min");
            charges = intent.getStringExtra("charges");

        }



      //  menuAdapter  =  new ShopMenuAdaptor(ShopMenu.this, ShopAdaptors.menulist);

        menuAdapter  =  new ShopMenuAdaptor(ShopMenu.this, FastFoodShopsAdopter.menulist);

        subcategoryrecyclerview.setAdapter(menuAdapter);
        // fooditemlistview.setAdapter(new FooitemAdapter(this, CategoryActivity.fooditems));
        //  getTotalCartCounter();
        //send_sub_Categoryid_to_Server();



        //getFoodItems(ShopAdaptors.menulist.get(0).getProductname());

        getFoodItems(FastFoodShopsAdopter.menulist.get(0).getProductname());


    }

    public static void HandleCartNumber() {

        ArrayList<BillModel> arrayList = null;

        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(activity);
        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        LinearLayout accountlayout = (LinearLayout)activity.findViewById(R.id.accountlayout);
        accountlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Toast.makeText(activity, preferences.getString("userid",""), Toast.LENGTH_SHORT).show();
                if(!preferences.getString("userid","").equals(""))
                    activity.  startActivity(new Intent(activity, SignupActivity.class).putExtra("update", 1));
                else
                    activity. startActivity(new Intent(activity, LoginPanel.class));
            }
        });


        LinearLayout searchlay= (LinearLayout)activity.findViewById(R.id.searchlayout) ;
        searchlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivity(new Intent(activity,ShopSearchActivity.class));
            }
        });
        String json = appSharedPrefs.getString("shopcart", "");
        if (!json.equals("")) {
            arrayList = gson.fromJson(json, type);

            textView.setText(arrayList.size() + "");
            textView.setVisibility(View.VISIBLE);
            cartlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity. startActivity(new Intent(activity, ShopCartActivity.class));
                }
            });
        } else {
            textView.setVisibility(View.GONE);
            cartlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(activity, "Nothing in the Cart Yet", Toast.LENGTH_SHORT).show();
                }
            });
        }

        LinearLayout homelayout = (LinearLayout)activity.findViewById(R.id.homelayout);

        homelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
                if(SubCategoryActivity.activity!=null)
                    SubCategoryActivity.activity.finish();
            }
        });


    }


    public static void getFoodItems(String id) {
        HttpAsyncRequest request = new HttpAsyncRequest(activity, ConstantsforShop.getProducts, HttpAsyncRequest.RequestType.POST, (BaseParser) activity, listenerr);
        request.addParam("shop_name", shopname);
        request.addParam("menu", id);

        request.execute();
        progressBar.setVisibility(View.VISIBLE);
        fooditemlistview.setVisibility(View.GONE);
        nomsg.setVisibility(View.GONE);


    }


    public static AsyncTaskListener listenerr = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            if (result.isSuccess()) {

                if (result.getMessage().equals("true")) {
                    progressBar.setVisibility(View.GONE);
                    foodlist = (ArrayList<FoodItemHolder>) result.getData();
                    fooditemadapter =    new ShopItemsAdaptor(activity, (ArrayList<FoodItemHolder>) result.getData());
                    fooditemlistview.setAdapter(fooditemadapter);
                    fooditemlistview.setVisibility(View.VISIBLE);

                } else if (result.getMessage().equals("false")) {
                    progressBar.setVisibility(View.GONE);
                    nomsg.setVisibility(View.VISIBLE);
                    // Toast.makeText(activity, "This Menu is not active yet", Toast.LENGTH_SHORT).show();
                }


            } else {

                Toast.makeText(activity, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();

            }
        }


    };

    private boolean verify() {

        if (!preferences.getString("userid", "0").equals("0")) {
            return true;
        }
        return false;
    }

    private void init() {
        progressBar = (ProgressBar) findViewById(R.id.progress);
        // gotocart = (ImageView) findViewById(R.id.cartlayout);
        fooditemlistview = (ListView) findViewById(R.id.fooditems_listview_items);
        subcategoryrecyclerview = (RecyclerView) findViewById(R.id.category_recycler);
        categoryname = (TextView) findViewById(R.id.categoryname);
        findViewById(R.id.goback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        preferences = this.getSharedPreferences("EStore", Context.MODE_PRIVATE);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        nomsg = (TextView) findViewById(R.id.nomsg);

        textView = (TextView) findViewById(R.id.itemnumber);
        cartlayout = (LinearLayout) findViewById(R.id.cartlayout);
    }




    @Override
    protected void onResume() {
        super.onResume();
        HandleCartNumber();
        if (fooditemadapter != null)
            fooditemadapter.notifyDataSetChanged();

    }
    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {

            try {
                result.success(true);
                JSONObject obj = new JSONObject(response);
                if (obj.optString("error").equals("0")) {
                    result.setMessage("true");

                    JSONArray array = obj.optJSONArray("items");
                    ArrayList<FoodItemHolder> list = new ArrayList<>(array.length());

                    if (array.length() != 0 && array!=null) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject items = array.getJSONObject(i);
                            FoodItemHolder food = new FoodItemHolder();
                            food.setImagepath(items.optString("item_image"));
                            food.setProductname(items.optString("item_name"));
                            food.setDealid(items.optString("id"));
                            food.setProductdescription(items.optString("item_unit"));
                            food.setPrice(items.optString("item_price"));
                            food.setStorename(items.optString("store_name"));
                            list.add(food);
                        }
                        result.setData(list);
                    }
                    else{
                        result.setMessage("false");


                    }

                } else if (obj.optString("error").equals("true")) {
                    result.setMessage("false");

                    Toast.makeText(activity, "Some error occured", Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);

            }


        }
        return result;



    }

    @Override
    public void onLocationChanged(Location location) {

    }
}
