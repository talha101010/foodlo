package bismsoft.bismapp.SuperStore;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.location.Location;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bismsoft.bismapp.DrawerActivities.DealsActivity;
import bismsoft.bismapp.FastFood.FoodItemHolder;
import bismsoft.bismapp.FastFood.MenuFoodItems;
import bismsoft.bismapp.FastFood.MyProgressDialog;
import bismsoft.bismapp.Http.AsyncTaskListener;
import bismsoft.bismapp.Http.HttpAsyncRequest;
import bismsoft.bismapp.Http.TaskResult;
import bismsoft.bismapp.Parser.BaseParser;
import bismsoft.bismapp.R;
import bismsoft.bismapp.Url.Constant;

public class ViewPagerAdapter extends PagerAdapter implements BaseParser {
    Activity mContext;
    public  static   ArrayList<FoodItemHolder> list;
    LayoutInflater mLayoutInflater;
    ViewPager viewPager;
    ArrayList<MenuFoodItems> menuItemslist;
    ImageView bundleimage;
    TextView bundlename,bundledescription,   name,description,id,price;
    ImageView popupimage;
    LinearLayout layout;
    LinearLayout popup;
    SharedPreferences preferences;
    private MyProgressDialog myProgressDialog;
    ProgressBar progressBar;
    String nameofselection;
    TextView nameofdeal;
    DisplayMetrics displayMetrics = new DisplayMetrics();
    int up, np;
    float ans;



    public ViewPagerAdapter(Activity context , ArrayList<MenuFoodItems> menuItemslist){

        mContext = context;
        this.menuItemslist = menuItemslist;
        preferences = mContext.getSharedPreferences("EMenu",Context.MODE_PRIVATE);


        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return menuItemslist.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }
    @Override
    public float getPageWidth(int position) {
        mContext.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        float width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        return(1.0f);
    }
    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        View v = mLayoutInflater.inflate(R.layout.row_packages, container, false);

        bundleimage =(ImageView) v.findViewById(R.id.mainimage_viewpager);

        layout  = (LinearLayout)v.findViewById(R.id.pagerlayout);
        progressBar = (ProgressBar)v.findViewById(R.id.progress);
        nameofdeal  = v.findViewById(R.id.name);






        //progressBar.setVisibility(View.VISIBLE);
        nameofdeal.setText(menuItemslist.get(position).getProductname());
        Glide.with(mContext)
                .load(menuItemslist.get(position).getImagepath())
                .dontAnimate()

                .placeholder(R.drawable.logoone)

                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);

                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(bundleimage)
        ;




        layout .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                nameofselection = menuItemslist.get(position).getProductname();

                getdeals(menuItemslist.get(position).getDealid());


            }
        });


        container.addView(v);

        return  v;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }




    private void getdeals(String id) {

        HttpAsyncRequest request = new HttpAsyncRequest(mContext, Constant.get_deal_details, HttpAsyncRequest.RequestType.POST, this, listenerfor_sub_categories);
        request.addParam("id", id);

        request.execute();
        myProgressDialog = new MyProgressDialog(mContext);
        myProgressDialog.show();
        myProgressDialog.setCanceledOnTouchOutside(false);


    }




    AsyncTaskListener listenerfor_sub_categories = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            if (result.isSuccess()) {

                if (result.getMessage().equals("true")) {
                    try{


                        myProgressDialog.dismiss();
                    }catch (Exception e){}


                } else {

                    Toast.makeText(mContext, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();
                    myProgressDialog.dismiss();

                }
            }
        }


    };

    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {

            try {
                result.success(true);
                JSONObject obj = new JSONObject(response);
                if (obj.optString("error").equals("0")) {
                    result.setMessage("true");

                    JSONArray array = obj.optJSONArray("items");
                    list = new ArrayList<>(array.length());
                    if (array.length() != 0  && array != null) {
                        for (int i = 0; i < array.length(); i++) {

                            JSONObject items = array.getJSONObject(i);
                            FoodItemHolder food = new FoodItemHolder();
                            food.setProductname(items.optString("itemname"));
                            food.setDealid(items.optString("id"));
                            food.setImagepath(items.optString("url"));
                            food.setProductdescription(items.optString("itemunit"));
                            food.setPrice(items.optString("saleprice"));
                            food.setStorename(items.optString("storename"));
                            food.setUnderlineprice(items.optString("underlineprice"));
                            food.setFavouriate(items.optString("favouriate"));
                            food.setPurchaseprice(items.optString("purchaseprice"));




                            list.add(food);
                        }
                        mContext.startActivity(new Intent(mContext, DealsActivity.class).putExtra("name",nameofselection));

                        result.setData(list);
                    }
                    else{
                        Toast.makeText(mContext, "Deal Items Not Availiable yet", Toast.LENGTH_SHORT).show();
                    }

                } else if (obj.optString("error").equals("true")) {
                    result.setMessage("false");
                }

            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);

            }


        }
        return result;
    }

    @Override
    public void onLocationChanged(Location location) {

    }


}