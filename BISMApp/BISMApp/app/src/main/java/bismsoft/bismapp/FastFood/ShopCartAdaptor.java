package bismsoft.bismapp.FastFood;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;

import java.util.ArrayList;

import bismsoft.bismapp.DrawerActivities.ShopCartActivity;
import bismsoft.bismapp.R;

public class ShopCartAdaptor extends BaseAdapter {

    private static Context context;
    private static LayoutInflater inflater;
    private final SharedPreferences preferences;
    ArrayList<BillModel> foodlist;
    ArrayList<QuantityHolder> quantityHolders;
    MyProgressDialog myProgressDialog ;

    public ShopCartAdaptor(Context mainActivity, ArrayList<BillModel> listitems) {
        // TODO Auto-generated constructor stub
        context = mainActivity;
        this.foodlist = listitems;
        preferences = context.getSharedPreferences("EStore", Context.MODE_PRIVATE);

        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return foodlist.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        ImageView foodimage;
        TextView foodname, price, quantity, size;
        Button delete, minus, plus;
        ImageView edit;
        ProgressBar progressBar ;

        public Holder(View v) {
            foodimage = (ImageView) v.findViewById(R.id.foodimage);
            foodname = (TextView) v.findViewById(R.id.ordername);
            delete = (Button) v.findViewById(R.id.delete);

            edit = (ImageView) v.findViewById(R.id.edit);
            quantity = (TextView) v.findViewById(R.id.quantity);
            price = (TextView) v.findViewById(R.id.totalbill);
            minus = (Button) v.findViewById(R.id.minus);
            plus = (Button) v.findViewById(R.id.plus);
            delete = (Button) v.findViewById(R.id.delete);
            size = (TextView) v.findViewById(R.id.size);
            progressBar = (ProgressBar)v.findViewById(R.id.progress);


        }
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final ShopCartAdaptor.Holder holder;
        View rowView;
        rowView = inflater.inflate(R.layout.row_cart, null);
        holder = new ShopCartAdaptor.Holder(rowView);
//        Glide.with(context)
//                .load(foodlist.get(position).getImagepath())
//                .centerCrop()
//                .crossFade()
//                .into(holder.foodimage);
        Glide.with(context)

                .load(foodlist.get(position).getImagepath())
                .dontAnimate()

                .placeholder(R.drawable.logoone)

                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder. progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.foodimage);

        holder.foodname.setText(foodlist.get(position).getName());


        int bill = Integer.parseInt(foodlist.get(position).getProductprice())*Integer.parseInt(foodlist.get(position).getQuantity());
        holder.size.setText("Rs "+bill);
        holder.quantity.setText(foodlist.get(position).getQuantity());
        holder.price.setText(foodlist.get(position).getUnit());



        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                foodlist.remove(position);

                Gson gson = new Gson();
                SharedPreferences   appSharedPrefs= PreferenceManager
                        .getDefaultSharedPreferences(context);
                SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
                String jsons = gson.toJson(foodlist);
                prefsEditor.putString("shopcart", jsons);
                prefsEditor.commit();

                notifyDataSetChanged();
                ShopCartActivity. calculateBill();
                ShopCartActivity.handlearrow();


            }
        });


        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int pos = position;


                int price = Integer.parseInt(foodlist.get(pos).getProductprice());
                int quantity = Integer.parseInt(foodlist.get(pos).getQuantity());


                quantity += 1;
                foodlist.get(pos).setQuantity(quantity + "");
                price *= quantity;
                foodlist.get(pos).setBillprice(price + "");


                Gson gson = new Gson();
                SharedPreferences   appSharedPrefs= PreferenceManager
                        .getDefaultSharedPreferences(context);
                SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
                String jsons = gson.toJson(foodlist);
                prefsEditor.putString("shopcart", jsons);
                prefsEditor.commit();
                notifyDataSetChanged();
                ShopCartActivity. calculateBill();


            }
        });


        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int quantity = Integer.parseInt(foodlist.get(position).getQuantity());

                if (quantity != 1) {
                    int price = Integer.parseInt(foodlist.get(position).getProductprice());
                    quantity -= 1;
                    foodlist.get(position).setQuantity(quantity + "");
                    price *= quantity;
                    foodlist.get(position).setBillprice(price + "");

                    Gson gson = new Gson();
                    SharedPreferences   appSharedPrefs= PreferenceManager
                            .getDefaultSharedPreferences(context);
                    SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
                    String jsons = gson.toJson(foodlist);
                    prefsEditor.putString("shopcart", jsons);
                    prefsEditor.commit();
                    notifyDataSetChanged();
                    ShopCartActivity.  calculateBill();
                } else {
                    // Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
                }


            }
        });


        return rowView;
    }
}
