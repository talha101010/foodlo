package hsdeveloper.beautylinks;

public class HomeDataProvider {

    private String btnNameEng,secondName;
    private  int homeIcon;

    public int getHomeIcon() {
        return homeIcon;
    }

    public String getBtnNameEng() {
        return btnNameEng;
    }

    public String getSecondName() {
        return secondName;
    }

    public HomeDataProvider(String btnNameEng, String secondName, int homeIcon) {
        this.btnNameEng =btnNameEng;
        this.homeIcon = homeIcon;
        this.secondName = secondName;

    }
}
