package hsdeveloper.beautylinks.Customers;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import hsdeveloper.beautylinks.Dashboard.DashboardHomeActivity;
import hsdeveloper.beautylinks.R;
import hsdeveloper.beautylinks.Shops.AddBusinessActivity;
import hsdeveloper.beautylinks.SingUpIn.LoginActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ProfileActivity extends AppCompatActivity {

    DatabaseReference databaseReference;
    SharedPreferences sharedPref;
    Boolean Registered;
    String email,phoneNumber;
    ArrayList<CustomersDataProvider> customerList;
    TextView customerName, customerPhoneNumber,emailAdress;
    Button addBusinessButton,dashboardButton;
    private ProgressDialog progressDialog;
    String[] types;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait....");
        progressDialog.setCanceledOnTouchOutside(false);
        views();

        types = getResources().getStringArray(R.array.home_array);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this);
  //      Registered = sharedPref.getBoolean("Registered", false);

        databaseReference = FirebaseDatabase.getInstance().getReference("customers");

        addBusinessButton.setVisibility(View.INVISIBLE);

        addBusinessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    startActivity(new Intent(ProfileActivity.this, AddBusinessActivity.class)
                            .putExtra("phone",phoneNumber)
                            .putExtra("stat","insert")
                    );
                }catch (Exception e){

                    Toast.makeText(ProfileActivity.this, e+"", Toast.LENGTH_SHORT).show();

                }

            }
        });

        dashboardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfileActivity.this, DashboardHomeActivity.class)
                .putExtra("phone",phoneNumber)
                );
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
            progressDialog.show();
            email = sharedPref.getString("Username",null);
            customerList = new ArrayList<>();

            getCustomers();
    }

    public void views(){

        customerName= findViewById(R.id.customer_name_textview);
        customerPhoneNumber= findViewById(R.id.phone_number_textview);
        emailAdress= findViewById(R.id.customer_email_textview);
        addBusinessButton = findViewById(R.id.add_business_button);
        dashboardButton = findViewById(R.id.dashboard_button);
    }

    private void getCustomers(){
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                progressDialog.dismiss();
                customerList.clear();

                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){

                    CustomersDataProvider db = dataSnapshot1.getValue(CustomersDataProvider.class);
                    customerList.add(db);
                }

                for (int i=0; i<customerList.size(); i++){

                    if (customerList.get(i).getEmail().equals(email)){

                        customerName.setText(customerList.get(i).getName());
                        customerPhoneNumber.setText(customerList.get(i).getPhoneNumber());
                        emailAdress.setText(email);
                        phoneNumber = customerList.get(i).getPhoneNumber();

                        for (int j=0; j<types.length; j++){
                            checkBusiness(phoneNumber,types[j]);

                        }




                        break;
                    }


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
             progressDialog.dismiss();
            }
        });
    }

    private void checkBusiness(final String phoneNumber,final String types){

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("businesses").child(phoneNumber);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()){

                    addBusinessButton.setVisibility(View.INVISIBLE);
                    dashboardButton.setVisibility(View.VISIBLE);
                }else {

                    addBusinessButton.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
