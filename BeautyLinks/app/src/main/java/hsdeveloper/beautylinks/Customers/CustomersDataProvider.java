package hsdeveloper.beautylinks.Customers;

public class CustomersDataProvider {


    String id,phoneNumber,email,name;

    public CustomersDataProvider() {
    }

    public CustomersDataProvider(String id, String phoneNumber, String email, String name) {
        this.id = id;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }
}
