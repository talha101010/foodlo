package hsdeveloper.beautylinks.SingUpIn;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.SignInMethodQueryResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hbb20.CountryCodePicker;

import hsdeveloper.beautylinks.Customers.CustomersDataProvider;
import hsdeveloper.beautylinks.R;

public class SignUpActivity extends AppCompatActivity {

    private RelativeLayout rlayout;
    private Animation animation;
    private Menu menu;
    CountryCodePicker countryCodePicker;
    String countryCode;
    Button signUpButton;
    EditText noEt,emailEt,nameEt,passwordEt,passworREt;
    private FirebaseAuth firebaseAuth;
    DatabaseReference databaseReference;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        views();

        countryCodePicker = findViewById(R.id.crp);
        signUpButton = findViewById(R.id.btnSingUp);

        firebaseAuth = FirebaseAuth.getInstance();

        databaseReference = FirebaseDatabase.getInstance().getReference("customers");


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        rlayout     = findViewById(R.id.rlayout);
        animation   = AnimationUtils.loadAnimation(this,R.anim.uptodown);
        rlayout.setAnimation(animation);


        countryCodePicker.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                countryCode=countryCodePicker.getSelectedCountryCode();

            }
        });

        noEt = findViewById(R.id.etNo);


        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (noEt.equals("")  || noEt.getText().toString().length()<5){

                    noEt.setError("Invalid Number");
                }else if (emailEt.getText().toString().equals("")){

                    emailEt.setError("Put your Email");
                }else if (nameEt.getText().toString().equals("")){

                    nameEt.setError("Put your Name");

                }else if (passwordEt.getText().toString().equals("") ||
                        passwordEt.getText().toString().length()<4){

                    passwordEt.setError("Your password is so weak");
                }else if (passworREt.getText().toString().equals("")){

                    passworREt.setError("Put your re-type password");

                }



                else {

                    if (passwordEt.getText().toString().equals(passworREt.getText().toString())){
                        checkPhoneNumber(countryCode+noEt.getText().toString());
                    }else {

                        passworREt.setError("Password does not match");
                    }


                }

                Toast.makeText(SignUpActivity.this, countryCode+"", Toast.LENGTH_SHORT).show();

            }
        });



//        signUpButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (noEt.equals("")  || noEt.getText().toString().length()<5){
//
//                    noEt.setError("Invalid Number");
//                }else if (emailEt.getText().toString().equals("")){
//
//                       emailEt.setError("Put your Email");
//                }else if (nameEt.getText().toString().equals("")){
//
//                       nameEt.setError("Put your Name");
//
//                }else if (passwordEt.getText().toString().equals("") ||
//                passwordEt.getText().toString().length()<4){
//
//                    passwordEt.setError("Your password is so weak");
//                }else if (passworREt.getText().toString().equals("")){
//
//                    passworREt.setError("Put your re-type password");
//
//                }
//
//
//
//                else {
//
//                    if (passwordEt.getText().toString().equals(passworREt.getText().toString())){
//
//                        firebaseAuth.fetchSignInMethodsForEmail(emailEt.getText().toString()).addOnCompleteListener(new OnCompleteListener<SignInMethodQueryResult>() {
//                            @Override
//                            public void onComplete(@NonNull Task<SignInMethodQueryResult> task) {
//
//                                if (task.isSuccessful()){
//
//                                    if (task.getResult().getSignInMethods().isEmpty()){
//
//                                        checkPhoneNumber(countryCode+noEt.getText().toString());
//
//
//
//
//                                    }else {
//
//                                        emailEt.setError("Email Already Exist");
//                                    }
//
//
//                                }else {
//
//                                    String error = task.getException().getMessage();
//                                    Toast.makeText(SignUpActivity.this, error, Toast.LENGTH_SHORT).show();
//                                }
//
//                            }
//                        });
//
//
//                    }else {
//
//                        passworREt.setError("Password does not match");
//                    }
//
//
//                }
//
//                Toast.makeText(SignUpActivity.this, countryCode+"", Toast.LENGTH_SHORT).show();
//
//            }
//        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void views(){

        emailEt = findViewById(R.id.etEmail);
        nameEt = findViewById(R.id.etUsername);
        passwordEt = findViewById(R.id.etPassword);
        passworREt = findViewById(R.id.etRePassword);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait....");
        progressDialog.setCanceledOnTouchOutside(false);
    }

    public void checkPhoneNumber(final String phoneNumber){
        progressDialog.show();
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.child(phoneNumber).exists()){
                    progressDialog.dismiss();
                    noEt.setError("Already Exists");
                    Toast.makeText(SignUpActivity.this, "This phone number already exist", Toast.LENGTH_SHORT).show();

                }else {
                    progressDialog.dismiss();
                    firebaseAuth.createUserWithEmailAndPassword(emailEt.getText().toString(),
                            passwordEt.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){

                                firebaseAuth.getCurrentUser().sendEmailVerification()
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task1) {
                                                if (task1.isSuccessful()){
                                                    DatabaseReference databaseReference =  FirebaseDatabase.getInstance().getReference("customers");
                                                    String cid = databaseReference.push().getKey();
                                                    CustomersDataProvider dp = new CustomersDataProvider(cid,"+"+countryCode+noEt.getText().toString(),emailEt.getText().toString(),nameEt.getText().toString());
                                                    databaseReference.child(cid).setValue(dp).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task2) {
                                                            progressDialog.dismiss();
                                                            if (task2.isSuccessful()){
                                                                Toast.makeText(SignUpActivity.this, "Registered Successfully.Please check your email for verification", Toast.LENGTH_SHORT).show();
                                                                finish();
                                                            }else{
                                                                Toast.makeText(SignUpActivity.this, task2.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                                            }
                                                        }
                                                    });

                                                }else{
                                                    progressDialog.dismiss();
                                                    Toast.makeText(SignUpActivity.this, task1.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });

                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(SignUpActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(SignUpActivity.this, e.getLocalizedMessage()+"", Toast.LENGTH_SHORT).show();
                        }
                    });


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }



//    public void checkPhoneNumber(final String phoneNumber){
//
//        databaseReference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                if (dataSnapshot.child(phoneNumber).exists()){
//
//                    noEt.setError("Already Exists");
//
//                }else {
//
//                    startActivity(new Intent(SignUpActivity.this,VerifyPhoneActivity.class)
//                            .putExtra("no",phoneNumber)
//                            .putExtra("email",emailEt.getText().toString())
//                            .putExtra("name",nameEt.getText().toString())
//                            .putExtra("pass",passwordEt.getText().toString())
//                            .putExtra("Rpass",passworREt.getText().toString())
//
//                    );
//                    finish();
//                }
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//    }
}
