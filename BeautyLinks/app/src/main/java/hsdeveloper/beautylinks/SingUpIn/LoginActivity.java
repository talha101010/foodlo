package hsdeveloper.beautylinks.SingUpIn;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import hsdeveloper.beautylinks.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageButton btRegister;
    private ImageView circle1;
    TextView tvLogin,tvForgot;
    private FirebaseAuth firebaseAuth;
    EditText etUserName, etPassword;
    private Button btnLogin;
    Boolean Registered;
     SharedPreferences sharedPref;
     private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btRegister  = findViewById(R.id.btRegister);
        tvLogin     = findViewById(R.id.tvLogin);
        circle1     = findViewById(R.id.circle1);
        tvForgot = findViewById(R.id.tvForgot);

        etUserName= findViewById(R.id.etUsername);
        etPassword= findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btLogin);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Checking Credentials....");
        progressDialog.setCanceledOnTouchOutside(false);

        firebaseAuth = FirebaseAuth.getInstance();
        sharedPref = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
        Registered = sharedPref.getBoolean("Registered", false);



       // FirebaseUser user = firebaseAuth.getCurrentUser();

        btRegister.setOnClickListener(this);



        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etUserName.getText().toString().equals("")){

                    etUserName.setError("Put Email Address");
                }else if (etPassword.getText().toString().equals("")){

                    etPassword.setError("Put Password");
                }else {
                    checkSignIn();

//                    if (!Registered)
//                    {
//                        checkSignIn();
//                    }else {
//
//                        Toast.makeText(LoginActivity.this, "Successfully", Toast.LENGTH_SHORT).show();
//
//                    }




                }
            }
        });

        tvForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,ForgotPassword.class));
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {
        if (v==btRegister){
            Intent a = new Intent(LoginActivity.this, SignUpActivity.class);
            Pair[] pairs = new Pair[1];
            pairs[0] = new Pair<View,String> (tvLogin,"login");
            ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation(LoginActivity.this,pairs);
            startActivity(a,activityOptions.toBundle());
        }
    }

    private void checkSignIn(){
        progressDialog.show();
        firebaseAuth.signInWithEmailAndPassword(etUserName.getText().toString().trim(),etPassword.getText().toString().trim())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            if (firebaseAuth.getCurrentUser().isEmailVerified()){
                                progressDialog.dismiss();
                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.putBoolean("Registered", true);
                                editor.putString("Username", etUserName.getText().toString());
                                editor.putString("Password", etPassword.getText().toString());
                                editor.apply();
                                Toast.makeText(LoginActivity.this, "Login successfully", Toast.LENGTH_SHORT).show();
                                finish();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(LoginActivity.this, "Please verify your email first", Toast.LENGTH_SHORT).show();
                            }

                        }else {
                            progressDialog.dismiss();
                            Toast.makeText(LoginActivity.this, "authentication failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


}
