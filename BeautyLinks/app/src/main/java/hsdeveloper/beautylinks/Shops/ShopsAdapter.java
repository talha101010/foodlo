package hsdeveloper.beautylinks.Shops;

import android.content.Context;
import android.content.Intent;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import hsdeveloper.beautylinks.HomeDataProvider;
import hsdeveloper.beautylinks.R;

public class ShopsAdapter extends RecyclerView.Adapter<ShopsAdapter.ViewHolder> {

    private Context mcontext;
    private ArrayList<UploadImage> mList;
    private ShopsAdapter.OnItemClickListener mListener;
    DatabaseReference databaseReference;

    public interface OnItemClickListener{
        void  onItemClick(int position);
        void  onButtonChange(int position);
    }

    public void setOnItemClickListener(ShopsAdapter.OnItemClickListener listener){
        mListener = listener;
    }

    public ShopsAdapter(Context context, ArrayList<UploadImage> list){

        this.mcontext = context;
        this.mList = list;


    }


    @Override
    public ShopsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mcontext);
        View view = inflater.inflate(R.layout.shops_customise,parent,false);
        ShopsAdapter.ViewHolder viewHolder = new ShopsAdapter.ViewHolder(view,mListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ShopsAdapter.ViewHolder holder, int position) {

        final UploadImage uploadImage  = mList.get(position);

        TextView nameEng = holder.nameEng;
        ImageView homeicon = holder.icons;
        CardView cardView = holder.cardView;

        nameEng.setText(uploadImage.getBusinessName());
       // homeicon.setImageResource(homeDataProvider.getHomeIcon());

        Picasso.with(mcontext)
                .load(uploadImage.getImageUri())
                //.placeholder(R.drawable.placeholder)
                .fit()
                .centerCrop()
                .into(homeicon);

        homeicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Toast.makeText(mcontext, "clicked", Toast.LENGTH_SHORT).show();

                try {


                    mcontext.startActivity(new Intent(mcontext, ShopsDetail.class)
                    .putExtra("imageurl",uploadImage.getImageUri())
                            .putExtra("name",uploadImage.getBusinessName())
                            .putExtra("phone",uploadImage.getPhoneNumber())
                            .putExtra("address",uploadImage.getAddress())
                            .putExtra("key",uploadImage.getKey())
                            .putExtra("lat",uploadImage.getLat())
                            .putExtra("lng",uploadImage.getLng())

                    );

                }catch (Exception e){

                    Toast.makeText(mcontext, e+"", Toast.LENGTH_SHORT).show();

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView nameEng,seondName;
        public ImageView icons;
        public CardView cardView;


        public ViewHolder(View itemView, final ShopsAdapter.OnItemClickListener listener) {
            super(itemView);

            cardView =  itemView.findViewById(R.id.cardview);
            nameEng =itemView.findViewById(R.id.homeBtnNameEng);
            icons = itemView.findViewById(R.id.homeicon);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listener!=null){

                        int position  = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }

                }
            });
        }


    }

    public void setFilter(List<UploadImage> listitems1){
        mList = new ArrayList<>();
        mList.addAll(listitems1);
        notifyDataSetChanged();
    }

}
