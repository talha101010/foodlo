package hsdeveloper.beautylinks.Shops;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import hsdeveloper.beautylinks.R;

public class ReviewsActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<ReviewsDataProvider> reviewsList;
    ReviewsAdapter adapter;
    private Button ratingbtn;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviews);

       progressDialog = new ProgressDialog(this);
       progressDialog.setMessage("Please wait....");
       progressDialog.setCanceledOnTouchOutside(false);
       recyclerView = findViewById(R.id.recyclerview);
       ratingbtn = findViewById(R.id.ratingbtn);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this,1));

        reviewsList = new ArrayList<>();

        if (getIntent().getStringExtra("stat").equals("admin")){
              ratingbtn.setVisibility(View.GONE);
        }else{
            ratingbtn.setVisibility(View.VISIBLE);
        }


        ratingbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               ratingDialog();
            }
        });

        getReviews();
    }

    private void getReviews(){
        progressDialog.show();
        Query query = FirebaseDatabase.getInstance().getReference().child("Reviews")
                .orderByChild("bid").equalTo(getIntent().getStringExtra("key"));
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                progressDialog.dismiss();
                if (adapter !=null){
                    reviewsList.clear();
                }
               for (DataSnapshot snapshot : dataSnapshot.getChildren()){

                   ReviewsDataProvider listItems = new ReviewsDataProvider(
                           snapshot.child("cname").getValue().toString(),
                        snapshot.child("views").getValue().toString(),
                        snapshot.child("rating").getValue().toString()
                   );
                   reviewsList.add(listItems);
               }
                adapter = new ReviewsAdapter(ReviewsActivity.this,reviewsList);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressDialog.dismiss();
                Toast.makeText(ReviewsActivity.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void ratingDialog(){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.rating_dialog);

        final RatingBar ratingBar =  dialog.findViewById(R.id.ratingbar);
        final EditText experience = dialog.findViewById(R.id.experience);

        Button dialogButton =  dialog.findViewById(R.id.yes);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (experience.getText().length()==0){
                    experience.setError("This field is required");
                }else{
                    String s = String.valueOf(ratingBar.getRating());
                    insertReviews(experience.getText().toString(),s,dialog);
                }
            }
        });

        Button dialogButton1 =  dialog.findViewById(R.id.no);
        dialogButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });



        dialog.show();
    }

    private void insertReviews(final String views, final String rating, final Dialog dialog){
        progressDialog.show();
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        final String email = sharedPref.getString("Username",null);
        Query query1 = FirebaseDatabase.getInstance().getReference().child("customers")
                .orderByChild("email").equalTo(email);
        query1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {
                String name = null;
                for (DataSnapshot snapshot1 : dataSnapshot1.getChildren()){
                    name = snapshot1.child("name").getValue().toString();
                }

                AddReviews addReviews = new AddReviews();
                addReviews.setViews(views);
                addReviews.setRating(rating);
                addReviews.setCname(name);
                addReviews.setBid(getIntent().getStringExtra("key"));
                FirebaseDatabase.getInstance().getReference().child("Reviews").push().setValue(addReviews)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                progressDialog.dismiss();
                                dialog.dismiss();
                                Toast.makeText(ReviewsActivity.this, "Thanks for your time", Toast.LENGTH_SHORT).show();

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressDialog.dismiss();
                        Toast.makeText(ReviewsActivity.this, "server error", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
