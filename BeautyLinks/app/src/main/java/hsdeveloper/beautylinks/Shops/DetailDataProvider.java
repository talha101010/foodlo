package hsdeveloper.beautylinks.Shops;

public class DetailDataProvider {

    String heading,amount,time,description;


    public DetailDataProvider() {
    }

    public DetailDataProvider(String heading, String amount, String time, String description) {
        this.heading = heading;
        this.amount = amount;
        this.time = time;
        this.description = description;
    }

    public String getHeading() {
        return heading;
    }

    public String getAmount() {
        return amount;
    }

    public String getTime() {
        return time;
    }

    public String getDescription() {
        return description;
    }
}
