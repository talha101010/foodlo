package hsdeveloper.beautylinks.Shops;

public class ReviewsDataProvider {

    String name,reviews,rating;

    public ReviewsDataProvider(String name, String reviews, String rating) {
        this.name = name;
        this.reviews = reviews;
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public String getReviews() {
        return reviews;
    }

    public String getRating() {
        return rating;
    }
}
