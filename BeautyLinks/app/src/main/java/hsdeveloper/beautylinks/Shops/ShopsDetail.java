package hsdeveloper.beautylinks.Shops;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.FileReader;
import java.util.ArrayList;

import hsdeveloper.beautylinks.Dashboard.ServicesDataProvider;
import hsdeveloper.beautylinks.Gallery.GalleryActivity;
import hsdeveloper.beautylinks.R;

public class ShopsDetail extends AppCompatActivity {

    RecyclerView recyclerView;
    DetailDataProvider detailDataProvider;
    ArrayList<ServicesDataProvider> servicesList;
    Button openGalleryButton,showmap;
    TextView reviewsButton,totalrating,address;
    ImageView detailImageview;
    Intent intent;
    String imageUrl,businessName;
    public static String phoneNumber;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shops_detail);

        views();

        openGalleryButton = findViewById(R.id.open_gallery_button);
        showmap = findViewById(R.id.showmap);

        intent = getIntent();
        imageUrl = intent.getStringExtra("imageurl");
        businessName = intent.getStringExtra("name");
        phoneNumber = intent.getStringExtra("phone");
        address.setText(intent.getStringExtra("address"));


        Toolbar toolbar= findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(businessName);

        if (getSupportActionBar()!=null){

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        //recyclerviw();

        openGalleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ShopsDetail.this, GalleryActivity.class));
            }
        });

        reviewsButton = findViewById(R.id.reviews_button);
        reviewsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ShopsDetail.this,ReviewsActivity.class);
                intent.putExtra("key",getIntent().getStringExtra("key"));
                intent.putExtra("stat","user");
                startActivity(intent);
            }
        });

        Picasso.with(this)
                .load(imageUrl)
                //.placeholder(R.drawable.placeholder)
                .fit()
                .centerCrop()
                .into(detailImageview);

        servicesList = new ArrayList<>();

        recyclerView = findViewById(R.id.recyclerview);

        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        recyclerView.setNestedScrollingEnabled(false);
        try {
            loadServices(phoneNumber);
        }catch (Exception e){

            Toast.makeText(this, e+"", Toast.LENGTH_SHORT).show();

        }
          getTotalRating();

        showmap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ShopsDetail.this,Location.class);
                intent.putExtra("lat",getIntent().getStringExtra("lat"));
                intent.putExtra("lng",getIntent().getStringExtra("lng"));
                startActivity(intent);
            }
        });

    }


    private void views(){

        detailImageview = findViewById(R.id.detail_imageview);
        totalrating = findViewById(R.id.totalrating);
        address = findViewById(R.id.address);
    }

    private  void loadServices(String phoneNumber){

        databaseReference = FirebaseDatabase.getInstance().getReference("businesses").child(phoneNumber).child("services");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                servicesList.clear();
                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){

                    ServicesDataProvider dp = dataSnapshot1.getValue(ServicesDataProvider.class);
                    servicesList.add(dp);
                }

                DetailAdapter adapter = new DetailAdapter(ShopsDetail.this,servicesList);
                recyclerView.setAdapter(adapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void getTotalRating(){

        Query query = FirebaseDatabase.getInstance().getReference().child("Reviews")
                .orderByChild("bid").equalTo(getIntent().getStringExtra("key"));
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    float getrating = 0;
                    int number = 0;
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                        getrating += Float.parseFloat(snapshot.child("rating").getValue().toString());
                        number++;
                    }
                    float finalrating = getrating/number;
                    totalrating.setText(finalrating+"");
                }else{
                    totalrating.setText("0.0");
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(ShopsDetail.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        });


    }
}
