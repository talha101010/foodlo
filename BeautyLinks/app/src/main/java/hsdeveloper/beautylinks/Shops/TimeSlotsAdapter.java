package hsdeveloper.beautylinks.Shops;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import hsdeveloper.beautylinks.R;

public class TimeSlotsAdapter extends RecyclerView.Adapter<TimeSlotsAdapter.ViewHolder> {
    private List<TimeSlotsListitems> list;
    private Context context;

    public TimeSlotsAdapter(List<TimeSlotsListitems> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.timeslots_items,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final TimeSlotsListitems listitems = list.get(position);
        FirebaseDatabase.getInstance().getReference().child("BookingTime").child(DetailAdapter.date1).child(listitems.getTimeslot())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()){
                            holder.status.setText("booked");
                            holder.textView.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
                            holder.textView.setTextColor(context.getResources().getColor(R.color.white));
                            holder.textView.setText(listitems.getTimeslot());
                        }else{
                            holder.textView.setText(listitems.getTimeslot());
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.status.getText().equals("booked")){
                    Toast.makeText(context, "This slot already booked", Toast.LENGTH_SHORT).show();
                }else{
                    addBooking(DetailAdapter.serviceid1,DetailAdapter.email1,ShopsDetail.phoneNumber,DetailAdapter.date1,listitems.getTimeslot(),DetailAdapter.dialog1);
                }

            }
        });
    }

    public void addBooking(String serviceId, String email, String phone, final String date, final String timeslot, final Dialog dialog){
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please Wait.....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        addBooking addbooking = new addBooking();
        addbooking.setServiceid(serviceId);
        addbooking.setEmail(email);
        addbooking.setPhonenumber(phone);
        addbooking.setAppointdate(date);
        addbooking.setStatus("Complete");
        addbooking.setTimeslot(timeslot);
        FirebaseDatabase.getInstance().getReference().child("Booking").push().setValue(addbooking)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                      BookingTime bookingTime = new BookingTime();
                      bookingTime.setStatus("true");
                      FirebaseDatabase.getInstance().getReference().child("BookingTime").child(date).child(timeslot).setValue(bookingTime)
                              .addOnSuccessListener(new OnSuccessListener<Void>() {
                                  @Override
                                  public void onSuccess(Void aVoid) {
                                      progressDialog.dismiss();
                                      dialog.dismiss();
                                      Toast.makeText(context, "Booked Successfully", Toast.LENGTH_SHORT).show();
                                  }
                              });
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressDialog.dismiss();
                Toast.makeText(context, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        LinearLayout linearLayout;
        TextView textView,status;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            linearLayout = itemView.findViewById(R.id.linearlayout);
            textView = itemView.findViewById(R.id.timeslots);
            status = itemView.findViewById(R.id.status);
        }
    }
}
