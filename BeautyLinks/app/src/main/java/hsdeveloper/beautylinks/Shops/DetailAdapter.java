package hsdeveloper.beautylinks.Shops;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import hsdeveloper.beautylinks.Dashboard.ServicesDataProvider;
import hsdeveloper.beautylinks.R;
import hsdeveloper.beautylinks.SingUpIn.LoginActivity;

public class DetailAdapter extends RecyclerView.Adapter<DetailAdapter.ViewHolder> {

    private Context mcontext;
    private ArrayList<ServicesDataProvider> mList;
    private DetailAdapter.OnItemClickListener mListener;
    private RecyclerView.Adapter adapter;
    public static String serviceid1,email1,date1;
    public static Dialog dialog1;

    public interface OnItemClickListener{
        void  onItemClick(int position);
        void  onButtonChange(int position);
    }

    public void setOnItemClickListener(DetailAdapter.OnItemClickListener listener){
        mListener = listener;
    }

    public DetailAdapter(Context context, ArrayList<ServicesDataProvider> list){

        this.mcontext = context;
        this.mList = list;


    }


    @Override
    public DetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mcontext);
        View view = inflater.inflate(R.layout.detail_customise_list,parent,false);
        DetailAdapter.ViewHolder viewHolder = new DetailAdapter.ViewHolder(view,mListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final DetailAdapter.ViewHolder holder, int position) {

        final ServicesDataProvider homeDataProvider  = mList.get(position);

        TextView headingTv = holder.headingTextview;
        TextView amountTv = holder.AmountTextview;
        TextView timeTv = holder.TimeTextview;
        TextView descriptionTv = holder.DescriptionTextview;
       if (FirebaseAuth.getInstance().getCurrentUser()!=null){
           SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mcontext);
           final String email = sharedPref.getString("Username",null);
           Query query = FirebaseDatabase.getInstance().getReference().child("Booking").orderByChild("email")
                   .equalTo(email);
           query.addValueEventListener(new ValueEventListener() {
               @Override
               public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                   if (dataSnapshot.exists()){
                       for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                        if (snapshot.child("serviceid").getValue().toString().equals(homeDataProvider.getsId())){
                            if (snapshot.child("status").getValue().toString().equals("Pending")){
                                holder.OppointmentButton.setText("Appointment Pending");
                                holder.appslot.setText(snapshot.child("timeslot").getValue().toString());
                                holder.date.setText(snapshot.child("appointdate").getValue().toString());
                                holder.appid.setText(snapshot.getKey().toString());
                                holder.OppointmentButton.setBackgroundColor(mcontext.getResources().getColor(R.color.colorPrimary));
                            }else if (snapshot.child("status").getValue().toString().equals("Assign")){
                                holder.OppointmentButton.setText("Accept Now");
                                holder.appslot.setText(snapshot.child("timeslot").getValue().toString());
                                holder.date.setText(snapshot.child("appointdate").getValue().toString());
                                holder.appid.setText(snapshot.getKey().toString());
                                holder.OppointmentButton.setBackgroundColor(mcontext.getResources().getColor(R.color.colorPrimary));
                            }else if (snapshot.child("status").getValue().toString().equals("Complete")){
                                holder.OppointmentButton.setText("Check Appointment");
                                holder.appslot.setText(snapshot.child("timeslot").getValue().toString());
                                holder.date.setText(snapshot.child("appointdate").getValue().toString());
                                holder.appid.setText(snapshot.getKey().toString());
                                holder.OppointmentButton.setBackgroundColor(mcontext.getResources().getColor(R.color.colorPrimary));
                            }
//                            holder.OppointmentButton.setText("Booked");
//
//                            holder.OppointmentButton.setClickable(false);
                        }
                       }


                   }
               }

               @Override
               public void onCancelled(@NonNull DatabaseError databaseError) {

               }
           });
       }


        headingTv.setText(homeDataProvider.getsTitle());
        amountTv.setText(homeDataProvider.getsPrice());
        timeTv.setText("/"+homeDataProvider.getsTime());
        descriptionTv.setText(homeDataProvider.getsDescription());

        holder.OppointmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (FirebaseAuth.getInstance().getCurrentUser()!=null && FirebaseAuth.getInstance().getCurrentUser().isEmailVerified()){


                         if (holder.OppointmentButton.getText().equals("book")){

                             final Calendar myCalendar = Calendar.getInstance();
                             final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                                 @Override
                                 public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                       int dayOfMonth) {
                                     // TODO Auto-generated method stub
                                     myCalendar.set(Calendar.YEAR, year);
                                     myCalendar.set(Calendar.MONTH, monthOfYear);
                                     myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                     String myFormat = "MM-dd-yyyy";
                                     SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                     String selecteddate = sdf.format(myCalendar.getTime());
                                     showdialog(homeDataProvider.getsId(),holder.OppointmentButton,selecteddate);
                                 }

                             };

                             new DatePickerDialog(mcontext, date, myCalendar
                                     .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                     myCalendar.get(Calendar.DAY_OF_MONTH)).show();

                    }else if (holder.OppointmentButton.getText().equals("Appointment Pending")){
                        Toast.makeText(mcontext, "Appoint time not assigned to you yet please wait", Toast.LENGTH_SHORT).show();
                    }else if (holder.OppointmentButton.getText().equals("Accept Now")){
                          appointDialog(holder.appslot.getText().toString(),holder.appid.getText().toString(),"accept",holder.date.getText().toString());
                    }else if (holder.OppointmentButton.getText().equals("Check Appointment")){
                        appointDialog(holder.appslot.getText().toString(),holder.appid.getText().toString(),"check",holder.date.getText().toString());
                    }

                }else{
                    mcontext.startActivity(new Intent(mcontext,LoginActivity.class));
                }

            }
        });


    }

    public void appointDialog(String appointtext, final String id, String status,String date){
        final Dialog dialog = new Dialog(mcontext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
       // dialog.setCancelable(false);
        dialog.setContentView(R.layout.slotdialog);

        TextView text =  dialog.findViewById(R.id.appointtext);
        text.setText(date+" at "+appointtext);

        Button dialogButton =  dialog.findViewById(R.id.accept);
        if (status.equals("check")){
            dialogButton.setVisibility(View.GONE);
        }
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Booking").child(id);
               databaseReference.child("status").setValue("Complete");
               Toast.makeText(mcontext, "You accept the time slot", Toast.LENGTH_SHORT).show();
               dialog.dismiss();
            }
        });



        dialog.show();
    }

    public void showdialog(final String serviceid, final Button btn, final String date){

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mcontext);
        final String email = sharedPref.getString("Username",null);
         serviceid1 = serviceid;
         email1 = email;
         date1 = date;
        final Dialog dialog = new Dialog(mcontext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        dialog1 = dialog;
          TextView text =  dialog.findViewById(R.id.offlinetext);
        final ProgressBar progressBar = dialog.findViewById(R.id.progressbar);
          final RecyclerView recyclerView = dialog.findViewById(R.id.recyclerview);
          final List<TimeSlotsListitems> list1 = new ArrayList<>();
          recyclerView.setHasFixedSize(true);
          recyclerView.setLayoutManager(new LinearLayoutManager(mcontext));
          if (adapter != null){
              list1.clear();
          }
          FirebaseDatabase.getInstance().getReference().child("TimeSlots")
                  .addValueEventListener(new ValueEventListener() {
                      @Override
                      public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                          progressBar.setVisibility(View.GONE);
                          if (dataSnapshot.exists()){
                              for (int i=1;i<=dataSnapshot.getChildrenCount();i++){
                                  TimeSlotsListitems listitems = new TimeSlotsListitems(dataSnapshot.child(i+"").getValue().toString());
                                  list1.add(listitems);
                              }
                              adapter = new TimeSlotsAdapter(list1,mcontext);
                              recyclerView.setAdapter(adapter);
                          }
                      }

                      @Override
                      public void onCancelled(@NonNull DatabaseError databaseError) {
                          progressBar.setVisibility(View.GONE);
                          Toast.makeText(mcontext, databaseError.getMessage()+"", Toast.LENGTH_SHORT).show();
                      }
                  });
        //  text.setText("Are you sure you want to get your appointment on "+date+" ?");

        Button dialogButton =  dialog.findViewById(R.id.yes);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addBooking(serviceid,email,ShopsDetail.phoneNumber,dialog,btn,date);
            }
        });

        Button dialogButton1 =  dialog.findViewById(R.id.no);
        dialogButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });



        dialog.show();
    }

    public void addBooking(String serviceId, String email, String phone, final Dialog dialog, final Button button,String date){
        final ProgressDialog progressDialog = new ProgressDialog(mcontext);
        progressDialog.setMessage("Please Wait.....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        addBooking addbooking = new addBooking();
        addbooking.setServiceid(serviceId);
        addbooking.setEmail(email);
        addbooking.setPhonenumber(phone);
        addbooking.setAppointdate(date);
        addbooking.setStatus("Pending");
        addbooking.setTimeslot("null");
        FirebaseDatabase.getInstance().getReference().child("Booking").push().setValue(addbooking)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        progressDialog.dismiss();
                        dialog.dismiss();
                        button.setText("Booked");
                        button.setBackgroundColor(mcontext.getResources().getColor(R.color.colorPrimary));
                        button.setClickable(false);
                        Toast.makeText(mcontext, "Booked Successfully", Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressDialog.dismiss();
                Toast.makeText(mcontext, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView headingTextview, AmountTextview, TimeTextview,DescriptionTextview,appid,appslot,date;
        Button OppointmentButton;



        public ViewHolder(View itemView, final DetailAdapter.OnItemClickListener listener) {

            super(itemView);

            headingTextview = itemView.findViewById(R.id.heading_textview);
            AmountTextview = itemView.findViewById( R.id.amount_textview);
            TimeTextview = itemView.findViewById(R.id.timetextview);
            DescriptionTextview = itemView.findViewById(R.id.descriptiontextview);
            OppointmentButton= itemView.findViewById(R.id.oppointment_button);
            appid = itemView.findViewById(R.id.appid);
            appslot = itemView.findViewById(R.id.appslot);
            date = itemView.findViewById(R.id.date);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listener!=null){

                        int position  = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }

                }
            });
        }
    }

}
