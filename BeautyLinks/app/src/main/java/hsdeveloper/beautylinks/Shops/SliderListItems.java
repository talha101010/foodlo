package hsdeveloper.beautylinks.Shops;

public class SliderListItems {
    private String name;
    private  int imageurl;

    public SliderListItems(String name, int imageurl) {
        this.name = name;
        this.imageurl = imageurl;
    }

    public String getName() {
        return name;
    }

    public int getImageurl() {
        return imageurl;
    }
}
