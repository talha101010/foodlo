package hsdeveloper.beautylinks.Shops;

public class UploadImage {

    public   String businessName, imageUri,key,phoneNumber,lat,lng,address;

    public UploadImage(String businessName, String imageUri, String key,String phoneNumber,
    String lat,String lng,String address) {
        this.businessName = businessName;
        this.imageUri = imageUri;
        this.key = key;
        this.phoneNumber = phoneNumber;
        this.lat = lat;
        this.lng = lng;
        this.address = address;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }


    public void setAddress(String address) {
        this.address = address;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }



    public String getAddress() {
        return address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getBusinessName() {
        return businessName;
    }

    public String getImageUri() {
        return imageUri;
    }

    public String getKey() {
        return key;
    }
}
