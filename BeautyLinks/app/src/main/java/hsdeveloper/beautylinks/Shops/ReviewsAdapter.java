package hsdeveloper.beautylinks.Shops;

import android.content.Context;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import hsdeveloper.beautylinks.R;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ViewHolder> {

    private Context mcontext;
    private ArrayList<ReviewsDataProvider> mList;
    private ReviewsAdapter.OnItemClickListener mListener;

    public interface OnItemClickListener{
        void  onItemClick(int position);
        void  onButtonChange(int position);
    }

    public void setOnItemClickListener(ReviewsAdapter.OnItemClickListener listener){
        mListener = listener;
    }

    public ReviewsAdapter(Context context, ArrayList<ReviewsDataProvider> list){

        this.mcontext = context;
        this.mList = list;


    }


    @Override
    public ReviewsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mcontext);
        View view = inflater.inflate(R.layout.reviews_customise,parent,false);
        ReviewsAdapter.ViewHolder viewHolder = new ReviewsAdapter.ViewHolder(view,mListener);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ReviewsAdapter.ViewHolder holder, int position) {

        ReviewsDataProvider homeDataProvider  = mList.get(position);

        TextView nameEng = holder.nameEng;
        CardView cardView = holder.cardView;
       Float rating = Float.parseFloat(homeDataProvider.rating);
        nameEng.setText(homeDataProvider.getName());
        holder.reviews.setText(homeDataProvider.getReviews());
        holder.ratingBar.setRating(rating);





    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView nameEng,reviews;
        public RatingBar ratingBar;
        public CardView cardView;


        public ViewHolder(View itemView, final ReviewsAdapter.OnItemClickListener listener) {
            super(itemView);

            cardView =  itemView.findViewById(R.id.cardview);
            nameEng =itemView.findViewById(R.id.reviews_name);
            reviews = itemView.findViewById(R.id.reviews);
            ratingBar = itemView.findViewById(R.id.reviews_rating_bar);



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listener!=null){

                        int position  = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }

                }
            });
        }
    }

}
