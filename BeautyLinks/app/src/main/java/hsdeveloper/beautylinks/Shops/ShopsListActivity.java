package hsdeveloper.beautylinks.Shops;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;

import androidx.annotation.NonNull;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;

import hsdeveloper.beautylinks.HomeAdapter;
import hsdeveloper.beautylinks.HomeDataProvider;
import hsdeveloper.beautylinks.NearbySalonActivity;
import hsdeveloper.beautylinks.R;

public class ShopsListActivity extends AppCompatActivity implements androidx.appcompat.widget.SearchView.OnQueryTextListener {

    SliderView sliderView;
    RecyclerView recyclerView;
    private List<SliderListItems> list1;
    int[] images = {R.drawable.banner,
            R.drawable.banner2,
            R.drawable.banner3
    };
    int[] shopImages ={
      R.drawable.s1,
      R.drawable.s2,
      R.drawable.s3
    };
    String[] nameArray = {"Image1","Image2","Image3"};
    SliderListItems sliderListItems;
    private ArrayList<UploadImage> shopsList;
    ShopsAdapter adapter;
    DatabaseReference databaseReference;
    Button mapButton;
    Intent intent;
    String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shops_list);

        intent = getIntent();
        type = intent.getStringExtra("type");

        sliderView = findViewById(R.id.imageSlider);

        sliderView.setIndicatorAnimation(IndicatorAnimations.SLIDE); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.CUBEINROTATIONTRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(5); //set scroll delay in seconds :
        sliderView.startAutoCycle();

        list1 = new ArrayList<>();


        mapButton = findViewById(R.id.map_button);

        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ShopsListActivity.this, NearbySalonActivity.class)
                .putExtra("type",type)
                );
            }
        });

        for (int i=0; i<images.length; i++){


            sliderListItems = new SliderListItems(nameArray[i],images[i]);
            list1.add(sliderListItems);
        }

        final SliderAdapter adapter = new SliderAdapter(ShopsListActivity.this,list1);
        sliderView.setSliderAdapter(adapter);
       // recyclerView();

        try {
            loadData(type);
        }catch (Exception e){

            Toast.makeText(this, e+"", Toast.LENGTH_SHORT).show();

        }

        shopsList = new ArrayList<>();
//
       recyclerView = findViewById(R.id.recyclerview);
       recyclerView.setHasFixedSize(true);
       recyclerView.setLayoutManager(new GridLayoutManager(this, 1));






    }

//    public void recyclerView() {
//
//        shopsList = new ArrayList<>();
//
//        recyclerView = findViewById(R.id.recyclerview);
//        recyclerView.setHasFixedSize(true);
//        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
//
//        try {
//            for (int i = 0; i < images.length; i++) {
//
//
//                String nameEng = nameArray[i];
//                int homeiconitem = shopImages[i];
//                HomeDataProvider homeDataProvider = new HomeDataProvider(nameEng, "", homeiconitem);
//                shopsList.add(homeDataProvider);
//            }
//
//            shopsAdapter = new ShopsAdapter(ShopsListActivity.this, shopsList);
//            recyclerView.setAdapter(shopsAdapter);
//        }catch (Exception e){
//
//            Toast.makeText(this, "Some Error Occured", Toast.LENGTH_SHORT).show();
//        }
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_items,menu);
        MenuItem menuitem = menu.findItem(R.id.search);
        androidx.appcompat.widget.SearchView searchView = (androidx.appcompat.widget.SearchView) MenuItemCompat.getActionView(menuitem);
        searchView.setOnQueryTextListener(this);




        return true;


    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        newText = newText.toLowerCase();
        List<UploadImage> newList = new ArrayList<>();
        for (UploadImage wordslist : shopsList){
            String name = wordslist.getBusinessName().toLowerCase();
            if (name.contains(newText)){
                newList.add(wordslist);
            }
        }
        ((ShopsAdapter) adapter).setFilter(newList);
        return true;
    }

    private void loadData(String type){
          final ProgressDialog progressDialog = new ProgressDialog(this);
          progressDialog.setMessage("Please Wait.....");
          progressDialog.setCanceledOnTouchOutside(false);
          progressDialog.show();
        Query query = FirebaseDatabase.getInstance().getReference("businesses").orderByChild("category")
                       .equalTo(type);
       // databaseReference = FirebaseDatabase.getInstance().getReference("businesses").child(type);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                progressDialog.dismiss();
                shopsList.clear();

                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
//                    UploadImage dp = dataSnapshot1.getValue(UploadImage.class);
                    UploadImage dp = new UploadImage(dataSnapshot1.child("businessName").getValue().toString(),
                            dataSnapshot1.child("imageUri").getValue().toString(),
                            dataSnapshot1.child("key").getValue().toString(),
                            dataSnapshot1.child("phoneNumber").getValue().toString(),
                            dataSnapshot1.child("lat").getValue().toString(),
                            dataSnapshot1.child("lng").getValue().toString(),
                            dataSnapshot1.child("address").getValue().toString()
                            );
                    shopsList.add(dp);
                }
                 adapter = new ShopsAdapter(ShopsListActivity.this,shopsList);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                 progressDialog.dismiss();
                Toast.makeText(ShopsListActivity.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
