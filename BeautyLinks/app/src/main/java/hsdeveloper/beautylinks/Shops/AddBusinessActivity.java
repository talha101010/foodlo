package hsdeveloper.beautylinks.Shops;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import hsdeveloper.beautylinks.R;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

public class AddBusinessActivity extends AppCompatActivity {

    private static final int PICK_IMAGE_REQUEST = 1;
    private static final int PLACE_PICKER_REQUEST = 2;
   private  EditText etNameOfBusiness,etLocation,etAltitude,etLogitude;
   private  Spinner selectCategory;
   private ImageView selectImage;
 //  private Button addButton, chooseImageButton,;

   private Button addButton, chooseImageButton,addLocationButton,updateBtn;
     LinearLayout coordinateLayout;

    ProgressBar imageProgressBar;
    String strTypeName,phoneNumber;
    String imageUri2;

    private Uri imageUri;
    private StorageReference storageReference;
    private DatabaseReference databaseReference;
    Intent intent;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_business);



        views();
        intent = getIntent();
        phoneNumber = intent.getStringExtra("phone");

        storageReference = FirebaseStorage.getInstance().getReference("categoriesimages/");
        databaseReference = FirebaseDatabase.getInstance().getReference("businesses");

        chooseImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser();
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etNameOfBusiness.getText().toString().equals("")){

                    etNameOfBusiness.setError("Put Name");
                }else if(etLocation.getText().toString().equals("")){

                    etLocation.setError("Select location");


                }else if (etAltitude.getText().toString().equals("")){

                          etAltitude.setError("Select Altitude");
                }else if (etLogitude.getText().toString().equals("")){

                    etLogitude.setError("Select Longitude");
                }




                else {

                    try {
                        uplaodimage(etNameOfBusiness.getText().toString(),selectCategory.getSelectedItem().toString());
                    }catch (Exception e){

                        Toast.makeText(AddBusinessActivity.this, e+"", Toast.LENGTH_SHORT).show();

                    }


                }
            }
        });

        addLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlacePicker.IntentBuilder builder=new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(AddBusinessActivity.this),PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                    Toast.makeText(AddBusinessActivity.this, e.getLocalizedMessage()+"", Toast.LENGTH_SHORT).show();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                    Toast.makeText(AddBusinessActivity.this, e.getLocalizedMessage()+"", Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (intent.getStringExtra("stat").equals("update")){
            getBusiness();
            updateBtn.setVisibility(View.VISIBLE);
            addButton.setVisibility(View.GONE);
            etLogitude.setVisibility(View.VISIBLE);
            etAltitude.setVisibility(View.VISIBLE);
            etLocation.setVisibility(View.VISIBLE);
        }else{
            addButton.setVisibility(View.VISIBLE);
            updateBtn.setVisibility(View.GONE);
            etLogitude.setVisibility(View.GONE);
            etAltitude.setVisibility(View.GONE);
            etLocation.setVisibility(View.GONE);
        }

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etNameOfBusiness.getText().toString().equals("")){

                    etNameOfBusiness.setError("Put Name");
                }else if(etLocation.getText().toString().equals("")){

                    etLocation.setError("Select location");


                }else if (etAltitude.getText().toString().equals("")){

                    etAltitude.setError("Select Altitude");
                }else if (etLogitude.getText().toString().equals("")){

                    etLogitude.setError("Select Longitude");
                }




                else {

                    try {

                        if (imageUri != null){
                            updateImage();
                        }else{
                            updateBusiness();
                        }

                       // uplaodimage(etNameOfBusiness.getText().toString(),selectCategory.getSelectedItem().toString());
                    }catch (Exception e){

                        Toast.makeText(AddBusinessActivity.this, e+"", Toast.LENGTH_SHORT).show();

                    }


                }
            }
        });

    }





    public void  views(){

        etNameOfBusiness = findViewById(R.id.et_name_of_business);
        etLocation = findViewById(R.id.et_location);
        etAltitude= findViewById(R.id.et_altitude);
        etLogitude= findViewById(R.id.et_longitude);
        selectCategory = findViewById(R.id.select_category_spinner);
        selectImage = findViewById(R.id.select_business_image);
        addButton = findViewById(R.id.add_business_btn);
        chooseImageButton = findViewById(R.id.choose_image_btn);
        imageProgressBar = findViewById(R.id.imageprogressbar);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Please Wait.....");
        updateBtn = findViewById(R.id.update_business_btn);
        addLocationButton = findViewById(R.id.add_location_btn);
        coordinateLayout = findViewById(R.id.coordinates_layout);

    }


    private void openFileChooser(){

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==PLACE_PICKER_REQUEST)
        {
            if(resultCode==RESULT_OK)
            {
                Place place=PlacePicker.getPlace(data,this);
                StringBuilder stringBuilder=new StringBuilder();
                String latitude=String.valueOf(place.getLatLng().latitude);
                String longitude=String.valueOf(place.getLatLng().longitude);
                stringBuilder.append("LATITUDE: "+latitude+" LONGITUDE: "+longitude);
                //Toast.makeText(getApplicationContext(),stringBuilder.toString(),Toast.LENGTH_LONG).show();

                etLocation.setVisibility(View.VISIBLE);
                coordinateLayout.setVisibility(View.VISIBLE);

                etLocation.setText(place.getAddress());
                etAltitude.setText(latitude);
                etLogitude.setText(longitude);
            }
        }

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null){

            imageUri = data.getData();
            Picasso.with(this).load(imageUri).into(selectImage);


        }
    }

    private String getFileExtention(Uri uri){

        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();

        return  mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    private void uplaodimage(final String businessName,final String category){

        if (imageUri !=null){
            progressDialog.show();
            String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
            final   StorageReference sr = storageReference.child(uid+"."+getFileExtention(imageUri));
            sr.putFile(imageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    imageProgressBar.setProgress(0);
                                }
                            },500);

                            sr.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {

                                    String imageId = databaseReference.push().getKey();



                                    imageUri2 = uri.toString();

                                    HashMap map=new HashMap();
                                    map.put("businessName",etNameOfBusiness.getText().toString());
                                    map.put("address",etLocation.getText().toString());
                                    map.put("lat",etAltitude.getText().toString());
                                    map.put("lng",etLogitude.getText().toString());
                                    map.put("key",imageId);
                                    map.put("imageUri",imageUri2);
                                    map.put("phoneNumber",phoneNumber);
                                    map.put("category",category);

                                   // UploadImage uploadImage = new UploadImage(businessName,imageUri2 ,imageId,phoneNumber,location,lat,lng
                                   // );

                                    databaseReference.child(phoneNumber).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            progressDialog.dismiss();
                                            Toast.makeText(AddBusinessActivity.this, "Upload Successfully", Toast.LENGTH_SHORT).show();
                                            finish();

                                        }
                                    });



                                }
                            });


                        }


                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(AddBusinessActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                            imageProgressBar.setProgress((int) progress);

                        }
                    });

        }else {

            Toast.makeText(this, "No file Chooses", Toast.LENGTH_SHORT).show();
        }


    }

    private void getBusiness(){
        progressDialog.show();
        databaseReference.child(phoneNumber).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                progressDialog.dismiss();
                etNameOfBusiness.setText(dataSnapshot.child("businessName").getValue().toString());
                etLocation.setText(dataSnapshot.child("address").getValue().toString());
                etAltitude.setText(dataSnapshot.child("lat").getValue().toString());
                etLogitude.setText(dataSnapshot.child("lng").getValue().toString());
                selectCategory.setSelection(((ArrayAdapter)selectCategory.getAdapter()).getPosition(dataSnapshot.child("category").getValue().toString()));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
              progressDialog.dismiss();
                Toast.makeText(AddBusinessActivity.this, databaseError.getMessage()+"", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateBusiness(){
        progressDialog.show();
        databaseReference.child(phoneNumber).child("businessName").setValue(etNameOfBusiness.getText().toString().trim());
        databaseReference.child(phoneNumber).child("address").setValue(etLocation.getText().toString().trim());
        databaseReference.child(phoneNumber).child("category").setValue(selectCategory.getSelectedItem().toString());
        databaseReference.child(phoneNumber).child("lat").setValue(etAltitude.getText().toString().trim());
        databaseReference.child(phoneNumber).child("lng").setValue(etLogitude.getText().toString().trim());
        progressDialog.dismiss();
        Toast.makeText(this, "Business data updated successfully", Toast.LENGTH_SHORT).show();
        finish();
    }

    private void updateImage(){
            progressDialog.show();
            String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
            final   StorageReference sr = storageReference.child(uid+"."+getFileExtention(imageUri));
            sr.putFile(imageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    imageProgressBar.setProgress(0);
                                }
                            },500);

                            sr.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    progressDialog.dismiss();
                                    String imageId = databaseReference.push().getKey();
                                    imageUri2 = uri.toString();

                                    databaseReference.child(phoneNumber).child("businessName").setValue(etNameOfBusiness.getText().toString().trim());
                                    databaseReference.child(phoneNumber).child("address").setValue(etLocation.getText().toString().trim());
                                    databaseReference.child(phoneNumber).child("category").setValue(selectCategory.getSelectedItem().toString());
                                    databaseReference.child(phoneNumber).child("lat").setValue(etAltitude.getText().toString().trim());
                                    databaseReference.child(phoneNumber).child("lng").setValue(etLogitude.getText().toString().trim());
                                    databaseReference.child(phoneNumber).child("imageUri").setValue(imageUri2);
                                    Toast.makeText(AddBusinessActivity.this, "Business data updated successfully", Toast.LENGTH_SHORT).show();
                                    finish();
                                }
                            });


                        }


                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(AddBusinessActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                            imageProgressBar.setProgress((int) progress);

                        }
                    });
    }

}
