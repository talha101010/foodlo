package hsdeveloper.beautylinks;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import hsdeveloper.beautylinks.Shops.UploadImage;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class NearbySalonActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener {

    private GoogleMap mMap;
    ArrayList<UploadImage> salonModelList;
    double userLat, userlng;
    LocationManager locationManager;
    double BOUND_DISTANCE=5;
    Button btn;
    Intent intent;
    String type;
    @RequiresApi(api = Build.VERSION_CODES.M)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_salon);

        btn=(Button)findViewById(R.id.btn_salon_nearby);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAllSlon(type);
            }
        });

        intent = getIntent();
        type = intent.getStringExtra("type");

        CheckPermission();
        salonModelList=new ArrayList<>();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        //getting all registered salon
        getAllSlon(type);
    }

    public void CheckPermission() {
        //check wether user has given the location permission
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }
    }

    public void getLocation() {
        try {
            //intialize locationmanager to get location updates
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    private void getAllSlon(String type) {

        //refrence to registered salon in firebase
        DatabaseReference reference= FirebaseDatabase.getInstance().getReference("businesses").child(type);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    salonModelList.clear();
                    for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren())
                    {
                        UploadImage model = new UploadImage(dataSnapshot1.child("businessName").getValue().toString(),
                                dataSnapshot1.child("imageUri").getValue().toString(),
                                dataSnapshot1.child("key").getValue().toString(),
                                dataSnapshot1.child("phoneNumber").getValue().toString(),
                                dataSnapshot1.child("lat").getValue().toString(),
                                dataSnapshot1.child("lng").getValue().toString(),
                                dataSnapshot1.child("address").getValue().toString()
                        );
                        salonModelList.add(model);
                    }
                    filterSalon();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
    public void  filterSalon(){
        for(UploadImage model:salonModelList)
        {
            double  distanceBtw=0;
            distanceBtw=distance(userLat, userlng,Double.parseDouble(model.getLat()),Double.parseDouble(model.getLng()));
            // showing location marker of those salon whose location not far by 5km from user location
            if(distanceBtw<=BOUND_DISTANCE)
            {
                mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(model.getLat()),Double.parseDouble(model.getLng()))).title(model.getBusinessName()+" "+String.format("%.2f",distanceBtw)+"(km) away"));

            }
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //33.667816, 72.996488
        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(userLat, userlng);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Current Location"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
        //  mMap.setMyLocationEnabled(true);
    }

    @Override
    public void onLocationChanged(Location location) {
        if(location!=null)
        {

            userLat=location.getLatitude();
            userlng=location.getLongitude();
            mMap.addMarker(new MarkerOptions().position(new LatLng(userLat,userlng)).title("Current Location"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(userLat,userlng)));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
            // Toast.makeText(getApplicationContext(),String.valueOf(userLat+"v"+userlng),Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLocation();
        getAllSlon(type);

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Enabled new provider!" + provider,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String s) {
        Toast.makeText(NearbySalonActivity.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
    }
}
