package hsdeveloper.beautylinks.Dashboard;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import hsdeveloper.beautylinks.R;

public class GetBookingAdapter extends RecyclerView.Adapter<GetBookingAdapter.ViewHolder> {
    private List<GetBookingListItems> list;
    private Context context;

    public GetBookingAdapter(List<GetBookingListItems> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.booking_layout,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
         final GetBookingListItems listItems = list.get(position);
         holder.businessname.setText(listItems.getEmail());
         holder.servicename.setText(listItems.getPhone());
         holder.date.setText(listItems.getDate());

         holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Intent intent = new Intent(context,GetBookingDetail.class);
                 intent.putExtra("email",listItems.getEmail());
                 intent.putExtra("phone",listItems.getPhone());
                 intent.putExtra("date",listItems.getDate());
                 intent.putExtra("status",listItems.getStatus());
                 intent.putExtra("serviceid",listItems.getServiceid());
                 intent.putExtra("appointid",listItems.getAppointid());
                 intent.putExtra("timeslot",listItems.getTimeslot());
                 context.startActivity(intent);
             }
         });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView businessname,servicename,date;
        RelativeLayout relativeLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            businessname = itemView.findViewById(R.id.businessname);
            servicename = itemView.findViewById(R.id.servicename);
            date = itemView.findViewById(R.id.date);
            relativeLayout = itemView.findViewById(R.id.relativelayout);
        }
    }
}
