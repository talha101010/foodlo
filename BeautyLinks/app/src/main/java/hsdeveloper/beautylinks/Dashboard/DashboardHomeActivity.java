package hsdeveloper.beautylinks.Dashboard;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import hsdeveloper.beautylinks.HomeAdapter;
import hsdeveloper.beautylinks.HomeDataProvider;
import hsdeveloper.beautylinks.R;
import hsdeveloper.beautylinks.Shops.AddBusinessActivity;
import hsdeveloper.beautylinks.Shops.ReviewsActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class DashboardHomeActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    Button updateButton;
//    String[] array = {"Pending \nBooking", "Assign \nBooking" , "Completed \nBooking", "Cleard \nBooking" ,
//            "Reviews", "Services"};
String[] array = {"Completed \nBooking", "Cleard \nBooking" ,
        "Reviews", "Services"};
//    int[] icons = {R.drawable.ic_pending_booking_icon,
//            R.drawable.ic_pending_booking_icon,
//            R.drawable.ic_completed_booking_icon,
//            R.drawable.ic_completed_booking_icon,
//            R.drawable.ic_reviews_icon,
//            R.drawable.ic_services_icon};
int[] icons = {
        R.drawable.ic_completed_booking_icon,
        R.drawable.ic_completed_booking_icon,
        R.drawable.ic_reviews_icon,
        R.drawable.ic_services_icon};
    HomeAdapter homeAdapter;
    private ArrayList<HomeDataProvider> list;
    Intent intent;
    String phoneNumber;
    private Button updateBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_home);
        updateBtn = findViewById(R.id.update_business_button);
        intent = getIntent();
        phoneNumber = intent.getStringExtra("phone");

        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        updateButton = findViewById(R.id.update_business_button);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        list = new ArrayList<HomeDataProvider>();


        try {
            for (int i = 0; i < array.length; i++) {


                String nameEng = array[i];
                int homeiconitem = icons[i];
                HomeDataProvider homeDataProvider = new HomeDataProvider(nameEng, "", homeiconitem);
                list.add(homeDataProvider);
            }

            homeAdapter = new HomeAdapter(getApplicationContext(), list);
            recyclerView.setAdapter(homeAdapter);


        }catch (Exception e){

            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();

        }

        homeAdapter.setOnItemClickListener(new HomeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                switch (position){
//
//                    case 0:
//                     Intent intent = new Intent(DashboardHomeActivity.this,GetBooking.class);
//                     intent.putExtra("check","Pending");
//                     startActivity(intent);
//
//                        break;
//
//                    case 1:
//                        Intent intent1 = new Intent(DashboardHomeActivity.this,GetBooking.class);
//                        intent1.putExtra("check","Assign");
//                        startActivity(intent1);
//
//
//                        break;

                    case 0:

                        Intent intent2 = new Intent(DashboardHomeActivity.this,GetBooking.class);
                        intent2.putExtra("check","Complete");
                        startActivity(intent2);


                        break;

                    case 1:

                        Intent intent3 = new Intent(DashboardHomeActivity.this,GetBooking.class);
                        intent3.putExtra("check","Clear");
                        startActivity(intent3);

                        break;

                    case 2:
                        FirebaseDatabase.getInstance().getReference().child("businesses").child(phoneNumber)
                                .addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            String key = dataSnapshot.child("key").getValue().toString();
                                            Intent intent4 = new Intent(DashboardHomeActivity.this,ReviewsActivity.class);
                                            intent4.putExtra("stat","admin");
                                            intent4.putExtra("key",key);
                                            startActivity(intent4);
                                        }else{
                                            Toast.makeText(DashboardHomeActivity.this, "no reviews added yet", Toast.LENGTH_SHORT).show();
                                        }

                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                        break;

                    case 3:

                        startActivity(new Intent(DashboardHomeActivity.this,ServicesActivity.class)
                        .putExtra("phone",phoneNumber)
                        );

                        break;
                }
            }

            @Override
            public void onButtonChange(int position) {

            }
        });

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardHomeActivity.this,AddBusinessActivity.class);
                intent.putExtra("phone",phoneNumber);
                intent.putExtra("stat","update");
                startActivity(intent);
            }
        });

}
}
