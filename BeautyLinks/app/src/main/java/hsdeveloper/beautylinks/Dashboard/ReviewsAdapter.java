package hsdeveloper.beautylinks.Dashboard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import hsdeveloper.beautylinks.R;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ViewHolder> {
    private List<ReviewsListItems> list;
    private Context context;

    public ReviewsAdapter(List<ReviewsListItems> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.reviewsfor_admin_layout,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
       ReviewsListItems listItems = list.get(position);
       holder.businessname.setText(listItems.getBusinessname());
       holder.totalrating.setText(listItems.getRating()+"");
       float rating = Float.parseFloat(listItems.getRating());
       holder.ratingbar.setRating(rating);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView businessname,totalrating;
        RatingBar ratingbar;
        RelativeLayout relativeLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            businessname = itemView.findViewById(R.id.businessname);
            totalrating = itemView.findViewById(R.id.totalrating);
            ratingbar = itemView.findViewById(R.id.ratingbar);
            relativeLayout = itemView.findViewById(R.id.relativelayout);
        }
    }
}
