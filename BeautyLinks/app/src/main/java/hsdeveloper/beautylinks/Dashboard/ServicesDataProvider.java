package hsdeveloper.beautylinks.Dashboard;

public class ServicesDataProvider {

    String sId, sTitle,sPrice,sTime,sDescription;

    public ServicesDataProvider() {
    }

    public ServicesDataProvider(String sId, String sTitle, String sPrice, String sTime, String sDescription) {
        this.sId = sId;
        this.sTitle = sTitle;
        this.sPrice = sPrice;
        this.sTime = sTime;
        this.sDescription = sDescription;
    }

    public String getsId() {
        return sId;
    }

    public String getsTitle() {
        return sTitle;
    }

    public String getsPrice() {
        return sPrice;
    }

    public String getsTime() {
        return sTime;
    }

    public String getsDescription() {
        return sDescription;
    }
}
