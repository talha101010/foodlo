package hsdeveloper.beautylinks.Dashboard;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import hsdeveloper.beautylinks.R;
import hsdeveloper.beautylinks.Shops.DetailAdapter;
import hsdeveloper.beautylinks.Shops.DetailDataProvider;
import hsdeveloper.beautylinks.Shops.ShopsDetail;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ServicesActivity extends AppCompatActivity {

    Button addServicesBtn;
    Intent intent;
    String phoneNumber;
    RecyclerView recyclerView;
    DetailDataProvider detailDataProvider;
    ArrayList<ServicesDataProvider> servicesList;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);

        intent = getIntent();
        phoneNumber = intent.getStringExtra("phone");

        addServicesBtn = findViewById(R.id.add_services_button);
        addServicesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ServicesActivity.this,AddServicesActivity.class)
                        .putExtra("phone",phoneNumber)
                        .putExtra("status","add")
                );
            }
        });
        servicesList = new ArrayList<>();

        recyclerView = findViewById(R.id.recyclerview);

        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        recyclerView.setNestedScrollingEnabled(false);
        try {
            loadServices(phoneNumber);
        }catch (Exception e){

            Toast.makeText(this, e+"", Toast.LENGTH_SHORT).show();

        }
    }

    private  void loadServices(final String phoneNumber){

        databaseReference = FirebaseDatabase.getInstance().getReference("businesses").child(phoneNumber).child("services");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                servicesList.clear();
                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){

                    ServicesDataProvider dp = dataSnapshot1.getValue(ServicesDataProvider.class);
                    servicesList.add(dp);
                }

                DashboardServicesAdapter adapter = new DashboardServicesAdapter(ServicesActivity.this,servicesList,phoneNumber);
                recyclerView.setAdapter(adapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
}
