package hsdeveloper.beautylinks.Dashboard;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import hsdeveloper.beautylinks.R;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Reviews extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<ReviewsListItems> list;
    private DatabaseReference databaseReference;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviews2);
        list = new ArrayList();
        recyclerView = findViewById(R.id.reviewsrv);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCanceledOnTouchOutside(false);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        getBusinesses();
    }

    private void getBusinesses(){
        progressDialog.show();
       databaseReference.child("businesses")
               .addValueEventListener(new ValueEventListener() {
                   @Override
                   public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                       progressDialog.dismiss();
                       for (final DataSnapshot snapshot : dataSnapshot.getChildren()){
                           Query query = databaseReference.child("Reviews").orderByChild("bid")
                                   .equalTo(snapshot.child("key").getValue().toString());
                           query.addValueEventListener(new ValueEventListener() {
                               @Override
                               public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {
                                   float getrating = 0;
                                   int number = 0;
                                   for (DataSnapshot snapshot1 : dataSnapshot1.getChildren()){
                                       getrating += Float.parseFloat(snapshot1.child("rating").getValue().toString());
                                       number++;
                                   }
                                   float finalrating = getrating/number;
                                   ReviewsListItems listItems = new ReviewsListItems(
                                           snapshot.child("businessName").getValue().toString(),
                                           snapshot.child("key").getValue().toString(),
                                           finalrating+""
                                   );
                                   list.add(listItems);
                               }

                               @Override
                               public void onCancelled(@NonNull DatabaseError databaseError) {
                                   Toast.makeText(Reviews.this, "Communication server error", Toast.LENGTH_SHORT).show();
                               }
                           });
                       }

                       adapter = new ReviewsAdapter(list,Reviews.this);
                       recyclerView.setAdapter(adapter);
                   }

                   @Override
                   public void onCancelled(@NonNull DatabaseError databaseError) {
                       progressDialog.dismiss();
                       Toast.makeText(Reviews.this, "communication server error", Toast.LENGTH_SHORT).show();
                   }
               });
    }


}
