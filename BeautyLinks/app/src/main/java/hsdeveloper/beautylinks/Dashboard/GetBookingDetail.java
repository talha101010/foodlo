package hsdeveloper.beautylinks.Dashboard;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import hsdeveloper.beautylinks.R;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class GetBookingDetail extends AppCompatActivity {
    private ImageView businessimage;
    private TextView businessname,servicetitle,price,time,description,name,email,phone,appointdate;
    private DatabaseReference databaseReference;
    private Spinner timeslots;
    private Button savebtn;
    private String[] timeSlotArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_booking_detail);
        getViews();
        getBusiness();
        getService();
        getCustomer();

        if (getIntent().getStringExtra("status").equals("Assign")){
            timeslots.setSelection(((ArrayAdapter)timeslots.getAdapter()).getPosition(getIntent().getStringExtra("timeslot")));
            savebtn.setText("Change Time Slot");
            appointdate.setVisibility(View.GONE);
        }else if (getIntent().getStringExtra("status").equals("Complete")){
            timeslots.setVisibility(View.GONE);
            appointdate.setVisibility(View.VISIBLE);
            appointdate.setText("Customer Appointment is on : "+getIntent().getStringExtra("date")+" at :"+getIntent().getStringExtra("timeslot"));
            savebtn.setText("Clear Now");
        }else if (getIntent().getStringExtra("status").equals("Clear")){
            appointdate.setText("Customer Appointment was on : "+getIntent().getStringExtra("date")+" at :"+getIntent().getStringExtra("timeslot"));
            savebtn.setVisibility(View.GONE);
            appointdate.setVisibility(View.VISIBLE);
            timeslots.setVisibility(View.GONE);
        }

        savebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (savebtn.getText().equals("Clear Now")){
                    updateTimeSlot1("Clear");
                }else{
                    if (timeslots.getSelectedItemPosition() ==0){
                        Toast.makeText(GetBookingDetail.this, "Please select time slot first", Toast.LENGTH_SHORT).show();
                    }else{
                        updateTimeSlot("Assign");
                    }
                }

            }
        });
    }
    private void getViews(){
        businessimage = findViewById(R.id.businessimage);
        businessname = findViewById(R.id.businessname);
        servicetitle = findViewById(R.id.servicetitle);
        price = findViewById(R.id.price);
        time = findViewById(R.id.time);
        description = findViewById(R.id.description);
        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        timeslots = findViewById(R.id.timeslots);
        savebtn = findViewById(R.id.savebtn);
        timeSlotArray = getResources().getStringArray(R.array.time_slot);
        appointdate = findViewById(R.id.appointdate);
    }

    private void getBusiness(){
        databaseReference = FirebaseDatabase.getInstance().getReference().child("businesses").child(getIntent().getStringExtra("phone"));
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    businessname.setText(dataSnapshot.child("businessName").getValue().toString());
                    Picasso.with(GetBookingDetail.this)
                            .load(dataSnapshot.child("imageUri").getValue().toString())
                            .fit()
                            .into(businessimage);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(GetBookingDetail.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getService(){
        databaseReference = FirebaseDatabase.getInstance().getReference().child("businesses").child(getIntent().getStringExtra("phone")).child("services").child(getIntent().getStringExtra("serviceid"));
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    servicetitle.setText(dataSnapshot.child("sTitle").getValue().toString());
                    price.setText(dataSnapshot.child("sPrice").getValue().toString());
                    time.setText(dataSnapshot.child("sTime").getValue().toString());
                    description.setText(dataSnapshot.child("sDescription").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(GetBookingDetail.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getCustomer(){
        Query query = FirebaseDatabase.getInstance().getReference().child("customers").orderByChild("email")
                .equalTo(getIntent().getStringExtra("email"));
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                        name.setText("Name : "+snapshot.child("name").getValue().toString());
                        email.setText("Email : "+snapshot.child("email").getValue().toString());
                        phone.setText("Phone : "+snapshot.child("phoneNumber").getValue().toString());
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(GetBookingDetail.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateTimeSlot(String status1){
        databaseReference = FirebaseDatabase.getInstance().getReference().child("Booking").child(getIntent().getStringExtra("appointid"));
        databaseReference.child("timeslot").setValue(timeSlotArray[timeslots.getSelectedItemPosition()]);
        databaseReference.child("status").setValue(status1);
        Toast.makeText(this, "Time slot updated", Toast.LENGTH_SHORT).show();
        finish();
    }

    private void updateTimeSlot1(String status1){
        databaseReference = FirebaseDatabase.getInstance().getReference().child("Booking").child(getIntent().getStringExtra("appointid"));
        databaseReference.child("status").setValue(status1);
        FirebaseDatabase.getInstance().getReference().child("BookingTime").child(getIntent().getStringExtra("date")).child(getIntent().getStringExtra("timeslot"))
                .removeValue(new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                        Toast.makeText(GetBookingDetail.this, "Time Slot Updated", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });

    }
}
