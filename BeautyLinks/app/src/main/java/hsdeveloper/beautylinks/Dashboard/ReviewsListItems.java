package hsdeveloper.beautylinks.Dashboard;

public class ReviewsListItems {
    private String businessname,bid,rating;

    public ReviewsListItems(String businessname, String bid, String rating) {
        this.businessname = businessname;
        this.bid = bid;
        this.rating = rating;
    }

    public String getBusinessname() {
        return businessname;
    }

    public String getBid() {
        return bid;
    }

    public String getRating() {
        return rating;
    }
}
