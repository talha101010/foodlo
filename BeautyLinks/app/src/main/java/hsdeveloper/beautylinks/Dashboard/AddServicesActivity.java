package hsdeveloper.beautylinks.Dashboard;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import hsdeveloper.beautylinks.R;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AddServicesActivity extends AppCompatActivity {

    EditText etTitleService,etPrice,etTime,etDescription;
    Spinner selectCurrancy;
    Button addServiceButton,updateButton;

    DatabaseReference databaseReference;
    Intent intent;
    String phoneNumber;
    String strTitle,strPrice,strTime,strId,strDescription,status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_services);

        views();

        intent = getIntent();
        phoneNumber = intent.getStringExtra("phone");
        status = intent.getStringExtra("status");

        if(status.equals("edit")){

            strTitle= intent.getStringExtra("title");
            strPrice = intent.getStringExtra("price");
            strTime = intent.getStringExtra("time");
            strDescription = intent.getStringExtra("des");
            strId = intent.getStringExtra("id");

            addServiceButton.setVisibility(View.GONE);
            updateButton.setVisibility(View.VISIBLE);

            etTitleService.setText(strTitle);
            etPrice.setText(strPrice);
            etTime.setText(strTime);
            etDescription.setText(strDescription);
        }





        addServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etTitleService.getText().toString().equals("")){

                    etTitleService.setError("Put Title");
                }else if (etPrice.getText().toString().equals("")){

                    etPrice.setError("Put Price");
                }else if (etTime.getText().toString().equals("")){

                    etTime.setError("Put Time");
                }else {

                    databaseReference = FirebaseDatabase.getInstance().getReference("businesses").child(phoneNumber).child("services");

                    if (etDescription.getText().toString().equals("")){

                        strDescription = "";
                    }else {

                        strDescription = etDescription.getText().toString();
                    }



                    String sId = databaseReference.push().getKey();

                    ServicesDataProvider dp = new ServicesDataProvider(
                            sId,
                            etTitleService.getText().toString(),
                            etPrice.getText().toString()+selectCurrancy.getSelectedItem().toString(),
                            etTime.getText().toString(),
                            strDescription
                    );

                    databaseReference.child(sId).setValue(dp).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            Toast.makeText(AddServicesActivity.this, "Added", Toast.LENGTH_SHORT).show();
                            etTitleService.setText("");

                        }
                    });
                }
            }
        });

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etTitleService.getText().toString().equals("")){

                    etTitleService.setError("Put Title");
                }else if (etPrice.getText().toString().equals("")){

                    etPrice.setError("Put Price");
                }else if (etTime.getText().toString().equals("")){

                    etTime.setError("Put Time");
                }else {

                    databaseReference = FirebaseDatabase.getInstance().getReference("businesses").child(phoneNumber).child("services").child(strId);

                    if (etDescription.getText().toString().equals("")){

                        strDescription = "";
                    }else {

                        strDescription = etDescription.getText().toString();
                    }





                    ServicesDataProvider dp = new ServicesDataProvider(
                            strId,
                            etTitleService.getText().toString(),
                            etPrice.getText().toString()+selectCurrancy.getSelectedItem().toString(),
                            etTime.getText().toString(),
                            strDescription
                    );

                    databaseReference.setValue(dp).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            Toast.makeText(AddServicesActivity.this, "Updated", Toast.LENGTH_SHORT).show();
                            etTitleService.setText("");
                            finish();

                        }
                    });
                }

            }
        });


    }

    private void views(){

        etTitleService= findViewById(R.id.et_service_title);
        etPrice = findViewById(R.id.et_service_price);
        etTime = findViewById(R.id.et_time);
        etDescription = findViewById(R.id.et_description_of_service);
        selectCurrancy = findViewById(R.id.select_currancy_type);
        addServiceButton = findViewById(R.id.btn_add_service);
        updateButton = findViewById(R.id.btn_update_service);
    }
}
