package hsdeveloper.beautylinks.Dashboard;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import hsdeveloper.beautylinks.R;
import hsdeveloper.beautylinks.Shops.DetailAdapter;

public class DashboardServicesAdapter  extends RecyclerView.Adapter<DashboardServicesAdapter.ViewHolder> {

    private Context mcontext;
    private ArrayList<ServicesDataProvider> mList;
    private DashboardServicesAdapter.OnItemClickListener mListener;
    private  String phoneNumber;
    DatabaseReference databaseReference;

    public interface OnItemClickListener{
        void  onItemClick(int position);
        void  onButtonChange(int position);
    }

    public void setOnItemClickListener(DashboardServicesAdapter.OnItemClickListener listener){
        mListener = listener;
    }

    public DashboardServicesAdapter(Context context, ArrayList<ServicesDataProvider> list,String phoneNumber){

        this.mcontext = context;
        this.mList = list;
        this.phoneNumber = phoneNumber;


    }


    @Override
    public DashboardServicesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mcontext);
        View view = inflater.inflate(R.layout.detail_customise_list,parent,false);
        DashboardServicesAdapter.ViewHolder viewHolder = new DashboardServicesAdapter.ViewHolder(view,mListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DashboardServicesAdapter.ViewHolder holder, int position) {

        final ServicesDataProvider homeDataProvider  = mList.get(position);

        TextView headingTv = holder.headingTextview;
        TextView amountTv = holder.AmountTextview;
        TextView timeTv = holder.TimeTextview;
        TextView descriptionTv = holder.DescriptionTextview;
        LinearLayout deleteUpdateLayout = holder.deleteUpdateLayout;
        Button opointmentButton = holder.OppointmentButton;
        ImageButton deleteImageButton = holder.deleteImageButton;
        ImageButton editImageButton = holder.editImageButton;

        headingTv.setText(homeDataProvider.getsTitle());
        amountTv.setText(homeDataProvider.getsPrice());
        timeTv.setText("/"+homeDataProvider.getsTime());
        descriptionTv.setText(homeDataProvider.getsDescription());
        deleteUpdateLayout.setVisibility(View.VISIBLE);
        opointmentButton.setVisibility(View.INVISIBLE);

        deleteImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseReference = FirebaseDatabase.getInstance().getReference("businesses").child(phoneNumber).child("services");
                databaseReference.child(homeDataProvider.getsId()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(mcontext, "Deleted", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        editImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mcontext.startActivity(new Intent(mcontext,AddServicesActivity.class)
                .putExtra("title",homeDataProvider.getsTitle())
                        .putExtra("price",homeDataProvider.getsPrice())
                        .putExtra("time",homeDataProvider.getsTime())
                        .putExtra("des",homeDataProvider.getsDescription())
                        .putExtra("id",homeDataProvider.getsId())
                        .putExtra("phone",phoneNumber)
                                .putExtra("status","edit")
                );


            }
        });





    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView headingTextview, AmountTextview, TimeTextview,DescriptionTextview;
        Button OppointmentButton;
        LinearLayout deleteUpdateLayout;
        ImageButton deleteImageButton, editImageButton;



        public ViewHolder(View itemView, final DashboardServicesAdapter.OnItemClickListener listener) {

            super(itemView);

            headingTextview = itemView.findViewById(R.id.heading_textview);
            AmountTextview = itemView.findViewById( R.id.amount_textview);
            TimeTextview = itemView.findViewById(R.id.timetextview);
            DescriptionTextview = itemView.findViewById(R.id.descriptiontextview);
            OppointmentButton= itemView.findViewById(R.id.oppointment_button);
            deleteUpdateLayout = itemView.findViewById(R.id.delete_update_layout);
            deleteImageButton= itemView.findViewById(R.id.delete_imagebtn);
            editImageButton = itemView.findViewById(R.id.edit_imagebtn);



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listener!=null){

                        int position  = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }

                }
            });
        }
    }

}
