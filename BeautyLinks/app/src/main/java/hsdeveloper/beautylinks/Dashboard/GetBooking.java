package hsdeveloper.beautylinks.Dashboard;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import hsdeveloper.beautylinks.R;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class GetBooking extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<GetBookingListItems> list;
    private DatabaseReference databaseReference;
    private String email;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_booking);
        list = new ArrayList<>();
        recyclerView = findViewById(R.id.bookingrv);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Please Wait......");
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(GetBooking.this);
        email = sharedPref.getString("Username",null);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        getData();
    }

    private void getData(){
      Query query = databaseReference.child("Booking").orderByChild("email").equalTo(email);
      progressDialog.show();
      query.addValueEventListener(new ValueEventListener() {
          @Override
          public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
              progressDialog.dismiss();
              if (adapter!=null){
                  list.clear();
              }
                 if (dataSnapshot.exists()){
                     for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                         if (getIntent().getStringExtra("check").equals("Pending")){
                             if (snapshot.child("status").getValue().toString().equals("Pending")){
                                 GetBookingListItems listItems = new GetBookingListItems(
                                         snapshot.child("appointdate").getValue().toString(),
                                         snapshot.child("email").getValue().toString(),
                                         snapshot.child("phonenumber").getValue().toString(),
                                         snapshot.child("status").getValue().toString(),
                                         snapshot.child("serviceid").getValue().toString(),
                                         snapshot.getKey(),
                                         snapshot.child("timeslot").getValue().toString()
                                 );
                                 list.add(listItems);
                             }
                         }else if (getIntent().getStringExtra("check").equals("Assign")){
                             if (snapshot.child("status").getValue().toString().equals("Assign")){
                                 GetBookingListItems listItems = new GetBookingListItems(
                                         snapshot.child("appointdate").getValue().toString(),
                                         snapshot.child("email").getValue().toString(),
                                         snapshot.child("phonenumber").getValue().toString(),
                                         snapshot.child("status").getValue().toString(),
                                         snapshot.child("serviceid").getValue().toString(),
                                         snapshot.getKey(),
                                         snapshot.child("timeslot").getValue().toString()
                                 );
                                 list.add(listItems);
                             }
                         }else if (getIntent().getStringExtra("check").equals("Complete")){
                             if (snapshot.child("status").getValue().toString().equals("Complete")){
                                 GetBookingListItems listItems = new GetBookingListItems(
                                         snapshot.child("appointdate").getValue().toString(),
                                         snapshot.child("email").getValue().toString(),
                                         snapshot.child("phonenumber").getValue().toString(),
                                         snapshot.child("status").getValue().toString(),
                                         snapshot.child("serviceid").getValue().toString(),
                                         snapshot.getKey(),
                                         snapshot.child("timeslot").getValue().toString()
                                 );
                                 list.add(listItems);
                             }
                         }else if (getIntent().getStringExtra("check").equals("Clear")){
                             if (snapshot.child("status").getValue().toString().equals("Clear")){
                                 GetBookingListItems listItems = new GetBookingListItems(
                                         snapshot.child("appointdate").getValue().toString(),
                                         snapshot.child("email").getValue().toString(),
                                         snapshot.child("phonenumber").getValue().toString(),
                                         snapshot.child("status").getValue().toString(),
                                         snapshot.child("serviceid").getValue().toString(),
                                         snapshot.getKey(),
                                         snapshot.child("timeslot").getValue().toString()
                                 );
                                 list.add(listItems);
                             }
                         }


                     }
                     adapter = new GetBookingAdapter(list,GetBooking.this);
                     recyclerView.setAdapter(adapter);
                 }
          }

          @Override
          public void onCancelled(@NonNull DatabaseError databaseError) {
            progressDialog.dismiss();
              Toast.makeText(GetBooking.this, "Communication server error", Toast.LENGTH_SHORT).show();
          }
      });
    }
}
