package hsdeveloper.beautylinks.Dashboard;

public class GetBookingListItems {
    private String date,email,phone,status,serviceid,appointid,timeslot;

    public GetBookingListItems(String date, String email, String phone, String status, String serviceid, String appointid,String timeslot) {
        this.date = date;
        this.email = email;
        this.phone = phone;
        this.status = status;
        this.serviceid = serviceid;
        this.appointid = appointid;
        this.timeslot = timeslot;
    }

    public String getDate() {
        return date;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getStatus() {
        return status;
    }

    public String getServiceid() {
        return serviceid;
    }

    public String getAppointid() {
        return appointid;
    }

    public String getTimeslot() {
        return timeslot;
    }
}
