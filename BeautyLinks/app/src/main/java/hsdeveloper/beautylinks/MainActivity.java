package hsdeveloper.beautylinks;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

import hsdeveloper.beautylinks.Customers.ProfileActivity;
import hsdeveloper.beautylinks.Shops.ShopsListActivity;
import hsdeveloper.beautylinks.SingUpIn.LoginActivity;
import hsdeveloper.beautylinks.SingUpIn.SignUpActivity;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private Animation animation;
    HomeDataProvider homeDataProvider;
    String[] homestringname,seocndString;
    com.google.android.material.floatingactionbutton.FloatingActionButton shopButton,profileButton,signInButton,signUpButton,logoutButton;
    int[] homeicon = {R.drawable.ic_facial_treatment,
            R.drawable.ic_spa_and_relax,
            R.drawable.ic_fashion,
            R.drawable.ic_clipper,
            R.drawable.ic_gym,R.drawable.ic_birthday_and_party};
    String[] seoondName;
    HomeAdapter homeAdapter;
    private ArrayList<HomeDataProvider> list;
    private ArrayList<HomeDataProvider> shopsList;
    SharedPreferences sharedPref;
    boolean Registered;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");



        views();

        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        Registered = sharedPref.getBoolean("Registered", false);


        try {


            signUpButton = findViewById(R.id.sing_up_fb);
            signInButton = findViewById(R.id.sing_in_fb);
            logoutButton = findViewById(R.id.logout_in_fb);
             profileButton = findViewById(R.id.profile_in_fb);


            signUpButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this, SignUpActivity.class));
                }
            });

            signInButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                }
            });

            logoutButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.clear();
                    editor.commit();
                    FirebaseAuth.getInstance().signOut();
                    Toast.makeText(MainActivity.this, "Log out Successfully", Toast.LENGTH_SHORT).show();

                  //  finish();
                    overridePendingTransition(0, 0);
                    startActivity(getIntent());
                    overridePendingTransition(0, 0);
                }
            });

            profileButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (FirebaseAuth.getInstance().getCurrentUser()!=null && FirebaseAuth.getInstance().getCurrentUser().isEmailVerified()){
                        startActivity(new Intent(MainActivity.this, ProfileActivity.class));
                    }else{
                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    }


                }
            });
        }catch (Exception e){

            Toast.makeText(this, e+"", Toast.LENGTH_SHORT).show();


        }


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        homestringname = getResources().getStringArray(R.array.home_array);


        list = new ArrayList<HomeDataProvider>();


        try {
            for (int i=0; i<homestringname.length; i++){



                String nameEng = homestringname[i];
                int homeiconitem = homeicon[i];
                HomeDataProvider homeDataProvider= new HomeDataProvider(nameEng,"",homeiconitem);
                list.add(homeDataProvider);
            }

            homeAdapter = new HomeAdapter(getApplicationContext(),list);
            recyclerView.setAdapter(homeAdapter);
            homeAdapter.setOnItemClickListener(new HomeAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    startActivity(new Intent(MainActivity.this, ShopsListActivity.class)
                    .putExtra("type",list.get(position).getBtnNameEng())
                    );
                }

                @Override
                public void onButtonChange(int position) {

                }
            });


        }catch (Exception e){

            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();

        }



        animation   = AnimationUtils.loadAnimation(this,R.anim.uptodown);
        recyclerView.setAnimation(animation);
    }

    public void views(){
        recyclerView = findViewById(R.id.recyclerview);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }




}
