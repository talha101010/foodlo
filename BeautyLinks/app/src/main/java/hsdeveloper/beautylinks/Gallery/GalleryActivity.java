package hsdeveloper.beautylinks.Gallery;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

import hsdeveloper.beautylinks.R;

public class GalleryActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<GalleryDataProvider> galleryList;
    GalleryDataProvider dataProvider;
    GalleryAdapter galleryAdapter;

    String[] imageName ={"image1","image2",
            "image1","image2",
            "image1","image2"};
    int[] image = {R.drawable.s1,R.drawable.s2,
            R.drawable.s1,R.drawable.s2
    ,R.drawable.s1,R.drawable.s2};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");


        recyclerView =findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this,3));

        galleryList =new ArrayList<>();

        for (int i=0;  i<image.length; i++)
        {

            GalleryDataProvider db = new GalleryDataProvider(imageName[i],image[i]);
            galleryList.add(db);
        }

        galleryAdapter = new GalleryAdapter(GalleryActivity.this,galleryList);
        recyclerView.setAdapter(galleryAdapter);


    }
}
