package hsdeveloper.beautylinks.Gallery;

import android.content.Context;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

import hsdeveloper.beautylinks.R;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    private Context mcontext;
    private ArrayList<GalleryDataProvider> mList;
    private GalleryAdapter.OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);

        void onButtonChange(int position);
    }

    public void setOnItemClickListener(GalleryAdapter.OnItemClickListener listener) {
        mListener = listener;
    }

    public GalleryAdapter(Context context, ArrayList<GalleryDataProvider> list) {

        this.mcontext = context;
        this.mList = list;


    }


    @Override
    public GalleryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mcontext);
        View view = inflater.inflate(R.layout.gallery_customise, parent, false);
        GalleryAdapter.ViewHolder viewHolder = new GalleryAdapter.ViewHolder(view, mListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(GalleryAdapter.ViewHolder holder, int position) {

        GalleryDataProvider homeDataProvider = mList.get(position);

       // TextView imageName = holder.nameEng;
        ImageView images = holder.images;
        CardView cardView = holder.cardView;


        images.setImageResource(homeDataProvider.getImage());


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {


        public ImageView images;
        public CardView cardView;


        public ViewHolder(View itemView, final GalleryAdapter.OnItemClickListener listener) {
            super(itemView);

            cardView = (CardView) itemView.findViewById(R.id.cardview);
            images = itemView.findViewById(R.id.gallery_imageview);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listener != null) {

                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }

                }
            });
        }
    }
}