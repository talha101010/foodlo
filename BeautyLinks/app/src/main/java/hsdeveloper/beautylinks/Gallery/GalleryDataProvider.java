package hsdeveloper.beautylinks.Gallery;

public class GalleryDataProvider {

    String imageName;
    int image;

    public GalleryDataProvider() {
    }

    public GalleryDataProvider(String imageName, int image) {
        this.imageName = imageName;
        this.image = image;
    }

    public String getImageName() {
        return imageName;
    }

    public int getImage() {
        return image;
    }
}
