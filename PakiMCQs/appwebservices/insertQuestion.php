<?php 
 require 'dbOperations.php';
 $response = array();
 if ($_SERVER['REQUEST_METHOD'] == 'POST') {
 	$db = new dbOperations();
 	$mainheading =mysqli_real_escape_string($db->con,$_POST['mainheading']);
    $question = mysqli_real_escape_string($db->con,$_POST['question']);
    $optiona = mysqli_real_escape_string($db->con,$_POST['optiona']);
    $optionb = mysqli_real_escape_string($db->con,$_POST['optionb']);
    $optionc = mysqli_real_escape_string($db->con,$_POST['optionc']);
    $optiond = mysqli_real_escape_string($db->con,$_POST['optiond']);
    $correctoption = mysqli_real_escape_string($db->con,$_POST['answer']);
    $explanation = mysqli_real_escape_string($db->con,$_POST['explanation']);
     $userid = $_POST['userid'];
 	  try {
 	  	
 	  	$result = $db->insertQuestion($mainheading,$question,$optiona,$optionb,$optionc,$optiond,$correctoption,$explanation,$userid);
 	  	 if ($result) {
 	  	 	 $response['error'] = false;
 	  	 	 $response['message'] = 'Question Has been inserted';
 	  	 }else{
 	  	 	$response['error'] = true;
 	  	 	$response['message'] = 'Error occur';
 	  	 }
 	  	
 	  } catch (Exception $e) {
 	  	
 	  }
 }else{
 	$response['error'] = true;
 	$response['message'] = 'Invalid Request Method';
 }

 echo json_encode($response);

 ?>