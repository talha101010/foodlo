<?php 
         require_once '../backend/Connection.php';	
         $response = array();
         if ($_SERVER['REQUEST_METHOD'] == 'POST') {	
			try{
			$mainheading = mysqli_real_escape_string($con,$_POST['mainheading']);
            $subheading = mysqli_real_escape_string($con,$_POST['subheading']);
	         $sql = "SELECT * FROM questions WHERE mainheading = '$mainheading' AND subheading = '$subheading' ORDER BY question_id ASC";
			$result = mysqli_query($con,$sql);
            echo mysqli_error($con);
            $response = array(); 
            $response['error'] = 0; 
            $response['questions'] = array(); 
 
            while($row = mysqli_fetch_array($result)){
            $temp = array();
            $temp['id'] = $row['question_id']; 
            $temp['mainheading'] = $row['mainheading'];
            $temp['subheading'] = $row['subheading'];
            $temp['question'] = $row['question'];
            $temp['optiona'] = $row['option_a'];
            $temp['optionb'] = $row['option_b'];
            $temp['optionc'] = $row['option_c'];
            $temp['optiond'] = $row['option_d'];
            $temp['correctoption'] = $row['correct_option'];
		    $temp['explanation'] = $row['explanation'];
            array_push($response['questions'],$temp);
 }

			}catch(Exception $e){
				$response['error'] = 1; 
				$response['message'] = $e->getMessage(); 
			}
		

	 mysqli_close($con);
	}else{
		$response['error']=1;
        $response['message'] = 'invalid request method';
	}	
	echo json_encode($response);
		
?>