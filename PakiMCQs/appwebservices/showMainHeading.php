<?php 
         require_once '../backend/Connection.php';	
         $response = array();
         if ($_SERVER['REQUEST_METHOD'] == 'GET') {	
			try{
	         $sql = "SELECT * FROM mainheadings ORDER BY mainheadingno ASC";
			$result = mysqli_query($con,$sql);
            $response = array(); 
            $response['error'] = 0; 
            $response['mainheading'] = array(); 
 
            while($row = mysqli_fetch_array($result)){
            $temp = array(); 
			$temp['name'] = $row['mainheadingname'];
			$temp['type'] = $row['mainheading_type'];
			$temp['image'] = "http://".$_SERVER['SERVER_NAME'].'/pakimcqs/headingimages/'.$row['mainheading_image'];
            array_push($response['mainheading'],$temp);
 }

			}catch(Exception $e){
				$response['error'] = 1; 
				$response['message'] = $e->getMessage(); 
			}
		

	 mysqli_close($con);
	}else{
		$response['error']=1;
        $response['message'] = 'invalid request method';
	}	
	echo json_encode($response);
		
?>