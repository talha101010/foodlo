<?php 
 require 'dbOperations.php';
 $response = array();
 if ($_SERVER['REQUEST_METHOD'] == 'POST') {
 	  try {
 	  	$db = new dbOperations();
 	  	$result = $db->changePassword(md5($_POST['oldpassword']),md5($_POST['newpassword']),$_POST['username']);
 	  	 if ($result == 0) {
 	  	 	 $response['error'] = true;
 	  	 	 $response['message'] = 'You entered an incorrect old password';
 	  	 }else if($result == 1){
 	  	 	$response['error'] = false;
 	  	 	$response['message'] = 'Password has been changed successfully';
 	  	 }else if ($result == 2) {
 	  	 	$response['error'] = true;
 	  	 	$response['message'] = 'Error occur';
 	  	 }
 	  	
 	  } catch (Exception $e) {
 	  	
 	  }
 }else{
 	$response['error'] = true;
 	$response['message'] = 'Invalid Request Method';
 }

 echo json_encode($response);

 ?>