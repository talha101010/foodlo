<?php 
session_start();
if (!isset($_SESSION['username'])) {
header('Location:../../index.php');
}
  require '../../backend/Connection.php';

 ?>

<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Pak Tests | MCQ's App</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- favicon
        ============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="../../img/favicon.png">
    <!-- Google Fonts
        ============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <!-- font awesome CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <!-- owl.carousel CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/owl.carousel.css">
    <link rel="stylesheet" href="../../css/owl.theme.css">
    <link rel="stylesheet" href="../../css/owl.transitions.css">
    <!-- meanmenu CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/meanmenu/meanmenu.min.css">
    <!-- animate CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/animate.css">
    <!-- normalize CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/normalize.css">
    <!-- wave CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/wave/waves.min.css">
    <link rel="stylesheet" href="../../css/wave/button.css">
    <!-- mCustomScrollbar CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- Notika icon CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/notika-custom-icon.css">
    <!-- Data Table JS
        ============================================ -->
    <link rel="stylesheet" href="../../css/jquery.dataTables.min.css">
    <!-- main CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/main.css">
    <!-- style CSS
        ============================================ -->
    <link rel="stylesheet" href="../../style.css">
    <!-- responsive CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/responsive.css">
    <!-- modernizr JS
        ============================================ -->
    <script src="../../js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Start Header Top Area -->
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="logo-area">
                        <a href="../../dashboard.php"><img src="../../img/logo/logo.png" alt="" height="70" width="90" /></a>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="header-top-menu">
                        <ul class="nav navbar-nav notika-top-nav">
                        <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><span><i class="notika-icon notika-menus"></i></span></a>
                                <div role="menu" class="dropdown-menu message-dd task-dd animated zoomIn">
                                    <div class="hd-mg-tt">
                                        <h2>Admin</h2>
                                    </div>
                                    <div class="hd-message-info hd-task-info">
                                        <div class="skill">
                                            <form action="../../backend/logout.php" method="POST">
                                           <center><button class="btn btn-primary" name="logoutlink">Logout</button></center>
                                           </form>
                                        </div>
                                    </div>
                                   
                                </div>
                            </li>
                    
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Header Top Area -->
    <!-- Mobile Menu start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul class="mobile-menu-nav">
                                <li><a data-toggle="collapse" data-target="#Charts" href="#">Home</a>
                                    <ul class="collapse dropdown-header-top">
                                        <li><a href="../../dashboard.php">Dashboard</a></li>
                                    </ul>
                                </li>
                                <li><a data-toggle="collapse" data-target="#demoevent" href="#">Current Affairs</a>
                                    <ul id="demoevent" class="collapse dropdown-header-top">
                                <li><a href="addmcq.php">Add New MCQ</a>
                                </li>
                                <li><a href="addheading.php">Add Heading</a>
                                </li>
                                <li><a href="userQuestions.php">User MCQ's</a>
                                        </li>

                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu end -->
    <!-- Main Menu area start-->
    <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
                        <li><a data-toggle="tab" href="#Home"><i class="notika-icon notika-house"></i> Home</a>
                        </li>
                        <li><a data-toggle="tab" href="#mailbox"><i class="notika-icon notika-mail"></i>Current Affairs</a>
                        </li>
                    </ul>
                    <div class="tab-content custom-menu-content">
                        <div id="Home" class="tab-pane in notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="../../dashboard.php">Dashboard</a>
                                </li>
                            </ul>
                        </div>
                        <div id="mailbox" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                 <li><a href="addmcq.php">Add New MCQ</a>
                                </li>
                                <li><a href="addheading.php">Add Heading</a>
                                </li>
                                <li><a href="userQuestions.php">User MCQ's</a>
                                        </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php 
         $check = $_GET['check'];
         if ($check == "true") {
        $id =base64_decode($_GET['id']);
        $result = mysqli_query($con,"SELECT * FROM questions WHERE question_id='$id'");
        $row = mysqli_fetch_array($result);
        $mainheading = $row['mainheading'];
        $subheading = $row['subheading'];
        $answer = $row['correct_option'];
         }else{
        $id =base64_decode($_GET['id']);
        $result = mysqli_query($con,"SELECT * FROM users_questions WHERE question_id='$id'");
        $row = mysqli_fetch_array($result);
        $mainheading = $row['mainheading'];
        $answer = $row['answer'];
         }
       
     ?>

        <div class="contact-info-area mg-t-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="contact-form sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
                        <div class="contact-hd sm-form-hd">
                            <h2>Update Data</h2>
                            <p>Update or delete data from here if you want to update or delete it.</p>
                        </div>
           <form action="../../backend/addNewQuestion.php" method="POST">
           <div class="contact-form-int">

                    <div class="form-group">
                    <select name="mainheading" class="form-control" required>
                    <option value="">Select Main Heading</option>
                     <?php 
                     $result1 = mysqli_query($con,"SELECT mainheadingname FROM mainheadings");
                     while ($row1 = mysqli_fetch_array($result1)) { ?> 
                      <option value="<?php echo $row1['mainheadingname']; ?>"<?php if ($row['mainheading'] == $row1['mainheadingname']) echo ' selected="selected"'; ?>><?php echo $row1['mainheadingname']; ?></option>


                   <?php   }  ?>
                     
                    </select>
                    </div>

                    <?php 
                      $result2 = mysqli_query($con,"SELECT mainheading_type FROM mainheadings WHERE mainheadingname='$mainheading'");
                      $row3 = mysqli_fetch_array($result2);
                      if ($row3['mainheading_type'] == 1) { ?>
                        
                    <div class="form-group">
                    <select name="subheading" class="form-control" required>
                    <option value="">Select Sub Heading</option>
                     <?php 
                     $result1 = mysqli_query($con,"SELECT headingname FROM headings");
                     while ($row1 = mysqli_fetch_array($result1)) {
                         if ($check == "true") {
                      ?> 

                      <option value="<?php echo $row1['headingname']; ?>"<?php if ($subheading == $row1['headingname']) echo ' selected="selected"'; ?>><?php echo $row1['headingname']; ?></option>
                       <?php   }else{ ?>
                           
                           <option value="<?php echo $row1['headingname']; ?>"><?php echo $row1['headingname']; ?></option>

                     <?php  }  } ?>
                     
                    </select>
                    </div>


                   <?php   }else{  ?>

                        <div class="form-group">
                    <select name="subheading" class="form-control" required>
                    <option value="">Select Sub Heading</option>
                     <?php 
                     $result1 = mysqli_query($con,"SELECT subheadingno FROM currentaffairs_subheading");
                     while ($row1 = mysqli_fetch_array($result1)) { 
                             if ($check == "true") {
                        ?> 
                      <option value="<?php echo $row1['subheadingno']; ?>"<?php if ($subheading == $row1['subheadingno']) echo ' selected="selected"'; ?>><?php echo $row1['subheadingno']; ?></option>
                       <?php   }else{ ?>

                        <option value="<?php echo $row1['subheadingno']; ?>"><?php echo $row1['subheadingno']; ?></option>

                    <?php   } }?>
                     
                    </select>
                    </div>

                     <?php  } 
                     ?>

                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" class="form-control" name="question" placeholder="Enter question" value="<?php echo $row['question']; ?>" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" class="form-control" name="optiona" placeholder="Enter option a" value="<?php echo $row['option_a']; ?>" required />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" class="form-control" name="optionb" placeholder="Enter option b" value="<?php echo $row['option_b']; ?>" required />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" class="form-control" name="optionc" placeholder="Enter option c" value="<?php echo $row['option_c']; ?>" required />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" class="form-control" name="optiond" placeholder="Enter option d" value="<?php echo $row['option_d']; ?>" required />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" class="form-control" name="answer" placeholder="Enter answer" value="<?php echo $answer; ?>" required />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                     <textarea class="form-control" cols="15" row="2" placeholder="Enter explanation if any" name="explanation" required><?php echo $row['explanation']; ?></textarea>
                                </div>
                            </div>
                              
                            <div class="contact-btn">
                                <input type="hidden" name="id" value="<?php echo $id; ?>">
                                <?php if ($check == "true") { ?>
                                   <button class="button btn" name="updatequestionbtn" style="width: 50%;">Update</button>
                                   <button class="btn btn-success" name="addquestionbtnone" style="width: 25%;">Insert</button>
                                <button class="btn btn-danger" name="deletequestionbtn" style="width: 20%;">Delete</button>
                             <?php   }else{ ?>

                                   <button class="button btn" name="approvequestionbtn" style="width: 50%;">Approve</button>
                                   <button class="btn btn-success" name="addquestionbtnone" style="width: 25%;">Insert</button>
                                <button class="btn btn-danger" name="deleteuserquestion" style="width: 20%;">Delete</button>

                            <?php } ?>
                                
                                
                                
                            </div>
            </div>
           </form>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="widget-tabs-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-ds-n dk-res-ds">
                        <div class="contact-hd tm-activity">
                            <h2><?php echo $mainheading; ?></h2>
                            <?php if ($check == "true") { ?>
                                <p><?php echo $subheading; ?></p>
                          <?php  }else{ ?>
                                <p>Sub Heading?</p>
                       <?php   } ?>
                            
                        </div>
                        <div class="widget-tabs-list">
                            <div class="tab-content">
                                <div id="home" class="tab-pane fade in active">
                                   <p>Option A:<?php echo $row['option_a']; ?></p>
                                   <p>Option B:<?php echo $row['option_b']; ?></p>
                                   <p>Option C:<?php echo $row['option_c']; ?></p>
                                   <p>Option D:<?php echo $row['option_d']; ?></p>
                                    <p>Answer:<?php echo $answer; ?></p>
                                   
                                   <p>Explanation:<?php echo $row['explanation']; ?></p>
                                   <?php if($check=="true"){ ?>
                                   <center><a href="addmcq.php">Go Back</a></center>
                               <?php }else{ ?>
                                   <center><a href="userQuestions.php">Go Back</a></center>
                               <?php } ?>
                                </div>
                                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Data Table area End-->
    <!-- Start Footer area-->
    <div class="footer-copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="footer-copy-right">
                        <p>Copyright © 2018 
. All rights reserved. Developed By <a href="http://www.pakisol.com">PakiSol</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer area-->


    <!-- jquery
        ============================================ -->
    <script src="../../js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
        ============================================ -->
    <script src="../../js/bootstrap.min.js"></script>
    <!-- wow JS
        ============================================ -->
    <script src="../../js/wow.min.js"></script>
    <!-- price-slider JS
        ============================================ -->
    <script src="../../js/jquery-price-slider.js"></script>
    <!-- owl.carousel JS
        ============================================ -->
    <script src="../../js/owl.carousel.min.js"></script>
    <!-- scrollUp JS
        ============================================ -->
    <script src="../../js/jquery.scrollUp.min.js"></script>
    <!-- meanmenu JS
        ============================================ -->
    <script src="../../js/meanmenu/jquery.meanmenu.js"></script>
    <!-- counterup JS
        ============================================ -->
    <script src="../../js/counterup/jquery.counterup.min.js"></script>
    <script src="../../js/counterup/waypoints.min.js"></script>
    <script src="../../js/counterup/counterup-active.js"></script>
    <!-- mCustomScrollbar JS
        ============================================ -->
    <script src="../../js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- sparkline JS
        ============================================ -->
    <script src="../../js/sparkline/jquery.sparkline.min.js"></script>
    <script src="../../js/sparkline/sparkline-active.js"></script>
    <!-- flot JS
        ============================================ -->
    <script src="../../js/flot/jquery.flot.js"></script>
    <script src="../../js/flot/jquery.flot.resize.js"></script>
    <script src="../../js/flot/flot-active.js"></script>
    <!-- knob JS
        ============================================ -->
    <script src="../../js/knob/jquery.knob.js"></script>
    <script src="../../js/knob/jquery.appear.js"></script>
    <script src="../../js/knob/knob-active.js"></script>
    <!--  Chat JS
        ============================================ -->
    <script src="../../js/chat/jquery.chat.js"></script>
    <!--  todo JS
        ============================================ -->
    <script src="../../js/todo/jquery.todo.js"></script>
    <!--  wave JS
        ============================================ -->
    <script src="../../js/wave/waves.min.js"></script>
    <script src="../../js/wave/wave-active.js"></script>
    <!-- plugins JS
        ============================================ -->
    <script src="../../js/plugins.js"></script>
    <!-- Data Table JS
        ============================================ -->
    <script src="../../js/data-table/jquery.dataTables.min.js"></script>
    <script src="../../js/data-table/data-table-act.js"></script>
    <!-- main JS
        ============================================ -->
    <script src="../../js/main.js"></script>
    <!-- tawk chat JS
        ============================================ -->

             <script type="text/javascript">
              $(document).ready(function(){
                $('#mainheading').change(function(){
                  var mainheading1 = $(this).val();
                  if(mainheading1){
                    $.ajax({
                      method:'POST',
                      dataType:'text',
                      url:'../../backend/getDataFromServer.php',
                      data:{main_heading:mainheading1},
                      success:function(data){
                        $('#subheading').html(data);
                      }
                    });
                  }else{
                    $('#subheading').html('<option value="">Select Main Heading First</option>');
                  }
                });
              });
    </script>
</body>

</html>