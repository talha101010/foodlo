<?php 
 session_start();
 if (!isset($_SESSION['username'])) {
header('Location:../../index.php');
}
require '../../backend/Connection.php';

 ?>
<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>PakiMCQ's | Admin Panel</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="../../img/favicon.png">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <!-- font awesome CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/owl.carousel.css">
    <link rel="stylesheet" href="../../css/owl.theme.css">
    <link rel="stylesheet" href="../../css/owl.transitions.css">
    <!-- meanmenu CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/meanmenu/meanmenu.min.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/normalize.css">
	<!-- wave CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/wave/waves.min.css">
    <link rel="stylesheet" href="../../css/wave/button.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- Notika icon CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/notika-custom-icon.css">
    <!-- codemirror CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/code-editor/codemirror.css">
    <link rel="stylesheet" href="../../css/code-editor/ambiance.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/main.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="../../style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="../../js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Start Header Top Area -->
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="logo-area">
                        <a href="#"><img src="../../img/logo/logo.png" alt="" height="70" width="90" /></a>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="header-top-menu">
                        <ul class="nav navbar-nav notika-top-nav">
                           <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><span><i class="notika-icon notika-menus"></i></span></a>
                                <div role="menu" class="dropdown-menu message-dd task-dd animated zoomIn">
                                    <div class="hd-mg-tt">
                                        <h2>Admin</h2>
                                    </div>
                                    <div class="hd-message-info hd-task-info">
                                        <div class="skill">
                                            <form action="../../backend/logout.php" method="POST">
                                           <center><button class="btn btn-primary" name="logoutlink">Logout</button></center>
                                           </form>
                                        </div>
                                    </div>
                                   
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Header Top Area -->
    <!-- Mobile Menu start -->
      <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul class="mobile-menu-nav">
                                <li><a data-toggle="collapse" data-target="#Charts" href="#">Home</a>
                                    <ul class="collapse dropdown-header-top">
                                        <li><a href="../../dashboard.php">Dashboard</a></li>
                                    </ul>
                                </li>
                                <li><a data-toggle="collapse" data-target="#demoevent" href="#">Current Affairs</a>
                                    <ul id="demoevent" class="collapse dropdown-header-top">
                                        <li><a href="addmcq.php"s>Add New MCQ</a></li>
                                        <li><a href="addheading.php">Add Heading</a>
                                        </li>
                                        <li><a href="userQuestions.php">User MCQ's</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu end -->
    <!-- Main Menu area start-->
    <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
                        <li><a data-toggle="tab" href="#Home"><i class="notika-icon notika-house"></i> Home</a>
                        </li>
                        <li class="active"><a data-toggle="tab" href="#mailbox">Current Affairs</a>
                        </li>
                    </ul>
                    <div class="tab-content custom-menu-content">
                        <div id="Home" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="../../dashboard.php">Dashboard</a>
                                </li>
                            </ul>
                        </div>
                        <div id="mailbox" class="tab-pane in active notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="addmcq.php">Add New MCQ</a>
                                </li>
                                <li><a href="addheading.php">Add Heading</a>
                                </li>
                                <li><a href="userQuestions.php">User MCQ's</a>
                                        </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- Main Menu area End-->
    <!-- Code Editor area start-->
    <div class="code-editor-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="nk-cd-ed-wp">
            <div>
               <center><h3 style="background: #00c292; color: white; padding: 15px; margin-bottom: 40px;">Add Main Heading</h3></center>
            </div>
            <hr>  
            <form action="../../backend/addHeading.php" method="POST" enctype="multipart/form-data">
           <div class="contact-form-int">
                             <div class="form-group">
                                <select name="type" class="form-control" required>
                                    <option value="">Select heading type</option>
                                    <option value="0">Number</option>
                                    <option value="1">List</option>
                                   
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="number" class="form-control" name="headingno" placeholder="Enter Heading No" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" class="form-control" name="headingname" placeholder="Enter Heading Name" required />
                                </div>
                            </div>
                              
                              <div class="form-group">
                             <label title="Upload image file" for="inputImage" class="btn btn-primary img-cropper-cp">
                               <input type="file" name="image" id="inputImage" class="hide form-control" required> Upload Image.
                             </label>
                                               
                            </div>


                            <div class="contact-btn">
                                <button class="button btn form-control" name="addmainheadingbtn">Add Main Heading</button>
                            </div>
            </div>
           </form>

                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="nk-cd-ed-wp sm-res-mg-t-30" style="height: 385px; overflow-y: scroll;">
                <center><h5 style="background: #00c292; color: white; padding: 10px;">Existing Headings</h5></center>
                <hr> 
                <?php 
                $result = mysqli_query($con,"SELECT * FROM mainheadings ORDER BY mainheadingno ASC");
                 ?>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center">Name</th>
                            <th class="text-center">Image</th>
                            <th style="width: 20px;" class="text-center">Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                          while ($row = mysqli_fetch_array($result)) {
                         ?>
                        <tr>
                            <td class="text-center"><?php echo $row['mainheadingname']; ?></td>
                            <td class="text-center"><img src="../../headingimages/<?php echo $row['mainheading_image']; ?>" alt="" height="40" width="60"></td>
                            <td class="text-center">
                                <form action="chapterDetails.php" method="get">
                                <input type="hidden" name="id" value="<?php echo base64_encode($row['mainheading_id']); ?>">
                                <input type="hidden" name="check" value="true">
                                <button class="btn btn-success">Details</button>
                                </form>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Code Editor area End-->

       <!-- Code Editor area start-->
    <div class="code-editor-area" id="headingid">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top: 10px;">
                    <div class="nk-cd-ed-wp">
            <div>
               <center><h3 style="background: #00c292; color: white; padding: 15px; margin-bottom: 40px;">Add Heading</h3></center>
            </div>
            <hr>  
            <form action="../../backend/addHeading.php" method="POST" enctype="multipart/form-data">
           <div class="contact-form-int">
                             <div class="form-group" style="margin-top: 40px;">
                                <select name="mainheading" class="form-control" required>
                                    <option value="">Select Main Heading</option>
                                    <?php 
                                    $result1 = mysqli_query($con,"SELECT mainheadingname FROM mainheadings ORDER BY mainheadingno ASC");
                                      while ($row1 = mysqli_fetch_array($result1)) { ?>
                                        <option value="<?php echo $row1['mainheadingname'];?>"><?php echo $row1['mainheadingname']; ?></option>
                                     <?php }
                                     ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="number" class="form-control" name="headingno" placeholder="Enter Heading No" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" class="form-control" name="headingname" placeholder="Enter Heading Name" required />
                                </div>
                            </div>
                            
                            <div class="contact-btn" style="margin-top: 30px;margin-bottom: 10px;">
                                <button class="button btn form-control" name="addheadingbtn">Add Heading</button>
                            </div>
            </div>
           </form>

                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top: 10px;">
                    <div class="nk-cd-ed-wp sm-res-mg-t-30" style="height: 385px; overflow-y: scroll;">
                <center><h5 style="background: #00c292; color: white; padding: 10px;">Existing Headings</h5></center>
                <hr> 
                <?php 
                $result = mysqli_query($con,"SELECT * FROM headings ORDER BY headingno ASC");
                 ?>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center">Main Heading</th>
                            <th class="text-center">Heading</th>
                            <th style="width: 20px;" class="text-center">Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                          while ($row = mysqli_fetch_array($result)) {
                         ?>
                        <tr>
                            <td class="text-center"><?php echo $row['mainheading']; ?></td>
                            <td class="text-center"><?php echo $row['headingname']; ?></td>
                            <td class="text-center">
                                <form action="chapterDetails.php" method="get">
                                <input type="hidden" name="id" value="<?php echo base64_encode($row['heading_id']); ?>">
                                <input type="hidden" name="check" value="false">
                                <button class="btn btn-success">Details</button>
                                </form>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <div class="code-editor-area" id="topicid">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top: 10px;">
                    <div class="nk-cd-ed-wp">
            <div>
               <center><h3 style="background: #00c292; color: white; padding: 15px; margin-bottom: 40px;">Add Sub-Heading</h3></center>
            </div>
            <hr>  
            <form action="../../backend/addHeading.php" method="POST">
           <div class="contact-form-int">
                            <div class="form-group" style="margin-top: 40px;">
                                <select name="headingname" class="form-control" required>
                                    <option value="">Select Heading</option>
                                    <?php 
                                    $result1 = mysqli_query($con,"SELECT mainheadingname FROM mainheadings ORDER BY mainheadingno ASC");
                                      while ($row1 = mysqli_fetch_array($result1)) { ?>
                                        <option value="<?php echo $row1['mainheadingname'];?>"><?php echo $row1['mainheadingname']; ?></option>
                                     <?php }
                                     ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="number" class="form-control" name="no" placeholder="Enter Sub-Heading Number" required />
                                </div>
                            </div>
                           
                            <div class="contact-btn" style="margin-top: 30px;margin-bottom: 10px;">
                                <button class="button btn form-control" name="addsubheadingbtn">Add Sub-Heading</button>
                            </div>
            </div>
           </form>

                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top: 10px;">
                    <div class="nk-cd-ed-wp sm-res-mg-t-30" style="height: 385px; overflow-y: scroll;">
                <center><h5 style="background: #00c292; color: white; padding: 10px;">Existing Sub-Heading</h5></center>
                <hr> 
                <?php 
                $result2 = mysqli_query($con,"SELECT * FROM currentaffairs_subheading ORDER BY subheadingno ASC");
                 ?>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center">heading</th>
                            <th class="text-center">Number</th>
                            <th style="width: 20px;" class="text-center">Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                          while ($row = mysqli_fetch_array($result2)) {
                         ?>
                        <tr>
                            <td class="text-center"><?php echo $row['headingname']; ?></td>
                            <td class="text-center"><?php echo $row['subheadingno']; ?></td>
                            <td class="text-center">
                                <form action="../../backend/addHeading.php" method="POST">
                                <input type="hidden" name="id" value="<?php echo $row['subheading_id']; ?>">
                                <button class="btn btn-danger" name="subheadingdeletebtn">Delete</button>
                                </form>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Start Footer area-->
    <div class="footer-copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="footer-copy-right">
                        <p>Copyright © 2018 
. All rights reserved. Developed by <a href="http://www.pakisol.com">PakiSol</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer area-->
    <!-- jquery
		============================================ -->
    <script src="../../js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="../../js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="../../js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="../../js/jquery-price-slider.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="../../js/owl.carousel.min.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="../../js/jquery.scrollUp.min.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="../../js/meanmenu/jquery.meanmenu.js"></script>
    <!-- counterup JS
		============================================ -->
    <script src="../../js/counterup/jquery.counterup.min.js"></script>
    <script src="../../js/counterup/waypoints.min.js"></script>
    <script src="../../js/counterup/counterup-active.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="../../js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="../../js/sparkline/jquery.sparkline.min.js"></script>
    <script src="../../js/sparkline/sparkline-active.js"></script>
    <!-- flot JS
		============================================ -->
    <script src="../../js/flot/jquery.flot.js"></script>
    <script src="../../js/flot/jquery.flot.resize.js"></script>
    <script src="../../js/flot/flot-active.js"></script>
    <!-- knob JS
		============================================ -->
    <script src="../../js/knob/jquery.knob.js"></script>
    <script src="../../js/knob/jquery.appear.js"></script>
    <script src="../../js/knob/knob-active.js"></script>
    <!-- code editor JS
		============================================ -->
    <script src="../../js/code-editor/codemirror.js"></script>
    <script src="../../js/code-editor/code-editor.js"></script>
    <script src="../../js/code-editor/code-editor-active.js"></script>
    <!--  Chat JS
		============================================ -->
    <script src="../../js/chat/jquery.chat.js"></script>
    <!--  todo JS
		============================================ -->
    <script src="../../js/todo/jquery.todo.js"></script>
	<!--  wave JS
		============================================ -->
    <script src="../../js/wave/waves.min.js"></script>
    <script src="../../js/wave/wave-active.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="../../js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="../../js/main.js"></script>

      <script type="text/javascript">
              $(document).ready(function(){
                $('#chaptername').change(function(){
                  var chaptername = $(this).val();
                  if(chaptername){
                    $.ajax({
                      method:'POST',
                      dataType:'text',
                      url:'../../backend/getDataFromServer.php',
                      data:{chapter_name:chaptername},
                      success:function(data){
                        $('#topicname').html(data);
                      }
                    });
                  }else{
                    $('#topicname').html('<option value="">Select Chapter First</option>');
                  }
                });
              });
            </script>
	
</body>

</html>