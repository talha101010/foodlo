<?php 
session_start();
if (!isset($_SESSION['username'])) {
header('Location:../../index.php');
}
 require '../../backend/Connection.php';
 ?>
<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Words | Dictionary App</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="../../img/favicon.png">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <!-- font awesome CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/owl.carousel.css">
    <link rel="stylesheet" href="../../css/owl.theme.css">
    <link rel="stylesheet" href="../../css/owl.transitions.css">
    <!-- meanmenu CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/meanmenu/meanmenu.min.css">
    <!-- Notika icon CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/notika-custom-icon.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/normalize.css">
    <!-- wave CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/wave/waves.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/main.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="../../style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="../../css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Start Header Top Area -->
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="logo-area">
                        <a href="../../dashboard.php"><img src="../../img/logo/logo.png" alt="" height="70" width="90" /></a>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="header-top-menu">
                        <ul class="nav navbar-nav notika-top-nav">
                            <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><span><i class="notika-icon notika-menus"></i></span></a>
                                <div role="menu" class="dropdown-menu message-dd task-dd animated zoomIn">
                                    <div class="hd-mg-tt">
                                        <h2>Admin</h2>
                                    </div>
                                    <div class="hd-message-info hd-task-info">
                                        <div class="skill">
                                          <form action="../../backend/logout.php" method="POST">
                                           <center><button class="btn btn-primary" name="logoutlink">Logout</button></center>
                                           </form>
                                        </div>
                                    </div>
                                    
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Header Top Area -->
    <!-- Mobile Menu start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul class="mobile-menu-nav">
                                <li><a data-toggle="collapse" data-target="#Charts" href="#">Home</a>
                                    <ul class="collapse dropdown-header-top">
                                        <li><a href="../../dashboard.php">Dashboard</a></li>
                                    </ul>
                                </li>
                                <li><a data-toggle="collapse" data-target="#demoevent" href="#">Electrical Engineering</a>
                                    <ul id="demoevent" class="collapse dropdown-header-top">
                                        <li><a href="addWords.php">Add New Word</a></li>
                                        <li><a href="alphabetical.php">Alphabetical Words</a></li>
                                        <li><a href="addChapter.php">Add Chapter</a>
                                        </li>
                                        <li><a href="addChapterDetail.php">Add Chapter Detail</a>
                                        </li>
                                        <li><button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Click To Add New Word</button></li>
                                  
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu end -->
    <!-- Main Menu area start-->
    <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
                        <li><a data-toggle="tab" href="#Home"><i class="notika-icon notika-house"></i> Home</a>
                        </li>
                        <li class="active"><a data-toggle="tab" href="#mailbox"><i class="notika-icon notika-mail"></i>Electrical Engineering</a>
                        </li>
                    </ul>
                    <div class="tab-content custom-menu-content">
                        <div id="Home" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="../../dashboard.php">Dashboard</a>
                                </li>
                            </ul>
                        </div>
                        <div id="mailbox" class="tab-pane in active notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="addWords.php">Add New Word</a>
                                </li>
                                <li><a href="alphabetical.php">Alphabetical Words</a>
                                </li>
                                <li><a href="addChapter.php">Add Chapter</a>
                                </li>
                                <li><a href="addChapterDetail.php">Add Chapter Detail</a>
                                </li>
                               <li><button class="btn btn-primary" data-toggle="modal" data-target="#myModal1">Click To Add New Word</button></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Menu area End-->
    <!-- Start Email Statistic area-->
    <div class="notika-email-post-area">
        <div class="container">
            <div class="row">
                <?php 
                for ($alphabet='a'; $alphabet <=  'z'; $alphabet++) { 
                $result = mysqli_query($con,"SELECT * FROM word WHERE name LIKE '$alphabet%'");                     
                 ?>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12" style="height: 400px; margin-top: 10px;">
                    <div class="recent-signup-inner notika-shadow mg-t-30" style="height: 400px;">
                        <div class="widget-signup-list" style="height: 400px;">
                            <a class="signup-wd-mn" id="widget-signup-list" data-toggle="tooltip" data-placement="top" title="Words starting with alphabet <?php echo $alphabet; ?>" href="#" style="text-transform: capitalize;"><?php echo $alphabet; ?></a><hr>
                            <input type="hidden" id="alphabet" value="<?php echo $alphabet; ?>">
                            <?php 
                            $counter = 0;
                            if (mysqli_num_rows($result)==0) { ?>
                               <center><p>Sorry No Word Available</p></center>
                         <?php   }else{
                            while ($row = mysqli_fetch_array($result)) {
                                $counter++;
                             ?>
                         <a href="wordsDetail.php?id=<?php echo base64_encode($row['word_id']); ?>"><p style="text-transform: capitalize;color: black;"><?php echo $row['name']; ?></p><hr></a>
                         <?php if($counter > 4){ ?>
                          <center><button class="btn btn-default detailbtn">Show More</button></center>
                      <?php 
                            break;
                       } ?>
                             <?php } } ?>
                            
                           
                        </div>
                    </div>
                </div>

            <?php

            if ($alphabet == 'z') {
                        break;
                    }

             } ?>

            </div>
        </div>
    </div>

    <script>
        function f1(objbtn){
            alert(objbtn.value);
        }
    </script>


    <!-- End Email Statistic area-->
    <!-- Start Footer area-->
    <div class="footer-copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="footer-copy-right">
                        <p>Copyright © 2018 
. All rights reserved. Developed by <a href="http://www.pakisol.com">PakiSol</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer area-->
<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">List Of Words</h4><hr>
        </div>
        <div class="modal-body">
          
        </div>
       
      </div>
      
    </div>
  </div>
  
</div>


<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Insert New Word</h4>
        </div>
        <div class="modal-body">
         
          <form action="../../backend/addNewWord.php" method="POST" enctype="multipart/form-data">
           <div class="contact-form-int">
                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" class="form-control" name="wordname" placeholder="Enter Word Name" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" class="form-control" name="wordmeanings" placeholder="Enter Meaning Of Word" />
                                </div>
                            </div>
                             <div class="form-group">
                             <label title="Upload image file" for="inputImage" class="btn btn-primary img-cropper-cp">
                               <input type="file" accept="image/*" name="image" id="inputImage" class="hide form-control"> Upload Image (Optional)
                             </label>
                                               
                            </div> 
                            <div class="contact-btn">
                                <button class="button btn form-control" name="addwordbtn">Add Word</button>
                            </div>
            </div>
           </form>


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>

    <!-- jquery
		============================================ -->
    <script src="../../js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="../../js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="../../js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="../../js/jquery-price-slider.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="../../js/owl.carousel.min.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="../../js/jquery.scrollUp.min.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="../../js/meanmenu/jquery.meanmenu.js"></script>
    <!-- counterup JS
		============================================ -->
    <script src="../../js/counterup/jquery.counterup.min.js"></script>
    <script src="../../js/counterup/waypoints.min.js"></script>
    <script src="../../js/counterup/counterup-active.js"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="../../js/sparkline/jquery.sparkline.min.js"></script>
    <script src="../../js/sparkline/sparkline-active.js"></script>
    <!-- knob JS
		============================================ -->
    <script src="../../js/knob/jquery.knob.js"></script>
    <script src="../../js/knob/jquery.appear.js"></script>
    <script src="../../js/knob/knob-active.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="../../js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- flot JS
		============================================ -->
    <script src="../../js/flot/flot-widget-anatic-active.js"></script>
    <script src="../../js/flot/chart-tooltips.js"></script>
    <script src="../../js/flot/flot-active.js"></script>
    <!--  wave JS
		============================================ -->
    <script src="js/wave/waves.min.js"></script>
    <script src="../../js/wave/wave-active.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="../../js/plugins.js"></script>
    <!-- Google map JS
		============================================ -->
    <script src="../../js/google.maps/google.maps-active.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCVOIQ3qXUCmKVVV7DVexPzlgBcj5mQJmQ&callback=initMap"></script>
    <!-- main JS
		============================================ -->
    <script src="../../js/main.js"></script>

    <script>
$('.detailbtn').on('click',function(){
  var alphabet = $('#alphabet').val();
    if(alphabet){
    $('.modal-body').load('../../backend/showAlphabetDetails.php',{alphabet:alphabet},function(){
        $('#myModal').modal({show:true});
    });
  }else{
    alert('Some thing goes wrong');
  }
});
</script>
</body>

</html>