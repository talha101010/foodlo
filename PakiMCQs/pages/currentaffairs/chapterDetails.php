<?php 
session_start();
if (!isset($_SESSION['username'])) {
header('Location:../../index.php');
}
  require '../../backend/Connection.php';

 ?>

<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Questions | MCQ's App</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- favicon
        ============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="../../img/favicon.png">
    <!-- Google Fonts
        ============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <!-- font awesome CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <!-- owl.carousel CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/owl.carousel.css">
    <link rel="stylesheet" href="../../css/owl.theme.css">
    <link rel="stylesheet" href="../../css/owl.transitions.css">
    <!-- meanmenu CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/meanmenu/meanmenu.min.css">
    <!-- animate CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/animate.css">
    <!-- normalize CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/normalize.css">
    <!-- wave CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/wave/waves.min.css">
    <link rel="stylesheet" href="../../css/wave/button.css">
    <!-- mCustomScrollbar CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- Notika icon CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/notika-custom-icon.css">
    <!-- Data Table JS
        ============================================ -->
    <link rel="stylesheet" href="../../css/jquery.dataTables.min.css">
    <!-- main CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/main.css">
    <!-- style CSS
        ============================================ -->
    <link rel="stylesheet" href="../../style.css">
    <!-- responsive CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/responsive.css">
    <!-- modernizr JS
        ============================================ -->
    <script src="../../js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Start Header Top Area -->
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="logo-area">
                        <a href="../../dashboard.php"><img src="../../img/logo/logo.png" alt="" height="70" width="90" /></a>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="header-top-menu">
                        <ul class="nav navbar-nav notika-top-nav">
                        <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><span><i class="notika-icon notika-menus"></i></span></a>
                                <div role="menu" class="dropdown-menu message-dd task-dd animated zoomIn">
                                    <div class="hd-mg-tt">
                                        <h2>Admin</h2>
                                    </div>
                                    <div class="hd-message-info hd-task-info">
                                        <div class="skill">
                                            <form action="../../backend/logout.php" method="POST">
                                           <center><button class="btn btn-primary" name="logoutlink">Logout</button></center>
                                           </form>
                                        </div>
                                    </div>
                                   
                                </div>
                            </li>
                    
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Header Top Area -->
    <!-- Mobile Menu start -->
      <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul class="mobile-menu-nav">
                                <li><a data-toggle="collapse" data-target="#Charts" href="#">Home</a>
                                    <ul class="collapse dropdown-header-top">
                                        <li><a href="../../index.php">Dashboard</a></li>
                                    </ul>
                                </li>
                                <li><a data-toggle="collapse" data-target="#demoevent" href="#">Current Affairs</a>
                                    <ul id="demoevent" class="collapse dropdown-header-top">
                                <li><a href="addmcq.php">Add New MCQ</a>
                                </li>
                                <li><a href="addheading.php">Add Heading</a>
                                </li>
                                <li><a href="userQuestions.php">User MCQ's</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu end -->
    <!-- Main Menu area start-->
    <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
                        <li><a data-toggle="tab" href="#Home"><i class="notika-icon notika-house"></i> Home</a>
                        </li>
                        <li><a data-toggle="tab" href="#mailbox"><i class="notika-icon notika-mail"></i>Current Affairs</a>
                        </li>
                    </ul>
                    <div class="tab-content custom-menu-content">
                        <div id="Home" class="tab-pane in notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="../../dashboard.php">Dashboard</a>
                                </li>
                            </ul>
                        </div>
                        <div id="mailbox" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="addmcq.php">Add New MCQ</a>
                                </li>
                                <li><a href="addheading.php">Add Heading</a>
                                </li>
                               <li><a href="userQuestions.php">User MCQ's</a>
                                        </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php 
       if ($_GET['check']=='true') {
            $id =base64_decode($_GET['id']);
            $check = $_GET['check'];
        $result = mysqli_query($con,"SELECT * FROM mainheadings WHERE mainheading_id='$id'");
        $row = mysqli_fetch_array($result);
        echo mysqli_error($con);
         $mainheadingname = $row['mainheadingname'];
         $mainheadingno = $row['mainheadingno'];
         $type = $row['mainheading_type'];
         $image = $row['mainheading_image'];
       }else{
         $check = $_GET['check'];
        $id =base64_decode($_GET['id']);
        $result = mysqli_query($con,"SELECT * FROM headings WHERE heading_id='$id'");
        $row = mysqli_fetch_array($result);
         $mainheading = $row['mainheading'];
         $headingname = $row['headingname'];
         $headingno = $row['headingno'];
       }
       
     
     ?>

        <div class="contact-info-area mg-t-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="contact-form sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
                        <div class="contact-hd sm-form-hd">
                            <h2>Update Data</h2>
                            <p>Update or delete data from here if you want to update or delete it.</p>
                        </div>
            <?php if($check == 'true'){ ?>
           <form action="../../backend/addHeading.php" method="POST" enctype="multipart/form-data">
           <div class="contact-form-int">
                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="number" class="form-control" name="mainheadingno" placeholder="Enter No" value="<?php echo $mainheadingno; ?>" required />
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" class="form-control" name="mainheadingname" placeholder="Enter heading name" value="<?php echo $mainheadingname; ?>" required />
                                </div>
                            </div>

                             <div class="form-group">
                             <label title="Upload image file" for="inputImage" class="btn btn-primary img-cropper-cp">
                               <input type="file" accept="image/*" name="image" id="inputImage" class="hide form-control"> Upload Image (If you don't want to change skip it)
                             </label>
                                               
                            </div> 
                            <div class="contact-btn">
                                <input type="hidden" name="id" value="<?php echo $id; ?>">
                             
                                <input type="hidden" name="url" value="<?php echo $image; ?>">
                          
                                <button class="button btn" name="updateheadingbtn" style="width: 50%;">Update</button>
                                <button class="btn btn-danger" name="deleteheadingbtn" style="width: 49%;">Delete</button>
                                
                                
                            </div>
            </div>
           </form>
       <?php }else{ ?>

               <form action="../../backend/addHeading.php" method="POST">
           <div class="contact-form-int">
                       <div class="form-group">
                    <select name="mainheadingname" class="form-control" required>
                    <option value="">Select Main Heading</option>
                     <?php 
                     $result1 = mysqli_query($con,"SELECT mainheadingname FROM mainheadings");
                     while ($row = mysqli_fetch_array($result1)) { ?> 
                      <option value="<?php echo $row['mainheadingname']; ?>"<?php if ($mainheading == $row['mainheadingname']) echo ' selected="selected"'; ?>><?php echo $row['mainheadingname']; ?></option>


                   <?php   }  ?>
                     
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" class="form-control" name="headingname" placeholder="Enter headingname" value="<?php echo $headingname; ?>" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="number" class="form-control" name="headingno" placeholder="Enter headingno" value="<?php echo $headingno; ?>" required />
                                </div>
                            </div>
                            <div class="contact-btn">
                                <input type="hidden" name="id" value="<?php echo $id; ?>">
                          
                                <button class="button btn" name="updatesubheadingbtn" style="width: 50%;">Update</button>
                                <button class="btn btn-danger" name="deletesubheadingbtn" style="width: 49%;">Delete</button>
                                
                                
                            </div>
            </div>
           </form>

       <?php } ?>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="widget-tabs-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-ds-n dk-res-ds">
                        <div class="contact-hd tm-activity">
                            <?php if($check == 'true'){ ?>
                            <h2>Main Heading : <?php echo $mainheadingname; ?></h2>
                            <p>Number :<?php echo $mainheadingno; ?></p>
                        <?php }else{ ?>
                           <h2><b>Main Heading:</b><i><?php echo $mainheading; ?></i></h2>
                           
                           <h5><b>Sub Heading:</b><i><?php echo $headingname; ?></i></h5>
                           <p>Number:<i><?php echo $headingno; ?></i></p>
                        <?php } ?>
                        </div>
                        <div class="widget-tabs-list">
                            <div class="tab-content">
                                <div id="home" class="tab-pane fade in active">
                                    <div class="tab-wd-img">
                                      <?php if($check == 'true'){ ?>
                                        <img src="../../headingimages/<?php echo $image; ?>" alt="" />
                                        <center><a href="addHeading.php">Go Back</a></center>
                                    <?php }else{ ?>
                                    <center><a href="addHeading.php#topicid">Go Back</a></center>
                                <?php } ?>
                                    </div>
                                </div>
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Data Table area End-->
    <!-- Start Footer area-->
    <div class="footer-copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="footer-copy-right">
                        <p>Copyright © 2018 
. All rights reserved. Developed By <a href="http://www.pakisol.com">PakiSol</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer area-->


    <!-- jquery
        ============================================ -->
    <script src="../../js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
        ============================================ -->
    <script src="../../js/bootstrap.min.js"></script>
    <!-- wow JS
        ============================================ -->
    <script src="../../js/wow.min.js"></script>
    <!-- price-slider JS
        ============================================ -->
    <script src="../../js/jquery-price-slider.js"></script>
    <!-- owl.carousel JS
        ============================================ -->
    <script src="../../js/owl.carousel.min.js"></script>
    <!-- scrollUp JS
        ============================================ -->
    <script src="../../js/jquery.scrollUp.min.js"></script>
    <!-- meanmenu JS
        ============================================ -->
    <script src="../../js/meanmenu/jquery.meanmenu.js"></script>
    <!-- counterup JS
        ============================================ -->
    <script src="../../js/counterup/jquery.counterup.min.js"></script>
    <script src="../../js/counterup/waypoints.min.js"></script>
    <script src="../../js/counterup/counterup-active.js"></script>
    <!-- mCustomScrollbar JS
        ============================================ -->
    <script src="../../js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- sparkline JS
        ============================================ -->
    <script src="../../js/sparkline/jquery.sparkline.min.js"></script>
    <script src="../../js/sparkline/sparkline-active.js"></script>
    <!-- flot JS
        ============================================ -->
    <script src="../../js/flot/jquery.flot.js"></script>
    <script src="../../js/flot/jquery.flot.resize.js"></script>
    <script src="../../js/flot/flot-active.js"></script>
    <!-- knob JS
        ============================================ -->
    <script src="../../js/knob/jquery.knob.js"></script>
    <script src="../../js/knob/jquery.appear.js"></script>
    <script src="../../js/knob/knob-active.js"></script>
    <!--  Chat JS
        ============================================ -->
    <script src="../../js/chat/jquery.chat.js"></script>
    <!--  todo JS
        ============================================ -->
    <script src="../../js/todo/jquery.todo.js"></script>
    <!--  wave JS
        ============================================ -->
    <script src="../../js/wave/waves.min.js"></script>
    <script src="../../js/wave/wave-active.js"></script>
    <!-- plugins JS
        ============================================ -->
    <script src="../../js/plugins.js"></script>
    <!-- Data Table JS
        ============================================ -->
    <script src="../../js/data-table/jquery.dataTables.min.js"></script>
    <script src="../../js/data-table/data-table-act.js"></script>
    <!-- main JS
        ============================================ -->
    <script src="../../js/main.js"></script>
    <!-- tawk chat JS
        ============================================ -->
</body>

</html>