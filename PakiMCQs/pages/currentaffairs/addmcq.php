<?php 
session_start();
if (!isset($_SESSION['username'])) {
header('Location:../../index.php');
}
  require '../../backend/Connection.php';

 ?>

<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Words | PakiMCQ's</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- favicon
        ============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="../../img/favicon.png">
    <!-- Google Fonts
        ============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <!-- font awesome CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <!-- owl.carousel CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/owl.carousel.css">
    <link rel="stylesheet" href="../../css/owl.theme.css">
    <link rel="stylesheet" href="../../css/owl.transitions.css">
    <!-- meanmenu CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/meanmenu/meanmenu.min.css">
    <!-- animate CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/animate.css">
    <!-- normalize CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/normalize.css">
    <!-- wave CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/wave/waves.min.css">
    <link rel="stylesheet" href="../../css/wave/button.css">
    <!-- mCustomScrollbar CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- Notika icon CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/notika-custom-icon.css">
    <!-- Data Table JS
        ============================================ -->
    <link rel="stylesheet" href="../../css/jquery.dataTables.min.css">
    <!-- main CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/main.css">
    <!-- style CSS
        ============================================ -->
    <link rel="stylesheet" href="../../style.css">
    <!-- responsive CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/responsive.css">
    <!-- modernizr JS
        ============================================ -->
    <script src="../../js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Start Header Top Area -->
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="logo-area">
                        <a href="../../dashboard.php"><img src="../../img/logo/logo.png" alt="" height="70" width="90" /></a>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="header-top-menu">
                        <ul class="nav navbar-nav notika-top-nav">
                        <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><span><i class="notika-icon notika-menus"></i></span></a>
                                <div role="menu" class="dropdown-menu message-dd task-dd animated zoomIn">
                                    <div class="hd-mg-tt">
                                        <h2>Admin</h2>
                                    </div>
                                    <div class="hd-message-info hd-task-info">
                                        <div class="skill">
                                            <form action="../../backend/logout.php" method="POST">
                                           <center><button class="btn btn-primary" name="logoutlink">Logout</button></center>
                                           </form>
                                        </div>
                                    </div>
                                   
                                </div>
                            </li>
                    
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Header Top Area -->
    <!-- Mobile Menu start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul class="mobile-menu-nav">
                                <li><a data-toggle="collapse" data-target="#Charts" href="#">Home</a>
                                    <ul class="collapse dropdown-header-top">
                                        <li><a href="../../dashboard.php">Dashboard</a></li>
                                    </ul>
                                </li>
                                <li><a data-toggle="collapse" data-target="#demoevent" href="#">Current Affairs</a>
                                    <ul id="demoevent" class="collapse dropdown-header-top">
                                        <li><a href="addmcq.php">Add New MCQ</a></li>
                                        <li><a href="addHeading.php">Add Heading</a>
                                        </li>
                                        <li><a href="userQuestions.php">User MCQ's</a>
                                        </li>
                                        <li><button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Click To Add New MCQ</button></li>
                                    </ul>
                                </li>
                               
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu end -->
    <!-- Main Menu area start-->
    <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
                        <li><a data-toggle="tab" href="#Home"><i class="notika-icon notika-house"></i> Home</a>
                        </li>
                        <li class="active"><a data-toggle="tab" href="#mailbox"><i class="notika-icon notika-mail"></i>Current Affairs</a>
                        </li>
                    </ul>
                    <div class="tab-content custom-menu-content">
                        <div id="Home" class="tab-pane in notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="../../dashboard.php">Dashboard</a>
                                </li>
                            </ul>
                        </div>
                        <div id="mailbox" class="tab-pane in active notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="addmcq.php">Add New MCQ</a>
                                </li>
                                <li><a href="addHeading.php">Add Heading</a>
                                </li>
                                <li><a href="userQuestions.php">User MCQ's</a>
                                </li>
                                <li><button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Click To Add New MCQ</button></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">
                        <div class="basic-tb-hd">
                            <h2>List Of Published Questions.</h2>
                        </div>
                        <?php 
                        $wordresult = mysqli_query($con,"SELECT * FROM questions ORDER BY question_id DESC");
                         ?>
                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Question</th>
                                        <th class="text-center" style="width: 10px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if (mysqli_num_rows($wordresult) > 0) {
                                    while ($wordrow = mysqli_fetch_array($wordresult)) {
                                     ?>
                                    <tr>
                                        <td><?php echo $wordrow['question']; ?></td>
                                        <td>
                                            <form action="questionDetail.php" method="GET">
                                            <input type="hidden" name="id" value="<?php echo base64_encode($wordrow['question_id']); ?>">
                                            <input type="hidden" name="check" value="true">
                                            <button class="btn btn-success">Detail</button>
                                            </form>
                                        </td>
                                    </tr>
                                   <?php } } ?>
                                  
                                </tbody>
                                
                                <tfoot>
                                    <tr>
                                        <th>Question</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Data Table area End-->
    <!-- Start Footer area-->
    <div class="footer-copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="footer-copy-right">
                        <p>Copyright © 2018 
. All rights reserved. Developed By <a href="http://www.pakisol.com">PakiSol</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer area-->

<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Insert New Word</h4>
        </div>
        <div class="modal-body">
         
          <form action="../../backend/addNewQuestion.php" method="POST">
           <div class="contact-form-int">
                             <div class="form-group">
                                <select name="mainheading1" id="mainheading1" class="form-control" required>
                                    <option value="">Select Main Heading</option>
                                    <?php 
                                    $result1 = mysqli_query($con,"SELECT mainheadingname FROM mainheadings ORDER BY mainheadingno ASC");
                                      while ($row1 = mysqli_fetch_array($result1)) { ?>
                                        <option value="<?php echo $row1['mainheadingname'];?>"><?php echo $row1['mainheadingname']; ?></option>
                                     <?php }
                                     ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <select name="subheading1" id="subheading1" class="form-control" required>
                                    <option value="">Select Main Heading First</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" class="form-control" name="question" placeholder="Enter Question" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" class="form-control" name="optiona" placeholder="Option A" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" class="form-control" name="optionb" placeholder="Option B" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" class="form-control" name="optionc" placeholder="Option C" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" class="form-control" name="optiond" placeholder="Option D" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" class="form-control" name="correctoption" placeholder="Correct Option" required/>
                                </div>
                            </div> 
                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                   <textarea class="form-control" cols="15" row="2" placeholder="Enter explanation if any" name="explanation"></textarea>
                                </div>
                            </div>
                            <div class="contact-btn">
                                <button class="button btn form-control" name="addquestionbtn">Add Question</button>
                            </div>
            </div>
           </form>


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>


    <!-- jquery
        ============================================ -->
    <script src="../../js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
        ============================================ -->
    <script src="../../js/bootstrap.min.js"></script>
    <!-- wow JS
        ============================================ -->
    <script src="../../js/wow.min.js"></script>
    <!-- price-slider JS
        ============================================ -->
    <script src="../../js/jquery-price-slider.js"></script>
    <!-- owl.carousel JS
        ============================================ -->
    <script src="../../js/owl.carousel.min.js"></script>
    <!-- scrollUp JS
        ============================================ -->
    <script src="../../js/jquery.scrollUp.min.js"></script>
    <!-- meanmenu JS
        ============================================ -->
    <script src="../../js/meanmenu/jquery.meanmenu.js"></script>
    <!-- counterup JS
        ============================================ -->
    <script src="../../js/counterup/jquery.counterup.min.js"></script>
    <script src="../../js/counterup/waypoints.min.js"></script>
    <script src="../../js/counterup/counterup-active.js"></script>
    <!-- mCustomScrollbar JS
        ============================================ -->
    <script src="../../js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- sparkline JS
        ============================================ -->
    <script src="../../js/sparkline/jquery.sparkline.min.js"></script>
    <script src="../../js/sparkline/sparkline-active.js"></script>
    <!-- flot JS
        ============================================ -->
    <script src="../../js/flot/jquery.flot.js"></script>
    <script src="../../js/flot/jquery.flot.resize.js"></script>
    <script src="../../js/flot/flot-active.js"></script>
    <!-- knob JS
        ============================================ -->
    <script src="../../js/knob/jquery.knob.js"></script>
    <script src="../../js/knob/jquery.appear.js"></script>
    <script src="../../js/knob/knob-active.js"></script>
    <!--  Chat JS
        ============================================ -->
    <script src="../../js/chat/jquery.chat.js"></script>
    <!--  todo JS
        ============================================ -->
    <script src="../../js/todo/jquery.todo.js"></script>
    <!--  wave JS
        ============================================ -->
    <script src="../../js/wave/waves.min.js"></script>
    <script src="../../js/wave/wave-active.js"></script>
    <!-- plugins JS
        ============================================ -->
    <script src="../../js/plugins.js"></script>
    <!-- Data Table JS
        ============================================ -->
    <script src="../../js/data-table/jquery.dataTables.min.js"></script>
    <script src="../../js/data-table/data-table-act.js"></script>
    <!-- main JS
        ============================================ -->
    <script src="../../js/main.js"></script>
    <!-- tawk chat JS
        ============================================ -->

     <script type="text/javascript">
              $(document).ready(function(){
                $('#mainheading1').change(function(){
                  var mainheading1 = $(this).val();
                  if(mainheading1){
                    $.ajax({
                      method:'POST',
                      dataType:'text',
                      url:'../../backend/getDataFromServer.php',
                      data:{main_heading:mainheading1},
                      success:function(data){
                        $('#subheading1').html(data);
                      }
                    });
                  }else{
                    $('#subheading1').html('<option value="">Select Main Heading First</option>');
                  }
                });
              });
    </script>


</body>

</html>