<?php 
  require 'scripthead.php';
  if(isset($_POST['addquestionbtn'])) {
  	$mainheading =mysqli_real_escape_string($con,$_POST['mainheading1']);
    $question = mysqli_real_escape_string($con,$_POST['question']);
    $optiona = mysqli_real_escape_string($con,$_POST['optiona']);
    $optionb = mysqli_real_escape_string($con,$_POST['optionb']);
    $optionc = mysqli_real_escape_string($con,$_POST['optionc']);
    $optiond = mysqli_real_escape_string($con,$_POST['optiond']);
    $correctoption = mysqli_real_escape_string($con,$_POST['correctoption']);
    $subheading = mysqli_real_escape_string($con,$_POST['subheading1']);
    $explanation = mysqli_real_escape_string($con,$_POST['explanation']);
    if ($explanation == "") {
      $explanation = "null";
    }
  	 
    $checkexistence = mysqli_query($con,"SELECT question_id FROM questions WHERE mainheading='$mainheading' AND subheading='$subheading' AND question='$question'");
     if (mysqli_num_rows($checkexistence) == 0) {
         $checkexistence1 = mysqli_query($con,"SELECT question_id FROM questions WHERE mainheading='$mainheading' AND subheading='$subheading'");
         if (mysqli_num_rows($checkexistence1) > 14) { ?>

         <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <script type="text/javascript">
             swal({
        title: "Sorry Exceeds",
        text: "15 Question already completed in this section",
        icon: "error",button: "close"
      }).then(function() {
// Redirect the user
window.location.href = "../pages/currentaffairs/addmcq.php";
//console.log('The Ok Button was clicked.');
});
        </script>
             
      <?php   }else{

       $query = "INSERT INTO questions (mainheading,subheading,question,option_a,option_b,option_c,option_d,correct_option,explanation,owner) VALUES ('$mainheading','$subheading','$question','$optiona','$optionb','$optionc','$optiond','$correctoption','$explanation','admin')";
  	if (mysqli_query($con,$query)) { ?>
  		
  		 <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <script type="text/javascript">
             swal({
        title: "Question Added",
        text: "Question has been added successfully",
        icon: "success",button: "close"
      }).then(function() {
// Redirect the user
window.location.href = "../pages/currentaffairs/addmcq.php";
//console.log('The Ok Button was clicked.');
});
        </script>
                  
  	 <?php  }else{ ?>

  	   	   <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <script type="text/javascript">
             swal({
        title: "Sorry",
        text: "Error in insertion process",
        icon: "error",button: "close"
      }).then(function() {
// Redirect the user
window.location.href = "../pages/currentaffairs/addmcq.php";
//console.log('The Ok Button was clicked.');
});
        </script>

  <?php	
  
}
}
}else{ ?>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <script type="text/javascript">
             swal({
        title: "Sorry",
        text: "This Question already exist",
        icon: "error",button: "close"
      }).then(function() {
// Redirect the user
window.location.href = "../pages/currentaffairs/addmcq.php";
//console.log('The Ok Button was clicked.');
});
        </script>

<?php }
  }


    if(isset($_POST['addquestionbtnone'])) {
    $mainheading =mysqli_real_escape_string($con,$_POST['mainheading']);
    $question = mysqli_real_escape_string($con,$_POST['question']);
    $optiona = mysqli_real_escape_string($con,$_POST['optiona']);
    $optionb = mysqli_real_escape_string($con,$_POST['optionb']);
    $optionc = mysqli_real_escape_string($con,$_POST['optionc']);
    $optiond = mysqli_real_escape_string($con,$_POST['optiond']);
    $correctoption = mysqli_real_escape_string($con,$_POST['answer']);
    $subheading = mysqli_real_escape_string($con,$_POST['subheading']);
    $explanation = mysqli_real_escape_string($con,$_POST['explanation']);
    if ($explanation == "") {
      $explanation = "null";
    }
     
    $checkexistence = mysqli_query($con,"SELECT question_id FROM questions WHERE mainheading='$mainheading' AND subheading='$subheading' AND question='$question'");
     if (mysqli_num_rows($checkexistence) == 0) {
         $checkexistence1 = mysqli_query($con,"SELECT question_id FROM questions WHERE mainheading='$mainheading' AND subheading='$subheading'");
         if (mysqli_num_rows($checkexistence1) > 14) { ?>

         <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <script type="text/javascript">
             swal({
        title: "Sorry Exceeds",
        text: "15 Question already completed in this section",
        icon: "error",button: "close"
      }).then(function() {
// Redirect the user
window.location.href = "../pages/currentaffairs/addmcq.php";
//console.log('The Ok Button was clicked.');
});
        </script>
             
      <?php   }else{

       $query = "INSERT INTO questions (mainheading,subheading,question,option_a,option_b,option_c,option_d,correct_option,explanation,owner) VALUES ('$mainheading','$subheading','$question','$optiona','$optionb','$optionc','$optiond','$correctoption','$explanation','admin')";
    if (mysqli_query($con,$query)) { ?>
      
       <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <script type="text/javascript">
             swal({
        title: "Question Added",
        text: "Question has been added successfully",
        icon: "success",button: "close"
      }).then(function() {
// Redirect the user
window.location.href = "../pages/currentaffairs/addmcq.php";
//console.log('The Ok Button was clicked.');
});
        </script>
                  
     <?php  }else{ ?>

           <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <script type="text/javascript">
             swal({
        title: "Sorry",
        text: "Error in insertion process",
        icon: "error",button: "close"
      }).then(function() {
// Redirect the user
window.location.href = "../pages/currentaffairs/addmcq.php";
//console.log('The Ok Button was clicked.');
});
        </script>

  <?php 
  
}
}
}else{ ?>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <script type="text/javascript">
             swal({
        title: "Sorry",
        text: "This Question already exist",
        icon: "error",button: "close"
      }).then(function() {
// Redirect the user
window.location.href = "../pages/currentaffairs/addmcq.php";
//console.log('The Ok Button was clicked.');
});
        </script>

<?php }
  }




    if(isset($_POST['approvequestionbtn'])) {
      $id = $_POST['id'];
    $mainheading =mysqli_real_escape_string($con,$_POST['mainheading']);
    $question = mysqli_real_escape_string($con,$_POST['question']);
    $optiona = mysqli_real_escape_string($con,$_POST['optiona']);
    $optionb = mysqli_real_escape_string($con,$_POST['optionb']);
    $optionc = mysqli_real_escape_string($con,$_POST['optionc']);
    $optiond = mysqli_real_escape_string($con,$_POST['optiond']);
    $correctoption = mysqli_real_escape_string($con,$_POST['answer']);
    $subheading = mysqli_real_escape_string($con,$_POST['subheading']);
    $explanation = mysqli_real_escape_string($con,$_POST['explanation']);
     
    $checkexistence = mysqli_query($con,"SELECT question_id FROM questions WHERE mainheading='$mainheading' AND subheading='$subheading' AND question='$question'");
     if (mysqli_num_rows($checkexistence) == 0) {
         $checkexistence1 = mysqli_query($con,"SELECT question_id FROM questions WHERE mainheading='$mainheading' AND subheading='$subheading'");
         if (mysqli_num_rows($checkexistence1) > 14) { ?>

         <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <script type="text/javascript">
             swal({
        title: "Sorry Exceeds",
        text: "15 Question already completed in this section",
        icon: "error",button: "close"
      }).then(function() {
// Redirect the user
window.location.href = "../pages/currentaffairs/questionDetail.php?id=<?php echo base64_encode($id); ?>&check=false";
//console.log('The Ok Button was clicked.');
});
        </script>
             
      <?php   }else{

       $query = "INSERT INTO questions (mainheading,subheading,question,option_a,option_b,option_c,option_d,correct_option,explanation,owner) VALUES ('$mainheading','$subheading','$question','$optiona','$optionb','$optionc','$optiond','$correctoption','$explanation','user')";
    if (mysqli_query($con,$query)) {
            mysqli_query($con,"DELETE FROM users_questions WHERE question_id = '$id'");

     ?>
      
       <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <script type="text/javascript">
             swal({
        title: "Question Added",
        text: "Question has been Approved successfully",
        icon: "success",button: "close"
      }).then(function() {
// Redirect the user
window.location.href = "../pages/currentaffairs/userQuestions.php";
//console.log('The Ok Button was clicked.');
});
        </script>
                  
     <?php  }else{ ?>

           <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <script type="text/javascript">
             swal({
        title: "Sorry",
        text: "Error in insertion process",
        icon: "error",button: "close"
      }).then(function() {
// Redirect the user
window.location.href = "../pages/currentaffairs/questionDetail.php?id=<?php echo base64_encode($id); ?>&check=false";
//console.log('The Ok Button was clicked.');
});
        </script>

  <?php 
  
}
}
}else{ ?>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <script type="text/javascript">
             swal({
        title: "Sorry",
        text: "This Question already exist",
        icon: "error",button: "close"
      }).then(function() {
// Redirect the user
window.location.href = "../pages/currentaffairs/questionDetail.php?id=<?php echo base64_encode($id); ?>&check=false";
//console.log('The Ok Button was clicked.');
});
        </script>

<?php }
  }





  if (isset($_POST['updatequestionbtn'])) {
    $id = $_POST['id'];
    $mainheading =mysqli_real_escape_string($con,$_POST['mainheading']);
    $question = mysqli_real_escape_string($con,$_POST['question']);
    $optiona = mysqli_real_escape_string($con,$_POST['optiona']);
    $optionb = mysqli_real_escape_string($con,$_POST['optionb']);
    $optionc = mysqli_real_escape_string($con,$_POST['optionc']);
    $optiond = mysqli_real_escape_string($con,$_POST['optiond']);
    $correctoption = mysqli_real_escape_string($con,$_POST['answer']);
    $subheading = mysqli_real_escape_string($con,$_POST['subheading']);
    $explanation = mysqli_real_escape_string($con,$_POST['explanation']);
      
      try {
        $query = "UPDATE questions SET mainheading='$mainheading', subheading='$subheading', question='$question', option_a='$optiona', option_b='$optionb',option_c='$optionc',option_d='$optiond',correct_option='$correctoption',explanation='$explanation' WHERE question_id='$id'";
        
      } catch (Exception $e) {
        
      }

    if (mysqli_query($con,$query)) { ?>
         

          <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <script type="text/javascript">
             swal({
        title: "Updated",
        text: "Question has been updated successfully",
        icon: "success",button: "close"
      }).then(function() {
// Redirect the user
window.location.href = "../pages/currentaffairs/questionDetail.php?id=<?php echo base64_encode($id); ?>&check=true";
//console.log('The Ok Button was clicked.');
});
        </script>
                  
     <?php  }else{ ?>

           <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <script type="text/javascript">
             swal({
        title: "Sorry",
        text: "Error in updation process",
        icon: "error",button: "close"
      }).then(function() {
// Redirect the user
window.location.href = "../pages/currentaffairs/questionDetail.php?id=<?php echo base64_encode($id); ?>&check=true";
//console.log('The Ok Button was clicked.');
});
        </script>


    <?php  }

  }

  if (isset($_POST['deletequestionbtn'])) {
    $id = $_POST['id'];
     
     if (mysqli_query($con,"DELETE FROM questions WHERE question_id = '$id'")) {?> 
           <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <script type="text/javascript">
             swal({
        title: "Deleted",
        text: "Question has been deleted successfully",
        icon: "success",button: "close"
      }).then(function() {
// Redirect the user
window.location.href = "../pages/currentaffairs/addmcq.php";
//console.log('The Ok Button was clicked.');
});
        </script>
                  
     <?php  }else{ ?>

           <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <script type="text/javascript">
             swal({
        title: "Sorry",
        text: "Error in deletion process",
        icon: "error",button: "close"
      }).then(function() {
// Redirect the user
window.location.href = "../pages/currentaffairs/addmcq.php";
//console.log('The Ok Button was clicked.');
});
        </script>

  <?php   }

  }

    if (isset($_POST['deleteuserquestion'])) {
    $id = $_POST['id'];
     
     if (mysqli_query($con,"DELETE FROM users_questions WHERE question_id = '$id'")) {?> 
           <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <script type="text/javascript">
             swal({
        title: "Deleted",
        text: "Question has been deleted successfully",
        icon: "success",button: "close"
      }).then(function() {
// Redirect the user
window.location.href = "../pages/currentaffairs/userQuestions.php";
//console.log('The Ok Button was clicked.');
});
        </script>
                  
     <?php  }else{ ?>

           <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <script type="text/javascript">
             swal({
        title: "Sorry",
        text: "Error in deletion process",
        icon: "error",button: "close"
      }).then(function() {
// Redirect the user
window.location.href = "../pages/currentaffairs/userQuestions.php";
//console.log('The Ok Button was clicked.');
});
        </script>

  <?php   }

  }


    if (isset($_POST['changepasswordbtn'])) {
    $oldpassword =mysqli_real_escape_string($con,$_POST['oldpassword']);
    $newpassword = mysqli_real_escape_string($con,$_POST['newpassword']);
    $oldpassword = md5($oldpassword);
    $newpassword = md5($newpassword);
      $result = mysqli_query($con,"SELECT id FROM admin WHERE password = '$oldpassword'");
      if (mysqli_num_rows($result)>0) {
      try {
        $query = "UPDATE admin SET password = '$newpassword' WHERE password = '$oldpassword'";
        
      } catch (Exception $e) {
        
      }

    if (mysqli_query($con,$query)) { ?>
         

          <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <script type="text/javascript">
             swal({
        title: "Changed",
        text: "Password has been changed successfully",
        icon: "success",button: "close"
      }).then(function() {
// Redirect the user
window.location.href = "../dashboard.php";
//console.log('The Ok Button was clicked.');
});
        </script>
                  
     <?php  }else{ ?>

           <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <script type="text/javascript">
             swal({
        title: "Sorry",
        text: "Error in updation process",
        icon: "error",button: "close"
      }).then(function() {
// Redirect the user
window.location.href = "../dashboard.php";
//console.log('The Ok Button was clicked.');
});
        </script>


    <?php  }
  }else{ ?>

              <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <script type="text/javascript">
             swal({
        title: "Sorry",
        text: "Old password not matched",
        icon: "error",button: "close"
      }).then(function() {
// Redirect the user
window.location.href = "../dashboard.php";
//console.log('The Ok Button was clicked.');
});
        </script>

 <?php }
  }



 ?>