package Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.talha.pakimcqs.ListActivity;
import com.example.talha.pakimcqs.NumberActivity;
import com.example.talha.pakimcqs.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import ListItems.MainHeadinListItems;

public class MainHeadingAdapter extends RecyclerView.Adapter<MainHeadingAdapter.ViewHolder> {
    List<MainHeadinListItems> list;
    Context context;

    public MainHeadingAdapter(List<MainHeadinListItems> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
       View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.options_list_items,viewGroup,false);
       return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
      final MainHeadinListItems listItems = list.get(i);
      viewHolder.headingname.setText(listItems.getName());
        Picasso.get().load(listItems.getImage()).fit().into(viewHolder.image, new Callback() {
            @Override
            public void onSuccess() {
             viewHolder.progressbar.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {

            }
        });

        viewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (listItems.getType() == 1){
                   Intent i = new Intent(context,ListActivity.class);
                   i.putExtra("headingname",listItems.getName());
                   context.startActivity(i);
               }else if (listItems.getType()==0){
                   Intent i = new Intent(context,NumberActivity.class);
                   i.putExtra("headingname",listItems.getName());
                   context.startActivity(i);
               }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView headingname;
        ImageView image;
        RelativeLayout relativeLayout;
        ProgressBar progressbar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            headingname = itemView.findViewById(R.id.mainheadingname);
            image = itemView.findViewById(R.id.headingimageview);
            relativeLayout = itemView.findViewById(R.id.relativelayout1);
            progressbar = itemView.findViewById(R.id.progressbar);
        }
    }
}
