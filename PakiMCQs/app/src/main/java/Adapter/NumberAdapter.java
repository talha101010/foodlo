package Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.talha.pakimcqs.R;
import com.example.talha.pakimcqs.ShowQuestions;

import java.util.List;

import ListItems.NumberListitems;

public class NumberAdapter extends RecyclerView.Adapter<NumberAdapter.ViewHolder> {
    private List<NumberListitems> list;
    private Context context;

    public NumberAdapter(List<NumberListitems> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
     View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.number_listitems,viewGroup,false);
     return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
      final NumberListitems listitems = list.get(i);
      viewHolder.numbertext.setText(listitems.getName());
      viewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              Intent i = new Intent(context,ShowQuestions.class);
              i.putExtra("mainheading",listitems.getMainheading());
              i.putExtra("subheading",listitems.getName());
              context.startActivity(i);
          }
      });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView numbertext;
        RelativeLayout relativeLayout;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            numbertext = itemView.findViewById(R.id.numbertext);
            relativeLayout = itemView.findViewById(R.id.relativelayout1);
        }
    }
}
