package Adapter;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.talha.pakimcqs.PromptMessage;
import com.example.talha.pakimcqs.R;

import java.util.List;

import ListItems.Questions_Listitems;

public class QuestionsAdapter extends RecyclerView.Adapter<QuestionsAdapter.ViewHolder> {
    public static List<Questions_Listitems> list;
    private Context context;
    int row_index = -1;

    public QuestionsAdapter(List<Questions_Listitems> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
       View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.questions_listitems,viewGroup,false);
       return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        final Questions_Listitems listitems = list.get(i);
        viewHolder.questionnumber.setText("Question:"+listitems.getCount()+"/"+list.size());
        viewHolder.question.setText(listitems.getQuestion());
        viewHolder.optiona.setText(listitems.getOptiona());
        viewHolder.optionb.setText(listitems.getOptionb());
        viewHolder.optionc.setText(listitems.getOptionc());
        viewHolder.optiond.setText(listitems.getOptiond());
          viewHolder.explanationbtn.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  if (listitems.getExplanation().equals("null")){
                      PromptMessage.showDialog(context,"No explanation available");
                  }else{
                      PromptMessage.showDialog(context,listitems.getExplanation());
                  }
              }
          });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
       public TextView question,optiona,optionb,optionc,optiond,questionnumber;
        Button explanationbtn;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            question = itemView.findViewById(R.id.question);
            optiona = itemView.findViewById(R.id.optiona);
            optionb = itemView.findViewById(R.id.optionb);
            optionc = itemView.findViewById(R.id.optionc);
            optiond = itemView.findViewById(R.id.optiond);
            explanationbtn = itemView.findViewById(R.id.explanationbtn);

            questionnumber = itemView.findViewById(R.id.questionnumber);

        }


    }
}
