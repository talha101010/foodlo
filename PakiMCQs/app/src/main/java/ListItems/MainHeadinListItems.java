package ListItems;

public class MainHeadinListItems {
    private String name,image;
    private int type;

    public MainHeadinListItems(String name, String image,int type) {
        this.name = name;
        this.image = image;
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }
}
