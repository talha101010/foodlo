package ListItems;

public class NumberListitems {
    private String name,mainheading;

    public NumberListitems(String name,String mainheading) {
        this.name = name;
        this.mainheading = mainheading;
    }

    public String getName() {
        return name;
    }

    public String getMainheading() {
        return mainheading;
    }
}
