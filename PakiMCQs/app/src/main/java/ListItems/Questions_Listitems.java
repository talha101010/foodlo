package ListItems;

public class Questions_Listitems {
    private String id,mainheading,subheading,question,optiona,optionb,optionc,optiond,correctoprion,explanation;
    private int count;

    public Questions_Listitems(String id, String mainheading, String subheading, String question, String optiona, String optionb, String optionc, String optiond, String correctoprion, String explanation) {
        this.id = id;
        this.mainheading = mainheading;
        this.subheading = subheading;
        this.question = question;
        this.optiona = optiona;
        this.optionb = optionb;
        this.optionc = optionc;
        this.optiond = optiond;
        this.correctoprion = correctoprion;
        this.explanation = explanation;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public String getId() {
        return id;
    }

    public String getMainheading() {
        return mainheading;
    }

    public String getSubheading() {
        return subheading;
    }

    public String getQuestion() {
        return question;
    }

    public String getOptiona() {
        return optiona;
    }

    public String getOptionb() {
        return optionb;
    }

    public String getOptionc() {
        return optionc;
    }

    public String getOptiond() {
        return optiond;
    }

    public String getCorrectoprion() {
        return correctoprion;
    }

    public String getExplanation() {
        return explanation;
    }
}
