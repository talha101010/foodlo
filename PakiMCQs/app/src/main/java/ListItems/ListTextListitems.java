package ListItems;

public class ListTextListitems {
    private String mainheading,headingname;

    public ListTextListitems(String mainheading, String headingname) {
        this.mainheading = mainheading;
        this.headingname = headingname;
    }

    public String getMainheading() {
        return mainheading;
    }

    public String getHeadingname() {
        return headingname;
    }
}
