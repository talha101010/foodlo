package com.example.talha.pakimcqs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AddMcqs extends AppCompatActivity {
   private Spinner categoryspinner;
   private EditText question,optiona,optionb,optionc,optiond,explanation,answer;
   private Button addmcqs;
   private ArrayList<String> categorylist;
   private ArrayAdapter<String> categoryadapter;
   private ProgressBar progressBar;
   private String explain;
   private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_mcqs);
        views();
        categoryadapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,categorylist);
          getCategory();
          addmcqs.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  if (categoryspinner.getSelectedItemPosition()==0){
                      Toast.makeText(AddMcqs.this, "Please select any category", Toast.LENGTH_SHORT).show();
                  }else if (question.getText().length()==0){
                      question.setError("This field is required");
                  }else if (optiona.getText().length()==0){
                      optiona.setError("This field is required");
                  }else if (optionb.getText().length()==0){
                      optionb.setError("This field is required");
                  }else if (optionc.getText().length()==0){
                      optionc.setError("This field is required");
                  }else if (optiond.getText().length()==0){
                      optiond.setError("This field is required");
                  }else if (answer.getText().length()==0) {
                       answer.setError("This field is required");
                  }else{
                          if (explanation.getText().toString().equals("")){
                              explain = "null";
                          }else{
                              explain = explanation.getText().toString().trim();
                          }
                          insetMcqToServer();
                      }
                  }
          });
    }
    private void views(){
        categoryspinner = findViewById(R.id.categoryspinner);
        question = findViewById(R.id.question);
        optiona = findViewById(R.id.optiona);
        optionb = findViewById(R.id.optionb);
        optionc = findViewById(R.id.optionc);
        optiond = findViewById(R.id.optiond);
        explanation = findViewById(R.id.explanation);
        addmcqs = findViewById(R.id.addmcqs);
        categorylist = new ArrayList<>();
        progressBar = findViewById(R.id.progressbar);
        answer = findViewById(R.id.answer);
        activity = AddMcqs.this;
    }

    private void getCategory(){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.Main_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("mainheading");
                            categorylist.add(0,"Select Question Category");
                            if (jsonArray.length()>0){
                                for (int i = 0;i<jsonArray.length();i++){
                                    JSONObject json = jsonArray.getJSONObject(i);
                                    categorylist.add(json.getString("name"));
                                }
                                categoryadapter.notifyDataSetChanged();
                                categoryspinner.setAdapter(categoryadapter);
                            }else {
                                categorylist.add(0,"No category available");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               progressBar.setVisibility(View.GONE);
                Toast.makeText(AddMcqs.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    private void insetMcqToServer(){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait.....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.INSERT_MCQS_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (!jsonObject.getBoolean("error")){
                                PromptMessage.message1(AddMcqs.this,"Question has been published successfully.Your question will be visible after admin approval.Thank you for your participation",activity);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               progressDialog.dismiss();
                PromptMessage.message(AddMcqs.this,"Question not inserted due to slow internet question or internal server error");
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("mainheading",categorylist.get(categoryspinner.getSelectedItemPosition()));
                param.put("question",question.getText().toString().trim());
                param.put("optiona",optiona.getText().toString().trim());
                param.put("optionb",optionb.getText().toString().trim());
                param.put("optionc",optionc.getText().toString().trim());
                param.put("optiond",optiond.getText().toString().trim());
                param.put("answer",answer.getText().toString().trim());
                param.put("explanation",explain);
                param.put("userid","1");
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
