package com.example.talha.pakimcqs;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Adapter.ListAdapter;
import Adapter.QuestionsAdapter;
import ListItems.ListTextListitems;
import ListItems.MainHeadinListItems;
import ListItems.Questions_Listitems;

public class ShowQuestions extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<Questions_Listitems> list;
    private ProgressBar progressBar;
    private Toolbar toolbar;
    private TextView next,previous,toolbartext,answertextview;
    private String mainheading,subheading;
    private Activity activity;
    private CustomLinearLayouManager lm;
    private InterstitialAd mInterstitialAd;
    private AdView mAdView;
    private String answer;
    private Button answerbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_questions);
        views();
        mainheading = getIntent().getStringExtra("mainheading");
        subheading = getIntent().getStringExtra("subheading");
        setSupportActionBar(toolbar);
        toolbartext.setText(subheading);
        lm = new CustomLinearLayouManager(this,LinearLayoutManager.HORIZONTAL,false);
        lm.setScrollEnabled(false);
        recyclerView.setLayoutManager(lm);
        recyclerView.setHasFixedSize(true);
        getQuestions();
        recyclerView.stopScroll();
        final MediaPlayer mp = MediaPlayer.create(this,R.raw.click);


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((lm.findLastVisibleItemPosition()+1)==adapter.getItemCount()){
                    PromptMessage.message2(ShowQuestions.this,"This set has been completed",activity,mInterstitialAd);
                }else {
                    mp.start();
                    if (lm.findLastCompletelyVisibleItemPosition() < (adapter.getItemCount() - 1)) {
                        int position = lm.findLastCompletelyVisibleItemPosition() + 1;
                        answer = QuestionsAdapter.list.get(position).getCorrectoprion();
                        lm.scrollToPosition(position);
                        answertextview.setVisibility(View.GONE);
                        //totall.setText((lm.findLastCompletelyVisibleItemPosition()+2)+"/"+adapter.getItemCount()+"");
                    }
                }

            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(lm.findFirstVisibleItemPosition()>0)){
                PromptMessage.message(ShowQuestions.this,"This is the first question in front of you");
                }else {
                    mp.start();
                    int position = lm.findLastCompletelyVisibleItemPosition() - 1;
                    lm.scrollToPosition(position);
                    answer = QuestionsAdapter.list.get(position).getCorrectoprion();
                    answertextview.setVisibility(View.GONE);
                    //totall.setText((lm.findLastCompletelyVisibleItemPosition())+"/"+adapter.getItemCount()+"");
                }
            }
        });

        answerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mp = MediaPlayer.create(ShowQuestions.this,R.raw.sound);
                mp.start();
             answertextview.setText(answer);
             answertextview.setVisibility(View.VISIBLE);
            }
        });

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(this.getString(R.string.adunitcompleteid));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                mAdView.setVisibility(View.VISIBLE);
            }
        });

    }
    private void views(){
        recyclerView  = findViewById(R.id.questionsrecyclerview);
        list = new ArrayList<>();
        toolbar = findViewById(R.id.toolbar);
        next = findViewById(R.id.next);
        previous = findViewById(R.id.previous);
       // totall = findViewById(R.id.totallquestions);
        toolbartext = findViewById(R.id.toolbartext);
        progressBar = findViewById(R.id.progressbar);
        activity = ShowQuestions.this;
        mAdView = findViewById(R.id.adView);
        answertextview = findViewById(R.id.answertextview);
        answerbtn = findViewById(R.id.answerbtn);
    }

    private void getQuestions(){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.QUESTIONS_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("questions");
                            if (jsonArray.length()>0){
                                int count = 1;
                                for (int i =0;i<jsonArray.length();i++) {
                                    JSONObject json = jsonArray.getJSONObject(i);
                                    Questions_Listitems listitems = new Questions_Listitems(json.getString("id"),
                                            json.getString("mainheading"),json.getString("subheading"),
                                            json.getString("question"),json.getString("optiona"),
                                            json.getString("optionb"),json.getString("optionc"),
                                            json.getString("optiond"),json.getString("correctoption"),
                                            json.getString("explanation"));
                                    listitems.setCount(count);
                                    list.add(listitems);
                                    count++;
                                }

                                adapter = new QuestionsAdapter(list,ShowQuestions.this);
                                 getViews();
                                recyclerView.setAdapter(adapter);
                                //totall.setText((lm.findLastVisibleItemPosition()+2)+"/"+adapter.getItemCount()+"");

                            }else{
                                Toast.makeText(ShowQuestions.this, "Sorry no data available", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(ShowQuestions.this, e.getMessage()+"", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ShowQuestions.this, error.getMessage()+"", Toast.LENGTH_SHORT).show();
                // PromptMessage.message(ShowQuestions.this,"Oops!It seems like you are not connected with internet or your internet connection is too slow.Please check your internet connection");
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("mainheading",mainheading);
                param.put("subheading",subheading);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void getViews() {
        answer =  QuestionsAdapter.list.get(0).getCorrectoprion();
    }
}
