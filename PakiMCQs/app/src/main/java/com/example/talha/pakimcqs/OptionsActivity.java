package com.example.talha.pakimcqs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.MobileAds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Adapter.MainHeadingAdapter;
import ListItems.MainHeadinListItems;

public class OptionsActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<MainHeadinListItems> listItems;
    private ProgressBar progressBar;
    private Toolbar toolbar;
    private Button addmcqs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        views();
        setSupportActionBar(toolbar);
        recyclerView.setLayoutManager(new GridLayoutManager(OptionsActivity.this,2));
        recyclerView.setHasFixedSize(true);
        getMainHeading();
        addmcqs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OptionsActivity.this,AddMcqs.class));
            }
        });

        MobileAds.initialize(this,this.getString(R.string.admobid));
    }
    private void views(){
        recyclerView = findViewById(R.id.mainheadingrecyclerview);
        progressBar = findViewById(R.id.progressbar);
        listItems = new ArrayList<>();
        toolbar = findViewById(R.id.toolbar);
        addmcqs = findViewById(R.id.addmcqs);
    }
    private void getMainHeading(){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.Main_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                     progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("mainheading");
                            if (jsonArray.length()>0){
                               for (int i =0;i<jsonArray.length();i++){
                                   JSONObject json = jsonArray.getJSONObject(i);
                                   MainHeadinListItems list = new MainHeadinListItems(json.getString("name"),
                                           json.getString("image"),json.getInt("type"));
                                   listItems.add(list);
                               }
                               adapter = new MainHeadingAdapter(listItems,OptionsActivity.this);
                               recyclerView.setAdapter(adapter);
                            }else{
                                Toast.makeText(OptionsActivity.this, "No data available yet", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                PromptMessage.message(OptionsActivity.this,"Oops!It seems like you are not connected with internet or your internet connection is too slow");
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
