package com.example.talha.pakimcqs;

public class Constants {
    private static final String ROOT_URL = "http://www.pakisol.com/jobmcqs/appwebservices/";

    public static final String Main_URL = ROOT_URL+"showMainHeading.php";
    public static final String HEADING_URL = ROOT_URL+"showHeading.php";
    public static final String SUBHEADING_URL = ROOT_URL+"showSubHeading.php";
    public static final String QUESTIONS_URL = ROOT_URL+"showQuestions.php";
    public static final String INSERT_MCQS_URL = ROOT_URL+"insertQuestion.php";

}
