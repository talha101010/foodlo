package com.example.talha.pakimcqs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Adapter.ListAdapter;
import ListItems.ListTextListitems;

public class ListActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<ListTextListitems> list;
    private ProgressBar progressBar;
    private Toolbar toolbar;
    private TextView toolbartext;
    private String headingname;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        views();
        setSupportActionBar(toolbar);
        headingname = getIntent().getStringExtra("headingname");
        toolbartext.setText(headingname);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        getHeadings();
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                mAdView.setVisibility(View.VISIBLE);
            }
        });
    }
    private void views(){
        recyclerView = findViewById(R.id.headingrecyclerview);
        list = new ArrayList<>();
        progressBar = findViewById(R.id.progressbar);
        toolbar = findViewById(R.id.toolbar);
        toolbartext = findViewById(R.id.toolbartext);
        mAdView = findViewById(R.id.adView);
    }
    private void getHeadings(){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.HEADING_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("headings");
                            if (jsonArray.length()>0){
                                for (int i =0;i<jsonArray.length();i++) {
                                    JSONObject json = jsonArray.getJSONObject(i);
                                    ListTextListitems listitems = new ListTextListitems(json.getString("mainheading"),
                                            json.getString("name"));
                                    list.add(listitems);
                                }
                                adapter = new ListAdapter(list,ListActivity.this);
                                recyclerView.setAdapter(adapter);

                            }else{
                                Toast.makeText(ListActivity.this, "Sorry no data available", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                PromptMessage.message(ListActivity.this,"Oops!It seems like you are not connected with internet or your internet connection is too slow.Please check your internet connection");
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("mainheading",headingname);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
