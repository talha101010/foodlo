package com.example.tracker.childtracker;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Timer;
import java.util.TimerTask;

public class BackgroundServices extends Service {
    private Handler mHandler ;
    private Runnable runnable;
    private Timer mTimer = null;
    long notify_interval = 8000;
    public static String str_receiver = "com.example.tracker.childtracker.receiver";
    Intent intent;
    public static boolean isRunning=false;
    private FusedLocationProviderClient mFusedLocationProvider;
    private Location mLastKnownLocation;
    private LocationCallback mLocationCallBack;
    public double latitude,longitude;

    public BackgroundServices(){

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        isRunning = true;
        mHandler = new Handler(Looper.getMainLooper());
        mTimer = new Timer();
        mTimer.schedule(new TimerTaskToGetLocation(), 5, notify_interval);
        mFusedLocationProvider = LocationServices.getFusedLocationProviderClient(BackgroundServices.this);
        intent = new Intent(str_receiver);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseDatabase.getInstance().getReference("Childs").child(user.getUid()).child("status").setValue("Offline");
        onDestroy();
    }

    @SuppressLint("MissingPermission")
    private void getDeviceLocation(){
        mFusedLocationProvider.getLastLocation()
                .addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()){
                            mLastKnownLocation = task.getResult();
                            if (mLastKnownLocation != null){
                                latitude = mLastKnownLocation.getLatitude();
                                longitude = mLastKnownLocation.getLongitude();
                                if (latitude == 0.0){
                                    getDeviceLocation();
                                    return;
                                }else{
                                    sendIntent();
                                }

                            }else{
                                final LocationRequest locationRequest = LocationRequest.create();
                                locationRequest.setInterval(10000);
                                locationRequest.setFastestInterval(5000);
                                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

                                mLocationCallBack = new LocationCallback(){
                                    @Override
                                    public void onLocationResult(LocationResult locationResult) {
                                        super.onLocationResult(locationResult);
                                        if (locationResult == null){
                                            return;
                                        }
                                        mLastKnownLocation = locationResult.getLastLocation();
                                        latitude = mLastKnownLocation.getLatitude();
                                        longitude = mLastKnownLocation.getLongitude();
                                        sendIntent();
                                        mFusedLocationProvider.removeLocationUpdates(mLocationCallBack);
                                    }
                                };

                                mFusedLocationProvider.requestLocationUpdates(locationRequest,mLocationCallBack,null);

                            }
                        }else{
                            Toast.makeText(BackgroundServices.this, "enable to get location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        isRunning = false;
        mTimer.cancel();
        mHandler.removeCallbacks(runnable);
    }


    private class TimerTaskToGetLocation extends TimerTask {
        @Override
        public void run() {

            mHandler.post(runnable = new Runnable() {
                @Override
                public void run() {
                    getDeviceLocation();
                }
            });

        }
    }

    private void sendIntent(){
        intent.putExtra("latitude",latitude+"");
        intent.putExtra("longitude",longitude+"");
        sendBroadcast(intent);
    }


}
