package com.example.tracker.childtracker;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefrences {
    private static SharedPrefrences mInstance;


    private static Context mCtx;

    private SharedPrefrences(Context context) {
        mCtx = context;


    }

    public static synchronized SharedPrefrences getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefrences(context);
        }
        return mInstance;
    }

    public boolean getPassword(String password,String parentuid){
        SharedPreferences pref = mCtx.getSharedPreferences("childtracker", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("password",password);
        editor.putString("parentuid",parentuid);
        editor.apply();

        return true;
    }

    public boolean clear(){
        SharedPreferences pref = mCtx.getSharedPreferences("childtracker", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.apply();
        return true;
    }
}
