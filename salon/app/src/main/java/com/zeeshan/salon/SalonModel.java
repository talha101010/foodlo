package com.zeeshan.salon;

public class SalonModel {

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getSalonId() {
        return salonId;
    }

    public void setSalonId(String salonId) {
        this.salonId = salonId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public  String lat,lng,name,loc,salonId,address;

    public SalonModel() {
    }

    public SalonModel(String lat, String lng, String name, String loc, String salonId, String address) {
        this.lat = lat;
        this.lng = lng;
        this.name = name;
        this.loc = loc;
        this.salonId = salonId;
        this.address = address;
    }
}
