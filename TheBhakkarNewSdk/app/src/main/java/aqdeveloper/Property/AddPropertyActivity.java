package aqdeveloper.Property;

import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import aqdeveloper.thebhakkar.DataProviders.UploadImage;
import aqdeveloper.thebhakkar.NewHome;
import aqdeveloper.thebhakkar.R;

public class AddPropertyActivity extends AppCompatActivity {

    TextView selectPropertyType;
    DatabaseReference database,adsDb;
    ArrayList<UploadImage> typeProperty;
    String[] customerArray,customerStringArray;
    ListView selectListview;
    ArrayAdapter<String> cityAdapter,areaAdapter;
    AlertDialog dialog;
    EditText propertyHeadingEt,propertyDescriptionEt, propertyCellNumberEt, propertyPriceEt, propertyAddressEt;
    String strHeading, strDescription, strNumber, strPrice, strAdress,formattedDate;
    Button propertyAddBtn;
    String selectedType;
    ProgressBar mProgressbar;
    LinearLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_property);


        selectPropertyType = findViewById(R.id.selectpropertyType);
        propertyHeadingEt = findViewById(R.id.propertyheadinget);
        propertyDescriptionEt= findViewById(R.id.propertydescriptionet);
        propertyCellNumberEt = findViewById(R.id.propertyphoneNumberet);
        propertyAddressEt= findViewById(R.id.propertyAdresset);
        propertyPriceEt = findViewById(R.id.propertypriceet);
        propertyAddBtn= findViewById(R.id.addPropertyBtn);
        layout = findViewById(R.id.layout);

        database = FirebaseDatabase.getInstance().getReference("propertytype");
        adsDb = FirebaseDatabase.getInstance().getReference("pending_ads");
        typeProperty = new ArrayList<>();

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        formattedDate = df.format(c);
        mProgressbar = findViewById(R.id.mProgressbar);


        selectPropertyType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alertdialog = new AlertDialog.Builder(AddPropertyActivity.this);
                final LayoutInflater inflater =getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.listview_dialog,null);
                alertdialog.setView(dialogView);
                alertdialog.setTitle("Select customer");
                selectListview = dialogView.findViewById(R.id.selectlistview_in_dialog);

                cityAdapter = new ArrayAdapter<>(AddPropertyActivity.this,android.R.layout.simple_list_item_1,customerStringArray);
                selectListview.setAdapter(cityAdapter);
                dialog = alertdialog.create();
                dialog.show();
                selectListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        selectPropertyType.setText(cityAdapter.getItem(position).toString());
                        selectedType= customerStringArray[position];
                        selectPropertyType.setTextColor(Color.BLACK);
                        dialog.dismiss();



                    }
                });

            }
        });

        propertyAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedType!=null){

                    if (propertyHeadingEt.getText().toString().length() <3){

                        propertyHeadingEt.setError("Write Heading Here");
                    }else if (propertyDescriptionEt.getText().toString().length() <10){

                        propertyDescriptionEt.setError("Write Descrition here");
                    }else if (propertyCellNumberEt.getText().toString().length()<5){

                        propertyCellNumberEt.setError("Write Number Here");
                    }else if (propertyPriceEt.getText().toString().length()<2){

                        propertyPriceEt.setError("Write Price Here");
                    }else if (propertyAddressEt.getText().toString().length()<5){

                        propertyAddressEt.setError("Write adress here");
                    }else  {

                        String id = adsDb.push().getKey();

                        strHeading= propertyHeadingEt.getText().toString();
                        strDescription = propertyDescriptionEt.getText().toString();
                        strNumber = propertyCellNumberEt.getText().toString();
                        strPrice = propertyPriceEt .getText().toString();
                        strAdress = propertyAddressEt.getText().toString();
                        AddPropertyDP dp = new AddPropertyDP(id, strHeading+"("+selectedType+")",strDescription,strNumber,strPrice,strAdress,formattedDate);
                        adsDb.child(id).setValue(dp);
                        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(AddPropertyActivity.this);
                        LayoutInflater inflater =getLayoutInflater();
                        final View dialogView = inflater.inflate(R.layout.shop_close_dialog,null);
                        final ImageButton helplineBtn= dialogView.findViewById(R.id.helplineCallBtn);
                        final  ImageButton helplineBtn2 = dialogView.findViewById(R.id.helplineCallBtn2);
                        final TextView note_eng = dialogView.findViewById(R.id.eng_note);
                        final TextView note_urdu = dialogView.findViewById(R.id.urdu_note);
                        final  TextView call_us_1 = dialogView.findViewById(R.id.call_us_1);
                        call_us_1.setText("Developer");
                        call_us_1.setVisibility(View.GONE);
                        helplineBtn.setVisibility(View.GONE);

                        final  TextView call_us_2 = dialogView.findViewById(R.id.call_us_2);
                        call_us_2.setText("رابطہ نمبر");
                        alertdialog.setView(dialogView);
                        note_eng.setText("آپ کا اشتہار نظر ثانی کے لیا ایڈمن کے پاس پہنچ چکا ہے۔جوکہ 24 گھنٹے میں ایپ میں لگا دیا جائے گا۔اگر آپ اپنے اشتہار میں تصاویر لگوانا چا ہتے ہیں یا کوئی تبدیلی کروانا چاہتے ہیں۔ درج زیل نمبر پر رابطہ کریں۔");
                        note_urdu.setVisibility(View.GONE);


                        final AlertDialog dialog = alertdialog.create();
                        dialog.show();
                        propertyHeadingEt.setText("");
                        propertyDescriptionEt.setText("");
                        propertyCellNumberEt.setText("");
                        propertyPriceEt.setText("");
                        propertyAddressEt.setText("");



                        helplineBtn2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {


                                try {
                                    Intent sendIntents = new Intent("android.intent.action.MAIN");
                                    sendIntents.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                                    sendIntents.putExtra("jid", PhoneNumberUtils.stripSeparators("923348870308") + "@s.whatsapp.net");//phone number without "+" prefix

                                    startActivity(sendIntents);
                                } catch (Exception e) {
                                    Toast.makeText(AddPropertyActivity.this, "Whatsapp is not install in your mobile Please install Whatsapp to continue", Toast.LENGTH_SHORT).show();
                                }

                            }
                        });


                    }

                }else {

                    selectPropertyType.setTextColor(Color.RED);
                    Toast.makeText(AddPropertyActivity.this, "Please select type", Toast.LENGTH_SHORT).show();
                }

            }
        });
        database.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                typeProperty.clear();
                for (DataSnapshot dataSnapshot1 :dataSnapshot.getChildren()){

                    UploadImage type = dataSnapshot1.getValue(UploadImage.class);
                    typeProperty.add(type);
                }



                customerStringArray = new String[typeProperty.size()];
                for (int i=0; i<customerStringArray.length; i++){

                    customerStringArray[i] = typeProperty.get(i).imageName;
                }

                layout.setVisibility(View.VISIBLE);

              mProgressbar.setVisibility(View.GONE);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
