package aqdeveloper.Property;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import aqdeveloper.thebhakkar.R;

public class AdsDetailActivity extends AppCompatActivity {

    TextView adsHeadingTv,adsDesTv,adsDateTv,adsPriceTv,AdressTv;
    String strId, strHeading, strDes, strNumber, strPrice, strAdress,strDate;
    Button addImagesBtn,adsEditBtn,adsContactBtn,adsShowImage;
    DatabaseReference showAdsPicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ads_detail);

        showAdsPicture = FirebaseDatabase.getInstance().getReference("adspictures");

        Intent intent = getIntent();
        strId = intent.getStringExtra("id");
        strHeading= intent.getStringExtra("heading");
        strDes = intent.getStringExtra("des");
        strNumber = intent.getStringExtra("number");
        strPrice = intent.getStringExtra("price");
        strAdress = intent.getStringExtra("adress");
        strDate= intent.getStringExtra("date");

        adsHeadingTv = findViewById(R.id.adsHeadingTv);

        adsHeadingTv.setText(strHeading);
        adsDesTv = findViewById(R.id.adsDesTv);
        adsDesTv.setText(strDes);
        adsDateTv= findViewById(R.id.adsDateTv);
        adsDateTv.setText("اشتہار کی تاریخ:"+"  "+strDate);
        adsPriceTv =findViewById(R.id.adsPriceTv);
        adsPriceTv.setText(strPrice+"  " +" روپے");
        AdressTv = findViewById(R.id.adsAdressTv);
        AdressTv.setText(strAdress);





        adsContactBtn = findViewById(R.id.adsContactBtn);
        adsContactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel",strNumber,null));
                startActivity(callIntent);

            }
        });

        adsShowImage = findViewById(R.id.adsShowImagetBtn);
        adsShowImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showAdsPicture.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        if (dataSnapshot.hasChild(strId)){


                            Intent intent1 = new Intent(AdsDetailActivity.this, AdsPicturesShowActivity.class);
                            intent1.putExtra("id", strId);
                            startActivity(intent1);

                        }else {

                            Toast.makeText(AdsDetailActivity.this, "Pictures not added", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        });


    }
}
