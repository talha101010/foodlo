package aqdeveloper.Property;

public class AddPropertyDP {

    String id,heading, description, number, price, adress, date,type;


    public AddPropertyDP(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public AddPropertyDP() {
    }

    public AddPropertyDP(String id, String heading, String description, String number, String price, String adress, String date) {
        this.id = id;
        this.heading = heading;
        this.description = description;
        this.number = number;
        this.price = price;
        this.adress = adress;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public String getHeading() {
        return heading;
    }

    public String getDescription() {
        return description;
    }

    public String getNumber() {
        return number;
    }

    public String getPrice() {
        return price;
    }

    public String getAdress() {
        return adress;
    }

    public String getDate() {
        return date;
    }
}
