package aqdeveloper.Property;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import aqdeveloper.thebhakkar.DataProviders.UploadImage;
import aqdeveloper.thebhakkar.R;

public class PropertyAdapter extends RecyclerView.Adapter<PropertyAdapter.viewHolder>{

    private Context mContext;
    private List<UploadImage> mList;

    private PropertyAdapter.OnItemClickListener mListener;

    public interface OnItemClickListener{
        void  onItemClick(int position);
        void  onButtonChange(int position);

    }

    public void setOnItemClickListener(PropertyAdapter.OnItemClickListener listener){
        mListener = listener;
    }

    public PropertyAdapter(Context mContext, List<UploadImage> mList){

        this.mContext = mContext;
        this.mList = mList;


    }


    @NonNull
    @Override
    public PropertyAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.propertytype,viewGroup,false);
        PropertyAdapter.viewHolder vHolder = new PropertyAdapter.viewHolder(view,mListener);
        return vHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull PropertyAdapter.viewHolder viewHolder, int i) {

        final UploadImage uploadImage = mList.get(i);
        final TextView name = viewHolder.typeTv;
        //  ImageView picture = viewHolder.typeImage;
        final ImageView picture = new ImageView(mContext);
        final LinearLayout layout = viewHolder.layout;



        name.setText(uploadImage.getImageName());
        name.setVisibility(View.GONE);
        Picasso.with(picture.getContext())
                .load(uploadImage.getImageUri())
                .into(picture, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        layout.setMinimumHeight(2);
                        layout.setBackgroundDrawable(picture.getDrawable());
                    }

                    @Override
                    public void onError() {

                    }
                });

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, ShowAdsActivity.class);
                intent.putExtra("type",uploadImage.getImageName());
                mContext.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class viewHolder extends RecyclerView.ViewHolder{

        TextView typeTv;
        // CardView showNoticeCardview;
        ImageView typeImage;
        LinearLayout layout;

        public viewHolder(@NonNull View itemView , final PropertyAdapter.OnItemClickListener listener) {
            super(itemView);

            typeTv = itemView.findViewById(R.id.type_name_tv);
            typeImage = itemView.findViewById(R.id.imageview);
            layout = itemView.findViewById(R.id.layout);



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listener!=null){

                        int position  = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }

                }
            });


        }
    }




}

