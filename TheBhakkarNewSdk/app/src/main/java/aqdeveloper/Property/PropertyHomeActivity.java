package aqdeveloper.Property;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

import aqdeveloper.thebhakkar.DataProviders.UploadImage;
import aqdeveloper.thebhakkar.R;

public class PropertyHomeActivity extends AppCompatActivity {

    RecyclerView showTypeRecyclerview;

    UploadImage uploadImage;
    List<UploadImage> imageList;
    FirebaseStorage showStorage;
    DatabaseReference showTeachersPicture;
    ProgressBar mProgressbar;
    PropertyAdapter showTypeAdaptor;
    private StorageReference storageReference;
    Button addPropertyBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_home);

        showTypeRecyclerview =findViewById(R.id.showtyperecyclerview);

        mProgressbar = findViewById(R.id.mProgressbar);
        showTypeRecyclerview.setHasFixedSize(true);
        showTypeRecyclerview.setLayoutManager(new GridLayoutManager(this,2));

        addPropertyBtn = findViewById(R.id.add_property_type);
        addPropertyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PropertyHomeActivity.this,AddPropertyActivity.class));
            }
        });





        imageList = new ArrayList<>();

        showTeachersPicture = FirebaseDatabase.getInstance().getReference("propertytype");
        storageReference = FirebaseStorage.getInstance().getReference("categoriesimages/");

        showTeachersPicture.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                imageList.clear();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){

                    UploadImage ui = dataSnapshot1.getValue(UploadImage.class);
                    imageList.add(ui);
                    showTypeAdaptor  = new PropertyAdapter(PropertyHomeActivity.this,imageList);

                    showTypeRecyclerview.setAdapter(showTypeAdaptor);
                    mProgressbar.setVisibility(View.GONE);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
