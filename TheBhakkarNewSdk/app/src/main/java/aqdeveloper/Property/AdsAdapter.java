package aqdeveloper.Property;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

import aqdeveloper.thebhakkar.R;

public class AdsAdapter extends RecyclerView.Adapter<AdsAdapter.viewHolder>{

    private Context mContext;
    private List<AddPropertyDP> mList;

    private AdsAdapter.OnItemClickListener mListener;

    public interface OnItemClickListener{
        void  onItemClick(int position);
        void  onButtonChange(int position);

    }

    public void setOnItemClickListener(AdsAdapter.OnItemClickListener listener){
        mListener = listener;
    }

    public AdsAdapter(Context mContext, List<AddPropertyDP> mList){

        this.mContext = mContext;
        this.mList = mList;


    }


    @NonNull
    @Override
    public AdsAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.customise_ads,viewGroup,false);
        AdsAdapter.viewHolder vHolder = new AdsAdapter.viewHolder(view,mListener);
        return vHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull AdsAdapter.viewHolder viewHolder, int i) {

        final AddPropertyDP dataprovider = mList.get(i);
        final TextView name = viewHolder.heading;
        final TextView adress = viewHolder.adress;
        final LinearLayout layout = viewHolder.layout;



        name.setText(dataprovider.getHeading());
        adress.setText(dataprovider.getAdress());



//        layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(mContext, AdsDetailActivity.class);
//                intent.putExtra("id",dataprovider.getId());
//                intent.putExtra("heading",dataprovider.getHeading());
//                intent.putExtra("des" , dataprovider.getDescription());
//                intent.putExtra("number",dataprovider.getNumber());
//                intent.putExtra("price",dataprovider.getPrice());
//                intent.putExtra("adress",dataprovider.getAdress());
//                intent.putExtra("date", dataprovider.getDate());
//
//
//                mContext.startActivity(intent);
//
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class viewHolder extends RecyclerView.ViewHolder{

        TextView heading, adress;
        // CardView showNoticeCardview;

        LinearLayout layout;

        public viewHolder(@NonNull View itemView , final AdsAdapter.OnItemClickListener listener) {
            super(itemView);

            heading = itemView.findViewById(R.id.propertyHeadingTv);
            adress = itemView.findViewById(R.id.propertyAdressTv);
            layout = itemView.findViewById(R.id.layout);



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listener!=null){

                        int position  = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }

                }
            });


        }
    }




}

