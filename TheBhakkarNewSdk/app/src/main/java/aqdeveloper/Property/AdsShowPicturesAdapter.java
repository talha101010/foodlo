package aqdeveloper.Property;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import aqdeveloper.thebhakkar.DataProviders.UploadImage;
import aqdeveloper.thebhakkar.R;

public class AdsShowPicturesAdapter extends RecyclerView.Adapter<AdsShowPicturesAdapter.viewHolder>{

    private Context mContext;
    private List<UploadImage> mList;

    private AdsShowPicturesAdapter.OnItemClickListener mListener;

    public interface OnItemClickListener{
        void  onItemClick(int position);
        void  onButtonChange(int position);

    }

    public void setOnItemClickListener(AdsShowPicturesAdapter.OnItemClickListener listener){
        mListener = listener;
    }

    public AdsShowPicturesAdapter(Context mContext, List<UploadImage> mList){

        this.mContext = mContext;
        this.mList = mList;


    }


    @NonNull
    @Override
    public AdsShowPicturesAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.customise_ads_show_pictures,viewGroup,false);
        AdsShowPicturesAdapter.viewHolder vHolder = new AdsShowPicturesAdapter.viewHolder(view,mListener);
        return vHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull AdsShowPicturesAdapter.viewHolder viewHolder, int i) {

        final UploadImage uploadImage = mList.get(i);
        final TextView name = viewHolder.typeTv;
        final ImageView picture = new ImageView(mContext);
        final LinearLayout layout = viewHolder.layout;
        final  ProgressBar mProgress = viewHolder.mProgressbar;



        name.setText(uploadImage.getImageName());
        Picasso.with(picture.getContext())
                .load(uploadImage.getImageUri())
                .into(picture, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        layout.setMinimumHeight(2);
                        layout.setBackgroundDrawable(picture.getDrawable());
                        mProgress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {

                    }
                });



    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class viewHolder extends RecyclerView.ViewHolder{

        TextView typeTv;
        ProgressBar mProgressbar;
        // CardView showNoticeCardview;

        LinearLayout layout;

        public viewHolder(@NonNull View itemView , final AdsShowPicturesAdapter.OnItemClickListener listener) {
            super(itemView);

            typeTv = itemView.findViewById(R.id.type_name_tv);

            layout = itemView.findViewById(R.id.layout);
            mProgressbar =itemView.findViewById(R.id.mProgressbar);



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listener!=null){

                        int position  = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }

                }
            });


        }
    }




}

