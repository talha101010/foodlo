package aqdeveloper.Property;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

import aqdeveloper.thebhakkar.DataProviders.UploadImage;
import aqdeveloper.thebhakkar.R;

public class AdsPicturesShowActivity extends AppCompatActivity {

    RecyclerView showTypeRecyclerview;

    UploadImage uploadImage;
    List<UploadImage> imageList;
    FirebaseStorage showStorage;
    DatabaseReference showAdsPicture;
    ProgressBar mProgressbar;
    AdsShowPicturesAdapter showTypeAdaptor;
    private StorageReference storageReference;
    private ValueEventListener mDbListenre;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ads_pictures_show);


        Intent intent = getIntent();
        id = intent.getStringExtra("id");


        showTypeRecyclerview =findViewById(R.id.recyclerview);

        // mProgressbar = findViewById(R.id.mProgressbar);
        showTypeRecyclerview.setHasFixedSize(true);
        showTypeRecyclerview.setLayoutManager(new GridLayoutManager(this,1));



        imageList = new ArrayList<>();



        storageReference = FirebaseStorage.getInstance().getReference("adsimages/");
        showAdsPicture = FirebaseDatabase.getInstance().getReference("adspictures").child(id);
        showStorage = FirebaseStorage.getInstance();

        getData();







    }

    public void getData() {

        showAdsPicture.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                imageList.clear();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {


                    UploadImage ui = dataSnapshot1.getValue(UploadImage.class);
                    ui.setKey(dataSnapshot1.getKey());
                    imageList.add(ui);


                    Toast.makeText(AdsPicturesShowActivity.this, imageList.size() + "", Toast.LENGTH_SHORT).show();
                    showTypeAdaptor = new AdsShowPicturesAdapter(AdsPicturesShowActivity.this, imageList);

                    showTypeRecyclerview.setAdapter(showTypeAdaptor);


                    //     mProgressbar.setVisibility(View.GONE);

                }





            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


}
