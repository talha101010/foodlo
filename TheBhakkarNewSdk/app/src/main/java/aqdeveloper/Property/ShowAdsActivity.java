package aqdeveloper.Property;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import aqdeveloper.thebhakkar.R;

public class ShowAdsActivity extends AppCompatActivity {

    String type;
    DatabaseReference adsDb;
    ArrayList<AddPropertyDP> adsList;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_ads);

        Intent intent= getIntent();
        type = intent.getStringExtra("type");

        adsDb = FirebaseDatabase.getInstance().getReference("ads").child(type);





        adsList = new ArrayList<>();

        recyclerView =findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));


        adsDb.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                adsList.clear();

                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){

                    AddPropertyDP dp = dataSnapshot1.getValue(AddPropertyDP.class);
                    adsList.add(dp);
                }

                AdsAdapter adsAdapter = new AdsAdapter(ShowAdsActivity.this, adsList);
                recyclerView.setAdapter(adsAdapter);

                adsAdapter.setOnItemClickListener(new AdsAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        Intent intent = new Intent(ShowAdsActivity.this, AdsDetailActivity.class);
                        intent.putExtra("id",adsList.get(position).getId());
                        intent.putExtra("heading",adsList.get(position).getHeading());
                        intent.putExtra("des" , adsList.get(position).getDescription());
                        intent.putExtra("number",adsList.get(position).getNumber());
                        intent.putExtra("price",adsList.get(position).getPrice());
                        intent.putExtra("adress",adsList.get(position).getAdress());
                        intent.putExtra("date", adsList.get(position).getDate());
                        intent.putExtra("type",type);
                        startActivity(intent);
                    }

                    @Override
                    public void onButtonChange(int position) {

                    }
                });

            }



            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
