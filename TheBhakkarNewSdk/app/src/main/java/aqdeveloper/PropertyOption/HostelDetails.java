package aqdeveloper.PropertyOption;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.IndicatorView.draw.controller.DrawController;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import aqdeveloper.PropertyOption.Adapters.SliderAdapter;
import aqdeveloper.PropertyOption.ListItems.SliderListItems;
import aqdeveloper.thebhakkar.R;

public class HostelDetails extends AppCompatActivity {

    private TextView hostelname,description,rent,rentinclude,rooms,address,nearby,features,uploadtime,views;
    private SliderView sliderView;
    private List<SliderListItems> list1;
    private int viewsInteger;
    public static Activity activity;
    public static boolean checkStat=false;
    private String nameString,descriptionString,rentString,rentUnitString,rentincludeString,roomsString,addressString,nearbyString,featuresString,url,hostelid,userid;
    private Button deleteAddBtn,updatebtn,changestatus,contactBtn;
    private String phoneNumber,name,secondNumberString;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hostel_details);
        getViews();
        activity = HostelDetails.this;
        if (getIntent().getBooleanExtra("check",true)){
            getPhoneNumber();
            updateViews();
            updatebtn.setVisibility(View.GONE);
            changestatus.setVisibility(View.GONE);
            deleteAddBtn.setVisibility(View.GONE);
            contactBtn.setVisibility(View.VISIBLE);
        }else{
            updatebtn.setVisibility(View.VISIBLE);
            changestatus.setVisibility(View.VISIBLE);
            deleteAddBtn.setVisibility(View.VISIBLE);
            contactBtn.setVisibility(View.GONE);
        }
        nameString = getIntent().getStringExtra("name");
        descriptionString = getIntent().getStringExtra("description");
        rentString = getIntent().getStringExtra("rent");
        rentUnitString = getIntent().getStringExtra("rentunit");
        rentincludeString = getIntent().getStringExtra("rentinclude");
        roomsString = getIntent().getStringExtra("rooms");
        addressString = getIntent().getStringExtra("address");
        nearbyString = getIntent().getStringExtra("nearby");
        featuresString = getIntent().getStringExtra("features");
        url = getIntent().getStringExtra("url");
        hostelid = getIntent().getStringExtra("hostelid");
        userid = getIntent().getStringExtra("userid");
        viewsInteger =Integer.parseInt(getIntent().getStringExtra("views"));
        secondNumberString = getIntent().getStringExtra("secondnumber");
        viewsInteger = viewsInteger+1;

        hostelname.setText(nameString);
        description.setText(descriptionString);
        rent.setText(rentString+"/"+rentUnitString);
        rentinclude.setText(rentincludeString);
        rooms.setText(roomsString);
        address.setText(addressString);
        nearby.setText(nearbyString);
        features.setText(featuresString);
        uploadtime.setText(getIntent().getStringExtra("uploadtime"));
        views.setText(viewsInteger+"");
        getExtraImages();

        sliderView.setIndicatorAnimation(IndicatorAnimations.SLIDE); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.CUBEINROTATIONTRANSFORMATION);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setOnIndicatorClickListener(new DrawController.ClickListener() {
            @Override
            public void onIndicatorClicked(int position) {
                sliderView.setCurrentPagePosition(position);
            }
        });

        deleteAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HostelDetails.this,DeleteAddActivity.class);
                intent.putExtra("id",hostelid+"hostel");
                startActivity(intent);
            }
        });

        contactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog();
            }
        });

        changestatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dealDone();
            }
        });

        updatebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HostelDetails.this,AddHostelActivity.class);
                intent.putExtra("hostelid",hostelid);
                intent.putExtra("name",nameString);
                intent.putExtra("description",descriptionString);
                intent.putExtra("rent",rentString);
                intent.putExtra("rentunit",rentUnitString);
                intent.putExtra("rentinclude",rentincludeString);
                intent.putExtra("rooms",roomsString);
                intent.putExtra("address",addressString);
                intent.putExtra("nearby",nearbyString);
                intent.putExtra("features",featuresString);
                intent.putExtra("url",url);
                intent.putExtra("imagename",getIntent().getStringExtra("imagename"));
                intent.putExtra("uploadtime",getIntent().getStringExtra("uploadtime"));
                intent.putExtra("userid",userid);
                intent.putExtra("secondnumber",secondNumberString);
                intent.putExtra("status","update");
                startActivity(intent);
            }
        });
    }

    private void getViews(){
        hostelname = findViewById(R.id.title);
        description = findViewById(R.id.description);
        rent = findViewById(R.id.rent);
        rentinclude = findViewById(R.id.rentinclude);
        rooms = findViewById(R.id.rooms);
        address = findViewById(R.id.address);
        nearby = findViewById(R.id.institue);
        features = findViewById(R.id.features);
        sliderView = findViewById(R.id.imageSlider);
        uploadtime = findViewById(R.id.date);
        views = findViewById(R.id.views);
        deleteAddBtn = findViewById(R.id.delete_add_btn);
        updatebtn = findViewById(R.id.updatebtn);
        changestatus = findViewById(R.id.changestatus);
        contactBtn =findViewById(R.id.contact_btn);
        list1 = new ArrayList<>();
    }

    private void getExtraImages(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.SHOW_EXTRA_IMAGES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            if (list1!=null){
                                list1.clear();
                            }
                            JSONObject json = new JSONObject(response);
                            JSONArray jsonArray = json.getJSONArray("extraimages");
                            SliderListItems sliderlist = new SliderListItems(nameString,url);
                            list1.add(sliderlist);
                            if (jsonArray.length()>0){
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    sliderlist = new SliderListItems(nameString,
                                            jsonObject.getString("image"));
                                    list1.add(sliderlist);
                                }
                            }else{

                            }

                            final SliderAdapter adapter = new SliderAdapter(HostelDetails.this,list1);
                            sliderView.setSliderAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(HostelDetails.this, "communication server error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("id",hostelid+"hostel");
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void getPhoneNumber(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.GET_USER_PHONE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject json = new JSONObject(response);
                            if (!json.getBoolean("error")){
                                phoneNumber = json.getString("cell_number");
                                name = json.getString("name");
                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(HostelDetails.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("id",userid);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void updateViews(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.UPDATE_HOSTELS_VIEWS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject json = new JSONObject(response);
                            if (!json.getBoolean("error")){
                               //  Toast.makeText(HostelDetails.this, "updated", Toast.LENGTH_SHORT).show();
                            }else{
                                // Toast.makeText(HostelDetails.this, "update error", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Toast.makeText(HostelDetails.this, "error in updating views", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("views",viewsInteger+"");
                param.put("id",hostelid);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void dealDone(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.CHANGE_HOSTEL_STATUS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject json = new JSONObject(response);
                            if (!json.getBoolean("error")){
                                checkStat = true;
                                Toast.makeText(HostelDetails.this, "Deal Done", Toast.LENGTH_SHORT).show();
                                finish();
                            }else{
                                Toast.makeText(HostelDetails.this, "update error", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(HostelDetails.this, "error in updating views", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("status","Pending");
                param.put("id",hostelid);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void dialog(){

        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(HostelDetails.this);
        LayoutInflater inflater =getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.call_dialog,null);
        final CardView primaryCallCardview = dialogView.findViewById(R.id.primary_number_cardview);
        final  CardView secondaryCallCardview = dialogView.findViewById(R.id.secondary_number_cardview);
        final  TextView primaryTextView =dialogView.findViewById(R.id.primary_number_textview);
        final TextView secondaryTextView =dialogView.findViewById(R.id.secondary_number_textview);
        final TextView nameTextview = dialogView.findViewById(R.id.name_textview);
        alertdialog.setView(dialogView);
        final AlertDialog dialog = alertdialog.create();
        dialog.show();

        primaryTextView.setText(phoneNumber);
        secondaryTextView.setText(secondNumberString);
        nameTextview.setText(name);

        if (secondNumberString.length()<10){

            secondaryCallCardview.setVisibility(View.GONE);

        }

        secondaryCallCardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (secondNumberString !=null ){
                    Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel",secondNumberString,null));
                    startActivity(callIntent);
                    dialog.dismiss();
                }else{

                    secondaryCallCardview.setVisibility(View.GONE);
                }
            }
        });


        primaryCallCardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (phoneNumber!=null){
                    Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel",phoneNumber,null));
                    startActivity(callIntent);
                    dialog.dismiss();
                }else{
                    Toast.makeText(HostelDetails.this, "sorry primary number not available", Toast.LENGTH_SHORT).show(); }

            }
        });
    }
}
