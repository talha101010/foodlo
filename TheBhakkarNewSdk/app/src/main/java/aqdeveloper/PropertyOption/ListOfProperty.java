package aqdeveloper.PropertyOption;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import aqdeveloper.PropertyOption.Adapters.HousesAdapter;
import aqdeveloper.PropertyOption.ListItems.HousesListItems;
import aqdeveloper.thebhakkar.DataProviders.HomeDataProvider;
import aqdeveloper.thebhakkar.R;

public class ListOfProperty extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<HousesListItems> list;
    private RecyclerView.Adapter adapter;
    private ProgressBar progressBar;
    private TextView noText;
    private Spinner citySpinner;
    private String[] cityArray;
    public static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_property);
        activity = ListOfProperty.this;
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        list = new ArrayList<>();
        progressBar = findViewById(R.id.progressbar);
        noText = findViewById(R.id.no_data_textview);
        citySpinner = findViewById(R.id.select_city_spinner);
        cityArray = getResources().getStringArray(R.array.area_array);
        if (getIntent().getStringExtra("prostatus").equals("houses")){
            citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position==0){
                        getHousesFromServer();
                    }else{
                        getHousesAgainstArea();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } else {
            citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position==0){
                        getPlotsFromServer();
                    }else{
                        getPlotsAgainstArea();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

    }

    private void getHousesFromServer(){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.showHouses,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (adapter!=null){
                            list.clear();
                        }
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("houses");
                            if (jsonArray.length()!=0){
                                noText.setVisibility(View.GONE);
                                for (int i =0;i<jsonArray.length();i++){
                                    JSONObject json = jsonArray.getJSONObject(i);
                                    HousesListItems listItems = new HousesListItems(json.getString("id"),
                                            json.getString("title"),json.getString("description"),
                                            json.getString("area"),json.getString("price"),
                                            json.getString("address"),json.getString("rooms"),
                                            json.getString("bathrooms"),json.getString("stories"),
                                            json.getString("image"),json.getString("uploadtime"),
                                            json.getString("views"),json.getString("imagename"),
                                            json.getString("secondnumber"),json.getString("areaunit"),
                                            json.getString("areaname"),json.getString("gas"),
                                            json.getString("userid"));
                                    list.add(listItems);
                                }
                                adapter = new HousesAdapter(list,ListOfProperty.this,true);
                                recyclerView.setAdapter(adapter);
                            }else{
                               // Toast.makeText(ListOfProperty.this, "no data available yet", Toast.LENGTH_SHORT).show();
                                noText.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                 progressBar.setVisibility(View.GONE);
                Toast.makeText(ListOfProperty.this, error.getMessage()+"", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
             Map<String,String> param = new HashMap<>();
             param.put("type",getIntent().getStringExtra("status"));
             return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void getHousesAgainstArea(){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.HOUSES_AGAINST_AREA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (adapter!=null){
                            list.clear();
                        }
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("houses");
                            if (jsonArray.length()!=0){
                                noText.setVisibility(View.GONE);
                                for (int i =0;i<jsonArray.length();i++){
                                    JSONObject json = jsonArray.getJSONObject(i);
                                    HousesListItems listItems = new HousesListItems(json.getString("id"),
                                            json.getString("title"),json.getString("description"),
                                            json.getString("area"),json.getString("price"),
                                            json.getString("address"),json.getString("rooms"),
                                            json.getString("bathrooms"),json.getString("stories"),
                                            json.getString("image"),json.getString("uploadtime"),
                                            json.getString("views"),json.getString("imagename"),
                                            json.getString("secondnumber"),json.getString("areaunit"),
                                            json.getString("areaname"),json.getString("gas"),
                                            json.getString("userid"));
                                    list.add(listItems);
                                }
                                adapter = new HousesAdapter(list,ListOfProperty.this,true);
                                recyclerView.setAdapter(adapter);
                            }else{
                                // Toast.makeText(ListOfProperty.this, "no data available yet", Toast.LENGTH_SHORT).show();
                                noText.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ListOfProperty.this, error.getMessage()+"", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("type",getIntent().getStringExtra("status"));
                param.put("areaname",cityArray[citySpinner.getSelectedItemPosition()]);
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void getPlotsFromServer(){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.SHOW_PLOTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (adapter!=null){
                            list.clear();
                        }
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("plots");
                            if (jsonArray.length()!=0){
                                noText.setVisibility(View.GONE);
                                for (int i =0;i<jsonArray.length();i++){
                                    JSONObject json = jsonArray.getJSONObject(i);
                                    HousesListItems listItems = new HousesListItems(json.getString("id"),
                                            json.getString("title"),json.getString("description"),
                                            json.getString("area"),json.getString("price"),
                                            json.getString("address"),"null",
                                            "null","null",
                                            json.getString("image"),json.getString("uploadtime"),
                                            json.getString("views"),json.getString("imagename"),
                                            json.getString("secondnumber"),json.getString("areaunit"),
                                            json.getString("areaname"),json.getString("plotstype"),
                                            json.getString("userid"));
                                    // plotstype in place of gas 2nd last.
                                    list.add(listItems);
                                }
                                adapter = new HousesAdapter(list,ListOfProperty.this,true);
                                recyclerView.setAdapter(adapter);
                            }else{
                                // Toast.makeText(ListOfProperty.this, "no data available yet", Toast.LENGTH_SHORT).show();
                                noText.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ListOfProperty.this, error.getMessage()+"", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("type",getIntent().getStringExtra("status"));
                param.put("plottype",getIntent().getStringExtra("prostatus"));
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void getPlotsAgainstArea(){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.SHOW_PLOTS_AGAINST_AREA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (adapter!=null){
                            list.clear();
                        }
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("plots");
                            if (jsonArray.length()!=0){
                                noText.setVisibility(View.GONE);
                                for (int i =0;i<jsonArray.length();i++){
                                    JSONObject json = jsonArray.getJSONObject(i);
                                    HousesListItems listItems = new HousesListItems(json.getString("id"),
                                            json.getString("title"),json.getString("description"),
                                            json.getString("area"),json.getString("price"),
                                            json.getString("address"),"null",
                                            "null","null",
                                            json.getString("image"),json.getString("uploadtime"),
                                            json.getString("views"),json.getString("imagename"),
                                            json.getString("secondnumber"),json.getString("areaunit"),
                                            json.getString("areaname"),json.getString("plotstype"),
                                            json.getString("userid"));
                                    // plotstype in place of gas 2nd last.
                                    list.add(listItems);
                                }
                                adapter = new HousesAdapter(list,ListOfProperty.this,true);
                                recyclerView.setAdapter(adapter);
                            }else{
                                // Toast.makeText(ListOfProperty.this, "no data available yet", Toast.LENGTH_SHORT).show();
                                noText.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ListOfProperty.this, error.getMessage()+"", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("type",getIntent().getStringExtra("status"));
                param.put("areaname",cityArray[citySpinner.getSelectedItemPosition()]);
                param.put("plottype",getIntent().getStringExtra("prostatus"));
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public static void addFavourites(final String userid, final String houseid){
        final ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Please Wait....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.ADD_FAVOURITES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject json = new JSONObject(response);
                            if (!json.getBoolean("error")){
                                 Toast.makeText(activity, "Added in Favourites", Toast.LENGTH_SHORT).show();
                            }else{
                                 Toast.makeText(activity, "error occur", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                 Toast.makeText(activity, "error in updating views", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("userid",userid);
                param.put("houseid",houseid);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(stringRequest);
    }

}