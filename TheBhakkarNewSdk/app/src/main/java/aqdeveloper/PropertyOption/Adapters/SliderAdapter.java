package aqdeveloper.PropertyOption.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.smarteist.autoimageslider.SliderViewAdapter;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import aqdeveloper.PropertyOption.ListItems.SliderListItems;
import aqdeveloper.thebhakkar.R;

public class SliderAdapter extends SliderViewAdapter<SliderAdapter.SliderAdapterVH> {
    private Context context;
    private List<SliderListItems> list;

    public SliderAdapter(Context context, List<SliderListItems> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
      View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.property_slider,null);
      return new SliderAdapterVH(view);
    }

    @Override
    public void onBindViewHolder(final SliderAdapterVH viewHolder, final int position) {

        SliderListItems listitems1 = list.get(position);
        viewHolder.textViewDescription.setText(listitems1.getName());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "This is item in position " + position, Toast.LENGTH_SHORT).show();
            }
        });
        Picasso.with(context).load(listitems1.getImageurl()).fit().into(viewHolder.imageView, new Callback() {
            @Override
            public void onSuccess() {
               viewHolder.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });

    }

    @Override
    public int getCount() {
        return list.size();
    }

    public class SliderAdapterVH extends SliderViewAdapter.ViewHolder{
        View itemView;
        ImageView imageView;
        ImageView imageGifContainer;
        TextView textViewDescription;
        ProgressBar progressBar;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageslider1);
            imageGifContainer = itemView.findViewById(R.id.iv_gif_container);
            textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider);
            progressBar = itemView.findViewById(R.id.progressbar);
            this.itemView = itemView;
        }
    }
}
