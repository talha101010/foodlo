package aqdeveloper.PropertyOption.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import aqdeveloper.PropertyOption.DeleteAddActivity;
import aqdeveloper.PropertyOption.ListItems.SliderListItems;
import aqdeveloper.thebhakkar.R;

public class ExtraImageAdapter extends RecyclerView.Adapter<ExtraImageAdapter.ViewHolder> {
    private List<SliderListItems> list;
    private Context context;

    public ExtraImageAdapter(List<SliderListItems> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
      View v = LayoutInflater.from(context).inflate(R.layout.extra_images_list_items,viewGroup,false);
      return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
       final SliderListItems listItems = list.get(i);
        Picasso.with(context).load(listItems.getImageurl()).fit().into(viewHolder.imageView, new Callback() {
            @Override
            public void onSuccess() {
                viewHolder.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });

        viewHolder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeleteAddActivity.deleteImages(listItems.getName(),listItems.getImageurl(),context);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        Button button;
        ProgressBar progressBar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageview);
            button = itemView.findViewById(R.id.deletebtn);
            progressBar = itemView.findViewById(R.id.progressbar);
        }
    }
}
