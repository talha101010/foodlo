package aqdeveloper.PropertyOption.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import aqdeveloper.PropertyOption.ListItems.HousesListItems;
import aqdeveloper.PropertyOption.ListOfProperty;
import aqdeveloper.PropertyOption.PropertyDetail;
import aqdeveloper.PropertyOption.SellingTypeActivity;
import aqdeveloper.thebhakkar.DrawerActivities.LoginPanel;
import aqdeveloper.thebhakkar.R;

public class HousesAdapter extends RecyclerView.Adapter<HousesAdapter.ViewHolder> {
    private List<HousesListItems> list;
    private Context context;
    private boolean check;

    public HousesAdapter(List<HousesListItems> list, Context context,boolean check) {
        this.list = list;
        this.context = context;
        this.check = check;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
      View v = LayoutInflater.from(context).inflate(R.layout.new_property_list_customise,viewGroup,false);
      return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
         final HousesListItems listItems = list.get(i);
         viewHolder.title.setText(listItems.getTitle());
         viewHolder.uploadtime.setText(listItems.getUploadtime());
         viewHolder.address.setText(listItems.getAddress());
         viewHolder.viewcount.setText(listItems.getViews());
         viewHolder.price.setText(listItems.getPrice());
         if (!check){
             viewHolder.favouriteimage.setVisibility(View.GONE);
         }
        Picasso.with(context).load(listItems.getImage()).fit().into(viewHolder.imageView, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {

            }
        });

        viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,PropertyDetail.class);
                intent.putExtra("title",listItems.getTitle());
                intent.putExtra("description",listItems.getDescription());
                intent.putExtra("area",listItems.getArea());
                intent.putExtra("price",listItems.getPrice());
                intent.putExtra("address",listItems.getAddress());
                intent.putExtra("rooms",listItems.getRooms());
                intent.putExtra("bathrooms",listItems.getBathrooms());
                intent.putExtra("stories",listItems.getStories());
                intent.putExtra("date",listItems.getUploadtime());
                intent.putExtra("views",listItems.getViews());
                intent.putExtra("id",listItems.getId());
                intent.putExtra("url",listItems.getImage());
                intent.putExtra("imagename",listItems.getImagename());
                intent.putExtra("secondnumber",listItems.getSecondnumber());
                intent.putExtra("areaunit",listItems.getAreaunit());
                intent.putExtra("areaname",listItems.getAreaname());
                intent.putExtra("gas",listItems.getGas());
                intent.putExtra("userid",listItems.getUserid());
                intent.putExtra("check",check);
                context.startActivity(intent);
            }
        });

        viewHolder.favouriteimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = context.getSharedPreferences("EStore", Context.MODE_PRIVATE);
                if (preferences.getString("userid","").equals("")){
                    Toast.makeText(context, "please login to continue", Toast.LENGTH_SHORT).show();
                    context.startActivity(new Intent(context,LoginPanel.class));
                    return;
                }else{
                    ListOfProperty.addFavourites(preferences.getString("userid",""),listItems.getId());
                    viewHolder.favouriteimage.setImageResource(R.drawable.favorite_icon);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
     ImageView imageView,favouriteimage;
     TextView title,uploadtime,address,viewcount,price;
     LinearLayout linearLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.property_image);
            title = itemView.findViewById(R.id.title_property_textview);
            uploadtime = itemView.findViewById(R.id.uploaded_date_and_time);
            address = itemView.findViewById(R.id.property_adress);
            viewcount = itemView.findViewById(R.id.viewcount);
            price = itemView.findViewById(R.id.price);
            favouriteimage = itemView.findViewById(R.id.favourite);
            linearLayout = itemView.findViewById(R.id.main_layout);
        }
    }
}
