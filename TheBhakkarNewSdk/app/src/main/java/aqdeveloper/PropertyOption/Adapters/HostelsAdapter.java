package aqdeveloper.PropertyOption.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import aqdeveloper.PropertyOption.HostelDetails;
import aqdeveloper.PropertyOption.ListItems.HostelsListItems;
import aqdeveloper.thebhakkar.R;

public class HostelsAdapter extends RecyclerView.Adapter<HostelsAdapter.ViewHolder> {
    private List<HostelsListItems> list;
    private Context context;
    private boolean check;

    public HostelsAdapter(List<HostelsListItems> list, Context context,boolean check) {
        this.list = list;
        this.context = context;
        this.check = check;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
       View v = LayoutInflater.from(context).inflate(R.layout.new_property_list_customise,viewGroup,false);
       return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final HostelsListItems listItems = list.get(i);
        viewHolder.title.setText(listItems.getName());
        viewHolder.uploadtime.setText(listItems.getUploadtime());
        viewHolder.address.setText(listItems.getAddress());
        viewHolder.viewcount.setText(listItems.getViews());
        viewHolder.price.setText(listItems.getRent());
        Picasso.with(context).load(listItems.getImage()).fit().into(viewHolder.imageView, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {

            }
        });

        viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,HostelDetails.class);
                intent.putExtra("hostelid",listItems.getId());
                intent.putExtra("name",listItems.getName());
                intent.putExtra("description",listItems.getDescription());
                intent.putExtra("rent",listItems.getRent());
                intent.putExtra("rentunit",listItems.getRentCategory());
                intent.putExtra("rentinclude",listItems.getRentinclude());
                intent.putExtra("rooms",listItems.getRooms());
                intent.putExtra("address",listItems.getAddress());
                intent.putExtra("nearby",listItems.getNearby());
                intent.putExtra("features",listItems.getFeatures());
                intent.putExtra("url",listItems.getImage());
                intent.putExtra("imagename",listItems.getImagename());
                intent.putExtra("uploadtime",listItems.getUploadtime());
                intent.putExtra("views",listItems.getViews());
                intent.putExtra("userid",listItems.getUserid());
                intent.putExtra("secondnumber",listItems.getSecondnumber());
                intent.putExtra("check",check);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView,favouriteimage;
        TextView title,uploadtime,address,viewcount,price;
        LinearLayout linearLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.property_image);
            title = itemView.findViewById(R.id.title_property_textview);
            uploadtime = itemView.findViewById(R.id.uploaded_date_and_time);
            address = itemView.findViewById(R.id.property_adress);
            viewcount = itemView.findViewById(R.id.viewcount);
            price = itemView.findViewById(R.id.price);
            favouriteimage = itemView.findViewById(R.id.favourite);
            linearLayout = itemView.findViewById(R.id.main_layout);
        }
    }
}
