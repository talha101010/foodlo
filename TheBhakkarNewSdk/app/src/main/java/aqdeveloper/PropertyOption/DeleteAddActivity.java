package aqdeveloper.PropertyOption;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import aqdeveloper.PropertyOption.Adapters.ExtraImageAdapter;
import aqdeveloper.PropertyOption.Adapters.SliderAdapter;
import aqdeveloper.PropertyOption.ListItems.SliderListItems;
import aqdeveloper.thebhakkar.Http.AsyncTaskListener;
import aqdeveloper.thebhakkar.Http.HttpAsyncRequest;
import aqdeveloper.thebhakkar.Http.TaskResult;
import aqdeveloper.thebhakkar.Parser.BaseParser;
import aqdeveloper.thebhakkar.R;

public class DeleteAddActivity extends AppCompatActivity implements BaseParser {

    private ImageView extraimage;
    private String extraImageString=null;
    protected static final int GALLERY_CODE = 20;
    private Button extraimagebtn;
    private  ProgressDialog progressDialog;
    private static String id;
    private static RecyclerView recyclerView;
    private static RecyclerView.Adapter adapter;
    private static List<SliderListItems> list;
    private static ProgressBar progressBar;
    private static Activity activity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_add);
        activity = DeleteAddActivity.this;
        extraimage = findViewById(R.id.extraimage);
        extraimagebtn = findViewById(R.id.extraimagebtn);
        progressDialog = new ProgressDialog(this);
        recyclerView = findViewById(R.id.recyclerview);
        progressBar = findViewById(R.id.progressbar);
        id = getIntent().getStringExtra("id");
        list = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        extraimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(DeleteAddActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},GALLERY_CODE);
            }
        });

        extraimagebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (extraImageString!=null){
                    insertExtraImageToServer();
                }else{
                    Toast.makeText(DeleteAddActivity.this, "Please select an image first", Toast.LENGTH_SHORT).show();
                }
            }
        });

        getExtraImages();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == GALLERY_CODE){
            if (grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_GRANTED){
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent,GALLERY_CODE);
            }else {
                Toast.makeText(this, "You don't have permission to access files", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public String getRealPathFromUri(Uri contentUri){
        String result;
        Cursor cursor = getContentResolver().query(contentUri,null,null,null,null);
        if (cursor == null){
            result = contentUri.getPath();
        }else{
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_CODE && resultCode == RESULT_OK && null != data){
            Uri selectedimage = data.getData();
            extraImageString = getRealPathFromUri(selectedimage);

            try {
                InputStream inputStream = getContentResolver().openInputStream(selectedimage);
                extraimage.setImageBitmap(BitmapFactory.decodeStream(inputStream));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }



        }else{
            Toast.makeText(this, "you have not pick image", Toast.LENGTH_SHORT).show();
        }
    }

    private void insertExtraImageToServer(){
        HttpAsyncRequest request = new HttpAsyncRequest(this,Constants.INSERT_EXTRA_IMAGES, HttpAsyncRequest.RequestType.POST,this,listener);
        request.addParam("houseid",id);
        request.addFile("image",extraImageString);


        request.execute();
        progressDialog.setMessage("Sending.......");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

    }

    AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            progressDialog.dismiss();
            if (result.isSuccess()){
                //  finish();
                extraimage.setImageResource(R.drawable.bhakkarmarketicon);
                extraImageString = null;
               // getExtraImages();
            }else{
                Toast.makeText(DeleteAddActivity.this, result.getMessage()+"", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();

        if (httpCode==SUCCESS){
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (!jsonObject.getBoolean("error")){
                    // check = true;
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    result.success(true);
                }else{
                    //  check = false;
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    result.success(true);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);
            }
        }
        return result;
    }

    private static void getExtraImages(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.SHOW_EXTRA_IMAGES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            if (adapter!=null){
                                list.clear();
                            }
                            JSONObject json = new JSONObject(response);
                            JSONArray jsonArray = json.getJSONArray("extraimages");
                            if (jsonArray.length()>0){
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                   SliderListItems sliderlist = new SliderListItems(jsonObject.getString("id"),
                                            jsonObject.getString("image"));
                                    list.add(sliderlist);
                                }
                            }else{

                            }

                             adapter = new ExtraImageAdapter(list,activity);
                             recyclerView.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(activity, "communication server error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("id",id);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(stringRequest);
    }

    public static void deleteImages(final String imageid, final String imageurl, final Context context){
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please Wait....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.DELETE_EXTRA_IMAGES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                      progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (!jsonObject.getBoolean("error")){
                                Toast.makeText(context, "Image Deleted", Toast.LENGTH_SHORT).show();
                                 getExtraImages();
                            }else{
                                Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put("imageid",imageid);
                map.put("url",imageurl);
                return map;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }
}
