package aqdeveloper.PropertyOption;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;

import aqdeveloper.thebhakkar.DrawerActivities.LoginPanel;
import aqdeveloper.thebhakkar.Http.AsyncTaskListener;
import aqdeveloper.thebhakkar.Http.HttpAsyncRequest;
import aqdeveloper.thebhakkar.Http.TaskResult;
import aqdeveloper.thebhakkar.Parser.BaseParser;
import aqdeveloper.thebhakkar.R;

public class AddHousesActivity extends AppCompatActivity implements BaseParser {

    String propertyType, propertyCategory,userid;
    TextView propertyTypeTv, propertyCategoryTv,username,usercontat;
    private EditText title,description,area,price,rooms,bathrooms,stories,address,anothernumber;
    private Spinner areaUnitSpinner,citySpinner;
    private ImageView propertyimage,image2,image3;
    protected static final int GALLERY_CODE = 20;
    String imgString=null,imgString2=null,imgString3=null;
    private String[] areaUnitArray,cityArray;
    private ProgressDialog progressDialog;
    private Button uploadbtn;
    private ImageButton uploadimagebtn;
    public  static SharedPreferences preferences;
    private String url;
    private RadioGroup radioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_houses);
        preferences = getSharedPreferences("EStore", Context.MODE_PRIVATE);
        userid = preferences.getString("userid","");
        views();
        username.setText(preferences.getString("name",""));
        usercontat.setText(preferences.getString("phone",""));
        uploadimagebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(AddHousesActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},GALLERY_CODE);
            }
        });

        if (getIntent().getStringExtra("status").equals("update")){
            title.setText(getIntent().getStringExtra("title"));
            description.setText(getIntent().getStringExtra("description"));
            area.setText(getIntent().getStringExtra("area"));
            areaUnitSpinner.setSelection(((ArrayAdapter)areaUnitSpinner.getAdapter()).getPosition(getIntent().getStringExtra("areaunit")));
            citySpinner.setSelection(((ArrayAdapter)citySpinner.getAdapter()).getPosition(getIntent().getStringExtra("areaname")));
            price.setText(getIntent().getStringExtra("price"));
            rooms.setText(getIntent().getStringExtra("rooms"));
            bathrooms.setText(getIntent().getStringExtra("bathrooms"));
            stories.setText(getIntent().getStringExtra("stories"));
            address.setText(getIntent().getStringExtra("address"));
            anothernumber.setText(getIntent().getStringExtra("secondnumber"));
            if (getIntent().getStringExtra("gas").equals("YES")){
                radioGroup.check(R.id.gas_yes_radio_button);
            }else{
                radioGroup.check(R.id.gas_no_radio_button);
            }
            uploadbtn.setText("Update Now");
            propertyTypeTv.setVisibility(View.GONE);
            propertyCategoryTv.setVisibility(View.GONE);
            url = Constants.UPDATE_HOUSES;
        }else{
            url = Constants.insertHouses;
            Intent intent = getIntent();
            propertyType = intent.getStringExtra("type");
            if (propertyType.equals("Sell")){
                propertyType = "Buy";
            }
            propertyCategory = intent.getStringExtra("category");

            propertyTypeTv.setText(propertyType);
            propertyCategoryTv.setText(propertyCategory);
            uploadbtn.setText("Upload Now");
        }

        uploadbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (title.getText().length()==0){
                    title.setError("This field is required");
                }else if (description.getText().length()==0){
                    description.setError("This field is required");
                }else if (area.getText().length()==0){
                    area.setError("This field is required");
                }else if (price.getText().length()==0){
                    price.setError("This field is required");
                }else if (rooms.getText().length()==0){
                    rooms.setError("This field is required");
                }else if (bathrooms.getText().length()==0){
                    bathrooms.setError("This field is required");
                }else if (stories.getText().length()==0){
                    stories.setError("This field is required");
                }else if (address.getText().length()==0){
                    address.setError("This field is required");
                }else if (getIntent().getStringExtra("status").equals("insert") && imgString==null){
                    Toast.makeText(AddHousesActivity.this, "Please Select at least one image", Toast.LENGTH_SHORT).show();
                }else{
                    insertDataToServer();
                }

            }
        });

    }

    private  void  views(){

        propertyTypeTv = findViewById(R.id.property_type_textview);
        propertyCategoryTv =findViewById(R.id.property_category_textview);
        title = findViewById(R.id.edit_text_property_title);
        description = findViewById(R.id.edit_text_property_description);
        area = findViewById(R.id.edit_text_property_area);
        price = findViewById(R.id.edit_text_property_price);
        rooms = findViewById(R.id.edit_text_houses_rooms);
        bathrooms = findViewById(R.id.edit_text_houses_bathrooms);
        stories = findViewById(R.id.edit_text_houses_stories);
        address = findViewById(R.id.edit_text_property_address);
        areaUnitSpinner = findViewById(R.id.areaunit);
        citySpinner = findViewById(R.id.select_city_spinner);
        propertyimage = findViewById(R.id.propertyimage);
        areaUnitArray = getResources().getStringArray(R.array.area_unit);
        cityArray = getResources().getStringArray(R.array.city_array);
        progressDialog = new ProgressDialog(this);
        uploadbtn = findViewById(R.id.upload_property_btn);
        image2 = findViewById(R.id.image2);
        image3 = findViewById(R.id.image3);
        uploadimagebtn = findViewById(R.id.uploadimagebtn);
        username = findViewById(R.id.contact_name_textview);
        usercontat = findViewById(R.id.contact_number_textview);
        anothernumber = findViewById(R.id.edit_text_another_cell_number_contact);
        radioGroup = findViewById(R.id.radiogroup);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == GALLERY_CODE){
            if (grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_GRANTED){
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                galleryIntent.setType("image/*");
//                galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true);
//                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(galleryIntent,GALLERY_CODE);
            }else {
                Toast.makeText(this, "You don't have permission to access files", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public String getRealPathFromUri(Uri contentUri){
        String result;
        Cursor cursor = getContentResolver().query(contentUri,null,null,null,null);
        if (cursor == null){
            result = contentUri.getPath();
        }else{
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_CODE && resultCode == RESULT_OK && null != data){
            Uri selectedimage = data.getData();
            imgString = getRealPathFromUri(selectedimage);

            try {
                InputStream inputStream = getContentResolver().openInputStream(selectedimage);
                propertyimage.setImageBitmap(BitmapFactory.decodeStream(inputStream));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }



        }else{
            Toast.makeText(this, "you have not pick image", Toast.LENGTH_SHORT).show();
        }
    }

    private void insertDataToServer(){
        HttpAsyncRequest request = new HttpAsyncRequest(this,url, HttpAsyncRequest.RequestType.POST,this,listener);
        int selectedid = radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = findViewById(selectedid);
        request.addParam("title",title.getText().toString().trim());
        request.addParam("description",description.getText().toString().trim());
        request.addParam("area",area.getText().toString().trim());
        request.addParam("areaunit",areaUnitArray[areaUnitSpinner.getSelectedItemPosition()]);
        request.addParam("areaname",cityArray[citySpinner.getSelectedItemPosition()]);
        request.addParam("price",price.getText().toString().trim());
        request.addParam("address",address.getText().toString().trim());
        request.addParam("rooms",rooms.getText().toString().trim());
        request.addParam("bathrooms",bathrooms.getText().toString().trim());
        request.addParam("secondnumber",anothernumber.getText().toString().trim());
        request.addParam("stories",stories.getText().toString().trim());
        request.addParam("gas",radioButton.getText().toString().trim());
        if (getIntent().getStringExtra("status").equals("update")){
            request.addParam("houseid",getIntent().getStringExtra("id"));
            if (imgString == null){
                request.addParam("imagestatus","false");
            }else{
                request.addParam("imagestatus","true");
                request.addFile("image",imgString);
            }
        }else{
            request.addParam("type",propertyType);
            request.addParam("userid",userid);
            request.addFile("image",imgString);
        }


        request.execute();
        progressDialog.setMessage("Sending.......");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

    }

    AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            progressDialog.dismiss();
            if (result.isSuccess()){
                finish();

            }else{
                Toast.makeText(AddHousesActivity.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();

        if (httpCode==SUCCESS){
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (!jsonObject.getBoolean("error")){
                   // check = true;
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    result.success(true);
                }else{
                  //  check = false;
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    result.success(true);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);
            }
        }
        return result;
    }
}
