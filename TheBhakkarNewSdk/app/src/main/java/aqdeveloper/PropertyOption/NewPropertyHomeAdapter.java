package aqdeveloper.PropertyOption;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import aqdeveloper.thebhakkar.Adapters.HomeAdapter;
import aqdeveloper.thebhakkar.DataProviders.HomeDataProvider;
import aqdeveloper.thebhakkar.R;

public class NewPropertyHomeAdapter extends RecyclerView.Adapter<NewPropertyHomeAdapter.ViewHolder> {

    private Context mcontext;
    private ArrayList<HomeDataProvider> mList;
    private NewPropertyHomeAdapter.OnItemClickListener mListener;

    public interface OnItemClickListener{
        void  onItemClick(int position);
        void  onButtonChange(int position);
    }

    public void setOnItemClickListener(NewPropertyHomeAdapter.OnItemClickListener listener){
        mListener = listener;
    }

    public NewPropertyHomeAdapter(Context context, ArrayList<HomeDataProvider> list){

        this.mcontext = context;
        this.mList = list;


    }


    @Override
    public NewPropertyHomeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mcontext);
        View view = inflater.inflate(R.layout.new_property_home_customise,parent,false);
        NewPropertyHomeAdapter.ViewHolder viewHolder = new NewPropertyHomeAdapter.ViewHolder(view,mListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NewPropertyHomeAdapter.ViewHolder holder, int position) {

        HomeDataProvider homeDataProvider  = mList.get(position);



        ImageView icons = holder.icons;
        TextView name = holder.btnName;
       // TextView nameEng = holder.nameEng;
       // CardView cardView = holder.cardView;

        icons.setImageResource(homeDataProvider.getBtnIcon());
        name.setText(homeDataProvider.getBtnName());
      //  nameEng.setText(homeDataProvider.getBtnNameEng());




    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView btnName,number,nameEng;
        public ImageView icons;
       // public CardView cardView;


        public ViewHolder(View itemView, final NewPropertyHomeAdapter.OnItemClickListener listener) {
            super(itemView);

            btnName = (TextView) itemView.findViewById(R.id.property_type_textview);
            icons = (ImageView) itemView.findViewById(R.id.property_type_imageview);
          //  cardView = (CardView) itemView.findViewById(R.id.cardview);
           // nameEng =itemView.findViewById(R.id.homeBtnNameEng);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listener!=null){

                        int position  = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }

                }
            });
        }
    }
}