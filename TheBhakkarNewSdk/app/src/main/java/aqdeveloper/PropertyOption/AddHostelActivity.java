package aqdeveloper.PropertyOption;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import aqdeveloper.thebhakkar.Http.AsyncTaskListener;
import aqdeveloper.thebhakkar.Http.HttpAsyncRequest;
import aqdeveloper.thebhakkar.Http.TaskResult;
import aqdeveloper.thebhakkar.Parser.BaseParser;
import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.Url.Constant;

public class AddHostelActivity extends AppCompatActivity implements BaseParser {

    String propertyType, propertyCategory,userid,hostelid,url;
    private String rentInclude="",features="";
    private CheckBox mess,electricity,cleanliness,qualeen,internet,gas,ups,kitchen,parking,
    openarea,ac,mosque,security;
    TextView propertyTypeTv, propertyCategoryTv,username,usercontat;;
    private EditText title,description,rent,rooms,address,nearyby,secondnumber;
    private Spinner rentSpinner;
    private ImageView propertyimage,image2,image3;
    protected static final int GALLERY_CODE = 20;
    String imgString=null,imgString2=null,imgString3=null;
    private String[] areaUnitArray;
    private ProgressDialog progressDialog;
    private Button uploadbtn;
    private ImageButton uploadimagebtn;
    public  static SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_hostel);
        views();
        preferences = getSharedPreferences("EStore", Context.MODE_PRIVATE);
        userid = preferences.getString("userid","");

        username.setText(preferences.getString("name",""));
        usercontat.setText(preferences.getString("phone",""));

        if (getIntent().getStringExtra("status").equals("update")){
            url = Constants.UPDATE_HOSTELS;
            title.setText(getIntent().getStringExtra("name"));
            description.setText(getIntent().getStringExtra("description"));
            rent.setText(getIntent().getStringExtra("rent"));
            rentSpinner.setSelection(((ArrayAdapter)rentSpinner.getAdapter()).getPosition(getIntent().getStringExtra("rentunit")));
            rooms.setText(getIntent().getStringExtra("rooms"));
            address.setText(getIntent().getStringExtra("address"));
            nearyby.setText(getIntent().getStringExtra("nearby"));
            secondnumber.setText(getIntent().getStringExtra("secondnumber"));
            hostelid = getIntent().getStringExtra("hostelid");
            String rentInclude = getIntent().getStringExtra("rentinclude");
            String features = getIntent().getStringExtra("features");
            rentInclude = rentInclude.replaceAll(",$","");` 
            features = features.replaceAll(",$","");
            List<String> rentList = new ArrayList<String>(Arrays.asList(rentInclude.split(",")));
            List<String> featuresList = new ArrayList<String>(Arrays.asList(features.split(",")));
            for (int i =0;i<rentList.size();i++){
                if (rentList.get(i).equals("Mess")){
                    mess.setChecked(true);
                }else if (rentList.get(i).equals("Electricity")){
                    electricity.setChecked(true);
                }else if (rentList.get(i).equals("Cleanliness")){
                    cleanliness.setChecked(true);
                }else if (rentList.get(i).equals("Qaleen")){
                    qualeen.setChecked(true);
                }
            }
            for (int j=0;j<featuresList.size();j++){
                if (featuresList.get(j).equals("Internet")){
                    internet.setChecked(true);
                }else if (featuresList.get(j).equals("Gas")){
                    gas.setChecked(true);
                }else if (featuresList.get(j).equals("Generator or UPS")){
                    ups.setChecked(true);
                }else if (featuresList.get(j).equals("Kitchen")){
                    kitchen.setChecked(true);
                }else if (featuresList.get(j).equals("Parking")){
                    parking.setChecked(true);
                }else if (featuresList.get(j).equals("Open Area")){
                    openarea.setChecked(true);
                }else if (featuresList.get(j).equals("AC or Air Coolers")){
                    ac.setChecked(true);
                }else if (featuresList.get(j).equals("Near By Mosque")){
                    mosque.setChecked(true);
                }else if (featuresList.get(j).equals("Security")){
                    security.setChecked(true);
                }
            }
            propertyTypeTv.setText("Hostels");
            propertyCategoryTv.setText("Update");
            uploadbtn.setText("Update Now");
        }else{
            url = Constants.ADD_HOSTELS;
            Intent intent = getIntent();
            propertyType = intent.getStringExtra("type");
            if (propertyType.equals("Sell")){
                propertyType = "Buy";
            }
            propertyCategory = intent.getStringExtra("category");

            propertyTypeTv.setText(propertyType);
            propertyCategoryTv.setText(propertyCategory);
            uploadbtn.setText("Upload Now");
        }


        uploadimagebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(AddHostelActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},GALLERY_CODE);

            }
        });

        uploadbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (title.getText().length()==0){
                    title.setError("This field is required");
                }else if (description.getText().length()==0){
                    description.setError("This field is required");
                }else if (rent.getText().length()==0){
                    rent.setError("This field is required");
                }else if (rooms.getText().length()==0){
                    rooms.setError("This field is required");
                }else if (address.getText().length()==0){
                    address.setError("This field is required");
                }else if (nearyby.getText().length()==0){
                    nearyby.setError("This field is required");
                }else if (secondnumber.getText().length()==0){
                    secondnumber.setError("This field is required");
                }else{
                    insertDataToServer();
                }
            }
        });

    }


    private  void  views(){

        propertyTypeTv = findViewById(R.id.property_type_textview);
        propertyCategoryTv =findViewById(R.id.property_category_textview);
        title = findViewById(R.id.edit_text_property_title);
        description = findViewById(R.id.edit_text_property_description);
        rent = findViewById(R.id.edit_text_property_area);
        rooms = findViewById(R.id.rooms);
        address = findViewById(R.id.edit_text_property_address);
        rentSpinner = findViewById(R.id.areaunit);
        propertyimage = findViewById(R.id.propertyimage);
        areaUnitArray = getResources().getStringArray(R.array.rent_perhead_perroom);
        progressDialog = new ProgressDialog(this);
        uploadbtn = findViewById(R.id.upload_property_btn);
        image2 = findViewById(R.id.image2);
        image3 = findViewById(R.id.image3);
        uploadimagebtn = findViewById(R.id.uploadimagebtn);
        nearyby = findViewById(R.id.edit_text_near_by_institute);
        secondnumber = findViewById(R.id.edit_text_another_cell_number_contact);
        mess = findViewById(R.id.mess);
        electricity = findViewById(R.id.electricity);
        cleanliness = findViewById(R.id.cleanliness);
        qualeen = findViewById(R.id.qualeen);
        internet = findViewById(R.id.internet);
        gas = findViewById(R.id.gas);
        ups = findViewById(R.id.ups);
        kitchen = findViewById(R.id.kitchen);
        parking = findViewById(R.id.parking);
        openarea = findViewById(R.id.openarea);
        ac = findViewById(R.id.ac);
        mosque = findViewById(R.id.mosque);
        security = findViewById(R.id.security);
        username = findViewById(R.id.contact_name_textview);
        usercontat = findViewById(R.id.contact_number_textview);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == GALLERY_CODE){
            if (grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_GRANTED){
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                galleryIntent.setType("image/*");
//                galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true);
//                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(galleryIntent,GALLERY_CODE);
            }else {
                Toast.makeText(this, "You don't have permission to access files", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public String getRealPathFromUri(Uri contentUri){
        String result;
        Cursor cursor = getContentResolver().query(contentUri,null,null,null,null);
        if (cursor == null){
            result = contentUri.getPath();
        }else{
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_CODE && resultCode == RESULT_OK && null != data){
            Uri selectedimage = data.getData();
            imgString = getRealPathFromUri(selectedimage);

            try {
                InputStream inputStream = getContentResolver().openInputStream(selectedimage);
                propertyimage.setImageBitmap(BitmapFactory.decodeStream(inputStream));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }



        }else{
            Toast.makeText(this, "you have not pick image", Toast.LENGTH_SHORT).show();
        }
    }

    private void insertDataToServer(){
        HttpAsyncRequest request = new HttpAsyncRequest(this,url, HttpAsyncRequest.RequestType.POST,this,listener);
        if (mess.isChecked()){
            rentInclude += mess.getText().toString()+",";
        }
        if (electricity.isChecked()){
            rentInclude += electricity.getText().toString()+",";
        }
        if (cleanliness.isChecked()){
            rentInclude += cleanliness.getText().toString()+",";
        }
        if (qualeen.isChecked()){
            rentInclude += qualeen.getText().toString()+",";
        }
        if (internet.isChecked()){
            features += internet.getText().toString()+",";
        }
        if (gas.isChecked()){
            features += gas.getText().toString()+",";
        }
        if (ups.isChecked()){
            features += ups.getText().toString()+",";
        }
        if (kitchen.isChecked()){
            features += kitchen.getText().toString()+",";
        }
        if (parking.isChecked()){
            features += parking.getText().toString()+",";
        }
        if (openarea.isChecked()){
            features += openarea.getText().toString()+",";
        }
        if (ac.isChecked()){
            features += ac.getText().toString()+",";
        }
        if (mosque.isChecked()){
            features += mosque.getText().toString()+",";
        }
        if (security.isChecked()){
            features += security.getText().toString()+",";
        }
        request.addParam("hostelname",title.getText().toString().trim());
        request.addParam("description",description.getText().toString().trim());
        request.addParam("rent",rent.getText().toString().trim());
        request.addParam("rentinclude",rentInclude);
        request.addParam("features",features);
        request.addParam("rentunit",areaUnitArray[rentSpinner.getSelectedItemPosition()]);
//        request.addParam("areaname",cityArray[citySpinner.getSelectedItemPosition()]);
        request.addParam("rooms",rooms.getText().toString().trim());
        request.addParam("address",address.getText().toString().trim());
        request.addParam("nearby",nearyby.getText().toString().trim());
        request.addParam("secondnumber",secondnumber.getText().toString().trim());
        if (getIntent().getStringExtra("status").equals("update")){
            request.addParam("hostelid",hostelid);
            request.addParam("imagename",getIntent().getStringExtra("imagename"));
            if (imgString == null){
                request.addParam("imagestatus","false");
            }else{
                request.addParam("imagestatus","true");
                request.addFile("image",imgString);
            }
        }else{
            request.addParam("userid",userid);
            request.addFile("image",imgString);
        }


        request.execute();
        progressDialog.setMessage("Sending.......");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

    }

    AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            progressDialog.dismiss();
            if (result.isSuccess()){
                finish();

            }else{
                Toast.makeText(AddHostelActivity.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();

        if (httpCode==SUCCESS){
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (!jsonObject.getBoolean("error")){
                    // check = true;
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    result.success(true);
                }else{
                    //  check = false;
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    result.success(true);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);
            }
        }
        return result;
    }
}
