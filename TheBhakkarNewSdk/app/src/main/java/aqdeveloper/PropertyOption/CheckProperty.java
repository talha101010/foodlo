package aqdeveloper.PropertyOption;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import aqdeveloper.thebhakkar.R;

public class CheckProperty extends AppCompatActivity {
    private Button houses,residentialplots,commercialplots,agricultureland,shops,buildings,
                    hostels,hotels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_property);
        houses = findViewById(R.id.housesbtn);
        residentialplots = findViewById(R.id.residentialplotsbtn);
        commercialplots = findViewById(R.id.commercialbtn);
        agricultureland = findViewById(R.id.agriculturebtn);
        shops = findViewById(R.id.shopsbtn);
        buildings = findViewById(R.id.buildings);
        hostels = findViewById(R.id.hostelsbtn);
        hotels = findViewById(R.id.hotelsbtn);

        houses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(CheckProperty.this,ShowProperty.class);
                intent1.putExtra("type","houses");
                startActivity(intent1);
            }
        });

        residentialplots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(CheckProperty.this,ShowProperty.class);
                intent1.putExtra("type","Residential");
                startActivity(intent1);
            }
        });

        commercialplots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(CheckProperty.this,ShowProperty.class);
                intent1.putExtra("type","Commertial");
                startActivity(intent1);
            }
        });

        agricultureland.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(CheckProperty.this,ShowProperty.class);
                intent1.putExtra("type","Agriculture");
                startActivity(intent1);
            }
        });

        shops.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(CheckProperty.this,ShowProperty.class);
                intent1.putExtra("type","Shops");
                startActivity(intent1);
            }
        });

        buildings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(CheckProperty.this,ShowProperty.class);
                intent1.putExtra("type","Buildings");
                startActivity(intent1);
            }
        });
        hostels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(CheckProperty.this,ShowHostels.class);
                intent1.putExtra("type","Hostels");
                intent1.putExtra("prostatus","user");
                startActivity(intent1);
            }
        });
        hotels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(CheckProperty.this,ShowProperty.class);
                intent1.putExtra("type","Hotels");

                startActivity(intent1);
            }
        });
    }
}
