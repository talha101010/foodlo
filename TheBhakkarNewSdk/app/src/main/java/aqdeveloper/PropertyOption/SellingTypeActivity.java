package aqdeveloper.PropertyOption;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import aqdeveloper.thebhakkar.DataProviders.HomeDataProvider;
import aqdeveloper.thebhakkar.DrawerActivities.LoginPanel;
import aqdeveloper.thebhakkar.NewHome;
import aqdeveloper.thebhakkar.R;

public class SellingTypeActivity extends AppCompatActivity {

    Button rent_btn, sell_btn,select_atleast_one_option_btn;
    TextView NoteTypeTv;
    public  static SharedPreferences preferences;
    RecyclerView recyclerView;
    // recycler view data

    String[] btnName = {"Houses", "Residential Plots", "Commertial Plots", "Agriculture Land", "Shops", "Buildings"};
    String[] rent_name = {"Houses", "Hostels", "Shops", "Agriculture Land", "Hotels", "Buildings"};
    String[] btnNameEng;
    private ArrayList<HomeDataProvider> list;
    int[] icon = {R.drawable.houses, R.drawable.residential_plot, R.drawable.commertial_plots, R.drawable.agriculture_land, R.drawable.shop, R.drawable.buildings};
    int[] rent_icon = {R.drawable.houses, R.drawable.room, R.drawable.shop, R.drawable.agriculture_land, R.drawable.hotels, R.drawable.buildings};
    SellingTypeActivityAdapter homeAdapter;
    HomeDataProvider homeDataProvider;
    Button checkPropertyBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selling_type);
        preferences = getSharedPreferences("EStore", Context.MODE_PRIVATE);
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        list = new ArrayList<HomeDataProvider>();
        NoteTypeTv = findViewById(R.id.type_textview_note);
        getArrayData(btnName,icon,null);
        checkPropertyBtn = findViewById(R.id.check_your_property_btn1);

        dialog();






        // rent_btn = findViewById(R.id.rent_btn_id);

        // Sell button code start from here

        select_atleast_one_option_btn = findViewById(R.id.select_at_least_one_option_btn);
        select_atleast_one_option_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog();
            }
        });


        checkPropertyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent1 = new Intent(SellingTypeActivity.this,CheckProperty.class);
                startActivity(intent1);

            }
        });





    }

    @Override
    protected void onResume() {
        super.onResume();
        if (preferences.getString("userid","").equals("")){
            Toast.makeText(SellingTypeActivity.this, "please login to continue", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(SellingTypeActivity.this,LoginPanel.class));
            return;
        }
    }

    private void getArrayData(String[] name, int[] icon, final String type) {





        try {
            for (int i = 0; i < name.length; i++) {

                int geticon = icon[i];
                String namestr = name[i];
                //   String nameEng = btnNameEng[i];
                HomeDataProvider homeDataProvider = new HomeDataProvider(namestr, type, geticon);
                list.add(homeDataProvider);
            }

            homeAdapter = new SellingTypeActivityAdapter(getApplicationContext(), list);
            recyclerView.setAdapter(homeAdapter);

            homeAdapter.setOnItemClickListener(new SellingTypeActivityAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(int position) {


                    switch (position) {

                        case 0:

                            if (preferences.getString("userid", "").equals("")) {
                                Toast.makeText(SellingTypeActivity.this, "please login to continue", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(SellingTypeActivity.this, LoginPanel.class));

                            } else {

                                if (type!=null){
                                    Intent intent = new Intent(SellingTypeActivity.this, AddHousesActivity.class);
                                    intent.putExtra("type", list.get(position).getBtnNameEng());
                                    intent.putExtra("category", list.get(position).getBtnName());
                                    intent.putExtra("status", "insert");
                                    startActivity(intent);}

                                else {

                                    dialog();
                                }
                            }


                            break;

                        case 1:

                            if (type!=null){
                                if (type.equals("Rent")){
                                    Intent intent = new Intent(SellingTypeActivity.this, AddHostelActivity.class);
                                    intent.putExtra("type", list.get(position).getBtnNameEng());
                                    intent.putExtra("category", list.get(position).getBtnName());
                                    intent.putExtra("plottype","Hostel");
                                    intent.putExtra("status", "insert");
                                    startActivity(intent);
                                }else{
                                    Intent intent = new Intent(SellingTypeActivity.this, AddPlotsActivity.class);
                                    intent.putExtra("type", list.get(position).getBtnNameEng());
                                    intent.putExtra("category", list.get(position).getBtnName());
                                    intent.putExtra("plottype","Residential");
                                    intent.putExtra("status", "insert");
                                    startActivity(intent);
                                }

                            } else {

                                dialog();
                            }

                            break;

                        case 2:

                            if (type!=null){
                                Intent intent = new Intent(SellingTypeActivity.this, AddPlotsActivity.class);
                                intent.putExtra("type", list.get(position).getBtnNameEng());
                                intent.putExtra("category", list.get(position).getBtnName());
                                intent.putExtra("plottype","Commertial");
                                intent.putExtra("status", "insert");
                                startActivity(intent);}

                            else {

                                dialog();
                            }

                            break;

                        case 3:

                            if (type!=null){
                                Intent intent = new Intent(SellingTypeActivity.this, AddPlotsActivity.class);
                                intent.putExtra("type", list.get(position).getBtnNameEng());
                                intent.putExtra("category", list.get(position).getBtnName());
                                intent.putExtra("plottype","Agriculture");
                                intent.putExtra("status", "insert");
                                startActivity(intent);}

                            else {

                                dialog();
                            }

                            break;

                        case 4:

                            if (type!=null){
                                Intent intent = new Intent(SellingTypeActivity.this, AddPlotsActivity.class);
                                intent.putExtra("type", list.get(position).getBtnNameEng());
                                intent.putExtra("category", list.get(position).getBtnName());
                                intent.putExtra("plottype","Shops");
                                intent.putExtra("status", "insert");
                                startActivity(intent);}

                            else {

                                dialog();
                            }

                            break;

                        case 5:
                            if (type!=null){
                                Intent intent = new Intent(SellingTypeActivity.this, AddPlotsActivity.class);
                                intent.putExtra("type", list.get(position).getBtnNameEng());
                                intent.putExtra("category", list.get(position).getBtnName());
                                intent.putExtra("plottype","Buildings");
                                intent.putExtra("status", "insert");
                                startActivity(intent);}

                            else {

                                dialog();
                            }

                            break;
                    }
                }

                @Override
                public void onButtonChange(int position) {

                }
            });


        } catch (Exception e) {

            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();

        }



    }

    public void dialog(){

        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(SellingTypeActivity.this);
        LayoutInflater inflater =getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.sell_rent_property_dialog,null);
        rent_btn = dialogView.findViewById(R.id.rent_btn_id);
        sell_btn =dialogView.findViewById(R.id.sell_btn_id);
        alertdialog.setView(dialogView);
        final AlertDialog dialog = alertdialog.create();
        dialog.show();

        rent_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NoteTypeTv.setVisibility(View.VISIBLE);
                sell_btn.setBackgroundResource(R.drawable.property_type_defualt_btn);
                sell_btn.setTextColor(Color.BLACK);
                rent_btn.setBackgroundResource(R.drawable.property_type_btn);
                rent_btn.setTextColor(Color.WHITE);
                NoteTypeTv.setText("Please select the type of rental property"+"\n"+"You have selected RENT Option");
                list.clear();
                getArrayData(rent_name,rent_icon,"Rent");
                homeAdapter.notifyDataSetChanged();
                dialog.dismiss();




            }
        });


        sell_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NoteTypeTv.setVisibility(View.VISIBLE);

                NoteTypeTv.setText("Please select the type of selling property"+"\n"+"You have selected SELL option");
                sell_btn.setBackgroundResource(R.drawable.property_type_btn);
                sell_btn.setTextColor(Color.WHITE);
                rent_btn.setBackgroundResource(R.drawable.property_type_defualt_btn);
                rent_btn.setTextColor(Color.BLACK);
                list.clear();
                getArrayData(btnName,icon,"Sell");
                homeAdapter.notifyDataSetChanged();
                dialog.dismiss();

            }
        });

    }
}