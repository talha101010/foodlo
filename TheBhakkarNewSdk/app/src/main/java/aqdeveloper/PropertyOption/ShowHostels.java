package aqdeveloper.PropertyOption;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import aqdeveloper.PropertyOption.Adapters.HostelsAdapter;
import aqdeveloper.PropertyOption.Adapters.HousesAdapter;
import aqdeveloper.PropertyOption.ListItems.HostelsListItems;
import aqdeveloper.PropertyOption.ListItems.HousesListItems;
import aqdeveloper.thebhakkar.R;

public class ShowHostels extends AppCompatActivity {

    private RecyclerView recyclerView;
    public  static SharedPreferences preferences;
    private List<HostelsListItems> list;
    private RecyclerView.Adapter adapter;
    private ProgressBar progressBar;
    private String userid;
    private TextView noPropertyYet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_hostels);
        preferences = getSharedPreferences("EStore", Context.MODE_PRIVATE);
        userid = preferences.getString("userid","");
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        list = new ArrayList<>();
        progressBar = findViewById(R.id.progressbar);
        noPropertyYet = findViewById(R.id.no_property_added_yet);
        if (getIntent().getStringExtra("prostatus").equals("all")){
            getHostelsFromServer();
        }else{
            getHostelsFromServerUser();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (HostelDetails.activity!=null){
            if (HostelDetails.checkStat){
                if (list!=null){
                    list.clear();
                }
                getHostelsFromServerUser();
                HostelDetails.checkStat = false;
            }
        }
    }

    private void getHostelsFromServer(){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.SHOW_HOSTELS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (adapter!=null){
                            list.clear();
                        }
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("hostels");
                            if (jsonArray.length()!=0){
                                for (int i =0;i<jsonArray.length();i++){
                                    JSONObject json = jsonArray.getJSONObject(i);
                                    HostelsListItems listItems = new HostelsListItems(json.getString("id"),
                                            json.getString("name"),json.getString("description"),
                                            json.getString("rent"),json.getString("rentcategory"),
                                            json.getString("rentinclude"),json.getString("rooms"),
                                            json.getString("address"),json.getString("nearby"),
                                            json.getString("features"),json.getString("secondnumber"),
                                            json.getString("userid"),json.getString("uploadtime"),
                                            json.getString("views"),json.getString("imagename"),
                                            json.getString("image"));
                                    list.add(listItems);
                                }
                                adapter = new HostelsAdapter(list,ShowHostels.this,true);
                                recyclerView.setAdapter(adapter);
                            }else{
                                noPropertyYet.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ShowHostels.this, error.getMessage()+"", Toast.LENGTH_SHORT).show();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void getHostelsFromServerUser(){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.SHOW_HOSTELS_AGAINT_USER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (adapter!=null){
                            list.clear();
                            adapter.notifyDataSetChanged();
                        }
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("hostels");
                            if (jsonArray.length()!=0){
                                for (int i =0;i<jsonArray.length();i++){
                                    JSONObject json = jsonArray.getJSONObject(i);
                                    HostelsListItems listItems = new HostelsListItems(json.getString("id"),
                                            json.getString("name"),json.getString("description"),
                                            json.getString("rent"),json.getString("rentcategory"),
                                            json.getString("rentinclude"),json.getString("rooms"),
                                            json.getString("address"),json.getString("nearby"),
                                            json.getString("features"),json.getString("secondnumber"),
                                            json.getString("userid"),json.getString("uploadtime"),
                                            json.getString("views"),json.getString("imagename"),
                                            json.getString("image"));
                                    list.add(listItems);
                                }
                                adapter = new HostelsAdapter(list,ShowHostels.this,false);
                                recyclerView.setAdapter(adapter);
                            }else{
                                noPropertyYet.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ShowHostels.this, error.getMessage()+"", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("userid",userid);
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
