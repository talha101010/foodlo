package aqdeveloper.PropertyOption;

public class Constants {
    private static final String ROOT_URL = "http://www.thebhakkar.com/property/appwebservices/";

    public static final String insertHouses = ROOT_URL+"insertHousesData.php";
    public static final String showHouses = ROOT_URL+"showHouses.php";
    public static final String SHOW_EXTRA_IMAGES = ROOT_URL+"show_extra_images.php";
    public static final String UPDATE_VIEWS = ROOT_URL+"updateViews.php";
    public static final String HOUSES_AGAINST_USER = ROOT_URL+"showHousesAgainstUser.php";
    public static final String INSERT_EXTRA_IMAGES = ROOT_URL+"insertExtraImages.php";
    public static final String DELETE_EXTRA_IMAGES = ROOT_URL+"deleteExtraImages.php";
    public static final String UPDATE_HOUSES = ROOT_URL+"updateHouses.php";
    public static final String DEAL_DONE = ROOT_URL+"changeHouseStatus.php";
    public static final String HOUSES_AGAINST_AREA = ROOT_URL+"showHousesAgainstArea.php";
    public static final String ADD_FAVOURITES = ROOT_URL+"addFavourites.php";

    public static final String ADD_PLOTS = ROOT_URL+"addPlots.php";
    public static final String SHOW_PLOTS = ROOT_URL+"showPlots.php";
    public static final String SHOW_PLOTS_AGAINST_AREA = ROOT_URL+"showPlotsAgainstArea.php";
    public static final String SHOW_PLOTS_AGAINST_USER = ROOT_URL+"showPlotsAgainstUser.php";
    public static final String UPDATE_PLOTS = ROOT_URL+"updatePlots.php";
    public static final String ADD_HOSTELS = ROOT_URL+"addHostel.php";
    public static final String SHOW_HOSTELS = ROOT_URL+"showHostels.php";
    public static final String SHOW_HOSTELS_AGAINT_USER = ROOT_URL+"showHostelsAgainstUser.php";
    public static final String UPDATE_HOSTELS = ROOT_URL+"updateHostels.php";
    public static final String UPDATE_HOSTELS_VIEWS = ROOT_URL+"updateHostelsViews.php";
    public static final String CHANGE_HOSTEL_STATUS = ROOT_URL+"changeHostelStatus.php";

    public static final String UPDATE_PLOTS_VIEWS = ROOT_URL+"updatePlotsViews.php";
    public static final String CHANGE_PLOTS_STATUS = ROOT_URL+"changePlotsStatus.php";

    public static final String GET_USER_PHONE = "http://www.thebhakkar.com/superstore/superstorewebservices/getUsersData.php";
}
