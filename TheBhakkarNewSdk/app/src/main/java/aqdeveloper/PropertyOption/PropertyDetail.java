package aqdeveloper.PropertyOption;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.IndicatorView.draw.controller.DrawController;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import aqdeveloper.PropertyOption.Adapters.SliderAdapter;
import aqdeveloper.PropertyOption.ListItems.SliderListItems;
import aqdeveloper.thebhakkar.Http.AsyncTaskListener;
import aqdeveloper.thebhakkar.Http.HttpAsyncRequest;
import aqdeveloper.thebhakkar.Http.TaskResult;
import aqdeveloper.thebhakkar.Parser.BaseParser;
import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.Url.Constant;

public class PropertyDetail extends AppCompatActivity {

    private TextView title,description,area,price,address,rooms,bathrooms,stories,date,views,areaname,gas;
    private String userid,url,id,titleString,descriptionString,areaString,priceString,addressString,roomsString,bathroomsString,storiesString,dateString,secondNumberString,areaUnitString,areaNameString,gasString;
    private int viewsInteger,updatedviews;
    private List<SliderListItems> list1;
    private LinearLayout roomslayout,bathroomslayout,storieslayout;
    SliderView sliderView;
    private Button deleteAddBtn,updatebtn,changestatus,contactBtn,rent_btn,sell_btn;
    private ProgressDialog progressDialog;
    private TextView extratext;
    public static Activity activity;
    public static boolean checkStat=false;
    private String phoneNumber,Name,viewsURL,statusURL;
//    public  static SharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_detail);
        getViews();
        activity = PropertyDetail.this;
        titleString = getIntent().getStringExtra("title");
        descriptionString = getIntent().getStringExtra("description");
        areaString = getIntent().getStringExtra("area");
        priceString = getIntent().getStringExtra("price");
        addressString = getIntent().getStringExtra("address");
        roomsString = getIntent().getStringExtra("rooms");
        bathroomsString = getIntent().getStringExtra("bathrooms");
        storiesString = getIntent().getStringExtra("stories");
        dateString = getIntent().getStringExtra("date");
        viewsInteger = Integer.parseInt(getIntent().getStringExtra("views"));
        url = getIntent().getStringExtra("url");
        id = getIntent().getStringExtra("id");
        secondNumberString = getIntent().getStringExtra("secondnumber");
        areaUnitString = getIntent().getStringExtra("areaunit");
        userid = getIntent().getStringExtra("userid");
        areaNameString = getIntent().getStringExtra("areaname");
        gasString = getIntent().getStringExtra("gas");

        if (roomsString.equals("null")){
            rooms.setVisibility(View.GONE);
            bathrooms.setVisibility(View.GONE);
            stories.setVisibility(View.GONE);
            roomslayout.setVisibility(View.GONE);
            bathroomslayout.setVisibility(View.GONE);
            storieslayout.setVisibility(View.GONE);
        }

        title.setText(titleString);
        description.setText(descriptionString);
        area.setText(areaString+" "+areaUnitString);
        price.setText(priceString);
        address.setText(addressString);
        rooms.setText(roomsString);
        bathrooms.setText(bathroomsString);
        stories.setText(storiesString);
        date.setText(dateString);
        views.setText(viewsInteger+"");
        areaname.setText(areaNameString);
        gas.setText(gasString);
        updatedviews = viewsInteger+1;
        getExtraImages();


//        for (int i =0;i<2;i++){
//            SliderListItems listitems1 = new SliderListItems("this is name","http://www.ilmistaan.com/property/housesimages/7.jpg");
//            list1.add(listitems1);
//        }
//        final SliderAdapter adapter = new SliderAdapter(PropertyDetail.this,list1);
//        sliderView.setSliderAdapter(adapter);

        sliderView.setIndicatorAnimation(IndicatorAnimations.SLIDE); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.CUBEINROTATIONTRANSFORMATION);
      //  sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        //sliderView.setScrollTimeInSec(10); //set scroll delay in seconds :
       // sliderView.startAutoCycle();

        sliderView.setOnIndicatorClickListener(new DrawController.ClickListener() {
            @Override
            public void onIndicatorClicked(int position) {
                sliderView.setCurrentPagePosition(position);
            }
        });
        if (getIntent().getBooleanExtra("check",true)){
            if (roomsString.equals("null")){
                viewsURL = Constants.UPDATE_PLOTS_VIEWS;
                statusURL = Constants.CHANGE_PLOTS_STATUS;
            }else{
                viewsURL = Constants.UPDATE_VIEWS;
                statusURL = Constants.DEAL_DONE;
            }
            updateViews();
            getPhoneNumber();
            deleteAddBtn.setVisibility(View.GONE);
            updatebtn.setVisibility(View.GONE);
            changestatus.setVisibility(View.GONE);
        }else{
            deleteAddBtn.setVisibility(View.VISIBLE);
            updatebtn.setVisibility(View.VISIBLE);
            changestatus.setVisibility(View.VISIBLE);
            contactBtn.setVisibility(View.GONE);
//            preferences = getSharedPreferences("EStore", Context.MODE_PRIVATE);
//            userid = preferences.getString("userid","");
        }

        deleteAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
           Intent intent = new Intent(PropertyDetail.this,DeleteAddActivity.class);
           if (roomsString.equals("null")){
               intent.putExtra("id",id+"p");
           }else{
               intent.putExtra("id",id);
           }
           startActivity(intent);

            }
        });

        contactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog();
            }
        });

        updatebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (roomsString.equals("null")){
                    Intent intent = new Intent(PropertyDetail.this,AddPlotsActivity.class);
                    intent.putExtra("title",titleString);
                    intent.putExtra("description",descriptionString);
                    intent.putExtra("area",areaString);
                    intent.putExtra("price",priceString);
                    intent.putExtra("address",addressString);
                    intent.putExtra("rooms",roomsString);
                    intent.putExtra("bathrooms",bathroomsString);
                    intent.putExtra("stories",storiesString);
                    intent.putExtra("id",id);
                    intent.putExtra("imagename",getIntent().getStringExtra("imagename"));
                    intent.putExtra("secondnumber",secondNumberString);
                    intent.putExtra("areaunit",areaUnitString);
                    intent.putExtra("areaname",areaNameString);
                    intent.putExtra("gas",gasString);
                    intent.putExtra("status","update");
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(PropertyDetail.this,AddHousesActivity.class);
                    intent.putExtra("title",titleString);
                    intent.putExtra("description",descriptionString);
                    intent.putExtra("area",areaString);
                    intent.putExtra("price",priceString);
                    intent.putExtra("address",addressString);
                    intent.putExtra("rooms",roomsString);
                    intent.putExtra("bathrooms",bathroomsString);
                    intent.putExtra("stories",storiesString);
                    intent.putExtra("id",id);
                    intent.putExtra("imagename",getIntent().getStringExtra("imagename"));
                    intent.putExtra("secondnumber",secondNumberString);
                    intent.putExtra("areaunit",areaUnitString);
                    intent.putExtra("areaname",areaNameString);
                    intent.putExtra("gas",gasString);
                    intent.putExtra("status","update");
                    startActivity(intent);
                }

            }
        });

        changestatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dealDone();
            }
        });
    }
    private void getViews(){

        contactBtn =findViewById(R.id.contact_btn);

        title = findViewById(R.id.title);
        description = findViewById(R.id.description);
        area = findViewById(R.id.area);
        price = findViewById(R.id.price);
        address = findViewById(R.id.address);
        rooms = findViewById(R.id.rooms);
        bathrooms = findViewById(R.id.bathrooms);
        stories = findViewById(R.id.stories);
        date = findViewById(R.id.date);
        views = findViewById(R.id.views);
        list1 = new ArrayList<>();
        sliderView = findViewById(R.id.imageSlider);
        progressDialog = new ProgressDialog(this);
        deleteAddBtn = findViewById(R.id.delete_add_btn);
        updatebtn = findViewById(R.id.updatebtn);
        changestatus = findViewById(R.id.changestatus);
        areaname = findViewById(R.id.areaname);
        gas = findViewById(R.id.gas);
        roomslayout = findViewById(R.id.roomslayout);
        bathroomslayout = findViewById(R.id.bathroomslayout);
        storieslayout = findViewById(R.id.storieslayout);
    }

    private void updateViews(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, viewsURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject json = new JSONObject(response);
                            if (!json.getBoolean("error")){
                               // Toast.makeText(PropertyDetail.this, "updated", Toast.LENGTH_SHORT).show();
                            }else{
                               // Toast.makeText(PropertyDetail.this, "update error", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               // Toast.makeText(PropertyDetail.this, "error in updating views", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("views",updatedviews+"");
                param.put("id",id);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void getExtraImages(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.SHOW_EXTRA_IMAGES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            if (list1!=null){
                                list1.clear();
                            }
                            JSONObject json = new JSONObject(response);
                            JSONArray jsonArray = json.getJSONArray("extraimages");
                            SliderListItems sliderlist = new SliderListItems(titleString,url);
                            list1.add(sliderlist);
                            if (jsonArray.length()>0){
                               for (int i=0;i<jsonArray.length();i++){
                                       JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        sliderlist = new SliderListItems(titleString,
                                               jsonObject.getString("image"));
                                       list1.add(sliderlist);
                               }
                            }else{

                            }

                            final SliderAdapter adapter = new SliderAdapter(PropertyDetail.this,list1);
                            sliderView.setSliderAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(PropertyDetail.this, "communication server error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                if (roomsString.equals("null")){
                    param.put("id",id+"p");
                }else{
                    param.put("id",id);
                }
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void dealDone(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, statusURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject json = new JSONObject(response);
                            if (!json.getBoolean("error")){
                                  checkStat = true;
                                  Toast.makeText(PropertyDetail.this, "Deal Done", Toast.LENGTH_SHORT).show();
                                  finish();
                            }else{
                                Toast.makeText(PropertyDetail.this, "update error", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(PropertyDetail.this, "error in updating views", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("status","Pending");
                param.put("id",id);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void getPhoneNumber(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.GET_USER_PHONE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject json = new JSONObject(response);
                            if (!json.getBoolean("error")){
                                phoneNumber = json.getString("cell_number");
                                Name = json.getString("name");
                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(PropertyDetail.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("id",userid);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void dialog(){

        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(PropertyDetail.this);
        LayoutInflater inflater =getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.call_dialog,null);
        final CardView primaryCallCardview = dialogView.findViewById(R.id.primary_number_cardview);
        final  CardView secondaryCallCardview = dialogView.findViewById(R.id.secondary_number_cardview);
        final  TextView primaryTextView =dialogView.findViewById(R.id.primary_number_textview);
        final TextView secondaryTextView =dialogView.findViewById(R.id.secondary_number_textview);
        final TextView nameTextview = dialogView.findViewById(R.id.name_textview);
          alertdialog.setView(dialogView);
          final AlertDialog dialog = alertdialog.create();
          dialog.show();

          primaryTextView.setText(phoneNumber);
          secondaryTextView.setText(secondNumberString);
          nameTextview.setText(Name);

          if (secondNumberString.length()<10){

              secondaryCallCardview.setVisibility(View.GONE);

          }

          secondaryCallCardview.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  if (secondNumberString !=null ){
                    Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel",secondNumberString,null));
                    startActivity(callIntent);
                    dialog.dismiss();
                }else{

                      secondaryCallCardview.setVisibility(View.GONE);
                }
              }
          });


        primaryCallCardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (phoneNumber!=null){
                    Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel",phoneNumber,null));
                    startActivity(callIntent);
                    dialog.dismiss();
                }else{
                    Toast.makeText(PropertyDetail.this, "sorry primary number not available", Toast.LENGTH_SHORT).show(); }

            }
        });




    }
}
