package aqdeveloper.PropertyOption;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

import aqdeveloper.thebhakkar.Adapters.HomeAdapter;
import aqdeveloper.thebhakkar.DataProviders.HomeDataProvider;
import aqdeveloper.thebhakkar.R;

public class NewPropertyHome extends AppCompatActivity {

    Button buy_btn,rent_btn,sell_btn;
    RecyclerView recyclerView;
    private String status="Buy";

    // recycler view data

    String[] btnName = {"Houses\n","Residential\nPlots","Commertial\nPlots","Agriculture\nLand","Shops\n","Buildings\n"};
    String[] rent_name = {"Houses\n","Hostels\n","Shops\n","Agriculture\nLand","Hotels\n","Buildings\n"};
     String[]       btnNameEng;
    private ArrayList<HomeDataProvider> list;
    int[] icon = {R.drawable.houses, R.drawable.residential_plot, R.drawable.commertial_plots,R.drawable.agriculture_land ,R.drawable.shop, R.drawable.buildings};
    int[] rent_icon = {R.drawable.houses, R.drawable.room, R.drawable.shop,R.drawable.agriculture_land ,R.drawable.hotels, R.drawable.buildings};
    NewPropertyHomeAdapter homeAdapter;
    HomeDataProvider homeDataProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_property_home);

        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        list = new ArrayList<HomeDataProvider>();
        getArrayData(btnName,icon);


        // Buy button code start from here

        buy_btn = findViewById(R.id.buy_btn);
        buy_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = "Buy";
                buy_btn.setBackgroundResource(R.drawable.property_type_btn);
                buy_btn.setTextColor(Color.WHITE);
                sell_btn.setBackgroundResource(R.drawable.property_type_defualt_btn);
                sell_btn.setTextColor(Color.BLACK);
                rent_btn.setBackgroundResource(R.drawable.property_type_defualt_btn);
                rent_btn.setTextColor(Color.BLACK);
                list.clear();
                getArrayData(btnName,icon);
                homeAdapter.notifyDataSetChanged();

            }
        });

        // Rent button code start from here

        rent_btn = findViewById(R.id.rent_btn_id);
        rent_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = "Rent";
                buy_btn.setBackgroundResource(R.drawable.property_type_defualt_btn);
                buy_btn.setTextColor(Color.BLACK);
                sell_btn.setBackgroundResource(R.drawable.property_type_defualt_btn);
                sell_btn.setTextColor(Color.BLACK);
                rent_btn.setBackgroundResource(R.drawable.property_type_btn);
                rent_btn.setTextColor(Color.WHITE);
                list.clear();
                getArrayData(rent_name,rent_icon);
                homeAdapter.notifyDataSetChanged();

            }
        });

        // Sell button code start from here

        sell_btn = findViewById(R.id.sell_btn_id);
        sell_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                buy_btn.setBackgroundResource(R.drawable.property_type_defualt_btn);
                buy_btn.setTextColor(Color.BLACK);
                sell_btn.setBackgroundResource(R.drawable.property_type_btn);
                sell_btn.setTextColor(Color.WHITE);
                rent_btn.setBackgroundResource(R.drawable.property_type_defualt_btn);
                rent_btn.setTextColor(Color.BLACK);
                startActivity(new Intent(NewPropertyHome.this,SellingTypeActivity.class));

            }
        });



    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//
//        buy_btn.setBackgroundResource(R.drawable.property_type_btn);
//        buy_btn.setTextColor(Color.WHITE);
//        sell_btn.setBackgroundResource(R.drawable.property_type_defualt_btn);
//        sell_btn.setTextColor(Color.BLACK);
//        rent_btn.setBackgroundResource(R.drawable.property_type_defualt_btn);
//        rent_btn.setTextColor(Color.BLACK);
//        list.clear();
//        getArrayData(btnName,icon);
//        homeAdapter.notifyDataSetChanged();
//
//
//    }

    private void  getArrayData(String[] name, int[] icon){




        try {
            for (int i = 0; i < name.length; i++) {

                int geticon = icon[i];
                String namestr = name[i];
             //   String nameEng = btnNameEng[i];
                HomeDataProvider homeDataProvider = new HomeDataProvider(namestr, "", geticon);
                list.add(homeDataProvider);
            }

            homeAdapter = new NewPropertyHomeAdapter(getApplicationContext(), list);
            recyclerView.setAdapter(homeAdapter);
            homeAdapter.setOnItemClickListener(new NewPropertyHomeAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(int position) {

                    switch (position){

                        case  0:

                            Intent intent = new Intent(NewPropertyHome.this, ListOfProperty.class);
                            intent.putExtra("status",status);
                            intent.putExtra("prostatus","houses");
                            startActivity(intent);

                            break;

                        case  1:


//                            if (status.equals("Buy")){
//                                Intent intent1 = new Intent(NewPropertyHome.this, ListOfProperty.class);
//                                intent1.putExtra("status",status);
//                                intent1.putExtra("prostatus","Residential");
//                                startActivity(intent1);
//                            }else{
//                                Intent intent1 = new Intent(NewPropertyHome.this, ShowHostels.class);
//                                intent1.putExtra("status",status);
//                                intent1.putExtra("prostatus","all");
//                                startActivity(intent1);
//                            }


                            break;

                        case  2:

//                            Intent intent2 = new Intent(NewPropertyHome.this, ListOfProperty.class);
//                            intent2.putExtra("status",status);
//                            intent2.putExtra("prostatus","Commertial");
//                            startActivity(intent2);

                            break;

                        case  3:

//                            Intent intent3 = new Intent(NewPropertyHome.this, ListOfProperty.class);
//                            intent3.putExtra("status",status);
//                            intent3.putExtra("prostatus","Agriculture");
//                            startActivity(intent3);

                            break;
                        case  4:

//                            Intent intent4 = new Intent(NewPropertyHome.this, ListOfProperty.class);
//                            intent4.putExtra("status",status);
//                            if (status.equals("Buy")){
//                                intent4.putExtra("prostatus","Shops");
//                            }else{
//                                intent4.putExtra("prostatus","Hotels");
//                            }
//
//                            startActivity(intent4);

                            break;
                        case  5:
//                            Intent intent5 = new Intent(NewPropertyHome.this, ListOfProperty.class);
//                            intent5.putExtra("status",status);
//                            intent5.putExtra("prostatus","Buildings");
//                            startActivity(intent5);

                            break;
                    }

                }

                @Override
                public void onButtonChange(int position) {

                }
            });



        } catch (Exception e) {

            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();

        }


    }
}
