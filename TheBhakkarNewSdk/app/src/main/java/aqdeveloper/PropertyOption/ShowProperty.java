package aqdeveloper.PropertyOption;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import aqdeveloper.PropertyOption.Adapters.HousesAdapter;
import aqdeveloper.PropertyOption.ListItems.HousesListItems;
import aqdeveloper.thebhakkar.R;

public class ShowProperty extends AppCompatActivity {

    private RecyclerView recyclerView;
    public  static SharedPreferences preferences;
    private List<HousesListItems> list;
    private RecyclerView.Adapter adapter;
    private ProgressBar progressBar;
    private String userid;
    private TextView noPropertyYet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_property);
        preferences = getSharedPreferences("EStore", Context.MODE_PRIVATE);
        userid = preferences.getString("userid","");
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        list = new ArrayList<>();
        progressBar = findViewById(R.id.progressbar);
        noPropertyYet = findViewById(R.id.no_property_added_yet);
        if (getIntent().getStringExtra("type").equals("houses")){
            getHousesFromServer();
        }else{
            getPlotsFromServer();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (PropertyDetail.activity!=null){
            if (PropertyDetail.checkStat){
                if (getIntent().getStringExtra("type").equals("houses")){
                    getHousesFromServer();
                }else{
                    getPlotsFromServer();
                }
                PropertyDetail.checkStat=false;
            }
        }
    }

    private void getHousesFromServer(){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.HOUSES_AGAINST_USER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (adapter!=null){
                            list.clear();
                        }
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("houses");
                            if (jsonArray.length()!=0){
                                for (int i =0;i<jsonArray.length();i++){
                                    JSONObject json = jsonArray.getJSONObject(i);
                                    HousesListItems listItems = new HousesListItems(json.getString("id"),
                                            json.getString("title"),json.getString("description"),
                                            json.getString("area"),json.getString("price"),
                                            json.getString("address"),json.getString("rooms"),
                                            json.getString("bathrooms"),json.getString("stories"),
                                            json.getString("image"),json.getString("uploadtime"),
                                            json.getString("views"),json.getString("imagename"),
                                            json.getString("secondnumber"),json.getString("areaunit"),
                                            json.getString("areaname"),json.getString("gas"),
                                            json.getString("userid"));
                                    list.add(listItems);
                                }
                                adapter = new HousesAdapter(list,ShowProperty.this,false);
                                recyclerView.setAdapter(adapter);
                            }else{
                                noPropertyYet.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ShowProperty.this, error.getMessage()+"", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("userid",userid);
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void getPlotsFromServer(){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.SHOW_PLOTS_AGAINST_USER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (adapter!=null){
                            list.clear();
                        }
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("plots");
                            if (jsonArray.length()!=0){
                                for (int i =0;i<jsonArray.length();i++){
                                    JSONObject json = jsonArray.getJSONObject(i);
                                    HousesListItems listItems = new HousesListItems(json.getString("id"),
                                            json.getString("title"),json.getString("description"),
                                            json.getString("area"),json.getString("price"),
                                            json.getString("address"),"null",
                                            "null","null",
                                            json.getString("image"),json.getString("uploadtime"),
                                            json.getString("views"),json.getString("imagename"),
                                            json.getString("secondnumber"),json.getString("areaunit"),
                                            json.getString("areaname"),json.getString("plotstype"),
                                            json.getString("userid"));
                                    // plotstype in place of gas 2nd last.
                                    list.add(listItems);
                                }
                                adapter = new HousesAdapter(list,ShowProperty.this,false);
                                recyclerView.setAdapter(adapter);
                            }else{
                                noPropertyYet.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ShowProperty.this, error.getMessage()+"", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("userid",userid);
                param.put("plotstype",getIntent().getStringExtra("type"));
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
