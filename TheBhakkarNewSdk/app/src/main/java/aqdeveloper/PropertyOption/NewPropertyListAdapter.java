package aqdeveloper.PropertyOption;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import aqdeveloper.thebhakkar.DataProviders.HomeDataProvider;
import aqdeveloper.thebhakkar.R;

public class NewPropertyListAdapter  extends RecyclerView.Adapter<NewPropertyListAdapter.ViewHolder> {

    private Context mcontext;
    private ArrayList<HomeDataProvider> mList;
    private NewPropertyListAdapter.OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);

        void onButtonChange(int position);
    }

    public void setOnItemClickListener(NewPropertyListAdapter.OnItemClickListener listener) {
        mListener = listener;
    }

    public NewPropertyListAdapter(Context context, ArrayList<HomeDataProvider> list) {

        this.mcontext = context;
        this.mList = list;


    }


    @Override
    public NewPropertyListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mcontext);
        View view = inflater.inflate(R.layout.new_property_list_customise, parent, false);
        NewPropertyListAdapter.ViewHolder viewHolder = new NewPropertyListAdapter.ViewHolder(view, mListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NewPropertyListAdapter.ViewHolder holder, int position) {

        final HomeDataProvider homeDataProvider = mList.get(position);

        TextView titleTextview = holder.title_property_textview;
        LinearLayout imageProperty = holder.image_property_linear_layout;
        ImageView propertyImage = holder.property_image;

        titleTextview.setText(homeDataProvider.getBtnName());
        propertyImage.setImageResource(homeDataProvider.getBtnIcon());






    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout image_property_linear_layout;
        TextView title_property_textview;
        ImageView property_image;




        public ViewHolder(View itemView, final NewPropertyListAdapter.OnItemClickListener listener) {
            super(itemView);

            image_property_linear_layout = itemView.findViewById(R.id.image_linear_layout);
            title_property_textview = itemView.findViewById(R.id.title_property_textview);
            property_image = itemView.findViewById(R.id.property_image);



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listener != null) {

                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }

                }
            });
        }
    }
}