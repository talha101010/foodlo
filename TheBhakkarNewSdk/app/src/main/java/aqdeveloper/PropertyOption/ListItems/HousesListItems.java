package aqdeveloper.PropertyOption.ListItems;

public class HousesListItems {
    private String id,title,description,area,price,address,rooms,bathrooms,stories,image,uploadtime,views,imagename,secondnumber,areaunit,areaname,gas,userid;

    public HousesListItems(String id, String title, String description, String area, String price, String address, String rooms, String bathrooms, String stories, String image, String uploadtime, String views, String imagename, String secondnumber, String areaunit, String areaname, String gas, String userid) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.area = area;
        this.price = price;
        this.address = address;
        this.rooms = rooms;
        this.bathrooms = bathrooms;
        this.stories = stories;
        this.image = image;
        this.uploadtime = uploadtime;
        this.views = views;
        this.imagename = imagename;
        this.secondnumber = secondnumber;
        this.areaunit = areaunit;
        this.areaname = areaname;
        this.gas = gas;
        this.userid = userid;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getArea() {
        return area;
    }

    public String getPrice() {
        return price;
    }

    public String getAddress() {
        return address;
    }

    public String getRooms() {
        return rooms;
    }

    public String getBathrooms() {
        return bathrooms;
    }

    public String getStories() {
        return stories;
    }

    public String getImage() {
        return image;
    }

    public String getUploadtime() {
        return uploadtime;
    }

    public String getViews() {
        return views;
    }

    public String getImagename() {
        return imagename;
    }

    public String getSecondnumber() {
        return secondnumber;
    }

    public String getAreaunit() {
        return areaunit;
    }

    public String getAreaname() {
        return areaname;
    }

    public String getGas() {
        return gas;
    }

    public String getUserid() {
        return userid;
    }
}
