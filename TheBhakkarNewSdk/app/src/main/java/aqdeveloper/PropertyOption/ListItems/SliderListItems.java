package aqdeveloper.PropertyOption.ListItems;

public class SliderListItems {
    private String name,imageurl;

    public SliderListItems(String name, String imageurl) {
        this.name = name;
        this.imageurl = imageurl;
    }

    public String getName() {
        return name;
    }

    public String getImageurl() {
        return imageurl;
    }
}
