package aqdeveloper.PropertyOption.ListItems;

public class HostelsListItems {
    private String id,name,description,rent,rentCategory,rentinclude,rooms,address,nearby,
    features,secondnumber,userid,uploadtime,views,imagename,image;

    public HostelsListItems(String id, String name, String description, String rent, String rentCategory, String rentinclude, String rooms, String address, String nearby, String features, String secondnumber, String userid, String uploadtime, String views, String imagename, String image) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.rent = rent;
        this.rentCategory = rentCategory;
        this.rentinclude = rentinclude;
        this.rooms = rooms;
        this.address = address;
        this.nearby = nearby;
        this.features = features;
        this.secondnumber = secondnumber;
        this.userid = userid;
        this.uploadtime = uploadtime;
        this.views = views;
        this.imagename = imagename;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getRent() {
        return rent;
    }

    public String getRentCategory() {
        return rentCategory;
    }

    public String getRentinclude() {
        return rentinclude;
    }

    public String getRooms() {
        return rooms;
    }

    public String getAddress() {
        return address;
    }

    public String getNearby() {
        return nearby;
    }

    public String getFeatures() {
        return features;
    }

    public String getSecondnumber() {
        return secondnumber;
    }

    public String getUserid() {
        return userid;
    }

    public String getUploadtime() {
        return uploadtime;
    }

    public String getViews() {
        return views;
    }

    public String getImagename() {
        return imagename;
    }

    public String getImage() {
        return image;
    }
}
