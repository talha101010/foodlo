package aqdeveloper.thebhakkar;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import aqdeveloper.thebhakkar.Activities.BMSplash;
import aqdeveloper.thebhakkar.Activities.BhakkarBazarActivity;
import aqdeveloper.thebhakkar.Activities.Doctors;
import aqdeveloper.thebhakkar.Activities.NumbersList;
import aqdeveloper.thebhakkar.Activities.PrayerTimesActivity;
import aqdeveloper.thebhakkar.Activities.Team;

import aqdeveloper.thebhakkar.Adapters.HomeAdapter;
import aqdeveloper.thebhakkar.Adapters.OfflinePagerAdapter;
import aqdeveloper.thebhakkar.DataProviders.HomeDataProvider;
import aqdeveloper.thebhakkar.DataProviders.ImageModel;
import aqdeveloper.thebhakkar.DbHelpers.SqlHelper;
import aqdeveloper.thebhakkar.DrawerActivities.LoginPanel;
import aqdeveloper.thebhakkar.DrawerActivities.SignupActivity;
import aqdeveloper.thebhakkar.FastFood.FFSplash;
import aqdeveloper.thebhakkar.FastFood.FastFoodShops;
import aqdeveloper.thebhakkar.FastFood.MyProgressDialog;
import aqdeveloper.thebhakkar.Http.AsyncTaskListener;
import aqdeveloper.thebhakkar.Http.HttpAsyncRequest;
import aqdeveloper.thebhakkar.Http.TaskResult;
import aqdeveloper.thebhakkar.Internet.NetworkUtil;
import aqdeveloper.thebhakkar.Parser.BaseParser;
import aqdeveloper.thebhakkar.ServiceMan.ServiceManActivity;
import aqdeveloper.thebhakkar.SuperStore.CategoryActivity;
import aqdeveloper.thebhakkar.SuperStore.OrderHistory;
import aqdeveloper.thebhakkar.SuperStore.SplashScreen;
import aqdeveloper.thebhakkar.Url.Constant;
import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

public class Home extends AppCompatActivity implements BaseParser {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_layout);

        if (isNetworkAvailable()) {
            getversionAndStoreStatus();
        } else {
           startActivity(new Intent(Home.this, NewHome.class));
           finish();
        }

    }

        private void getversionAndStoreStatus () {
            HttpAsyncRequest requests = new HttpAsyncRequest(this, Constant.getstoreinfo, HttpAsyncRequest.RequestType.POST, this, listenerforstore);


            requests.execute();

        }

        private boolean isNetworkAvailable () {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        public void show_Dialog () {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);


            dialogBuilder.setTitle("Warning");
            dialogBuilder.setMessage("Your device has no internet connection. Please connect to internet to continue");

            dialogBuilder.setPositiveButton("Enable", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                }
            });
            dialogBuilder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    finish();

                }
            });
            AlertDialog b = dialogBuilder.create();
            b.show();
        }

        AsyncTaskListener listenerforstore = new AsyncTaskListener() {
            @Override
            public void onComplete(TaskResult result) {

                if (result.isSuccess()) {


                } else {
                    try {
                        //  Toast.makeText(SplashScreen.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();
                        AlertDialog.Builder builder;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            builder = new AlertDialog.Builder(Home.this, android.R.style.Theme_Material_Dialog_Alert);
                        } else {
                            builder = new AlertDialog.Builder(Home.this);
                        }
                        builder.setTitle("Warning")
                                .setMessage("Error Communicating with the Server")
                                .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                        getversionAndStoreStatus();

                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    } catch (Exception e) {
                    }


                }
            }


        };

        @Override

        public TaskResult parse ( int httpCode, String response){
            TaskResult result = new TaskResult();
            Log.d("Response", response);
            if (httpCode == SUCCESS) {
                result.success(true);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (Integer.parseInt(obj.optString("version_number")) == Constant.currentversion) {
                        Intent intent = new Intent(Home.this, NewHome.class);
                        startActivity(intent);
                        finish();

                    } else {

                        final  AlertDialog.Builder alertdialog = new AlertDialog.Builder(Home.this);
                        LayoutInflater inflater = getLayoutInflater();
                        final  View dialogview = inflater.inflate(R.layout.version_alert_dialog,null);
                        alertdialog.setView(dialogview);
                        final Button laterupdatebtn = dialogview.findViewById(R.id.lateupdatebtn);
                        final Button nowupdatebtn = dialogview.findViewById(R.id.nowupdatebtn);
                        alertdialog.setTitle("New Version Update");
                        final AlertDialog dialog = alertdialog.create();
                        dialog.show();

                        laterupdatebtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Intent intent = new Intent(Home.this, NewHome.class);
                                startActivity(intent);
                                finish();

                            }
                        });

                        nowupdatebtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                        try {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                        } catch (ActivityNotFoundException anfe) {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                        }
                            }
                        });
//                        AlertDialog.Builder builder;
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                            builder = new AlertDialog.Builder(Home.this, android.R.style.Theme_Material_Dialog_Alert);
//                        } else {
//                            builder = new AlertDialog.Builder(Home.this);
//                        }
//                        builder.setTitle("Warning")
//                                .setMessage("New Version of App is available\nPlease Update App to Continue\nClick Update Below")
//                                .setPositiveButton("Update", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        // continue with delete
//                                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
//                                        try {
//                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
//                                        } catch (ActivityNotFoundException anfe) {
//                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
//                                        }
//                                    }
//                                })
//                                .setIcon(android.R.drawable.ic_dialog_alert)
//                                .show();
                    }


                    result.success(true);
                } catch (JSONException e) {
                    e.printStackTrace();
                    result.success(false);
                }


            }
            return result;
        }


    }









