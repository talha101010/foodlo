package aqdeveloper.thebhakkar.DrawerActivities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import aqdeveloper.thebhakkar.FastFood.BillModel;
import aqdeveloper.thebhakkar.FastFood.FoodItemHolder;
import aqdeveloper.thebhakkar.FastFood.ShopMenu;
import aqdeveloper.thebhakkar.FastFood.ShopSimilarAdaptor;
import aqdeveloper.thebhakkar.R;

public class ShopDetailActivity extends AppCompatActivity {

    TextView quantity, name, unit, price,topname,nomsg;
    Button addtocart, plus, minus;
    ImageView foodimage;
    ProgressBar progressBar;
    String nametxt, unittxt, pricetxt, pathtxt, id;
    RecyclerView foodrecyler;
    public static ShopDetailActivity activity;
    RelativeLayout imagelayput;



    ArrayList<FoodItemHolder> foodItemlist,similarlist ;
    public static SharedPreferences appSharedPrefs;
    private int index;
    private String storename;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_detail);

        init();
        activity = this;
        imagelayput=(RelativeLayout)findViewById(R.id.imagelayout);







        Intent intent = getIntent();


        if (intent.hasExtra("name")) {
            nametxt = intent.getStringExtra("name");
            pricetxt = intent.getStringExtra("price");
            unittxt = intent.getStringExtra("unit");
            pathtxt = intent.getStringExtra("path");
            id = intent.getStringExtra("id");
            storename = intent.getStringExtra("store");
            imagelayput.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  //  startActivity(new Intent(ShopDetailActivity.this,ImageActivity.class).putExtra("image",pathtxt));

                }
            });

        }
        if(verifyOrder(id,quantity))
        {
            addtocart.setVisibility(View.GONE);
            plus.setVisibility(View.VISIBLE);
            minus.setVisibility(View.VISIBLE);
            quantity.setVisibility(View.VISIBLE);



        }
        else{
            addtocart.setVisibility(View.VISIBLE);
            plus.setVisibility(View.GONE);
            minus.setVisibility(View.GONE);
            quantity.setVisibility(View.GONE);

        }

        addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddtoNormalCart();
                minus.setVisibility(View.VISIBLE);
                plus.setVisibility(View.VISIBLE);
                quantity.setVisibility(View.VISIBLE);
                addtocart.setVisibility(View.GONE);
                quantity.setText("1");
                HandleCartNumber();
            }
        });

        name.setText(nametxt);
        topname.setText(nametxt);
        price.setText("Rs. " + pricetxt);
        unit.setText(unittxt);
        progressBar.setVisibility(View.VISIBLE);
        Glide.with(this)
                .load(pathtxt)
                .dontAnimate()

                .placeholder(R.drawable.logoone)

                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(foodimage);


        if(ShopMenu.activity != null)
            foodItemlist = ShopMenu.foodlist;

        if(ShopSearchActivity.activity != null)
            foodItemlist = ShopSearchActivity.foodlist;



        similarlist = new ArrayList<>();
        for(int i  = 0; i<foodItemlist.size();i++)
        {
            if(!foodItemlist.get(i).getDealid().equals(id))
            {
                similarlist.add(foodItemlist.get(i));
            }
        }

        if(similarlist!=null && similarlist.size() !=0) {
            foodrecyler.setAdapter(new ShopSimilarAdaptor(this, similarlist));

            foodrecyler.setVisibility(View.VISIBLE);
        }
        else
        {
            nomsg.setVisibility(View.VISIBLE);
            foodrecyler.setVisibility(View.GONE);
        }
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int q = Integer.parseInt(quantity.getText().toString());
                q += 1;
                quantity.setText(q + "");
                updateCart(q + "");


            }
        });


        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int q = Integer.parseInt(quantity.getText().toString());
                if (q != 1) {
                    q -= 1;
                    quantity.setText(q + "");
                    updateCart(q + "");

                } else {
                    if (RemoveOrder(id)) {
                        ShopMenu.HandleCartNumber();
                        minus.setVisibility(View.GONE);
                        plus.setVisibility(View.GONE);
                        quantity.setVisibility(View.GONE);
                        addtocart.setVisibility(View.VISIBLE);

                        HandleCartNumber();


                    }

                }
            }
        });





    }

    private void init() {
        quantity = (TextView) findViewById(R.id.quantity);
        topname = (TextView) findViewById(R.id.topname);
        name = (TextView) findViewById(R.id.name);
        nomsg = (TextView) findViewById(R.id.nomsg);
        unit = (TextView) findViewById(R.id.units);
        price = (TextView) findViewById(R.id.price);
        addtocart = (Button) findViewById(R.id.addtocart);
        plus = (Button) findViewById(R.id.plus);
        minus = (Button) findViewById(R.id.minus);
        foodimage = (ImageView) findViewById(R.id.image);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        foodrecyler = (RecyclerView)findViewById(R.id.food_recyler);
        appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(this);



    }


    private boolean AddtoNormalCart() {
        ArrayList<BillModel> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        String json = appSharedPrefs.getString("shopcart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            arrayList = new ArrayList<>();


        BillModel billHolder = new BillModel();
        billHolder.setExsist(true);
        billHolder.setFooditem_id(id);
        billHolder.setBillprice(pricetxt);
        billHolder.setQuantity("1");
        billHolder.setProductprice(pricetxt);
        billHolder.setImagepath(pathtxt);
        billHolder.setStorename(storename);
        billHolder.setUnit(unittxt);


        billHolder.setName(nametxt);
        arrayList.add(billHolder);


        // savong cart to share preferences


        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("shopcart", jsons);
        prefsEditor.commit();
        return true;
        // Toast.makeText(context, "item added", Toast.LENGTH_SHORT).show();

    }


    public boolean verifyOrder(String id, TextView quantity) {
        ArrayList<BillModel> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        String json = appSharedPrefs.getString("shopcart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            return false;


        for (int i = 0; i < arrayList.size(); i++) {


            if (arrayList.get(i).getFooditem_id().equals(id)) {
                index = i;
                quantity.setText(arrayList.get(index).getQuantity());
                return true;

            }


        }

        return false;
    }

    public boolean RemoveOrder(String id) {
        ArrayList<BillModel> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        String json = appSharedPrefs.getString("shopcart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            return false;


        for (int i = 0; i < arrayList.size(); i++) {


            if (arrayList.get(i).getFooditem_id().equals(id)) {
                index = i;
                arrayList.remove(index);
                break;
            }


        }

        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("shopcart", jsons);
        prefsEditor.commit();
        return true;


    }
    public void updateCart(String quantity) {
        ArrayList<BillModel> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        String json = appSharedPrefs.getString("shopcart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            arrayList = new ArrayList<>();

        for (int i = 0; i < arrayList.size(); i++) {


            if (arrayList.get(i).getFooditem_id().equals(id)) {
                index = i;
                BillModel bill = arrayList.get(i);
                bill.setQuantity(quantity);
                break;


            }


        }


        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("shopcart", jsons);
        prefsEditor.commit();
    }
    @Override
    protected void onResume() {
        super.onResume();
        HandleCartNumber();
    }

    public static  void HandleCartNumber()
    {

        ArrayList<BillModel> arrayList = null;

        appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(activity);
        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        TextView textView  = (TextView) activity.findViewById(R.id.itemnumber);
        LinearLayout cartlayout = (LinearLayout)activity.findViewById(R.id.cartlayout);
        LinearLayout accountlayout = (LinearLayout)activity.findViewById(R.id.accountlayout);
        accountlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity. startActivity(new Intent(activity,SignupActivity.class).putExtra("update",1));
            }
        });

        LinearLayout searchlay= (LinearLayout)activity.findViewById(R.id.searchlayout) ;
        searchlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivity(new Intent(activity,ShopSearchActivity.class));
            }
        });

        String json = appSharedPrefs.getString("shopcart", "");
        if (!json.equals("")) {
            arrayList = gson.fromJson(json, type);

            textView.setText(arrayList.size()+"");
            textView.setVisibility(View.VISIBLE);
            cartlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity. startActivity(new Intent(activity,ShopCartActivity.class));
                }
            });
        }
        else{
            textView.setVisibility(View.GONE);
            cartlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(activity, "Nothing in the Cart Yet", Toast.LENGTH_SHORT).show();
                }
            });
        }


        LinearLayout homelayout = (LinearLayout)activity.findViewById(R.id.homelayout);

        homelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity. finish();
                if(SubCategoryActivity.activity!=null)
                    SubCategoryActivity.activity.finish();

                if(ShopMenu.activity!=null)
                    ShopMenu.activity.finish();
                if(ShopDetailActivity.activity!=null)
                    ShopDetailActivity.activity.finish();


                if(DealsActivity.activity!=null)
                    DealsActivity.activity.finish();

            }
        });

    }
}
