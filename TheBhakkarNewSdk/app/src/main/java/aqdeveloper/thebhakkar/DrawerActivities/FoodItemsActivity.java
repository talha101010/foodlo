package aqdeveloper.thebhakkar.DrawerActivities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import aqdeveloper.thebhakkar.FastFood.BillModel;
import aqdeveloper.thebhakkar.FastFood.FoodItemHolder;
import aqdeveloper.thebhakkar.FastFood.MyProgressDialog;
import aqdeveloper.thebhakkar.Http.AsyncTaskListener;
import aqdeveloper.thebhakkar.Http.HttpAsyncRequest;
import aqdeveloper.thebhakkar.Http.TaskResult;
import aqdeveloper.thebhakkar.Parser.FoodItemParser;
import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.SuperStore.CartActivity;
import aqdeveloper.thebhakkar.SuperStore.MenuAdapter;
import aqdeveloper.thebhakkar.SuperStore.FooitemAdapter;
import aqdeveloper.thebhakkar.SuperStore.SearchActivity;
import aqdeveloper.thebhakkar.SuperStore.SubCategoryAdapter;
import aqdeveloper.thebhakkar.Url.Constant;

public class FoodItemsActivity extends AppCompatActivity {

    public static ListView fooditemlistview;
    public static SharedPreferences preferences;
    public static TextView categoryname;

    public ArrayList<FoodItemHolder> fooditems;
    RecyclerView subcategoryrecyclerview;
    public static MyProgressDialog myProgressDialog;
    public static String categoryid, sub_category_id;
    MenuAdapter menuAdapter;
    public static Activity activity;
    public static ProgressBar progressBar;
    public static TextView nomsg;
    public static LinearLayout cartlayout;
    public static TextView textView;
    public static FooitemAdapter fooditemadapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_items);

        init();
        activity = this;


        Intent intent = getIntent();

        if (intent.hasExtra("categoryname")) {
            categoryname.setText(intent.getStringExtra("categoryname"));

        }

        if (getIntent().hasExtra("categoryid")) {
            categoryid = getIntent().getStringExtra("categoryid");
        }
        if (getIntent().hasExtra("subcategoryid")) {
            sub_category_id = getIntent().getStringExtra("subcategoryid");
        }

        menuAdapter = new MenuAdapter(FoodItemsActivity.this, SubCategoryAdapter.fooditems);

        subcategoryrecyclerview.setAdapter(menuAdapter);
        // fooditemlistview.setAdapter(new FooitemAdapter(this, CategoryActivity.fooditems));
        //  getTotalCartCounter();
        //send_sub_Categoryid_to_Server();
        getFoodItems(SubCategoryAdapter.fooditems.get(0).getDealid());


    }

    public static void HandleCartNumber() {

        ArrayList<BillModel> arrayList = null;

        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(activity);
        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        LinearLayout accountlayout = (LinearLayout) activity.findViewById(R.id.accountlayout);
        accountlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!preferences.getString("userid", "").equals(""))
                    activity.startActivity(new Intent(activity, SignupActivity.class).putExtra("update", 1));
                else
                    activity.startActivity(new Intent(activity, LoginPanel.class));
            }
        });


        LinearLayout searchlay = (LinearLayout) activity.findViewById(R.id.searchlayout);
        searchlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivity(new Intent(activity, SearchActivity.class));
            }
        });
        String json = appSharedPrefs.getString("cart", "");
        if (!json.equals("")) {
            arrayList = gson.fromJson(json, type);

            textView.setText(arrayList.size() + "");
            textView.setVisibility(View.VISIBLE);
            cartlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.startActivity(new Intent(activity, CartActivity.class));
                }
            });
        } else {
            textView.setVisibility(View.GONE);
            cartlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(activity, "Nothing in the Cart Yet", Toast.LENGTH_SHORT).show();
                }
            });
        }

        LinearLayout homelayout = (LinearLayout) activity.findViewById(R.id.homelayout);

        homelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
                if (SubCategoryActivity.activity != null)
                    SubCategoryActivity.activity.finish();
            }
        });


    }


    public static void getFoodItems(String id) {
        HttpAsyncRequest request = new HttpAsyncRequest(activity, Constant.get_items_url, HttpAsyncRequest.RequestType.POST, new FoodItemParser(), listenerr);
        request.addParam("maincategory", categoryid);
        request.addParam("subcategory", sub_category_id);
        request.addParam("menu", id);

        request.execute();
        progressBar.setVisibility(View.VISIBLE);
        fooditemlistview.setVisibility(View.GONE);
        nomsg.setVisibility(View.GONE);


    }


    public static AsyncTaskListener listenerr = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            if (result.isSuccess()) {

                if (result.getMessage().equals("true")) {
                    progressBar.setVisibility(View.GONE);
                    fooditemadapter = new FooitemAdapter(activity, (ArrayList<FoodItemHolder>) result.getData());
                    fooditemlistview.setAdapter(fooditemadapter);
                    fooditemlistview.setVisibility(View.VISIBLE);

                } else if (result.getMessage().equals("false")) {
                    progressBar.setVisibility(View.GONE);
                    nomsg.setVisibility(View.VISIBLE);
                    // Toast.makeText(activity, "This Menu is not active yet", Toast.LENGTH_SHORT).show();
                }


            } else {

                Toast.makeText(activity, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();

            }
        }


    };

    private boolean verify() {

        if (!preferences.getString("userid", "0").equals("0")) {
            return true;
        }
        return false;
    }

    private void init() {
        progressBar = (ProgressBar) findViewById(R.id.progress);
        // gotocart = (ImageView) findViewById(R.id.cartlayout);
        fooditemlistview = (ListView) findViewById(R.id.fooditems_listview_items);
        subcategoryrecyclerview = (RecyclerView) findViewById(R.id.category_recycler);
        categoryname = (TextView) findViewById(R.id.categoryname);
        findViewById(R.id.goback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        preferences = this.getSharedPreferences("EStore", Context.MODE_PRIVATE);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        nomsg = (TextView) findViewById(R.id.nomsg);

        textView = (TextView) findViewById(R.id.itemnumber);
        cartlayout = (LinearLayout) findViewById(R.id.cartlayout);
    }


    @Override
    protected void onResume() {
        super.onResume();
        HandleCartNumber();
        if (fooditemadapter != null)
            fooditemadapter.notifyDataSetChanged();
    }

}