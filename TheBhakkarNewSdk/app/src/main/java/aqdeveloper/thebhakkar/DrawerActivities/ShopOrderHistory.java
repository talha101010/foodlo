package aqdeveloper.thebhakkar.DrawerActivities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import aqdeveloper.thebhakkar.FastFood.MyProgressDialog;
import aqdeveloper.thebhakkar.FastFood.ShopOrderHistoryAdapter;
import aqdeveloper.thebhakkar.Helper.HistoryModel;
import aqdeveloper.thebhakkar.Http.AsyncTaskListener;
import aqdeveloper.thebhakkar.Http.HttpAsyncRequest;
import aqdeveloper.thebhakkar.Http.TaskResult;
import aqdeveloper.thebhakkar.Parser.BaseParser;
import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.Url.ConstantsforShop;

public class ShopOrderHistory extends AppCompatActivity  implements BaseParser {

    private SharedPreferences preferences;
    ListView list;
    TextView nomsg;
    private MyProgressDialog myProgressDialog;
    ShopOrderHistoryAdapter orderadapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_order_history);

        init();

        get_Order_History();




    }

    private void init() {
        preferences = getSharedPreferences("EStore", Context.MODE_PRIVATE);
        list = (ListView)findViewById(R.id.list);
        nomsg = (TextView)findViewById(R.id.nomsg);

    }

    private void get_Order_History() {

        HttpAsyncRequest request = new HttpAsyncRequest(this, ConstantsforShop.GET_PREVIOUS_HISTORY_URl, HttpAsyncRequest.RequestType.POST, this, listener);
        request.addParam("userid", preferences.getString("userid", "0"));
        request.execute();
        myProgressDialog = new MyProgressDialog(ShopOrderHistory.this);
        myProgressDialog.show();
        myProgressDialog.setCanceledOnTouchOutside(false);

    }

    AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            try{
                myProgressDialog.dismiss();

            }catch (Exception e){}

            if (result.isSuccess()) {



            } else {

                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(ShopOrderHistory.this, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(ShopOrderHistory.this);
                }
                builder.setTitle("Warning")
                        .setMessage("Error Communicating with the Server")
                        .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                get_Order_History();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }
        }


    };

    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {

            try {
                result.success(true);
                JSONObject obj = new JSONObject(response);
                if (obj.optString("error").equals("0")) {
                    result.setMessage("true");

                    JSONArray jsonArray = obj.optJSONArray("order_tracking");
                    ArrayList<HistoryModel> arrayList = null;
                    if(jsonArray!=null && jsonArray.length()!=0)
                    {
                        arrayList = new ArrayList<>();

                        list.setVisibility(View.VISIBLE);

                        for(int i = 0 ; i <jsonArray.length();i++)
                        {

                            JSONObject object = jsonArray.optJSONObject(i);

                            HistoryModel model = new HistoryModel();
                            model.setBill(object.optString("bill"));
                            model.setId(object.optString("order_id"));
                            model.setStatus(object.optString("status"));
                            model.setDate(object.optString("order_date"));

                            arrayList.add(model);
                        }
                        orderadapter=   new ShopOrderHistoryAdapter(ShopOrderHistory.this,arrayList);
                        list.setAdapter(orderadapter);


                    }
                    else{
                        list.setVisibility(View.GONE);
                        nomsg.setVisibility(View.VISIBLE);
                    }




                } else  {
                    Toast.makeText(this, "Some Error Occured", Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);

            }


        }
        return result;
    }
}
