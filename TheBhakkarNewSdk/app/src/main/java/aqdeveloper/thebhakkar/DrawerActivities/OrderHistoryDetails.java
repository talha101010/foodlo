package aqdeveloper.thebhakkar.DrawerActivities;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import aqdeveloper.thebhakkar.Adapters.OrderHistoryDetailAdapter;
import aqdeveloper.thebhakkar.DataProviders.OrderHistoryListItems;
import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.Url.Constant;

public class OrderHistoryDetails extends AppCompatActivity {
    private String orderid;
    private RecyclerView recyclerView;
    private List<OrderHistoryListItems> list;
    private RecyclerView.Adapter adapter;
    private TextView total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history_details);

        orderid = getIntent().getStringExtra("orderid");
        recyclerView = findViewById(R.id.detailrecyclerveiw);
        list = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        total = findViewById(R.id.proceedbtn);
        getDataFromServer();
    }

    public void getDataFromServer(){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait......");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.gethistorydetail,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("detail");
                            for (int i=0; i<jsonArray.length() ;i++){
                                JSONObject json = jsonArray.getJSONObject(i);
                                OrderHistoryListItems listItems = new OrderHistoryListItems(json.getString("url"),
                                        json.getString("itemname"),json.getString("itemquantity"),json.getString("itemunit"),
                                        json.getString("itemprice"));
                                list.add(listItems);

                            }
                            adapter = new OrderHistoryDetailAdapter(list,OrderHistoryDetails.this);
                            recyclerView.setAdapter(adapter);
                            total.setText("Totall Bill = "+jsonObject.getString("totall") +"\nTotal Items ="+jsonArray.length());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(OrderHistoryDetails.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("orderid",orderid);
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
