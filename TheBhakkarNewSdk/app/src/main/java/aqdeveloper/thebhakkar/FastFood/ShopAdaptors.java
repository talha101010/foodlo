package aqdeveloper.thebhakkar.FastFood;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import aqdeveloper.thebhakkar.Http.AsyncTaskListener;
import aqdeveloper.thebhakkar.Http.HttpAsyncRequest;
import aqdeveloper.thebhakkar.Http.TaskResult;
import aqdeveloper.thebhakkar.Parser.BaseParser;
import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.Url.ConstantsforShop;

public class ShopAdaptors extends BaseAdapter implements BaseParser {

    private static Context context;
    private static LayoutInflater inflater;
    private final SharedPreferences preferences;
    ArrayList<ShopModel> shoplist;
    public  static  ArrayList<MenuFoodItems> menulist;
    MyProgressDialog myProgressDialog;
    String selectedname;
    String min_shopping;
    private String charges;
    private SharedPreferences appSharedPrefs;

    public ShopAdaptors(Context mainActivity, ArrayList<ShopModel> listitems) {
        // TODO Auto-generated constructor stub
        context = mainActivity;
        this.shoplist = listitems;
        preferences = context.getSharedPreferences("EStore", Context.MODE_PRIVATE);
        appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);

        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return shoplist.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }


    public class Holder {

        TextView shopnameeng,shopnameurdu,address,time;
        ImageView shopimage;
        RelativeLayout layout;


        public Holder(View v) {

            shopimage = (ImageView)v.findViewById(R.id.shop_image);
            shopnameeng = (TextView)v.findViewById(R.id.shopnameng);
            shopnameurdu = (TextView)v.findViewById(R.id.shopnameurdu);
            address = (TextView)v.findViewById(R.id.shopaddress);
            time = (TextView)v.findViewById(R.id.shoptime);
            layout = (RelativeLayout)v.findViewById(R.id.row);




        }
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final ShopAdaptors.Holder holder;

        View rowView;
        rowView = inflater.inflate(R.layout.row_shops, null);
        holder = new ShopAdaptors.Holder(rowView);
        holder.shopnameeng.setText(shoplist.get(position).getShop_name_in_english());

        holder.shopnameurdu.setText(shoplist.get(position).getShop_name_in_urdu());



        if(shoplist.get(position).getStatus().toLowerCase().equals("close"))
            holder.time.setText(shoplist.get(position).getStatus()+"d") ;


        holder.address.setText(shoplist.get(position).getShop_address());





        Glide.with(context)
                .load(shoplist.get(position).getShop_image())
                .dontAnimate()
                .placeholder(R.drawable.logoone)


                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

                        return false;
                    }
                })
                .into(holder.shopimage);




        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!shoplist.get(position).getStatus().toLowerCase().equals("close")) {

                    if (!preferences.getString("shop", "").equals("")) {
                        if (preferences.getString("shop", "").equals(shoplist.get(position).getShop_name_in_english())) {
                            selectedname = shoplist.get(position).getShop_name_in_english();
                            min_shopping = shoplist.get(position).getMin_shopping();
                            charges = shoplist.get(position).getDelivery_charges();
                            get_Shops();
                        } else {


                            if(appSharedPrefs.getString("shopcart","").equals(""))
                            {
                                selectedname = shoplist.get(position).getShop_name_in_english();
                                min_shopping = shoplist.get(position).getMin_shopping();
                                charges = shoplist.get(position).getDelivery_charges();
                                get_Shops();
                            }
                            else {


                                AlertDialog.Builder builder;
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
                                } else {
                                    builder = new AlertDialog.Builder(context);
                                }
                                builder.setTitle("Warning")
                                        .setMessage("As you are going to new shop your previous cart will be cleared")
                                        .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // continue with delete

                                                empty();
                                                selectedname = shoplist.get(position).getShop_name_in_english();
                                                min_shopping = shoplist.get(position).getMin_shopping();
                                                charges = shoplist.get(position).getDelivery_charges();
                                                get_Shops();
                                            }
                                        })

                                        .setNegativeButton("Cancel", null)
                                        .setIcon(android.R.drawable.ic_dialog_alert)

                                        .show();
                            }


                        }
                    }
                    else{
                        selectedname = shoplist.get(position).getShop_name_in_english();
                        min_shopping = shoplist.get(position).getMin_shopping();
                        charges = shoplist.get(position).getDelivery_charges();
                        get_Shops();
                    }
                }
                else{
                    Toast.makeText(context, "Shop Close Now", Toast.LENGTH_SHORT).show();
                }
            }
        });




        return rowView;
    }
    private boolean empty() {
        ArrayList<BillModel> arrayList = null;

        Gson gson = new Gson();




        arrayList = new ArrayList<>();


        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("shopcart", jsons);
        prefsEditor.remove("shopcart");
        prefsEditor.commit();
        return true;
        // Toast.makeText(context, "item added", Toast.LENGTH_SHORT).show();


    }

    private void get_Shops() {
        HttpAsyncRequest requests = new HttpAsyncRequest(context, ConstantsforShop.getShopMenu, HttpAsyncRequest.RequestType.POST, this, listenerforcategories);

        requests.addParam("shop_name",selectedname);

        requests.execute();
        myProgressDialog = new MyProgressDialog(context);
        myProgressDialog.show();
        myProgressDialog.setCanceledOnTouchOutside(false);
    }
    AsyncTaskListener listenerforcategories = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            try{

                myProgressDialog.dismiss();
            }catch ( Exception e){}

            if (result.isSuccess()) {





            } else {

                //  Toast.makeText(SplashScreen.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(context);
                }
                builder.setTitle("Warning")
                        .setMessage("Error Communicating with the Server")
                        .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                get_Shops();

                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();



            }
        }


    };
    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {

            try {
                result.success(true);
                JSONObject obj = new JSONObject(response);
                if (obj.optString("error").equals("0")) {
                    result.setMessage("true");

                    JSONArray array = obj.optJSONArray("shops_menu");
                    menulist = new ArrayList<>(array.length());

                    if (array.length() != 0 && array!=null) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject items = array.getJSONObject(i);
                            MenuFoodItems food = new MenuFoodItems();
                            food.setDealid(items.optString("id"));
                            food.setProductname(items.optString("menu_name"));

                            menulist.add(food);
                        }

                        context.startActivity(new Intent(context, ShopMenu.class)
                                .putExtra("shopname",selectedname)
                                .putExtra("min",min_shopping)
                                .putExtra("charges",charges)


                        );
                        //result.setData(list);
                    }
                    else{
                        result.setMessage("false");
                        Toast.makeText(context, "This Shop has No Menu yet", Toast.LENGTH_SHORT).show();

                    }

                } else if (obj.optString("error").equals("true")) {
                    result.setMessage("false");
                    Toast.makeText(context, "This Shop has No Menu yet", Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);

            }


        }
        return result;
    }
}
