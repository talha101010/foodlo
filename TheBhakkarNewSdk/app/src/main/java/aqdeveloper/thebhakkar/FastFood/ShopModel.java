package aqdeveloper.thebhakkar.FastFood;

public class ShopModel {

    public String  id,
            shop_name_in_english,
            shop_name_in_urdu,
            shop_address,
            shop_image,
            delivery_charges,
            status,
            min_shopping,
            timestamp;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShop_name_in_english() {
        return shop_name_in_english;
    }

    public void setShop_name_in_english(String shop_name_in_english) {
        this.shop_name_in_english = shop_name_in_english;
    }

    public String getShop_name_in_urdu() {
        return shop_name_in_urdu;
    }

    public void setShop_name_in_urdu(String shop_name_in_urdu) {
        this.shop_name_in_urdu = shop_name_in_urdu;
    }

    public String getShop_address() {
        return shop_address;
    }

    public void setShop_address(String shop_address) {
        this.shop_address = shop_address;
    }

    public String getShop_image() {
        return shop_image;
    }

    public void setShop_image(String shop_image) {
        this.shop_image = shop_image;
    }

    public String getDelivery_charges() {
        return delivery_charges;
    }

    public void setDelivery_charges(String delivery_charges) {
        this.delivery_charges = delivery_charges;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMin_shopping() {
        return min_shopping;
    }

    public void setMin_shopping(String min_shopping) {
        this.min_shopping = min_shopping;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
