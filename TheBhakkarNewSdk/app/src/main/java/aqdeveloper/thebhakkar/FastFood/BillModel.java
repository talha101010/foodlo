package aqdeveloper.thebhakkar.FastFood;

public class BillModel {
    String storename,unit,purchaseprice;

    public String getPurchaseprice() {
        return purchaseprice;
    }

    public void setPurchaseprice(String purchaseprice) {
        this.purchaseprice = purchaseprice;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getStorename() {
        return storename;
    }

    public void setStorename(String storename) {
        this.storename = storename;
    }

    boolean exsist,isdeal;

    public boolean isdeal() {
        return isdeal;
    }

    public void setIsdeal(boolean isdeal) {
        this.isdeal = isdeal;
    }

    public boolean isOptionexsist() {
        return optionexsist;
    }

    public void setOptionexsist(boolean optionexsist) {
        this.optionexsist = optionexsist;
    }

    boolean optionexsist;
    String fooditem_id;
    String billprice;
    String branch_id;
    String name;
    String quantity;
    String mode;
    String productprice;
    String options_id;
    String imagepath;

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    public String getOptionname() {
        return optionname;
    }

    public void setOptionname(String optionname) {
        this.optionname = optionname;
    }

    String optionname;



    public boolean isExsist() {
        return exsist;
    }

    public void setExsist(boolean exsist) {
        this.exsist = exsist;
    }

    public String getFooditem_id() {
        return fooditem_id;
    }

    public void setFooditem_id(String fooditem_id) {
        this.fooditem_id = fooditem_id;
    }

    public String getBillprice() {
        return billprice;
    }

    public void setBillprice(String billprice) {
        this.billprice = billprice;
    }

    public String getBranch_id() {
        return branch_id;
    }

    public void setBranch_id(String branch_id) {
        this.branch_id = branch_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getProductprice() {
        return productprice;
    }

    public void setProductprice(String productprice) {
        this.productprice = productprice;
    }

    public String getOptions_id() {
        return options_id;
    }

    public void setOptions_id(String options_id) {
        this.options_id = options_id;
    }
}
