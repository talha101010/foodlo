package aqdeveloper.thebhakkar.FastFood;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import aqdeveloper.thebhakkar.R;

public class ShopMenuAdaptor   extends RecyclerView.Adapter<ShopMenuAdaptor.MyViewHolder> {

    Context c;
    ArrayList<MenuFoodItems> categorieslist;
    private MyProgressDialog myProgressDialog;
    int hold = 0;

    public ShopMenuAdaptor(Context context, ArrayList<MenuFoodItems> categorieslist) {
        c = context;
        this.categorieslist = categorieslist;


    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView categoryname;
        View view;
        // ImageView categoryimage;
        RelativeLayout lay;

        public MyViewHolder(View v) {
            super(v);

            //categoryimage = (ImageView) v.findViewById(R.id.subcategoryimage);
            categoryname = (TextView) v.findViewById(R.id.subcategoryname);
            lay = (RelativeLayout) v.findViewById(R.id.lay);
            view = (View) v.findViewById(R.id.view);

        }


    }


    @Override
    public ShopMenuAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_menu, parent, false);
        ShopMenuAdaptor.MyViewHolder myViewHolder = new ShopMenuAdaptor.MyViewHolder(view);


        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final ShopMenuAdaptor.MyViewHolder holder, final int position) {


        if (hold == position) {

            holder.categoryname.setText(categorieslist.get(position).getProductname());

            holder.view.setBackgroundColor(ResourcesCompat.getColor(c.getResources(), R.color.black, null));
            holder.categoryname.setTextColor(ResourcesCompat.getColor(c.getResources(), R.color.black, null));

            holder.view.setVisibility(View.INVISIBLE);

        } else {

           // holder.view.setBackgroundColor(ResourcesCompat.getColor(c.getResources(), R.color.transparent, null));
            holder.categoryname.setTextColor(ResourcesCompat.getColor(c.getResources(), R.color.colorPrimaryDark, null));
            holder.categoryname.setText(categorieslist.get(position).getProductname());

        }


        holder.lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                holder.view.setBackgroundColor(ResourcesCompat.getColor(c.getResources(), R.color.white, null));
                holder.categoryname.setTextColor(ResourcesCompat.getColor(c.getResources(), R.color.white, null));

                hold = position;
                notifyDataSetChanged();


                ShopMenu.getFoodItems(categorieslist.get(position).getProductname());
            }
        });


    }

    @Override
    public int getItemCount() {
        return categorieslist.size();
    }
}
