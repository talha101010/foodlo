package aqdeveloper.thebhakkar.FastFood;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import aqdeveloper.thebhakkar.DrawerActivities.ShopDetailActivity;
import aqdeveloper.thebhakkar.R;

public class ShopSimilarAdaptor     extends RecyclerView.Adapter<ShopSimilarAdaptor.MyViewHolder> {

    Context context;
    ArrayList<FoodItemHolder> foodlist;
    private MyProgressDialog myProgressDialog;
    int hold = 0;
    private SharedPreferences appSharedPrefs;
    int index;

    public ShopSimilarAdaptor(Context contexts, ArrayList<FoodItemHolder> foodlist) {
        context = contexts;
        this.foodlist = foodlist;
        appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);


    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView foodimage;
        TextView foodname, fooddescription, price, quantity;
        Button addtocart, plus, minus;
        ProgressBar progressBar;
        RelativeLayout buttonslayput;
        LinearLayout layout;




        public MyViewHolder(View v) {
            super(v);

            foodimage = (ImageView)v.findViewById(R.id.similar_image);
            foodname = (TextView)v.findViewById(R.id.similar_name);
            fooddescription = (TextView)v.findViewById(R.id.similar_size);
            price = (TextView)v.findViewById(R.id.similar_price);
            quantity = (TextView)v.findViewById(R.id.quantity);
            addtocart = (Button)v.findViewById(R.id.similar_addtocart);
            plus = (Button)v.findViewById(R.id.plus);
            minus = (Button)v.findViewById(R.id.minus);
            progressBar = (ProgressBar)v.findViewById(R.id.progress );
            layout = (LinearLayout)v.findViewById(R.id.row);
            buttonslayput = (RelativeLayout)v.findViewById(R.id.similar_cartpanel);





        }


    }


    @Override
    public ShopSimilarAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_similar, parent, false);
        final ShopSimilarAdaptor.MyViewHolder holder = new ShopSimilarAdaptor.MyViewHolder(view);


        return holder;
    }

    @Override
    public void onBindViewHolder(final ShopSimilarAdaptor.MyViewHolder holder, final int position) {
        if (verifyOrder(position, holder.quantity)) {
            holder.addtocart.setVisibility(View.GONE);
            holder.plus.setVisibility(View.VISIBLE);
            holder.minus.setVisibility(View.VISIBLE);
            holder.quantity.setVisibility(View.VISIBLE);
            holder.buttonslayput.setVisibility(View.VISIBLE);

        } else {
            holder.addtocart.setVisibility(View.VISIBLE);
            holder.plus.setVisibility(View.GONE);
            holder.minus.setVisibility(View.GONE);
            holder.quantity.setVisibility(View.GONE);
        }


        Glide.with(context)
                .load(foodlist.get(position).getImagepath())
                .dontAnimate()

                .placeholder(R.drawable.logoone)


                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.foodimage);


        holder.foodname.setText(foodlist.get(position).getProductname());
        holder.price.setText(foodlist.get(position).getProductdescription());

        holder.fooddescription.setText("Rs " + foodlist.get(position).getPrice());


        holder.addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (AddtoNormalCart(position)) {
                    holder.addtocart.setVisibility(View.GONE);
                    holder.addtocart.setVisibility(View.GONE);
                    holder.plus.setVisibility(View.VISIBLE);
                    holder.minus.setVisibility(View.VISIBLE);
                    holder.quantity.setVisibility(View.VISIBLE);
                    holder.quantity.setText("1");
                    holder.buttonslayput.setVisibility(View.VISIBLE);
                }

                ShopDetailActivity.HandleCartNumber();


            }
        });


        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int q = Integer.parseInt(holder.quantity.getText().toString());
                q += 1;
                holder.quantity.setText(q + "");
                updateCart(q + "", position);


            }
        });


        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int q = Integer.parseInt(holder.quantity.getText().toString());
                if (q != 1) {
                    q -= 1;
                    holder.quantity.setText(q + "");
                    updateCart(q + "", position);

                } else {
                    if (RemoveOrder(foodlist.get(position).getDealid())) {
                        notifyDataSetChanged();
                        ShopDetailActivity.HandleCartNumber();
                    }

                }
            }
        });


        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(ShopDetailActivity.activity!=null)
                    ShopDetailActivity.activity.finish();

                context.startActivity(new Intent(context, ShopDetailActivity.class)
                        .putExtra("name",foodlist.get(position).getProductname())
                        .putExtra("price",foodlist.get(position).getPrice())
                        .putExtra("unit",foodlist.get(position).getProductdescription())
                        .putExtra("path",foodlist.get(position).getImagepath())
                        .putExtra("id",foodlist.get(position).getDealid())
                );




            }
        });





    }

    @Override
    public int getItemCount() {
        return foodlist.size();
    }


    private boolean AddtoNormalCart(int position) {
        ArrayList<BillModel> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        String json = appSharedPrefs.getString("shopcart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            arrayList = new ArrayList<>();


        BillModel billHolder = new BillModel();
        billHolder.setExsist(true);
        billHolder.setFooditem_id(foodlist.get(position).getDealid());
        billHolder.setBillprice(foodlist.get(position).getPrice());
        billHolder.setQuantity("1");
        billHolder.setProductprice(foodlist.get(position).getPrice());
        billHolder.setImagepath(foodlist.get(position).getImagepath());
        billHolder.setUnit(foodlist.get(position).getProductdescription());
        billHolder.setStorename(foodlist.get(position).getStorename());


        billHolder.setName(foodlist.get(position).getProductname());
        arrayList.add(billHolder);


        // savong cart to share preferences


        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("shopcart", jsons);
        prefsEditor.commit();
        return true;
        // Toast.makeText(context, "item added", Toast.LENGTH_SHORT).show();

    }


    public boolean verifyOrder(int position, TextView quantity) {
        ArrayList<BillModel> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        String json = appSharedPrefs.getString("shopcart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            return false;


        for (int i = 0; i < arrayList.size(); i++) {


            if (arrayList.get(i).getFooditem_id().equals(foodlist.get(position).getDealid())) {
                index = i;
                quantity.setText(arrayList.get(index).getQuantity());
                return true;

            }


        }

        return false;
    }

    public boolean RemoveOrder(String id) {
        ArrayList<BillModel> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        String json = appSharedPrefs.getString("shopcart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            return false;


        for (int i = 0; i < arrayList.size(); i++) {


            if (arrayList.get(i).getFooditem_id().equals(id)) {
                index = i;
                arrayList.remove(index);
                break;
            }


        }

        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("shopcart", jsons);
        prefsEditor.commit();
        return true;


    }

    public void updateCart(String quantity, int position) {
        ArrayList<BillModel> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        String json = appSharedPrefs.getString("shopcart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            arrayList = new ArrayList<>();

        for (int i = 0; i < arrayList.size(); i++) {


            if (arrayList.get(i).getFooditem_id().equals(foodlist.get(position).getDealid())) {
                index = i;
                BillModel bill = arrayList.get(i);
                bill.setQuantity(quantity);
                break;


            }


        }


        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("shopcart", jsons);
        prefsEditor.commit();
    }

}


