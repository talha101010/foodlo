package aqdeveloper.thebhakkar.FastFood;

public class MenuFoodItems {

    String imagepath;
    String productname;
    String productdescription;
    String productprice;
    String dealid;
    String nameinUrdu;

    public String getNameinUrdu() {
        return nameinUrdu;
    }

    public void setNameinUrdu(String nameinUrdu) {
        this.nameinUrdu = nameinUrdu;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    String quantity;

    public String getImagepath() {
        return imagepath;
    }

    public String getDealid() {
        return dealid;
    }

    public void setDealid(String dealid) {
        this.dealid = dealid;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getProductdescription() {
        return productdescription;
    }

    public void setProductdescription(String productdescription) {
        this.productdescription = productdescription;
    }

    public String getProductprice() {
        return productprice;
    }

    public void setProductprice(String productprice) {
        this.productprice = productprice;
    }
}
