package aqdeveloper.thebhakkar.Url;

import java.util.ArrayList;

import aqdeveloper.thebhakkar.FastFood.MenuFoodItems;

public class Constant {

    public static int totalbill,totalprofit;

    public static String senddataurl="http://thebhakkar.com/php/upload.php";
    public static final String IMAGES_URL = "http://thebhakkar.com/php/Fetch.php";
    public static final String updateurl = "http://thebhakkar.com/php/update.php";
    public static final String deleteurl = "http://thebhakkar.com/php/delete.php";

    public static String domainame="http://www.thebhakkar.com/bhakkarbazr03348870308/bhakkarbazr03348870308";

    public static final String SEND_SHOP_ADS_URL = domainame+"/shopsproduct.php";
    public static String ADD_CATEGORYURL = domainame+"/shopcategory.php";
    public static String ADD_GENERAL_ADS = domainame+"/generalbanner.php";
    public static String GET_SHOPTYPES_URL = domainame+"/shopcategoryspinner.php";


    public static String ADD_SHOP_URL = domainame+"/addshop.php";
    public static String GET_SHOP_FROMSERVER_URL = domainame+"/bhakkarbazarbtn.php";

    public static String UPDATE_SHOPURL = domainame+"/updateshopcategory.php";
    public static String DELETE_SHOPURL = domainame+"/deleteshopcategory.php";


    public static String GET_SHOP_RELATEDADS = domainame+"/shopsproductshow.php";
    public static String GET_GENERALADS_URL = domainame+"/generalbannershow.php";

    public static String GET_SHOP_AGAINSTTYPE_URL = domainame+"/addshopshow.php";
    public static String ADD_PENDING_SHOP_URl = domainame+"/pendingshops.php";
    public static String GET_PENDINGSHOP_URL = domainame+"/showpendingshops.php";
    public static String deleteShopUrl =domainame+"/deletependingshops.php";
    public static  String get_Detail_of_Shop =domainame+"/addshopshowdetail.php";
    public static String GET_SHOP_ADS_URL=domainame+"/showspecificadfrombazartable.php";
    public static String DELETE_SHOP_URL =domainame+"/deleteaddshop.php";
    public  static  String updateshopurl=domainame+"/updateaddshop.php";
    public static String DELETE_AD_URL=domainame+"/deleterechclerpics.php";

    public  static  String GET_SHOPKEEPER_ADS_URL =domainame+"/showbazar.php";
    public static String DELETE_SHOPKEEPER_AD_URL=domainame+"/bazartabledelete.php";
    public static String EDIT_SHOPKEEPER_AD_URL =domainame+"/updatebazartable.php";


    public static  String serverUrl = "http://thebhakkar.com/bhakkarbazarbanner";
    public  static  String postDataUrl = serverUrl+"/banneradd.php";
    public  static  String getDataUrl = serverUrl+"/bannershow.php";
    public  static  String deleteDataUrl = serverUrl+"/bannerdelete.php";
    public  static  String updateDataUrl = serverUrl+"/bannerupdate.php";



    /////// service man urls


    private static final String ROOT_UTL = "http://www.thebhakkar.com/SM/publishedapp/";
    //https://tmh101010.000webhostapp.com/publishedapp/
    public static final String EMAIL_API = "https://servicemanmailapi.000webhostapp.com/emailapi.php";
    public static final String SHOW_SERVICE_URL = ROOT_UTL+"showcategory.php";
    public static final String SHOW_SERVICE_DETAIL_URL = ROOT_UTL+"show_services_detail.php";
    public static final String SHOW_SERVICE_MAN_PROFILE_DETAIL = ROOT_UTL+"show_service_man_profile_details.php";
    public static final String SHOW_AREA_URL = ROOT_UTL+"showarea.php";
    public static final String SHOW_CATEGORY_URL = ROOT_UTL+"showcategory.php";
    public static final String BRAND_SMS_API = ROOT_UTL+"phonenumberapi.php";
    public static final String SHOW_DATA_TO_USER = ROOT_UTL+"showprofiledetailtouser.php";
    public static final String LOG_IN_URL = ROOT_UTL+"login.php";
    public static final String UPDATE_URL = ROOT_UTL+"updateuser.php";
    public  static final String UPDATE_STATUS_URL = ROOT_UTL+"updatestatus.php";
    public static final String ADD_USER_TO_PENDING_URL = ROOT_UTL+"pending_users_data.php";
    public static final String SPINNER_AREA_URL = ROOT_UTL+"areasearchspinner.php";
    public static final String CHANGE_PASSWORD_URL = ROOT_UTL+"changepassword.php";
    public static final String FORGOT_PASSWORD_URL = ROOT_UTL+"forgotpassword.php";

    public static String ipfield = "http://www.thebhakkar.com/superstore/superstorewebservices/";
    public static  String EDIT_PROFILE_URL = ipfield + "/editProfile";






    public static String GET_PREVIOUS_HISTORY_URl = ipfield + "showOrderTrackingToUser.php";
    public static String gethistorydetail = ipfield + "detailOfOrderTracking.php";



    public  static ArrayList<MenuFoodItems> categoriesArrayList;


    public  static  String get_AREAS =ipfield+ "showArea.php";
    // public static String SIGNUPURL = ipfield + "signup.php";
    public static String SIGNUPURL = ipfield + "newSignup.php";
    public static String SIGINURL = ipfield + "login.php";
    public static String get_CATEGORIES_URL = ipfield + "showMainCategory.php";


    public static String GET_SUBCATEGORY_URL=ipfield+"showSubCategory.php";
    public static String GET_MENUURL=ipfield+"showMenu.php";
    public  static  String get_deals= ipfield+"showDiscountButton.php";
    public  static String get_items_url = ipfield+"showItems.php";
    public  static String get_deal_details = ipfield+"showDiscountItems.php";
    public  static String get_time_stamp = ipfield+"showTimestamp.php";


    public static String checkout_url_location = ipfield+"orders_location.php";

    //    public static String checkout_url  = ipfield+"orders.php";
    public static String checkout_url  = ipfield+"orders1.php";
    public  static  String update_profile = ipfield+"updateprofile.php";
    public static String searchURL=ipfield+"searchdata.php";
    public static String get_Tehsil_url = ipfield+"showTehsil.php";
    public static String get_Charges_url=ipfield+"showDeliveryChargesInfo.php";
    public static String FORGETPASSWORD=ipfield+"forgotpassword.php";
    public static String PASSWORDURL= ipfield+"changepassword.php";
    public static String getstoreinfo = ipfield + "showVersionAndStatus.php";
    public static  int currentversion= 44;
    public static String getfavouriateurl=ipfield+"showFavourite.php";
    public static String getcoupon= ipfield+"checkCoupan.php";
    public static String favouriateURl=ipfield+"insertFavourite.php";
}
