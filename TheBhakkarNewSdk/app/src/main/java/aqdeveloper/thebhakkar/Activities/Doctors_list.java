package aqdeveloper.thebhakkar.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import aqdeveloper.thebhakkar.Adapters.DoctorDataAdapter;
import aqdeveloper.thebhakkar.DataProviders.Doctor_Data_provider;
import aqdeveloper.thebhakkar.R;

public class Doctors_list extends AppCompatActivity {

    ListView doctors_list_view;
    DoctorDataAdapter doctorDataAdapter;
    String [] doctors_name_in_english,doctors_name_in_urdu,doctors_adress,doctors_timing,numbers;
    String s1,s2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctors_list);

        Intent intent =getIntent();
        doctors_name_in_english = intent.getStringArrayExtra("dr.name_in_eng");
        doctors_name_in_urdu = intent.getStringArrayExtra("drname_in_urdu");
        doctors_adress = intent.getStringArrayExtra("dr.adress");
        doctors_timing = intent.getStringArrayExtra("timing");
        numbers = intent.getStringArrayExtra("numbers");
        s1 = intent.getStringExtra("s1");
        s2 = intent.getStringExtra("s2");


        doctors_list_view = (ListView) findViewById(R.id.doctors_listview);
        doctorDataAdapter = new DoctorDataAdapter(getApplicationContext(),R.layout.doctors_list_customise);
        doctors_list_view.setAdapter(doctorDataAdapter);
        for (int i =0; i<doctors_name_in_english.length; i++){
            String name = doctors_name_in_english[i];
            String name_in_urdu = doctors_name_in_urdu[i];
            String adress = doctors_adress[i];

            Doctor_Data_provider doctor_data_provider = new Doctor_Data_provider(name,name_in_urdu,adress);
            doctorDataAdapter.add(doctor_data_provider);
        }

        doctors_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                sendDrData(position);
            }
        });
    }

    private void sendDrData(int position){

        Intent intent = new Intent(Doctors_list.this,Doctros_detail.class);
        intent.putExtra("name_english",doctors_name_in_english[position]);
        intent.putExtra("name_urdu",doctors_name_in_urdu[position]);
        intent.putExtra("adress",doctors_adress[position]);
        intent.putExtra("timing",doctors_timing[position]);
        intent.putExtra("numbers",numbers[position]);
        intent.putExtra("s1",s1);
        intent.putExtra("s2",s2);
        startActivity(intent);
    }
}
