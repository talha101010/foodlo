package aqdeveloper.thebhakkar.Activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import aqdeveloper.thebhakkar.Adapters.CategoryAdapter;
import aqdeveloper.thebhakkar.DataProviders.ShopsHolder;
import aqdeveloper.thebhakkar.Dialogs.ProDialog;
import aqdeveloper.thebhakkar.Http.AsyncTaskListener;
import aqdeveloper.thebhakkar.Http.HttpAsyncRequest;
import aqdeveloper.thebhakkar.Http.TaskResult;
import aqdeveloper.thebhakkar.Parser.ShopListParser;
import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.Url.Constant;

public class BhakkarBazarActivity extends AppCompatActivity {

    public static ListView listofshops;
    public static ProDialog myProgressDialog;
    public static ArrayList<ShopsHolder> listofdata;
    public static Activity activity;
    Button addshopbtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bhakkar_bazar);

        addshopbtn = (Button) findViewById(R.id.addshop);
        addshopbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BhakkarBazarActivity.this, AddPendingShop.class));
            }
        });

        activity = this;
        init();
        getShopfromServer();



    }

    private void init() {
        listofshops = findViewById(R.id.shopslist);
    }

    public static void getShopfromServer() {
        HttpAsyncRequest request = new HttpAsyncRequest(activity, Constant.GET_SHOP_FROMSERVER_URL, HttpAsyncRequest.RequestType.POST, new ShopListParser(), listenerforshop);


        request.execute();
        myProgressDialog = new ProDialog(activity);
        myProgressDialog.setCanceledOnTouchOutside(false);
        myProgressDialog.show();
    }

    public static AsyncTaskListener listenerforshop = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {


            try {
                myProgressDialog.dismiss();
            } catch (Exception e) {
//                if (swipeRefreshLayout.isRefreshing())
//                    swipeRefreshLayout.setRefreshing(false);
            }
            if (result.isSuccess()) {
                if (result.getMessage().equals("true")) {
                    listofdata = (ArrayList<ShopsHolder>) result.getData();
                    listofshops.setAdapter(new CategoryAdapter(activity, listofdata));


                } else {
                    Toast.makeText(activity, "No Shop Added Yet", Toast.LENGTH_SHORT).show();

                }


            } else {

                Toast.makeText(activity, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();

            }
        }


    };

}
