package aqdeveloper.thebhakkar.Activities;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import aqdeveloper.thebhakkar.R;

public class PrayerTimesActivity extends AppCompatActivity {

    TextView subah_sadiq, talo_aftab,ishraq,nisf_ul_nahar,misl_awwal,asr_hanfi,ghrob_aftab,
            isha,date_tv,chand_aham_masael;
    String[] time_oqat_array,splited_array;
    String oqat_item;
    Button next;
    int number;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prayer_times);

        getview();

        Calendar c = Calendar.getInstance();
        Calendar d = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("DD");
        String date = sdf.format(c.getTime());
        SimpleDateFormat s = new SimpleDateFormat("dd/MM/yyyy");
        String completeDate = s.format(d.getTime());



        try{
            time_oqat_array = getResources().getStringArray(R.array.namaz_kay_oqat_array);


            oqat_item = time_oqat_array[(Integer.parseInt(date))];

            splited_array = oqat_item.split("_");
            setTime(splited_array);
            date_tv.setText(completeDate);



        }catch (Exception e){

            Toast.makeText(this, e.toString()+date, Toast.LENGTH_SHORT).show();

        }


        chand_aham_masael.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final AlertDialog.Builder alertdialog = new AlertDialog.Builder(PrayerTimesActivity.this);
                LayoutInflater inflater =getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.chand_aham_masel_dialog,null);
                alertdialog.setView(dialogView);
                alertdialog.setTitle(".....چند اہم مسائل.....");

                final AlertDialog dialog = alertdialog.create();
                dialog.show();
                final TextView delete_list_item = (TextView) dialogView.findViewById(R.id.chand_aham_maslay_tv);
                final Button cancel_namaz_dialog_btn = (Button) dialogView.findViewById(R.id.cancel_namaz_dialog);
                cancel_namaz_dialog_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

            }
        });

    }

    public void  getview(){

        subah_sadiq = (TextView) findViewById(R.id.subah_sadiq);
        talo_aftab = (TextView) findViewById(R.id.talo_aftab);
        ishraq = (TextView) findViewById(R.id.ishraq);
        nisf_ul_nahar = (TextView) findViewById(R.id.nisful_nhar);
        misl_awwal = (TextView) findViewById(R.id.misl_awwal);
        asr_hanfi = (TextView) findViewById(R.id.asre_hanfi);
        ghrob_aftab = (TextView) findViewById(R.id.ghroob_aftab);
        isha = (TextView) findViewById(R.id.isha);
        date_tv = (TextView) findViewById(R.id.date);
        chand_aham_masael = (TextView) findViewById(R.id.chand_aham_malay_btn);

    }

    public void  setTime(String[] splited_array){

        subah_sadiq.setText(splited_array[0]);
        talo_aftab.setText(splited_array[1]);
        ishraq.setText(splited_array[2]);
        nisf_ul_nahar.setText(splited_array[3]);
        misl_awwal.setText(splited_array[4]);
        asr_hanfi.setText(splited_array[5]);
        ghrob_aftab.setText(splited_array[6]);
        isha.setText(splited_array[7]);





    }


}
