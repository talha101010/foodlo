package aqdeveloper.thebhakkar.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import aqdeveloper.thebhakkar.Adapters.NumbersAdaptor;
import aqdeveloper.thebhakkar.DataProviders.DataProvider;
import aqdeveloper.thebhakkar.R;


public class ShowNumbers extends AppCompatActivity {

    String getCityString;
    DataProvider dataProvider;
    DatabaseReference databaseReference;
    List<DataProvider> list;
    NumbersAdaptor numbersAdaptor;
    RecyclerView recyclerView;
    EditText searchNumber;
    ProgressBar mProgressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_numbers);

        mProgressbar = findViewById(R.id.mProgressbar);

        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this,1));

        Intent intent = getIntent();
        getCityString = intent.getStringExtra("city");
        databaseReference = FirebaseDatabase.getInstance().getReference("data").child(getCityString);
        list = new ArrayList<>();
        searchNumber = findViewById(R.id.search_number);




        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                list.clear();

                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                    DataProvider dp = dataSnapshot1.getValue(DataProvider.class);
                    list.add(dp);

                }

                numbersAdaptor = new NumbersAdaptor(ShowNumbers.this,list);
                recyclerView.setAdapter(numbersAdaptor);
                mProgressbar.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




        searchNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                ShowNumbers.this.numbersAdaptor.setFilter(list);
                List<DataProvider> newList = new ArrayList<>();
                for (DataProvider dp : list){

                    String name = dp.getName().toLowerCase();
                    if (name.contains(s)){
                        newList.add(dp);
                    }
                }
                numbersAdaptor.setFilter(newList);
            }
        });

    }
}
