package aqdeveloper.thebhakkar.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

import aqdeveloper.thebhakkar.Adapters.ShowTypeAdaptor;
import aqdeveloper.thebhakkar.DataProviders.UploadImage;
import aqdeveloper.thebhakkar.R;

public class BkrPhoneDirectoryHome extends AppCompatActivity {

    RecyclerView showTypeRecyclerview;

    UploadImage uploadImage;
    List<UploadImage> imageList;
    FirebaseStorage showStorage;
    DatabaseReference showTeachersPicture;
    ProgressBar mProgressbar;
    ShowTypeAdaptor showTypeAdaptor;
    private StorageReference storageReference;
    Button addNumberBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bkr_phone_directory_home);

        showTypeRecyclerview =findViewById(R.id.showtyperecyclerview);

        mProgressbar = findViewById(R.id.mProgressbar);
        showTypeRecyclerview.setHasFixedSize(true);
        showTypeRecyclerview.setLayoutManager(new GridLayoutManager(this,3));

        imageList = new ArrayList<>();

        showTeachersPicture = FirebaseDatabase.getInstance().getReference("type");
        storageReference = FirebaseStorage.getInstance().getReference("categoriesimages/");

        showTeachersPicture.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                imageList.clear();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){

                    UploadImage ui = dataSnapshot1.getValue(UploadImage.class);
                    imageList.add(ui);
                    showTypeAdaptor  = new ShowTypeAdaptor(BkrPhoneDirectoryHome.this,imageList);

                    showTypeRecyclerview.setAdapter(showTypeAdaptor);
                    mProgressbar.setVisibility(View.GONE);
                    showTypeAdaptor.setOnItemClickListener(new ShowTypeAdaptor.OnItemClickListener() {
                        @Override
                        public void onItemClick(int position) {
                            UploadImage ui = imageList.get(position);
                            Intent intent = new Intent(BkrPhoneDirectoryHome.this,ShowNumbers.class);
                            intent.putExtra("city",ui.getImageName());
                            startActivity(intent);



                        }

                        @Override
                        public void onButtonChange(int position) {

                        }
                    });

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        addNumberBtn = findViewById(R.id.addNumberBtn);
        addNumberBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BkrPhoneDirectoryHome.this,NumbersList.class));
            }
        });


    }
}
