package aqdeveloper.thebhakkar.Activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import aqdeveloper.thebhakkar.R;

public class Doctros_detail extends AppCompatActivity {

    TextView detail_name_urdu,detail_name_eng,s1,s2,detail_adress,timing;
    Button share_info_btn,wrong_info_btn;
    LinearLayout call_to_doctor_linear_layout;
    String number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctros_detail);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        detail_name_urdu = (TextView) findViewById(R.id.detail_urdu_name);
        detail_name_eng = (TextView) findViewById(R.id.detail_name_in_eng);
        s1 = (TextView) findViewById(R.id.s1);
        s2 = (TextView) findViewById(R.id.s2);
        detail_adress = (TextView) findViewById(R.id.detail_adress);
        timing = (TextView) findViewById(R.id.timing);
        share_info_btn = (Button) findViewById(R.id.share_info_btn);
        share_info_btn.setMinimumWidth(width/2);
        call_to_doctor_linear_layout = (LinearLayout) findViewById(R.id.call_to_doctor_linear_layout);
        call_to_doctor_linear_layout.setMinimumWidth(width/2);

        final Intent intent = getIntent();

        detail_name_urdu.setText(intent.getStringExtra("name_urdu"));
        detail_name_eng.setText(intent.getStringExtra("name_english"));
        s1.setText(intent.getStringExtra("s1"));
        s2.setText(intent.getStringExtra("s2"));
        detail_adress.setText(intent.getStringExtra("adress"));
        timing.setText(intent.getStringExtra("timing"));
        number = intent.getStringExtra("numbers");

        call_to_doctor_linear_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel",number,null));
                startActivity(callIntent);
            }
        });

        share_info_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent smsintent = new Intent(Intent.ACTION_VIEW);
//                smsintent.setData(Uri.parse("smsto:"));
//                smsintent.setType("vnd.android-dir/mms-sms");
//                smsintent.putExtra("address",new String());
//                smsintent.putExtra("sms_body",new String(intent.getStringExtra("name_urdu")+"\n"+
//                        intent.getStringExtra("s2")+"\n"+
//                        intent.getStringExtra("adress")+"\n"+
//                        number+"\n\n"+"https://play.google.com/store/apps/details?id=aqdeveloper.thebhakkar&hl=en"));
//                startActivity(smsintent);

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = intent.getStringExtra("name_urdu")+"\n"+
                        intent.getStringExtra("s2")+"\n"+
                        intent.getStringExtra("adress")+"\n"+
                        number+"\n\n"+"https://play.google.com/store/apps/details?id=aqdeveloper.thebhakkar&hl=en";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share Information"));
            }
        });


    }
}

