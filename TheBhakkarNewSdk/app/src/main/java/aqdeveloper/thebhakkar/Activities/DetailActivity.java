package aqdeveloper.thebhakkar.Activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import aqdeveloper.thebhakkar.R;

public class DetailActivity extends AppCompatActivity {

    TextView detail_name,detail_number,urduName;
    int icon;
    String setName,setNumber,urduName_str;
    Button share_info_btn2,call_btn;
    ImageView detail_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        detail_name =  findViewById(R.id.detail_name_in_eng);
        detail_number =  findViewById(R.id.detail_set_number);
        urduName =  findViewById(R.id.detail_name_in_urdu);
        detail_icon = findViewById(R.id.detail_icon);
        share_info_btn2 = findViewById(R.id.detail_share_btn);
        call_btn = findViewById(R.id.detail_call_btn);

        try {

            Intent intent = getIntent();
            setName = intent.getStringExtra("sendName");
            detail_name.setText(setName);
            setNumber = intent.getStringExtra("sendNumber");
            detail_number.setText(setNumber);
            urduName_str = intent.getStringExtra("urduName");
            urduName.setText(urduName_str);
            icon = intent.getIntExtra("icon",0);
            detail_icon.setImageResource(icon);

        }catch (Exception e){

            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();

        }

        share_info_btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = setName+"\n\n"+setNumber+"\n\n\n"+"Provided by The Bhakkar android application Download from link below"+"\n\n"+"https://play.google.com/store/apps/details?id=aqdeveloper.thebhakkar&hl=en";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share Information"));
            }
        });

        call_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel",setNumber,null));
                startActivity(callIntent);
            }
        });




    }
}
