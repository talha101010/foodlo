package aqdeveloper.thebhakkar.Activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import aqdeveloper.thebhakkar.R;

public class PhoneDirectoryDetail extends AppCompatActivity {

    TextView name, num1, num2,adress,description;
    String strName, strNum1, strNum2, strAdress,strDes;
    CardView landlineCardview, desCardview;
    LinearLayout landlineCalllayout;

    ImageButton cellCallBtn, landlineCallBtn;
    Button shareBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_directory_detail);

        name = findViewById(R.id.name);
        num1 = findViewById(R.id.num1);
        num2= findViewById(R.id.num2);
        adress = findViewById(R.id.adress);
        description = findViewById(R.id.description);
        landlineCardview = findViewById(R.id.cellNumber2CardView);
        desCardview = findViewById(R.id.desCardview);
        landlineCalllayout = findViewById(R.id.landlinelayout);
        cellCallBtn = findViewById(R.id.cellPhoneCallBtn);
        landlineCallBtn = findViewById(R.id.landlineCallBtn);
        shareBtn = findViewById(R.id.shareBtn);

        Intent intent = getIntent();

        strName= intent.getStringExtra("name");
        strNum1= intent.getStringExtra("num1");
        strNum2= intent.getStringExtra("num2");
        strAdress= intent.getStringExtra("adress");
        strDes = intent.getStringExtra("des");

        if (strNum2.equals("null")){

            num2.setText("Not Available");
            landlineCallBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(PhoneDirectoryDetail.this, "Not Available", Toast.LENGTH_SHORT).show();
                }
            });


        }else {

            num2.setText(strNum2);


            landlineCallBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel",strNum2,null));
                    startActivity(callIntent);

                }
            });


        }

        if (strDes.equals("null")){

            description.setText("Not Provided");


        }else {

            description.setText(strDes);


        }

        name.setText(strName);
        num1.setText(strNum1);

        adress.setText(strAdress);

        cellCallBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel",strNum1,null));
                startActivity(callIntent);
            }
        });

        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody ="Name:\t"+ strName+"\nPhone Number:\t"+strNum1+"\nAdress:\t"+strAdress+"\n\n\n"+"This information is provided by The Bhakkar android application Download from link below"+"\n\n"+"https://play.google.com/store/apps/details?id=aqdeveloper.thebhakkar&hl=en";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share Information"));
            }
        });



    }
}
