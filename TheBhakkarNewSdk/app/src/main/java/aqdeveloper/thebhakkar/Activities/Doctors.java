package aqdeveloper.thebhakkar.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import aqdeveloper.thebhakkar.Adapters.DoctorCategoryAdapter;
import aqdeveloper.thebhakkar.DataProviders.Doctor_Data_provider;
import aqdeveloper.thebhakkar.R;

public class Doctors extends AppCompatActivity {

    ListView doctorCatagoryLv;
    Doctor_Data_provider dataProvider;
    DoctorCategoryAdapter doctorAdaptor;


    String [] doctors_name_in_english,doctors_name_in_urdu,doctors_adress,doctors_timing,phone_numbers,doctorcatory_eng,doctorcategory_urdu;
    String specialist_eng,specialist_urdu;

    int[] icons = {R.drawable.ic_ribbon,R.drawable.ic_stomach_shape,R.drawable.ic_dentist_icon,R.drawable.ic_kidneys
            , R.drawable.ic_sore_throat,R.drawable.ic_neuro_icon,R.drawable.ic_general_sergon,
            R.drawable.ic_heart_checkup,R.drawable.ic_new_born_baby,R.drawable.ic_eye,
            R.drawable.ic_pediatrician,R.drawable.ic_orthopedics,R.drawable.ic_psychologist,R.drawable.ic_laboratory,
            R.drawable.ic_vaternory_doctor_icon,R.drawable.hakeem_icon,R.drawable.homeopath_icon,R.drawable.ic_medicines
            ,R.drawable.radiologyicon
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctors);

        getView();
        getArrays();

        doctorAdaptor = new DoctorCategoryAdapter(getApplicationContext(),R.layout.doctorcatagory_lv_customise);

        for (int i=0; i<doctorcatory_eng.length; i++){

            String name_str = doctorcatory_eng[i];
            String number_str = doctorcategory_urdu[i];
            int icon_int  = icons[i];
            dataProvider = new Doctor_Data_provider(name_str,number_str,icon_int);
            doctorAdaptor.add(dataProvider);
        }

        doctorCatagoryLv.setAdapter(doctorAdaptor);

        doctorCatagoryLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
//                    case 0:
//
////                        doctors_name_in_english = getResources().getStringArray(R.array.cancer_name_in_english);
////                        doctors_name_in_urdu = getResources().getStringArray(R.array.cancer_name_in_urdu);
////                        doctors_adress = getResources().getStringArray(R.array.cancer_adress);
////                        doctors_timing = getResources().getStringArray(R.array.cancer_timing);
////                        phone_numbers = getResources().getStringArray(R.array.cancer_numbers);
////                        specialist_eng = "Cancer";
////                        specialist_urdu="ماہر امراض کینسر";
////                        sendArrays();
//
//                        break;

                    case 0:

                        doctors_name_in_english = getResources().getStringArray(R.array.gastroenterologist_name_in_english);
                        doctors_name_in_urdu = getResources().getStringArray(R.array.gastroenterologist_name_in_urdu);
                        doctors_adress = getResources().getStringArray(R.array.gastroenterologist_adress);
                        doctors_timing = getResources().getStringArray(R.array.gastroenterologist_timing);
                        phone_numbers = getResources().getStringArray(R.array.gastroenterologist_numbers);
                        specialist_eng = "Gastroenterologist";
                        specialist_urdu="ما ہرامراض معدہ و جگر";
                        sendArrays();

                        break;

                    case 1:

                        doctors_name_in_english = getResources().getStringArray(R.array.dentists_dr_name_in_english);
                        doctors_name_in_urdu = getResources().getStringArray(R.array.dentists_dr_name_in_urdu);
                        doctors_adress = getResources().getStringArray(R.array.dentists_dr_adress);
                        doctors_timing = getResources().getStringArray(R.array.dentists_dr_timing);
                        phone_numbers = getResources().getStringArray(R.array.dentists_dr_numbers);
                        specialist_eng = "Dentist";
                        specialist_urdu="دانتوں کے ماہر";
                        sendArrays();

                        break;

                    case 2:
                        doctors_name_in_english = getResources().getStringArray(R.array.uorology_dr_name_in_english);
                        doctors_name_in_urdu = getResources().getStringArray(R.array.uorology_dr_name_in_urdu);
                        doctors_adress = getResources().getStringArray(R.array.uorology_dr_adress);
                        doctors_timing = getResources().getStringArray(R.array.uorology_dr_timing);
                        phone_numbers = getResources().getStringArray(R.array.uorology_dr_numbers);
                        specialist_eng = "Urologist";
                        specialist_urdu="ماہر امراض گردہ ومثانہ";
                        sendArrays();

                        break;

                    case 3:

                        doctors_name_in_english = getResources().getStringArray(R.array.ent_dr_name_in_enlish);
                        doctors_name_in_urdu = getResources().getStringArray(R.array.ent_dr_name_in_urdu);
                        doctors_adress = getResources().getStringArray(R.array.ent_dr_adress);
                        doctors_timing = getResources().getStringArray(R.array.ent_dr_timing);
                        phone_numbers = getResources().getStringArray(R.array.ent_dr_numbers);
                        specialist_eng = "ENT";
                        specialist_urdu="کان،ناک،گلا کے ماہر";
                        sendArrays();

                        break;
                    case 4:

                        doctors_name_in_english = getResources().getStringArray(R.array.neuro_dr_name_in_english);
                        doctors_name_in_urdu = getResources().getStringArray(R.array.neuro_dr_name_in_urdu);
                        doctors_adress = getResources().getStringArray(R.array.neuro_dr_adress);
                        doctors_timing = getResources().getStringArray(R.array.neuro_dr_timing);
                        phone_numbers = getResources().getStringArray(R.array.neuro_dr_numbers);
                        specialist_eng = "Neurologist";
                        specialist_urdu="ماہر امراض دماغ و اعصاب";
                        sendArrays();

                        break;
                    case 5:
                        doctors_name_in_english = getResources().getStringArray(R.array.general_surgeon_dr_name_in_english);
                        doctors_name_in_urdu = getResources().getStringArray(R.array.general_surgeon_dr_name_in_urdu);
                        doctors_adress = getResources().getStringArray(R.array.general_surgeon_dr_adress);
                        doctors_timing = getResources().getStringArray(R.array.general_surgeon_dr_timing);
                        phone_numbers = getResources().getStringArray(R.array.general_surgeon_dr_numbers );
                        specialist_eng = "General Surgeon";
                        specialist_urdu="جنرل سرجن";
                        sendArrays();

                        break;
                    case 6:

                        doctors_name_in_english = getResources().getStringArray(R.array.cardialogy_dr_name_in_english);
                        doctors_name_in_urdu = getResources().getStringArray(R.array.cardialogy_dr_name_in_urdu);
                        doctors_adress = getResources().getStringArray(R.array.cardialogy_dr_adress);
                        doctors_timing = getResources().getStringArray(R.array.cardialogy_dr_timing);
                        phone_numbers = getResources().getStringArray(R.array.cardialogy_dr_numbers );
                        specialist_eng = "Cardialogist";
                        specialist_urdu="ما ہرامراض قلب";
                        sendArrays();

                        break;
                    case 7:

                        doctors_name_in_english = getResources().getStringArray(R.array.gyni_dr_name_in_english);
                        doctors_name_in_urdu = getResources().getStringArray(R.array.gyni_dr_name_in_urdu);
                        doctors_adress = getResources().getStringArray(R.array.gyni_dr_adress);
                        doctors_timing = getResources().getStringArray(R.array.gyni_dr_timing);
                        phone_numbers = getResources().getStringArray(R.array.gyni_dr_numbers );
                        specialist_eng = "Gynaecologist";
                        specialist_urdu="ماہر امراض نسواں،زچہ وبچہ";
                        sendArrays();

                        break;
                    case 8:

                        doctors_name_in_english = getResources().getStringArray(R.array.eye_dr_name_english);
                        doctors_name_in_urdu = getResources().getStringArray(R.array.eye_dr_name_urdu);
                        doctors_adress = getResources().getStringArray(R.array.eye_dr_adress);
                        doctors_timing = getResources().getStringArray(R.array.eye_dr_timing);
                        phone_numbers = getResources().getStringArray(R.array.eye_dr_numbers);
                        specialist_eng = "Ophthalmologist";
                        specialist_urdu="ماہر امراض چشم";
                        sendArrays();


                        break;
                    case 9:

                        doctors_name_in_english = getResources().getStringArray(R.array.child_specialist_dr_name_in_english);
                        doctors_name_in_urdu = getResources().getStringArray(R.array.child_specialist_dr_name_in_urdu);
                        doctors_adress = getResources().getStringArray(R.array.child_specialist_dr_adress);
                        doctors_timing = getResources().getStringArray(R.array.child_specialist_dr_timing);
                        phone_numbers = getResources().getStringArray(R.array.child_specialist_dr_numbers);
                        specialist_eng = "Pediatrician";
                        specialist_urdu="بچوں کے امراض کے ماہر";
                        sendArrays();


                        break;

                    case 10:

                        doctors_name_in_english = getResources().getStringArray(R.array.orthopedics_dr_name_in_english);
                        doctors_name_in_urdu = getResources().getStringArray(R.array.orthopedics_dr_name_in_urdu);
                        doctors_adress = getResources().getStringArray(R.array.orthopedics_dr_adress);
                        doctors_timing = getResources().getStringArray(R.array.orthopedics_dr_timing);
                        phone_numbers = getResources().getStringArray(R.array.orthopedics_dr_numbers);
                        specialist_eng = "Orthopedics";
                        specialist_urdu="ہڈیوں اور جوڑوں کے ڈاکٹرز";
                        sendArrays();

                        break;
                    case 11:

//                        doctors_name_in_english = getResources().getStringArray(R.array.psychiatrists_dr_name_in_english);
//                        doctors_name_in_urdu = getResources().getStringArray(R.array.psychiatrists_dr_name_in_urdu);
//                        doctors_adress = getResources().getStringArray(R.array.psychiatrists_dr_adress);
//                        doctors_timing = getResources().getStringArray(R.array.psychiatrists_dr_timing);
//                        phone_numbers = getResources().getStringArray(R.array.psychiatrists_dr_numbers);
//                        specialist_eng = "Psychiatrists";
//                        specialist_urdu="ماہر امراض نفسیا ت";
//                        sendArrays();

                        break;
                    case 12:

                        doctors_name_in_english = getResources().getStringArray(R.array.lab_name_in_enlish);
                        doctors_name_in_urdu = getResources().getStringArray(R.array.lab_name_in_urdu);
                        doctors_adress = getResources().getStringArray(R.array.lab_adress);
                        doctors_timing = getResources().getStringArray(R.array.lab_timing);
                        phone_numbers = getResources().getStringArray(R.array.lab_numbers);
                        specialist_eng = "Labartory";
                        specialist_urdu="لیبارٹری";
                        sendArrays();

                        break;
                    case 13:

                        doctors_name_in_english = getResources().getStringArray(R.array.vaternary_name_in_eng);
                        doctors_name_in_urdu = getResources().getStringArray(R.array.vaternary_name_in_urdu);
                        doctors_adress = getResources().getStringArray(R.array.vaternary_adress);
                        doctors_timing = getResources().getStringArray(R.array.vaternary_timing);
                        phone_numbers = getResources().getStringArray(R.array.vaternary_numbers);
                        specialist_eng = "Doctors of Veterinary";
                        specialist_urdu="جانوروں کے ڈاکٹرز";
                        sendArrays();

                        break;

                    case 14:

                        doctors_name_in_english = getResources().getStringArray(R.array.hakeem_name_in_english);
                        doctors_name_in_urdu = getResources().getStringArray(R.array.hakeem_name_in_urdu);
                        doctors_adress = getResources().getStringArray(R.array.hakeem_adress);
                        doctors_timing = getResources().getStringArray(R.array.hakeem_timing);
                        phone_numbers = getResources().getStringArray(R.array.hakeem_numbers);
                        specialist_eng = "Hakeem and Tabeeb";
                        specialist_urdu="حکیم اور طبیب";
                        sendArrays();

                        break;

                    case 15:

                        doctors_name_in_english = getResources().getStringArray(R.array.homeopath_dr_name_in_english);
                        doctors_name_in_urdu = getResources().getStringArray(R.array.homeopath_dr_name_in_urdu);
                        doctors_adress = getResources().getStringArray(R.array.homeopath_dr_adress);
                        doctors_timing = getResources().getStringArray(R.array.homeopath_dr_timing);
                        phone_numbers = getResources().getStringArray(R.array.homeopath_dr_numbers);
                        specialist_eng = "Homeopath";
                        specialist_urdu="ہو میو پیتھک ڈاکٹرز";
                        sendArrays();

                        break;
                    case  16:

                        doctors_name_in_english = getResources().getStringArray(R.array.pharmacy_drs_name_eng);
                        doctors_name_in_urdu = getResources().getStringArray(R.array.pharmacy_drs_name_urdu);
                        doctors_adress = getResources().getStringArray(R.array.pharmacy_drs_adress);
                        doctors_timing = getResources().getStringArray(R.array.pharmacy_drs_timing);
                        phone_numbers = getResources().getStringArray(R.array.pharmacy_drs_numbers);
                        specialist_eng = "Doctors of Pharmacy";
                        specialist_urdu="فارمیسی ڈاکٹرز";
                        sendArrays();

                        break;

                    case  17:

                        doctors_name_in_english = getResources().getStringArray(R.array.radiologists_name_in_eng);
                        doctors_name_in_urdu = getResources().getStringArray(R.array.radiologists_name_in_urdu);
                        doctors_adress = getResources().getStringArray(R.array.radiologists_adress);
                        doctors_timing = getResources().getStringArray(R.array.radiologists_timing);
                        phone_numbers = getResources().getStringArray(R.array.radiologists_numbers);
                        specialist_eng = "Radiologists";
                        specialist_urdu="ایکسرے اور الٹراساؤنڈ کے ماہر";
                        sendArrays();

                        break;

                }
            }
        });


    }


    private  void  sendArrays(){

        Intent intent = new Intent(Doctors.this,Doctors_list.class);
        intent.putExtra("dr.name_in_eng",doctors_name_in_english);
        intent.putExtra("drname_in_urdu",doctors_name_in_urdu);
        intent.putExtra("dr.adress",doctors_adress);
        intent.putExtra("timing",doctors_timing);
        intent.putExtra("numbers",phone_numbers);
        intent.putExtra("s1",specialist_eng);
        intent.putExtra("s2",specialist_urdu);
        startActivity(intent);
    }

    public  void  getView(){

        doctorCatagoryLv = (ListView) findViewById(R.id.doctors_catagory_lv);

    }

    public void getArrays(){

        doctorcatory_eng = getResources().getStringArray(R.array.doctorcategory_eng);
        doctorcategory_urdu =getResources().getStringArray(R.array.doctorcategory_urdu);
        // icons = getResources().getIntArray(R.array.icon);
    }
    }

