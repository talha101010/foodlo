package aqdeveloper.thebhakkar.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import aqdeveloper.thebhakkar.Adapters.DoctorDataAdapter;
import aqdeveloper.thebhakkar.DataProviders.Doctor_Data_provider;
import aqdeveloper.thebhakkar.Dialogs.ProDialog;
import aqdeveloper.thebhakkar.Parser.BaseParser;
import aqdeveloper.thebhakkar.R;

public class BKRShopsList extends AppCompatActivity  {

    public  static final  String PROPERTY_NAME_URDU = "property_name_urdu";
    public  static  final  String PROPERTY_NAME_ENG = "property_name_eng";
    public  static  final  String PROPERTY_OWNER_NAME = "property_owner_name";
    public static final  String PROPERTY_ADRESS = "property_adress";
    public static final String PROPERTY_CONTACT_NO = "property_contact_number";

    private static Activity activity;
    public static ProDialog dialogboxforshop;
    String shpop_type;
    ListView shopesListview;

    JSONObject jsonObject;
    JSONArray jsonArray;

    Doctor_Data_provider doctor_data_provider;
    DoctorDataAdapter doctorDataAdapter;
    AlertDialog alertDialog;
    String JSON_STRING,jsonstr;
    ProgressDialog dowloading;
    String login_url,op;
    Context context = this;
    List<Doctor_Data_provider> list;
    String[] contact_numbers_list,urdu_name_lists,owners_list,property_eng_list,adress_list;
    String name_eng,name_urdu,onwer_name,adress,contact_number,id;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bkrshops_list);

        activity = this;

        Intent intent = getIntent();
       // shpop_type = intent.getStringExtra("name");
        shopesListview = (ListView) findViewById(R.id.shopesListview);
        doctorDataAdapter = new DoctorDataAdapter(this,R.layout.doctors_list_customise);
        op = intent.getStringExtra("name");
        new bgWork().execute();

        shopesListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(BKRShopsList.this,ShopesDetail.class);
                intent.putExtra(PROPERTY_CONTACT_NO,contact_numbers_list[position]);
                intent.putExtra(PROPERTY_NAME_URDU,urdu_name_lists[position]);
                intent.putExtra(PROPERTY_OWNER_NAME,owners_list[position]);
                intent.putExtra(PROPERTY_NAME_ENG,property_eng_list[position]);
                intent.putExtra(PROPERTY_ADRESS,adress_list[position]);
                startActivity(intent);
            }
        });

    }

    class bgWork extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            login_url = "http://www.thebhakkar.com/bhakkarbazr03348870308/bhakkarbazr03348870308/addshopshow.php";

            dowloading = ProgressDialog.show(BKRShopsList.this,"دوکانوں کی لسٹ لوڈ ہو رہی ہے۔","انتظار کیجیئے۔۔۔");
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String post_data = URLEncoder.encode("type","UTF-8")+"="+URLEncoder.encode(op,"UTF-8");
                bw.write(post_data);
                bw.flush();
                bw.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bf = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String result="";
                StringBuilder stringBuilder = new StringBuilder();

                while ((JSON_STRING = bf.readLine()) !=null){


                    stringBuilder.append(JSON_STRING+"\n");


                }
                bf.close();
                inputStream.close();
                httpURLConnection.disconnect();

                return  stringBuilder.toString().trim();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }catch (Exception e){

                Toast.makeText(BKRShopsList.this, "no", Toast.LENGTH_SHORT).show();
            }

            return null;
        }



        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dowloading.dismiss();
            try{

                jsonstr = result;
//                alertDialog = new AlertDialog.Builder(BloodGroupsList.this).create();
//                alertDialog.setTitle(" status");
//                alertDialog.setMessage(jsonstr);
//                alertDialog.show();
                try {
                    jsonObject = new JSONObject(jsonstr);
                    jsonArray = jsonObject.getJSONArray("add_shop_info");


                    int count=0;

                    contact_numbers_list = new String[jsonArray.length()];
                    urdu_name_lists = new String[jsonArray.length()];
                    owners_list = new String[jsonArray.length()];
                    property_eng_list =new String[jsonArray.length()];
                    adress_list =new String[jsonArray.length()];

                    while (count<jsonArray.length()){

                        JSONObject jo = jsonArray.getJSONObject(count);
                        name_eng = jo.getString("shop_name_in_english");
                        name_urdu = jo.getString("shop_name_in_urdu");
                        onwer_name= jo.getString("shop_owner_name");
                        adress = jo.getString("shop_address");
                        contact_number = jo.getString("shop_phone_number");
                        id = jo.getString("id");
                        doctor_data_provider = new Doctor_Data_provider(name_eng,name_urdu,adress);
                        contact_numbers_list[count] =contact_number;
                        urdu_name_lists [count] = name_urdu;
                        owners_list[count] =onwer_name;
                        property_eng_list[count] = name_eng;
                        adress_list[count] =adress;

                        doctorDataAdapter.add(doctor_data_provider);

                        count++;

                        shopesListview.setAdapter(doctorDataAdapter);


                    } }catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(BKRShopsList.this, e.toString(), Toast.LENGTH_SHORT).show();

                }}catch (Exception e){

                alertDialog = new AlertDialog.Builder(BKRShopsList.this).create();
                alertDialog.setTitle("Check Internet Connection");
                alertDialog.setMessage("آپ انٹرنیٹ سے کنکٹ نہیں ہیں۔یا آپ کا انٹر ںیٹ پیکیج نہیں ہے۔یا انٹرنیٹ کی سپیڈ بہت کم ہے۔");
                alertDialog.show();
            }


        }

//        @Override
//        protected void onProgressUpdate(Void... values) {
//            super.onProgressUpdate(values);
//        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // dowloading.dismiss();
    }
}
