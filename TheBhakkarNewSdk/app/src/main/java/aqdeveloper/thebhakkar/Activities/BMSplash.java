package aqdeveloper.thebhakkar.Activities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import aqdeveloper.thebhakkar.FastFood.FFSplash;
import aqdeveloper.thebhakkar.FastFood.FastFoodShops;
import aqdeveloper.thebhakkar.Home;
import aqdeveloper.thebhakkar.Http.AsyncTaskListener;
import aqdeveloper.thebhakkar.Http.HttpAsyncRequest;
import aqdeveloper.thebhakkar.Http.TaskResult;
import aqdeveloper.thebhakkar.NewHome;
import aqdeveloper.thebhakkar.Parser.BaseParser;
import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.Url.Constant;

public class BMSplash extends AppCompatActivity implements BaseParser {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmsplash);

        if(isNetworkAvailable())
        {
            getversionAndStoreStatus();
        }
        else{
            show_Dialog();
        }
    }

    private void getversionAndStoreStatus() {
        HttpAsyncRequest requests = new HttpAsyncRequest(this, Constant.getstoreinfo, HttpAsyncRequest.RequestType.POST, this, listenerforstore);



        requests.execute();

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void show_Dialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);







        dialogBuilder.setTitle("Warning");
        dialogBuilder.setMessage("Your device has no internet connection. Please connect to internet to continue");

        dialogBuilder.setPositiveButton("Enable", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        });
        dialogBuilder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                finish();

            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    AsyncTaskListener listenerforstore = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            if (result.isSuccess()) {


            } else {
                try {
                    //  Toast.makeText(SplashScreen.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();
                    AlertDialog.Builder builder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(BMSplash.this, android.R.style.Theme_Material_Dialog_Alert);
                    } else {
                        builder = new AlertDialog.Builder(BMSplash.this);
                    }
                    builder.setTitle("Warning")
                            .setMessage("Error Communicating with the Server")
                            .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    getversionAndStoreStatus();

                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }catch (Exception e){}



            }
        }


    };

    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {
            result.success(true);
            try {
                JSONObject obj = new JSONObject(response);
                if( Integer.parseInt(obj.optString("version_number") )==Constant.currentversion)
                {
                    Intent intent = new Intent(BMSplash.this, BhakkarBazarActivity.class);
                    startActivity(intent);
                    finish();

                }
                else{

                    final  AlertDialog.Builder alertdialog = new AlertDialog.Builder(BMSplash.this);
                    LayoutInflater inflater = getLayoutInflater();
                    final View dialogview = inflater.inflate(R.layout.version_alert_dialog,null);
                    alertdialog.setView(dialogview);
                    final Button laterupdatebtn = dialogview.findViewById(R.id.lateupdatebtn);
                    final Button nowupdatebtn = dialogview.findViewById(R.id.nowupdatebtn);
                    alertdialog.setTitle("New Version Update");
                    final AlertDialog dialog = alertdialog.create();
                    dialog.show();

                    laterupdatebtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Intent intent = new Intent(BMSplash.this, BhakkarBazarActivity.class);
                            startActivity(intent);
                            finish();

                        }
                    });

                    nowupdatebtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            }
                        }
                    });
//                    AlertDialog.Builder builder;
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        builder = new AlertDialog.Builder(BMSplash.this, android.R.style.Theme_Material_Dialog_Alert);
//                    } else {
//                        builder = new AlertDialog.Builder(BMSplash.this);
//                    }
//                    builder.setTitle("Warning")
//                            .setMessage("New Version of App is available\nPlease Update App to Continue\nClick Update Below")
//                            .setPositiveButton("Update", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int which) {
//                                    // continue with delete
//                                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
//                                    try {
//                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
//                                    } catch (ActivityNotFoundException anfe) {
//                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
//                                    }
//                                }
//                            })
//                            .setIcon(android.R.drawable.ic_dialog_alert)
//                            .show();
                }




                result.success(true);
            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);
            }


        }
        return result;
    }

}
