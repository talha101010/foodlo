package aqdeveloper.thebhakkar.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import aqdeveloper.thebhakkar.Parser.BaseParser;
import aqdeveloper.thebhakkar.R;
import uk.co.senab.photoview.PhotoViewAttacher;

public class ShopsProductDetailActivity extends AppCompatActivity {

    ImageView image;
    TextView name, description;
    ProgressBar progressBar;
    PhotoViewAttacher pAttacherNewsPaper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shops_product_detail);

        init();


        Intent intent = getIntent();

        if (intent.hasExtra("imageurl") && intent.hasExtra("name") && intent.hasExtra("description")) {

            Glide.with(ShopsProductDetailActivity.this)
                    .load(intent.getStringExtra("imageurl"))

                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(image);
            pAttacherNewsPaper = new PhotoViewAttacher(image);
            pAttacherNewsPaper.update();
            name.setText(intent.getStringExtra("name"));
            description.setText(intent.getStringExtra("description"));





        }
    }

    private void init() {
        image = (ImageView)findViewById(R.id.image);
        name = (TextView)findViewById(R.id.name);
        description = (TextView)findViewById(R.id.description);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);

    }
}
