package aqdeveloper.thebhakkar.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import aqdeveloper.thebhakkar.Adapters.DoctorCategoryAdapter;
import aqdeveloper.thebhakkar.DataProviders.DataProvider;
import aqdeveloper.thebhakkar.DataProviders.Doctor_Data_provider;
import aqdeveloper.thebhakkar.DataProviders.UploadImage;
import aqdeveloper.thebhakkar.R;


public class NumbersList extends AppCompatActivity {

    TextView select_type;
    DatabaseReference databaseReference;
    List<UploadImage> lsit;
    String[] typeArray;
    ArrayAdapter<String> arrayAdapter;
    EditText name,firstNumber,secondNumber,adress,description;
    String strName, strFirstNumber,strSecondNumber, strAdress,strDescription,strSelectType;
    Button addBtn;
    ProgressBar mProgressbar;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbers_list);

        mProgressbar = findViewById(R.id.mProgressbar);

        databaseReference = FirebaseDatabase.getInstance().getReference("type");

        select_type = findViewById(R.id.select_type);
        addBtn = findViewById(R.id.add_data);

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText();
                DatabaseReference db = FirebaseDatabase.getInstance().getReference("pending");
                String id = db.push().getKey();

                DataProvider dataProvider = new DataProvider(id,strSelectType,strName,strFirstNumber,strSecondNumber,strAdress,strDescription);

                db.child(id).setValue(dataProvider);


            }
        });

        lsit = new ArrayList<>();

        select_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alertdialog = new AlertDialog.Builder(NumbersList.this);
                final LayoutInflater inflater =getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.select_phone_dialog,null);
                final ListView lv = dialogView.findViewById(R.id.select_type_listview);
                alertdialog.setView(dialogView);


                alertdialog.setTitle("Select Type");

                setTypeArray();
                lv.setAdapter(arrayAdapter);

                final AlertDialog dialog = alertdialog.create();
                dialog.show();
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        select_type.setText(arrayAdapter.getItem(position).toString());
                        dialog.dismiss();
                    }
                });

            }
        });

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                lsit.clear();

                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){

                    UploadImage ui = dataSnapshot1.getValue(UploadImage.class);
                    lsit.add(ui);
                    typeArray = new String[lsit.size()];
                    for (int i =0; i<typeArray.length; i++){

                        typeArray[i] = lsit.get(i).getImageName();
                    }

                    mProgressbar.setVisibility(View.INVISIBLE);

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void setTypeArray(){

        if (typeArray==null){

            Toast.makeText(this, "No Internet Access", Toast.LENGTH_SHORT).show();
        }else {

            arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, typeArray);

        }
    }

    public void  editText(){

        name = findViewById(R.id.name);
        firstNumber = findViewById(R.id.number1);
        secondNumber =findViewById(R.id.number2);
        adress =findViewById(R.id.adress);
        description = findViewById(R.id.brief_description);

        if (name.getText().toString().length()<3){

            name.setError("Please put a valid name");
        }else if (firstNumber.getText().toString().length()<11){

            firstNumber.setError("Write a valid number 1");
        }else if (adress.getText().toString().length()<6){

            adress.setError("Write a valid address");
        }else if (select_type.getText().toString().equals("Select Type")){

            Toast.makeText(this, "Select Type", Toast.LENGTH_SHORT).show();

        }else
        {

            strSelectType = select_type.getText().toString();

            strName = name.getText().toString();
            strFirstNumber = firstNumber.getText().toString();
            if (secondNumber.getText().toString().length()<10){

                strSecondNumber = "null";
            }else {

                strSecondNumber = secondNumber.getText().toString();
            }

            strAdress = adress.getText().toString();

            if (description.getText().toString().length()<5){

                strDescription = "null";
            }else {

                strDescription = description.getText().toString();
            }

            name.setText("");
            firstNumber.setText("");
            secondNumber.setText("");
            adress.setText("");
            description.setText("");
            Toast.makeText(this, "Thank you dear", Toast.LENGTH_SHORT).show();
            Toast.makeText(this, "Your number is added as pending", Toast.LENGTH_SHORT).show();
            Toast.makeText(this, "After review by admin", Toast.LENGTH_SHORT).show();
            Toast.makeText(this, "It will add to The Bhakkar App", Toast.LENGTH_SHORT).show();

        }






    }
}
