package aqdeveloper.thebhakkar.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import aqdeveloper.thebhakkar.Dialogs.ProDialog;
import aqdeveloper.thebhakkar.Http.AsyncTaskListener;
import aqdeveloper.thebhakkar.Http.HttpAsyncRequest;
import aqdeveloper.thebhakkar.Http.TaskResult;
import aqdeveloper.thebhakkar.Parser.AdShopParser;
import aqdeveloper.thebhakkar.Parser.BaseParser;
import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.Url.Constant;

public class AddPendingShop extends AppCompatActivity implements BaseParser {

    EditText nameineng, nameinurdu, ownername, shopaddress, phonenumber, pincode;
    Button addshopbtn;
    Spinner shoptypespinner;
    private ArrayList<String> shoptypelist = new ArrayList<>();
    private ArrayAdapter<String> shoptypeadapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pending_shop);


        init();
        // get values for shop type from server
        getShoptypeFromServer();
        // adapter for shop type spinner
        shoptypeadapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, shoptypelist);

        addshopbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean check = true;
                if (nameineng.getText().length() == 0) {
                    check = false;
                    nameineng.setError("Cannot be EMPTY");
                }
                if (nameinurdu.getText().length() == 0) {
                    check = false;
                    nameinurdu.setError("Cannot be EMPTY");
                }
                if (shoptypespinner.getSelectedItemPosition() == 0) {
                    check = false;
                    Toast.makeText(AddPendingShop.this, "Please Select Shop Type", Toast.LENGTH_SHORT).show();
                }


                if (ownername.getText().length() == 0) {
                    check = false;
                    ownername.setError("Cannot be EMPTY");
                }
                if (shopaddress.getText().length() == 0) {
                    check = false;
                    shopaddress.setError("Cannot be EMPTY");
                }


                if (phonenumber.getText().length() == 0) {
                    check = false;
                    phonenumber.setError("Cannot be EMPTY");
                }
                if (pincode.getText().length() == 0) {
                    check = false;
                    pincode.setError("Cannot be EMPTY");
                }
                if (check) {
                    // sending data to server to post ads
                    sendDataToServer();
                }


            }
        });

    }

    private void init() {

        nameineng = (EditText) findViewById(R.id.shopnameenglish);
        nameinurdu = (EditText) findViewById(R.id.shopnameurdu);
        ownername = (EditText) findViewById(R.id.shopownername);

        shopaddress = (EditText) findViewById(R.id.shopaddress);
        phonenumber = (EditText) findViewById(R.id.phonenumber);

        pincode = (EditText) findViewById(R.id.pincode);


        addshopbtn = (Button) findViewById(R.id.addbtn);
        shoptypespinner = (Spinner) findViewById(R.id.shoptypeSpinner);


    }

    private void sendDataToServer() {
        HttpAsyncRequest request = new HttpAsyncRequest(this, Constant.ADD_PENDING_SHOP_URl, HttpAsyncRequest.RequestType.POST, new AdShopParser(), listenerforshop);
        request.addParam("name", nameineng.getText().toString());
        request.addParam("shop_name_in_urdu", nameinurdu.getText().toString());
        request.addParam("shop_phone_number", phonenumber.getText().toString());
        request.addParam("shop_owner_name", ownername.getText().toString());
        request.addParam("shop_address", shopaddress.getText().toString());
        request.addParam("shop_type", shoptypelist.get(shoptypespinner.getSelectedItemPosition()));
        request.addParam("password", pincode.getText().toString());


        request.execute();
        myProgressDialog = new ProDialog(AddPendingShop.this);
        myProgressDialog.setCanceledOnTouchOutside(false);
        myProgressDialog.show();
    }

    AsyncTaskListener listenerforshop = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {


            try {
                myProgressDialog.dismiss();
            } catch (Exception e) {

            }
            if (result.isSuccess()) {
                if (result.getMessage().equals("false")) {
                    nameineng .getText().clear();
                    nameinurdu .getText().clear();
                    ownername .getText().clear();

                    shopaddress .getText().clear();
                    phonenumber .getText().clear();

                    pincode .getText().clear();


                    shoptypespinner.setSelection(0);

                    Toast.makeText(AddPendingShop.this, "Shop Added SuccessFully", Toast.LENGTH_SHORT).show();
                }
                else  if (result.getMessage().equals("phone")) {

                    phonenumber.setError("Already Exsist Enter Antother one");
                }
                else if (result.getMessage().equals("name")) {

                    nameineng.setError("Already Exsist Enter Antother one");
                    Toast.makeText(AddPendingShop.this, "Name should be unique", Toast.LENGTH_SHORT).show();
                }

                else {
                    Toast.makeText(AddPendingShop.this, "There is some error", Toast.LENGTH_SHORT).show();

                }


            } else {

                Toast.makeText(AddPendingShop.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();

            }
        }


    };


    public void getShoptypeFromServer() {
        HttpAsyncRequest request = new HttpAsyncRequest(this, Constant.GET_SHOPTYPES_URL, HttpAsyncRequest.RequestType.POST, this, listener);
        request.execute();
        myProgressDialog = new ProDialog(AddPendingShop.this);
        myProgressDialog.setCanceledOnTouchOutside(false);
        myProgressDialog.show();

    }

    private ProDialog myProgressDialog;
    AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {


            try {
                myProgressDialog.dismiss();
            } catch (Exception e) {

            }
            if (result.isSuccess()) {


            } else {

                Toast.makeText(AddPendingShop.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();

            }
        }


    };

    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {
            result.success(true);
            try {
                JSONObject obj = new JSONObject(response);
                if (obj.optString("error").equals("false")) {

                    JSONArray array = obj.optJSONArray("name_in_english");
                    if (array != null) {
                        shoptypelist.add(0, "Select Shop Type");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject rs = array.getJSONObject(i);

                            shoptypelist.add(rs.optString("name_in_english"));


                        }

                        shoptypeadapter.notifyDataSetChanged();
                        shoptypespinner.setAdapter(shoptypeadapter);
                    }


                } else {
                    Toast.makeText(this, "There is some error", Toast.LENGTH_SHORT).show();
                }

                result.success(true);
            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);
            }


        }
        return result;
    }
}
