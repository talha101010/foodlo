package aqdeveloper.thebhakkar.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import aqdeveloper.thebhakkar.Adapters.RecyclerViewAdaptorDetailsOfShops;
import aqdeveloper.thebhakkar.Adapters.ShopAdsParser;
import aqdeveloper.thebhakkar.DataProviders.Doctor_Data_provider;
import aqdeveloper.thebhakkar.DataProviders.ShopDetailsHolder;
import aqdeveloper.thebhakkar.Http.AsyncTaskListener;
import aqdeveloper.thebhakkar.Http.HttpAsyncRequest;
import aqdeveloper.thebhakkar.Http.TaskResult;
import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.Url.Constant;

public class ShopesDetail extends AppCompatActivity {

    public static final  String PRODUCT_HEADING = "product_heading";
    public static final String PRODUCT_DETAIL = "product_detail";
    public static final String PRODUCT_NUMBER ="product_number";
    TextView property_detail_number,detail_property_urdu_name,onwer_name,propert_detail_eng_name,property_adress,icontextview;
    String property_cell_no_str,urdu_name_str,onwer_name_str,property_detail_eng_name_str,property_adress_str;
    LinearLayout call_to_doctor_linear_layout;
    Button share_info_btn;
    String json_string;
    JSONObject jsonObject;
    JSONArray jsonArray;
    Doctor_Data_provider doctor_data_provider;
    AlertDialog alertDialog;
    String JSON_STRING,jsonstr;
    ProgressDialog dowloading;
    String login_url,op,op2;
    Context context = this;
    String[] product_heading_list,product_detail_list,product_number_list,property_eng_list,adress_list;
    String product_heading,product_detail,product_number,adress,contact_number,id,icon_letter_str;
    RecyclerView adsRecyclerView;
    private Activity activity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopes_detail);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        adsRecyclerView=findViewById(R.id.recyclerview_shopads);
        activity = this;

        share_info_btn =  findViewById(R.id.share_info_btn);
        share_info_btn.setMinimumWidth(width/2);
        call_to_doctor_linear_layout =  findViewById(R.id.call_to_doctor_linear_layout);
        call_to_doctor_linear_layout.setMinimumWidth(width/2);
        call_to_doctor_linear_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel",property_cell_no_str,null));
                startActivity(callIntent);
            }
        });

        property_detail_number =  findViewById(R.id.shopes_cell_number);
        detail_property_urdu_name  =  findViewById(R.id.shopes_urdu_name);
        onwer_name =  findViewById(R.id.shopes_owner_name);
        propert_detail_eng_name =  findViewById(R.id.shopes_name_in_eng);
        property_adress =  findViewById(R.id.shopes_adress);
        icontextview =  findViewById(R.id.icon_textview);

        Intent intent = getIntent();
        property_cell_no_str = intent.getStringExtra(BKRShopsList.PROPERTY_CONTACT_NO);
        urdu_name_str = intent.getStringExtra(BKRShopsList.PROPERTY_NAME_URDU);
        onwer_name_str =intent.getStringExtra(BKRShopsList.PROPERTY_OWNER_NAME);
        property_detail_eng_name_str =intent.getStringExtra(BKRShopsList.PROPERTY_NAME_ENG);
        property_adress_str =intent.getStringExtra(BKRShopsList.PROPERTY_ADRESS);
        char icon_letter = property_detail_eng_name_str.charAt(0);
        icon_letter_str = String.valueOf(icon_letter);
        icontextview.setText(icon_letter_str);
        onwer_name.setText(onwer_name_str);
        property_detail_number.setText(property_cell_no_str);
        detail_property_urdu_name.setText(urdu_name_str);
        propert_detail_eng_name.setText(property_detail_eng_name_str);
        property_adress.setText(property_adress_str);

        share_info_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = urdu_name_str+"\n\nپروپرائٹر:"+onwer_name_str+"\n\nفون نمبر: "+property_cell_no_str+"\n\n\n"+"Provided by The Bhakkar android application Download from play store, link is given below"+"\n\n"+"https://play.google.com/store/apps/details?id=aqdeveloper.thebhakkar&hl=en";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share Information"));
            }
        });

        getShopRelatedAds(property_detail_eng_name_str,property_cell_no_str);


    }

    public  void getShopRelatedAds(String name, String phonenumber) {
        HttpAsyncRequest request = new HttpAsyncRequest(activity, Constant.GET_SHOP_ADS_URL, HttpAsyncRequest.RequestType.POST, new ShopAdsParser(activity), listener);
        request.addParam("name",name);
        request.addParam("number",phonenumber);
        request.execute();


    }

    private RecyclerViewAdaptorDetailsOfShops recyclerViewAdaptor;
    public AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            try {
              //  dialogboxforshop.dismiss();
            } catch (Exception e) {


            }
            if (result.isSuccess()) {


                ArrayList<ShopDetailsHolder> list = (ArrayList<ShopDetailsHolder>) result.getData();
                if(list!= null) {
                    recyclerViewAdaptor = new RecyclerViewAdaptorDetailsOfShops(activity, list);
                    adsRecyclerView.setAdapter(recyclerViewAdaptor);
                }else{
                    Toast.makeText(activity, "No Data", Toast.LENGTH_SHORT).show();
                }

            } else {

                Toast.makeText(activity, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();

            }
        }


    };



}
