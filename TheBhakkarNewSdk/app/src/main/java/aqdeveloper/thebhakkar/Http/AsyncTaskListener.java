package aqdeveloper.thebhakkar.Http;

public interface AsyncTaskListener {

    public void onComplete(TaskResult result);

}