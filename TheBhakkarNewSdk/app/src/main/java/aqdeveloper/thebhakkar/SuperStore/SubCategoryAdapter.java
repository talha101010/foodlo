package aqdeveloper.thebhakkar.SuperStore;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import aqdeveloper.thebhakkar.DrawerActivities.FoodItemsActivity;
import aqdeveloper.thebhakkar.DrawerActivities.SubCategoryActivity;
import aqdeveloper.thebhakkar.FastFood.MenuFoodItems;
import aqdeveloper.thebhakkar.FastFood.MyProgressDialog;
import aqdeveloper.thebhakkar.Http.AsyncTaskListener;
import aqdeveloper.thebhakkar.Http.HttpAsyncRequest;
import aqdeveloper.thebhakkar.Http.TaskResult;
import aqdeveloper.thebhakkar.Parser.BaseParser;
import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.Url.Constant;

public class SubCategoryAdapter  extends RecyclerView.Adapter<SubCategoryAdapter.MyViewHolder> implements BaseParser {

    Context c;
    ArrayList<MenuFoodItems> categorieslist;
    private MyProgressDialog myProgressDialog;
    public static  ArrayList<MenuFoodItems> fooditems;
    int index;


    public SubCategoryAdapter(Context context,  ArrayList<MenuFoodItems> categorieslist) {
        c = context;
        this.categorieslist = categorieslist;


    }

    public class MyViewHolder extends RecyclerView.ViewHolder {



        TextView categoryname,urduname;
        ImageView categoryimage;
        LinearLayout lay ;
        ProgressBar progressBar;
        public MyViewHolder(View v) {
            super(v);

            categoryimage = (ImageView) v.findViewById(R.id.subcategoryimage);
            categoryname = (TextView) v.findViewById(R.id.subcategoryname);
            urduname = (TextView) v.findViewById(R.id.urdu);
            lay = (LinearLayout)v.findViewById(R.id.lay);
            progressBar = (ProgressBar)v.findViewById(R.id.progress);

        }






    }


    @Override
    public SubCategoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_sub_category, parent, false);
        SubCategoryAdapter.MyViewHolder myViewHolder = new SubCategoryAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final SubCategoryAdapter.MyViewHolder holder, final int position) {

//        Typeface tf = Typeface.createFromAsset(c.getAssets(), "fonts/burnstown_dam.otf");
//        holder.urduname.setTypeface(tf);


        holder.categoryname.setText(categorieslist.get(position).getProductname());
        holder.urduname.setText(categorieslist.get(position).getNameinUrdu());


        Glide.with(c)

                .load(categorieslist.get(position).getImagepath())
                .dontAnimate()

                .placeholder(R.drawable.logoone)

                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder. progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.categoryimage);


        holder.lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index = position;
                send_sub_Categoryid_to_Server(categorieslist.get(position).getDealid());


            }
        });



    }

    @Override
    public int getItemCount() {
        return categorieslist.size();
    }
    private void send_sub_Categoryid_to_Server(String id) {

        HttpAsyncRequest request = new HttpAsyncRequest(c, Constant.GET_MENUURL, HttpAsyncRequest.RequestType.POST, this, listenerfor_food);
        request.addParam("subcategoryid", id);
        request.addParam("maincategoryid", SubCategoryActivity.categoryid);
        request.execute();

        myProgressDialog = new MyProgressDialog(c);
        myProgressDialog.show();
        myProgressDialog.setCanceledOnTouchOutside(false);


    }

    AsyncTaskListener listenerfor_food = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            if (result.isSuccess()) {

                if (result.getMessage().equals("true")) {

                    myProgressDialog.dismiss();
                    fooditems = (ArrayList<MenuFoodItems>) result.getData();
                    c.startActivity(new Intent(c, FoodItemsActivity.class)
                            .putExtra("categoryname", categorieslist.get(index).getProductname())
                            .putExtra("categoryid", SubCategoryActivity.categoryid)
                            .putExtra("subcategoryid",categorieslist.get(index).getDealid())


                    );




                } else if (result.getMessage().equals("false")) {
                    Toast.makeText(c, "Under Process. It will come soon.", Toast.LENGTH_SHORT).show();
                    myProgressDialog.dismiss();
                }


            } else {

                Toast.makeText(c, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();
                myProgressDialog.dismiss();


            }
        }


    };
    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {

            try {
                result.success(true);
                JSONObject obj = new JSONObject(response);
                if (obj.optString("error").equals("0")) {
                    result.setMessage("true");

                    JSONArray array = obj.optJSONArray("menu");
                    ArrayList<MenuFoodItems> list = new ArrayList<>(array.length());
                    if (array.length() != 0 && array != null) {
                        for (int i = 0; i < array.length(); i++) {

                            JSONObject items = array.getJSONObject(i);
                            MenuFoodItems food = new MenuFoodItems();
                            food.setProductname(items.optString("menu_name"));
                            food.setDealid(items.optString("id"));


                            list.add(food);
                        }


                        result.setData(list);
                    }
                    else
                    {
                        result.setMessage("false");

                    }

                } else if (obj.optString("error").equals("2")) {
                    result.setMessage("false");
                }

            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);

            }


        }
        return result;
    }

}




