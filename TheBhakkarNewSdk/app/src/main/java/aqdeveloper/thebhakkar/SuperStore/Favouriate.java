package aqdeveloper.thebhakkar.SuperStore;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import aqdeveloper.thebhakkar.DrawerActivities.LoginPanel;
import aqdeveloper.thebhakkar.DrawerActivities.SignupActivity;
import aqdeveloper.thebhakkar.FastFood.BillModel;
import aqdeveloper.thebhakkar.FastFood.FoodItemHolder;
import aqdeveloper.thebhakkar.FastFood.MyProgressDialog;
import aqdeveloper.thebhakkar.FastFood.SizeObject;
import aqdeveloper.thebhakkar.Http.AsyncTaskListener;
import aqdeveloper.thebhakkar.Http.HttpAsyncRequest;
import aqdeveloper.thebhakkar.Http.TaskResult;
import aqdeveloper.thebhakkar.Parser.BaseParser;
import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.Url.Constant;

public class Favouriate extends AppCompatActivity  implements BaseParser {

    private MyProgressDialog myProgressDialog;
    private SharedPreferences preferences;
    FooitemAdapter fooditemadapter;
    ListView fooditemlistview;
    TextView nomsg;
    public  static  Favouriate instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favouriate);

        preferences = getSharedPreferences("EStore", Context.MODE_PRIVATE);
        fooditemlistview = findViewById(R.id.list);
        nomsg = findViewById(R.id.nomsg) ;getFoodItems();
        instance = this;
    }

    public  void getFoodItems() {
        HttpAsyncRequest request = new HttpAsyncRequest(Favouriate.this, Constant.getfavouriateurl, HttpAsyncRequest.RequestType.POST,this, listenerr);
        request.addParam("userid", preferences.getString("userid",""));


        request.execute();
        myProgressDialog = new MyProgressDialog(Favouriate.this);
        myProgressDialog.show();
        myProgressDialog.setCanceledOnTouchOutside(false);


    }


    public AsyncTaskListener listenerr = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            if (result.isSuccess()) {
                try{myProgressDialog.dismiss();}catch (Exception e){}
                if (result.getMessage().equals("true")) {
                    fooditemadapter =    new FooitemAdapter(Favouriate.this, (ArrayList<FoodItemHolder>) result.getData());
                    fooditemlistview.setAdapter(fooditemadapter);
                    fooditemlistview.setVisibility(View.VISIBLE);

                } else if (result.getMessage().equals("false")) {

                    nomsg.setVisibility(View.VISIBLE);
                    // Toast.makeText(activity, "This Menu is not active yet", Toast.LENGTH_SHORT).show();
                }


            } else {

                Toast.makeText(Favouriate.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();

            }
        }


    };
    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {

            try {
                result.success(true);
                JSONObject obj = new JSONObject(response);
                if (obj.optString("error").equals("0")) {
                    result.setMessage("true");

                    JSONArray array = obj.optJSONArray("favourites");
                    ArrayList<FoodItemHolder> list = new ArrayList<>(array.length());
                    ArrayList<SizeObject> temp = new ArrayList<>();

                    if (array.length() != 0 && array!=null) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject items = array.getJSONObject(i);
                            FoodItemHolder food = new FoodItemHolder();
                            food.setImagepath(items.optString("url"));
                            food.setProductname(items.optString("item_name"));
                            food.setDealid(items.optString("product_id"));
                            food.setProductdescription(items.optString("item_unit"));
                            food.setPrice(items.optString("item_price"));
                            food.setStorename(items.optString("store_name"));
                            food.setIsfavouriate(1);
                            food.setPurchaseprice(items.optString("purchase_price"));

                            list.add(food);
                        }
                        result.setData(list);
                    }
                    else{
                        result.setMessage("false");

                    }

                } else if (obj.optString("error").equals("true")) {
                    result.setMessage("false");
                }

            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);

            }


        }
        return result;
    }

    @Override
    protected void onResume() {
        super.onResume();
        HandleCartNumber();
    }

    public  void HandleCartNumber()
    {

        ArrayList<BillModel> arrayList = null;

        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        TextView textView  = (TextView) findViewById(R.id.itemnumber);
        LinearLayout cartlayout = (LinearLayout)findViewById(R.id.cartlayout);
        LinearLayout accountlayout = (LinearLayout)findViewById(R.id.accountlayout);
        accountlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!preferences.getString("userid","").equals(""))
                    startActivity(new Intent(getApplicationContext(), SignupActivity.class).putExtra("update", 1));
                else
                    startActivity(new Intent(getApplicationContext(), LoginPanel.class));

            }
        });

        LinearLayout searchlay= (LinearLayout)findViewById(R.id.searchlayout) ;
        searchlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),SearchActivity.class));
            }
        });

        String json = appSharedPrefs.getString("cart", "");
        if (!json.equals("")) {
            arrayList = gson.fromJson(json, type);

            textView.setText(arrayList.size()+"");
            textView.setVisibility(View.VISIBLE);
            cartlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(),CartActivity.class));
                }
            });
        }
        else{
            textView.setVisibility(View.GONE);
            cartlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(), "Nothing in the Cart Yet", Toast.LENGTH_SHORT).show();
                }
            });
        }
        LinearLayout homelayout = (LinearLayout)findViewById(R.id.homelayout);

        homelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
