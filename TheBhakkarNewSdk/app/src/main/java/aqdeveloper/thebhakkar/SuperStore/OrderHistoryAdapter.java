package aqdeveloper.thebhakkar.SuperStore;

import android.content.Context;
import android.content.DialogInterface;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import android.widget.ImageView;
import android.widget.TextView;






import java.util.ArrayList;

import aqdeveloper.thebhakkar.DrawerActivities.OrderHistoryDetails;
import aqdeveloper.thebhakkar.Helper.HistoryModel;
import aqdeveloper.thebhakkar.R;


/**
 * Created by Hamza on 12/23/17.
 */

public class OrderHistoryAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    ArrayList<HistoryModel> foodlist;

    public OrderHistoryAdapter(Context mainActivity, ArrayList<HistoryModel> listitems) {
        // TODO Auto-generated constructor stub
        context = mainActivity;
        this.foodlist = listitems;

        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return foodlist.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView billid,totalbill,date;
        View approveview,delieveryview,delieveredview;
        ImageView approveimage,deliveryimage,deliveredimage;
        CardView card_view;


        public Holder(View v) {
            billid = (TextView)v.findViewById(R.id.billid);
            totalbill = (TextView)v.findViewById(R.id.totalbill);
            date = (TextView)v.findViewById(R.id.date);

            approveview = (View)v.findViewById(R.id.approveview);
            delieveryview = (View)v.findViewById(R.id.deliveryview);
            delieveredview = (View)v.findViewById(R.id.delieveredview);

            approveimage = (ImageView) v.findViewById(R.id.approveimge);
            deliveryimage = (ImageView) v.findViewById(R.id.deliveryimage);
            deliveredimage = (ImageView) v.findViewById(R.id.deliveredimage);
            card_view = (CardView) v.findViewById(R.id.card_view);



        }
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final OrderHistoryAdapter.Holder holder;

        View rowView;
        rowView = inflater.inflate(R.layout.row_orderhistory, null);
        holder = new OrderHistoryAdapter.Holder(rowView);


        holder.billid.setText(foodlist.get(position).getId());
        holder.date.setText(foodlist.get(position).getDate());
        holder.totalbill.setText("Rs "+foodlist.get(position).getBill());


        switch (foodlist.get(position).getStatus())
        {
            case "Approve":
                holder.approveimage.setImageResource(R.drawable.approvecolor);


                if( android.os.Build.VERSION.SDK_INT <23)
                    holder. approveview.setBackgroundColor(context.getResources().getColor(R.color.main_color));
                else
                    holder.approveview.setBackgroundColor(context.getResources().getColor(R.color.main_color,null));


                holder.deliveryimage.setImageResource(R.drawable.deliverycolor);


                if( android.os.Build.VERSION.SDK_INT <23)
                    holder. delieveryview.setBackgroundColor(context.getResources().getColor(R.color.main_color));
                else
                    holder.delieveryview.setBackgroundColor(context.getResources().getColor(R.color.main_color,null));
                break;
            case "Rejected":
                holder.approveimage.setImageResource(R.drawable.reject);


                if( android.os.Build.VERSION.SDK_INT <23)
                    holder. approveview.setBackgroundColor(context.getResources().getColor(R.color.main_color));
                else
                    holder.approveview.setBackgroundColor(context.getResources().getColor(R.color.main_color,null));

                break;
            case "Delivered":
                holder.approveimage.setImageResource(R.drawable.approvecolor);


                if( android.os.Build.VERSION.SDK_INT <23)
                    holder. approveview.setBackgroundColor(context.getResources().getColor(R.color.main_color));
                else
                    holder.approveview.setBackgroundColor(context.getResources().getColor(R.color.main_color,null));


                holder.deliveryimage.setImageResource(R.drawable.deliverycolor);


                if( android.os.Build.VERSION.SDK_INT <23)
                    holder. delieveryview.setBackgroundColor(context.getResources().getColor(R.color.main_color));
                else
                    holder.delieveryview.setBackgroundColor(context.getResources().getColor(R.color.main_color,null));
                holder.deliveredimage.setImageResource(R.drawable.delieveredcolor);


                if( android.os.Build.VERSION.SDK_INT <23)
                    holder. delieveredview.setBackgroundColor(context.getResources().getColor(R.color.main_color));
                else
                    holder.delieveredview.setBackgroundColor(context.getResources().getColor(R.color.main_color,null));

                break;





        }
        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context,OrderHistoryDetails.class);
                i.putExtra("orderid",foodlist.get(position).getId());
                context.startActivity(i);

            }
        });




        return rowView;
    }


}