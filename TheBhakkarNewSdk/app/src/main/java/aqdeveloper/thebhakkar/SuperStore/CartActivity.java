package aqdeveloper.thebhakkar.SuperStore;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import aqdeveloper.thebhakkar.DrawerActivities.LoginPanel;
import aqdeveloper.thebhakkar.FastFood.BillModel;
import aqdeveloper.thebhakkar.FastFood.MyProgressDialog;
import aqdeveloper.thebhakkar.Helper.TaxValuesClass;
import aqdeveloper.thebhakkar.Http.AsyncTaskListener;
import aqdeveloper.thebhakkar.Http.HttpAsyncRequest;
import aqdeveloper.thebhakkar.Http.TaskResult;
import aqdeveloper.thebhakkar.Parser.BaseParser;
import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.Url.Constant;

public class CartActivity extends AppCompatActivity implements BaseParser {

    public  static    SharedPreferences preferences;
    public static ListView listView;
    public  static   Button checkout;
    private MyProgressDialog myProgressDialog;
    RelativeLayout layout;
    public  static    ArrayList<TaxValuesClass> taxaraylist;
    public  static   boolean istoCalculatetax;
    public  static Activity activity;
    private static SharedPreferences appSharedPrefs;
    String notetext,charges,urgentCharges,urgesntnote,regularNote,lessThanMini,lessThanMini50,lessThanMini100;
    public static ImageView arrowgobelow;
    int minimumShopping;
    ArrayList<BillModel> arrayList;
    DatabaseReference deliveryDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        init();
        activity = this;


        // get_cart();
        //get_Taxes();
        getCharges();
        if(preferences.getString("userid","").equals(""))
            checkout.setText("Sign In to CheckOut");

        arrayList = null;

        Gson gson = new Gson();
        final Type type = new TypeToken<List<BillModel>>() {
        }.getType();
        appSharedPrefs= PreferenceManager
                .getDefaultSharedPreferences(this);
        String json = appSharedPrefs.getString("cart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);


        listView.setAdapter(new CartAdapter(this,arrayList));
        handlearrow();


        calculateBill();


        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //////////////////////////


                final AlertDialog.Builder alertdialog = new AlertDialog.Builder(CartActivity.this);
                final LayoutInflater inflater = getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.delivery_type_dialog, null);
                final AlertDialog dialog;
                final Button regularDeliveryBtn = dialogView.findViewById(R.id.regularDeliveryBtn);
                final Button urgentDeliveryBtn = dialogView.findViewById(R.id.urgentDeliveryBtn);
                final  TextView urgentTextview = dialogView.findViewById(R.id.urgentTextview);
                final  TextView regularTextview = dialogView.findViewById(R.id.regularTextview);
                regularTextview.setText(regularNote);

                urgentTextview.setText(urgesntnote);

                regularDeliveryBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ArrayList<BillModel> arrayList = null;

                        String json = appSharedPrefs.getString("cart", "");
                        Gson gson = new Gson();

                        if (!json.equals(""))
                            arrayList = gson.fromJson(json, type);
                            if (!preferences.getString("userid", "").equals(""))
                                if (arrayList != null && !(arrayList.size() <= 0)) {


                                    if (Constant.totalbill >= minimumShopping) {
                                        startActivity(new Intent(CartActivity.this, CheckoutSecondStep.class)
                                                .putExtra("note", notetext)
                                                .putExtra("charges", charges)
                                                .putExtra("ctype","r")


                                        );


                                    } else {

                                        if (Constant.totalbill < minimumShopping) {

                                            if (Constant.totalbill<50){

                                                startActivity(new Intent(CartActivity.this, CheckoutSecondStep.class)
                                                        .putExtra("note", "Delivery Charges " + lessThanMini50 + "\nNote: Free delivery on order above Rs." + minimumShopping + "")
                                                        .putExtra("charges", lessThanMini50)
                                                        .putExtra("ctype", "r")


                                                );


                                            }else if (Constant.totalbill<100){

                                                startActivity(new Intent(CartActivity.this, CheckoutSecondStep.class)
                                                        .putExtra("note", "Delivery Charges " + lessThanMini100 + "\nNote: Free delivery on order above Rs." + minimumShopping + "")
                                                        .putExtra("charges", lessThanMini100)
                                                        .putExtra("ctype", "r")


                                                );


                                            }else {

                                                startActivity(new Intent(CartActivity.this, CheckoutSecondStep.class)
                                                        .putExtra("note", "Delivery Charges " + lessThanMini + "\nNote: Free delivery on order above Rs." + minimumShopping + "")
                                                        .putExtra("charges", lessThanMini)
                                                        .putExtra("ctype", "r")


                                                );

                                            }

                                        }

                                    }



//
                                }
                             else
                                Toast.makeText(CartActivity.this, "Please Do Shopping to Continue", Toast.LENGTH_SHORT).show();

                        else


                            activity.startActivity(new Intent(activity, LoginPanel .class));






                    }
                });

                urgentDeliveryBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        ArrayList<BillModel> arrayList = null;

                        String json = appSharedPrefs.getString("cart", "");
                        Gson gson = new Gson();

                        if (!json.equals(""))
                            arrayList = gson.fromJson(json, type);
                        if (!preferences.getString("userid", "").equals(""))
                            if (arrayList != null && !(arrayList.size() <= 0)) {


                                if (Constant.totalbill >= minimumShopping) {
                                    startActivity(new Intent(CartActivity.this, CheckoutSecondStep.class)
                                            .putExtra("note", "Delivery Charges  Rs." + urgentCharges + "")
                                            .putExtra("charges", urgentCharges)
                                            .putExtra("ctype","u")
                                            .putExtra("notesdelivery",urgesntnote)


                                    );


                                } else {

                                    if (Constant.totalbill < minimumShopping) {

                                        startActivity(new Intent(CartActivity.this, CheckoutSecondStep.class)
                                                .putExtra("note", "Delivery Charges  Rs." + urgentCharges + "")
                                                .putExtra("charges", urgentCharges)
                                                .putExtra("ctype","u")
                                                .putExtra("notesdelivery",urgesntnote)


                                        );

                                    }

                                }



//
                            }
                            else
                                Toast.makeText(CartActivity.this, "Please Do Shopping to Continue", Toast.LENGTH_SHORT).show();

                        else


                            activity.startActivity(new Intent(activity, LoginPanel .class));


                    }
                });

                alertdialog.setView(dialogView);
                dialog = alertdialog.create();
                dialog.show();





//////////////////////////////
        }

        });

        arrowgobelow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listView.setSelection(arrayList.size()-1);
            }
        });


    }

    private void getCharges()

    {

        deliveryDb = FirebaseDatabase.getInstance().getReference("delivery");
        deliveryDb.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                urgentCharges = dataSnapshot.child("charge").getValue().toString();
                urgesntnote = dataSnapshot.child("note").getValue().toString();
                regularNote =dataSnapshot.child("regular").getValue().toString();
                lessThanMini =dataSnapshot.child("mini").getValue().toString();
                lessThanMini50 = dataSnapshot.child("mini50").getValue().toString();
                lessThanMini100 = dataSnapshot.child("mini100").getValue().toString();




            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        HttpAsyncRequest request = new HttpAsyncRequest(CartActivity.this, Constant.get_Charges_url, HttpAsyncRequest.RequestType.POST, this, listener);



        request.execute();
        myProgressDialog = new MyProgressDialog(CartActivity.this);
        myProgressDialog.show();
        myProgressDialog.setCanceledOnTouchOutside(false);
    }


    AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            try {
                myProgressDialog.dismiss();
            }catch (Exception e){}

            if (result.isSuccess()) {




            } else {

                Toast.makeText(CartActivity.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();

            }
        }


    };

    private void init() {

        preferences = getSharedPreferences("EStore", Context.MODE_PRIVATE);
        listView = (ListView) findViewById(R.id.list);
        checkout = (Button) findViewById(R.id.checkout);
        layout = (RelativeLayout)findViewById(R.id.out);
        arrowgobelow = (ImageView)findViewById(R.id.arrow);

    }

    public static void calculateBill() {
        ArrayList<BillModel> arrayList=null ;


        Gson gson = new Gson();
        Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        String json = appSharedPrefs.getString("cart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);




        Constant.totalbill = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            Constant.totalbill += Integer.parseInt(arrayList.get(i).getQuantity() ) *Integer.parseInt(arrayList.get(i).getProductprice() );

        }

        if(preferences.getString("userid","").equals(""))

            checkout.setText("Log in to Order = "+" "+"Rs. "+Constant.totalbill);
        else
            checkout.setText("Order Now = "+" "+"Rs. "+Constant.totalbill);


    }
    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {
            result.success(true);
            try {
                JSONObject obj = new JSONObject(response);
                notetext=obj.optString("text");
                charges  = obj.optString("charges");
                minimumShopping =Integer.parseInt( obj.optString("minshopping"));




                result.success(true);
            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);
            }


        }
        return result;
    }

    public  static void handlearrow()
    {try {


        Runnable fitsOnScreen = new Runnable() {
            @Override
            public void run() {
                int last = listView.getLastVisiblePosition();
                if(!( last< 0))
                    if (last == listView.getCount()  && listView.getChildAt(last).getBottom() <= listView.getHeight()) {
                        // It fits!
                        arrowgobelow.setVisibility(View.GONE);
                    } else {
                        arrowgobelow.setVisibility(View.VISIBLE);
                    }
            }
        };
        listView.post(fitsOnScreen);
    }catch (Exception e){}
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(preferences.getString("userid","").equals(""))
            checkout.setText("Log in to Order = "+" "+"Rs. "+Constant.totalbill);
        else
            checkout.setText("Order Now = "+" "+"Rs. "+Constant.totalbill);
    }
}
