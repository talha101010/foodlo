package aqdeveloper.thebhakkar.SuperStore;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.ServiceMan.ServiceManActivity;

public class OrderSubmitActivity extends AppCompatActivity {

    TextView billtextview, timetextview,daytextview;
    Button check_order_btn,done_btn;
    String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_submit);
       // AutomaticLocationPicker.progressDialog.dismiss();
        billtextview = findViewById(R.id.totalpricetextview);
        timetextview = findViewById(R.id.timeofdeliverytextview);
        daytextview  = findViewById(R.id.deliverydatetextview);
        check_order_btn = findViewById(R.id.myordersbtn);
        done_btn = findViewById(R.id.doneorderbtn);

        Intent intent = getIntent();

        billtextview.setText(intent.getStringExtra("bill"));
        timetextview.setText(intent.getStringExtra("time"));
        daytextview.setText(intent.getStringExtra("day"));

        type = intent.getStringExtra("a");

        if (type.equals("f")){

            check_order_btn.setVisibility(View.GONE);
        }else if (type.equals("s")){

            check_order_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent1 = new Intent(OrderSubmitActivity.this, ServiceManActivity.class);
                    startActivity(intent1);




                }
            });



        }



        done_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
