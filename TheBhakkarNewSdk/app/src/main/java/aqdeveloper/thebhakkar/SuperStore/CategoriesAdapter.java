package aqdeveloper.thebhakkar.SuperStore;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import aqdeveloper.thebhakkar.FastFood.MenuFoodItems;
import aqdeveloper.thebhakkar.R;

/**
 * Created by Hamza on 11/25/17.
 */

public class CategoriesAdapter extends BaseAdapter {
    private static Context context;
    private static LayoutInflater inflater;
    ArrayList<MenuFoodItems> categorieslist;

    public CategoriesAdapter(Context mainActivity, ArrayList<MenuFoodItems> listitems) {
        // TODO Auto-generated constructor stub
        context = mainActivity;
        this.categorieslist = listitems;


        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return categorieslist.size();

    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }


    public class Holder {
        TextView categoryname,urduname;
        ImageView categoryimage;
        ProgressBar progressBar;
        CardView cardView;

        public Holder(View v) {
            categoryimage = (ImageView) v.findViewById(R.id.categoryimage_category);
            categoryname = (TextView) v.findViewById(R.id.categoryname_category);
            urduname = (TextView) v.findViewById(R.id.categorynameurdu_category);
            progressBar = (ProgressBar)v.findViewById(R.id.progress);
            cardView = v.findViewById(R.id.card_view);

        }
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final Holder holder;

        View rowView;
        rowView = inflater.inflate(R.layout.row_category, null);
        holder = new Holder(rowView);

        holder.categoryname.setText(categorieslist.get(position).getProductname());

        holder.urduname.setText(categorieslist.get(position).getNameinUrdu());
        CardView cardView =holder.cardView;




        Glide.with(context)
                .load(categorieslist.get(position).getImagepath())
                .dontAnimate()

                .placeholder(R.drawable.logonew)


                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder. progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.categoryimage);

        return rowView;
    }


}