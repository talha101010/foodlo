package aqdeveloper.thebhakkar.SuperStore;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import aqdeveloper.thebhakkar.FastFood.BillModel;
import aqdeveloper.thebhakkar.FastFood.MyProgressDialog;
import aqdeveloper.thebhakkar.Http.AsyncTaskListener;
import aqdeveloper.thebhakkar.Http.HttpAsyncRequest;
import aqdeveloper.thebhakkar.Http.TaskResult;
import aqdeveloper.thebhakkar.Parser.BaseParser;
import aqdeveloper.thebhakkar.Parser.CheckOutParser;
import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.Url.Constant;
import aqdeveloper.thebhakkar.Url.ConstantsforShop;

public class CheckoutSecondStep extends AppCompatActivity implements BaseParser {

    private static final String TAG = "RegisterComplainAct";
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int REQUEST_CODE = 123;
    private static final int PLACE_PICKER_REQUEST = 1;
    private Boolean locationPermissionGranted = false;
    private FusedLocationProviderClient fusedLocationProviderClient;
    static Spinner date;
    static Spinner time;
    static EditText comments;
    Button checkout;
    public static ProgressDialog progressDialog;
    private String day1,time1;

    static MyProgressDialog myProgressDialog;
    static ArrayList<String> timestamp;
    static String[] days = {"Select Day","Today", "Tomorrow"};
    private static SharedPreferences appSharedPrefs;
    static SharedPreferences preferences;
    static ImageView check;
    TextView notes;
    String note,dType;
    static int charges;
    static int currenttime=0;
    static SharedPreferences.Editor editor;

    public static String time_str, date_str,adress_str,urgentNote;
    private double latitude,longitude;

    static Intent intent;
    RelativeLayout dayLayout, timeLayout;
    TextView urgentTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_second_step);
        myProgressDialog = new MyProgressDialog(CheckoutSecondStep.this);
        date = (Spinner) findViewById(R.id.date);
        time = (Spinner) findViewById(R.id.time);
        checkout = (Button) findViewById(R.id.checkout);
        comments = (EditText) findViewById(R.id.comments);
        check = (ImageView) findViewById(R.id.check);
        notes  = (TextView)findViewById(R.id.instructions);
        progressDialog = new ProgressDialog(this);
        dayLayout = findViewById(R.id.daylayout);
        timeLayout = findViewById(R.id.timeLayout);
        urgentTv = findViewById(R.id.urgentTv);

       // showGPSDisabledAlertToUser();

        if(getIntent().hasExtra("note"))
        {
            note = getIntent().getStringExtra("note");
            notes.setText(note);
            charges = Integer.parseInt(getIntent().getStringExtra("charges"));

        }

        Intent intent1 = getIntent();
        dType = intent1.getStringExtra("ctype");
        urgentNote= intent1.getStringExtra("notesdelivery");

        if (dType.equals("r")){

            timeLayout.setVisibility(View.VISIBLE);
            dayLayout.setVisibility(View.VISIBLE);
            notes.setVisibility(View.VISIBLE);
        }else if (dType.equals("u")){

            timeLayout.setVisibility(View.GONE);
            dayLayout.setVisibility(View.GONE);
            urgentTv.setVisibility(View.VISIBLE);
            urgentTv.setText(urgentNote);




        }






        Calendar c = Calendar.getInstance();
        System.out.println("Current time =&gt; "+c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        String formattedDate = df.format(c.getTime());
        String tm[] = formattedDate.split(":");
        String t = tm[0]+tm[1];
        currenttime = Integer.parseInt(t);




        preferences = getSharedPreferences("EStore", Context.MODE_PRIVATE);
        editor = preferences.edit();

        getTimeStamp();
        date.setAdapter(new ArrayAdapter<String>(this, R.layout.row_area, R.id.text, days));


        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean check = true;

                if (dType.equals("r")) {
                    if (time.getSelectedItemPosition() == 0) {
                        check = false;
                        Toast.makeText(CheckoutSecondStep.this, "Please Select Time", Toast.LENGTH_SHORT).show();
                    }
                    if (date.getSelectedItemPosition() == 0) {
                        check = false;
                        Toast.makeText(CheckoutSecondStep.this, "Please Select Date", Toast.LENGTH_SHORT).show();
                    }
                    if (check) {
                        time1 = timestamp.get(time.getSelectedItemPosition());
                        day1 = days[date.getSelectedItemPosition()];
                        getLocationPermission(time1,day1 );
//                        myProgressDialog.setCanceledOnTouchOutside(false);
//                        myProgressDialog.show();

                                            //  checkoutCart(timestamp.get(time.getSelectedItemPosition()), days[date.getSelectedItemPosition()]);
                                              checkoutCart2();

                    }

                }else if (dType.equals("u")){

                    if (check) {
//                        myProgressDialog.setCanceledOnTouchOutside(false);
//                        myProgressDialog.show();
                        time1 = "Urgent";
                        day1 = "Today";
                        getLocationPermission(time1,day1 );
                       // checkoutCart(time1, day1);
                         checkoutCart2();

                    }





                }

            }
        });


    }

    private void showGPSDisabledAlertToUser(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Goto Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                finish();
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        dialog.cancel();
                        finish();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }



    public void checkoutCart(String day,String timeofDelivery) {

        HttpAsyncRequest request = new HttpAsyncRequest(CheckoutSecondStep.this, Constant.checkout_url, HttpAsyncRequest.RequestType.POST, new CheckOutParser(), listenerbill);
        request.addParam("json_array", makeOrdersJson().toString());
        request.addParam("bill", (Constant.totalbill + charges)+"");
        request.addParam("profit",Constant.totalprofit+"");
        request.addParam("referral_coupan",preferences.getString("referralcode","null"));
        request.addParam("timestamp",day);
        request.addParam("day",timeofDelivery  );
        request.addParam("userid", preferences.getString("userid", ""));
        request.addParam("charges",charges+"");
        request.addParam("latitude",latitude+"");
        request.addParam("longitude",longitude+"");

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c);

        request.addParam("date", formattedDate);
        request.addParam("comments", comments.getText().toString());
        time_str = day;
        date_str=timeofDelivery;
        adress_str = (Constant.totalbill + charges)+"";


        request.execute();

    }
//////////////////////////////////////////////////////////////////////////

    public void checkoutCart2() {


        editor.commit();
        HttpAsyncRequest request = new HttpAsyncRequest(CheckoutSecondStep.this, ConstantsforShop.emailapiforsuperstoreorder, HttpAsyncRequest.RequestType.POST, new CheckOutParser(), listenerbill);
        request.addParam("json_array", makeOrdersJson().toString());
        request.addParam("bill", (Constant.totalbill + charges)+"");
        request.addParam("profit",Constant.totalprofit+"");
        request.addParam("timestamp", timestamp.get(time.getSelectedItemPosition()));
        request.addParam("day", days[date.getSelectedItemPosition()]);
        request.addParam("charges",charges+"");
        request.addParam("comments", comments.getText().toString());

        request.execute();

    }




    /////////////////////////////////////////////////////////////////////
    AsyncTaskListener listenerbill = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            if (result.isSuccess()) {
                try {




                    String rs = result.getMessage();
                    if (rs.equals("0")) {

                        Toast.makeText(CheckoutSecondStep.this, "Order has been submitted successfully", Toast.LENGTH_SHORT).show();
                        check.setBackgroundResource(R.drawable.checkoutthird);
                        empty();
                        myProgressDialog.dismiss();
                        intent = new Intent(CheckoutSecondStep.this,OrderSubmitActivity.class);
                        intent.putExtra("bill",adress_str);
                        intent.putExtra("time",time_str);
                        intent.putExtra("day",date_str);
                        intent.putExtra("a","s");
                        startActivity(intent);
                        finish();

                        if(CartActivity.activity !=null)
                            CartActivity.activity.finish();

                    }


                } catch (Exception e) {


                }


            } else {

                //  Toast.makeText(CheckoutSecondStep.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();
                //  myProgressDialog.dismiss();

                Toast.makeText(CheckoutSecondStep.this, "Order has been submitted successfully", Toast.LENGTH_SHORT).show();
                check.setBackgroundResource(R.drawable.checkoutthird);
                empty();
                myProgressDialog.dismiss();
                intent = new Intent(CheckoutSecondStep.this,OrderSubmitActivity.class);
                intent.putExtra("bill",adress_str);
                intent.putExtra("time",time_str);
                intent.putExtra("day",date_str);
                intent.putExtra("a","s");
                startActivity(intent);
                finish();

                if(CartActivity.activity !=null)
                    CartActivity.activity.finish();

            }


        }


    };

    private void getTimeStamp() {

        HttpAsyncRequest request = new HttpAsyncRequest(this, Constant.get_time_stamp, HttpAsyncRequest.RequestType.POST, this, listener);
        request.execute();
        myProgressDialog = new MyProgressDialog(CheckoutSecondStep.this);
        myProgressDialog.show();
        myProgressDialog.setCanceledOnTouchOutside(false);


    }


    AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            if (result.isSuccess()) {
                try {

                    myProgressDialog.dismiss();
                } catch (Exception e) {
                }


            } else {

                Toast.makeText(CheckoutSecondStep.this, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();
                myProgressDialog.dismiss();

            }
        }


    };

    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {

            try {
                result.success(true);
                JSONObject obj = new JSONObject(response);
                if (obj.optString("error").equals("false")) {
                    JSONArray array = obj.optJSONArray("timestamp");
                    if (array != null) {

                        timestamp = new ArrayList<>();
                        timestamp.add(  0,"Select Time");

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject time = array.getJSONObject(i);
                            if(currenttime !=0) {
                                String tm[]=  time.optString("start").split(":");
                                String t = tm[0]+tm[1];
                                int tt =  Integer.parseInt(t);

                                if(false)
                                {
                                    try {
                                        final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                                        Date dateObj = sdf.parse(time.optString("start"));
                                        System.out.println(dateObj);
                                        String st =  new SimpleDateFormat("K:mm a").format(dateObj);

                                        dateObj = sdf.parse(time.optString("end"));
                                        System.out.println(dateObj);
                                        String ed =  new SimpleDateFormat("K:mm a").format(dateObj);

                                        timestamp .add( st+" to "+ed);


                                    } catch (final ParseException e) {
                                        e.printStackTrace();
                                    }

                                }
                                else{

                                    timestamp.add( time.optString("start")+" to " +time.optString("end"));

                                }

                            }
                            else{
                                timestamp.add( time.optString("start")+" to " +time.optString("end"));

                            }






                        }


                        time.setAdapter(new ArrayAdapter<String>(this, R.layout.row_area, R.id.text, timestamp));
                    } else
                        Toast.makeText(this, "Time Stamp Not Availiable", Toast.LENGTH_SHORT).show();


                }
            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);

            }


        }
        return result;
    }

    private JSONArray makeOrdersJson() {
        ArrayList<BillModel> arrayList = null;
        appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(CheckoutSecondStep.this);
        String json = appSharedPrefs.getString("cart", "");
        Gson gson = new Gson();
        final Type type = new TypeToken<List<BillModel>>() {
        }.getType();

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);


        JSONArray jsonArray = new JSONArray();

        for (int i = 0; i < arrayList.size(); i++) {
            JSONObject obj = new JSONObject();

            try {
                obj.put("price", arrayList.get(i).getProductprice());
                obj.put("quantity", arrayList.get(i).getQuantity());
                obj.put("id", arrayList.get(i).getFooditem_id());
                obj.put("url", arrayList.get(i).getImagepath());
                obj.put("name", arrayList.get(i).getName());
                obj.put("store_name",arrayList.get(i).getStorename());
                obj.put("unit",arrayList.get(i).getUnit());


                jsonArray.put(obj);

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }


        return jsonArray;


    }

    public static boolean empty() {
        ArrayList<BillModel> arrayList = null;

        Gson gson = new Gson();




        arrayList = new ArrayList<>();


        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("cart", jsons);
        prefsEditor.remove("cart");
        prefsEditor.commit();
        return true;



    }

    public void getLocationPermission(String day,String timeofDelivery){
        Log.d(TAG,"getLocationPermission : getting location permission");
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

        if (ActivityCompat.checkSelfPermission(this.getApplicationContext(),FINE_LOCATION)== PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.checkSelfPermission(this.getApplicationContext(),COARSE_LOCATION)== PackageManager.PERMISSION_GRANTED){
                locationPermissionGranted = true;
                myProgressDialog.setCanceledOnTouchOutside(false);
                myProgressDialog.show();
                getDeviceLocation(day,timeofDelivery);
            }else{
                ActivityCompat.requestPermissions(this,permissions,REQUEST_CODE);
            }
        }else{
            ActivityCompat.requestPermissions(this,permissions,REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG,"onRequestPermissionResult : Called");
        locationPermissionGranted = false;
        switch (requestCode){
            case REQUEST_CODE:
                if (grantResults.length >0){
                    for (int i=0;i<grantResults.length;i++){
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED){
                            Log.d(TAG,"onRequestPermissionResult : permission not granted");
                            locationPermissionGranted = false;

                            break;
                        }
                    }
                    Log.d(TAG,"onRequestPermissionResult : Permission granted");
                    locationPermissionGranted = true;
                    myProgressDialog.setCanceledOnTouchOutside(false);
                    myProgressDialog.show();
                    getDeviceLocation(day1,time1);
                }

        }

    }

    public void locationAddress(double lat,double lng) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());


        try {
            List<Address> addresses = geocoder.getFromLocation(lat,lng, 1);
            String address23 = addresses.get(0).getAddressLine(0);

        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Exception:"+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void getDeviceLocation(final String day, final String timeofDelivery){
        Log.d(TAG,"getDeviceLocation : getting device location");
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        final String address;

        try {
            if (locationPermissionGranted){
                Task location = fusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful() && task.getResult() != null){
                            Log.d(TAG,"onComplete : found location ");
                            Location currentlocation = (Location) task.getResult();

                            latitude = currentlocation.getLatitude();
                            longitude = currentlocation.getLongitude();
                            checkoutCart(day,timeofDelivery);
                            //checkoutCart2();
                            locationAddress(currentlocation.getLatitude(),currentlocation.getLongitude());



                        }else{
                            Log.d(TAG,"onComplete : location not found");
                            checkoutCart(day,timeofDelivery);
                            // checkoutCart2();
                        }
                    }
                });
            }
        }catch (SecurityException e){
            Toast.makeText(this, "Security Exception "+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}