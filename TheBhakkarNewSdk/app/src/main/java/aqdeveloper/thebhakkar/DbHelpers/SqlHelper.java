package aqdeveloper.thebhakkar.DbHelpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Hamza on 1/19/18.
 */

public class SqlHelper extends SQLiteOpenHelper {
    public static String dbname = "Store.db";
    public static String tablename = "Products";
    public static String query = "create table " + tablename + "(orderid integer primary key autoincrement, image  BLOB NOT NULL  )";
    Context c;

    public SqlHelper(Context context) {
        super(context, dbname, null, 1);
        c = context;


    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(query);
        // Toast.makeText(c, "table created ", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("drop table if exsists " + tablename);
        onCreate(db);

    }

    public long insert(byte[] imageBytes) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues val = new ContentValues();

        val.put("image", imageBytes);


        return db.insert(tablename, null, val);
    }

    public Cursor getdata(){
        SQLiteDatabase db = this.getWritableDatabase();
        String[] columns = {"image"};

        return    db.query(tablename,columns,null,null,null,null,null);
    }
    public  void deleteAll()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("drop table if exists " + tablename);
        db.execSQL(query);

    }

}
