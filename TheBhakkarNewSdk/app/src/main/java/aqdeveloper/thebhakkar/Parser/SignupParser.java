package aqdeveloper.thebhakkar.Parser;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import aqdeveloper.thebhakkar.Http.TaskResult;

public class SignupParser implements BaseParser {

    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {
            result.success(true);
            try {
                JSONObject obj = new JSONObject(response);

                if(obj.has("message"))
                    if( obj.optString("message").equals("1"))
                    {

                        result.setMessage("true");
                        result.setData(obj.optString("id"));
                    }
                    else if( obj.optString("message").equals("0")){

                        result.setMessage("false");

                    }
                    else if( obj.optString("message").equals("3")){

                        result.setMessage("wrong");

                    }
                result.success(true);
            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);
            }


        }
        return  result;
    }
}
