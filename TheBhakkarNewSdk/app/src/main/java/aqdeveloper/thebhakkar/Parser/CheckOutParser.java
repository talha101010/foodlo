package aqdeveloper.thebhakkar.Parser;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import aqdeveloper.thebhakkar.Http.TaskResult;

public class CheckOutParser implements BaseParser {

    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {
            result.success(true);
            try {
                JSONObject obj = new JSONObject(response);
                result.setMessage(obj.optString("error"));

                result.success(true);
            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);
            }


        }
        return result;
    }
}
