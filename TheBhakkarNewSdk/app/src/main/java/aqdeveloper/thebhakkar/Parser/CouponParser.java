package aqdeveloper.thebhakkar.Parser;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import aqdeveloper.thebhakkar.Http.TaskResult;

public class CouponParser implements BaseParser {
    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {
            result.success(true);
            try {
                JSONObject obj = new JSONObject(response);
                result.setMessage(obj.optString("coupan"));
                result.setData(obj.optString("wallet_price"));

                result.success(true);
            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);
            }


        }
        return result;
    }
}
