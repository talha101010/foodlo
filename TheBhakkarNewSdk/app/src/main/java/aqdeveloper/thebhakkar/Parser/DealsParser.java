package aqdeveloper.thebhakkar.Parser;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import aqdeveloper.thebhakkar.FastFood.MenuFoodItems;
import aqdeveloper.thebhakkar.Http.TaskResult;

public class DealsParser implements BaseParser {
    @Override

    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {

            try {
                result.success(true);
                JSONObject obj = new JSONObject(response);
                if( obj.optString("error").equals("0"))
                {
                    result.setMessage("true");

                    JSONArray array = obj.optJSONArray("buttons");
                    ArrayList<MenuFoodItems> list = new ArrayList<>(array.length());
                    if(array.length() !=0 && array!=null )
                    {
                        for(int i =0;i<array.length();i++){

                            JSONObject items = array.getJSONObject(i);
                            MenuFoodItems food = new MenuFoodItems();
                            food.setImagepath(items.optString("url"));

                            food.setDealid(items.optString("id"));
                            food.setProductname(items.optString("buttonname"));

                            list.add(food);
                        }


                        result.setData(list);
                    }
                    else{
                        result.setMessage("false");

                    }

                }
                else       if( obj.optString("error").equals("1"))
                {
                    result.setMessage("false");
                }



            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);

            }


        }
        return  result;
    }
}

