package aqdeveloper.thebhakkar.Adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import aqdeveloper.thebhakkar.DataProviders.ImageModel;
import aqdeveloper.thebhakkar.R;


public class OfflinePagerAdapter extends PagerAdapter {
    Context mContext;
    LayoutInflater mLayoutInflater;
    ArrayList<ImageModel> imagepaths;
    public OfflinePagerAdapter(Context context, ArrayList<ImageModel> paths ) {
        mContext = context;
        imagepaths = paths;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return imagepaths.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        View v = mLayoutInflater.inflate(R.layout.row_list, container, false);

        ImageView imageView = (ImageView)v.findViewById(R.id.image);
        final ProgressBar progressBar = (ProgressBar)v.findViewById(R.id.progress);

        Glide.with(mContext)
                .load(imagepaths.get(position).getImagepath())

                .crossFade()
                .into(imageView);

        container.addView(v);


        return  v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }


}
