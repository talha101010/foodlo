package aqdeveloper.thebhakkar.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;

import aqdeveloper.thebhakkar.Activities.BKRShopsList;
import aqdeveloper.thebhakkar.Activities.BhakkarBazarActivity;
import aqdeveloper.thebhakkar.DataProviders.ShopsHolder;
import aqdeveloper.thebhakkar.Dialogs.ProDialog;
import aqdeveloper.thebhakkar.Http.AsyncTaskListener;
import aqdeveloper.thebhakkar.Http.HttpAsyncRequest;
import aqdeveloper.thebhakkar.Http.TaskResult;
import aqdeveloper.thebhakkar.Parser.BaseParser;
import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.Url.Constant;

/**
 * Created by Hamza on 2/2/18.
 */

public class CategoryAdapter extends BaseAdapter implements BaseParser {
    private static Context context;
    private static LayoutInflater inflater;
    ArrayList<ShopsHolder> shopdatalist;
    ProDialog dialogbox;


    public CategoryAdapter(Context mainActivity, ArrayList<ShopsHolder> shoplist) {
        // TODO Auto-generated constructor stub
        context = mainActivity;
        shopdatalist= shoplist;

        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return shopdatalist.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView nameinEnglish,nameinUrdu;
        LinearLayout layout ;


        public Holder(View v) {

            nameinEnglish = (TextView)v.findViewById(R.id.nameinenglish);
            nameinUrdu=(TextView)v.findViewById(R.id.nameinurdu);
            layout = (LinearLayout)v.findViewById(R.id.shoplayout);


        }
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final CategoryAdapter.Holder holder;
        View rowView;
        rowView = inflater.inflate(R.layout.row_categories, null);
        holder = new CategoryAdapter.Holder(rowView);

        holder.nameinUrdu.setText(shopdatalist.get(position).getNameinurdu());
        holder.nameinEnglish.setText(shopdatalist.get(position).getNameinenglish());


        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.  startActivity(new Intent(context,BKRShopsList.class).putExtra("name",shopdatalist.get(position).getNameinenglish()));

            }
        });

        return rowView;
    }

    public void edit(String id,String name,String nameinurdu) {

        HttpAsyncRequest request = new HttpAsyncRequest(context, Constant.UPDATE_SHOPURL, HttpAsyncRequest.RequestType.POST, this, listener);
        request.addParam("id", id);

        request.addParam("name",name);
        request.addParam("name_in_urdu",nameinurdu);



        request.execute();
        dialogbox = new ProDialog(context);
        dialogbox.setCanceledOnTouchOutside(false);
        dialogbox.show();


    }
    public void delete(String id,String name) {

        HttpAsyncRequest request = new HttpAsyncRequest(context, Constant.DELETE_SHOPURL, HttpAsyncRequest.RequestType.POST, this, listener);
        request.addParam("id", id);
        request.addParam("name",name);

        request.execute();
        dialogbox = new ProDialog(context);
        dialogbox.setCanceledOnTouchOutside(false);
        dialogbox.show();


    }

    AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            try {
                dialogbox.dismiss();
            } catch (Exception e) {


            }
            if (result.isSuccess()) {

                BhakkarBazarActivity.getShopfromServer();
                Toast.makeText(context, "Successfully Done", Toast.LENGTH_SHORT).show();


            } else {

                Toast.makeText(context, "Error Communicating with the Server", Toast.LENGTH_SHORT).show();

            }
        }


    };

    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {
            result.success(true);
            try {


                result.success(true);
            } catch (Exception e) {
                e.printStackTrace();
                result.success(false);
            }


        }
        return result;

    }
}