package aqdeveloper.thebhakkar.Adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import aqdeveloper.thebhakkar.DataProviders.Doctor_Data_provider;
import aqdeveloper.thebhakkar.R;

/**
 * Created by sajid on 12/7/2017.
 */

public class DoctorDataAdapter extends ArrayAdapter {

    List doctors_list = new ArrayList();

    public DoctorDataAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }

    static  class  LayoutHander{

        TextView doctor_name_in_enlish, doctor_name_in_urdu,doctor_adress;
    }

    @Override
    public void add(@Nullable Object object) {
        super.add(object);
        doctors_list.add(object);
    }

    @Override
    public int getCount() {

        return  doctors_list.size();
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return doctors_list.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View row = convertView;
        DoctorDataAdapter.LayoutHander layoutHander;

        if (row ==null){

            LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.doctors_list_customise,parent,false);
            layoutHander = new DoctorDataAdapter.LayoutHander();
            layoutHander.doctor_name_in_enlish = (TextView) row.findViewById(R.id.doctor_name_in_english);
            layoutHander.doctor_name_in_urdu = (TextView) row.findViewById(R.id.doctor_name_in_urdu);
            layoutHander.doctor_adress = (TextView) row.findViewById(R.id.doctor_adress_in_urdu);
            row.setTag(layoutHander);


        }else {

            layoutHander = (DoctorDataAdapter.LayoutHander)row.getTag();
        }

        Doctor_Data_provider doctor_data_provider = (Doctor_Data_provider) this.getItem(position);
        layoutHander.doctor_name_in_enlish.setText(doctor_data_provider.getDoctor_name_in_english());
        layoutHander.doctor_name_in_urdu.setText(doctor_data_provider.getDoctor_name_in_urdu());
        layoutHander.doctor_adress.setText(doctor_data_provider.getDoctor_adress());

        return row;
    }
}
