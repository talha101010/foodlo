package aqdeveloper.thebhakkar.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import aqdeveloper.thebhakkar.DataProviders.Doctor_Data_provider;
import aqdeveloper.thebhakkar.R;

public class DoctorCategoryAdapter extends ArrayAdapter {

    List doctors_list = new ArrayList();

    public DoctorCategoryAdapter(@NonNull Context context, int resource) {
        super(context, resource);
    }

    public class LayoutHandler {

        TextView doctor_name_in_english, doctor_name_in_urdu;
        ImageView icon;
    }

    @Override
    public void add(@Nullable Object object) {
        super.add(object);
        doctors_list.add(object);
    }

    @Override
    public int getCount() {

        return doctors_list.size();
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return doctors_list.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View row = convertView;
        DoctorCategoryAdapter.LayoutHandler layoutHander;

        if (row == null) {

            LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.doctorcatagory_lv_customise, parent, false);
            layoutHander = new DoctorCategoryAdapter.LayoutHandler();
            layoutHander.doctor_name_in_english = (TextView) row.findViewById(R.id.doctor_name_in_english);
            layoutHander.doctor_name_in_urdu = (TextView) row.findViewById(R.id.doctor_name_in_urdu);
            layoutHander.icon = (ImageView) row.findViewById(R.id.image_icon);
            row.setTag(layoutHander);



        } else {

            layoutHander = (DoctorCategoryAdapter.LayoutHandler) row.getTag();
        }

        Doctor_Data_provider doctor_data_provider = (Doctor_Data_provider) this.getItem(position);
        layoutHander.doctor_name_in_english.setText(doctor_data_provider.getDoctor_name_in_english());
        layoutHander.doctor_name_in_urdu.setText(doctor_data_provider.getDoctor_name_in_urdu());
        layoutHander.icon.setImageResource(doctor_data_provider.getDoctor_icon());


        if (position%2==0){
            layoutHander.icon.setBackgroundResource(R.drawable.image_veiw_bg);
        }else if(position%3==0){
            layoutHander.icon.setBackgroundResource(R.drawable.imageview_green_bg);
        }else {
            layoutHander.icon.setBackgroundResource(R.drawable.imageview_blue_bg);
        }
        return row;
    }
}
