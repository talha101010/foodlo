package aqdeveloper.thebhakkar.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;


import java.util.ArrayList;

import aqdeveloper.thebhakkar.Activities.ShopsProductDetailActivity;
import aqdeveloper.thebhakkar.DataProviders.ShopDetailsHolder;
import aqdeveloper.thebhakkar.R;


public class RecyclerViewAdaptorDetailsOfShops extends RecyclerView.Adapter<RecyclerViewAdaptorDetailsOfShops.MyViewHolder> {

    ArrayList<ShopDetailsHolder> datafromserver;


    Context c;


    public RecyclerViewAdaptorDetailsOfShops(Context context, ArrayList<ShopDetailsHolder> data) {
        c = context;


        datafromserver = data;

    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageViewIcon;
        TextView name, description;
        ProgressBar progressBar;
        RelativeLayout rowlayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.retriveimage);
            this.name = (TextView) itemView.findViewById(R.id.name);
            this.description = (TextView) itemView.findViewById(R.id.description);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
            rowlayout = (RelativeLayout) itemView.findViewById(R.id.rowlayout);


        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_recycler, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);


        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.name.setText(datafromserver.get(position).getName());
        holder.description.setText(datafromserver.get(position).getDescription());


        holder.rowlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(c, ShopsProductDetailActivity.class);
                intent.putExtra("imageurl", datafromserver.get(position).getImgurl());
                intent.putExtra("name", datafromserver.get(position).getName());
                intent.putExtra("description", datafromserver.get(position).getDescription());

                intent.putExtra("id", datafromserver.get(position).getId());
                intent.putExtra("type", "shopdetailsdelete");


                c.startActivity(intent);


            }
        });


        Glide.with(c)
                .load(datafromserver.get(position).getImgurl())

                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.imageViewIcon);


    }

    @Override
    public int getItemCount() {
        return datafromserver.size();
    }


}
