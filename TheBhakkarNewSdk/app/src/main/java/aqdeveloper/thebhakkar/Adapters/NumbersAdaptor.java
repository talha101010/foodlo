package aqdeveloper.thebhakkar.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import aqdeveloper.thebhakkar.Activities.PhoneDirectoryDetail;
import aqdeveloper.thebhakkar.Activities.ShowNumbers;
import aqdeveloper.thebhakkar.DataProviders.DataProvider;
import aqdeveloper.thebhakkar.R;

public class NumbersAdaptor  extends RecyclerView.Adapter<NumbersAdaptor.viewHolder> {

    private Context mContext;
    private List<DataProvider> mList;

    private NumbersAdaptor.OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);

        void onButtonChange(int position);

    }

    public void setOnItemClickListener(NumbersAdaptor.OnItemClickListener listener) {
        mListener = listener;
    }

    public NumbersAdaptor(Context mContext, List<DataProvider> mList) {

        this.mContext = mContext;
        this.mList = mList;


    }


    @NonNull
    @Override
    public NumbersAdaptor.viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.number_customise_recyclerview, viewGroup, false);
        NumbersAdaptor.viewHolder vHolder = new NumbersAdaptor.viewHolder(view, mListener);
        return vHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull NumbersAdaptor.viewHolder viewHolder,final int i) {

        DataProvider uploadImage = mList.get(i);
        TextView name = viewHolder.name;
        TextView number1 = viewHolder.number1;
        TextView number2 = viewHolder.number2;
        TextView adress = viewHolder.address;
        TextView description = viewHolder.description;
        RelativeLayout layout = viewHolder.layout;

        name.setText(uploadImage.getName());

        number1.setText(uploadImage.getNumber1());
        number2.setText(uploadImage.getNumber2());
        adress.setText(uploadImage.getAdress());
        description.setText(uploadImage.getDescription());

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(mContext,PhoneDirectoryDetail.class);
                intent1.putExtra("name",mList.get(i).getName());
                intent1.putExtra("num1",mList.get(i).getNumber1());
                intent1.putExtra("num2",mList.get(i).getNumber2());
                intent1.putExtra("adress",mList.get(i).getAdress());
                intent1.putExtra("des",mList.get(i).getDescription());
                mContext.startActivity(intent1);
            }
        });







    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class viewHolder extends RecyclerView.ViewHolder {

        TextView name, number1, number2, address, description;

        // CardView showNoticeCardview;
        RelativeLayout layout;


        public viewHolder(@NonNull View itemView, final NumbersAdaptor.OnItemClickListener listener) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            number1 = itemView.findViewById(R.id.number1);
            number2 = itemView.findViewById(R.id.number2);
            address = itemView.findViewById(R.id.adress);
            description = itemView.findViewById(R.id.brief_description);
            layout = itemView.findViewById(R.id.layout);



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listener != null) {

                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }

                }
            });


        }
    }

    public void setFilter(List<DataProvider> listitems1){
        mList = new ArrayList<>();
        mList.addAll(listitems1);
        notifyDataSetChanged();
    }
}

