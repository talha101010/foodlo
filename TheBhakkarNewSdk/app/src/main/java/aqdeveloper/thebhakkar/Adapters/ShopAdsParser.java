package aqdeveloper.thebhakkar.Adapters;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import aqdeveloper.thebhakkar.DataProviders.ShopDetailsHolder;
import aqdeveloper.thebhakkar.Http.TaskResult;
import aqdeveloper.thebhakkar.Parser.BaseParser;

/**
 * Created by Hamza on 2/3/18.
 */

public class ShopAdsParser implements BaseParser {
    Activity activity;

    public ShopAdsParser(Activity activity) {
        this.activity = activity;
    }

    @Override
    public TaskResult parse(int httpCode, String response) {

        TaskResult result = new TaskResult();
        Log.d("Response", response);
        if (httpCode == SUCCESS) {
            result.success(true);
            try {
                JSONObject obj = new JSONObject(response);
                JSONArray array = obj.optJSONArray("shops_product_info");
                ArrayList<ShopDetailsHolder> arraylist = new ArrayList<>();
                if(array!= null) {

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject data = array.getJSONObject(i);
                        ShopDetailsHolder bit = new ShopDetailsHolder();
                        bit.setId(data.optString("id"));
                        bit.setImgurl(data.optString("url"));
                        bit.setName(data.optString("ad_title"));
                        bit.setDescription(data.optString("description"));
                        arraylist.add(bit);


                    }
                    result.setData(arraylist);
                }
                else{
                    Toast.makeText(activity, "no ads placed yet", Toast.LENGTH_SHORT).show();
                }

                result.success(true);
            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);
            }


        }
        return result;
    }
}
