package aqdeveloper.thebhakkar.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import aqdeveloper.thebhakkar.DataProviders.HomeDataProvider;
import aqdeveloper.thebhakkar.R;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    private Context mcontext;
    private ArrayList<HomeDataProvider> mList;
    private OnItemClickListener mListener;

    public interface OnItemClickListener{
        void  onItemClick(int position);
        void  onButtonChange(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public HomeAdapter(Context context, ArrayList<HomeDataProvider> list){

        this.mcontext = context;
        this.mList = list;


    }


    @Override
    public HomeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mcontext);
        View view = inflater.inflate(R.layout.homscustomiselistview,parent,false);
        ViewHolder viewHolder = new ViewHolder(view,mListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(HomeAdapter.ViewHolder holder, int position) {

        HomeDataProvider homeDataProvider  = mList.get(position);



        ImageView icons = holder.icons;
        TextView name = holder.btnName;
        TextView nameEng = holder.nameEng;
        CardView cardView = holder.cardView;

        icons.setImageResource(homeDataProvider.getBtnIcon());
        name.setText(homeDataProvider.getBtnName());
        nameEng.setText(homeDataProvider.getBtnNameEng());

        switch (position){




            case 10:
                cardView.setCardBackgroundColor(Color.rgb(153, 204, 0));
                nameEng.setTextColor(Color.BLACK);
                name.setTextColor(Color.BLACK);
                break;
            case 11:
                cardView.setCardBackgroundColor(Color.rgb(255, 153, 0));
                nameEng.setTextColor(Color.BLACK);
                name.setTextColor(Color.BLACK);
                break;
            case 12:
                cardView.setCardBackgroundColor(Color.rgb(102, 102, 255));
                nameEng.setTextColor(Color.WHITE);
                name.setTextColor(Color.WHITE);
                break;
            case 13:
                cardView.setCardBackgroundColor(Color.rgb(204, 51, 0));
                nameEng.setTextColor(Color.WHITE);
                name.setTextColor(Color.WHITE);
                break;
            case 14:
                cardView.setCardBackgroundColor(Color.rgb(102, 255, 153));
                nameEng.setTextColor(Color.BLACK);
                name.setTextColor(Color.BLACK);
                break;
            case 15:
                cardView.setCardBackgroundColor(Color.rgb(255, 255, 0));
                nameEng.setTextColor(Color.BLACK);
                name.setTextColor(Color.BLACK);
                break;
            case 16:
                cardView.setCardBackgroundColor(Color.rgb(255, 153, 0));
                nameEng.setTextColor(Color.BLACK);
                name.setTextColor(Color.BLACK);
                break;
            case 17:
                cardView.setCardBackgroundColor(Color.rgb(102, 102, 255));
                nameEng.setTextColor(Color.WHITE);
                name.setTextColor(Color.WHITE);
                break;
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView btnName,number,nameEng;
        public ImageView icons;
        public CardView cardView;


        public ViewHolder(View itemView, final OnItemClickListener listener) {
            super(itemView);

            btnName = (TextView) itemView.findViewById(R.id.homeBtnName);
            icons = (ImageView) itemView.findViewById(R.id.homeImageIcon);
            cardView = (CardView) itemView.findViewById(R.id.cardview);
            nameEng =itemView.findViewById(R.id.homeBtnNameEng);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listener!=null){

                        int position  = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }

                }
            });
        }
    }
}
