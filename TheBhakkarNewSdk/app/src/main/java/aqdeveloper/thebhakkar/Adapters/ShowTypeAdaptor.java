package aqdeveloper.thebhakkar.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import aqdeveloper.thebhakkar.DataProviders.UploadImage;
import aqdeveloper.thebhakkar.R;

public class ShowTypeAdaptor extends RecyclerView.Adapter<ShowTypeAdaptor.viewHolder>{

    private Context mContext;
    private List<UploadImage> mList;

    private ShowTypeAdaptor.OnItemClickListener mListener;

    public interface OnItemClickListener{
        void  onItemClick(int position);
        void  onButtonChange(int position);

    }

    public void setOnItemClickListener(ShowTypeAdaptor.OnItemClickListener listener){
        mListener = listener;
    }

    public  ShowTypeAdaptor(Context mContext, List<UploadImage> mList){

        this.mContext = mContext;
        this.mList = mList;


    }


    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.customise_phone_directory_type,viewGroup,false);
        ShowTypeAdaptor.viewHolder vHolder = new ShowTypeAdaptor.viewHolder(view,mListener);
        return vHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder viewHolder, int i) {

        UploadImage uploadImage = mList.get(i);
        TextView name = viewHolder.typeTv;
        ImageView picture = viewHolder.typeImage;


        name.setText(uploadImage.getImageName());
        Picasso.with(mContext)
                .load(uploadImage.getImageUri())
                .placeholder(R.drawable.placeholder)
                .fit()
                .centerCrop()
                .into(picture);

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class viewHolder extends RecyclerView.ViewHolder{

        TextView typeTv;
        // CardView showNoticeCardview;
        ImageView typeImage;

        public viewHolder(@NonNull View itemView , final ShowTypeAdaptor.OnItemClickListener listener) {
            super(itemView);

            typeTv = itemView.findViewById(R.id.type_name_tv);
            typeImage = itemView.findViewById(R.id.imageview);



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listener!=null){

                        int position  = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }

                }
            });


        }
    }




}

