package aqdeveloper.thebhakkar.ServiceMan;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import aqdeveloper.thebhakkar.Dialogs.ProDialog;
import aqdeveloper.thebhakkar.Http.AsyncTaskListener;
import aqdeveloper.thebhakkar.Http.HttpAsyncRequest;
import aqdeveloper.thebhakkar.Http.TaskResult;
import aqdeveloper.thebhakkar.Parser.BaseParser;
import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.Url.Constant;

public class ServiceManRegister extends AppCompatActivity implements BaseParser {

    String [] smstatus = {"Please Select status","Available","Busy"};
    ImageView imageviewtest;
    Spinner smspinner_areatxt,smspinner_status,category_spinner;
    EditText smnameinenglish,smphone_number,smpassword,smre_enterpassword;
    ProgressDialog progressDialog;
    Button btn_register,btn_login,alreadyamember;
    CheckBox checkBox;
    private ArrayList<String> arealist = new ArrayList<>();
    private ArrayAdapter<String> areaadapter;
    private ArrayList<String> categorylst = new ArrayList<>();
    private ArrayAdapter<String> categoryadapter;
    private static final int REQUEST_CODE_GALLERY = 20;
    private String picturePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_man_register);

        progressDialog = new ProgressDialog(this);
        getViews();
        getAreaFromServer();
        getCategoryFromServer();
        areaadapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,arealist);
        smspinner_status.setAdapter(new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,smstatus));
        //    category_spinner.setAdapter(new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,category));
        categoryadapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,categorylst);
        progressDialog = new ProgressDialog(this);
        alreadyamember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ServiceManRegister.this,ServieManLogin.class));
                finish();
            }
        });

        // implementing click listener on register button

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(smnameinenglish.getText().length() == 0){
                    smnameinenglish.setError("This field is required");
                }
                else if(smphone_number.getText().length() ==0){
                    smphone_number.setError("This field is required");
                }
                else if(smphone_number.getText().length() < 11 || smphone_number.getText().toString().charAt(0) != '0'){
                    smphone_number.setError("Please provide an 11 digit correct phone number");
                    Toast.makeText(ServiceManRegister.this, "Your phone number should start with 0 and it should be 11 digits in length", Toast.LENGTH_SHORT).show();
                }
                else if (smpassword.getText().length() == 0){
                    smpassword.setError("This field is required");
                }
                else if (smre_enterpassword.getText().length() == 0){
                    smre_enterpassword.setError("This field is required");

                }
                else if (smspinner_areatxt.getSelectedItemPosition() == 0){
                    Toast.makeText(ServiceManRegister.this, "Please select area", Toast.LENGTH_LONG).show();

                }
                else if (smspinner_status.getSelectedItemPosition() ==0){
                    Toast.makeText(ServiceManRegister.this, "Please select status", Toast.LENGTH_LONG).show();
                }
                else if (!checkBox.isChecked()){
                    Toast.makeText(ServiceManRegister.this, "Please accept terms&conditions to proceed", Toast.LENGTH_SHORT).show();
                }else if (category_spinner.getSelectedItemPosition() == 0){
                    Toast.makeText(ServiceManRegister.this, "Please Select Category from the first field", Toast.LENGTH_SHORT).show();
                }
                else{
                    if(isPassword()){
                        if (isNetworkAvailable()) {
                            uploadData();
                        }else{
                            Toast.makeText(ServiceManRegister.this, "your internet connection is not available", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        smre_enterpassword.setError("Password does not match");
                        Toast.makeText(ServiceManRegister.this, "Password does not match", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        imageviewtest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ActivityCompat.requestPermissions(ServiceManRegister.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_GALLERY);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CODE_GALLERY) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE_GALLERY);
            } else {
                Toast.makeText(getApplicationContext(), "You don't have permission to access file location!", Toast.LENGTH_SHORT).show();
            }
            return;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            picturePath = getRealPathFromURI(uri);


            try {
                InputStream inputStream = getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                imageviewtest.setImageBitmap(bitmap);

                // bitmap.recycle();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    public void uploadData(){
        HttpAsyncRequest request = new HttpAsyncRequest(this,Constant.ADD_USER_TO_PENDING_URL,HttpAsyncRequest.RequestType.POST,this,listener);
        request.addParam("name_in_english",smnameinenglish.getText().toString());
        request.addParam("phone_number",smphone_number.getText().toString());
        request.addParam("select_area",arealist.get(smspinner_areatxt.getSelectedItemPosition()));
        request.addParam("password",smpassword.getText().toString());
        request.addParam("status",smstatus[smspinner_status.getSelectedItemPosition()]);
        request.addParam("category",categorylst.get(category_spinner.getSelectedItemPosition()));
        if (picturePath != null){
            request.addParam("pic","false");
            request.addFile("image",picturePath);
        }else{
            request.addParam("pic","true");
            request.addParam("url1","null.jpg");
        }
        request.execute();
        dilog = new ProDialog(this);
        dilog.setCanceledOnTouchOutside(false);
        dilog.show();
    }
    private ProDialog dilog ;
    AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            try {
                dilog.dismiss();
            }catch (Exception e){

            }
            if (result.isSuccess()){
                clear();
            }else {
                LayoutInflater li = LayoutInflater.from(ServiceManRegister.this);
                View prompt = li.inflate(R.layout.promptdilog,null);
                AlertDialog.Builder alertdialog = new AlertDialog.Builder(ServiceManRegister.this);
                alertdialog.setView(prompt);
                TextView text = (TextView) prompt.findViewById(R.id.prompttextview);
                alertdialog.setCancelable(false).setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(ServiceManRegister.this,ServiceManRegister.class));
                        finish();
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        finish();
                    }
                });

                AlertDialog alert = alertdialog.create();
                alert.show();
            }
        }
    };


    // getting views

    public void getViews(){
        smnameinenglish = (EditText) findViewById(R.id.name_in_englishtxt);
        smphone_number = (EditText) findViewById(R.id.phone_numbertxt);
        smpassword = (EditText) findViewById(R.id.passwordtxt);
        smre_enterpassword = (EditText) findViewById(R.id.re_enter_passwordtxt);
        smspinner_areatxt = (Spinner) findViewById(R.id.spinner_area);
        smspinner_status = (Spinner) findViewById(R.id.status_spinner);
        btn_register = (Button) findViewById(R.id.btn_register);
        checkBox = (CheckBox) findViewById(R.id.chkBox1);
        alreadyamember = (Button) findViewById(R.id.txt_login);
        category_spinner = (Spinner) findViewById(R.id.category_spinner);
        imageviewtest = (ImageView) findViewById(R.id.imageviewtest);
    }

    public boolean isPassword(){
        if(smpassword.getText().toString().equals(smre_enterpassword.getText().toString())){
            return true;
        }else{
            return false;
        }
    }

    public void getAreaFromServer(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.SHOW_AREA_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            arealist.add(0,"Please Select Area");
                            for (int i=0;i< jsonArray.length();i++){
                                JSONObject json = jsonArray.getJSONObject(i);
                                arealist.add(json.getString("areaname"));
                            }
                            areaadapter.notifyDataSetChanged();
                            smspinner_areatxt.setAdapter(areaadapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LayoutInflater li = LayoutInflater.from(ServiceManRegister.this);
                        View prompt = li.inflate(R.layout.promptdilog,null);
                        AlertDialog.Builder alertdialog = new AlertDialog.Builder(ServiceManRegister.this);
                        alertdialog.setView(prompt);
                        TextView text = (TextView) prompt.findViewById(R.id.prompttextview);
                        alertdialog.setCancelable(false).setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(ServiceManRegister.this,ServiceManRegister.class));
                                finish();
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                finish();
                            }
                        });

                        AlertDialog alert = alertdialog.create();
                        alert.show();
                    }
                });

        RequestQueue resuestqueue = Volley.newRequestQueue(this);
        resuestqueue.add(stringRequest);
    }

    public void getCategoryFromServer(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.SHOW_CATEGORY_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            categorylst.add(0,"Please Select Category");
                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject json = jsonArray.getJSONObject(i);
                                categorylst.add(json.getString("category"));
                            }
                            categoryadapter.notifyDataSetChanged();
                            category_spinner.setAdapter(categoryadapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LayoutInflater li = LayoutInflater.from(ServiceManRegister.this);
                        View prompt = li.inflate(R.layout.promptdilog,null);
                        AlertDialog.Builder alertdialog = new AlertDialog.Builder(ServiceManRegister.this);
                        alertdialog.setView(prompt);
                        TextView text = (TextView) prompt.findViewById(R.id.prompttextview);
                        alertdialog.setCancelable(false).setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(ServiceManRegister.this,ServiceManRegister.class));
                                finish();
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                finish();
                            }
                        });

                        AlertDialog alert = alertdialog.create();
                        alert.show();
                    }
                });

        RequestQueue resuestqueue = Volley.newRequestQueue(this);
        resuestqueue.add(stringRequest);
    }

    public void clear(){
        smnameinenglish.getText().clear();
        smphone_number.getText().clear();
        smpassword.getText().clear();
        smre_enterpassword.getText().clear();
        smspinner_areatxt.setSelection(0);
        smspinner_status.setSelection(0);
        category_spinner.setSelection(0);
        checkBox.toggle();
        picturePath = null;
        imageviewtest.setImageResource(R.drawable.defult);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response",response);
        if (httpCode == SUCCESS){
            result.success(true);
            try {

                JSONObject jsonObject = new JSONObject(response);
                Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                result.success(true);
            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);
            }
        }
        return result;
    }
}
