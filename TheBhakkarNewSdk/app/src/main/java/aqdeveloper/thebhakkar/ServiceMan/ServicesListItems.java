package aqdeveloper.thebhakkar.ServiceMan;

public class ServicesListItems {

    private String category;
    private String id;
    private String category_urdu;

    public ServicesListItems(String category,String id,String category_urdu) {

        this.category = category;
        this.id =id;
        this.category_urdu = category_urdu;
    }

    public String getService_name() {
        return category;
    }

    public String getId() {
        return id;
    }

    public String getCategory_urdu() {
        return category_urdu;
    }
}
