package aqdeveloper.thebhakkar.ServiceMan;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import aqdeveloper.thebhakkar.R;

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {

    private List<ServicesListItems> listItem;
    private Context context;


    public ServicesAdapter(List<ServicesListItems> listItem,Context context) {

        this.listItem = listItem;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.serviceslistitemcustomise,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final ServicesListItems list = listItem.get(position);
        holder.category.setText(list.getService_name());
        holder.id.setText(list.getId());
        holder.category_urdu.setText(list.getCategory_urdu());
        holder.lineraLayoutservice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ServicesListActivity.prompt(holder.category.getText().toString());
            }
        });

    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView category,id,category_urdu;
        LinearLayout lineraLayoutservice;

        public ViewHolder(View itemView) {
            super(itemView);
            category = (TextView) itemView.findViewById(R.id.categorynametxt);
            id = (TextView) itemView.findViewById(R.id.idtxt);
            category_urdu = (TextView) itemView.findViewById(R.id.category_urdutxt);
            lineraLayoutservice = (LinearLayout) itemView.findViewById(R.id.serviceslinearlayout);

        }
    }
}
