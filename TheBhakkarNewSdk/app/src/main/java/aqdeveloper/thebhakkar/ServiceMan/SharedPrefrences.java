package aqdeveloper.thebhakkar.ServiceMan;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by www10 on 4/17/2018.
 */

public class SharedPrefrences {
    private static SharedPrefrences mInstance;

    public static final String SHARE_PREF_NAME = "sharepreferences";
    public static final String PHONE_KEY = "phone";


    private static Context mCtx;

    private SharedPrefrences(Context context) {
        mCtx = context;


    }

    public static synchronized SharedPrefrences getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefrences(context);
        }
        return mInstance;
    }
    public boolean login(String phone){
        SharedPreferences pref = mCtx.getSharedPreferences(SHARE_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(PHONE_KEY,phone);
        editor.apply();

        return true;
    }
    public boolean isLoggedIn(){
        SharedPreferences sharedpreferences = mCtx.getSharedPreferences(SHARE_PREF_NAME,Context.MODE_PRIVATE);
        if(sharedpreferences.getString(PHONE_KEY,null) != null){
            return true;
        }
        return false;
    }
    public boolean logout(){
        SharedPreferences pref = mCtx.getSharedPreferences(SHARE_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.apply();
        return true;
    }
}
