package aqdeveloper.thebhakkar.ServiceMan;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import aqdeveloper.thebhakkar.R;

public class ServiceManListAdapter extends RecyclerView.Adapter<ServiceManListAdapter.ViewHolder> {

    private List<ServiceManListItems> serviceListItems;
    Context context;
    boolean status;

    public ServiceManListAdapter(List<ServiceManListItems> serviceListItems, Context context) {
        this.serviceListItems = serviceListItems;
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.servicemanlistitem,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {


        final ServiceManListItems list = serviceListItems.get(position);
        holder.servicename.setText(list.getNametxt());
        holder.serviceaddress.setText(list.getAddresstxt());
        holder.serviceid.setText(list.getIdtxt());
        holder.phonenumber.setText(list.getPhonenumber());
        holder.categorytxt.setText(list.getCategory());
        holder.progressBar.setVisibility(View.VISIBLE);
        Picasso.with(context).load(list.getImageUrl()).into(holder.imagview, new Callback() {
            @Override
            public void onSuccess() {
                holder.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });

        if(list.getStatus().equals("Available")){
            holder.statusbtn.setText("حاضر ہوں جناب");
            holder.statusbtn.setBackgroundResource(R.drawable.serviceman_status_btn);
            holder.statusbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ListOfServiceMan.prompt(holder.servicename.getText().toString(),holder.serviceaddress.getText().toString(),
                            holder.phonenumber.getText().toString(),
                            holder.categorytxt.getText().toString(),holder.statusbtn.getText().toString());
                }
            });
            holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ListOfServiceMan.prompt(holder.servicename.getText().toString(),holder.serviceaddress.getText().toString(),
                            holder.phonenumber.getText().toString(),
                            holder.categorytxt.getText().toString(),holder.statusbtn.getText().toString());
                }
            });


        }else{
            holder.statusbtn.setText("کام پر ہوں");
            holder.statusbtn.setBackgroundResource(R.drawable.serviceman_busy_status_btn_bg);
            holder.statusbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "ابھی کام کر رہا ہوں۔", Toast.LENGTH_SHORT).show();
                    Toast.makeText(context, "ابھی کام کر رہا ہوں۔", Toast.LENGTH_SHORT).show();

                }
            });
            holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "ابھی کام کر رہا ہوں۔", Toast.LENGTH_SHORT).show();
                    Toast.makeText(context, "ابھی کام کر رہا ہوں۔", Toast.LENGTH_SHORT).show();

                }
            });
        }

    }



    @Override
    public int getItemCount() {
        return serviceListItems.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView servicename,serviceaddress,serviceid,phonenumber,categorytxt;
        public RelativeLayout relativeLayout;
        public Button statusbtn;
        public ImageView imagview;
        public ProgressBar progressBar;

        public ViewHolder(View itemView) {
            super(itemView);
            servicename = (TextView) itemView.findViewById(R.id.servicenametxt);
            serviceaddress = (TextView) itemView.findViewById(R.id.serviceaddresstxt);
            serviceid = (TextView) itemView.findViewById(R.id.serviceidtxt);
            statusbtn = (Button) itemView.findViewById(R.id.statusbtn);
            phonenumber = (TextView) itemView.findViewById(R.id.servicephonenumbertxt);
            categorytxt = (TextView) itemView.findViewById(R.id.servicecategorytxt);
            imagview = (ImageView) itemView.findViewById(R.id.imgview);
            progressBar = (ProgressBar) itemView.findViewById(R.id.servicemanprogressbar);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.servicesrealativelayout);

        }
    }
}

