package aqdeveloper.thebhakkar.ServiceMan;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.Url.Constant;

public class ServiceManUserDetails extends AppCompatActivity {

    TextView nameinenglish, category, phonenumber, selectedarea,
            status,url;
    Button updatebtn,logoutbtn,statusupdatebtn;
    ImageView profileimage;
    ProgressDialog progress;
    ProgressBar progressBar;
    String phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_man_user_details);


        progress = new ProgressDialog(this);
        getViews();
        SharedPreferences sharedPreferences = this.getSharedPreferences("sharepreferences", Context.MODE_PRIVATE);
        phone = sharedPreferences.getString("phone",null);
        FetchDataFromServer();
        logoutbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPrefrences.getInstance(getApplicationContext()).logout()){
                    startActivity(new Intent(ServiceManUserDetails.this,ServieManLogin.class));
                    finish();
                }
            }
        });
        updatebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ServiceManUserDetails.this,ServiceManUpdateData.class);
                i.putExtra("nameinenglish",nameinenglish.getText().toString());
                i.putExtra("category",category.getText().toString());
                i.putExtra("phonenumber",phonenumber.getText().toString());
                i.putExtra("selectedarea",selectedarea.getText().toString());
                i.putExtra("status",status.getText().toString());
                i.putExtra("url1",url.getText().toString());
                startActivity(i);
            }
        });

        statusupdatebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusUpdate();
            }
        });
    }

    public void getViews() {
        nameinenglish = (TextView) findViewById(R.id.profilenameinenglishtxt);
        category = (TextView) findViewById(R.id.profilecategorytxt);
        phonenumber = (TextView) findViewById(R.id.profilephonenumbertxt);
        selectedarea = (TextView) findViewById(R.id.profileselectedareatxt);
        status = (TextView) findViewById(R.id.profilestatustxt);
        logoutbtn = (Button) findViewById(R.id.logout_btn);
        updatebtn = (Button) findViewById(R.id.btn_update);
        profileimage = (ImageView) findViewById(R.id.profileimageupdate);
        statusupdatebtn = (Button) findViewById(R.id.statusupdatebtn);
        url = (TextView) findViewById(R.id.urltxt);
        progressBar = (ProgressBar) findViewById(R.id.updateprogressbar);

    }

    public void FetchDataFromServer() {
        progress.setMessage("Loading Data.......");
        progress.setCanceledOnTouchOutside(false);
        progress.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.SHOW_DATA_TO_USER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progress.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject json = jsonArray.getJSONObject(i);
                                nameinenglish.setText(json.getString("name_in_english"));
                                category.setText(json.getString("category"));
                                phonenumber.setText(json.getString("phone_number"));
                                selectedarea.setText(json.getString("selected_area"));
                                status.setText(json.getString("status"));
                                url.setText(json.getString("url1"));
                                progressBar.setVisibility(View.VISIBLE);
                                Picasso.with(ServiceManUserDetails.this).load(json.getString("url")).into(profileimage, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        progressBar.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onError() {

                                    }
                                });
                                String stat =    json.getString("status");
                                statusupdatebtn.setText(stat);
                                if (stat.equals("Available")){
                                    // statusupdatebtn.setBackgroundColor(Color.rgb(34,177,76));
                                    statusupdatebtn.setBackgroundResource(R.drawable.serman_submit_call_btn_bg);
                                }else{
                                    // statusupdatebtn.setBackgroundColor(Color.rgb(221,51,51));
                                    statusupdatebtn.setBackgroundResource(R.drawable.serviceman_busy_status_btn_bg);
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.dismiss();
                        LayoutInflater li = LayoutInflater.from(ServiceManUserDetails.this);
                        View prompt = li.inflate(R.layout.promptdilog,null);
                        AlertDialog.Builder alertdialog = new AlertDialog.Builder(ServiceManUserDetails.this);
                        alertdialog.setView(prompt);
                        TextView text = (TextView) prompt.findViewById(R.id.prompttextview);
                        alertdialog.setCancelable(false).setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(ServiceManUserDetails.this,ServiceManUserDetails.class));
                                finish();
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                finish();
                            }
                        });

                        AlertDialog alert = alertdialog.create();
                        alert.show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("phone_number",phone);
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void statusUpdate(){
        progress.setMessage("Updating Status.....");
        progress.setCanceledOnTouchOutside(false);
        progress.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.UPDATE_STATUS_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progress.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Toast.makeText(ServiceManUserDetails.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            finish();
                            startActivity(new Intent(ServiceManUserDetails.this,ServiceManUserDetails.class));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.dismiss();
                        LayoutInflater li = LayoutInflater.from(ServiceManUserDetails.this);
                        View prompt = li.inflate(R.layout.promptdilog,null);
                        AlertDialog.Builder alertdialog = new AlertDialog.Builder(ServiceManUserDetails.this);
                        alertdialog.setView(prompt);
                        TextView text = (TextView) prompt.findViewById(R.id.prompttextview);
                        alertdialog.setCancelable(false).setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(ServiceManUserDetails.this,ServiceManUserDetails.class));
                                finish();
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                finish();
                            }
                        });

                        AlertDialog alert = alertdialog.create();
                        alert.show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("status",status.getText().toString().trim());
                param.put("phone_number",phone);
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }
}
