package aqdeveloper.thebhakkar.ServiceMan;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import aqdeveloper.thebhakkar.Dialogs.ProDialog;
import aqdeveloper.thebhakkar.Http.AsyncTaskListener;
import aqdeveloper.thebhakkar.Http.HttpAsyncRequest;
import aqdeveloper.thebhakkar.Http.TaskResult;
import aqdeveloper.thebhakkar.Parser.BaseParser;
import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.Url.Constant;

public class ServiceManUpdateData extends AppCompatActivity implements BaseParser {

    String [] smstatus = {"Please Select status","Available","Busy"};
    EditText nameinenglish,phone;
    TextView url1;
    Button btn_updatedata;
    ImageView imageviewupdate;
    Bitmap bitmap;
    Spinner category_spinner,selected_area,status;
    private ArrayList<String> arealist = new ArrayList<>();
    private ArrayAdapter<String> areaadapter;
    private ArrayList<String> categorylst = new ArrayList<>();
    private ArrayAdapter<String> categoryadapter;
    private ArrayAdapter<String> statusadapter;

    String phone_number,category,area;
    ProgressDialog progress;

    private static final int REQUEST_CODE_GALLERY = 20;
    private String picturePath;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_man_update_data);

        views();
        areaadapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,arealist);
        statusadapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,smstatus);
        status.setAdapter(statusadapter);
        status.setSelection(statusadapter.getPosition(getIntent().getStringExtra("status")));
        categoryadapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,categorylst);

//        SharedPreferences sharedPrefrences = this.getSharedPreferences("sharepreferences", Context.MODE_PRIVATE);
//        phone_number = sharedPrefrences.getString("phone",null);
        progress = new ProgressDialog(this);

        getAreaFromServer();
        getCategoryFromServer();
        if (getIntent().hasExtra("nameinenglish")){
            nameinenglish.setText(getIntent().getStringExtra("nameinenglish"));
            phone.setText(getIntent().getStringExtra("phonenumber"));
            url1.setText(getIntent().getStringExtra("url1"));

        }
        btn_updatedata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (category_spinner.getSelectedItemPosition() ==0){
                    Toast.makeText(ServiceManUpdateData.this, "please select category", Toast.LENGTH_SHORT).show();
                }else if (nameinenglish.getText().length() ==0){
                    nameinenglish.setError("this field is required");
                }else if (phone.getText().length() ==0){
                    phone.setError("this field is required");
                }else if (phone.getText().length() < 11){
                    phone.setError("please provide an 11 digit correct phone number");
                }
                else if (selected_area.getSelectedItemPosition()==0){
                    Toast.makeText(ServiceManUpdateData.this, "please select any area", Toast.LENGTH_SHORT).show();
                }else if (status.getSelectedItemPosition()==0){
                    Toast.makeText(ServiceManUpdateData.this, "please select status", Toast.LENGTH_SHORT).show();
                }else {
                    updateUserData();
                }
            }
        });

        imageviewupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ActivityCompat.requestPermissions(ServiceManUpdateData.this,
                        new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_GALLERY);
            }
        });
    }

    public void views(){
        category_spinner = (Spinner) findViewById(R.id.category_spinnerupdate);
        nameinenglish = (EditText) findViewById(R.id.name_in_englishtxtupdate);
        phone = (EditText) findViewById(R.id.phone_numbertxtupdate);
        selected_area = (Spinner) findViewById(R.id.spinner_areaupdate);
        status = (Spinner) findViewById(R.id.status_spinnerupdate);
        btn_updatedata = (Button) findViewById(R.id.btn_updatedata);
        imageviewupdate = (ImageView) findViewById(R.id.imageviewupdate);
        url1 = (TextView) findViewById(R.id.url1txt);

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CODE_GALLERY) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE_GALLERY);
            } else {
                Toast.makeText(getApplicationContext(), "You don't have permission to access file location!", Toast.LENGTH_SHORT).show();
            }
            return;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            picturePath = getRealPathFromURI(uri);


            try {
                InputStream inputStream = getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                imageviewupdate.setImageBitmap(bitmap);

                // bitmap.recycle();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }



    // end of method

    public void getAreaFromServer(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.SHOW_AREA_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            arealist.add(0,"Please Select Area");
                            for (int i=0;i< jsonArray.length();i++){
                                JSONObject json = jsonArray.getJSONObject(i);
                                arealist.add(json.getString("areaname"));
                            }
                            areaadapter.notifyDataSetChanged();
                            selected_area.setAdapter(areaadapter);
                            selected_area.setSelection(areaadapter.getPosition(getIntent().getStringExtra("selectedarea")));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ServiceManUpdateData.this, "error"+error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        RequestQueue resuestqueue = Volley.newRequestQueue(this);
        resuestqueue.add(stringRequest);
    }

    public void getCategoryFromServer(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.SHOW_CATEGORY_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            categorylst.add(0,"Please Select Category");
                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject json = jsonArray.getJSONObject(i);
                                categorylst.add(json.getString("category"));
                            }
                            categoryadapter.notifyDataSetChanged();
                            category_spinner.setAdapter(categoryadapter);
                            category_spinner.setSelection(categoryadapter.getPosition(getIntent().getStringExtra("category")));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ServiceManUpdateData.this, "some error "+error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        RequestQueue resuestqueue = Volley.newRequestQueue(this);
        resuestqueue.add(stringRequest);
    }

    // update user data

    public void updateUserData(){
        HttpAsyncRequest request = new HttpAsyncRequest(this,Constant.UPDATE_URL,HttpAsyncRequest.RequestType.POST,this,listener);
        request.addParam("name_in_english",nameinenglish.getText().toString());
        request.addParam("phone_number",phone.getText().toString());
        request.addParam("select_area",arealist.get(selected_area.getSelectedItemPosition()));
        request.addParam("status",smstatus[status.getSelectedItemPosition()]);
        request.addParam("category",categorylst.get(category_spinner.getSelectedItemPosition()));
        request.addParam("url1",url1.getText().toString());
        if (picturePath != null){
            request.addParam("pic","false");
            request.addFile("pic",picturePath);
        }else {
            request.addParam("pic","true");

        }
        request.execute();
        dilog = new ProDialog(this);
        dilog.setCanceledOnTouchOutside(false);
        dilog.show();
    }
    private ProDialog dilog ;
    AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            try {
                dilog.dismiss();
            }catch (Exception e){

            }
            if (result.isSuccess()){
                startActivity(new Intent(ServiceManUpdateData.this,ServiceManUpdateData.class));
                finish();
            }else {
                Toast.makeText(ServiceManUpdateData.this, "Error communicationg with server", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        Log.d("Response",response);
        if (httpCode == SUCCESS){
            result.success(true);
            try {

                JSONObject jsonObject = new JSONObject(response);
                Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                result.success(true);
            } catch (JSONException e) {
                e.printStackTrace();
                result.success(false);
            }
        }
        return result;

    }
}
