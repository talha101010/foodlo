package aqdeveloper.thebhakkar.ServiceMan;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.Url.Constant;

public class ListOfServiceMan extends AppCompatActivity {

    private static RecyclerView recyclerView;
    private static ServiceManListAdapter adapter;
    private static List<ServiceManListItems> list;
    private static ProgressDialog progress;
    private static TextView servicemanname,servicemanarea,message,remove1,remove2,remove3;
    private static Button button,button1;
    private static EditText phonenumberdilog1,namedilog1,addressindilog;

    Button btn;
    boolean status = false;
    // String category;
    String selecteditem, category;
    public static Context context;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_service_man);

        context = ListOfServiceMan.this;
        recyclerView = (RecyclerView) findViewById(R.id.servicemanrecyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        list = new ArrayList<>();
        progress = new ProgressDialog(this);

        if (getIntent().hasExtra("selecteditem")){
            selecteditem = getIntent().getStringExtra("selecteditem");
            category = getIntent().getStringExtra("category");
        }

        recieveDataFromServeronitemselected(selecteditem,category);



    }

    public static void recieveDataFromServeronitemselected(final String selecteditem,final String category1){

        progress.setMessage("Loading please wait....");
        progress.setCanceledOnTouchOutside(false);
        progress.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.SPINNER_AREA_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progress.dismiss();
                        try {
                            JSONObject json = new JSONObject(response);
                            JSONArray jsonArray = json.getJSONArray("data");
                            if (jsonArray.length() != 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    ServiceManListItems listit = new ServiceManListItems(jsonObject.getString("name_in_english"),
                                            jsonObject.getString("selected_area"),
                                            jsonObject.getString("id"),jsonObject.getString("url"),jsonObject.getString("status"),
                                            jsonObject.getString("phone_number"),jsonObject.getString("category"));

                                    list.add(listit);

                                }
                            }else{
                                Toast.makeText(context, "ابھی تک یہاں سے کوئی سروس مین رجسٹرڈ نہیں ہوا۔۔", Toast.LENGTH_SHORT).show();
                                Toast.makeText(context, "ابھی تک یہاں سے کوئی سروس مین رجسٹرڈ نہیں ہوا۔۔", Toast.LENGTH_SHORT).show();

                            }

                            adapter = new ServiceManListAdapter(list, context);
                            recyclerView.setAdapter(adapter);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.dismiss();
                        Toast.makeText(context, "Error communicating with server", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("area",selecteditem);
                param.put("category",category1);
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    public static void prompt(final String name, final String selected_area,final String phonenumber, final String category, final String status){
        LayoutInflater li = LayoutInflater.from(context);
        View prompt = li.inflate(R.layout.getbuttonprompt,null);
        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(context);
        alertdialog.setView(prompt);
        servicemanname = (TextView) prompt.findViewById(R.id.servicemannameindilog);
        servicemanarea = (TextView) prompt.findViewById(R.id.servicemanareaindilog);
        servicemanname.setText(name);
        servicemanarea.setText(selected_area);
        phonenumberdilog1 = (EditText) prompt.findViewById(R.id.phonenumberindilog);
        namedilog1 = (EditText) prompt.findViewById(R.id.nameindilog);
        addressindilog = (EditText) prompt.findViewById(R.id.addressindilog);
        button = (Button) prompt.findViewById(R.id.submitdilogbtn);
        button1 = (Button) prompt.findViewById(R.id.servicemancallbtndilog);
        message = (TextView) prompt.findViewById(R.id.successmessagedilogtext);
        remove1 = (TextView) prompt.findViewById(R.id.remove1);
        remove2 = (TextView) prompt.findViewById(R.id.remove2);
        remove3 = (TextView) prompt.findViewById(R.id.remove3);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (phonenumberdilog1.getText().length() == 0){
                    phonenumberdilog1.setError("phone number is required");
                }else if (namedilog1.getText().length() == 0){
                    namedilog1.setError("Name is required");
                }else if (addressindilog.getText().length() == 0){
                    addressindilog.setError("Your address required");
                }else{
                    sendDataToServer(name,category,selected_area,phonenumber,status,
                            phonenumberdilog1.getText().toString().trim(),namedilog1.getText().toString().trim(),
                            addressindilog.getText().toString().trim());
                }
            }
        });

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri number = Uri.parse("tel:"+phonenumber);
                Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
                view.getContext().startActivity(callIntent);
            }
        });
        alertdialog.setCancelable(false).setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog alert = alertdialog.create();
        alert.show();
    }

    public static void sendDataToServer(final String nameinenglishtxt,final String designationtxt, final String areatxt,
                                        final String servicemanphonenumber, final String statustxt,
                                        final String phonenumberdilog, final String namedilog, final String addressdilog){
        progress.setMessage("processing.....");
        progress.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.EMAIL_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progress.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            button.setVisibility(View.GONE);
                            phonenumberdilog1.setVisibility(View.GONE);
                            namedilog1.setVisibility(View.GONE);
                            addressindilog.setVisibility(View.GONE);
                            servicemanname.setVisibility(View.GONE);
                            servicemanarea.setVisibility(View.GONE);
                            remove1.setVisibility(View.GONE);
                            remove2.setVisibility(View.GONE);
                            remove3.setVisibility(View.GONE);
                            message.setVisibility(View.VISIBLE);
                            button1.setVisibility(View.VISIBLE);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.dismiss();
                        Toast.makeText(context, "Error Communicating with server", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("name_in_english",nameinenglishtxt);
                param.put("category",designationtxt);
                param.put("area",areatxt);
                param.put("service_man_phone_number",servicemanphonenumber);
                param.put("status",statustxt);
                param.put("getter_phone_number",phonenumberdilog);
                param.put("getter_name",namedilog);
                param.put("getter_address",addressdilog);
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);

    }
}
