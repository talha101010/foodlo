package aqdeveloper.thebhakkar.ServiceMan;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.Url.Constant;

public class ServicesListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ServicesAdapter adapter;
    private List<ServicesListItems> list;
    ProgressDialog progress;
    public static Context context;
    private static ArrayList<String> arealist = new ArrayList<>();
    private static ArrayAdapter<String> areaadapter;
    public static Spinner areaspinner;
    public static TextView loadingarea;
    private static ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services_list);

        context = ServicesListActivity.this;
        recyclerView = (RecyclerView) findViewById(R.id.servicesrecyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        list = new ArrayList<>();
        progress = new ProgressDialog(this);
        loadDataFromServer();
    }

    public void loadDataFromServer() {
        progress.setMessage("Loading.....");
        progress.setCanceledOnTouchOutside(false);
        progress.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.SHOW_SERVICE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progress.dismiss();
                        try {
                            JSONObject json = new JSONObject(response);
                            JSONArray jsonArray = json.getJSONArray("data");
                            if (jsonArray.length() != 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    ServicesListItems listit = new ServicesListItems(jsonObject.getString("category"), jsonObject.getString("id"), jsonObject.getString("category_urdu"));
                                    list.add(listit);
                                }
                                adapter = new ServicesAdapter(list, getApplicationContext());
                                recyclerView.setAdapter(adapter);
                            } else {
                                Toast.makeText(ServicesListActivity.this, "Sorry no data available yet", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.dismiss();
                        LayoutInflater li = LayoutInflater.from(ServicesListActivity.this);
                        View prompt = li.inflate(R.layout.promptdilog, null);
                        AlertDialog.Builder alertdialog = new AlertDialog.Builder(ServicesListActivity.this);
                        alertdialog.setView(prompt);
                        TextView text = (TextView) prompt.findViewById(R.id.prompttextview);
                        alertdialog.setCancelable(false).setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(ServicesListActivity.this, ServicesListActivity.class));
                                finish();
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                finish();
                            }
                        });

                        AlertDialog alert = alertdialog.create();
                        alert.show();

                    }
                });
        RequestQueue resuestqueue = Volley.newRequestQueue(this);
        resuestqueue.add(stringRequest);
    }

    public static void getAreaFromServer() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.SHOW_AREA_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            arealist.add(0, "Please select your area");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject json = jsonArray.getJSONObject(i);
                                arealist.add(json.getString("areaname"));
                            }
                            areaadapter.notifyDataSetChanged();
                            areaspinner.setAdapter(areaadapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(context, "Communication server error please try again later", Toast.LENGTH_SHORT).show();
                    }
                });

        RequestQueue resuestqueue = Volley.newRequestQueue(context);
        resuestqueue.add(stringRequest);
    }

    public static void prompt(final String category) {
        LayoutInflater li = LayoutInflater.from(context);
        View prompt = li.inflate(R.layout.servicemanprompt, null);
        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(context);
        alertdialog.setView(prompt);
        progressBar = (ProgressBar) prompt.findViewById(R.id.areaprogressbar);
        areaspinner = (Spinner) prompt.findViewById(R.id.selectareaserviceman);
        areaadapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, arealist);

        getAreaFromServer();

        areaspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selecteditem = adapterView.getItemAtPosition(i).toString();
                if (i != 0) {
                    Intent myintent = new Intent(view.getContext(), ListOfServiceMan.class);
                    myintent.putExtra("category", category);
                    myintent.putExtra("selecteditem", selecteditem);
                    view.getContext().startActivity(myintent);

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        alertdialog.setCancelable(false).setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                arealist.clear();
                areaspinner.setAdapter(null);

            }
        });


        AlertDialog alert = alertdialog.create();
        alert.show();

    }
}