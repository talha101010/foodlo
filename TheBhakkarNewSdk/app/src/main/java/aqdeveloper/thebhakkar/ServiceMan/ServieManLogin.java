package aqdeveloper.thebhakkar.ServiceMan;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import aqdeveloper.thebhakkar.R;
import aqdeveloper.thebhakkar.Url.Constant;

public class ServieManLogin extends AppCompatActivity {


    TextView loginphone_number,loginpassword;
    Button notamember,login_btn,changepassword,forgotpassword,changepagsswordbtn,forgotpasswordbtn;
    EditText phone_numberpopup,old_password,new_password,forgotpasswordphonenumber;
    ProgressDialog progress;
    AlertDialog.Builder alBuilder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servie_man_login);

        if (SharedPrefrences.getInstance(getApplicationContext()).isLoggedIn()){
            SharedPreferences sharedpreferences = this.getSharedPreferences("sharepreferences", Context.MODE_PRIVATE);
            Intent i = new Intent(ServieManLogin.this,ServiceManUserDetails.class);
            i.putExtra("phone_number",sharedpreferences.getString("phone",null));
            startActivity(i);
            finish();
        }
        views();
        progress = new ProgressDialog(this);
        notamember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ServieManLogin.this,ServiceManRegister.class));
                finish();
            }
        });

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataFromServer();
            }
        });

        changepassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePasswordPopUp();
            }
        });

        forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forgotPasswordPopup();
            }
        });
    }

    public void views(){
        notamember = (Button) findViewById(R.id.txt_register);
        loginphone_number = (TextView) findViewById(R.id.loginphone_numbertxt);
        loginpassword = (TextView) findViewById(R.id.loginpasswordtxt);
        login_btn = (Button) findViewById(R.id.login_btn);
        changepassword = (Button) findViewById(R.id.changepassword);
        forgotpassword = (Button) findViewById(R.id.forgotpassword);
    }

    public void getDataFromServer(){
        progress.setMessage("Checking credentials....");
        progress.setCanceledOnTouchOutside(false);
        progress.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.LOG_IN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progress.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (!jsonObject.getBoolean("error")){
                                SharedPrefrences.getInstance(getApplicationContext()).login(loginphone_number.getText().toString().trim());
                                Intent i = new Intent(ServieManLogin.this,ServiceManUserDetails.class);
                                i.putExtra("phone_number",loginphone_number.getText().toString().trim());
                                startActivity(i);
                                finish();
                                Toast.makeText(ServieManLogin.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(ServieManLogin.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.dismiss();
                        LayoutInflater li = LayoutInflater.from(ServieManLogin.this);
                        View prompt = li.inflate(R.layout.promptdilog,null);
                        AlertDialog.Builder alertdialog = new AlertDialog.Builder(ServieManLogin.this);
                        alertdialog.setView(prompt);
                        TextView text = (TextView) prompt.findViewById(R.id.prompttextview);
                        alertdialog.setCancelable(false).setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(ServieManLogin.this,ServieManLogin.class));
                                finish();
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                finish();
                            }
                        });

                        AlertDialog alert = alertdialog.create();
                        alert.show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("phone_number",loginphone_number.getText().toString().trim());
                param.put("password",loginpassword.getText().toString().trim());
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void changePasswordPopUp(){
        View prompt = LayoutInflater.from(ServieManLogin.this).inflate(R.layout.change_password_pop_up,null);
        final AlertDialog.Builder alBuilder = new AlertDialog.Builder(ServieManLogin.this);
        alBuilder.setView(prompt);
        phone_numberpopup = (EditText) prompt.findViewById(R.id.phone_numberpopup);
        old_password = (EditText) prompt.findViewById(R.id.oldpassword);
        new_password = (EditText) prompt.findViewById(R.id.newpassword);
        changepagsswordbtn = (Button) prompt.findViewById(R.id.changepassword_btn);

        changepagsswordbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (phone_numberpopup.getText().length() == 0){
                    phone_numberpopup.setError("phone number is required");
                }else if (old_password.getText().length() ==0){
                    old_password.setError("old password is required");
                }else if (new_password.getText().length() ==0){
                    new_password.setError("new password is required");
                }else{
                    changePassword();
                }
            }
        });

        alBuilder.setCancelable(false).setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        alBuilder.create().show();

    }

    private void changePassword() {
        progress.setMessage("Processing please wait....");
        progress.setCanceledOnTouchOutside(false);
        progress.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.CHANGE_PASSWORD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progress.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Toast.makeText(ServieManLogin.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(ServieManLogin.this,ServieManLogin.class));
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.dismiss();
                        Toast.makeText(ServieManLogin.this, "communication server error", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("phone",phone_numberpopup.getText().toString());
                param.put("password",old_password.getText().toString());
                param.put("new_password",new_password.getText().toString());
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(ServieManLogin.this);
        requestQueue.add(stringRequest);
    }

    public void forgotPasswordPopup(){
        View prompt = LayoutInflater.from(ServieManLogin.this).inflate(R.layout.forgot_password,null);
        alBuilder = new AlertDialog.Builder(ServieManLogin.this);
        alBuilder.setView(prompt);

        forgotpasswordphonenumber = (EditText) prompt.findViewById(R.id.forgotpasswordphonenumber);
        forgotpasswordbtn = (Button) prompt.findViewById(R.id.forgotpassword_btn);


        forgotpasswordbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forGotPassword();
            }
        });

        alBuilder.setCancelable(false).setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        alBuilder.create().show();

    }

    private void forGotPassword() {
        progress.setMessage("Processing please wait.....");
        progress.setCanceledOnTouchOutside(false);
        progress.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.FORGOT_PASSWORD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progress.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Toast.makeText(ServieManLogin.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(ServieManLogin.this,ServieManLogin.class));
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.dismiss();
                        Toast.makeText(ServieManLogin.this, "Communication server error", Toast.LENGTH_SHORT).show();

                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("phone",forgotpasswordphonenumber.getText().toString());
                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(ServieManLogin.this);
        requestQueue.add(stringRequest);
    }
}
