package aqdeveloper.thebhakkar.ServiceMan;

public class ServiceManListItems {

    private String nametxt;
    private String addresstxt;
    private String idtxt;
    public String imageUrl;
    private String status,phonenumber,category;

    public ServiceManListItems(String nametxt, String addresstxt, String idtxt, String imageUrl, String status,String phonenumber, String category) {
        this.nametxt = nametxt;
        this.addresstxt = addresstxt;
        this.idtxt = idtxt;
        this.imageUrl = imageUrl;
        this.status = status;
        this.phonenumber = phonenumber;
        this.category = category;
    }

    public String getNametxt() {
        return nametxt;
    }

    public String getAddresstxt() {
        return addresstxt;
    }


    public String getIdtxt() {
        return idtxt;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getStatus() {
        return status;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public String getCategory() {
        return category;
    }
}
