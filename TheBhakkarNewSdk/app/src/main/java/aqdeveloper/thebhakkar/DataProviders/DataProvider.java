package aqdeveloper.thebhakkar.DataProviders;

public class DataProvider {
    String id, type, name, number1, number2, adress, description;



    public DataProvider(String id, String type, String name, String number1, String number2, String adress, String description) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.number1 = number1;
        this.number2 = number2;
        this.adress = adress;
        this.description = description;
    }

    public DataProvider() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber1() {
        return number1;
    }

    public void setNumber1(String number1) {
        this.number1 = number1;
    }

    public String getNumber2() {
        return number2;
    }

    public void setNumber2(String number2) {
        this.number2 = number2;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
