package aqdeveloper.thebhakkar.DataProviders;

public class UploadImage {

    public   String imageName, imageUri,key;

    public UploadImage(String imageName, String imageUri, String key) {
        this.imageName = imageName;
        this.imageUri = imageUri;
        this.key = key;
    }

    public UploadImage() {
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
