package aqdeveloper.thebhakkar.DataProviders;

public class ShopsHolder {

    String nameinenglish,nameinurdu,shopid;

    public String getNameinenglish() {
        return nameinenglish;
    }

    public void setNameinenglish(String nameinenglish) {
        this.nameinenglish = nameinenglish;
    }

    public String getNameinurdu() {
        return nameinurdu;
    }

    public void setNameinurdu(String nameinurdu) {
        this.nameinurdu = nameinurdu;
    }

    public String getShopid() {
        return shopid;
    }

    public void setShopid(String shopid) {
        this.shopid = shopid;
    }
}
