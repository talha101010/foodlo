package aqdeveloper.thebhakkar.DataProviders;

public class HomeDataProvider {

    private String btnName,btnNameEng;
    private int btnIcon;


    public String getBtnNameEng() {
        return btnNameEng;
    }

    public HomeDataProvider(String btnName, String btnNameEng, int btnIcon) {
        this.btnName = btnName;
        this.btnIcon = btnIcon;
        this.btnNameEng =btnNameEng;

    }

    public String getBtnName() {
        return btnName;
    }

    public int getBtnIcon() {
        return btnIcon;
    }
}
