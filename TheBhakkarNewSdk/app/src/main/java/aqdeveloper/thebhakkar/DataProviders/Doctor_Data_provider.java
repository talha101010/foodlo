package aqdeveloper.thebhakkar.DataProviders;

public class Doctor_Data_provider {

    private  String doctor_name_in_english;
    private  String doctor_name_in_urdu;
    private  String doctor_adress;
    private String product_heading;
    private  String product_detail;
    private  String product_number;
    private int doctor_icon;

    public int getDoctor_icon() {
        return doctor_icon;
    }

    public void setDoctor_icon(int doctor_icon) {
        this.doctor_icon = doctor_icon;
    }

    public Doctor_Data_provider(String doctor_name_in_english, String doctor_name_in_urdu, int doctor_icon) {

        this.doctor_name_in_english = doctor_name_in_english;
        this.doctor_name_in_urdu = doctor_name_in_urdu;
        this.doctor_icon = doctor_icon;
    }

    public Doctor_Data_provider(String doctor_name_in_english, String doctor_name_in_urdu, String doctor_adress) {
        this.doctor_name_in_english = doctor_name_in_english;
        this.doctor_name_in_urdu = doctor_name_in_urdu;
        this.doctor_adress = doctor_adress;
    }

    public  Doctor_Data_provider(){

    }

    public Doctor_Data_provider(String product_heading) {
        this.product_heading = product_heading;

    }

    public String getProduct_heading() {
        return product_heading;
    }

    public String getProduct_detail() {
        return product_detail;
    }

    public String getProduct_number() {
        return product_number;
    }

    public String getDoctor_name_in_english() {
        return doctor_name_in_english;
    }

    public void setDoctor_name_in_english(String doctor_name_in_english) {
        this.doctor_name_in_english = doctor_name_in_english;
    }

    public String getDoctor_name_in_urdu() {
        return doctor_name_in_urdu;
    }

    public void setDoctor_name_in_urdu(String doctor_name_in_urdu) {
        this.doctor_name_in_urdu = doctor_name_in_urdu;
    }

    public String getDoctor_adress() {
        return doctor_adress;
    }

    public void setDoctor_adress(String doctor_adress) {
        this.doctor_adress = doctor_adress;
    }
}
