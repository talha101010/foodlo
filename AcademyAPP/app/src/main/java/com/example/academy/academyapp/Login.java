package com.example.academy.academyapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class Login extends AppCompatActivity {
    private Button loginbtn;
    private EditText rollnumber,password;
    private DatabaseReference databaseReference;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getViews();
        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             if (rollnumber.getText().length()==0){
                 rollnumber.setError("This field is required");
             }else if (password.getText().length()==0){
                 password.setError("This field is required");
             }else{
                 progressDialog.setMessage("Authenticating.....");
                 progressDialog.setCanceledOnTouchOutside(false);
                 progressDialog.show();
                 getStudentData();
             }
            }
        });
    }
    private void getViews(){
        loginbtn = findViewById(R.id.loginbtn);
        rollnumber = findViewById(R.id.phonenumbertext);
        password = findViewById(R.id.passwordtext);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        progressDialog = new ProgressDialog(this);
    }

    private void getStudentData(){
        Query query = databaseReference.child("Students").orderByChild("rollno")
                .equalTo(rollnumber.getText().toString());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {
                if (dataSnapshot1.getChildrenCount()==0){
                    progressDialog.dismiss();
                    Toast.makeText(Login.this, "You enter an incorrect rollno", Toast.LENGTH_SHORT).show();
                }else {
                    for (DataSnapshot dataSnapshot:dataSnapshot1.getChildren()){
                        if (dataSnapshot.child("password").getValue().toString().equals(password.getText().toString().trim())) {
                            Toast.makeText(Login.this, "Logged in successfully", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Login.this,StudentDetail.class);
                        intent.putExtra("studentname",dataSnapshot.child("studentname").getValue().toString());
                        intent.putExtra("fathername",dataSnapshot.child("fathername").getValue().toString());
                        intent.putExtra("contactnumber",dataSnapshot.child("studentcontact").getValue().toString());
                        intent.putExtra("fcontactnumber",dataSnapshot.child("fathercontact").getValue().toString());
                        intent.putExtra("cnic",dataSnapshot.child("cnic").getValue().toString());
                        intent.putExtra("dob",dataSnapshot.child("dob").getValue().toString());
                        intent.putExtra("address",dataSnapshot.child("address").getValue().toString());
                        intent.putExtra("gender",dataSnapshot.child("gender").getValue().toString());
                        intent.putExtra("classid",dataSnapshot.child("classid").getValue().toString());
                        intent.putExtra("sessionid",dataSnapshot.child("sessionid").getValue().toString());
                        intent.putExtra("studentid",dataSnapshot.getKey());
                        intent.putExtra("rollno",dataSnapshot.child("rollno").getValue().toString());
                        intent.putExtra("insti",dataSnapshot.child("institutename").getValue().toString());
                        startActivity(intent);
                        finish();
                        progressDialog.dismiss();
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(Login.this, "You entered an incorrect password", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressDialog.dismiss();
                Toast.makeText(Login.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
