package com.example.academy.academyapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class BasicInfo extends Fragment {
    private TextView studentname,fname,scontactnumber,fcontactnumber,scnic,dob,address,classname,session,gender,
            assignedsubject,totalfee,rollno,institutename;
    private String subjects="";
    public static int totalfee1=0;
    private long count=0;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.basic_info, container, false);
        studentname = rootView.findViewById(R.id.studentname);
        fname = rootView.findViewById(R.id.fathername);
        scontactnumber = rootView.findViewById(R.id.contactnumber);
        fcontactnumber = rootView.findViewById(R.id.fcontactnumber);
        fcontactnumber = rootView.findViewById(R.id.fcontactnumber);
        scnic = rootView.findViewById(R.id.cnic);
        dob = rootView.findViewById(R.id.dob);
        address = rootView.findViewById(R.id.address);
        classname = rootView.findViewById(R.id.classname);
        session = rootView.findViewById(R.id.session);
        gender = rootView.findViewById(R.id.gender);
        assignedsubject = rootView.findViewById(R.id.assignedsubject);
        totalfee = rootView.findViewById(R.id.totalfee);
        rollno = rootView.findViewById(R.id.rollno);
        institutename = rootView.findViewById(R.id.institutename);
           getSubjects();
           getClassed();
           getSession();
        studentname.setText(getActivity().getIntent().getStringExtra("studentname"));
        fname.setText(getActivity().getIntent().getStringExtra("fathername"));
        scontactnumber.setText(getActivity().getIntent().getStringExtra("contactnumber"));
        fcontactnumber.setText(getActivity().getIntent().getStringExtra("fcontactnumber"));
        scnic.setText(getActivity().getIntent().getStringExtra("cnic"));
        dob.setText(getActivity().getIntent().getStringExtra("dob"));
        address.setText(getActivity().getIntent().getStringExtra("address"));
        gender.setText(getActivity().getIntent().getStringExtra("gender"));
        rollno.setText(getActivity().getIntent().getStringExtra("rollno"));
        institutename.setText(getActivity().getIntent().getStringExtra("insti"));


        return rootView;
    }

    private void getSubjects(){
        totalfee1=0;
        final DatabaseReference dbreference = FirebaseDatabase.getInstance().getReference().child("Students").
                child(getActivity().getIntent().getStringExtra("studentid")).child("Subjects");
        dbreference
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        subjects = "";
                        if (dataSnapshot.exists()){
                            count = dataSnapshot.getChildrenCount();
                            for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                                subjects +=   snapshot.child("name").getValue().toString()+" , ";
                                totalfee1 +=Integer.parseInt(snapshot.child("fee").getValue().toString());
                            }
                        }

                        if (count>0){
                            assignedsubject.setText(subjects);
                            totalfee.setText(totalfee1+"");
                        }else{
                            assignedsubject.setText("No subject assigned yet to this student");
                            totalfee.setText("No subject assigned yet to this student");
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(getActivity(), databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void getClassed(){
        FirebaseDatabase.getInstance().getReference().child("Classes")
                .child(getActivity().getIntent().getStringExtra("classid")).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                classname.setText(dataSnapshot.child("classname").getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getSession(){
        FirebaseDatabase.getInstance().getReference().child("Sessions")
                .child(getActivity().getIntent().getStringExtra("sessionid"))
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        session.setText(dataSnapshot.child("sessionname").getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }
}
