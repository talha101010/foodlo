package com.example.academy.academyapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Results extends Fragment {
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private DatabaseReference databaseReference;
    private List<ResultListItems> list;
    private RecyclerView.Adapter adapter;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.results, container, false);
        recyclerView = rootView.findViewById(R.id.resultrecyclerview);
        progressBar = rootView.findViewById(R.id.progressbar);
        list = new ArrayList<>();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        getResultsFromFirebase();

        return rootView;
    }

    private void getResultsFromFirebase(){
        progressBar.setVisibility(View.VISIBLE);
        Query query = databaseReference.child("Results").orderByChild("studentid")
                .equalTo(getActivity().getIntent().getStringExtra("studentid"));
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (adapter!=null){
                    list.size();
                }
                progressBar.setVisibility(View.GONE);
                for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                    ResultListItems listItems = new ResultListItems(snapshot.getKey(),
                            snapshot.child("mainexam").getValue().toString(),snapshot.child("subexam").getValue().toString(),
                            snapshot.child("subject").getValue().toString(),snapshot.child("date").getValue().toString(),
                            snapshot.child("totalmarks").getValue().toString(),snapshot.child("obtainedmarks").getValue().toString(),
                            snapshot.child("studentid").getValue().toString());
                    list.add(listItems);
                }
                adapter = new ResultAdapter(getActivity(),list);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
