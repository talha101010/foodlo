package com.example.academy.academyapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Fee extends Fragment {
    private RecyclerView recyclerView;
    private TextView totalfee;
    private DatabaseReference databaseReference;
    private ProgressBar progressBar;
    private List<FeeListItems> list;
    private RecyclerView.Adapter adapter;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fee, container, false);
        recyclerView = rootView.findViewById(R.id.feerecyclerview);
        list = new ArrayList<>();
        progressBar = rootView.findViewById(R.id.progressbar);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        getFee();

        return rootView;
    }

    private void getFee(){
        Query query = databaseReference.child("Fee").orderByChild("studentid")
                .equalTo(getActivity().getIntent().getStringExtra("studentid"));
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (adapter!=null){
                    list.clear();
                }
                progressBar.setVisibility(View.GONE);
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    FeeListItems listItems = new FeeListItems(snapshot.getKey(),snapshot.child("studentid").getValue().toString(),
                            snapshot.child("fee").getValue().toString(),snapshot.child("fine").getValue().toString(),
                            snapshot.child("feedate").getValue().toString());
                    list.add(listItems);
                }
                adapter = new FeeAdapter(getActivity(),list);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
