package com.example.academy.academyapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Attendance extends Fragment {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<AttendanceListItems> list;
    private ProgressBar progressBar;


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.attendance, container, false);
        recyclerView = rootView.findViewById(R.id.showattendancerv);
        list = new ArrayList<>();
        progressBar = rootView.findViewById(R.id.progressbar);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        getAttendance();
        return rootView;
    }

    private void getAttendance(){
        progressBar.setVisibility(View.VISIBLE);
        FirebaseDatabase.getInstance().getReference().child("Students").child(getActivity().getIntent().getStringExtra("studentid")).child("Attendance").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (adapter!=null){
                    list.clear();
                }
                progressBar.setVisibility(View.GONE);
                if (dataSnapshot.exists()){
                    for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                        AttendanceListItems listItems = new AttendanceListItems(snapshot.getKey(),
                                snapshot.child("present").getValue().toString(),snapshot.child("absent").getValue().toString(),
                                snapshot.child("leave").getValue().toString());
                        list.add(listItems);
                    }
                    adapter = new AttendanceAdapter(list,getActivity());
                    recyclerView.setAdapter(adapter);
                }else{
                    Toast.makeText(getActivity(), "no data is available", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


}
