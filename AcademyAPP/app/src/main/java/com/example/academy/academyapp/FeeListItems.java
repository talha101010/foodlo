package com.example.academy.academyapp;

public class FeeListItems {
    private String feeid,studentid,fee,fine,feedate;

    public FeeListItems(String feeid, String studentid, String fee, String fine, String feedate) {
        this.feeid = feeid;
        this.studentid = studentid;
        this.fee = fee;
        this.fine = fine;
        this.feedate = feedate;
    }

    public String getFeeid() {
        return feeid;
    }

    public String getStudentid() {
        return studentid;
    }

    public String getFee() {
        return fee;
    }

    public String getFine() {
        return fine;
    }

    public String getFeedate() {
        return feedate;
    }
}
