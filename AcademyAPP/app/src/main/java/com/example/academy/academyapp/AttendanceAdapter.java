package com.example.academy.academyapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class AttendanceAdapter extends RecyclerView.Adapter<AttendanceAdapter.ViewHolder> {
    private List<AttendanceListItems> list;
    private Context context;

    public AttendanceAdapter(List<AttendanceListItems> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.attendance_listitems,viewGroup,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
         AttendanceListItems listItems = list.get(i);
         viewHolder.month.setText(listItems.getAttendanceyear());
         viewHolder.absent.setText(listItems.getAbsent());
         viewHolder.present.setText(listItems.getPresent());
         viewHolder.leave.setText(listItems.getLeave());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView month,present,absent,leave;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            month = itemView.findViewById(R.id.month);
            present = itemView.findViewById(R.id.present);
            absent = itemView.findViewById(R.id.absent);
            leave = itemView.findViewById(R.id.leave);
        }
    }
}
