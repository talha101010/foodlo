package com.example.academy.academyapp;

public class ResultListItems {
    private String resultid,mainexam,subexam,subject,date,totalmarks,obtainedmarks,studentid;

    public ResultListItems(String resultid, String mainexam, String subexam, String subject, String date, String totalmarks, String obtainedmarks, String studentid) {
        this.resultid = resultid;
        this.mainexam = mainexam;
        this.subexam = subexam;
        this.subject = subject;
        this.date = date;
        this.totalmarks = totalmarks;
        this.obtainedmarks = obtainedmarks;
        this.studentid = studentid;
    }

    public String getResultid() {
        return resultid;
    }

    public String getMainexam() {
        return mainexam;
    }

    public String getSubexam() {
        return subexam;
    }

    public String getSubject() {
        return subject;
    }

    public String getDate() {
        return date;
    }

    public String getTotalmarks() {
        return totalmarks;
    }

    public String getObtainedmarks() {
        return obtainedmarks;
    }

    public String getStudentid() {
        return studentid;
    }
}
