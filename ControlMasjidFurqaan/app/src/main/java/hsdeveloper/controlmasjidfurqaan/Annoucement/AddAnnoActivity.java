package hsdeveloper.controlmasjidfurqaan.Annoucement;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import hsdeveloper.controlmasjidfurqaan.R;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

public class AddAnnoActivity extends AppCompatActivity {

    private static final int PICK_IMAGE_REQUEST = 1;
    private EditText etTitle;
    private ImageView selectImage;
    private Button addButton, chooseImageButton,updateBtn;
    ProgressBar imageProgressBar;
    String imageUri2;

    private Uri imageUri;
    private StorageReference storageReference;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_anno);

        views();

        storageReference = FirebaseStorage.getInstance().getReference("annoimages/");
        databaseReference = FirebaseDatabase.getInstance().getReference("anno");

        chooseImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser();
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etTitle.getText().toString().equals("")){

                    etTitle.setError("Put title");
                }else {

                    try {
                        uplaodimage(etTitle.getText().toString());
                    }catch (Exception e){

                        Toast.makeText(AddAnnoActivity.this, e+"", Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null){

            imageUri = data.getData();
            Picasso.with(this).load(imageUri).into(selectImage);


        }
    }

    private String getFileExtention(Uri uri){

        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();

        return  mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    private void uplaodimage(final String businessName){

        if (imageUri !=null){
          //  progressDialog.show();
            final   StorageReference sr = storageReference.child(businessName+"."+getFileExtention(imageUri));
            sr.putFile(imageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    imageProgressBar.setProgress(0);
                                }
                            },500);

                            sr.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {

                                    String imageId = databaseReference.push().getKey();



                                    imageUri2 = uri.toString();

                                    HashMap map=new HashMap();
                                    map.put("title",etTitle.getText().toString());

                                    map.put("key",imageId);
                                    map.put("imageUri",imageUri2);


                                    // UploadImage uploadImage = new UploadImage(businessName,imageUri2 ,imageId,phoneNumber,location,lat,lng
                                    // );

                                    databaseReference.child(imageId).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                          //  progressDialog.dismiss();
                                            Toast.makeText(AddAnnoActivity.this, "Upload Successfully", Toast.LENGTH_SHORT).show();
                                            finish();

                                        }
                                    });



                                }
                            });


                        }


                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            //progressDialog.dismiss();
                            Toast.makeText(AddAnnoActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                            imageProgressBar.setProgress((int) progress);

                        }
                    });

        }else {

            Toast.makeText(this, "No file Chooses", Toast.LENGTH_SHORT).show();
        }


    }



    private void openFileChooser(){

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,PICK_IMAGE_REQUEST);
    }


    public void  views(){

        etTitle = findViewById(R.id.et_name_of_business);

        addButton = findViewById(R.id.add_business_btn);
        chooseImageButton = findViewById(R.id.choose_image_btn);
        imageProgressBar = findViewById(R.id.imageprogressbar);
        updateBtn = findViewById(R.id.update_business_btn);
        selectImage = findViewById(R.id.imageview);


    }
}
