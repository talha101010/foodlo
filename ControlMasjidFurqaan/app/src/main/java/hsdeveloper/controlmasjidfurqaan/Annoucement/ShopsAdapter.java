package hsdeveloper.controlmasjidfurqaan.Annoucement;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import hsdeveloper.controlmasjidfurqaan.PrayerTimeActivity;
import hsdeveloper.controlmasjidfurqaan.R;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class ShopsAdapter extends RecyclerView.Adapter<ShopsAdapter.ViewHolder> {

    private Context mcontext;
    private ArrayList<UploadImage> mList;
    private ShopsAdapter.OnItemClickListener mListener;
    DatabaseReference databaseReference;

    public interface OnItemClickListener{
        void  onItemClick(int position);
        void  onButtonChange(int position);
    }

    public void setOnItemClickListener(ShopsAdapter.OnItemClickListener listener){
        mListener = listener;
    }

    public ShopsAdapter(Context context, ArrayList<UploadImage> list){

        this.mcontext = context;
        this.mList = list;


    }


    @Override
    public ShopsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mcontext);
        View view = inflater.inflate(R.layout.homscustomiselistview,parent,false);
        ShopsAdapter.ViewHolder viewHolder = new ShopsAdapter.ViewHolder(view,mListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ShopsAdapter.ViewHolder holder, int position) {

        final UploadImage uploadImage  = mList.get(position);

        TextView nameEng = holder.nameEng;
        ImageView homeicon = holder.icons;
        CardView cardView = holder.cardView;

        nameEng.setText(uploadImage.getTitle());
        // homeicon.setImageResource(homeDataProvider.getHomeIcon());

        Picasso.with(mcontext)
                .load(uploadImage.getImageUri())
                //.placeholder(R.drawable.placeholder)
                .fit()
                .centerCrop()
                .into(homeicon);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alertdialog = new AlertDialog.Builder(mcontext);
                final LayoutInflater inflater =LayoutInflater.from(mcontext);
                final View dialogView = inflater.inflate(R.layout.delete_update_dialog,null);
                final Button yesBtn = dialogView.findViewById(R.id.yes_btn);
                final Button noBtn = dialogView.findViewById(R.id.no_btn);

                alertdialog.setView(dialogView);
                final AlertDialog dialog = alertdialog.create();
                dialog.show();

                noBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                yesBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        databaseReference = FirebaseDatabase.getInstance().getReference("anno");
                        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
                        StorageReference storageReference = firebaseStorage.getReferenceFromUrl(uploadImage.getImageUri());
                        storageReference.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {

                            }
                        });
                        databaseReference.child(uploadImage.getKey()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {

                                dialog.dismiss();



                            }
                        });



                    }
                });
            }
        });


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView nameEng,seondName;
        public ImageView icons;
        public CardView cardView;


        public ViewHolder(View itemView, final ShopsAdapter.OnItemClickListener listener) {
            super(itemView);

            cardView =  itemView.findViewById(R.id.cardview);
            nameEng =itemView.findViewById(R.id.homeBtnNameEng);
            icons = itemView.findViewById(R.id.homeicon);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listener!=null){

                        int position  = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }

                }
            });
        }


    }

    public void setFilter(List<UploadImage> listitems1){
        mList = new ArrayList<>();
        mList.addAll(listitems1);
        notifyDataSetChanged();
    }

}
