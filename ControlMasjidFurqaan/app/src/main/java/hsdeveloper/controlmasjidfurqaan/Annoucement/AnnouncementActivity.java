package hsdeveloper.controlmasjidfurqaan.Annoucement;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import hsdeveloper.controlmasjidfurqaan.R;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class AnnouncementActivity extends AppCompatActivity {

    Button addAnnoBtn;
    RecyclerView recyclerView;
    private ArrayList<UploadImage> shopsList;
    ShopsAdapter adapter;
    DatabaseReference databaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announcement);

        views();



        addAnnoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               startActivity(new Intent(AnnouncementActivity.this, AddAnnoActivity.class));
            }
        });


        try {
            loadData();
        }catch (Exception e){

            Toast.makeText(this, e+"", Toast.LENGTH_SHORT).show();

        }

        shopsList = new ArrayList<>();
//
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
    }

    public  void  views(){

        addAnnoBtn = findViewById(R.id.add_annoucement_btn);
    }

    private void loadData() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait.....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        Query query = FirebaseDatabase.getInstance().getReference("anno");

        // databaseReference = FirebaseDatabase.getInstance().getReference("businesses").child(type);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                progressDialog.dismiss();
                shopsList.clear();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
//                    UploadImage dp = dataSnapshot1.getValue(UploadImage.class);
                    UploadImage dp = new UploadImage(dataSnapshot1.child("title").getValue().toString(),
                            dataSnapshot1.child("key").getValue().toString(),
                            dataSnapshot1.child("imageUri").getValue().toString()

                    );
                    shopsList.add(dp);
                }
                adapter = new ShopsAdapter(AnnouncementActivity.this, shopsList);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressDialog.dismiss();
                Toast.makeText(AnnouncementActivity.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
