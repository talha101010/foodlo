package hsdeveloper.controlmasjidfurqaan;

public class AddPrayerTimeDataProvider {

    String id,fajar,sunRise,duhr,asar,marib,isha,date,imDate;

    public AddPrayerTimeDataProvider(String id, String fajar, String sunRise, String duhr, String asar, String marib, String isha, String date, String imDate) {
        this.id = id;
        this.fajar = fajar;
        this.sunRise = sunRise;
        this.duhr = duhr;
        this.asar = asar;
        this.marib = marib;
        this.isha = isha;
        this.date = date;
        this.imDate = imDate;
    }

    public AddPrayerTimeDataProvider() {
    }

    public String getId() {
        return id;
    }

    public String getFajar() {
        return fajar;
    }

    public String getSunRise() {
        return sunRise;
    }

    public String getDuhr() {
        return duhr;
    }

    public String getAsar() {
        return asar;
    }

    public String getMarib() {
        return marib;
    }

    public String getIsha() {
        return isha;
    }

    public String getDate() {
        return date;
    }

    public String getImDate() {
        return imDate;
    }
}
