package hsdeveloper.controlmasjidfurqaan;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class AddPrayerTimeActivity extends AppCompatActivity {

    DatabaseReference databaseReference;
    AddPrayerTimeDataProvider dataProvider;
    final Calendar myCalendar = Calendar.getInstance();
    TextView DateTextview;
    CardView dateCardview;
    String strDate;

    Button addPrayerBtn;
    Spinner selectIslamicMonth;
    EditText etDateIslamic;
    EditText etFajrSTH,etFajrSTM,etFajrJTH,etFajrJTM;
    EditText etSunRiseSTH,etSunRiseSTM,etSunRiseJTH,etSunRiseJTM;
    EditText etDuhrSTH,etDuhrSTM,etDuhrJTH,etDuhrJTM;
    EditText etAsarSTH,etAsarSTM,etAsarJTH,etAsarJTM;
    EditText etMaribSTH,etMaribSTM,etMaribJTH,etMaribJTM;
    EditText etIshaSTH,etIshaSTM,etIshaJTH,etIshaJTM;

    Intent intent;
    String status;
    String id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_prayer_time);

        Views();

        intent = getIntent();
        status = intent.getStringExtra("status");
        if (status.equals("update")){

            addPrayerBtn.setText("Update Prayers Time");
            etFajrSTH.setText(intent.getStringExtra("sfh"));
            etFajrSTM.setText(intent.getStringExtra("sfm"));
            etFajrJTH.setText(intent.getStringExtra("jfh"));
            etFajrJTM.setText(intent.getStringExtra("jfm"));
            etSunRiseSTH.setText(intent.getStringExtra("ssh"));
            etSunRiseSTM.setText(intent.getStringExtra("ssm"));
            etDuhrSTH.setText(intent.getStringExtra("dsh"));
            etDuhrSTM.setText(intent.getStringExtra("dsm"));
            etDuhrJTH.setText(intent.getStringExtra("djh"));
            etDuhrJTM.setText(intent.getStringExtra("djm"));
            etAsarSTH.setText(intent.getStringExtra("ash"));
            etAsarSTM.setText(intent.getStringExtra("asm"));
            etAsarJTH.setText(intent.getStringExtra("ajh"));
            etAsarJTM.setText(intent.getStringExtra("ajm"));
            etMaribSTH.setText(intent.getStringExtra("msh"));
            etMaribSTM.setText(intent.getStringExtra("msm"));
            etMaribJTH.setText(intent.getStringExtra("mjh"));
            etMaribJTM.setText(intent.getStringExtra("mjm"));
            etIshaSTH.setText(intent.getStringExtra("ish"));
            etIshaSTM.setText(intent.getStringExtra("ism"));
            etIshaJTH.setText(intent.getStringExtra("ijh"));
            etIshaJTM.setText(intent.getStringExtra("ijm"));
            id = intent.getStringExtra("id");
        }else if (status.equals("new")){

            id = databaseReference.push().getKey();

        }

        DateTextview = findViewById(R.id.service_date_tv);
        dateCardview = findViewById(R.id.date_cardview);

        String myFormat = "MM-dd-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        DateTextview.setText(sdf.format(myCalendar.getTime()));
        strDate = sdf.format(myCalendar.getTime());

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "MM-dd-yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

               DateTextview.setText(sdf.format(myCalendar.getTime()));
                strDate = sdf.format(myCalendar.getTime());

            }

        };

        dateCardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                new DatePickerDialog(AddPrayerTimeActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        addPrayerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectIslamicMonth.getSelectedItemPosition() == 0){

                    Toast.makeText(AddPrayerTimeActivity.this, "Islamic Month Not Selected", Toast.LENGTH_SHORT).show();
                }else if (etDateIslamic.getText().toString().equals("")){

                    etDateIslamic.setError("Put Islamic Date");

                }else if (etFajrSTH.getText().toString().equals("")){

                    etFajrSTH.setError("Put Time Here");

                }else if (etFajrSTM.getText().toString().equals("")){

                    etFajrSTM.setError("Put Time Here");

                }else if (etFajrJTH.getText().toString().equals("")){

                    etFajrJTH.setError("Put Time Here");

                }else if (etFajrJTM.getText().toString().equals("")){

                    etFajrJTM.setError("Put Time Here");

                }else if (etSunRiseSTH.getText().toString().equals("")){

                    etSunRiseSTH.setError("Put Time Here");

                }else if (etSunRiseSTM.getText().toString().equals("")){

                    etSunRiseSTM.setError("Put Time Here");
                }else if (etDuhrSTH.getText().toString().equals("")){

                    etDuhrSTH.setError("Put Time Here");
                }else if (etDuhrSTM.getText().toString().equals("")){

                    etDuhrSTM.setError("Put Time Here");
                }else if (etDuhrJTH.getText().toString().equals("")){

                    etDuhrJTH.setError("Put Time Here");
                }else if (etDuhrJTM.getText().toString().equals("")){

                    etDuhrJTM.setError("Put Time Here");
                }else if (etAsarSTH.getText().toString().equals("")){

                    etAsarSTH.setError("Put Time Here");
                }else if (etAsarSTM.getText().toString().equals("")){

                    etAsarSTM.setError("Put Time Here");
                }else if (etAsarJTH.getText().toString().equals("")){

                    etAsarJTH.setError("Put Time Here");
                }else if (etAsarJTM.getText().toString().equals("")){
                    etAsarJTM.setError("Put Time Here");
                }else if (etMaribSTH.getText().toString().equals("")){

                    etMaribSTH.setError("Put Time Here");
                }else if (etMaribSTM.getText().toString().equals("")){
                    etMaribSTM.setError("Put Time Here");
                }else if (etMaribJTH.getText().toString().equals("")){

                    etMaribJTH.setError("Put Time Here");
                }else if (etMaribJTM.getText().toString().equals("")){

                    etMaribJTM.setError("Put Time Here");
                }else if (etIshaSTH.getText().toString().equals("")){

                    etIshaSTH.setError("Put Time Here");
                }else if (etIshaSTM.getText().toString().equals("")){

                    etIshaSTM.setError("Put Time Here");
                }else if (etIshaJTH.getText().toString().equals("")){
                    etIshaJTH.setError("Put Time Here");
                }else if (etIshaJTM.getText().toString().equals("")){

                    etIshaJTM.setError("Put Time Here");
                }else {


                    dataProvider = new AddPrayerTimeDataProvider(

                            id,
                            etFajrSTH.getText().toString()+":"+etFajrSTM.getText().toString()+"--"
                            +etFajrJTH.getText().toString()+":"+etFajrJTM.getText().toString(),
                            etSunRiseSTH.getText().toString()+":"+etSunRiseSTM.getText().toString()+"--"+
                                    "00:00",
                            etDuhrSTH.getText().toString()+":"+etDuhrSTM.getText().toString()+"--"
                                    +etDuhrJTH.getText().toString()+":"+etDuhrJTM.getText().toString(),
                            etAsarSTH.getText().toString()+":"+etAsarSTM.getText().toString()+"--"
                                    +etAsarJTH.getText().toString()+":"+etAsarJTH.getText().toString(),
                            etMaribSTH.getText().toString()+":"+etMaribSTM.getText().toString()+"--"
                                    +etMaribJTH.getText().toString()+":"+etMaribJTM.getText().toString(),
                            etIshaSTH.getText().toString()+":"+etIshaSTM.getText().toString()+"--"
                                    +etIshaJTH.getText().toString()+":"+etIshaJTM.getText().toString(),
                            DateTextview.getText().toString(),
                            selectIslamicMonth.getSelectedItem().toString()



                    );

                    databaseReference.child(id).setValue(dataProvider).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            Toast.makeText(AddPrayerTimeActivity.this, "Added", Toast.LENGTH_SHORT).show();
                            etAsarJTH.setText("");

                        }
                    });



                }

            }
        });


    }


    public void Views(){

        databaseReference = FirebaseDatabase.getInstance().getReference("prayers_times");

        addPrayerBtn = findViewById(R.id.add_prayer_btn);
        selectIslamicMonth = findViewById(R.id.select_islami_month);
        etDateIslamic = findViewById(R.id.et_date_islamic);

        etFajrSTH= findViewById(R.id.et_fajar_start_time_hours);
        etFajrSTM= findViewById(R.id.et_fajar_start_time_minuts);
        etFajrJTH = findViewById(R.id.et_fajar_jammat_time_hours);
        etFajrJTM= findViewById(R.id.et_fajar_jammat_time_minutes);

        etSunRiseSTH = findViewById(R.id.et_sunrise_starttime_hours);
        etSunRiseSTM= findViewById(R.id.et_sunrise_starttime_minutes);

        etDuhrSTH= findViewById(R.id.et_duhr_start_time_hours);
        etDuhrSTM= findViewById(R.id.et_duhr_start_time_minutes);
        etDuhrJTH= findViewById(R.id.et_duhr_jammat_time_hours);
        etDuhrJTM= findViewById(R.id.et_duhr_jammat_time_minutes);

        etAsarSTH= findViewById(R.id.et_asar_start_time_hours);
        etAsarSTM= findViewById(R.id.et_asar_start_time_minutes);
        etAsarJTH= findViewById(R.id.et_asar_jammat_time_hours);
        etAsarJTM= findViewById(R.id.et_asar_jammat_time_minutes);

        etMaribSTH= findViewById(R.id.et_marib_start_time_hours);
        etMaribSTM= findViewById(R.id.et_marib_start_time_minutes);
        etMaribJTH= findViewById(R.id.et_marib_jammat_time_hours);
        etMaribJTM= findViewById(R.id.et_marib_jammat_time_minutes);

        etIshaSTH = findViewById(R.id.et_isha_start_time_hours);
        etIshaSTM = findViewById(R.id.et_isha_start_time_miuntes);
        etIshaJTH = findViewById(R.id.et_isha_jammat_time_hours);
        etIshaJTM = findViewById(R.id.et_isha_jammat_time_minutes);

    }
}
