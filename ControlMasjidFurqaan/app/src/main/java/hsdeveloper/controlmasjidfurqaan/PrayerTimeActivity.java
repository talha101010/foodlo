package hsdeveloper.controlmasjidfurqaan;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class PrayerTimeActivity extends AppCompatActivity {

    //PrayerDataProvider dp;
    Query query;
    final Calendar myCalendar = Calendar.getInstance();
    String strDate;
    LinearLayout buttonsLayout;
    TextView DateTextview,islamicDateTextView,dayTextview,noDataTv;
    String imName;
    String fajarTime,sunRiseTime,DuhrTime,asarTime,maribTime,ishaTime;
    String[] fajrArray,sunRiseArray,duhrArray,asarArray,maribArray,ishaArray;
    TextView fajrStartTime, fajrJammatTime;
    TextView sunRiseStartTime, sunRiseEndTime;
    TextView duhrStartTime, duhrJammatTime;
    TextView asarStartTime, asarJammatTime;
    TextView maribStartTime, maribJammatTime;
    TextView ishaStartTime, ishaJammatTime;
    Button addNewTimeBtn, updateTimeBtn, deleteTimeBtn;
    String[] fajarHMarray,fajarJHMArray,sunRiseSHMArray, sunRiseJHMArray,duhrSHMArray, duharJHMArray;
    String[] asarSHMArray, asarJHMArray,maribSHMArray, maribJHMArray,ishaSHMArray, ishaJHMArray;
    String id;
    DatabaseReference databaseReference;
    ScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prayer_time);

        views();

        String myFormat = "MM-dd-yyyy";
        String dayFormat = "EEEE";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.UK);
        SimpleDateFormat dayf = new SimpleDateFormat(dayFormat, Locale.UK);

        DateTextview.setText(sdf.format(myCalendar.getTime()));
        //Toast.makeText(this, dayf.format(myCalendar.getTime()), Toast.LENGTH_SHORT).show();
        dayTextview.setText(dayf.format(myCalendar.getTime()));
        strDate = sdf.format(myCalendar.getTime());

        try {

            loadData(strDate);

        }catch (Exception e){

            Toast.makeText(this, e+"", Toast.LENGTH_SHORT).show();
            Toast.makeText(this, e+"", Toast.LENGTH_SHORT).show();
            Toast.makeText(this, e+"", Toast.LENGTH_SHORT).show();
            Toast.makeText(this, e+"", Toast.LENGTH_SHORT).show();
            Toast.makeText(this, e+"", Toast.LENGTH_SHORT).show();
        }

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "MM-dd-yyyy";
                String dayFormat = "EEEE";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.UK);
                SimpleDateFormat dayf = new SimpleDateFormat(dayFormat, Locale.UK);


                DateTextview.setText(sdf.format(myCalendar.getTime()));
                dayTextview.setText(dayf.format(myCalendar.getTime()));
                strDate = sdf.format(myCalendar.getTime());
                loadData(strDate);

            }

        };

        DateTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DatePickerDialog(PrayerTimeActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();



            }
        });


        addNewTimeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PrayerTimeActivity.this, AddPrayerTimeActivity.class)
                .putExtra("status","new")
                );
            }
        });

        deleteTimeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog.Builder alertdialog = new AlertDialog.Builder(PrayerTimeActivity.this);
                final LayoutInflater inflater =LayoutInflater.from(PrayerTimeActivity.this);
                final View dialogView = inflater.inflate(R.layout.delete_update_dialog,null);
                final  Button yesBtn = dialogView.findViewById(R.id.yes_btn);
                final Button noBtn = dialogView.findViewById(R.id.no_btn);

                alertdialog.setView(dialogView);
                final AlertDialog dialog = alertdialog.create();
                dialog.show();

                yesBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        databaseReference = FirebaseDatabase.getInstance().getReference("prayers_times");
                        databaseReference.child(id).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {

                                finish();

                            }
                        });



                    }
                });

                noBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });



            }
        });

        updateTimeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PrayerTimeActivity.this, AddPrayerTimeActivity.class)
                .putExtra("status","update")
                        .putExtra("date",DateTextview.getText().toString())
                        .putExtra("sfh",fajarHMarray[0])
                        .putExtra("sfm",fajarHMarray[1])
                        .putExtra("jfh",fajarJHMArray[0])
                        .putExtra("jfm",fajarJHMArray[1])
                        .putExtra("ssh",sunRiseSHMArray[0])
                        .putExtra("ssm",sunRiseSHMArray[1])
                        .putExtra("dsh",duhrSHMArray[0])
                        .putExtra("dsm",duhrSHMArray[1])
                        .putExtra("djh",duharJHMArray[0])
                        .putExtra("djm",duharJHMArray[1])
                        .putExtra("ash",asarSHMArray[0])
                        .putExtra("asm",asarSHMArray[1])
                        .putExtra("ajh",asarJHMArray[0])
                        .putExtra("ajm",asarJHMArray[1])
                        .putExtra("msh",maribSHMArray[0])
                        .putExtra("msm",maribSHMArray[1])
                        .putExtra("mjh",maribJHMArray[0])
                        .putExtra("mjm",maribJHMArray[1])
                        .putExtra("ish",ishaSHMArray[0])
                        .putExtra("ism",ishaSHMArray[1])
                        .putExtra("ijh",ishaJHMArray[0])
                        .putExtra("ijm",ishaJHMArray[1])
                        .putExtra("id",id)

                );
            }
        });

    }

    public void views(){

        DateTextview = findViewById(R.id.DateTextview);
        islamicDateTextView = findViewById(R.id.islamic_month_textview);
        fajrStartTime= findViewById(R.id.tv_fajar_start_time);
        fajrJammatTime = findViewById(R.id.tv_fajr_jammat_time);
        sunRiseStartTime = findViewById(R.id.tv_sunrise_start_time);
        sunRiseEndTime= findViewById(R.id.tv_sunrise_jammat_time);
        duhrStartTime= findViewById(R.id.tv_duhr_start_time);
        duhrJammatTime = findViewById(R.id.tv_duhr_jammat_time);
        asarStartTime = findViewById(R.id.tv_asar_start_time);
        asarJammatTime= findViewById(R.id.tv_asar_jammat_time);
        maribStartTime = findViewById(R.id.tv_marib_start_time);
        maribJammatTime = findViewById(R.id.tv_marib_jammat_time);
        ishaStartTime = findViewById(R.id.tv_isha_start_time);
        ishaJammatTime= findViewById(R.id.tv_isha_jammat_time);
        dayTextview = findViewById(R.id.day_textview);

        addNewTimeBtn = findViewById(R.id.add_time_button);
        updateTimeBtn = findViewById(R.id.update_time_button);
        deleteTimeBtn = findViewById(R.id.delete_time_button);
        buttonsLayout = findViewById(R.id.buttonsLayout);
        noDataTv = findViewById(R.id.no_data_tv);
        scrollView = findViewById(R.id.scrollview);

    }

    public  void loadData(String date){

        query = FirebaseDatabase.getInstance().getReference("prayers_times").orderByChild("date").equalTo(date);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                try {


                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                        imName = dataSnapshot1.child("imDate").getValue().toString();
                        fajarTime = dataSnapshot1.child("fajar").getValue().toString();
                        sunRiseTime = dataSnapshot1.child("sunRise").getValue().toString();
                        DuhrTime = dataSnapshot1.child("duhr").getValue().toString();
                        asarTime = dataSnapshot1.child("asar").getValue().toString();
                        maribTime = dataSnapshot1.child("marib").getValue().toString();
                        ishaTime = dataSnapshot1.child("isha").getValue().toString();
                        id = dataSnapshot1.child("id").getValue().toString();
                    }



                islamicDateTextView.setText(imName);
                fajrArray = fajarTime.split("--");
                sunRiseArray = sunRiseTime.split("--");
                duhrArray = DuhrTime.split("--");
                asarArray= asarTime.split("--");
                maribArray = maribTime.split("--");
                ishaArray = ishaTime.split("--");

                fajrStartTime.setText(fajrArray[0]);
                fajrJammatTime.setText(fajrArray[1]);
                sunRiseStartTime.setText(sunRiseArray[0]);
                sunRiseEndTime.setText("--");
                duhrStartTime.setText(duhrArray[0]);
                duhrJammatTime.setText(duhrArray[1]);
                asarStartTime.setText(asarArray[0]);
                asarJammatTime.setText(asarArray[1]);
                maribStartTime.setText("--");
                maribJammatTime.setText(maribArray[1]);
                ishaStartTime.setText(ishaArray[0]);
                ishaJammatTime.setText(ishaArray[1]);

                fajarHMarray = fajrArray[0].split(":");
                fajarJHMArray = fajrArray[1].split(":");
                sunRiseSHMArray = sunRiseArray[0].split(":");
                duhrSHMArray = duhrArray[0].split(":");
                duharJHMArray = duhrArray[1].split(":");
                asarSHMArray= asarArray[0].split(":");
                asarJHMArray= asarArray[1].split(":");
                maribSHMArray = maribArray[0].split(":");
                maribJHMArray = maribArray[1].split(":");
                ishaSHMArray= ishaArray[0].split(":");
                ishaJHMArray = ishaArray[1].split(":");
                buttonsLayout.setVisibility(View.VISIBLE);
                }catch (Exception e){

                    noDataTv.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
