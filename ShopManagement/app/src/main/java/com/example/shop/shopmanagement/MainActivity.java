package com.example.shop.shopmanagement;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shop.shopmanagement.Adapter.MenuAdapter;
import com.example.shop.shopmanagement.Adapter.ProductAdapter;
import com.example.shop.shopmanagement.Addition.AddProduct;
import com.example.shop.shopmanagement.Addition.AddType;
import com.example.shop.shopmanagement.ListItems.BillDataProvider;
import com.example.shop.shopmanagement.ListItems.MenuListItems;
import com.example.shop.shopmanagement.ListItems.ProductsListItems;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends AppCompatActivity {
    private EditText typename,productname,productprice,productquantity,limitnumber;
    private Spinner typespinner;
    private Toolbar toolbar;
    private NavigationView nv;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle mToggle;
    private DatabaseReference databaseReference;
    private ArrayList<String> typeList,typeid;
    public static List<MenuListItems> menuList;
    private ArrayAdapter<String> typeAdapter;
    private RecyclerView menurecyclerview;
    private RecyclerView.Adapter menuAdapter;
    private ProgressBar menuprogressbar;
    public static ProgressBar productsprogressbar;
    public static RecyclerView productrecyclerview;
    public static RecyclerView.Adapter productAdapter;
    public static List<ProductsListItems> productList;
    public static TextView cartnumber;
    public static Activity activity;
    DynamicFragmentAdapter dynamicFragmentAdapter;


    public static ViewPager viewPager;
    private TabLayout mTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = MainActivity.this;
        getViews();
        setSupportActionBar(toolbar);
        getTypeForMenu();
        mToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);

        drawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        View itemview =  nv.inflateHeaderView(R.layout.navigation_header);

        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id)
                {
                    case R.id.addtype:
                        openDialog();
                        break;
                    case R.id.addproduct:
                        openProductDialog();
                        break;
                    default:
                        return true;
                }
                return true;
            }
        });

        productrecyclerview.setLayoutManager(new LinearLayoutManager(this));
        productrecyclerview.setHasFixedSize(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        cartNumber();
    }

    private void getViews(){
        nv = findViewById(R.id.nv);
        drawerLayout = findViewById(R.id.drawerlayout);
        toolbar = findViewById(R.id.toolbar);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        typeList = new ArrayList<>();
        menuList = new ArrayList<>();
        typeid = new ArrayList<>();
        productList = new ArrayList<>();
        menurecyclerview = findViewById(R.id.menurecyclerview);
        menuprogressbar = findViewById(R.id.menuprogressbar);
        productsprogressbar = findViewById(R.id.productsprogressbar);
        productrecyclerview = findViewById(R.id.productsrecyclerview);
        cartnumber = findViewById(R.id.cartnumber);




//        viewPager = findViewById(R.id.viewpager);
//        mTabLayout =  findViewById(R.id.tabs);
//        viewPager.setOffscreenPageLimit(5);
//        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
//        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                viewPager.setCurrentItem(tab.getPosition());
//                if (tab.getPosition()!=0){
//                    getProducts(menuList.get(tab.getPosition()).getMenuid(),DynamicFragment.productrecyclerview,DynamicFragment.progressBar);
//                }
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//                getProducts(menuList.get(tab.getPosition()).getMenuid(),DynamicFragment.productrecyclerview,DynamicFragment.progressBar);
//            }
//        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openDialog(){
        final Dialog dialog = new Dialog(MainActivity.this,R.style.Theme_AppCompat_Light_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
          typename = dialog.findViewById(R.id.typenameedittext);
        Button dialogButton =  dialog.findViewById(R.id.closebtn);
        Button addbtn = dialog.findViewById(R.id.addbtn);
        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (typename.getText().length()==0){
                    typename.setError("This field is required");
                }else{
                    addTypeToFirebase(dialog);
                }

            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void openProductDialog(){
        final Dialog dialog = new Dialog(MainActivity.this,R.style.Theme_AppCompat_Light_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.addproductdialog);
        Button dialogButton =  dialog.findViewById(R.id.closebtn);
        Button addbtn = dialog.findViewById(R.id.addbtn);
        productname = dialog.findViewById(R.id.productname);
        productprice = dialog.findViewById(R.id.productprice);
        productquantity = dialog.findViewById(R.id.productquantity);
        limitnumber = dialog.findViewById(R.id.alertnumber);
        typespinner = dialog.findViewById(R.id.typespinner);
        final ProgressBar progressBar = dialog.findViewById(R.id.progressbar);
        typeAdapter = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_spinner_dropdown_item,typeList);
        getType(progressBar);
        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (productname.getText().length()==0){
                   productname.setError("This field is required");
               }else if (productprice.getText().length()==0){
                   productprice.setError("This field is required");
               }else if (productquantity.getText().length()==0){
                   productquantity.setError("This field is required");
               }else if (limitnumber.getText().length()==0){
                   limitnumber.setError("This field is required");
               }else if (typespinner.getSelectedItemPosition()==0){
                   Toast.makeText(MainActivity.this, "Please select type", Toast.LENGTH_SHORT).show();
               }else{
                  addProductToFirebase(dialog);
               }
            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void addTypeToFirebase(final Dialog dialog){
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);
        sweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#008577"));
        sweetAlertDialog.setTitleText("Loading");
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.show();
        AddType addType = new AddType();
        addType.setTypename(typename.getText().toString().trim());
        databaseReference.child("Types").push().setValue(addType)
        .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                sweetAlertDialog.dismiss();
                dialog.dismiss();
                Toast.makeText(MainActivity.this, "Type has been added", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                sweetAlertDialog.dismiss();
                Toast.makeText(MainActivity.this, "Communication server error", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void addProductToFirebase(final Dialog dialog){
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);
        sweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#008577"));
        sweetAlertDialog.setTitleText("Loading");
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.show();
        AddProduct addProduct = new AddProduct();
        addProduct.setProductname(productname.getText().toString().trim());
        addProduct.setProductprice(productprice.getText().toString().trim());
        addProduct.setProductquantity(productquantity.getText().toString().trim());
        addProduct.setLimit(limitnumber.getText().toString().trim());
        addProduct.setTypename(typeList.get(typespinner.getSelectedItemPosition()));
        addProduct.setTypeid(typeid.get(typespinner.getSelectedItemPosition()));
        databaseReference.child("Products").push().setValue(addProduct)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        sweetAlertDialog.dismiss();
                        dialog.dismiss();
                        Toast.makeText(MainActivity.this, "Product has been added", Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                sweetAlertDialog.dismiss();
                Toast.makeText(MainActivity.this, "Communication server error", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void getType(final ProgressBar progressBar){
        databaseReference.child("Types").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (typeList != null){
                    typeAdapter.clear();
                }
                progressBar.setVisibility(View.GONE);
                typeList.add(0,"Please Select Type");
                typeid.add(0,"typeid");
                for (DataSnapshot routeSnapShot:dataSnapshot.getChildren()){
                    String value =    routeSnapShot.child("typename").getValue().toString();
                    typeList.add(value);
                    typeid.add(routeSnapShot.getKey().toString());
                }
                typeAdapter.notifyDataSetChanged();
                typespinner.setAdapter(typeAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getTypeForMenu() {

        databaseReference.child("Types").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                menuprogressbar.setVisibility(View.GONE);
                 if (menuAdapter!=null){
                     menuList.clear();
                 }
                for (DataSnapshot routeSnapShot:dataSnapshot.getChildren()){
//                    mTabLayout.addTab(mTabLayout.newTab().setText(routeSnapShot.child("typename").getValue().toString()));
                    MenuListItems listItems = new MenuListItems(routeSnapShot.getKey().toString(),
                            routeSnapShot.child("typename").getValue().toString());
                    menuList.add(listItems);
                }
//                dynamicFragmentAdapter = new DynamicFragmentAdapter(getSupportFragmentManager(), mTabLayout.getTabCount());
//                viewPager.setAdapter(dynamicFragmentAdapter);
//                viewPager.setCurrentItem(0);
                menuAdapter = new MenuAdapter(menuList,MainActivity.this);
                 menurecyclerview.setAdapter(menuAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                menuprogressbar.setVisibility(View.GONE);
                Toast.makeText(MainActivity.this, databaseError.getMessage()+"", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getProducts(String typeid, final RecyclerView productrecyclerview, final ProgressBar productprogressbar){
        productprogressbar.setVisibility(View.VISIBLE);
        Query query = FirebaseDatabase.getInstance().getReference("Products")
                .orderByChild("typeid").equalTo(typeid);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (productAdapter != null){
                    productList.clear();
                }
                if (dataSnapshot.exists()){
                    productprogressbar.setVisibility(View.GONE);
                    for (DataSnapshot routesnapShot : dataSnapshot.getChildren()){
                        ProductsListItems listItems = new ProductsListItems(routesnapShot.getKey().toString(),
                                routesnapShot.child("productname").getValue().toString(),routesnapShot.child("productprice").getValue().toString(),
                                routesnapShot.child("productquantity").getValue().toString(),routesnapShot.child("limit").getValue().toString());
                        MainActivity.productList.add(listItems);
                    }
                    productAdapter = new ProductAdapter(MainActivity.productList,MainActivity.this);
                    productrecyclerview.setAdapter(MainActivity.productAdapter);
                }else{
                    productprogressbar.setVisibility(View.GONE);
                    Toast.makeText(MainActivity.this, "no data available", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                productprogressbar.setVisibility(View.GONE);
                Toast.makeText(MainActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void cartNumber(){
        ArrayList<BillDataProvider> arrayList = null;
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(activity);
        Gson gson = new Gson();
        Type type = new TypeToken<List<BillDataProvider>>() {
        }.getType();
        String json = appSharedPrefs.getString("cart", "");
        if (!json.equals("")) {
            arrayList = gson.fromJson(json, type);
            if (arrayList.size()==0){
//                cartnumber.setVisibility(View.GONE);

                cartnumber.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(activity, "Nothing in the cart", Toast.LENGTH_SHORT).show();
                    }
                });
            }else{
                cartnumber.setText(arrayList.size() + "");
                cartnumber.setVisibility(View.VISIBLE);

                cartnumber.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        activity. startActivity(new Intent(activity, CartActivity.class));
                    }
                });
            }

        } else {
            cartnumber.setVisibility(View.GONE);
            cartnumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(activity, "Nothing in the Cart Yet", Toast.LENGTH_SHORT).show();
                }
            });
        }


    }
}
