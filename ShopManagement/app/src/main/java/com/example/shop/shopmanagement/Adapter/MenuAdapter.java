package com.example.shop.shopmanagement.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shop.shopmanagement.ListItems.MenuListItems;
import com.example.shop.shopmanagement.ListItems.ProductsListItems;
import com.example.shop.shopmanagement.MainActivity;
import com.example.shop.shopmanagement.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {
    private List<MenuListItems> list;
    private Context context;

    public MenuAdapter(List<MenuListItems> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
       View v = LayoutInflater.from(context).inflate(R.layout.menu_list_items,viewGroup,false);
       return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
         final MenuListItems listItems = list.get(i);
         viewHolder.menuname.setText(listItems.getMenuname());
          if (i==0){
              getProducts(listItems.getMenuid());
          }
         viewHolder.menulayout.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 MainActivity.productsprogressbar.setVisibility(View.VISIBLE);
                 getProducts(listItems.getMenuid());
             }
         });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
          TextView menuname;
          RelativeLayout menulayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            menuname = itemView.findViewById(R.id.menuname);
            menulayout = itemView.findViewById(R.id.menulayout);
        }
    }

    public void getProducts(String typeid){
        Query query = FirebaseDatabase.getInstance().getReference("Products")
                .orderByChild("typeid").equalTo(typeid);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (MainActivity.productAdapter != null){
                    MainActivity.productList.clear();
                }
                MainActivity.productsprogressbar.setVisibility(View.GONE);
                for (DataSnapshot routesnapShot : dataSnapshot.getChildren()){
                    ProductsListItems listItems = new ProductsListItems(routesnapShot.getKey().toString(),
                            routesnapShot.child("productname").getValue().toString(),routesnapShot.child("productprice").getValue().toString(),
                            routesnapShot.child("productquantity").getValue().toString(),routesnapShot.child("limit").getValue().toString());
                    MainActivity.productList.add(listItems);
                }
                MainActivity.productAdapter = new ProductAdapter(MainActivity.productList,context);
                MainActivity.productrecyclerview.setAdapter(MainActivity.productAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(context, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
