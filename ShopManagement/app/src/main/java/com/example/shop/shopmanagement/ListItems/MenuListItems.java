package com.example.shop.shopmanagement.ListItems;

public class MenuListItems {
    private String menuid,menuname;

    public MenuListItems(String menuid, String menuname) {
        this.menuid = menuid;
        this.menuname = menuname;
    }

    public String getMenuid() {
        return menuid;
    }

    public String getMenuname() {
        return menuname;
    }
}
