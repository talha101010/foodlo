package com.example.shop.shopmanagement.ListItems;

public class ProductsListItems {
    private String productid,productname,productprice,productquantity,productlimi;

    public ProductsListItems(String productid, String productname, String productprice, String productquantity, String productlimi) {
        this.productid = productid;
        this.productname = productname;
        this.productprice = productprice;
        this.productquantity = productquantity;
        this.productlimi = productlimi;
    }

    public String getProductid() {
        return productid;
    }

    public String getProductname() {
        return productname;
    }

    public String getProductprice() {
        return productprice;
    }

    public String getProductquantity() {
        return productquantity;
    }

    public String getProductlimi() {
        return productlimi;
    }
}
