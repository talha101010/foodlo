package com.example.shop.shopmanagement.ListItems;

public class BillDataProvider {
    private String productid,productname,productprice,productquantity,productamount;

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getProductprice() {
        return productprice;
    }

    public void setProductprice(String productprice) {
        this.productprice = productprice;
    }

    public String getProductquantity() {
        return productquantity;
    }

    public void setProductquantity(String productquantity) {
        this.productquantity = productquantity;
    }

    public String getProductamount() {
        return productamount;
    }

    public void setProductamount(String productamount) {
        this.productamount = productamount;
    }
}
