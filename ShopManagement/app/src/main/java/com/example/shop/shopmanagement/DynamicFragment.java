package com.example.shop.shopmanagement;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shop.shopmanagement.Adapter.ProductAdapter;
import com.example.shop.shopmanagement.ListItems.MenuListItems;
import com.example.shop.shopmanagement.ListItems.ProductsListItems;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class DynamicFragment extends Fragment {
   public static RecyclerView productrecyclerview;
   public static ProgressBar progressBar;

    public static DynamicFragment newInstance() {
        return new DynamicFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dynamic_fragment_layout, container, false);
        initViews(view);
        productrecyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        productrecyclerview.setHasFixedSize(true);
        if (getArguments().getInt("position")==0){
            getProducts(MainActivity.menuList.get(0).getMenuid(),productrecyclerview,progressBar);
        }

        return view;
    }

    private void initViews(View view) {

         productrecyclerview = view.findViewById(R.id.productsrecyclerview);
         progressBar = view.findViewById(R.id.productsprogressbar);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void getProducts(String typeid, final RecyclerView productrecyclerview, final ProgressBar productprogressbar){
        productprogressbar.setVisibility(View.VISIBLE);
        Query query = FirebaseDatabase.getInstance().getReference("Products")
                .orderByChild("typeid").equalTo(typeid);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (MainActivity.productAdapter != null){
                    MainActivity.productList.clear();
                }
                productprogressbar.setVisibility(View.GONE);
                for (DataSnapshot routesnapShot : dataSnapshot.getChildren()){
                    ProductsListItems listItems = new ProductsListItems(routesnapShot.getKey().toString(),
                            routesnapShot.child("productname").getValue().toString(),routesnapShot.child("productprice").getValue().toString(),
                            routesnapShot.child("productquantity").getValue().toString(),routesnapShot.child("limit").getValue().toString());
                    MainActivity.productList.add(listItems);
                }
                MainActivity.productAdapter = new ProductAdapter(MainActivity.productList,getActivity());
                productrecyclerview.setAdapter(MainActivity.productAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                productprogressbar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
