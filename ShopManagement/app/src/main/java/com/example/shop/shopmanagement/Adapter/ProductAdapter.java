package com.example.shop.shopmanagement.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shop.shopmanagement.ListItems.BillDataProvider;
import com.example.shop.shopmanagement.ListItems.ProductsListItems;
import com.example.shop.shopmanagement.MainActivity;
import com.example.shop.shopmanagement.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    private List<ProductsListItems> list;
    private Context context;
    private SharedPreferences appSharePreference;
    private int index;

    public ProductAdapter(List<ProductsListItems> list, Context context) {
        this.list = list;
        this.context = context;
        appSharePreference = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
      View v = LayoutInflater.from(context).inflate(R.layout.product_list_items,viewGroup,false);
      return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
         ProductsListItems listItems = list.get(i);

        if (verifyOrder(i,viewHolder.cartquantity)){
            viewHolder.addtocart.setVisibility(View.GONE);
            viewHolder.plus.setVisibility(View.VISIBLE);
            viewHolder.minus.setVisibility(View.VISIBLE);
            viewHolder.cartquantity.setVisibility(View.VISIBLE);
        }else{
            viewHolder.addtocart.setVisibility(View.VISIBLE);
            viewHolder.plus.setVisibility(View.GONE);
            viewHolder.minus.setVisibility(View.GONE);
            viewHolder.cartquantity.setVisibility(View.GONE);
        }

         viewHolder.productname.setText(listItems.getProductname());
         viewHolder.productquantity.setText("Quantity="+listItems.getProductquantity());
         viewHolder.productprice.setText("Price="+listItems.getProductprice());
         int amount = Integer.parseInt(listItems.getProductquantity()) * Integer.parseInt(listItems.getProductprice());
         viewHolder.productamout.setText("Amount="+amount+"");

         viewHolder.edit.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Toast.makeText(context, "edit clicked", Toast.LENGTH_SHORT).show();
             }
         });

         viewHolder.addtocart.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 if (addToCart(i)){
                     viewHolder.addtocart.setVisibility(View.GONE);
                     viewHolder.plus.setVisibility(View.VISIBLE);
                     viewHolder.minus.setVisibility(View.VISIBLE);
                     viewHolder.cartquantity.setVisibility(View.VISIBLE);
                     viewHolder.cartquantity.setText("1");
                 }
             }
         });

         viewHolder.plus.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 int q = Integer.parseInt(viewHolder.cartquantity.getText().toString());
                 q += 1;
                 viewHolder.cartquantity.setText(q + "");
                 updateCart(q+"" , i);
             }
         });

        viewHolder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int q = Integer.parseInt(viewHolder.cartquantity.getText().toString());
                if (q != 1 ){
                    q -= 1;
                    viewHolder.cartquantity.setText(q+"");
                    updateCart(q+"",i);
                }else{
                    if (RemoveOrder(list.get(i).getProductid())){
                        notifyDataSetChanged();
                        MainActivity.cartNumber();
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private boolean addToCart(int position){
        ArrayList<BillDataProvider> arrayList = null;
        Gson gson = new Gson();
        Type type = new TypeToken<List<BillDataProvider>>() {
        }.getType();
        String json = appSharePreference.getString("cart","");
        if (!json.equals("")){
            arrayList = gson.fromJson(json,type);
        }else{
            arrayList = new ArrayList<>();
        }
        BillDataProvider billDataProvider = new BillDataProvider();
        billDataProvider.setProductid(list.get(position).getProductid());
        billDataProvider.setProductname(list.get(position).getProductname());
        billDataProvider.setProductprice(list.get(position).getProductprice());
        billDataProvider.setProductquantity("1");
        arrayList.add(billDataProvider);

        SharedPreferences.Editor prefsEditor = appSharePreference.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("cart",jsons);
        prefsEditor.commit();
        MainActivity.cartNumber();
        return true;
    }

    public void updateCart(String quantity, int position) {
        ArrayList<BillDataProvider> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillDataProvider>>() {
        }.getType();

        String json = appSharePreference.getString("cart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            arrayList = new ArrayList<>();

        for (int i = 0; i < arrayList.size(); i++) {


            if (arrayList.get(i).getProductid().equals(list.get(position).getProductid())) {
                index = i;
                BillDataProvider bill = arrayList.get(i);
                bill.setProductquantity(quantity);
                break;
            }


        }


        SharedPreferences.Editor prefsEditor = appSharePreference.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("cart", jsons);
        prefsEditor.commit();
    }

    public boolean RemoveOrder(String id) {
        ArrayList<BillDataProvider> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillDataProvider>>() {
        }.getType();

        String json = appSharePreference.getString("cart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            return false;


        for (int i = 0; i < arrayList.size(); i++) {


            if (arrayList.get(i).getProductid().equals(id)) {
                index = i;
                arrayList.remove(index);
                break;
            }


        }

        SharedPreferences.Editor prefsEditor = appSharePreference.edit();
        String jsons = gson.toJson(arrayList);
        prefsEditor.putString("cart", jsons);
        prefsEditor.commit();
        return true;


    }

    public boolean verifyOrder(int position, TextView quantity) {
        ArrayList<BillDataProvider> arrayList = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<BillDataProvider>>() {
        }.getType();

        String json = appSharePreference.getString("cart", "");

        if (!json.equals(""))
            arrayList = gson.fromJson(json, type);
        else
            return false;


        for (int i = 0; i < arrayList.size(); i++) {


            if (arrayList.get(i).getProductid().equals(list.get(position).getProductid())) {
                index = i;
                quantity.setText(arrayList.get(index).getProductquantity());
                return true;

            }


        }

        return false;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView productname,productquantity,productprice,productamout,cartquantity;
        Button addtocart,plus,minus;
        ImageView edit;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            productname = itemView.findViewById(R.id.productname);
            productquantity = itemView.findViewById(R.id.productquantity);
            productprice = itemView.findViewById(R.id.productprice);
            productamout = itemView.findViewById(R.id.productamout);
            cartquantity = itemView.findViewById(R.id.cartquantity);
            addtocart = itemView.findViewById(R.id.addtocartbtn);
            plus = itemView.findViewById(R.id.plus);
            minus = itemView.findViewById(R.id.minus);
            edit = itemView.findViewById(R.id.editbtn);
        }
    }
}
