package com.example.shop.shopmanagement.Addition;

public class AddType {
    private String typename;

    public AddType() {
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }
}
