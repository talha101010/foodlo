package com.example.shop.shopmanagement;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.shop.shopmanagement.ListItems.BillDataProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class CartActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private ArrayList<BillDataProvider> arrayList;
    private static SharedPreferences appSharedPreferences;
    private TextView nocart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        getViews();


        arrayList = null;
        Gson gson = new Gson();
        final Type type = new TypeToken<List<BillDataProvider>>() {
        }.getType();
        appSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String json = appSharedPreferences.getString("cart","");
        if (!json.equals("")){
            arrayList = gson.fromJson(json,type);
        }else{
            nocart.setVisibility(View.VISIBLE);
//            checkoutbtn.setEnabled(false);
        }
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new CartAdapter(arrayList,CartActivity.this));
    }

    private void getViews(){
        recyclerView = findViewById(R.id.cartrecyclerview);
        arrayList = new ArrayList<>();
        nocart = findViewById(R.id.nocarttext);
    }
}
