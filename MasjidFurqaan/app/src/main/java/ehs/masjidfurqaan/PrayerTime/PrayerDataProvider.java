package ehs.masjidfurqaan.PrayerTime;

public class PrayerDataProvider {

    String pName, pStartTime, pJamatTime;

    public PrayerDataProvider(String pName, String pStartTime, String pJamatTime) {
        this.pName = pName;
        this.pStartTime = pStartTime;
        this.pJamatTime = pJamatTime;
    }

    public PrayerDataProvider() {
    }

    public String getpName() {
        return pName;
    }

    public String getpStartTime() {
        return pStartTime;
    }

    public String getpJamatTime() {
        return pJamatTime;
    }
}
