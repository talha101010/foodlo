package ehs.masjidfurqaan.PrayerTime;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import ehs.masjidfurqaan.HomeAdapter;
import ehs.masjidfurqaan.HomeDataProvider;
import ehs.masjidfurqaan.R;

public class PrayerAdapter extends RecyclerView.Adapter<PrayerAdapter.ViewHolder> {

    private Context mcontext;
    private ArrayList<PrayerDataProvider> mList;
    private PrayerAdapter.OnItemClickListener mListener;

    public interface OnItemClickListener{
        void  onItemClick(int position);
        void  onButtonChange(int position);
    }

    public void setOnItemClickListener(PrayerAdapter.OnItemClickListener listener){
        mListener = listener;
    }

    public PrayerAdapter(Context context, ArrayList<PrayerDataProvider> list){

        this.mcontext = context;
        this.mList = list;


    }


    @Override
    public PrayerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mcontext);
        View view = inflater.inflate(R.layout.prayer_customise,parent,false);
        PrayerAdapter.ViewHolder viewHolder = new PrayerAdapter.ViewHolder(view,mListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PrayerAdapter.ViewHolder holder, int position) {

        PrayerDataProvider homeDataProvider  = mList.get(position);
        holder.pNameTv.setText(homeDataProvider.getpName());
        holder.pStartTv.setText(homeDataProvider.getpStartTime());
        holder.pJammatTv.setText(homeDataProvider.getpJamatTime());




    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView pNameTv, pStartTv,pJammatTv;

        public ViewHolder(View itemView, final PrayerAdapter.OnItemClickListener listener) {
            super(itemView);

            pNameTv = itemView.findViewById(R.id.prayer_name_tv);
            pStartTv = itemView.findViewById(R.id.start_time_tv);
            pJammatTv =itemView.findViewById(R.id.jammat_time_tv);




            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listener!=null){

                        int position  = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }

                }
            });
        }
    }
}
