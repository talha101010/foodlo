package ehs.masjidfurqaan;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import ehs.masjidfurqaan.Annoucement.AnnoucementAdapter;
import ehs.masjidfurqaan.Annoucement.AnnoucementDataProvider;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class AnnoucementActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    int icon[] = {R.drawable.banner,R.drawable.banner1,R.drawable.banner2};
    String[] title = {"Announcement 1","Announcement 2","Announcement 3"};
    String[] date ={"23-Feb-2020","24-Feb-2020","25-Feb-2020"};
    ArrayList<AnnoucementDataProvider> list;
    DatabaseReference databaseReference;
    AnnoucementAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_annoucement);

        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this,1));

        list = new ArrayList();

        loadData();

    }

    private void loadData() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait.....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        Query query = FirebaseDatabase.getInstance().getReference("anno");

        // databaseReference = FirebaseDatabase.getInstance().getReference("businesses").child(type);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                progressDialog.dismiss();
                 list.clear();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                  //  AnnoucementDataProvider dp = dataSnapshot1.getValue(AnnoucementDataProvider.class);
                    AnnoucementDataProvider dp1 = new AnnoucementDataProvider(dataSnapshot1.child("title").getValue().toString(),
                            dataSnapshot1.child("key").getValue().toString(),
                            dataSnapshot1.child("imageUri").getValue().toString()

                    );
                    list.add(dp1);
                }

                adapter = new AnnoucementAdapter(AnnoucementActivity.this, list);
                recyclerView.setAdapter(adapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressDialog.dismiss();
                Toast.makeText(AnnoucementActivity.this, "Communication server error", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
