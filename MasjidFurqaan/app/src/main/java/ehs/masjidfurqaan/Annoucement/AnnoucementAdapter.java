package ehs.masjidfurqaan.Annoucement;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import ehs.masjidfurqaan.HomeAdapter;
import ehs.masjidfurqaan.HomeDataProvider;
import ehs.masjidfurqaan.R;

public class AnnoucementAdapter extends RecyclerView.Adapter<AnnoucementAdapter.ViewHolder> {

    private Context mcontext;
    private ArrayList<AnnoucementDataProvider> mList;
    private AnnoucementAdapter.OnItemClickListener mListener;

    public interface OnItemClickListener{
        void  onItemClick(int position);
        void  onButtonChange(int position);
    }

    public void setOnItemClickListener(AnnoucementAdapter.OnItemClickListener listener){
        mListener = listener;
    }

    public AnnoucementAdapter(Context context, ArrayList<AnnoucementDataProvider> list){

        this.mcontext = context;
        this.mList = list;


    }


    @Override
    public AnnoucementAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mcontext);
        View view = inflater.inflate(R.layout.announcement_customise,parent,false);
        AnnoucementAdapter.ViewHolder viewHolder = new AnnoucementAdapter.ViewHolder(view,mListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AnnoucementAdapter.ViewHolder holder, int position) {

        AnnoucementDataProvider homeDataProvider  = mList.get(position);
        holder.announcementTitle.setText(homeDataProvider.getTitle());
       // holder.annoucementDate.setText(homeDataProvider.getTitle());
        Picasso.with(mcontext)
                .load(homeDataProvider.getImageUri())
                //.placeholder(R.drawable.placeholder)
                .fit()
                .centerCrop()
                .into(holder.icon);



    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView announcementTitle, annoucementDate;
        ImageView icon;


        public ViewHolder(View itemView, final AnnoucementAdapter.OnItemClickListener listener) {
            super(itemView);

            announcementTitle = itemView.findViewById(  R.id.announcemet_title);
            annoucementDate= itemView.findViewById(R.id.announcemet_date);
            icon = itemView.findViewById(R.id.announcemet_icon);




            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listener!=null){

                        int position  = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }

                }
            });
        }
    }
}

