package ehs.masjidfurqaan.Annoucement;

public class AnnoucementDataProvider {

    public   String title, imageUri,key;

    public AnnoucementDataProvider(String title, String key ,String imageUri
    ) {
        this.title = title;
        this.imageUri = imageUri;
        this.key = key;

    }


    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public String getTitle() {
        return title;
    }

    public void setKey(String key) {
        this.key = key;
    }


    public String getImageUri() {
        return imageUri;
    }

    public String getKey() {
        return key;
    }
}
