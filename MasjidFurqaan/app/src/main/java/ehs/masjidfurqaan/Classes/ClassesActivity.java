package ehs.masjidfurqaan.Classes;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import ehs.masjidfurqaan.HomeAdapter;
import ehs.masjidfurqaan.HomeDataProvider;
import ehs.masjidfurqaan.R;

import android.os.Bundle;

import java.util.ArrayList;

public class ClassesActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<HomeDataProvider> list;
    String[] title = {"Classes and \n Activities Calendar",
    "Men Regular \nClasses",
     "Ladies Regular\nClasses",
     "Upcomming Lectures \n and Events"};

    int[] icon = {R.drawable.ic_calendar__icon,
    R.drawable.ic_man_icon,
    R.drawable.ic_women_icon,
    R.drawable.ic_announcement_icon};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classes);

        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this,2));

        list = new ArrayList<>();

        for (int i=0; i<title.length; i++){

            HomeDataProvider dp = new HomeDataProvider(
                    title[i],
                    icon[i]
            );

            list.add(dp);

            HomeAdapter adapter = new HomeAdapter(ClassesActivity.this,list);

            recyclerView.setAdapter(adapter);
        }
    }
}
