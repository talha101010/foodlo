package ehs.masjidfurqaan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import ehs.masjidfurqaan.Classes.ClassesActivity;
import ehs.masjidfurqaan.PrayerTime.PrayerTimeActivity;

import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    String[]  homeName;
    ArrayList<HomeDataProvider> nameList;
    int[] homeIcon = {R.drawable.ic_prayer_time_icon,
            R.drawable.ic_announcement_icon,
            R.drawable.ic_classes_activity_icon,
            R.drawable.ic_classes_activity_icon,
            R.drawable.ic_classes_activity_icon,
            R.drawable.ic_madrassah_icon,
            R.drawable.ic_share_icon,
            R.drawable.ic_contact_us_icon,

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this,2));

        homeName = getResources().getStringArray(R.array.home_array);

        nameList = new ArrayList<>();

        for (int i=0; i<homeName.length; i++){

            HomeDataProvider dataProvider = new HomeDataProvider(homeName[i],homeIcon[i]);
            nameList.add(dataProvider);
        }

        HomeAdapter adapter = new HomeAdapter(MainActivity.this,nameList);
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new HomeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                switch (position){

                    case 0:

                        startActivity(new Intent(MainActivity.this, PrayerTimeActivity.class));

                        break;
                    case 1:
                        startActivity(new Intent(MainActivity.this,AnnoucementActivity.class));
                        break;
                    case 2:
                        startActivity(new Intent(MainActivity.this, ClassesActivity.class));
                        break;


                }
            }

            @Override
            public void onButtonChange(int position) {

            }
        });
    }
}
