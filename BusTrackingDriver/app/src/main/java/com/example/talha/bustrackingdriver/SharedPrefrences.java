package com.example.talha.bustrackingdriver;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefrences {
    private static SharedPrefrences mInstance;

    public static final String SHARE_PREF_NAME = "bustracking";
    public static final String SHARE_PREF_NAME1 = "bustracking1";


    private static Context mCtx;

    private SharedPrefrences(Context context) {
        mCtx = context;


    }

    public static synchronized SharedPrefrences getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefrences(context);
        }
        return mInstance;
    }

    public boolean login(String userid,String route,String driverid){
        SharedPreferences pref = mCtx.getSharedPreferences(SHARE_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("userid",userid);
        editor.putString("route",route);
        editor.putString("driverid",driverid);
        editor.apply();

        return true;
    }
    public boolean userType(String usertype){
        SharedPreferences pref = mCtx.getSharedPreferences(SHARE_PREF_NAME1, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("usertype",usertype);
        editor.apply();

        return true;
    }

    public boolean isLoggedIn(){
        SharedPreferences sharedpreferences = mCtx.getSharedPreferences(SHARE_PREF_NAME, Context.MODE_PRIVATE);
        if(sharedpreferences.getString("userid",null) != null){
            return true;
        }
        return false;
    }
    public boolean logout(){
        SharedPreferences pref = mCtx.getSharedPreferences(SHARE_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.apply();
        return true;
    }

    public boolean logout1(){
        SharedPreferences pref = mCtx.getSharedPreferences(SHARE_PREF_NAME1, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.apply();
        return true;
    }
}
