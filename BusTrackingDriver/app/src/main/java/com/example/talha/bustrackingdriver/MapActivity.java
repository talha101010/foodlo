package com.example.talha.bustrackingdriver;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.skyfishjy.library.RippleBackground;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProvider;
    private Location mLastKnownLocation;
    private LocationCallback mLocationCallBack;

    private Button startbtn,endbtn;
    private RippleBackground rippleBackground;
    private ImageView locationimage,locationimage1;

    private final float DEFAULT_ZOOM = 17;
    private double latitude,longitude;
    private Toolbar toolbar;
    private NavigationView nv;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle mToggle;
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private DatabaseReference dbreference;
    private Intent intent1;

    Handler h = new Handler();
    int delay = 5000;
    Runnable runnable;
    private boolean check;
    public static Activity activity;
    Intent intent;
    String userName, userId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        intent = getIntent();
        userName= intent.getStringExtra("ui");
        userId = intent.getStringExtra("pas");
        views();
        activity = this;



        Toast.makeText(activity, userName+userId, Toast.LENGTH_SHORT).show();
        Toast.makeText(activity, userName+userId, Toast.LENGTH_SHORT).show();
        setSupportActionBar(toolbar);
        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);

        startbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (check) {
                    final LatLng currentMarketLocation = mMap.getCameraPosition().target;
                    rippleBackground.startRippleAnimation();
                    startbtn.setVisibility(View.GONE);
                    endbtn.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            rippleBackground.stopRippleAnimation();
                            rippleBackground.removeAllViews();
                            locationimage.setVisibility(View.VISIBLE);
                            startService(intent1);
                           // startJourney();
                        }
                    }, 3000);
                }else{
                    startActivity(new Intent(MapActivity.this,SignIn_Activity.class));
                }
            }
        });

        endbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startbtn.setVisibility(View.VISIBLE);
                endbtn.setVisibility(View.GONE);
                startbtn.setText("Start Journey Again");
//                locationimage.setVisibility(View.GONE);
//                locationimage1.setVisibility(View.VISIBLE);
               // unregisterReceiver(broadcastReceiver);
                stopService(intent1);
//                h.removeCallbacks(runnable);
            }
        });



    }

    private void views(){
        startbtn = findViewById(R.id.startjourneybtn);
        rippleBackground = findViewById(R.id.ripple_bg);
        locationimage = findViewById(R.id.locationimage);
        mFusedLocationProvider = LocationServices.getFusedLocationProviderClient(MapActivity.this);
        endbtn = findViewById(R.id.endjourneybtn);
        toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawerlayout);
        nv = findViewById(R.id.nv);
        mAuth = FirebaseAuth.getInstance();
        dbreference = FirebaseDatabase.getInstance().getReference().child("Drivers").child(userName+userId);
        intent1 = new Intent(getApplicationContext(), GoogleService.class);
        locationimage1 = findViewById(R.id.locationimage1);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Toast.makeText(this, "map is ready", Toast.LENGTH_SHORT).show();

        try {

            mMap = googleMap;
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        }catch (Exception e){

            Toast.makeText(activity, e+"", Toast.LENGTH_SHORT).show();
            Toast.makeText(activity, e+"", Toast.LENGTH_SHORT).show();
            Toast.makeText(activity, e+"", Toast.LENGTH_SHORT).show();
            Toast.makeText(activity, e+"", Toast.LENGTH_SHORT).show();
            Toast.makeText(activity, e+"", Toast.LENGTH_SHORT).show();
            Toast.makeText(activity, e+"", Toast.LENGTH_SHORT).show();
            Toast.makeText(activity, e+"", Toast.LENGTH_SHORT).show();
            Toast.makeText(activity, e+"", Toast.LENGTH_SHORT).show();
        }


//        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
//                PackageManager.PERMISSION_GRANTED &&
//               ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
//                       PackageManager.PERMISSION_GRANTED) {
//
//
//
//
//
//
//
//        } else {
//
//       }

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        SettingsClient settingsClient = LocationServices.getSettingsClient(MapActivity.this);
        Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(builder.build());

        task.addOnSuccessListener(MapActivity.this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                   getDeviceLocation();
            }
        });

        task.addOnFailureListener(MapActivity.this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException){
                    ResolvableApiException resolvableApiException = (ResolvableApiException) e;
                    try {
                        resolvableApiException.startResolutionForResult(MapActivity.this,51);
                    } catch (IntentSender.SendIntentException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 51){
            if (resultCode == RESULT_OK){
                getDeviceLocation();
            }
        }
    }

    @SuppressLint("MissingPermission")
    private void getDeviceLocation(){
       mFusedLocationProvider.getLastLocation()
               .addOnCompleteListener(new OnCompleteListener<Location>() {
                   @Override
                   public void onComplete(@NonNull Task<Location> task) {
                       if (task.isSuccessful()){
                           mLastKnownLocation = task.getResult();
                           if (mLastKnownLocation != null){
                               latitude = mLastKnownLocation.getLatitude();
                               longitude = mLastKnownLocation.getLongitude();
                               if (latitude == 0.0){
                                   getDeviceLocation();
                                   return;
                               }else{
                                   mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(),mLastKnownLocation.getLongitude()),DEFAULT_ZOOM));
                               }

                           }else{
                               final LocationRequest locationRequest = LocationRequest.create();
                               locationRequest.setInterval(10000);
                               locationRequest.setFastestInterval(5000);
                               locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

                               mLocationCallBack = new LocationCallback(){
                                   @Override
                                   public void onLocationResult(LocationResult locationResult) {
                                       super.onLocationResult(locationResult);
                                       if (locationResult == null){
                                           return;
                                       }
                                       mLastKnownLocation = locationResult.getLastLocation();
                                       latitude = mLastKnownLocation.getLatitude();
                                       longitude = mLastKnownLocation.getLongitude();
                                       mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(),mLastKnownLocation.getLongitude()),DEFAULT_ZOOM));
                                       mFusedLocationProvider.removeLocationUpdates(mLocationCallBack);
                                   }
                               };

                               mFusedLocationProvider.requestLocationUpdates(locationRequest,mLocationCallBack,null);

                           }
                       }else{
                           Toast.makeText(MapActivity.this, "enable to get location", Toast.LENGTH_SHORT).show();
                       }
                   }
               });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (GoogleService.isRunning){
            startbtn.setVisibility(View.GONE);
            endbtn.setVisibility(View.VISIBLE);
            locationimage.setVisibility(View.VISIBLE);
            locationimage1.setVisibility(View.GONE);
        }
       // currentUser = mAuth.getCurrentUser();
        startbtn.setText("Start Journey");
        check = true;
//
        registerReceiver(broadcastReceiver,new IntentFilter(GoogleService.str_receiver));
    }

    private void updateData(double latitude1,double longitude1){
        Map<String,Object> updateValues = new HashMap<>();
        updateValues.put("/latitude",latitude1+"");
        updateValues.put("/longitude",longitude1+"");
        dbreference.updateChildren(updateValues, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                Toast.makeText(MapActivity.this, "location updated", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            double latitude1 = Double.parseDouble(intent.getStringExtra("latitude"));
            double longitude1 = Double.parseDouble(intent.getStringExtra("longitude"));
            updateData(latitude1,longitude1);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude1,longitude1),DEFAULT_ZOOM));
        }
    };
}
