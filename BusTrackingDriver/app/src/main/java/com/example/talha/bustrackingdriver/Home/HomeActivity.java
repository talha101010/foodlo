package com.example.talha.bustrackingdriver.Home;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.talha.bustrackingdriver.Add.AddCities;
import com.example.talha.bustrackingdriver.AddBusesActivity;
import com.example.talha.bustrackingdriver.BusesActivity;
import com.example.talha.bustrackingdriver.Passanger.BookingListActivity;
import com.example.talha.bustrackingdriver.R;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    HomeDataProvider homeDataProvider;
    String[] homestringname,seocndString;
    int[] homeicon = {R.drawable.ic_bus_icon
    ,R.drawable.ic_passanger_icon,R.drawable.ic_add_bus_icon
    ,R.drawable.ic_add_city};
    String[] seoondName;
    HomeAdapter homeAdapter;
    private ArrayList<HomeDataProvider> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        homestringname = getResources().getStringArray(R.array.home_array);
        //seocndString = getResources().getStringArray(R.array.second_name);

        list = new ArrayList<HomeDataProvider>();


        try {
            for (int i=0; i<homestringname.length; i++){



                String nameEng = homestringname[i];
                int homeiconitem = homeicon[i];
                // String second = seocndString[i];
                HomeDataProvider homeDataProvider= new HomeDataProvider(nameEng,"",homeiconitem);
                list.add(homeDataProvider);
            }

            homeAdapter = new HomeAdapter(getApplicationContext(),list);
            recyclerView.setAdapter(homeAdapter);


        }catch (Exception e){

            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();

        }

        homeAdapter.setOnItemClickListener(new HomeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                switch (position){

                    case 0:

                        startActivity(new Intent(HomeActivity.this, BusesActivity.class));

                        break;

                    case 2:

                          startActivity(new Intent(HomeActivity.this, AddBusesActivity.class));
                        break;

                    case 3:
                        startActivity(new Intent(HomeActivity.this, AddCities.class));
                        break;
                    case 1:

                        startActivity(new Intent(HomeActivity.this, BookingListActivity.class));

                        break;
                }
            }

            @Override
            public void onButtonChange(int position) {

            }
        });
    }
}
