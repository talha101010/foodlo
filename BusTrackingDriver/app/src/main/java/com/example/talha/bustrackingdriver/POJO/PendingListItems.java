package com.example.talha.bustrackingdriver.POJO;

public class PendingListItems {
    private String name,route,id;

    public PendingListItems(String name, String route,String id) {
        this.name = name;
        this.route = route;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRoute() {
        return route;
    }
}
