package com.example.talha.bustrackingdriver;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.talha.bustrackingdriver.Home.DataLoadingActivity;
import com.example.talha.bustrackingdriver.Home.HomeActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

public class SignIn_Activity extends AppCompatActivity {
    private EditText email,password;
    private Button signinbtn,registerbtn;
    Spinner selectType;


    public static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_);
        views();
        activity = this;
        signinbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email.getText().length() == 0){
                    email.setError("This field is required");
                }else if (password.getText().length() == 0 ){
                    password.setError("This field is required");
                }else  {

                    switch (selectType.getSelectedItem().toString()){

                        case "Passanger":

                            startActivity(new Intent(SignIn_Activity.this, DataLoadingActivity.class)
                            .putExtra("type","passanger")
                                    .putExtra("ui",email.getText().toString())
                                    .putExtra("pas",password.getText().toString())
                            );
                            finish();

                            break;

                        case "Bus Driver":

                            break;

                        case "Admin":

                            startActivity(new Intent(SignIn_Activity.this, DataLoadingActivity.class)
                            .putExtra("type","admin")
                                    .putExtra("ui","admin")
                                    .putExtra("pas","admin123")
                            );



                            break;

                        case "Driver":

                            startActivity(new Intent(SignIn_Activity.this,DataLoadingActivity.class)
                            .putExtra("type","bus")
                                    .putExtra("ui",email.getText().toString())
                                    .putExtra("pas",password.getText().toString())
                            );

                            break;
                    }

                }
            }
        });
        registerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignIn_Activity.this,Registration_Activity.class));
            }
        });
    }

    private void views(){
        email = findViewById(R.id.emailtext);
        password = findViewById(R.id.passwordtext);
        signinbtn = findViewById(R.id.loginbtn);
        registerbtn = findViewById(R.id.registerbtn);
        selectType =findViewById(R.id.select_type);
    }

}
