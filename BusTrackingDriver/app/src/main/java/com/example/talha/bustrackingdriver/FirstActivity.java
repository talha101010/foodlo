package com.example.talha.bustrackingdriver;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;

public class FirstActivity extends AppCompatActivity {
    private Button studentbtn,driverbtn,adminbtn;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        preferences = this.getSharedPreferences("bustracking1", Context.MODE_PRIVATE);
        String userType = preferences.getString("usertype",null);
        if (userType != null){
            if (userType.equals("Student")){
                startActivity(new Intent(FirstActivity.this,StudentTrackingActivity.class));
                return;
            }else if (userType.equals("Driver")){
                startActivity(new Intent(FirstActivity.this,MainActivity.class));
                return;
            }
        }
        studentbtn = findViewById(R.id.studentbtn);
        driverbtn = findViewById(R.id.driverbtn);
        adminbtn = findViewById(R.id.adminbtn);
        studentbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPrefrences.getInstance(getApplicationContext()).userType("Student");
                SharedPrefrences.getInstance(getApplicationContext()).logout();
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(FirstActivity.this,StudentTrackingActivity.class));
                finish();
            }
        });
        driverbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPrefrences.getInstance(getApplicationContext()).userType("Driver");
                SharedPrefrences.getInstance(getApplicationContext()).logout();
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(FirstActivity.this,MainActivity.class));
                finish();
            }
        });

        adminbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPrefrences.getInstance(getApplicationContext()).userType("Admin");
                SharedPrefrences.getInstance(getApplicationContext()).logout();
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(FirstActivity.this,AdminLogin.class));
            }
        });
    }
}
