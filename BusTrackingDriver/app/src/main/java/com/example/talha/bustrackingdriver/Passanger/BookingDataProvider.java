package com.example.talha.bustrackingdriver.Passanger;

public class BookingDataProvider {

    String bookId,fromCity,toCity,depatDate,returnDate,adultSeats,childSeats;
    String passangerId,credintial;

    public BookingDataProvider() {
    }

    public BookingDataProvider(String bookId, String fromCity, String toCity, String depatDate, String returnDate, String adultSeats, String childSeats, String passangerId,String credintial) {
        this.credintial = credintial;
        this.bookId = bookId;
        this.fromCity = fromCity;
        this.toCity = toCity;
        this.depatDate = depatDate;
        this.returnDate = returnDate;
        this.adultSeats = adultSeats;
        this.childSeats = childSeats;
        this.passangerId = passangerId;
    }

    public String getCredintial() {
        return credintial;
    }

    public String getBookId() {
        return bookId;
    }

    public String getFromCity() {
        return fromCity;
    }

    public String getToCity() {
        return toCity;
    }

    public String getDepatDate() {
        return depatDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public String getAdultSeats() {
        return adultSeats;
    }

    public String getChildSeats() {
        return childSeats;
    }

    public String getPassangerId() {
        return passangerId;
    }
}
