package com.example.talha.bustrackingdriver.Add;

public class AddDataProvider {

    String cityId,cityName;

    public AddDataProvider() {
    }

    public AddDataProvider(String cityId, String cityName) {
        this.cityId = cityId;
        this.cityName = cityName;
    }

    public String getCityId() {
        return cityId;
    }

    public String getCityName() {
        return cityName;
    }
}
