package com.example.talha.bustrackingdriver;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class StudentTrackingActivity extends AppCompatActivity implements OnMapReadyCallback {
    private DatabaseReference reference,dbreferenceroute;
    private double latitude,longitude;
    private GoogleMap mMap;
    private final float DEFAULT_ZOOM = 17;
    private ProgressDialog progressDialog;
    private Toolbar toolbar;
    private NavigationView nv;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle mToggle;
    private boolean check;
   // private FirebaseAuth mAuth;
   // private FirebaseUser currentUser;
    private Button loginbtn;
    private String route,driverid;
   // SharedPreferences preferences;
    public static Activity activity;
    private TextView logintext,routetrackingtext;
    private ArrayList<String> routelist;
    private ArrayAdapter<String> routelistAdapter;
    String busCre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_tracking);
        Intent intent = getIntent();
        busCre= intent.getStringExtra("buscre");

        views();

        setSupportActionBar(toolbar);
        activity = this;
        progressDialog = new ProgressDialog(this);

        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);

        mToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);

        drawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        View itemview =  nv.inflateHeaderView(R.layout.navigation_header);

        updateMap();

    }

    private void views(){
        toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawerlayout);
        nv = findViewById(R.id.nv);
      //  mAuth = FirebaseAuth.getInstance();
        loginbtn = findViewById(R.id.loginbtn);
        logintext = findViewById(R.id.logintext);
        routelist = new ArrayList<>();
        routetrackingtext = findViewById(R.id.routetrackingtext);
      //  dbreferenceroute = FirebaseDatabase.getInstance().getReference("Routs");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Toast.makeText(this, "map is ready", Toast.LENGTH_SHORT).show();
        mMap = googleMap;
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
    }







    private void updateMap(){


            progressDialog.setMessage("Checking Location....");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
            FirebaseDatabase.getInstance().getReference().child("Drivers").child(busCre)
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            progressDialog.dismiss();
                            if (dataSnapshot.exists()) {
                                latitude = Double.parseDouble(dataSnapshot.child("latitude").getValue().toString());
                                longitude = Double.parseDouble(dataSnapshot.child("longitude").getValue().toString());
                                if (latitude !=0 && longitude !=0){
                                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), DEFAULT_ZOOM));
                                }else{
                                    Toast.makeText(StudentTrackingActivity.this, "Bus did not start its journey yet", Toast.LENGTH_SHORT).show();
                                    logintext.setText("OOPS! IT SEEMS LIKE BUS DID NOT START ITS JOURNEY YET.");
                                    loginbtn.setText("Bus not started");
                                }

                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(StudentTrackingActivity.this, "bus not exist", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

    }
}
