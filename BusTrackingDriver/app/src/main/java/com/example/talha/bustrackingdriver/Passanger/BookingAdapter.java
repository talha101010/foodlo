package com.example.talha.bustrackingdriver.Passanger;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.talha.bustrackingdriver.AddBusDataProvider;
import com.example.talha.bustrackingdriver.Buses.BusesAdapter;
import com.example.talha.bustrackingdriver.R;

import java.util.ArrayList;

public class BookingAdapter extends RecyclerView.Adapter<BookingAdapter.ViewHolder> {

    private Context mcontext;
    private ArrayList<BookingDataProvider> mList;
    private BookingAdapter.OnItemClickListener mListener;

    public interface OnItemClickListener{
        void  onItemClick(int position);
        void  onButtonChange(int position);
    }

    public void setOnItemClickListener(BookingAdapter.OnItemClickListener listener){
        mListener = listener;
    }

    public BookingAdapter(Context context, ArrayList<BookingDataProvider> list){

        this.mcontext = context;
        this.mList = list;


    }


    @Override
    public BookingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mcontext);
        View view = inflater.inflate(R.layout.buses_customise,parent,false);
        BookingAdapter.ViewHolder viewHolder = new BookingAdapter.ViewHolder(view,mListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BookingAdapter.ViewHolder holder, int position) {

      final BookingDataProvider homeDataProvider  = mList.get(position);
        TextView busNo = holder.busesNo;
        TextView busFromTo = holder.busesFromTo;
        TextView busTiming = holder.busesTiming;
        TextView busHire = holder.hireTv;
        holder.icon_image_view.setImageResource(R.drawable.ic_passanger_icon);
        holder.timeImageView.setVisibility(View.GONE);
        holder.dollarImageView.setVisibility(View.GONE);


        busNo.setText(""+homeDataProvider.getFromCity()+"-"+homeDataProvider.getToCity());
        busFromTo.setText("Departure Date"+homeDataProvider.getDepatDate()+"\n Return Date" +homeDataProvider.getReturnDate());
        busTiming.setText("Seats "+homeDataProvider.getAdultSeats());
        busHire.setText("Child  "+homeDataProvider.getChildSeats());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mcontext.startActivity(new Intent(mcontext,ConfirmationActivity.class)
                .putExtra("customer",homeDataProvider.getPassangerId())
                        .putExtra("cre",homeDataProvider.getCredintial())
                        .putExtra("date",homeDataProvider.getDepatDate())
                );
            }
        });




    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView busesNo, busesFromTo,busesTiming,hireTv;
        public ImageView icon_image_view,timeImageView,dollarImageView;
        public CardView cardView;


        public ViewHolder(View itemView, final BookingAdapter.OnItemClickListener listener) {
            super(itemView);
            busesNo= itemView.findViewById(R.id.bus_no_textview);
            busesFromTo = itemView.findViewById(R.id.bus_from_to_textview);
            busesTiming= itemView.findViewById(R.id.timing_tv);
            hireTv = itemView.findViewById(R.id.hire_tv);
            icon_image_view = itemView.findViewById(R.id.bus_icon_imageview);
            timeImageView = itemView.findViewById(R.id.time_imageview);
            dollarImageView = itemView.findViewById(R.id.dollar_imageview);
            cardView = itemView.findViewById(R.id.cardview);



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listener!=null){

                        int position  = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }

                }
            });
        }
    }
}