package com.example.talha.bustrackingdriver.Passanger;

public class ConfirmedDataProvider {

    String credintial, busCredintial,date,amount,time;
    String seats,busno;

    public ConfirmedDataProvider() {
    }

    public ConfirmedDataProvider(String credintial, String busCredintial, String seats, String date, String amount, String time,String busno) {
        this.credintial = credintial;
        this.busCredintial = busCredintial;
        this.seats = seats;
        this.date = date;
        this.amount = amount;
        this.time = time;
        this.busno = busno;
    }

    public String getBusno() {
        return busno;
    }

    public String getCredintial() {
        return credintial;
    }

    public String getBusCredintial() {
        return busCredintial;
    }

    public String getSeats() {
        return seats;
    }

    public String getDate() {
        return date;
    }

    public String getAmount() {
        return amount;
    }

    public String getTime() {
        return time;
    }
}
