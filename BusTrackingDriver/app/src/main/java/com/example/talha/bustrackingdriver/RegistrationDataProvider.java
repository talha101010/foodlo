package com.example.talha.bustrackingdriver;

public class RegistrationDataProvider {

    String id, name, username,password;

    public RegistrationDataProvider() {
    }

    public RegistrationDataProvider(String id, String name, String username, String password) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
    }


    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setId(String id) {
        this.id = id;
    }
}
