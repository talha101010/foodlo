package com.example.talha.bustrackingdriver;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class StudentLogin extends AppCompatActivity {
    private EditText email,password;
    private Button signinbtn,registerbtn;

    private FirebaseAuth mAuth;
    private ProgressDialog progressDialog;
    public static Activity activity;
    private String uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_login);
        views();
        activity = this;
        signinbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email.getText().length() == 0){
                    email.setError("This field is required");
                }else if (password.getText().length() == 0 ){
                    password.setError("This field is required");
                }else{
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.setMessage("Checking Credentials.....");
                    progressDialog.show();
                    loginUser();
                }
            }
        });
        registerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StudentLogin.this,Registration_Activity.class));
            }
        });
    }


    private void views(){
        email = findViewById(R.id.emailtext);
        password = findViewById(R.id.passwordtext);
        signinbtn = findViewById(R.id.loginbtn);
        mAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);
        registerbtn = findViewById(R.id.registerbtn);
    }
    private void loginUser(){
        mAuth.signInWithEmailAndPassword(email.getText().toString().trim(), password.getText().toString().trim())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            progressDialog.dismiss();

                            final FirebaseUser user = mAuth.getCurrentUser();
                            FirebaseDatabase.getInstance().getReference().child("Students").child(user.getUid())
                                    .addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.child("studentstatus").getValue().toString().equals("Approved")) {
                                                Toast.makeText(StudentLogin.this, "Signed In Successfully", Toast.LENGTH_SHORT).show();
                                                FirebaseDatabase.getInstance().getReference().child("RoutsDetail")
                                                        .child(dataSnapshot.child("route").getValue().toString())
                                                        .addValueEventListener(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {
                                                                uid = dataSnapshot1.child("uid").getValue().toString();
                                                                SharedPrefrences.getInstance(getApplicationContext()).login(user.getUid(),
                                                                        dataSnapshot.child("route").getValue().toString(), uid);
                                                                finish();
                                                            }

                                                            @Override
                                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                                            }
                                                        });
                                            }else{
                                                Toast.makeText(StudentLogin.this, "Sorry student not approved yet", Toast.LENGTH_SHORT).show();
                                            }

                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });


                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(StudentLogin.this, "Authentication failed."+task.getException()+"",
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }
}
