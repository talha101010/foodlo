package com.example.talha.bustrackingdriver.Home;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.talha.bustrackingdriver.Add.AddDataProvider;
import com.example.talha.bustrackingdriver.AddBusDataProvider;
import com.example.talha.bustrackingdriver.MapActivity;
import com.example.talha.bustrackingdriver.Passanger.BookingActivity;
import com.example.talha.bustrackingdriver.R;
import com.example.talha.bustrackingdriver.RegistrationDataProvider;
import com.example.talha.bustrackingdriver.SignIn_Activity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class DataLoadingActivity extends AppCompatActivity {

    String userName,userPassward,strType;
    DatabaseReference databaseReference,busesDb;
    ArrayList<AddBusDataProvider> busesList;
    ArrayList<RegistrationDataProvider> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_loading);

        Intent intent = getIntent();

        strType = intent.getStringExtra("type");
        userName =intent.getStringExtra("ui");
        userPassward = intent.getStringExtra("pas");

        if (strType.equals("passanger")){

            dataLaod(userName,userPassward);


        }else if (strType.equals("admin")){

            if (userName.equals("admin")){

                if (userPassward.equals("admin123")){

                    startActivity(new Intent(DataLoadingActivity.this,HomeActivity.class));
                    finish();

                }else {

                    Toast.makeText(this, "Password is wrong", Toast.LENGTH_SHORT).show();
                }

            }else {

                Toast.makeText(this, "User Name is wrong", Toast.LENGTH_SHORT).show();
            }
        }

        busesList =new ArrayList<>();
        list =new ArrayList<>();
        if (strType.equals("bus")){
            busesDataLoad(userName,userPassward);
        }






    }

    public void dataLaod(final String userName, final String userPassward){

        databaseReference = FirebaseDatabase.getInstance().getReference("passanger").child(userName+userPassward);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()){

                    list.clear();

                    for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){

                        RegistrationDataProvider dp  = dataSnapshot1.getValue(RegistrationDataProvider.class);
                        list.add(dp);

                    }

                    String id = list.get(0).getId();

                    startActivity(new Intent(DataLoadingActivity.this, BookingActivity.class)
                    .putExtra("pid",id)
                            .putExtra("ui",userName)
                            .putExtra("pas",userPassward)

                    );



                    finish();
                }else {

                    Toast.makeText(DataLoadingActivity.this, "Not Exist", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(DataLoadingActivity.this,SignIn_Activity.class));
                    finish();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {


            }
        });



    }

    public void busesDataLoad(final  String userName, final String userPassward){

        busesDb = FirebaseDatabase.getInstance().getReference("buses");
        busesDb.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                busesList.clear();
                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){

                    AddBusDataProvider db = dataSnapshot1.getValue(AddBusDataProvider.class);
                    busesList.add(db);
                }

                for (int i=0; i<busesList.size(); i++){

                    if (userName.equals(busesList.get(i).getStrbusNo()) && userPassward.equals(busesList.get(i).getStrbuspassward())){

                        startActivity(new Intent(DataLoadingActivity.this, MapActivity.class)

                                        .putExtra("ui",busesList.get(i).getStrbusNo())
                                        .putExtra("pas",busesList.get(i).getStrbuspassward())
                        );
                        finish();
                        break;


                    }



                }

               // startActivity(new Intent(DataLoadingActivity.this,SignIn_Activity.class));
               // finish();



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
