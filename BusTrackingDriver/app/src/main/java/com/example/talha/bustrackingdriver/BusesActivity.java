package com.example.talha.bustrackingdriver;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.talha.bustrackingdriver.Buses.BusesAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class BusesActivity extends AppCompatActivity {

    Button openActivity,trackingButton;
    RecyclerView recyclerView;
    DatabaseReference databaseReference;
    ArrayList<AddBusDataProvider> busesList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buses);

        openActivity = findViewById(R.id.ma_activity_btn);
        openActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BusesActivity.this,MapActivity.class));
            }
        });

        trackingButton = findViewById(R.id.tracking_button);
        trackingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BusesActivity.this,StudentTrackingActivity.class));
            }
        });

        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));

        busesList =new ArrayList<>();

        databaseReference = FirebaseDatabase.getInstance().getReference("buses");
        databaseReference.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                busesList.clear();

                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){

                    AddBusDataProvider db = dataSnapshot1.getValue(AddBusDataProvider.class);
                    busesList.add(db);
                }

                if (busesList == null){

                    Toast.makeText(BusesActivity.this, "No bus added", Toast.LENGTH_SHORT).show();
                }
                else {

                    BusesAdapter adapter = new BusesAdapter(BusesActivity.this,busesList);
                    recyclerView.setAdapter(adapter);
                }



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
