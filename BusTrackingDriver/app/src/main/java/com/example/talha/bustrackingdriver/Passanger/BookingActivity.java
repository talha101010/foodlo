package com.example.talha.bustrackingdriver.Passanger;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.talha.bustrackingdriver.Add.AddDataProvider;
import com.example.talha.bustrackingdriver.R;
import com.example.talha.bustrackingdriver.StudentTrackingActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class BookingActivity extends AppCompatActivity {


    ListView selectListview;
    ArrayAdapter<String> cityAdapter,areaAdapter;
    AlertDialog dialog;
    TextView selectCityTv,selectToCityTv,departureDateTextview;
    TextView returnDateTv,plusSeatButton,numberofSeatsTv,minusNoOfSeats;
    TextView childMinusButton, childNoOfSeats,childPlusButton;
    ArrayList<AddDataProvider> citiesarray;
    DatabaseReference databaseReference,confirmeddatabase;
    String[] citiesStrArray;
    String selectedToCity,selectedCity,departtureDate,returnDate;
    final Calendar myCalendar = Calendar.getInstance();
    Button bookBtn;
    int noOfseats,minusNoOfSeatsMini;
    DatabaseReference bookingDb;
    String passangerId,userName,userPassword;
    String busCre,busno,bustime,bushire,departuredate,seats;
    Button trackBusButton,bookingIncvoice;
    ImageView bus_pics;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c);

        Intent intent = getIntent();
        passangerId = intent.getStringExtra("pid");
        userName = intent.getStringExtra("ui");
        userPassword= intent.getStringExtra("pas");
        Toast.makeText(this, userName+userPassword, Toast.LENGTH_SHORT).show();

        confirmedDb(userName+userPassword);

        databaseReference = FirebaseDatabase.getInstance().getReference("cities");
        bookingDb= FirebaseDatabase.getInstance().getReference().child("booking");

        selectedCity="Click to Select City";
        selectedToCity="Click to Select City";

        selectCityTv = findViewById(R.id.select_city_textview);
        selectToCityTv= findViewById(R.id.to_city_textview);
        bookBtn = findViewById(R.id.book_btn);
        departureDateTextview = findViewById(R.id.departure_date_textview);
        returnDateTv = findViewById(R.id.return_date);
        plusSeatButton = findViewById(R.id.plus_seat_button);
        numberofSeatsTv =findViewById(R.id.no_of_seats_tv);
        minusNoOfSeats =findViewById(R.id.minus_no_of_seats);
        childMinusButton =findViewById(R.id.child_minus_button);
        childNoOfSeats= findViewById(R.id.child_no_of_seats_tv);
        childPlusButton= findViewById(R.id.child_plus_button);
        trackBusButton = findViewById(R.id.track_bus_button);
        bookingIncvoice = findViewById(R.id.invoice_booking_button);
        bus_pics =findViewById(R.id.bus_pic);
        citiesarray = new ArrayList<AddDataProvider>();

        noOfseats =1;
        minusNoOfSeatsMini= 0;
        numberofSeatsTv.setText(noOfseats+"");
        childNoOfSeats.setText(minusNoOfSeatsMini+"");

        childPlusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minusNoOfSeatsMini++;
                childNoOfSeats.setText(minusNoOfSeatsMini+"");
            }
        });
        childMinusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {


                    if (minusNoOfSeatsMini == 0) {

                        Toast.makeText(BookingActivity.this, "You have reached minimum of seats", Toast.LENGTH_SHORT).show();
                    } else {

                        minusNoOfSeatsMini--;
                        childNoOfSeats.setText(minusNoOfSeatsMini + "");
                    }

                }catch (Exception e){

                    Toast.makeText(BookingActivity.this, "You have reached minimum of seats", Toast.LENGTH_SHORT).show();
                }
            }
        });


        plusSeatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noOfseats++;
                numberofSeatsTv.setText(noOfseats+"");

            }
        });

        minusNoOfSeats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{

                if (noOfseats ==1){

                    Toast.makeText(BookingActivity.this, "You have to select minimum one seat", Toast.LENGTH_SHORT).show();
                }else {

                    noOfseats--;
                    numberofSeatsTv.setText(noOfseats+"");
                }}catch (Exception e){

                    Toast.makeText(BookingActivity.this, "You have to select minimum one seat", Toast.LENGTH_SHORT).show();
                }

            }
        });


        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                citiesarray.clear();
                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){

                    AddDataProvider cityDb = dataSnapshot1.getValue(AddDataProvider.class);
                    citiesarray.add(cityDb);
                }

                citiesStrArray = new String[citiesarray.size()];
                for (int i =0; i<citiesStrArray.length; i++){

                    citiesStrArray[i] = citiesarray.get(i).getCityName();

                }





            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        selectCityTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectDialog(citiesStrArray,"Select your city");
                selectListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        selectedCity =  cityAdapter.getItem(position).toString();
                        selectCityTv.setText(selectedCity);
                        dialog.dismiss();

                    }
                });




            }
        });

        selectToCityTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectDialog(citiesStrArray,"Select Your City");

                selectListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        selectedToCity =  cityAdapter.getItem(position).toString();
                        selectToCityTv.setText(selectedToCity);
                        dialog.dismiss();

                    }
                });
            }
        });

        departureDateTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calendarOpen(departureDateTextview);

            }
        });

        returnDateTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendarOpen(returnDateTv);

            }
        });




        bookBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {


                if (selectedCity.equals(selectedToCity) ){

                    Toast.makeText(BookingActivity.this, "From and To cities are same", Toast.LENGTH_SHORT).show();
                }else {

                    if (selectedCity.equals("Click to Select City") || selectedToCity.equals("Click to Select City")){

                        Toast.makeText(BookingActivity.this, "You did not select city", Toast.LENGTH_SHORT).show();

                    }else {
                        String bookId = bookingDb.push().getKey();
                        BookingDataProvider dp = new BookingDataProvider(bookId,
                                selectedCity,selectedToCity,departureDateTextview.getText().toString(),
                                returnDateTv.getText().toString(),noOfseats+"",minusNoOfSeatsMini+""
                                ,passangerId,userName+userPassword);

                        bookingDb.child(departureDateTextview.getText().toString()).child(bookId).setValue(dp).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {

                                Toast.makeText(BookingActivity.this, "Sent Booking", Toast.LENGTH_SHORT).show();
                                selectedCity="Click to Select City";
                                selectedToCity="Click to Select City";

                            }
                        });


                    }


                }}catch (Exception e){


                }
            }
        });

        trackBusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BookingActivity.this, StudentTrackingActivity.class)
                .putExtra("buscre",busCre)
                );
            }
        });

        bookingIncvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alertdialog = new AlertDialog.Builder(BookingActivity.this);
                final LayoutInflater inflater =getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.month_dialog,null);
                final AlertDialog dialog;
                final  TextView busNumber = dialogView.findViewById(R.id.bus_number_tv);
                final TextView seatNumber = dialogView.findViewById(R.id.seat_number_tv);
                final  TextView busHire = dialogView.findViewById(R.id.bus_hire_tv);
                final  TextView busTime = dialogView.findViewById(R.id.bus_time_tv);
                final  TextView busDate =dialogView.findViewById(R.id.bus_date_tv);

                final  Button okButton = dialogView.findViewById(R.id.addInstallmentBtn);

                busNumber.setText(busno);
                seatNumber.setText(seats);
                busHire.setText(bushire);
                busTime.setText(bustime);
                busDate.setText(departtureDate);

                alertdialog.setView(dialogView);
                dialog = alertdialog.create();
                dialog.show();

                okButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });
    }

    public void selectDialog(String[] param,String parm1){

        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(BookingActivity.this);
        LayoutInflater inflater =getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.listview_dialog,null);
        alertdialog.setView(dialogView);
        alertdialog.setTitle(parm1);
        selectListview = dialogView.findViewById(R.id.selectlistview_in_dialog);
        EditText editText = dialogView.findViewById(R.id.searchtext);
        cityAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,param);
        selectListview.setAdapter(cityAdapter);
        selectListview.setTextFilterEnabled(true);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialog = alertdialog.create();
        dialog.show();
    }


    public void calendarOpen(final TextView dateTv){

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "dd-MMM-yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                dateTv.setText(sdf.format(myCalendar.getTime()));



            }

        };

        new DatePickerDialog(BookingActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();

    }

    public void confirmedDb(final String cre){

        confirmeddatabase = FirebaseDatabase.getInstance().getReference("confirmed").child(cre);
        confirmeddatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()){

                    try {

                        busCre = dataSnapshot.child("busCredintial").getValue().toString();
                        busno = dataSnapshot.child("busno").getValue().toString();
                        seats = dataSnapshot.child("seats").getValue().toString();
                        bushire =dataSnapshot.child("amount").getValue().toString();
                        bustime = dataSnapshot.child("time").getValue().toString();
                        departtureDate =dataSnapshot.child("date").getValue().toString();

                    }catch (Exception e){

                        Toast.makeText(BookingActivity.this, e+"", Toast.LENGTH_SHORT).show();
                    }


                }
                else {
                    bookingIncvoice.setVisibility(View.GONE);
                    trackBusButton.setVisibility(View.GONE);
                    bus_pics.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
