package com.example.talha.bustrackingdriver.Passanger;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.talha.bustrackingdriver.AddBusDataProvider;
import com.example.talha.bustrackingdriver.Buses.BusesAdapter;
import com.example.talha.bustrackingdriver.BusesActivity;
import com.example.talha.bustrackingdriver.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ConfirmationActivity extends AppCompatActivity implements MultipleChoiceDialogFragment.onMultiChoiceListnere{

    String passangerId,credintial,date;
    DatabaseReference databaseReference,confirmedSeats;
    TextView passangerLogoTv, passangerNameTv;
    RecyclerView recyclerView;
    ArrayList<AddBusDataProvider> busesList;
    BusesAdapter adapter;
    DialogFragment dialogFragment;
    int busNumber,bookSeats;
     StringBuilder stringBuilder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);

        Intent intent = getIntent();
        credintial = intent.getStringExtra("cre");
        passangerId = intent.getStringExtra("customer");
        date = intent.getStringExtra("date");
        getData(credintial,passangerId);

        passangerLogoTv= findViewById(R.id.passanger_logo_tv);
        passangerNameTv= findViewById(R.id.passanger_name_tv);


        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));

        busesList =new ArrayList<>();

        databaseReference = FirebaseDatabase.getInstance().getReference("buses");

        databaseReference.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                busesList.clear();

                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){

                    AddBusDataProvider db = dataSnapshot1.getValue(AddBusDataProvider.class);
                    busesList.add(db);
                }

                if (busesList == null){

                    Toast.makeText(ConfirmationActivity.this, "No bus added", Toast.LENGTH_SHORT).show();
                }
                else {

                    adapter = new BusesAdapter(ConfirmationActivity.this,busesList);
                    recyclerView.setAdapter(adapter);
                }

                adapter.setOnItemClickListener(new BusesAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(final int position) {
                       dialogFragment =new MultipleChoiceDialogFragment();
                        dialogFragment.setCancelable(false);
                        dialogFragment.show(getSupportFragmentManager(),"Multi Choice Dialog");
                        busNumber = position;
                    }

                    @Override
                    public void onButtonChange(int position) {

                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    public  void getData(String cre, String id){

        databaseReference = FirebaseDatabase.getInstance().getReference("passanger").child(cre).child(id);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String name = dataSnapshot.child("name").getValue().toString();
                char chlogo =name.charAt(0);
                String  strLogo = String.valueOf(chlogo);
                passangerLogoTv.setText(strLogo.toUpperCase());
                passangerNameTv.setText(name);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void onPositiveButtonClicked(String[] list, ArrayList<String> selectedList) {
         stringBuilder = new StringBuilder();
        stringBuilder.append("");
        for (String str:selectedList){

            stringBuilder.append(str+"_");


        }

        try {

            String str1 =  stringBuilder.toString();
            confirm(credintial,selectedList.size(),str1);
        }catch (Exception e){

            Toast.makeText(this, e+"", Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public void onNegativeButtonClicked() {


        dialogFragment.dismiss();


    }


    public void confirm(String credintial,int seats,String str1){

        int hire = Integer.parseInt(busesList.get(busNumber).getStrbusHire());
        int totalhire = hire*seats;

        confirmedSeats = FirebaseDatabase.getInstance().getReference("confirmed").child(credintial);
        ConfirmedDataProvider dataProvider = new ConfirmedDataProvider(credintial,
              busesList.get(busNumber).getStrbusNo()+busesList.get(busNumber).getStrbuspassward(),
                str1,date,
                String.valueOf(totalhire),
                busesList.get(busNumber).getStrbusDepatTime()
        ,busesList.get(busNumber).getStrbusNo()
           );
         confirmedSeats.setValue(dataProvider).addOnSuccessListener(new OnSuccessListener<Void>() {
             @Override
             public void onSuccess(Void aVoid) {
                 Toast.makeText(ConfirmationActivity.this, "Booked", Toast.LENGTH_SHORT).show();
             }
         });


    }
}
