package com.example.talha.bustrackingdriver.Buses;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.talha.bustrackingdriver.AddBusDataProvider;
import com.example.talha.bustrackingdriver.Home.HomeAdapter;
import com.example.talha.bustrackingdriver.Home.HomeDataProvider;
import com.example.talha.bustrackingdriver.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class BusesAdapter extends RecyclerView.Adapter<BusesAdapter.ViewHolder> {

    private Context mcontext;
    private ArrayList<AddBusDataProvider> mList;
    private BusesAdapter.OnItemClickListener mListener;

    public interface OnItemClickListener{
        void  onItemClick(int position);
        void  onButtonChange(int position);
    }

    public void setOnItemClickListener(BusesAdapter.OnItemClickListener listener){
        mListener = listener;
    }

    public BusesAdapter(Context context, ArrayList<AddBusDataProvider> list){

        this.mcontext = context;
        this.mList = list;


    }


    @Override
    public BusesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mcontext);
        View view = inflater.inflate(R.layout.buses_customise,parent,false);
        BusesAdapter.ViewHolder viewHolder = new BusesAdapter.ViewHolder(view,mListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BusesAdapter.ViewHolder holder, int position) {

       AddBusDataProvider homeDataProvider  = mList.get(position);
       TextView busNo = holder.busesNo;
       TextView busFromTo = holder.busesFromTo;
        TextView busTiming = holder.busesTiming;
        TextView busHire = holder.hireTv;

       busNo.setText("Bus No - "+homeDataProvider.getStrbusNo());
       busFromTo.setText("Rout - "+homeDataProvider.getStrbusFrom()+"-"+homeDataProvider.getStrbusTo());
       busTiming.setText(homeDataProvider.getStrbusDepatTime());
       busHire.setText(homeDataProvider.getStrbusHire());




    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView busesNo, busesFromTo,busesTiming,hireTv;
        public ImageView icons;
        public CardView cardView;


        public ViewHolder(View itemView, final BusesAdapter.OnItemClickListener listener) {
            super(itemView);
             busesNo= itemView.findViewById(R.id.bus_no_textview);
             busesFromTo = itemView.findViewById(R.id.bus_from_to_textview);
             busesTiming= itemView.findViewById(R.id.timing_tv);
             hireTv = itemView.findViewById(R.id.hire_tv);



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listener!=null){

                        int position  = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }

                }
            });
        }
    }
}

