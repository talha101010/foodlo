package com.example.talha.bustrackingdriver;

public class AddBusDataProvider {


    private String busId;
    private String strbusNo,strbusFrom,strbusTo,strbusStartLong,strbusStartAlti;
    private String strbusEndLong, strbusEndAlti,strbusNoOfSeats,strbusDepatTime;
    private String strbusHire,strbuspassward;

    public AddBusDataProvider() {
    }


    public AddBusDataProvider(String busId, String strbusNo, String strbusFrom, String strbusTo, String strbusStartLong, String strbusStartAlti, String strbusEndLong, String strbusEndAlti, String strbusNoOfSeats, String strbusDepatTime, String strbusHire, String strbuspassward) {
        this.busId = busId;
        this.strbusNo = strbusNo;
        this.strbusFrom = strbusFrom;
        this.strbusTo = strbusTo;
        this.strbusStartLong = strbusStartLong;
        this.strbusStartAlti = strbusStartAlti;
        this.strbusEndLong = strbusEndLong;
        this.strbusEndAlti = strbusEndAlti;
        this.strbusNoOfSeats = strbusNoOfSeats;
        this.strbusDepatTime = strbusDepatTime;
        this.strbusHire = strbusHire;
        this.strbuspassward = strbuspassward;
    }


    public String getBusId() {
        return busId;
    }

    public String getStrbusNo() {
        return strbusNo;
    }

    public String getStrbusFrom() {
        return strbusFrom;
    }

    public String getStrbusTo() {
        return strbusTo;
    }

    public String getStrbusStartLong() {
        return strbusStartLong;
    }

    public String getStrbusStartAlti() {
        return strbusStartAlti;
    }

    public String getStrbusEndLong() {
        return strbusEndLong;
    }

    public String getStrbusEndAlti() {
        return strbusEndAlti;
    }

    public String getStrbusNoOfSeats() {
        return strbusNoOfSeats;
    }

    public String getStrbusDepatTime() {
        return strbusDepatTime;
    }

    public String getStrbusHire() {
        return strbusHire;
    }

    public String getStrbuspassward() {
        return strbuspassward;
    }
}
