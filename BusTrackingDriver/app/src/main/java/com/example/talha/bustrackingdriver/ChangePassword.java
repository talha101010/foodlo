package com.example.talha.bustrackingdriver;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ChangePassword extends AppCompatActivity {
    private EditText newpassword;
    private Button changebtn;
    private FirebaseAuth mAuth;
    FirebaseUser currentUser;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        newpassword = findViewById(R.id.newpassword);
        changebtn = findViewById(R.id.changepassord);
        mAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);
        currentUser = mAuth.getInstance().getCurrentUser();


        changebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (newpassword.getText().length() ==0){
                    newpassword.setError("This field is required");
                }else{
                    progressDialog.setMessage("Please Wait....");
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.show();
                    changePassword();
                }
            }
        });
    }

    private void changePassword() {
        currentUser.updatePassword(newpassword.getText().toString().trim())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            progressDialog.dismiss();
                            Toast.makeText(ChangePassword.this, "Password has been changed", Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
                            progressDialog.dismiss();
                            Toast.makeText(ChangePassword.this, "error in updation process"+task.getException()+" Please login again", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
}
