package com.example.talha.bustrackingdriver.Passanger;

import android.app.DatePickerDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.example.talha.bustrackingdriver.AddBusDataProvider;
import com.example.talha.bustrackingdriver.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class BookingListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    DatabaseReference databaseReference,confirmeddatabase;
    ArrayList<BookingDataProvider> passangerList;
    TextView previousBooking;
    String formattedDate;
    final Calendar myCalendar = Calendar.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_list);

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        formattedDate = df.format(c);

        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));

        passangerList =new ArrayList<>();

        getData(formattedDate);

        previousBooking = findViewById(R.id.previous_booking_button);

        previousBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               calendarOpen(previousBooking);


            }
        });





    }


    public void getData(String date){

        databaseReference = FirebaseDatabase.getInstance().getReference("booking").child(date);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                passangerList.clear();

                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){

                    BookingDataProvider dp = dataSnapshot1.getValue(BookingDataProvider.class);
                    passangerList.add(dp);
                }

                BookingAdapter adapter =new BookingAdapter(BookingListActivity.this,passangerList);
                recyclerView.setAdapter(adapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }


    public void calendarOpen(final TextView dateTv){

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "dd-MMM-yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);



                dateTv.setText(sdf.format(myCalendar.getTime()));
                getData(previousBooking.getText().toString());



            }

        };

        new DatePickerDialog(BookingListActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();

    }


}
