package com.example.talha.bustrackingdriver;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AddBusesActivity extends AppCompatActivity {

    private   EditText busNo,busFrom,busTo,busStartLong,busStartAlti;
    private  EditText busEndLong, busEndAlti,busNoOfSeats,busDepatTime;
    private EditText busHire,buspassward;
    private  Button addBtn;
    private String strbusNo,strbusFrom,strbusTo,strbusStartLong,strbusStartAlti;
    private String strbusEndLong, strbusEndAlti,strbusNoOfSeats,strbusDepatTime;
    private String strbusHire,strbuspassward;

    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_buses);

        views();

        addBtn = findViewById(R.id.bus_add_button);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkEditText();
            }
        });

    }

    public void checkEditText(){

        strbusNo = busNo.getText().toString().trim();
        strbusFrom = busFrom.getText().toString().trim();
        strbusTo = busTo.getText().toString().trim();
        strbusStartLong = busStartLong.toString().trim();
        strbusStartAlti = busStartAlti.getText().toString().trim();
        strbusEndLong = busEndLong.getText().toString().trim();
        strbusEndAlti = busEndAlti.getText().toString().trim();
        strbusNoOfSeats = busNoOfSeats.getText().toString().trim();
        strbusDepatTime = busDepatTime.getText().toString().trim();
        strbusHire = busHire.getText().toString().trim();
        strbuspassward = buspassward.getText().toString().trim();


        if (strbusNo.equals("")){

            busNo.setError("Put");
        }else if (strbusFrom.equals("")){

            busFrom.setError("Put");
        }else if (strbusTo.equals("")){

            busTo.setError("Put");
        }else if (strbusStartLong.equals("")){

            busStartLong.setError("Put");
        }else if (strbusStartAlti.equals("")){

            busStartAlti.setError("Put");
        }else if (strbusEndLong.equals("")){

            busEndLong.setError("Put");
        }else if (strbusEndAlti.equals("")){

            busEndAlti.setError("Put");
        }else if (strbusNoOfSeats.equals("")){

            busNoOfSeats.setError("Put");
        }else if (strbusDepatTime.equals("")){

            busDepatTime.setError("Put");
        }else if (strbusHire.equals("")){

            busHire.setError("Put");
        }else if (strbuspassward.equals("")){

            buspassward.setError("Put");
        }else {

            databaseReference = FirebaseDatabase.getInstance().getReference("buses");
            String busId = databaseReference.push().getKey();
            AddBusDataProvider dataProvider = new AddBusDataProvider(busId,
                    strbusNo,strbusFrom,strbusTo,strbusStartLong,
                    strbusStartAlti,strbusEndLong,strbusEndAlti,
                    strbusNoOfSeats,strbusDepatTime,strbusHire,strbuspassward);
            databaseReference.child(busId).setValue(dataProvider)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            Toast.makeText(AddBusesActivity.this, "Add", Toast.LENGTH_SHORT).show();
                            busNo.setText("");
                            buspassward.setText("");

                        }
                    });



        }


    }


    public void views(){

        busNo = findViewById(R.id.bus_number_et);
        busFrom = findViewById(R.id.bus_from_et);
        busTo = findViewById(R.id.bus_to_et);
        busStartLong = findViewById(R.id.bus_start_longitude_et);
        busStartAlti = findViewById(R.id.bus_start_altitude_et);
        busEndLong = findViewById(R.id.bus_end_longitude_et);
        busEndAlti= findViewById(R.id.bus_end_altitude_et);
        busNoOfSeats = findViewById(R.id.bus_no_of_seats_et);
        busDepatTime = findViewById(R.id.bus_timing_et);
        busHire = findViewById(R.id.bus_hire_et);
        buspassward = findViewById(R.id.bus_passward_et);


    }
}
