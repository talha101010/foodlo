package com.example.talha.bustrackingdriver;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.talha.bustrackingdriver.POJO.Drivers;
import com.example.talha.bustrackingdriver.POJO.Routs;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Registration_Activity extends AppCompatActivity {

    private EditText name,email,password;
    private Button registerbtn,signinbtn;


    private DatabaseReference dbreference;
    private Drivers drivers;
    private String strName,strUsername,strPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_);
        views();


        registerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().length() == 0){
                    name.setError("This field is required");
                }else if (email.getText().length() == 0){
                    email.setError("This field is required");
                }else if (password.getText().length() == 0 ) {
                    password.setError("This field is required");
                }else
                    {

                     strName = name.getText().toString().trim();
                     strUsername = email.getText().toString().trim();
                     strPassword = password.getText().toString().trim();

                     registerUser(strName,strUsername,strPassword);

                }
            }
        });

        signinbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Registration_Activity.this,SignIn_Activity.class));
                finish();

            }
        });

    }
    private void views(){
        email = findViewById(R.id.emailtext);
        password = findViewById(R.id.passwordtext);
        registerbtn = findViewById(R.id.registerbtn);
        signinbtn = findViewById(R.id.signinbtn);
        name = findViewById(R.id.nametext);



    }

    private void registerUser(String name, String userName, String password) {

        dbreference =FirebaseDatabase.getInstance().getReference("passanger");
        String id =dbreference.push().getKey();
        RegistrationDataProvider dataProvider = new RegistrationDataProvider(id,name,userName,password);
        dbreference.child(userName+password).child(id).setValue(dataProvider).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                Toast.makeText(Registration_Activity.this, "Added", Toast.LENGTH_SHORT).show();
                finish();

            }
        });

    }
}
