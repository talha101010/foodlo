package com.example.talha.bustrackingdriver.Passanger;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

import com.example.talha.bustrackingdriver.R;

import java.util.ArrayList;


public class MultipleChoiceDialogFragment extends DialogFragment {

    public interface onMultiChoiceListnere{

        void onPositiveButtonClicked(String[] list,ArrayList<String> selectedList);
        void  onNegativeButtonClicked();
    }

    onMultiChoiceListnere mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener= (onMultiChoiceListnere) context;
        } catch (Exception e) {
            throw  new ClassCastException(getActivity().toString()+"onMultiChoiceListener must impelemented ");
        }

    }

    ArrayList selectedItemList = new ArrayList();

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final String[] wasteChoiceArray = new String[50];
        for (int i=0; i<wasteChoiceArray.length; i++){

            wasteChoiceArray[i] = i+"";

        }
        builder.setTitle("Choose Seats Number")
                .setMultiChoiceItems(wasteChoiceArray, null, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                        if (isChecked){

                            selectedItemList.add(wasteChoiceArray[which]);

                        }else {

                            selectedItemList.remove(wasteChoiceArray[which]);
                        }

                    }
                }).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                mListener.onPositiveButtonClicked(wasteChoiceArray,selectedItemList);

            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                mListener.onNegativeButtonClicked();

            }
        });

        return  builder.create();

    }


}

