package com.example.talha.bustrackingdriver.POJO;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.talha.bustrackingdriver.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class PendingAdapter extends RecyclerView.Adapter<PendingAdapter.ViewHolder> {
    private List<PendingListItems> list;
    private Context context;

    public PendingAdapter(List<PendingListItems> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      View v = LayoutInflater.from(context).inflate(R.layout.pending_student_items,parent,false);
      return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
         final PendingListItems listItems = list.get(position);
         holder.studentname.setText("Student Name : "+listItems.getName());
         holder.studentroute.setText("Student Route: "+listItems.getRoute());
         holder.approvebtn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Students").child(listItems.getId());
                 ref.child("studentstatus").setValue("Approved");
                 Toast.makeText(context, "student has been approved", Toast.LENGTH_SHORT).show();
             }
         });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView studentname,studentroute,approvebtn;

        public ViewHolder(View itemView) {
            super(itemView);
            studentname = itemView.findViewById(R.id.studentname);
            studentroute = itemView.findViewById(R.id.studentroute);
            approvebtn = itemView.findViewById(R.id.approvebtn);
        }
    }
}
