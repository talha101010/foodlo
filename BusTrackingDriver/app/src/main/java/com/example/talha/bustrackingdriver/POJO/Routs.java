package com.example.talha.bustrackingdriver.POJO;

public class Routs {
    private String rout,uid;

    public String getRout() {
        return rout;
    }

    public void setRout(String rout) {
        this.rout = rout;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
