package com.example.talha.bustrackingdriver;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AdminLogin extends AppCompatActivity {
    private EditText email,password;
    private Button loginbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);
        email = findViewById(R.id.emailtext);
        password = findViewById(R.id.passwordtext);
        loginbtn = findViewById(R.id.loginbtn);
        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email.getText().length()==0){
                    email.setError("This field is required");
                }else if (password.getText().length()==0){
                    password.setError("This field is required");
                }else{
                    if (email.getText().toString().equals("admin@gmail.com") && password.getText().toString().equals("admin")){
                         startActivity(new Intent(AdminLogin.this,StudentApproval.class));
                         finish();
                    }else{
                        Toast.makeText(AdminLogin.this, "email or password is incorrect", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}
