package com.example.talha.bustrackingdriver;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

public class MainActivity extends AppCompatActivity {
    private Button btn;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        preferences = this.getSharedPreferences("bustracking", Context.MODE_PRIVATE);
        String userid =preferences.getString("userid",null);
        if (userid != null) {
            if (!userid.equals("Driver")) {
                startActivity(new Intent(MainActivity.this, StudentTrackingActivity.class));
                return;
            }
        }
            if (ContextCompat.checkSelfPermission(MainActivity.this,Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED){
                startActivity(new Intent(MainActivity.this,MapActivity.class));
                finish();
                return;
            }


        btn = findViewById(R.id.grantpermissionbtn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dexter.withActivity(MainActivity.this)
                        .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                        .withListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse response) {
                               startActivity(new Intent(MainActivity.this,MapActivity.class));
                               finish();
                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse response) {
                                    if (response.isPermanentlyDenied()){
                                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                        builder.setTitle("Permission Denied");
                                        builder.setMessage("Location permission has been permanently denied you need to go to device settings to allow permission");
                                        builder.setNegativeButton("Cancel",null);
                                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent intent = new Intent();
                                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                                intent.setData(Uri.fromParts("package",getPackageName(),null));

                                            }
                                        })
                                        .show();
                                    }else{
                                        Toast.makeText(MainActivity.this, "Permission denied", Toast.LENGTH_SHORT).show();
                                    }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                 token.continuePermissionRequest();
                            }
                        })
                        .check();
            }
        });
    }
}
