package com.example.talha.bustrackingdriver;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.talha.bustrackingdriver.POJO.PendingAdapter;
import com.example.talha.bustrackingdriver.POJO.PendingListItems;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class StudentApproval extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<PendingListItems> list;
    private ProgressDialog progressDialog;
    private DatabaseReference dbreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_approval);
        recyclerView = findViewById(R.id.studentrecyclerview);
        list = new ArrayList<>();
        progressDialog = new ProgressDialog(this);
        dbreference = FirebaseDatabase.getInstance().getReference("Students");

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        getStudents();
    }

    private void getStudents(){
        progressDialog.setMessage("Please Wait....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        dbreference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                progressDialog.dismiss();
                if (adapter != null){
                    list.clear();
                }
                if (dataSnapshot.getChildrenCount()!=0) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        if (snapshot.child("studentstatus").getValue().equals("Pending")) {

                            PendingListItems listItems = new PendingListItems(snapshot.child("name").getValue().toString(),
                                    snapshot.child("route").getValue().toString(), snapshot.child("uid").getValue().toString());
                            list.add(listItems);
                        }
                    }

                        adapter = new PendingAdapter(list, StudentApproval.this);
                        recyclerView.setAdapter(adapter);

                }else{
                    Toast.makeText(StudentApproval.this, "sorry no student is in pending list", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressDialog.dismiss();
                Toast.makeText(StudentApproval.this, databaseError.getMessage()+"", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
