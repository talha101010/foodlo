package com.example.talha.bustrackingdriver.Add;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.talha.bustrackingdriver.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AddCities extends AppCompatActivity {

    EditText cityNameEt;
    Button addCityBtn;
    DatabaseReference databaseReference;
    private String strCityName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cities);

        cityNameEt = findViewById(R.id.city_name_et);
        addCityBtn = findViewById(R.id.add_city_btn);

        databaseReference = FirebaseDatabase.getInstance().getReference("cities");

        addCityBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strCityName = cityNameEt.getText().toString().trim();

                if (strCityName.equals("")){

                    cityNameEt.setError("Put");
                }else {
                    String cityId = databaseReference.push().getKey();
                    AddDataProvider cityDb = new AddDataProvider(cityId,strCityName);
                    databaseReference.child(cityId).setValue(cityDb).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            Toast.makeText(AddCities.this, "Added", Toast.LENGTH_SHORT).show();
                            cityNameEt.setText("");

                        }
                    });
                }





            }
        });
    }
}
